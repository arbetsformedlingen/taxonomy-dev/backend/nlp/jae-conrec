# Results for 'c44ea97752ae23a2c4ab6c4c7cf7ba9bb59cc559'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c44ea97752ae23a2c4ab6c4c7cf7ba9bb59cc559](README.md) | 1 | 3849 | 19 | 33 | 104/475 = **22%** | 6/21 = **29%** |

## Source text

Fysioterapeut till primärvårdsrehabilitering i Kil Region Värmland är till för att alla i Värmland ska må bra och för att vår region ska vara attraktiv och konkurrenskraftig. Vi är cirka 8000 medarbetare som arbetar med hälso- och sjukvård, tandvård, regional utveckling och kollektivtrafik.   Hälso- och sjukvården är Region Värmlands största verksamhet. Tillsammans med våra patienter stärker vi folkhälsan och utvecklar hälso- och sjukvården så att den kan bidra till en livskvalitet i världsklass för Värmlands befolkning. Vi driver 30 vårdcentraler med mödra- och barnhälsovård och många andra specialistmottagningar. I Värmland finns akutsjukhus i Arvika, Karlstad och Torsby.    Om verksamheten Vi söker en ny medarbetare till fysioterapimottagningen i Kil, här arbetar 3-4 fysioterapeuter.  På mottagningen bedriver vi vanlig primärvårdsrehabilitering, inklusive patientutbildningar och träning i vår träningslokal. Vi är en hälsofrämjande arbetsplats vilket bland annat märks genom att vi schemalägger tid för egen fysisk träning.  Vi är en del av primärvårdsrehabilitering södra, som även innefattar arbetsterapi- och fysioterapimottagningar i Karlstad och Hammarö. Vi jobbar för att det ska bli bra för patienterna inom hela enheten och är flexibla och stöttar upp på mottagningar där behov finns. Tjänstgöring på annan ort kan förekomma.  Vi har ett välkomnande och hjälpande klimat, där vi vill att du får en så bra start som möjligt. Du kommer därför ges möjlighet att påverka din introduktion och erbjudas en mentor om du så önskar. Ledarskapet bygger på utvecklande ledarskap och tillitsbaserat ledarskap för att både du som individ och organisationen ska utvecklas på bästa sätt. Som medarbetare ger det dig större möjlighet till inflytande och mer frihet och ansvar i ditt arbete. På arbetsplatsen är det ett mycket gott klimat men en god kollegial stämning.  Arbetsbeskrivning Som fysioterapeut hos oss arbetar du för att våra patienter ska uppnå eller bibehålla bästa möjliga funktionsförmåga, ett större oberoende och en god hälsa. Du möter vårdsökande med varierade behov av undersökning, råd och behandling. Vi utför sedvanlig primärvård där teamarbete och samarbete med andra aktörer är en viktig del.  Kvalifikationer Vi söker dig som är legitimerad fysioterapeut eller sjukgymnast meriterande är om du har erfarenhet och intresse av arbete i primärvård.  Som fysioterapeut tar du ansvar för din uppgift, strukturerar själv ditt angreppssätt och driver dina processer vidare. Du har lätt för att anpassa dig till ändrade omständigheter, är positiv och ser möjligheterna i förändringar. Du ser helheter, tar hänsyn till det större perspektivet och ser till hela verksamhetens bästa i agerande och beslut. Du arbetar bra med andra människor, relaterar till dem på ett lyhört och smidigt sätt. Du lyssnar och har lätt för att kommunicera med både patienter och andra aktörer.  https://www.regionvarmland.se/berattelser   Övrigt  I samband med eventuell anställning kan vi komma att be dig som sökande att lämna ett utdrag ur polisens belastningsregister. Inför anställning inom psykiatrisk vård, vård av funktionshindrade, vård av barn eller ungdomar gör arbetsgivaren en kontroll i polisens misstanke och belastningsregister. Ett utslag i detta register kan komma att påverka din möjlighet till anställning.   Inför rekryteringsarbetet har Region Värmland tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför alla erbjudanden om annonserings- och rekryteringshjälp i samband med denna annons.   Har du skyddad identitet och vill göra en ansökan ber vi dig att kontakta den kontaktpersonen som finns angiven i annonsen. Då får du hjälp att lämna in din ansökan utan att den hanteras i rekryteringsverktyget. Tänk på att endast ta med information som är relevant för den aktuella befattningen.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Fysioterapeut** till primärvårdsrehabiliterin... | x | x | 13 | 13 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 1 | **Fysioterapeut** till primärvårdsrehabiliterin... |  | x |  | 13 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 2 | ...l primärvårdsrehabilitering i **Kil** Region Värmland är till för a... | x | x | 3 | 3 | [Kil, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ocMw_Rz5_B1L) |
| 3 | ...dsrehabilitering i Kil Region **Värmland** är till för att alla i Värmla... |  | x |  | 8 | [Värmlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/EVVp_h6U_GSZ) |
| 4 | ...0 medarbetare som arbetar med **hälso- oc**h sjukvård, tandvård, regional... |  | x |  | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 5 | ...etar med hälso- och sjukvård, **tandvård**, regional utveckling och koll... |  | x |  | 8 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 6 | ...vård, regional utveckling och **kollektivtrafik**.   Hälso- och sjukvården är R... |  | x |  | 15 | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| 7 | ...ckling och kollektivtrafik.   **Hälso- och **sjukvården är Region Värmlands... |  | x |  | 11 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 8 | ...r vi folkhälsan och utvecklar **hälso- och **sjukvården så att den kan bidr... |  | x |  | 11 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 9 | ...i driver 30 vårdcentraler med **mödra- och barnhälsovård** och många andra specialistmot... |  | x |  | 24 | [Barnhälsovård/BVC, **skill**](http://data.jobtechdev.se/taxonomy/concept/kXEj_s4x_ATw) |
| 10 | ...dra specialistmottagningar. I **Värmland** finns akutsjukhus i Arvika, K... |  | x |  | 8 | [Värmlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/EVVp_h6U_GSZ) |
| 11 | ... Värmland finns akutsjukhus i **Arvika**, Karlstad och Torsby.    Om... | x | x | 6 | 6 | [Arvika, **municipality**](http://data.jobtechdev.se/taxonomy/concept/yGue_F32_wev) |
| 12 | ...d finns akutsjukhus i Arvika, **Karlstad** och Torsby.    Om verksamhe... | x | x | 8 | 8 | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| 13 | ...jukhus i Arvika, Karlstad och **Torsby**.    Om verksamheten Vi söke... | x | x | 6 | 6 | [Torsby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hQdb_zn9_Sok) |
| 14 | ...ill fysioterapimottagningen i **Kil**, här arbetar 3-4 fysioterapeu... | x | x | 3 | 3 | [Kil, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ocMw_Rz5_B1L) |
| 15 | ...ningen i Kil, här arbetar 3-4 **fysioterapeuter**.  På mottagningen bedriver ... |  | x |  | 15 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 15 | ...ningen i Kil, här arbetar 3-4 **fysioterapeuter**.  På mottagningen bedriver ... |  | x |  | 15 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 16 | ...och fysioterapimottagningar i **Karlstad** och Hammarö. Vi jobbar för at... | x | x | 8 | 8 | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| 17 | ...pimottagningar i Karlstad och **Hammarö**. Vi jobbar för att det ska bl... | x | x | 7 | 7 | [Hammarö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/x5qW_BXr_aut) |
| 18 | ...mning.  Arbetsbeskrivning Som **fysioterapeut** hos oss arbetar du för att vå... | x | x | 13 | 13 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 18 | ...mning.  Arbetsbeskrivning Som **fysioterapeut** hos oss arbetar du för att vå... |  | x |  | 13 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 19 | ...t större oberoende och en god **hälsa**. Du möter vårdsökande med var... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 20 | ...ehandling. Vi utför sedvanlig **primärvård** där teamarbete och samarbete ... |  | x |  | 10 | [primärvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WvGv_6YE_mun) |
| 21 | ...tför sedvanlig primärvård där **teamarbete** och samarbete med andra aktör... | x |  |  | 10 | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| 22 | ...primärvård där teamarbete och **samarbete** med andra aktörer är en vikti... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 23 | ... söker dig som är legitimerad **fysioterapeut** eller sjukgymnast meriterande... | x | x | 13 | 13 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 23 | ... söker dig som är legitimerad **fysioterapeut** eller sjukgymnast meriterande... |  | x |  | 13 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 24 | ...gitimerad fysioterapeut eller **sjukgymnast** meriterande är om du har erfa... | x | x | 11 | 11 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 25 | ...nhet och intresse av arbete i **primärvård**.  Som fysioterapeut tar du an... | x |  |  | 10 | [Primärvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/73kE_uFW_vvT) |
| 25 | ...nhet och intresse av arbete i **primärvård**.  Som fysioterapeut tar du an... |  | x |  | 10 | [primärvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WvGv_6YE_mun) |
| 26 | ... av arbete i primärvård.  Som **fysioterapeut** tar du ansvar för din uppgift... | x | x | 13 | 13 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 26 | ... av arbete i primärvård.  Som **fysioterapeut** tar du ansvar för din uppgift... |  | x |  | 13 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 27 | ...rd.  Som fysioterapeut tar du **ansvar** för din uppgift, strukturerar... | x |  |  | 6 | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
| 28 | ...dina processer vidare. Du har **lätt för att anpassa dig till ändrade omständigheter**, är positiv och ser möjlighet... | x |  |  | 52 | [anpassa sig efter ändrade förutsättningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gnWC_Nrw_yKE) |
| 29 | ...sta i agerande och beslut. Du **arbetar bra med andra människor**, relaterar till dem på ett ly... | x |  |  | 31 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 30 | ... smidigt sätt. Du lyssnar och **har lätt för att kommunicera** med både patienter och andra ... | x |  |  | 28 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 31 | ...ister. Inför anställning inom **psykiatrisk vård**, vård av funktionshindrade, v... |  | x |  | 16 | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| 32 | ...ekryteringsarbetet har Region **Värmland** tagit ställning till rekryter... |  | x |  | 8 | [Värmlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/EVVp_h6U_GSZ) |
| | **Overall** | | | **104** | **475** | 104/475 = **22%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Primärvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/73kE_uFW_vvT) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
|  | x |  | [Värmlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/EVVp_h6U_GSZ) |
|  | x |  | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| x |  |  | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
|  | x |  | [primärvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WvGv_6YE_mun) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
|  | x |  | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
|  | x |  | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| x |  |  | [anpassa sig efter ändrade förutsättningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gnWC_Nrw_yKE) |
| x | x | x | [Torsby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hQdb_zn9_Sok) |
| x | x | x | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
|  | x |  | [Barnhälsovård/BVC, **skill**](http://data.jobtechdev.se/taxonomy/concept/kXEj_s4x_ATw) |
| x | x | x | [Kil, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ocMw_Rz5_B1L) |
| x | x | x | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
|  | x |  | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
|  | x |  | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| x | x | x | [Hammarö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/x5qW_BXr_aut) |
| x | x | x | [Arvika, **municipality**](http://data.jobtechdev.se/taxonomy/concept/yGue_F32_wev) |
| | | **6** | 6/21 = **29%** |