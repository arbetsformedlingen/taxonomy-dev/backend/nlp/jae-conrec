# Results for '31e638dbf7771764b147d49accbe448ad5df1d80'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [31e638dbf7771764b147d49accbe448ad5df1d80](README.md) | 1 | 880 | 3 | 6 | 36/79 = **46%** | 2/4 = **50%** |

## Source text

Vilka yrken leder utbildningen till? Utbildningen ger dig grundkunskaper utifrån branschens krav för arbete som fordonslackerare.  Inom yrket arbetar man  mest med krockskadade och rostskadade bilar. Det som lagas kan vara plast- och plåtskador, rostskador, repor och andra skador. Noggrannhet, tålamod och sinne för färg och form är viktigt för den som vill bli fordonslackerare.  Hur går utbildningen till? Utbildningen varvar teori och praktiska övningar hos utbildningsleverantören och en del av utbildningen kommer du att göra på en arbetsplats där du lär dig på plats och får stöttning både från utbildningsleverantör och handledaren på företaget. Du har en egen utbildningsplan som bygger på just dina erfarenheter och vad du redan kan. Utbildningen är 40 timmar per vecka och går du hela utbildningen är den ca 37 veckor varav minst 4 veckor är på en arbetsplats.  

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ranschens krav för arbete som **fordonslackerare**.  Inom yrket arbetar man  me... |  | x |  | 16 | [Fordonslackerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XGS1_7V1_8EN) |
| 1 | ...ranschens krav för arbete som **fordonslackerare**.  Inom yrket arbetar man  me... | x | x | 16 | 16 | [fordonslackerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ajMm_iQe_MH9) |
| 2 | ...ador, repor och andra skador. **Noggrannhet**, tålamod och sinne för färg o... |  | x |  | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 3 | ...annhet, tålamod och sinne för **färg** och form är viktigt för den s... | x | x | 4 | 4 | [Färg/Kemikalieprodukter/Tapeter, **skill**](http://data.jobtechdev.se/taxonomy/concept/fyJi_1k3_CdV) |
| 4 | ... viktigt för den som vill bli **fordonslackerare**.  Hur går utbildningen till... |  | x |  | 16 | [Fordonslackerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XGS1_7V1_8EN) |
| 4 | ... viktigt för den som vill bli **fordonslackerare**.  Hur går utbildningen till... | x | x | 16 | 16 | [fordonslackerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ajMm_iQe_MH9) |
| | **Overall** | | | **36** | **79** | 36/79 = **46%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Fordonslackerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XGS1_7V1_8EN) |
| x | x | x | [fordonslackerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ajMm_iQe_MH9) |
| x | x | x | [Färg/Kemikalieprodukter/Tapeter, **skill**](http://data.jobtechdev.se/taxonomy/concept/fyJi_1k3_CdV) |
|  | x |  | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| | | **2** | 2/4 = **50%** |