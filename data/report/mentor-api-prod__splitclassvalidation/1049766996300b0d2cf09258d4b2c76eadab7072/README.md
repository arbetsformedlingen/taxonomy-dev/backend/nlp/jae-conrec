# Results for '1049766996300b0d2cf09258d4b2c76eadab7072'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1049766996300b0d2cf09258d4b2c76eadab7072](README.md) | 1 | 1104 | 7 | 4 | 28/119 = **24%** | 2/6 = **33%** |

## Source text

Sveriges största fiskgrossist söker nya medarbetare! Nu söker vi en kollega till vår kund som är en Sveriges största fiskgrossister, vi söker dig som arbetat inom fiskproduktion och har kunskap av att filea fisk. Om Kunden Vår kund är verksamma inom livsmedelsbranschen och producerar fisk & skaldjur. Det är ett stort företag som har funnits sen 1988 deras värdeord är kunskap, kvalitet och service. Produktionsanläggningen är belägen i Farsta och är i gång hela året runt. Om tjänsten Det är ett tungt och fysiskt jobbigt för kroppen så det gäller att vara tålig och ha uthållighet. Du kommer att arbeta på en enhet tillsammans med ditt team och arbetstiderna är: 05.00-14.00. Så det krävs att du är morgonpigg och inte har problem att ta dig till arbetsplatsen vid den tiden. Om dig Vi söker dig som har tidigare erfarenhet i fiskindustrin och är en fena på att filea fisk. Det krävs att du är väldigt noggrann, plikttrogen och positiv. Vi lägger stor vikt vid personlig lämplighet när vi rekryterar, för vi vet att rätt person på rätt plats skapar ett stort mervärde för både kund och dig som person.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...llega till vår kund som är en **Sveriges** största fiskgrossister, vi sö... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 2 | ...vi söker dig som arbetat inom **fiskproduktion** och har kunskap av att filea ... | x |  |  | 14 | [Fisk och skaldjur, **skill**](http://data.jobtechdev.se/taxonomy/concept/9uHp_GKQ_4qR) |
| 2 | ...vi söker dig som arbetat inom **fiskproduktion** och har kunskap av att filea ... |  | x |  | 14 | [rapportera skördad fiskproduktion, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gQNQ_hRf_XWP) |
| 3 | ...uktion och har kunskap av att **filea fisk**. Om Kunden Vår kund är verksa... | x | x | 10 | 10 | [filea fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/udfD_Q4b_VWz) |
| 4 | ...för kroppen så det gäller att **vara tålig och ha uthållighet**. Du kommer att arbeta på en e... | x |  |  | 29 | [vara uthållig, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XVSr_yhQ_BZu) |
| 5 | ...Vi söker dig som har tidigare **erfarenhet i fiskindustrin** och är en fena på att filea f... | x |  |  | 26 | [Fisk och skaldjur, **skill**](http://data.jobtechdev.se/taxonomy/concept/9uHp_GKQ_4qR) |
| 6 | ...dustrin och är en fena på att **filea fisk**. Det krävs att du är väldigt ... | x | x | 10 | 10 | [filea fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/udfD_Q4b_VWz) |
| 7 | .... Det krävs att du är väldigt **noggrann**, plikttrogen och positiv. Vi ... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| | **Overall** | | | **28** | **119** | 28/119 = **24%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Fisk och skaldjur, **skill**](http://data.jobtechdev.se/taxonomy/concept/9uHp_GKQ_4qR) |
| x |  |  | [vara uthållig, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XVSr_yhQ_BZu) |
|  | x |  | [rapportera skördad fiskproduktion, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gQNQ_hRf_XWP) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [filea fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/udfD_Q4b_VWz) |
| | | **2** | 2/6 = **33%** |