# Results for 'afc4723682481cbaa36b4745532d183bd193c813'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [afc4723682481cbaa36b4745532d183bd193c813](README.md) | 1 | 2312 | 22 | 23 | 138/295 = **47%** | 8/13 = **62%** |

## Source text

Bilmekaniker/-tekniker på Motor-Nilsson Motor-Nilsson rekryterar: Bilmekaniker/-tekniker   Vi söker fler Fordonstekniker  Motor-Nilsson i Köping stärker nu upp inför framtiden och söker tekniker/mekaniker. Bilbranschen står inför en häftig omställning och en ny tids mobilitet i både kundperspektiv och kundupplevelse, digitalisering fortskrider i hög takt, nya drivmedel med elektrifiering som fokus, nya sätt att äga sina bilar på, och vikten av enkelhet och stöttning i bil-ägandet.  Vi växer, utökar med fler nya varumärken och breddar vårt utbud och behöver stärka personalstyrkan i Köping. Är du erfaren bil- / lastbilsmekaniker eller är du nyutbildad och vill växa och utvecklas är du välkommen till oss på Motor-Nilsson.  Som fordonstekniker på Motor-Nilsson förstärker du serviceteamet och blir en viktig del i vår utveckling, fortsatta expansion och framtid. Som person är du noggrann, strukturerad och en lagspelare. Arbetet är mångsidigt med arbete såväl med nybilar som begagnade bilar och flera moment som hör bilen till.  Tjänsten är i Köping. Tjänsten är tillsvidare och 100%  Till din hjälp har du ett härligt och mycket strukturerat team i en välskött och fin bilanläggning. Du har stor möjlighet till utveckling och arbete med hög frihetskänsla. Motor-Nilsson ingår idag i en företagsgrupp med totalt 5 stora bilanläggningar i Örebro, Norrköping, Jönköping, Eskilstuna, Köping och representerar nybilsmärken Peugeot, DS, Citroen, Opel och Mitsubishi.   Är du den vi söker? För eventuella frågor kontakta Servicemarknadschef Victor Mandl på 0735-822978 eller mail victor.mandl@motornilsson.se  Ansök vi länken eller skicka ditt personliga brev och CV till victor.mandl@motornilsson.se  Du har kommit till ett bilföretag med anor i regionen sedan 1955.Företaget var då representant för SAAB. I början av 80-talet fanns företaget representerat i Köping. På 90-talet började företaget sälja Opel.  Vi är ett fullserviceföretag med serviceverkstad och reservdelsförsäljning för Opel och Fiat är auktoriserad reparatör för SAAB. Vi är numer auktoriserad Serviceverkstad för Peugeot, Citroen och Mitsubishi.  Sedan 2022 ingår Motor Nilsson i företagsgruppen Mercari Group med anläggningar i Örebro, Eskilstuna, Norrköping och Jönköping som representerar varumärken Peugeot, DS, Opel och Mitsubishi.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bilmekaniker**/-tekniker på Motor-Nilsson Mo... | x | x | 12 | 12 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 2 | Bilmekaniker/-**tekniker** på Motor-Nilsson Motor-Nilsso... | x |  |  | 8 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 3 | ...son Motor-Nilsson rekryterar: **Bilmekaniker**/-tekniker   Vi söker fler F... | x | x | 12 | 12 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 4 | ...son rekryterar: Bilmekaniker/-**tekniker**   Vi söker fler Fordonstekn... | x |  |  | 8 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 5 | ...r/-tekniker   Vi söker fler **Fordonstekniker**  Motor-Nilsson i Köping stärk... | x | x | 15 | 15 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 6 | ...donstekniker  Motor-Nilsson i **Köping** stärker nu upp inför framtide... | x | x | 6 | 6 | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| 7 | ...upp inför framtiden och söker **tekniker**/mekaniker. Bilbranschen står ... | x |  |  | 8 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 8 | ... framtiden och söker tekniker/**mekaniker**. Bilbranschen står inför en h... | x |  |  | 9 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 9 | ...över stärka personalstyrkan i **Köping**. Är du erfaren bil- / lastbil... | x | x | 6 | 6 | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| 10 | ...yrkan i Köping. Är du erfaren **bil-** / lastbilsmekaniker eller är ... | x |  |  | 4 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 11 | ... Köping. Är du erfaren bil- / **lastbilsmekaniker** eller är du nyutbildad och vi... | x | x | 17 | 17 | [Lastbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/o7LP_duC_u9Z) |
| 12 | ...ll oss på Motor-Nilsson.  Som **fordonstekniker** på Motor-Nilsson förstärker d... | x | x | 15 | 15 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 13 | ...och framtid. Som person är du **noggrann**, strukturerad och en lagspela... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 14 | ...ör bilen till.  Tjänsten är i **Köping**. Tjänsten är tillsvidare och ... | x | x | 6 | 6 | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| 15 | ...sten är i Köping. Tjänsten är **tillsvidare** och 100%  Till din hjälp har ... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 16 | .... Tjänsten är tillsvidare och **100%**  Till din hjälp har du ett hä... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 17 | ...alt 5 stora bilanläggningar i **Örebro**, Norrköping, Jönköping, Eskil... | x | x | 6 | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 18 | ...ora bilanläggningar i Örebro, **Norrköping**, Jönköping, Eskilstuna, Köpin... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 19 | ...bilanläggningar i Örebro, Norr**köping**, Jönköping, Eskilstuna, Köpin... |  | x |  | 6 | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| 20 | ...gningar i Örebro, Norrköping, **Jönköping**, Eskilstuna, Köping och repre... | x | x | 9 | 9 | [Jönköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KURg_KJF_Lwc) |
| 21 | ...rebro, Norrköping, Jönköping, **Eskilstuna**, Köping och representerar nyb... | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 22 | ...öping, Jönköping, Eskilstuna, **Köping** och representerar nybilsmärke... | x |  |  | 6 | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| 23 | ...ör eventuella frågor kontakta **Servicemarknadschef** Victor Mandl på 0735-822978 e... |  | x |  | 19 | [Servicemarknadschef, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ePv1_PHu_gQq) |
| 24 | ...nns företaget representerat i **Köping**. På 90-talet började företage... | x | x | 6 | 6 | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| 25 | ...är ett fullserviceföretag med **serviceverkstad** och reservdelsförsäljning för... |  | x |  | 15 | [ansvarig, serviceverkstad, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/in5o_v9x_oEN) |
| 26 | ...Opel och Fiat är auktoriserad **reparatör** för SAAB. Vi är numer auktori... |  | x |  | 9 | [Maskinreparatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/J4Bq_CAF_dEe) |
| 27 | ...AAB. Vi är numer auktoriserad **Serviceverkstad** för Peugeot, Citroen och Mits... |  | x |  | 15 | [ansvarig, serviceverkstad, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/in5o_v9x_oEN) |
| 28 | ...cari Group med anläggningar i **Örebro**, Eskilstuna, Norrköping och J... |  | x |  | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 29 | ...up med anläggningar i Örebro, **Eskilstuna**, Norrköping och Jönköping som... |  | x |  | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 30 | ...gningar i Örebro, Eskilstuna, **Norrköping** och Jönköping som representer... |  | x |  | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 31 | ...o, Eskilstuna, Norrköping och **Jönköping** som representerar varumärken ... |  | x |  | 9 | [Jönköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KURg_KJF_Lwc) |
| | **Overall** | | | **138** | **295** | 138/295 = **47%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Maskinreparatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/J4Bq_CAF_dEe) |
| x | x | x | [Jönköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KURg_KJF_Lwc) |
| x | x | x | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| x | x | x | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
|  | x |  | [Servicemarknadschef, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ePv1_PHu_gQq) |
|  | x |  | [ansvarig, serviceverkstad, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/in5o_v9x_oEN) |
| x | x | x | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Lastbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/o7LP_duC_u9Z) |
| | | **8** | 8/13 = **62%** |