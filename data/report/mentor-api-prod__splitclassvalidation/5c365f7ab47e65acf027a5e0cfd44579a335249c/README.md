# Results for '5c365f7ab47e65acf027a5e0cfd44579a335249c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [5c365f7ab47e65acf027a5e0cfd44579a335249c](README.md) | 1 | 857 | 26 | 19 | 116/270 = **43%** | 13/20 = **65%** |

## Source text

Steglo söker fler PHP-utvecklare, möjlighet till distansarbete! Systemutvecklare/webbutvecklare (LEMP), Kalmar/Norrköping eller distans! Steglo söker fler system-/webbutvecklare till ett uppdrag hos ett produktionsteam i Kalmar. Det finns möjlighet till distansarbete, men går fint om du bor i Norrköping. Vi söker dig som har goda kunskaper och intresse inom:  Objektorienterad webbutveckling i LEMP-miljön (Linux, Nginx, MySQL, PHP) HTML5, CSS, Javascript MVC Unit testing, CI Git  Meriterande kunskaper:  Jquery React Native Design och frontende-utveckling  Vi tror att:  Du har ett par års erfarenhet av system-webbutveckling. Mycket god kommunikationsförmåga i svenska och engelska Van att ta ansvar för dina uppgifter och kommunicera mot kund och har intresse för webbteknik (gärna media och design)  Tillsättning omgående och intervjuer sker löpande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Steglo söker fler **PHP**-utvecklare, möjlighet till di... | x |  |  | 3 | [PHP, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/e4tA_b22_rfY) |
| 2 | ...HP-utvecklare, möjlighet till **distansarbete**! Systemutvecklare/webbutveckl... | x | x | 13 | 13 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 3 | ...möjlighet till distansarbete! **Systemutvecklare**/webbutvecklare (LEMP), Kalmar... | x |  |  | 16 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 4 | ...stansarbete! Systemutvecklare/**webbutvecklare** (LEMP), Kalmar/Norrköping ell... |  | x |  | 14 | [webbutvecklare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/AkVP_op7_dSC) |
| 4 | ...stansarbete! Systemutvecklare/**webbutvecklare** (LEMP), Kalmar/Norrköping ell... | x |  |  | 14 | [Webbutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/URP5_iwe_oGE) |
| 5 | ...cklare/webbutvecklare (LEMP), **Kalmar**/Norrköping eller distans! Ste... | x | x | 6 | 6 | [Kalmar, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Pnmg_SgP_uHQ) |
| 6 | ...webbutvecklare (LEMP), Kalmar/**Norrköping** eller distans! Steglo söker f... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 7 | ...EMP), Kalmar/Norrköping eller **distans**! Steglo söker fler system-/we... | x |  |  | 7 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 8 | ...er distans! Steglo söker fler **system**-/webbutvecklare till ett uppd... | x |  |  | 6 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 9 | ...ns! Steglo söker fler system-/**webbutvecklare** till ett uppdrag hos ett prod... |  | x |  | 14 | [webbutvecklare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/AkVP_op7_dSC) |
| 9 | ...ns! Steglo söker fler system-/**webbutvecklare** till ett uppdrag hos ett prod... | x |  |  | 14 | [Webbutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/URP5_iwe_oGE) |
| 10 | ...rag hos ett produktionsteam i **Kalmar**. Det finns möjlighet till dis... | x | x | 6 | 6 | [Kalmar, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Pnmg_SgP_uHQ) |
| 11 | ...mar. Det finns möjlighet till **distansarbete**, men går fint om du bor i Nor... | x | x | 13 | 13 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 12 | ...ete, men går fint om du bor i **Norrköping**. Vi söker dig som har goda ku... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 13 | ...resse inom:  Objektorienterad **webbutveckling** i LEMP-miljön (Linux, Nginx, ... | x |  |  | 14 | [Webbutveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/t4sa_rKY_zru) |
| 14 | ...webbutveckling i LEMP-miljön (**Linux**, Nginx, MySQL, PHP) HTML5, CS... | x | x | 5 | 5 | [Linux, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/z52H_f4y_4ZA) |
| 15 | ... i LEMP-miljön (Linux, Nginx, **MySQL**, PHP) HTML5, CSS, Javascript ... | x | x | 5 | 5 | [MySQL, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eF8Z_oLZ_mgy) |
| 16 | ...-miljön (Linux, Nginx, MySQL, **PHP**) HTML5, CSS, Javascript MVC U... | x | x | 3 | 3 | [PHP, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/e4tA_b22_rfY) |
| 17 | ...ux, Nginx, MySQL, PHP) HTML5, **CSS**, Javascript MVC Unit testing,... | x | x | 3 | 3 | [CSS, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/g5MZ_uTz_89a) |
| 18 | ...ginx, MySQL, PHP) HTML5, CSS, **Javascript** MVC Unit testing, CI Git  Mer... | x | x | 10 | 10 | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
| 19 | ...P) HTML5, CSS, Javascript MVC **Unit testing**, CI Git  Meriterande kunskape... |  | x |  | 12 | [Unit tester, metod/systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/LuMk_suh_iyw) |
| 20 | ... Javascript MVC Unit testing, **CI** Git  Meriterande kunskaper:  ... | x | x | 2 | 2 | [Continuous integration/CI, **skill**](http://data.jobtechdev.se/taxonomy/concept/xT8A_n4a_Zji) |
| 21 | ...vascript MVC Unit testing, CI **Git**  Meriterande kunskaper:  Jque... | x | x | 3 | 3 | [Git, versionshanteringssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/2REh_Drn_nsh) |
| 22 | ...eriterande kunskaper:  Jquery **React Native** Design och frontende-utveckli... | x | x | 12 | 12 | [React native, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/TdnY_1gB_9pZ) |
| 23 | ...query React Native Design och **frontende-utveckling**  Vi tror att:  Du har ett par... | x |  |  | 20 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 24 | ...har ett par års erfarenhet av **system**-webbutveckling. Mycket god ko... | x |  |  | 6 | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| 25 | ... par års erfarenhet av system-**webbutveckling**. Mycket god kommunikationsför... | x |  |  | 14 | [Webbutveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/t4sa_rKY_zru) |
| 26 | ...t god kommunikationsförmåga i **svenska** och engelska Van att ta ansva... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 27 | ...ikationsförmåga i svenska och **engelska** Van att ta ansvar för dina up... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| | **Overall** | | | **116** | **270** | 116/270 = **43%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Git, versionshanteringssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/2REh_Drn_nsh) |
|  | x |  | [webbutvecklare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/AkVP_op7_dSC) |
| x |  |  | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| x | x | x | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
| x |  |  | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
|  | x |  | [Unit tester, metod/systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/LuMk_suh_iyw) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Kalmar, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Pnmg_SgP_uHQ) |
| x | x | x | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| x | x | x | [React native, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/TdnY_1gB_9pZ) |
| x |  |  | [Webbutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/URP5_iwe_oGE) |
| x | x | x | [PHP, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/e4tA_b22_rfY) |
| x | x | x | [MySQL, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eF8Z_oLZ_mgy) |
| x |  |  | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| x | x | x | [CSS, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/g5MZ_uTz_89a) |
| x |  |  | [Webbutveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/t4sa_rKY_zru) |
| x | x | x | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| x | x | x | [Continuous integration/CI, **skill**](http://data.jobtechdev.se/taxonomy/concept/xT8A_n4a_Zji) |
| x | x | x | [Linux, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/z52H_f4y_4ZA) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **13** | 13/20 = **65%** |