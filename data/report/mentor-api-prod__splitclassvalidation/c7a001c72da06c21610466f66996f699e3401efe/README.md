# Results for 'c7a001c72da06c21610466f66996f699e3401efe'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c7a001c72da06c21610466f66996f699e3401efe](README.md) | 1 | 2160 | 21 | 15 | 126/264 = **48%** | 5/11 = **45%** |

## Source text

Teckenspråks och dövblindtolk VI söker en tolk som vill jobba i Stockholm i vårt härliga team.  Allsign Consulting erbjuder tolk till olika uppdragsgivare i hela landet men främst i Stockholm. Vi jobbar tillsammans i team för att stärka varandra och utvecklas tillsammans.   Som medarbetare hos oss får du arbeta för allas lika värde i ett företag där delaktighet och engagerade medarbetare värderas högt. Vi värderar att alla mår bra så friskvård och nära samarbete är viktigt hos oss. Vi jobbar flexibelt med att hitta det mest optimala för både anställda och företaget.   Vi jobbar både på plats och distans, där vi försöker få en blandning av uppdrag.  Du har nu möjlighet att få en tjänst hos oss som tolk på en arbetsplats som erbjuder glädje, gemenskap och stabilitet. Vi söker en till två teckenspråkstolkar med placering i Stockholm.  ARBETSUPPGIFTER I arbetet som teckenspråkstolk och dövblindtolk i Stockholm ingår tolkning för olika uppdragsgivare.   Exempel på uppdrag kan vara möten, läkarbesök, APT, olika utbildningar men även större evenemang. Våra tolkar utgår hemifrån men vi har kontor i UPPLANDSVÄSBY. KVALIFIKATIONER Vi söker dig som har: Tolk- och översättarinstitutets föreskrivna tolkutbildning med godkänt resultat.  Minst två års erfarenhet i närtid av arbete som teckenspråkstolk.  Utbildning och erfarenhet som dövblindtolk Det är meriterande om du har  Erfarenhet av tolkning på distans via digitala plattformar som tex Zoom. Erfarenhet av tolkning på engelska.  Personliga egenskaper  Vi önskar att du är trygg i din yrkesroll och vågar ta plats på ett respektfullt sätt. Vi lägger också stor vikt vid att du är serviceinriktad, lyhörd för företagets behov, initiativrik och lösningsfokuserad samt har god förmåga att skapa goda samarbetsrelationer.  Du kan behålla ett lugnt förhållningssätt även i stressiga situationer samt tycker om att jobba både självständigt och i team.  ÖVRIGT Utdrag från belastningsregistret ska visas upp inför anställning. Inför rekryteringsarbetet har vi tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss denna gång alla erbjudanden om annonserings- och rekryteringshjälp.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Teckenspråks och **dövblindtolk VI söker en tolk ... | x |  |  | 17 | [Teckenspråkstolk/Dövblindtolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aPeE_Gxs_cWJ) |
| 2 | Teckenspråks och **dövblindtolk** VI söker en tolk som vill job... | x | x | 12 | 12 | [Teckenspråkstolk/Dövblindtolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aPeE_Gxs_cWJ) |
| 3 | ... och dövblindtolk VI söker en **tolk** som vill jobba i Stockholm i ... | x | x | 4 | 4 | [Tolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vaEY_R9R_LjB) |
| 4 | ...öker en tolk som vill jobba i **Stockholm** i vårt härliga team.  Allsign... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 5 | ...  Allsign Consulting erbjuder **tolk** till olika uppdragsgivare i h... | x | x | 4 | 4 | [Tolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vaEY_R9R_LjB) |
| 6 | ...re i hela landet men främst i **Stockholm**. Vi jobbar tillsammans i team... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 7 | ... värderar att alla mår bra så **friskvård** och nära samarbete är viktigt... | x |  |  | 9 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 8 | ...  Vi jobbar både på plats och **distans**, där vi försöker få en blandn... | x |  |  | 7 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 9 | ... att få en tjänst hos oss som **tolk** på en arbetsplats som erbjude... | x | x | 4 | 4 | [Tolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vaEY_R9R_LjB) |
| 10 | ...bilitet. Vi söker en till två **teckenspråkstolkar** med placering i Stockholm.  A... | x | x | 18 | 18 | [teckenspråkstolk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/jHsd_wAS_aAT) |
| 11 | ...nspråkstolkar med placering i **Stockholm**.  ARBETSUPPGIFTER I arbetet s... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 12 | ...ARBETSUPPGIFTER I arbetet som **teckenspråkstolk** och dövblindtolk i Stockholm ... |  | x |  | 16 | [teckenspråkstolk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/jHsd_wAS_aAT) |
| 13 | ...ARBETSUPPGIFTER I arbetet som **teckenspråkstolk och **dövblindtolk i Stockholm ingår... | x |  |  | 21 | [Teckenspråkstolk/Dövblindtolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aPeE_Gxs_cWJ) |
| 14 | ...etet som teckenspråkstolk och **dövblindtolk** i Stockholm ingår tolkning fö... | x | x | 12 | 12 | [Teckenspråkstolk/Dövblindtolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aPeE_Gxs_cWJ) |
| 15 | ...språkstolk och dövblindtolk i **Stockholm** ingår tolkning för olika uppd... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 16 | ...n även större evenemang. Våra **tolkar** utgår hemifrån men vi har kon... |  | x |  | 6 | [Tolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vaEY_R9R_LjB) |
| 17 | ...kar utgår hemifrån men vi har **kontor** i UPPLANDSVÄSBY. KVALIFIKATIO... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 18 | ... hemifrån men vi har kontor i **UPPLANDSVÄSBY**. KVALIFIKATIONER Vi söker dig... | x |  |  | 13 | [Upplands Väsby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/XWKY_c49_5nv) |
| 19 | ... med godkänt resultat.  Minst **två års erfarenhet** i närtid av arbete som tecken... | x |  |  | 18 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 20 | ...renhet i närtid av arbete som **teckenspråkstolk**.  Utbildning och erfarenhet s... | x | x | 16 | 16 | [teckenspråkstolk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/jHsd_wAS_aAT) |
| 21 | ...Utbildning och erfarenhet som **dövblindtolk** Det är meriterande om du har ... | x | x | 12 | 12 | [Teckenspråkstolk/Dövblindtolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aPeE_Gxs_cWJ) |
| 22 | ...ar  Erfarenhet av tolkning på **distans** via digitala plattformar som ... | x |  |  | 7 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 23 | ...om. Erfarenhet av tolkning på **engelska**.  Personliga egenskaper  Vi ö... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 24 | ...ituationer samt tycker om att **jobba** både självständigt och i team... | x |  |  | 5 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 25 | ...samt tycker om att jobba både **självständigt** och i team.  ÖVRIGT Utdrag fr... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| | **Overall** | | | **126** | **264** | 126/264 = **48%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| x |  |  | [Upplands Väsby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/XWKY_c49_5nv) |
| x | x | x | [Teckenspråkstolk/Dövblindtolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aPeE_Gxs_cWJ) |
| x | x | x | [teckenspråkstolk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/jHsd_wAS_aAT) |
| x |  |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| x | x | x | [Tolk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vaEY_R9R_LjB) |
| x |  |  | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| | | **5** | 5/11 = **45%** |