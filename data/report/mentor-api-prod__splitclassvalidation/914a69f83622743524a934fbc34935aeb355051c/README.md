# Results for '914a69f83622743524a934fbc34935aeb355051c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [914a69f83622743524a934fbc34935aeb355051c](README.md) | 1 | 4881 | 18 | 21 | 157/336 = **47%** | 8/16 = **50%** |

## Source text

Sjuksköterska/Kontaktsjuksköterska till Medicinskt Centrum Capio S:t Görans Sjukhus ingår i en av Europas ledande vårdkoncerner, Ramsay Santé, och är det första och enda privat drivna akutsjukhuset i Sverige. Sjukhuset är centralt beläget på Kungsholmen i Stockholm och har cirka 2 300 anställda, 340 vårdplatser och tar emot drygt 290 000 besök om året.     Vår vision är att driva framtidens sjukvård, att ge bättre vård genom att lära, utmana och förnya. Våra värdeord och värderingar är:      - Kvalitet: vi vill alltid bli bättre,   - Samhällsansvar: tillsammans tar vi ansvar för en hållbar framtid.    - Innovation: Vi är nyfikna och modiga,   - Empowerment: Du är viktig, din åsikt gör skillnad.     Vi erbjuder dig en attraktiv poänglönemodell, S:t Göranmodellen, och en mängd förmåner via vår förmånsportal Benify, bland annat friskvårdsbidrag på 3 000 kr/år och tillgång till ett gratis gym. Vi kan även erbjuda nybyggda personalbostäder nära sjukhuset.    Beskrivning Vi erbjuder en stor, trevlig och utvecklande arbetsplats med gott samarbete inom alla professioner och flöden!  Medicinskt Centrum ingår i Medicinkliniken, som är den största kliniken inom Capio S:t Görans sjukhus.  Vi arbetar i flera arbetsgrupper och flöden hos oss; endokrinologi och diabetes, tromboembolism, hematologi och gastroenterologi samt även i viss mån lungmedicin.  Patienten kommer på remiss till vår mottagning för läkarbesök, sjuksköterskebesök och för behandlingar på vår behandlingsavdelning där patienten stannar över dagen.  Till hösten kommer ett projekt påbörjas med skriven projektplan kring hur vår nuvarande behandlingsavdelning och dagvård kan utökas, och bli ännu bättre för våra patienter och medarbetare.  Professionerna kommer tillsammans se över och ta fram nya arbetssätt för ännu tryggare, kvalitetssäker vård tillsammans med våra patienter.  Vill Du vara med och nyfiken att driva fram modernare arbetssätt, med och för våra patienter – för oss är du viktig och vi vill gärna att Du gör det tillsammans med oss!  Arbetsuppgifter Vi söker sjuksköterska alternativt kontaktsjuksköterska till vår behandlingsavdelning på Medicinskt Centrum.  På behandlingsavdelningen kommer patienter som behandlas för hematologiska och gastroenterologiska sjukdomar som kräver en vårdplats över dagen, och som därefter kan återvända hem.  I arbetsuppgifterna ingår planering, bokning och administrering av behandlingar tillsammans med gastroenterologer och hematologer. I huvudsak utförs behandlingar med cytostatika, transfusioner och parenteral adminis­trering av biologiska läkemedel.  Som kontaktsjuksköterska deltar du tillsammans med läkare vid diagnosbesked i enlighet med den nationella cancerstrategin, vilket innebär att alla patienter med cancer erbjudas en fast kontaktperson vid den cancervårdande kliniken.  I det dagliga arbetet innefattar bland annat även telefonrådgivning och hantering av ärenden i 1177.  Arbetstiderna är dagtid, måndag – fredag.  Kvalifikationer Du är legitimerad sjuksköterska med minst 3 års erfarenhet.  Erfarenhet av arbete inom hematologi och gastroenterologi är meriterande liksom även om du är utbildad kontaktsjuksköterska och har cytostatikakörkort.  Du har vilja att arbeta på ett strukturerat, standardiserat och flödesinriktat arbetssätt där patienten sätts i fokus i en god arbetsmiljö och du ser samarbete som en självklar del i arbetet.  Arbetssättet är flexibelt och verksamhetsanpassat. För oss är det viktigt att du känner att dina värderingar stämmer överens med Capio S:t Görans grundläggande värderingar.  Vi gör urval och intervjuar löpande vilket innebär att tjänsten kan komma att tillsättas innan sista ansökningsdag.  Välkommen med din ansökan!    Villkor Heltid 100% tillsvidare, tillträde 220815  Capio – Nordens största privata vårdgivare Capios vision är att förbättra hälsan för människor varje dag. Vi vill göra skillnad för våra patienter genom att se, lyssna och finnas här – idag och genom hela livet. Våra medarbetare är viktiga för oss och vi vill ge dem kraften att finnas där för våra patienter men också för att utveckla vården i stort. I vårt dagliga arbete jobbar vi utifrån våra värderingar: Quality, Empowerment, Innovation och Social responsibility. De hjälper oss att uppfylla vårt patientlöfte om att vara en pålitlig partner – närvarande och nytänkande. Tillsammans förbättrar vi folkhälsan – en individ i taget!   Capio erbjuder ett brett och högkvalitativt vårdutbud genom våra sjukhus, specialistkliniker och vårdcentraler i Sverige, Norge och Danmark. Våra patienter kan även få hjälp via våra digitala tjänster. Capio är sedan 2018 en del av Ramsay Santé - en ledande vårdgivare med 36 000 medarbetare i sex länder. För mer information, besök https://capio.se/.   Vi undanber oss vänligt men bestämt all direktkontakt med bemannings- och rekryteringsföretag samt försäljare av jobbannonser.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sjuksköterska**/Kontaktsjuksköterska till Med... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 1 | **Sjuksköterska**/Kontaktsjuksköterska till Med... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 2 | Sjuksköterska/**Kontaktsjuksköterska** till Medicinskt Centrum Capio... | x | x | 20 | 20 | [Kontaktsjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/P255_NB8_Aa7) |
| 3 | ...privat drivna akutsjukhuset i **Sverige**. Sjukhuset är centralt beläge... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ...ralt beläget på Kungsholmen i **Stockholm** och har cirka 2 300 anställda... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 5 | ...tsgrupper och flöden hos oss; **endokrinologi** och diabetes, tromboembolism,... | x | x | 13 | 13 | [Endokrinologi och diabetologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/wcGF_ouA_knh) |
| 6 | ...en hos oss; endokrinologi och **diabetes**, tromboembolism, hematologi o... | x | x | 8 | 8 | [Diabetesvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/GkJU_4dd_Cit) |
| 7 | ...och diabetes, tromboembolism, **hematologi** och gastroenterologi samt äve... | x | x | 10 | 10 | [Hematologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/y9uS_oYh_4Kr) |
| 8 | ...romboembolism, hematologi och **gastroenterologi** samt även i viss mån lungmedi... |  | x |  | 16 | [gastroenterologisk kirurgi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rD2P_TpU_Sry) |
| 9 | ...ten kommer på remiss till vår **mottagning** för läkarbesök, sjuksköterske... | x | x | 10 | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 10 | ...ss!  Arbetsuppgifter Vi söker **sjuksköterska** alternativt kontaktsjuksköter... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 10 | ...ss!  Arbetsuppgifter Vi söker **sjuksköterska** alternativt kontaktsjuksköter... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 11 | ...ker sjuksköterska alternativt **kontaktsjuksköterska** till vår behandlingsavdelning... | x | x | 20 | 20 | [Kontaktsjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/P255_NB8_Aa7) |
| 12 | ...r patienter som behandlas för **hematologiska** och gastroenterologiska sjukd... | x |  |  | 13 | [Hematologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/y9uS_oYh_4Kr) |
| 13 | ...ans med gastroenterologer och **hematologer**. I huvudsak utförs behandling... |  | x |  | 11 | [Hematolog, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Pq4V_e45_VCG) |
| 14 | ...adminis­trering av biologiska **läkemedel**.  Som kontaktsjuksköterska de... | x | x | 9 | 9 | [Läkemedel/Sjukvårdsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/vVXb_pYT_E1z) |
| 15 | ...av biologiska läkemedel.  Som **kontaktsjuksköterska** deltar du tillsammans med läk... | x | x | 20 | 20 | [Kontaktsjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/P255_NB8_Aa7) |
| 16 | ...ska deltar du tillsammans med **läkare** vid diagnosbesked i enlighet ... | x | x | 6 | 6 | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| 17 | ...ifikationer Du är legitimerad **sjuksköterska** med minst 3 års erfarenhet.  ... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 17 | ...ifikationer Du är legitimerad **sjuksköterska** med minst 3 års erfarenhet.  ... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 18 | ...t.  Erfarenhet av arbete inom **hematologi** och gastroenterologi är merit... | x | x | 10 | 10 | [Hematologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/y9uS_oYh_4Kr) |
| 19 | ...av arbete inom hematologi och **gastroenterologi** är meriterande liksom även om... |  | x |  | 16 | [gastroenterologisk kirurgi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rD2P_TpU_Sry) |
| 20 | ...liksom även om du är utbildad **kontaktsjuksköterska** och har cytostatikakörkort.  ... | x | x | 20 | 20 | [Kontaktsjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/P255_NB8_Aa7) |
| 21 | ...ienten sätts i fokus i en god **arbetsmiljö** och du ser samarbete som en s... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 22 | ...n med din ansökan!    Villkor **Heltid 100%** tillsvidare, tillträde 220815... | x |  |  | 11 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 23 | ...sökan!    Villkor Heltid 100% **tillsvidare**, tillträde 220815  Capio – No... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 24 | ...tkliniker och vårdcentraler i **Sverige**, Norge och Danmark. Våra pati... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **157** | **336** | 157/336 = **47%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Diabetesvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/GkJU_4dd_Cit) |
| x | x | x | [Kontaktsjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/P255_NB8_Aa7) |
|  | x |  | [Hematolog, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Pq4V_e45_VCG) |
| x | x | x | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [gastroenterologisk kirurgi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rD2P_TpU_Sry) |
| x | x | x | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| x | x | x | [Läkemedel/Sjukvårdsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/vVXb_pYT_E1z) |
| x | x | x | [Endokrinologi och diabetologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/wcGF_ouA_knh) |
| x | x | x | [Hematologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/y9uS_oYh_4Kr) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **8** | 8/16 = **50%** |