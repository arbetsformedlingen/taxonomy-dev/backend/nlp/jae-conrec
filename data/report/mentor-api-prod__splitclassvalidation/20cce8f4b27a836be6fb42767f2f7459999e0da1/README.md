# Results for '20cce8f4b27a836be6fb42767f2f7459999e0da1'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [20cce8f4b27a836be6fb42767f2f7459999e0da1](README.md) | 1 | 628 | 7 | 4 | 42/68 = **62%** | 4/7 = **57%** |

## Source text

Mobilkranförare A-LYFT AB söker 1 förare till Liebherr mobil tornkran / mobilkran. Vi är ett välmående mobilkranföretag med fräsch maskinpark. Vi har hög beläggning och kommande investeringar. Vårt rykte är gott och vi har en ljus framtid. Trivsel och teamanda är viktigt för oss, det finner du här. Du hittar oss i moderna lokaler i Mölndal. Du är: -Självständig och ansvarsfull -Lätt att sammarbeta med -Ordningsam, var sak har sin plats -Flexibel med arbetstider då dagen snabbt kan förändras, tidiga morgnar och sena kvällar förekommer. Kranförarbevis är meriterande. Hittar vi rätt person med C-kort står vi för utbildning.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Mobilkranförare** A-LYFT AB söker 1 förare till... | x | x | 15 | 15 | [Mobilkranförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8c4V_VDM_K7q) |
| 2 | ... 1 förare till Liebherr mobil **tornkran** / mobilkran. Vi är ett välmåe... | x |  |  | 8 | [Tornkranförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z1K2_Ens_9zu) |
| 3 | ...ill Liebherr mobil tornkran / **mobilkran**. Vi är ett välmående mobilkra... | x | x | 9 | 9 | [Mobilkran, **skill**](http://data.jobtechdev.se/taxonomy/concept/w2ch_nk6_kqc) |
| 4 | ...ittar oss i moderna lokaler i **Mölndal**. Du är: -Självständig och ans... | x | x | 7 | 7 | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
| 5 | ...na lokaler i Mölndal. Du är: -**Självständig** och ansvarsfull -Lätt att sam... | x |  |  | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 6 | ...dal. Du är: -Självständig och **ansvarsfull** -Lätt att sammarbeta med -Ord... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 7 | ...de. Hittar vi rätt person med **C-kort** står vi för utbildning. | x |  |  | 6 | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
| | **Overall** | | | **42** | **68** | 42/68 = **62%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Mobilkranförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8c4V_VDM_K7q) |
| x |  |  | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
| x | x | x | [Mobilkran, **skill**](http://data.jobtechdev.se/taxonomy/concept/w2ch_nk6_kqc) |
| x |  |  | [Tornkranförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z1K2_Ens_9zu) |
| | | **4** | 4/7 = **57%** |