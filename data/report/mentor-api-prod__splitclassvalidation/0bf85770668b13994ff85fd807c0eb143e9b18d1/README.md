# Results for '0bf85770668b13994ff85fd807c0eb143e9b18d1'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0bf85770668b13994ff85fd807c0eb143e9b18d1](README.md) | 1 | 1067 | 12 | 14 | 45/173 = **26%** | 3/10 = **30%** |

## Source text

Cafémedarbetare Henrys Bageri grundades 1965 i Lidköping. Verksamheten har utökas under åren med café och conditori. Hösten 2012 flyttade företaget till helt nya lokaler i Lidköping. Bageriet har haft som tradition att alltid använda de bästa råvarorna och jobbar så än idag. Henrys bageri har ett högklassigt sortiment av stenugnsbakat bröd, kaffebröd, tårtor, bakelser och mycket annat gott från sitt eget bageri Lidköping. I april 2016 öppnade Henrys bageri en caféservering i helt nyrenoverade lokaler vid Hertig Johans torg i Skövde. Caféserveringen har blivit populär. Vi vill förstärka vårt team med en: Cafémedarbetare Vi söker dig som har erfarenhet och trivs att jobba med hög servicekänsla. Du sätter alltid kunderna i första hand och har lätt för att samarbeta med kollegor samt gillar att hålla rent och snyggt omkring dig. Vi kommer att lägga stor vikt vid personlig lämplighet och där vi vill ha lagspelare som tar fullt ansvar för kvalitet, service och resultat. Passar denna beskrivning in på dig så är du välkommen att söka tjänsten. En tjänst 100 %

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Cafémedarbetare** Henrys Bageri grundades 1965 ... |  | x |  | 15 | [Cafeteriapersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Pmxj_WR6_Ka3) |
| 1 | **Cafémedarbetare** Henrys Bageri grundades 1965 ... | x |  |  | 15 | [Cafébiträde, **job-title**](http://data.jobtechdev.se/taxonomy/concept/qZG4_pVE_ZYZ) |
| 2 | Cafémedarbetare Henrys **Bageri** grundades 1965 i Lidköping. V... |  | x |  | 6 | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| 3 | ...enrys Bageri grundades 1965 i **Lidköping**. Verksamheten har utökas unde... | x | x | 9 | 9 | [Lidköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/FN1Y_asc_D8y) |
| 4 | ...ten har utökas under åren med **café** och conditori. Hösten 2012 fl... | x | x | 4 | 4 | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| 5 | ...tökas under åren med café och **conditori**. Hösten 2012 flyttade företag... |  | x |  | 9 | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| 6 | ...taget till helt nya lokaler i **Lidköping**. Bageriet har haft som tradit... | x | x | 9 | 9 | [Lidköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/FN1Y_asc_D8y) |
| 7 | ...och jobbar så än idag. Henrys **bageri** har ett högklassigt sortiment... |  | x |  | 6 | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| 8 | ...tenugnsbakat bröd, kaffebröd, **tårtor**, bakelser och mycket annat go... | x | x | 6 | 6 | [Tårtor/Bakelser, **skill**](http://data.jobtechdev.se/taxonomy/concept/UVj4_qZp_iqS) |
| 9 | ...akat bröd, kaffebröd, tårtor, **bakelser** och mycket annat gott från si... | x | x | 8 | 8 | [Tårtor/Bakelser, **skill**](http://data.jobtechdev.se/taxonomy/concept/UVj4_qZp_iqS) |
| 10 | ...at gott från sitt eget bageri **Lidköping**. I april 2016 öppnade Henrys ... | x | x | 9 | 9 | [Lidköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/FN1Y_asc_D8y) |
| 11 | .... I april 2016 öppnade Henrys **bageri** en caféservering i helt nyren... |  | x |  | 6 | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| 12 | ...2016 öppnade Henrys bageri en **café**servering i helt nyrenoverade ... | x |  |  | 4 | [Café, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sv3s_HEf_rbC) |
| 13 | ...aler vid Hertig Johans torg i **Skövde**. Caféserveringen har blivit p... |  | x |  | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 14 | ... Hertig Johans torg i Skövde. **Café**serveringen har blivit populär... | x |  |  | 4 | [Café, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sv3s_HEf_rbC) |
| 15 | ...l förstärka vårt team med en: **Cafémedarbetare** Vi söker dig som har erfarenh... |  | x |  | 15 | [Cafeteriapersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Pmxj_WR6_Ka3) |
| 15 | ...l förstärka vårt team med en: **Cafémedarbetare** Vi söker dig som har erfarenh... | x |  |  | 15 | [Cafébiträde, **job-title**](http://data.jobtechdev.se/taxonomy/concept/qZG4_pVE_ZYZ) |
| 16 | ...sta hand och har lätt för att **samarbeta med kollegor** samt gillar att hålla rent oc... | x |  |  | 22 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 17 | ... att söka tjänsten. En tjänst **100 %** | x |  |  | 5 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **45** | **173** | 45/173 = **26%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Lidköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/FN1Y_asc_D8y) |
|  | x |  | [Cafeteriapersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Pmxj_WR6_Ka3) |
| x |  |  | [Café, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sv3s_HEf_rbC) |
| x | x | x | [Tårtor/Bakelser, **skill**](http://data.jobtechdev.se/taxonomy/concept/UVj4_qZp_iqS) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
|  | x |  | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| x |  |  | [Cafébiträde, **job-title**](http://data.jobtechdev.se/taxonomy/concept/qZG4_pVE_ZYZ) |
| | | **3** | 3/10 = **30%** |