# Results for 'a0d6b383fb834ed5469b29ccb22b1ac0aea0e94c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [a0d6b383fb834ed5469b29ccb22b1ac0aea0e94c](README.md) | 1 | 2372 | 19 | 12 | 129/210 = **61%** | 6/13 = **46%** |

## Source text

Testledare Vi söker en Testledare med erfarenhet av testledning av automatiserade tester, integrationstester, prestandatester, systemtester och acceptanstester. Som testledare har du ett helhetsansvar för att ta fram krav för testning, du leder och ansvarar för testarbetet, tar fram testplaner och testfall, utvärderar testresultat, leder personal operativt samt styr och leder testarbetet i samverkan med leverantörer.  Uppdraget innebär att planera, koordinera, genomföra och följa upp tester, utöver detta ansvarar du för framtagande av en ny teststrategi med fungerande testprocesser som möjliggör nya testmöjligheter.  Som person präglas ditt arbetssätt av struktur samt ordning och reda. Du har en stor initiativförmåga och personligt driv. Du är trygg med att fatta beslut i vardagen och kan tydligt kommunicera dessa till berörda intressenter.  För rollen krävs att du har erfarenhet av ha arbetat med att ta fram krav för testning, lett och ansvarat för testarbetet, även operativ ledning av personal.Du har erfarenhet av att infört eller utvecklat testverktyg. Samt erfarenhet av testledning av automatiserade tester, integrationstester, prestandatester, systemtester och acceptanstester.God samarbetsförmåga och förmåga att skapa goda relationer både inom och utanför teamet krävs  Din ansökan  Låter rollen intressant och passande? Ansök i så fall omgående för vi intervjuar löpande och rollen kan tillsättas innan sista ansökningsdatum.   Vi är flera rekryterare som hanterar rekryteringen av denna tjänst, vid frågor gällande den här rekryteringen eller tjänsten når du oss enklast på e-post info@quest.se   Vi kan enbart ta emot och bearbeta din ansökan genom att du registrerar ditt CV i vår portal. Med avseende på GDPR kan vi ej ta emot ansökningar via e-post.   Varmt välkommen med din ansökan!   Uppdraget är en del av Quest Consulting personaluthyrning. Du kommer att vara anställd av Quest Consulting och arbeta som konsult hos vår kund.  Om Quest Consulting  Quest Consulting är ett auktoriserat konsultbolag med kollektivavtal, försäkringar, friskvård och tjänstepension. Vi är specialiserade inom IT, Teknik, HR, Administration och Ekonomi. Vår målsättning är att vara din personliga samarbetspartner och just därför är det så viktigt för oss att arbeta efter våra kärnvärden där våra ledord är att vara Personliga, Nyskapande och Professionella.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Testledare** Vi söker en Testledare med er... | x | x | 10 | 10 | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| 2 | Testledare Vi söker en **Testledare** med erfarenhet av testledning... | x | x | 10 | 10 | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| 3 | ... Testledare med erfarenhet av **testledning** av automatiserade tester, int... | x | x | 11 | 11 | [Testledning, datasystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/dexT_Znd_Yrs) |
| 4 | ...e tester, integrationstester, **prestandatester**, systemtester och acceptanste... | x |  |  | 15 | [utföra prestandatester, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eYuM_Gc7_VpT) |
| 5 | ...tionstester, prestandatester, **systemtester** och acceptanstester. Som test... | x | x | 12 | 12 | [Systemtestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/xAbu_uKA_Jeu) |
| 6 | ...tandatester, systemtester och **acceptanstester**. Som testledare har du ett he... | x | x | 15 | 15 | [Acceptanstestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/jYaA_Cwa_Yyf) |
| 7 | ...ster och acceptanstester. Som **testledare** har du ett helhetsansvar för ... | x | x | 10 | 10 | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| 8 | ...svar för att ta fram krav för **testning**, du leder och ansvarar för te... | x |  |  | 8 | [Testning, mjukvara/Verifiering, mjukvara, **skill**](http://data.jobtechdev.se/taxonomy/concept/2kEw_jWL_6Zz) |
| 9 | ...tvärderar testresultat, leder **personal** operativt samt styr och leder... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 10 | ...nsvarat för testarbetet, även **operativ ledning** av personal.Du har erfarenhet... | x | x | 16 | 16 | [Operativ ledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aB7R_JQN_sDE) |
| 11 | ...tet, även operativ ledning av **personal**.Du har erfarenhet av att infö... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 12 | ...stverktyg. Samt erfarenhet av **testledning** av automatiserade tester, int... | x | x | 11 | 11 | [Testledning, datasystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/dexT_Znd_Yrs) |
| 13 | ...e tester, integrationstester, **prestandatester**, systemtester och acceptanste... | x |  |  | 15 | [utföra prestandatester, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eYuM_Gc7_VpT) |
| 14 | ...tionstester, prestandatester, **systemtester** och acceptanstester.God samar... | x | x | 12 | 12 | [Systemtestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/xAbu_uKA_Jeu) |
| 15 | ...tandatester, systemtester och **acceptanstester**.God samarbetsförmåga och förm... | x | x | 15 | 15 | [Acceptanstestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/jYaA_Cwa_Yyf) |
| 16 | ...auktoriserat konsultbolag med **kollektivavtal**, försäkringar, friskvård och ... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 17 | ...kollektivavtal, försäkringar, **friskvård** och tjänstepension. Vi är spe... | x |  |  | 9 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 18 | ...on. Vi är specialiserade inom **IT**, Teknik, HR, Administration o... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 19 | ...ecialiserade inom IT, Teknik, **HR**, Administration och Ekonomi. ... | x |  |  | 2 | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
| 20 | ...eknik, HR, Administration och **Ekonomi**. Vår målsättning är att vara ... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| | **Overall** | | | **129** | **210** | 129/210 = **61%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Testning, mjukvara/Verifiering, mjukvara, **skill**](http://data.jobtechdev.se/taxonomy/concept/2kEw_jWL_6Zz) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
| x | x | x | [Operativ ledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aB7R_JQN_sDE) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| x | x | x | [Testledning, datasystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/dexT_Znd_Yrs) |
| x |  |  | [utföra prestandatester, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eYuM_Gc7_VpT) |
| x | x | x | [Acceptanstestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/jYaA_Cwa_Yyf) |
| x | x | x | [Systemtestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/xAbu_uKA_Jeu) |
| x |  |  | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **6** | 6/13 = **46%** |