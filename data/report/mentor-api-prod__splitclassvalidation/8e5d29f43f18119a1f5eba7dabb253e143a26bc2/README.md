# Results for '8e5d29f43f18119a1f5eba7dabb253e143a26bc2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8e5d29f43f18119a1f5eba7dabb253e143a26bc2](README.md) | 1 | 2681 | 34 | 21 | 72/554 = **13%** | 6/30 = **20%** |

## Source text

Efterfrågan på fastighetsförvaltare ökar i takt med att det byggs mer. Tillgången på yrkesgruppen är inte tillräcklig för att möta efterfrågan de närmaste åren, vilket innebär att möjligheterna till arbete är goda. En fastighetsförvaltare har ett övergripande ansvar för att utveckla, bevara och förbättra en eller flera fastigheter. I rollen ingår ofta ett personalansvar Under utbildningens 2,5 år läser du en lång rad olika kurser inom ekonomi, teknik och juridik.    Framtidsutsikter  Under de kommande tio åren kommer det att saknas runt 10 000 personer inom fastighetsbranschen- bland annat på grund av stora pensionsavgångar, nya kompetensbehov genom teknikutveckling, upprustning av miljonprogramsområden och nyetableringar.    Utbildningens mål  Efter utbildningen har du kunskaper, färdigheter och kompetenser att sköta förvaltningen av ett fastighetsbestånd samt leda förvaltningsorganisationen mot uppsatta mål, utveckla, bevara och förädla fastigheter samt dra nytta av digitaliseringens möjligheter.  Du kommer också att kunna värdera branschinformation inom både det tekniska och ekonomiska området och välja anpassade metoder för problemlösning samt genomföra inköp, upphandlingar samt genomföra förhandlingar på ett juridiskt korrekt sätt med hänsyn tagen till ekonomiska och miljömässiga mål.    Distansstudier  Att läsa på distans innebär att du har stor frihet att vara var du vill under din studietid, men för vissa kurser måste du ibland träffa lärare och studiekamrater. Distansutbildningar med träffar innebär att vissa moment som till exempel introduktion, tentamen eller studiebesök som utgör obligatoriska moment görs i Umeå. Det innebär att du måste kunna resa till Umeå några dagar per termin. Vid obligatoriska möten betalar du själv eventuella resor.  Att studera på distans innebär frihet men kräver samtidigt disciplin. Du planerar själv för att kunna följa med i utbildningens planer, och även som distansstudent innebär heltidsstudier att du studerar ungefär 40 timmar per vecka.    Kurser  Kommunikation och ledarskap  Projektmetodik  Ekonomi med fastighetsinriktning  Fastighetsföretagande  IT inom fastighet  Fastighetsjuridik  Investeringskalkylering  Fastighetsteknik  Elteknik  VVS-teknik  Styr och reglerteknik  Omvärldsanalys och hållbar utveckling  Marknadsföring, förhandlings- och mötesteknik  Fastighetsjuridik - avtal  LIA 1, 45 p Fastighetsföretagande  LIA 2, Teknik  LIA 3, Fastighetsförvaltning  Examensarbete    LIA  Under utbildningens 3 LIA-perioder (31 veckor) jobbar du i skarpt läge på olika arbetsplatser. Under LIA-perioderna utvecklar du dina teoretiska kunskaper i praktiken och skapar kontakter som leder till jobb.    

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Efterfrågan på **fastighetsförvaltare** ökar i takt med att det byggs... | x | x | 20 | 20 | [Fastighetsförvaltare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EPe8_EQu_YCL) |
| 2 | ...terna till arbete är goda. En **fastighetsförvaltare** har ett övergripande ansvar f... | x | x | 20 | 20 | [Fastighetsförvaltare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EPe8_EQu_YCL) |
| 3 | ...eter. I rollen ingår ofta ett **personalansvar** Under utbildningens 2,5 år lä... | x | x | 14 | 14 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| 4 | ...en lång rad olika kurser inom **ekonomi**, teknik och juridik.    Framt... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 4 | ...en lång rad olika kurser inom **ekonomi**, teknik och juridik.    Framt... | x |  |  | 7 | [Annan utbildning i industriell ekonomi och organisation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/m3Go_x9t_6xP) |
| 5 | ...ad olika kurser inom ekonomi, **teknik** och juridik.    Framtidsutsik... | x |  |  | 6 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 6 | ...rser inom ekonomi, teknik och **juridik**.    Framtidsutsikter  Under d... |  | x |  | 7 | [Juridik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z9ZH_7XU_Kkb) |
| 6 | ...rser inom ekonomi, teknik och **juridik**.    Framtidsutsikter  Under d... | x |  |  | 7 | [Annan utbildning i juridik och rättsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/jLhR_w1C_nTT) |
| 7 | ...rdigheter och kompetenser att **sköta förvaltningen av ett fastighetsbestånd** samt leda förvaltningsorganis... | x |  |  | 44 | [Fastighetsförvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/4XyZ_6t8_TWq) |
| 8 | ...av ett fastighetsbestånd samt **leda förvaltningsorganisationen** mot uppsatta mål, utveckla, b... | x |  |  | 31 | [leda en grupp, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/QZKT_724_B6i) |
| 9 | ...anisationen mot uppsatta mål, **utveckla, bevara och förädla fastigheter** samt dra nytta av digitaliser... | x |  |  | 40 | [leda fastighetsutvecklingsprojekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hsSx_kwC_pjw) |
| 10 | ...h välja anpassade metoder för **problemlösning** samt genomföra inköp, upphand... | x |  |  | 14 | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| 11 | ...  Distansstudier  Att läsa på **distans** innebär att du har stor frihe... | x |  |  | 7 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 12 | ...fa lärare och studiekamrater. **Distansutbildningar** med träffar innebär att vissa... |  | x |  | 19 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 13 | ...r obligatoriska moment görs i **Umeå**. Det innebär att du måste kun... | x | x | 4 | 4 | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| 14 | ... att du måste kunna resa till **Umeå** några dagar per termin. Vid o... | x | x | 4 | 4 | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| 15 | ...tuella resor.  Att studera på **distans** innebär frihet men kräver sam... | x |  |  | 7 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 16 | ...en som distansstudent innebär **heltidsstudier** att du studerar ungefär 40 ti... | x |  |  | 14 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 17 | ... timmar per vecka.    Kurser  **Kommunikation och ledarskap**  Projektmetodik  Ekonomi med ... | x |  |  | 27 | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
| 18 | ...ch ledarskap  Projektmetodik  **Ekonomi** med fastighetsinriktning  Fas... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 18 | ...ch ledarskap  Projektmetodik  **Ekonomi** med fastighetsinriktning  Fas... | x |  |  | 7 | [Annan utbildning i industriell ekonomi och organisation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/m3Go_x9t_6xP) |
| 19 | ...tning  Fastighetsföretagande  **IT** inom fastighet  Fastighetsjur... | x |  |  | 2 | [Informations- och kommunikationsteknik (IKT), allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/Wswh_P8P_qEL) |
| 20 | ...retagande  IT inom fastighet  **Fastighetsjuridik**  Investeringskalkylering  Fas... | x |  |  | 17 | [Annan utbildning i juridik och rättsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/jLhR_w1C_nTT) |
| 20 | ...retagande  IT inom fastighet  **Fastighetsjuridik**  Investeringskalkylering  Fas... |  | x |  | 17 | [Juridik och rättsvetenskap, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/yrf9_Zzw_FLx) |
| 21 | ...dik  Investeringskalkylering  **Fastighetsteknik**  Elteknik  VVS-teknik  Styr o... |  | x |  | 16 | [Fastighetsverksamhet, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/9uyq_gBA_8UJ) |
| 21 | ...dik  Investeringskalkylering  **Fastighetsteknik**  Elteknik  VVS-teknik  Styr o... |  | x |  | 16 | [Fastighetsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/crzp_WfW_wWC) |
| 21 | ...dik  Investeringskalkylering  **Fastighetsteknik**  Elteknik  VVS-teknik  Styr o... | x |  |  | 16 | [Fastighetsförmedling och fastighetsförvaltning på uppdrag, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/nzyW_7Zx_yev) |
| 21 | ...dik  Investeringskalkylering  **Fastighetsteknik**  Elteknik  VVS-teknik  Styr o... |  | x |  | 16 | [Fastighetsförvaltning på uppdrag, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/vepN_y8A_oA6) |
| 22 | ...alkylering  Fastighetsteknik  **Elteknik**  VVS-teknik  Styr och reglert... |  | x |  | 8 | [Projektledare, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1jm3_XFQ_TdJ) |
| 22 | ...alkylering  Fastighetsteknik  **Elteknik**  VVS-teknik  Styr och reglert... |  | x |  | 8 | [Elteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/TGZ1_Xb5_77h) |
| 22 | ...alkylering  Fastighetsteknik  **Elteknik**  VVS-teknik  Styr och reglert... | x |  |  | 8 | [Elektronik-, tele- och datatekniskt arbete, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/rbPL_MYk_xDF) |
| 23 | ...  Fastighetsteknik  Elteknik  **VVS-teknik**  Styr och reglerteknik  Omvär... | x | x | 10 | 10 | [VVS-utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/24ra_4jC_iFz) |
| 24 | ...lteknik  VVS-teknik  Styr och **reglerteknik**  Omvärldsanalys och hållbar u... |  | x |  | 12 | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
| 25 | ...eknik  Styr och reglerteknik  **Omvärldsanalys** och hållbar utveckling  Markn... |  | x |  | 14 | [Business Intelligence/Omvärldsanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/T7e8_s89_R2x) |
| 26 | ...nalys och hållbar utveckling  **Marknadsföring**, förhandlings- och mötestekni... | x |  |  | 14 | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| 27 | ...r utveckling  Marknadsföring, **förhandlings- och **mötesteknik  Fastighetsjuridik... |  | x |  | 18 | [Förhandlingsteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xk4b_999_TGb) |
| 28 | ...nadsföring, förhandlings- och **mötesteknik**  Fastighetsjuridik - avtal  L... |  | x |  | 11 | [Mötesteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QKRo_5d7_pLp) |
| 29 | ...örhandlings- och mötesteknik  **Fastighetsjuridik** - avtal  LIA 1, 45 p Fastighe... | x |  |  | 17 | [Annan utbildning i juridik och rättsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/jLhR_w1C_nTT) |
| 30 | ...agande  LIA 2, Teknik  LIA 3, **Fastighetsförvaltning**  Examensarbete    LIA  Under ... | x |  |  | 21 | [Fastighetsförvaltning på uppdrag, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/vepN_y8A_oA6) |
| | **Overall** | | | **72** | **554** | 72/554 = **13%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Projektledare, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1jm3_XFQ_TdJ) |
| x | x | x | [VVS-utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/24ra_4jC_iFz) |
| x |  |  | [Fastighetsförvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/4XyZ_6t8_TWq) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Fastighetsverksamhet, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/9uyq_gBA_8UJ) |
| x |  |  | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
| x | x | x | [Fastighetsförvaltare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EPe8_EQu_YCL) |
| x |  |  | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
|  | x |  | [Mötesteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QKRo_5d7_pLp) |
| x |  |  | [leda en grupp, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/QZKT_724_B6i) |
| x | x | x | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
|  | x |  | [Business Intelligence/Omvärldsanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/T7e8_s89_R2x) |
|  | x |  | [Elteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/TGZ1_Xb5_77h) |
|  | x |  | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
| x |  |  | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| x |  |  | [Informations- och kommunikationsteknik (IKT), allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/Wswh_P8P_qEL) |
|  | x |  | [Juridik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z9ZH_7XU_Kkb) |
| x | x | x | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
|  | x |  | [Fastighetsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/crzp_WfW_wWC) |
| x |  |  | [leda fastighetsutvecklingsprojekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hsSx_kwC_pjw) |
| x |  |  | [Annan utbildning i juridik och rättsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/jLhR_w1C_nTT) |
| x |  |  | [Annan utbildning i industriell ekonomi och organisation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/m3Go_x9t_6xP) |
| x |  |  | [Fastighetsförmedling och fastighetsförvaltning på uppdrag, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/nzyW_7Zx_yev) |
| x |  |  | [Elektronik-, tele- och datatekniskt arbete, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/rbPL_MYk_xDF) |
| x | x | x | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| x | x | x | [Fastighetsförvaltning på uppdrag, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/vepN_y8A_oA6) |
|  | x |  | [Förhandlingsteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xk4b_999_TGb) |
|  | x |  | [Juridik och rättsvetenskap, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/yrf9_Zzw_FLx) |
| x |  |  | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| | | **6** | 6/30 = **20%** |