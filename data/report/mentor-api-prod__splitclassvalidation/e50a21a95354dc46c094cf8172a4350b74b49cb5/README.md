# Results for 'e50a21a95354dc46c094cf8172a4350b74b49cb5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e50a21a95354dc46c094cf8172a4350b74b49cb5](README.md) | 1 | 3559 | 42 | 36 | 201/517 = **39%** | 18/38 = **47%** |

## Source text

Hårdvaruspecialist / Hårdvarutekniker Om företaget Consafe Logistics är Europas ledande leverantör av lagerhanteringssystem. Vi levererar tillförlitliga, flexibla och smarta lösningar som skapar konkurrensfördelar för våra kunder över hela världen. Med hjälp av vår expertis inom lagerhantering och ett starkt kundfokus levererar vi hållbara lösningar som passar alla lager, oavsett volym och komplexitet. Vi fokuserar på att optimera kundernas verksamhet och i kombination med mjukvaruutveckling i världsklass, ger det våra kunder en konkurrenskraftig fördel. Det har vi gjort sedan 1978. Consafe Logistics-koncernen har 400 anställda och huvudkontoret är placerat i Lund. Vi arbetar med våra globala kunder via våra lokala kontor i Sverige, Norge, Danmark, Finland, Nederländerna, Polen och Storbritannien. www.consafelogistics.com Om tjänsten Som tekniker inom hårdvara hos Consafe Logistics arbetar du nära både kunder och den interna organisationen. Detta då hårdvara supporterar hela verksamheten avseende hårdvaruleveranser. Consafe Logistics mission är att optimera sina kunders logistikflöden med hjälp av vårt egenutvecklade lagerhanteringssystem Astro WMS® och rätt hårdvara för kundernas lagerprocesser såsom truckdatorer, handdatorer, röstterminaler, skanners och etikettskrivare. Du kommer att ha ansvar för installation, konfiguration, leverans, implementering, support och service i samarbete med övriga i hårdvaruteamet. Vi räknar även med att din erfarenhet och personliga egenskaper kommer att bli ett viktigt bidrag till att stärka teamet och ytterligare förbättra våra processer. Du agerar spindeln i nätet till våra kunder och fyller en viktig representerande och kontaktintensiv roll såväl internt som externt. Det är en mycket omväxlande roll där stor vikt läggs vid eget ansvar och god förmåga att samarbeta och samspela med övriga medarbetare i teamet. Du kommer framförallt arbeta med våra produkter från större tillverkare som exempelvis hårdvara från Honeywell & Zebra uppkopplade i trådlösa nätverk och ibland med MDM system från SOTI. Tjänsten är en rekrytering där anställningen ingås med Consafe Logistics men EMPLOID driver urvalsprocessen. Om dig För att vara aktuell för tjänsten krävs följande: - Erfarenhet av hårdvara och support från roller som It-tekniker, teknisk support, drifttekniker eller roller med motsvarande arbetsuppgifter - Kunskap inom hårdvara, nätverk och IT-miljöer - Konfiguration av OS såsom Embedded Windows (IoT) eller Android är meriterande -  Det är meriterande om du har kunskaper inom C++ och Python - God förståelse för kundkontakt och ärendehantering - Flytande i svenska och engelska Som person är du serviceminded, har ett stort tekniskt intresse och brinner för att lösa tekniska problem. Du har ambitionen att alltid leverera goda resultat och ser till att hjälpa kunderna till varje pris. Du har en arbetsmetodik som genomsyras av struktur, vilket hjälper dig att självständigt driva ditt arbete framåt men som även möjliggör samarbete med dina kollegor.   Övrig information Start: Enligt överenskommelse Plats: Lund Arbetstid: Kontorstider vardag Lön: Enligt överenskommelse   Om du har frågor om tjänsten ber vi dig att maila info@emploid.se så hjälper någon i teamet dig. Ange vilken tjänst det gäller. Vi rekommenderar att du skickar in din ansökan omgående då vi gör ett löpande urval. Välkommen med din ansökan!   Sökord: It-Support, It-tekniker, Supporttekniker, support specialist, support, teknisk support, hårdvara, nätverk, server, heltid, Lund, Malmö, Helsingborg, rekrytering

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Hårdvaruspecialist / **Hårdvarutekniker** Om företaget Consafe Logistic... | x | x | 16 | 16 | [hårdvarutekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/P69K_8r2_DAK) |
| 2 | ...Europas ledande leverantör av **lagerhanteringssystem**. Vi levererar tillförlitliga,... | x |  |  | 21 | [Lagerhanteringsprogram, **skill**](http://data.jobtechdev.se/taxonomy/concept/1oA3_GfA_82Y) |
| 2 | ...Europas ledande leverantör av **lagerhanteringssystem**. Vi levererar tillförlitliga,... |  | x |  | 21 | [använda lagerhanteringssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/s3Ns_5Fn_Vwe) |
| 3 | ...ed hjälp av vår expertis inom **lagerhantering** och ett starkt kundfokus leve... |  | x |  | 14 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 4 | ...ara lösningar som passar alla **lager**, oavsett volym och komplexite... | x |  |  | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 5 | ...ksamhet och i kombination med **mjukvaruutveckling** i världsklass, ger det våra k... | x | x | 18 | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 6 | ...h huvudkontoret är placerat i **Lund**. Vi arbetar med våra globala ... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 7 | ...lobala kunder via våra lokala **kontor** i Sverige, Norge, Danmark, Fi... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 8 | ...nder via våra lokala kontor i **Sverige**, Norge, Danmark, Finland, Ned... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 9 | ...våra lokala kontor i Sverige, **Norge**, Danmark, Finland, Nederlände... | x |  |  | 5 | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
| 10 | ...kala kontor i Sverige, Norge, **Danmark**, Finland, Nederländerna, Pole... | x |  |  | 7 | [Danmark, **country**](http://data.jobtechdev.se/taxonomy/concept/Co3h_1xq_Cwb) |
| 11 | ...or i Sverige, Norge, Danmark, **Finland**, Nederländerna, Polen och Sto... | x |  |  | 7 | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
| 12 | ...ige, Norge, Danmark, Finland, **Nederländerna**, Polen och Storbritannien. ww... | x |  |  | 13 | [Nederländerna, **country**](http://data.jobtechdev.se/taxonomy/concept/mc3v_dHh_dzf) |
| 13 | ...mark, Finland, Nederländerna, **Polen** och Storbritannien. www.consa... | x |  |  | 5 | [Polen, **country**](http://data.jobtechdev.se/taxonomy/concept/EV41_7cy_jw7) |
| 14 | ...and, Nederländerna, Polen och **Storbritannien**. www.consafelogistics.com Om ... | x |  |  | 14 | [Storbritannien och Nordirland, **country**](http://data.jobtechdev.se/taxonomy/concept/7wHq_Mri_wCt) |
| 15 | ...Om tjänsten Som tekniker inom **hårdvara** hos Consafe Logistics arbetar... | x | x | 8 | 8 | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
| 16 | ...erna organisationen. Detta då **hårdvara** supporterar hela verksamheten... | x | x | 8 | 8 | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
| 17 | ... hjälp av vårt egenutvecklade **lagerhanteringssystem** Astro WMS® och rätt hårdvara ... | x |  |  | 21 | [Lagerhanteringsprogram, **skill**](http://data.jobtechdev.se/taxonomy/concept/1oA3_GfA_82Y) |
| 17 | ... hjälp av vårt egenutvecklade **lagerhanteringssystem** Astro WMS® och rätt hårdvara ... |  | x |  | 21 | [använda lagerhanteringssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/s3Ns_5Fn_Vwe) |
| 18 | ...ngssystem Astro WMS® och rätt **hårdvara** för kundernas lagerprocesser ... | x | x | 8 | 8 | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
| 19 | ...re tillverkare som exempelvis **hårdvara** från Honeywell & Zebra uppkop... | x | x | 8 | 8 | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
| 20 | ...ävs följande: - Erfarenhet av **hårdvara** och support från roller som I... | x | x | 8 | 8 | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
| 21 | ...a och support från roller som **It-tekniker**, teknisk support, driftteknik... | x |  |  | 11 | [IT-tekniker/Datatekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tdCZ_6Pn_VzT) |
| 21 | ...a och support från roller som **It-tekniker**, teknisk support, driftteknik... |  | x |  | 11 | [it-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ybqU_Mu2_Eti) |
| 22 | ...It-tekniker, teknisk support, **drifttekniker** eller roller med motsvarande ... | x |  |  | 13 | [Drifttekniker, IT, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/13md_uyV_BNG) |
| 22 | ...It-tekniker, teknisk support, **drifttekniker** eller roller med motsvarande ... |  | x |  | 13 | [Fastighetstekniker/Drifttekniker, fastighet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9NVZ_LfD_vqT) |
| 23 | ...rbetsuppgifter - Kunskap inom **hårdvara**, nätverk och IT-miljöer - Kon... | x | x | 8 | 8 | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
| 24 | ...fter - Kunskap inom hårdvara, **nätverk** och IT-miljöer - Konfiguratio... | x |  |  | 7 | [Nätverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/nHDU_dpj_R9H) |
| 25 | ...ap inom hårdvara, nätverk och **IT**-miljöer - Konfiguration av OS... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 26 | ... inom hårdvara, nätverk och IT**-miljöer** - Konfiguration av OS såsom E... |  | x |  | 8 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 27 | ...IT-miljöer - Konfiguration av **OS** såsom Embedded Windows (IoT) ... | x |  |  | 2 | [Operativsystem, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/xAWr_WYq_JPP) |
| 28 | ...r - Konfiguration av OS såsom **Embedded** Windows (IoT) eller Android ä... |  | x |  | 8 | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| 28 | ...r - Konfiguration av OS såsom **Embedded** Windows (IoT) eller Android ä... |  | x |  | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 29 | ...guration av OS såsom Embedded **Windows** (IoT) eller Android är merite... | x | x | 7 | 7 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 30 | ...av OS såsom Embedded Windows (**IoT**) eller Android är meriterande... | x | x | 3 | 3 | [Internet of things/IoT, **skill**](http://data.jobtechdev.se/taxonomy/concept/v1kW_h4M_Kej) |
| 31 | ... Embedded Windows (IoT) eller **Android** är meriterande -  Det är meri... | x | x | 7 | 7 | [Android, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/WQdS_Jtu_qPC) |
| 32 | ...ande om du har kunskaper inom **C++** och Python - God förståelse f... |  | x |  | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 33 | ...du har kunskaper inom C++ och **Python** - God förståelse för kundkont... | x | x | 6 | 6 | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| 34 | ... ärendehantering - Flytande i **svenska** och engelska Som person är du... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 35 | ...ring - Flytande i svenska och **engelska** Som person är du serviceminde... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 36 | ...uktur, vilket hjälper dig att **självständigt driva ditt arbete** framåt men som även möjliggör... | x |  |  | 31 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 37 | ...Enligt överenskommelse Plats: **Lund** Arbetstid: Kontorstider varda... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 38 | ...en med din ansökan!   Sökord: **It-Support**, It-tekniker, Supporttekniker... | x | x | 10 | 10 | [IT-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1Nz1_z46_39V) |
| 38 | ...en med din ansökan!   Sökord: **It-Support**, It-tekniker, Supporttekniker... |  | x |  | 10 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 39 | ...nsökan!   Sökord: It-Support, **It-tekniker**, Supporttekniker, support spe... | x | x | 11 | 11 | [it-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ybqU_Mu2_Eti) |
| 40 | ...ord: It-Support, It-tekniker, **Supporttekniker**, support specialist, support,... | x | x | 15 | 15 | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| 41 | ...It-tekniker, Supporttekniker, **support specialist**, support, teknisk support, hå... |  | x |  | 18 | [Supportspecialist, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yogt_XRM_iNR) |
| 42 | ...st, support, teknisk support, **hårdvara**, nätverk, server, heltid, Lun... | x | x | 8 | 8 | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
| 43 | ...t, teknisk support, hårdvara, **nätverk**, server, heltid, Lund, Malmö,... | x |  |  | 7 | [Nätverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/nHDU_dpj_R9H) |
| 44 | ...t, hårdvara, nätverk, server, **heltid**, Lund, Malmö, Helsingborg, re... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 45 | ...ara, nätverk, server, heltid, **Lund**, Malmö, Helsingborg, rekryter... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 46 | ...ätverk, server, heltid, Lund, **Malmö**, Helsingborg, rekrytering | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 47 | ... server, heltid, Lund, Malmö, **Helsingborg**, rekrytering | x | x | 11 | 11 | [Helsingborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qj3q_oXH_MGR) |
| | **Overall** | | | **201** | **517** | 201/517 = **39%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Drifttekniker, IT, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/13md_uyV_BNG) |
| x | x | x | [IT-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1Nz1_z46_39V) |
| x |  |  | [Lagerhanteringsprogram, **skill**](http://data.jobtechdev.se/taxonomy/concept/1oA3_GfA_82Y) |
| x | x | x | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
|  | x |  | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Storbritannien och Nordirland, **country**](http://data.jobtechdev.se/taxonomy/concept/7wHq_Mri_wCt) |
|  | x |  | [Fastighetstekniker/Drifttekniker, fastighet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9NVZ_LfD_vqT) |
| x | x | x | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| x |  |  | [Danmark, **country**](http://data.jobtechdev.se/taxonomy/concept/Co3h_1xq_Cwb) |
| x |  |  | [Polen, **country**](http://data.jobtechdev.se/taxonomy/concept/EV41_7cy_jw7) |
| x | x | x | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x | x | x | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [hårdvarutekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/P69K_8r2_DAK) |
| x |  |  | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
| x | x | x | [Android, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/WQdS_Jtu_qPC) |
| x |  |  | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Nederländerna, **country**](http://data.jobtechdev.se/taxonomy/concept/mc3v_dHh_dzf) |
| x | x | x | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| x |  |  | [Nätverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/nHDU_dpj_R9H) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x | x | x | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| x | x | x | [Helsingborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qj3q_oXH_MGR) |
|  | x |  | [använda lagerhanteringssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/s3Ns_5Fn_Vwe) |
| x |  |  | [IT-tekniker/Datatekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tdCZ_6Pn_VzT) |
| x | x | x | [Internet of things/IoT, **skill**](http://data.jobtechdev.se/taxonomy/concept/v1kW_h4M_Kej) |
| x |  |  | [Operativsystem, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/xAWr_WYq_JPP) |
|  | x |  | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
|  | x |  | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| x | x | x | [it-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ybqU_Mu2_Eti) |
|  | x |  | [Supportspecialist, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yogt_XRM_iNR) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **18** | 18/38 = **47%** |