# Results for 'e924f2151b53fc64088c4bf8a201bf7f169ca0c2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e924f2151b53fc64088c4bf8a201bf7f169ca0c2](README.md) | 1 | 3123 | 22 | 23 | 149/496 = **30%** | 5/23 = **22%** |

## Source text

Trädgårdsarbetare till Einar Mattsson Tjänstebeskrivning & erbjudande Till Einar Mattsson söker vi nu en trädgårdsarbetare. Tjänsten som utgår från Södermalm innebär att arbeta med skötsel av bostadsgårdar i Stockholms innerstad med omnejd. Du kommer att tillhöra ett trädgårdsteam om cirka tio personer där dina huvudsakliga arbetsuppgifter kommer att vara gräsklippning, ogräsrensning, lövblåsning, beskärning samt kontroller av samtliga växters trivsel i anslutning till kundens bostäder. Som trädgårdsarbetare kan det även förekomma enklare anläggningsarbeten.  I rollen som trädgårdsarbetare kommer du till exempel att: •  Plantera och bevattna växter. •  Beskära träd och buskar. •  Vår- och höststäda  Tjänsten passar dig som trivs med att arbeta med varierande arbetsuppgifter utomhus oavsett väder samt är van att arbeta med kroppen fysiskt. Det är även av stor vikt att du som ansöker har lätt att samarbeta i team, då arbetet mestadels sker två och två.    Personprofil Vi söker dig som vill arbeta utomhus samt har ett intresse för trädgårdsskötsel. Vidare ser vi att du som ansöker är positiv, stark och uthållig samt lösningsorienterad.  För att lyckas i rollen som trädgårdsarbetare krävs vidare:  •	B-körkort •	Serviceminded •	God samarbetsförmåga •	Digitalt intresse  Det är meriterande om du har en gymnasial examen från trädgårdsprogrammet, god körvana och tidigare dokumenterad arbetserfarenhet av trädgårdsarbete. Vi ser att du som söker är stresstålig, har en god fysik samt trivs att arbeta utomhus oavsett väder.  I din roll som trädgårdsarbete kommer du att både arbeta självständigt och i grupp. Därför är det av stor vikt att du är bekväm i både rollerna.  Vi värderar dina egenskaper högt och ser gärna att du har en bakgrund inom idrotts- eller föreningslivet. Berätta vilken aktivitet du utövat, på vilket sätt det har format dig som person samt hur du tror att detta påverkar dig i en roll som trädgårdsarbetare.   Övrigt Start: Enligt överenskommelse  Omfattning: Heltid Arbetstid: Mån-tor 06.45 – 16.00, fre 06.45 – 14.45  Plats: Stockholm innerstad  Uppdraget ingår i vår konsultverksamhet vilket innebär att du kommer att vara anställd av PerformIQ och uthyrd till Einar Mattsson.   Skicka in din ansökan genom att trycka på ”ansök”.    Företagspresentation Einar Mattsson Fastighetsförvaltning AB förvaltar cirka 15 000 bostäder samt kommersiella lokaler till ett värde om 58 miljarder kronor. Bolaget förvaltar bostäder och lokaler för en bred och etablerad grupp av fastighetsägare såväl som Einar Mattssons 6 000 egna bostäder. Med ett långsiktigt perspektiv på fastighetsaffären och gedigen kunskap erbjuder Einar Mattsson fastighetsägare en trygg, personlig och proaktiv förvaltning i syfte att utveckla kundens fastighetsvärden. Bolaget är en del av Einar Mattsson-koncernen som har varit verksam på Storstockholms bygg- och fastighetsmarknad sedan 1935. Koncernens övriga verksamheter utgörs av fastighetsägande, byggproduktion, projektutveckling och övriga investeringar.  Läs mer om oss på www.einarmattsson.se och Facebook  Sökord: trädgårdarbete, parkarbete, fastighetsskötare

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Trädgårdsarbetare** till Einar Mattsson Tjänstebe... | x | x | 17 | 17 | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| 2 | ...Einar Mattsson söker vi nu en **trädgårdsarbetare**. Tjänsten som utgår från Söde... | x | x | 17 | 17 | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| 3 | ...ed skötsel av bostadsgårdar i **Stockholms** innerstad med omnejd. Du komm... | x |  |  | 10 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 4 | ...betsuppgifter kommer att vara **gräsklippning**, ogräsrensning, lövblåsning, ... |  | x |  | 13 | [använda olika typer av utrustning för gräsklippning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/b2w6_YKJ_qZX) |
| 5 | ...ommer att vara gräsklippning, **ogräsrensning**, lövblåsning, beskärning samt... |  | x |  | 13 | [utföra insatser för att bekämpa ogräs, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mY9p_ai7_b9N) |
| 6 | ..., ogräsrensning, lövblåsning, **beskärning** samt kontroller av samtliga v... |  | x |  | 10 | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
| 7 | ...ng till kundens bostäder. Som **trädgårdsarbetare** kan det även förekomma enklar... | x | x | 17 | 17 | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| 8 | ...an det även förekomma enklare **anläggningsarbeten**.  I rollen som trädgårdsarbet... |  | x |  | 18 | [Anläggningsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/myMG_ygC_kDW) |
| 9 | ...ggningsarbeten.  I rollen som **trädgårdsarbetare** kommer du till exempel att: •... | x | x | 17 | 17 | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| 10 | ...r vikt att du som ansöker har **lätt att samarbeta** i team, då arbetet mestadels ... | x |  |  | 18 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 11 | ...hus samt har ett intresse för **trädgårdsskötsel**. Vidare ser vi att du som ans... |  | x |  | 16 | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| 12 | ...itiv, stark och uthållig samt **lösningsorienterad**.  För att lyckas i rollen som... | x |  |  | 18 | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| 13 | ...  För att lyckas i rollen som **trädgårdsarbetare** krävs vidare:  •	B-körkort •	... | x | x | 17 | 17 | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| 14 | ...årdsarbetare krävs vidare:  •	**B-körkort** •	Serviceminded •	God samarbe... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 15 | ...•	B-körkort •	Serviceminded •	**God samarbetsförmåga** •	Digitalt intresse  Det är m... | x |  |  | 20 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 16 | ...t är meriterande om du har en **gymnasial examen** från trädgårdsprogrammet, god... | x |  |  | 16 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 17 | ...men från trädgårdsprogrammet, **god körvana** och tidigare dokumenterad arb... | x |  |  | 11 | [köra fordon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/8eY1_8ME_8rC) |
| 18 | ...umenterad arbetserfarenhet av **trädgårdsarbete**. Vi ser att du som söker är s... |  | x |  | 15 | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| 18 | ...umenterad arbetserfarenhet av **trädgårdsarbete**. Vi ser att du som söker är s... | x |  |  | 15 | [trädgårdsskötsel, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Jq7c_9Km_gLw) |
| 19 | ...e. Vi ser att du som söker är **stresstålig**, har en god fysik samt trivs ... | x |  |  | 11 | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
| 19 | ...e. Vi ser att du som söker är **stresstålig**, har en god fysik samt trivs ... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 20 | ... söker är stresstålig, har en **god fysik** samt trivs att arbeta utomhus... | x | x | 9 | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 21 | ...r en god fysik samt trivs att **arbeta utomhus** oavsett väder.  I din roll so... | x |  |  | 14 | [arbeta utomhus, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Jvu2_Ain_MsJ) |
| 22 | ...avsett väder.  I din roll som **trädgårdsarbete** kommer du att både arbeta sjä... |  | x |  | 15 | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| 22 | ...avsett väder.  I din roll som **trädgårdsarbete** kommer du att både arbeta sjä... | x |  |  | 15 | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| 23 | ...årdsarbete kommer du att både **arbeta självständigt** och i grupp. Därför är det av... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 24 | ...både arbeta självständigt och **i grupp**. Därför är det av stor vikt a... | x |  |  | 7 | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| 25 | ...ta påverkar dig i en roll som **trädgårdsarbetare**.   Övrigt Start: Enligt övere... | x | x | 17 | 17 | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| 26 | ... överenskommelse  Omfattning: **Heltid** Arbetstid: Mån-tor 06.45 – 16... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 27 | ...00, fre 06.45 – 14.45  Plats: **Stockholm** innerstad  Uppdraget ingår i ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 28 | ...m på Storstockholms bygg- och **fastighetsmarknad** sedan 1935. Koncernens övriga... |  | x |  | 17 | [fastighetsmarknad, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HavX_5vN_vWc) |
| 29 | ...ighetsägande, byggproduktion, **projektutveckling** och övriga investeringar.  Lä... |  | x |  | 17 | [övervaka våtmarker vid projektutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kD31_wW5_ybW) |
| 30 | ...sson.se och Facebook  Sökord: **trädgårdarbete**, parkarbete, fastighetsskötar... |  | x |  | 14 | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| 31 | ...book  Sökord: trädgårdarbete, **parkarbete**, fastighetsskötare |  | x |  | 10 | [Anläggningsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/myMG_ygC_kDW) |
| 32 | ...: trädgårdarbete, parkarbete, **fastighetsskötare** |  | x |  | 17 | [Fastighetsskötare/Fastighetsarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/C6Cv_9ZR_7Gb) |
| | **Overall** | | | **149** | **496** | 149/496 = **30%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [köra fordon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/8eY1_8ME_8rC) |
|  | x |  | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [Fastighetsskötare/Fastighetsarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/C6Cv_9ZR_7Gb) |
|  | x |  | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
|  | x |  | [fastighetsmarknad, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HavX_5vN_vWc) |
| x |  |  | [trädgårdsskötsel, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Jq7c_9Km_gLw) |
| x |  |  | [arbeta utomhus, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Jvu2_Ain_MsJ) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
|  | x |  | [använda olika typer av utrustning för gräsklippning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/b2w6_YKJ_qZX) |
| x | x | x | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
|  | x |  | [övervaka våtmarker vid projektutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kD31_wW5_ybW) |
|  | x |  | [utföra insatser för att bekämpa ogräs, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mY9p_ai7_b9N) |
|  | x |  | [Anläggningsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/myMG_ygC_kDW) |
| x |  |  | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **5** | 5/23 = **22%** |