# Results for '957e21e0c5fcb07f446d34d76b683685a2cb8288'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [957e21e0c5fcb07f446d34d76b683685a2cb8288](README.md) | 1 | 6049 | 33 | 23 | 96/423 = **23%** | 8/28 = **29%** |

## Source text

SMHI söker erfaren designer som vill jobba för ett hållbart samhälle Är du en erfaren UX/UI Designer som gillar att vara med från idé till färdig produkt? Vill du bidra till ett säkert samhälle och en hållbar framtid i ett förändrat klimat? Då ska du jobba på SMHI! Vi räddar världen, lite grann varje dag.  SMHIs verksamhet handlar om att samla in väder-, vatten- och klimatrelaterad data och analysera den. Men allt detta viktiga, och också spännande arbete, är betydelselöst om SMHI inte lyckas förmedla vad vetenskapen säger i meningsfulla underlag för beslutfattare och medborgare. Det är där som din kompetens som designer gör livsviktig skillnad.  Om tjänsten SMHI bygger just nu en designfunktion som stödjer alla myndighetens ca 20 utvecklingsteam. I flera år har designers på SMHI jobbat självständigt med ett eller några team. Nu vill vi etablera ett mer systematiskt arbetssätt där designers kan stödja varandra och förbättra en gemensam verktygslåda för design. Du och en till designer etablerar best practice för att integrera lämpliga aktiviteter i krav-, utvecklings- och förvaltningsprocessen. Ni jobbar med konkreta uppgifter för flera team och får stöd av, samt coachar, en till tre juniora designers. Vid behov får ni även stöd av konsulter. Under de sista åren har vi etablerat en designprocess som vi förankrat i hela organisationen och identifierat verktyg som effektiviserar arbetssättet. Till exempel använder alla team SMHIs egenframtagna designsystem och designfunktionen har en stor roll för att behålla systemet uppdaterat.  SMHI har en portfölj av ca 100 system, webb produkter och appar som behöver vara intuitiva att använda. Dessa system och produkter varierar stort i användning. Det finns komplexa arbetsverktyg som används dagligen i 24/7-produktionen av väder-, vatten- och klimatspecialister på SMHI. Det finns också portaler och appar som används av all sorts yrkesexpertis både i Sverige och internationellt. Slutligen flera webbtjänster och appar som används flitigt av allmänheten varje dag. Vädertjänsten på www.smhi.se är en av de mest besökta sidorna i Sverige med 18 miljoner besök bara i maj 2022, medan SMHIs app hade 47 miljoner besök samma månad.  Vad får du möjlighet till som en av två seniora designers på SMHI?   • Variation i vardagen genom att du får ta olika roller, från metodansvarig till hands-on designer för flera system och webbtjänster. Det ger dig en stor möjlighet till att vara kreativ på olika sätt som passar dig bäst.  • Jobba tillsammans med kunniga människor, båda designers och andra roller. • Du har stort utrymme för att påverka hur man bäst designar tjänster på SMHI, både processmässigt och gällande vilka verktyg du känner är bäst att använda. • Skapa balans i vardagen genom att du jobbar kontorstid men har möjlighet att strukturera upp din dag enligt förtroendearbetstidsavtalet. • Du jobbar nära våra väder-, vatten- och klimatspecialister, där du sitter längre tidsperioder i individuella team och hinner fördjupa dig i specifika verksamhetsbehov. • Du får även möjlighet till att vara en del av en inkluderande, nyfiken och kreativ kultur där allt du gör är samhällsviktigt. Varje dag är du med och bidrar till en bättre och säkrare vardag för Sverige.  Vi erbjuder en tillsvidareanställning vid en myndighet med ett aktuellt samhällsuppdrag. Tjänsten är placerad vid SMHIs huvudkontor i Norrköping.  Din profil Vi tror att du har jobbat minst 3 år som designer av olika IT-system. Att du har uppnått en så pass hög kompetens att du känner dig trygg i din roll som UX/UI designer både för att arbeta som designer, men också för att agera som förebild för andra designers. Du har en dokumenterad erfarenhet av hela designkedjan: från organisering och genomförande av förstudier, prototypdesign, hela vägen till att ha jobbat i agila team i nära relation till systemutvecklare, kravställare och användare.  Vi kommer att lägga stort vikt på dina personliga erfarenheter och din erfarenhet inom design. Under intervjun kommer du att kunna visa din portfolio, gärna med fokus på bredden av din erfarenhet.  Det är meriterande om du har erfarenhet av:   • WCAG  2.1 AA  • Designsystem • Sketch, eller liknande verktyg • Adobe Design Suite inkl. Illustrator • Design av appar och hands-on förståelse av skillnaden mellan iOS och Android  Vi tror att du har lätt att samarbeta med andra och är självgående. Du är också strukturerad och har lätt att byta mellan olika arbetsuppgifter utan att försämra kvaliteten eller din noggrannhet.  Flytande svenska i tal och skrift är önskvärt då all intern kommunikation sker på svenska Om svenskkunskaper saknas eller är otillräckliga kommer en gedigen plan för att uppnå kunskaperna att tillsättas. Goda kunskaper i engelska i tal och skrift är ett krav.  Det finns mycket att berätta Vill du veta mer om tjänsten kontakta gruppchef Cecile Åberg, har du frågor om rekryteringsprocessen kontakta HR-specialist Vendela Johansson. Fackliga representanter är för SACO Anders Ekner och för ST Christina Sverker. Samtliga nås via e-post fornamn.efternamn@smhi.se eller telefon 011-495 8000.  Välkommen med din ansökan redan idag, dock senast den 28 augusti 2022.      SMHI är en svensk expertmyndighet med globalt perspektiv och en livsviktig uppgift i att förutse förändringar i väder, vatten och klimat. Med vetenskaplig grund och genom kunskap, forskning och tjänster bidrar vi till att öka hela samhällets hållbarhet. Varje dag, dygnet runt, året om.  Tänk på att de handlingar och uppgifter du skickar till SMHI genom din ansökan blir en allmän handling. Detta betyder att allt material i ansökan, inklusive bilagor, kan behöva lämnas ut till den som begär det om uppgifterna inte omfattas av sekretess enligt offentlighets- och sekretesslagen. Tänk på att i första hand skriva det som du bedömer är relevant i förhållande till kraven på tjänsten. Tänk på din integritet och undvik att lämna information som innehåller känsliga personuppgifter, uppgifter om din eller närståendes hälsa, politiska åsikter eller religiös övertygelse.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | SMHI söker erfaren **designer** som vill jobba för ett hållba... | x |  |  | 8 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 2 | ...art samhälle Är du en erfaren **UX**/UI Designer som gillar att va... | x |  |  | 2 | [UX-designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/iwJU_gW4_SxT) |
| 3 | ... samhälle Är du en erfaren UX/**UI Designer** som gillar att vara med från ... |  | x |  | 11 | [UI-designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3GKG_7xX_AcT) |
| 3 | ... samhälle Är du en erfaren UX/**UI Designer** som gillar att vara med från ... | x |  |  | 11 | [UI designer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/GdC7_Bzv_S21) |
| 4 | ...r i meningsfulla underlag för **beslutfattare** och medborgare. Det är där so... |  | x |  | 13 | [påverka beslutsfattare i frågor som rör socialtjänsten, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BjNm_4fR_2t7) |
| 4 | ...r i meningsfulla underlag för **beslutfattare** och medborgare. Det är där so... |  | x |  | 13 | [ge kostrekommendationer till offentliga beslutsfattare, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WVtA_3ZB_e3T) |
| 4 | ...r i meningsfulla underlag för **beslutfattare** och medborgare. Det är där so... |  | x |  | 13 | [ge råd till beslutsfattare inom hälso- och sjukvården, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tieT_MzT_Vp8) |
| 4 | ...r i meningsfulla underlag för **beslutfattare** och medborgare. Det är där so... |  | x |  | 13 | [informera beslutsfattare om hälsorelaterade utmaningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uMFP_cGd_FtZ) |
| 5 | ... är där som din kompetens som **designer** gör livsviktig skillnad.  Om ... | x |  |  | 8 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 6 | ...lera år har designers på SMHI **jobbat självständigt** med ett eller några team. Nu ... | x |  |  | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 7 | ...da för design. Du och en till **designer** etablerar best practice för a... | x |  |  | 8 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 8 | ... coachar, en till tre juniora **designers**. Vid behov får ni även stöd a... | x |  |  | 9 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 9 | ...alla team SMHIs egenframtagna **designsystem** och designfunktionen har en s... |  | x |  | 12 | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| 10 | ...en portfölj av ca 100 system, **webb** produkter och appar som behöv... | x |  |  | 4 | [Webb, **skill**](http://data.jobtechdev.se/taxonomy/concept/KKQV_aGF_bWq) |
| 11 | ...ll sorts yrkesexpertis både i **Sverige** och internationellt. Slutlige... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 12 | ... av de mest besökta sidorna i **Sverige** med 18 miljoner besök bara i ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 13 | ...et till som en av två seniora **designers** på SMHI?   • Variation i vard... | x |  |  | 9 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 14 | ...n metodansvarig till hands-on **designer** för flera system och webbtjän... | x |  |  | 8 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 15 | ...s med kunniga människor, båda **designers** och andra roller. • Du har st... | x |  |  | 9 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 16 | ...bättre och säkrare vardag för **Sverige**.  Vi erbjuder en tillsvidarea... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 17 | ... för Sverige.  Vi erbjuder en **tillsvidareanställning **vid en myndighet med ett aktue... | x |  |  | 23 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 18 | ...cerad vid SMHIs huvudkontor i **Norrköping**.  Din profil Vi tror att du h... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 19 | ... du har jobbat minst 3 år som **designer** av olika IT-system. Att du ha... | x |  |  | 8 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 20 | ...st 3 år som designer av olika **IT-system**. Att du har uppnått en så pas... |  | x |  | 9 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 21 | ...nner dig trygg i din roll som **UX**/UI designer både för att arbe... | x |  |  | 2 | [UX designer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/BARa_87y_Zpb) |
| 22 | ...r dig trygg i din roll som UX/**UI designer** både för att arbeta som desig... |  | x |  | 11 | [UI-designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3GKG_7xX_AcT) |
| 22 | ...r dig trygg i din roll som UX/**UI designer** både för att arbeta som desig... | x |  |  | 11 | [UI designer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/GdC7_Bzv_S21) |
| 23 | ...igner både för att arbeta som **designer**, men också för att agera som ... | x |  |  | 8 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 24 | ... agera som förebild för andra **designers**. Du har en dokumenterad erfar... | x |  |  | 9 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 25 | ...ila team i nära relation till **systemutvecklare**, kravställare och användare. ... | x |  |  | 16 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 26 | ... om du har erfarenhet av:   • **WCAG**  2.1 AA  • Designsystem • Ske... | x | x | 4 | 4 | [Web Content Accessibility Guidelines/WCAG, **skill**](http://data.jobtechdev.se/taxonomy/concept/4Dqo_LPf_qrU) |
| 27 | ...enhet av:   • WCAG  2.1 AA  • **Designsystem** • Sketch, eller liknande verk... |  | x |  | 12 | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| 28 | ...stem • Sketch, eller liknande **verktyg** • Adobe Design Suite inkl. Il... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 29 | ...yg • Adobe Design Suite inkl. **Illustrator** • Design av appar och hands-o... | x |  |  | 11 | [Adobe Illustrator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bGA6_xqh_Cvv) |
| 30 | ...rståelse av skillnaden mellan **iOS** och Android  Vi tror att du h... | x |  |  | 3 | [IOS, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/t78J_nRk_xGn) |
| 31 | ... av skillnaden mellan iOS och **Android**  Vi tror att du har lätt att ... | x | x | 7 | 7 | [Android, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/WQdS_Jtu_qPC) |
| 32 | ...försämra kvaliteten eller din **noggrannhet**.  Flytande svenska i tal och ... | x | x | 11 | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 33 | ...er din noggrannhet.  Flytande **svenska** i tal och skrift är önskvärt ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 34 | ... intern kommunikation sker på **svenska** Om svenskkunskaper saknas ell... | x |  |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 35 | ...munikation sker på svenska Om **svenskkunskaper** saknas eller är otillräckliga... | x | x | 15 | 15 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 36 | ... tillsättas. Goda kunskaper i **engelska** i tal och skrift är ett krav.... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 37 | ...ekryteringsprocessen kontakta **HR-specialist** Vendela Johansson. Fackliga r... | x | x | 13 | 13 | [HR-specialist/HR-advisor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s28Q_sya_S2b) |
| 38 | ...ess enligt offentlighets- och **sekretesslagen**. Tänk på att i första hand sk... |  | x |  | 14 | [dataskydd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/J7Xa_3VE_VTe) |
| 39 | ...fter om din eller närståendes **hälsa**, politiska åsikter eller reli... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| | **Overall** | | | **96** | **423** | 96/423 = **23%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [UI-designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3GKG_7xX_AcT) |
| x |  |  | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| x | x | x | [Web Content Accessibility Guidelines/WCAG, **skill**](http://data.jobtechdev.se/taxonomy/concept/4Dqo_LPf_qrU) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [UX designer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/BARa_87y_Zpb) |
|  | x |  | [påverka beslutsfattare i frågor som rör socialtjänsten, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BjNm_4fR_2t7) |
|  | x |  | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| x |  |  | [UI designer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/GdC7_Bzv_S21) |
|  | x |  | [dataskydd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/J7Xa_3VE_VTe) |
| x |  |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| x |  |  | [Webb, **skill**](http://data.jobtechdev.se/taxonomy/concept/KKQV_aGF_bWq) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| x | x | x | [Android, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/WQdS_Jtu_qPC) |
|  | x |  | [ge kostrekommendationer till offentliga beslutsfattare, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WVtA_3ZB_e3T) |
|  | x |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Adobe Illustrator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bGA6_xqh_Cvv) |
| x |  |  | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [UX-designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/iwJU_gW4_SxT) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [HR-specialist/HR-advisor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s28Q_sya_S2b) |
| x |  |  | [IOS, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/t78J_nRk_xGn) |
|  | x |  | [ge råd till beslutsfattare inom hälso- och sjukvården, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tieT_MzT_Vp8) |
|  | x |  | [informera beslutsfattare om hälsorelaterade utmaningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uMFP_cGd_FtZ) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/28 = **29%** |