# Results for '3fd6cefff404c81db9af63db3ea37d2b7bfc675d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [3fd6cefff404c81db9af63db3ea37d2b7bfc675d](README.md) | 1 | 4622 | 27 | 22 | 117/428 = **27%** | 10/32 = **31%** |

## Source text

Software Developer Platform SW applications - Autonomous Drive Who are we? Volvo Cars is a company on a mission; to bring traditional car manufacturing into a connected, sustainable and smart future. Since 1927, we have been a brand known for our commitment to safety, creating innovative cars that make life less complicated for our consumers. In 2010, we decided to transform our business, resulting in a totally new generation of cars and technologies, as well as steady growth and record sales. Today, we’re expanding our global footprint in Europe, China and the US, and we’re on the lookout for new talent.  We are constantly pushing our own skills and abilities to drive change in the automobile industry like never before. We are looking for innovative, committed people to join us in this endeavour and create safe, sustainable and connected cars. We believe in the power of people and will challenge and support you to reach your full potential. Join us and be part of Volvo Cars’ journey into the future.  Software development that makes a difference Nothing beats being part of positive change. We’re on a truly exciting journey, working together in a fast-paced global environment to break new ground in almost every aspect of our operations. Now we offer you a once-in-a-lifetime chance to make an impact in a company with unique opportunities to grow and make a true difference. What we offer  At Safe Vehicle Automation we create tomorrow’s solutions for our active safety and self-driving vehicles. Our Agile Release Train (ART) Autonomous Drive & ADAS Platform develops the sensor and software platform that enables deployment of functionality such as Collision Avoidance, Automatic Parking and Autonomous Driving. Within our group Platform Software, we have an opportunity for you, a dedicated SW developer, who want to be part of the team driving the future in our world-class platform that will enable delivery of autonomous drive features powered by Artificial Intelligence. We develop the platform solutions for both sensor integration as well as sensor perception & fusion, and we have several openings in our teams, possible to tailor pending your background and technical passions. With us, you will have a unique chance to make an impact in solving the “engineering problem of the century”. This is what you will do As Software Developer you will together with the team be implementing and deploying platform solutions in terms of mission critical and memory intensive applications. The team is responsible for end-to-end delivery of the platform application including design, architecture, documentation, implementation and automated test cases. You will be part of an agile development team that handles its own backlog and works closely with the Product Owner to ensure a capable, modular, scalable and quality assured software platform. We use the latest tools and technologies available and collaborate with partners and other teams to deliver state-of-the-art software.  This is you You have a clear passion for SW design and development of real time, safety critical software for automotive applications. While being a committed coder, you also have the skills to work “hands-on” in our rigs with test environment development and verification. Your mindset is characterized by curiosity to innovate and continuously learn and try out new ideas. You are a driven and self-motivated person yet team-oriented and you thrive in working in a collaborative and dynamic environment where you can build software development communities through applying open-source development principles. You are a Software developer with minimum 3 years of experience from automotive embedded systems and C++/Python development. Your academic background is a M Sc degree in Computer Science, Computer Engineering or similar. You have experience in areas such as QNX, memory intensive applications, Computer vision, Machine learning/Deep learning, sensor perception & sensor fusion, optimization of SW code and test automation. Tools such as Docker, JFrog, Artifactory, CANalyzer, Wireshark are well-known to you, as well as are agile methods and the SAFe framework. Having worked with ISO26262/DO178(Functional Safety), Cyber Security or Nvidia GPU development platform is meritorious. As we are a truly international team, we see you have excellent communication skills being fluent in English both verbally and in writing. Here is how you apply! You apply via the link in the advert, no later than 19th August. We are recruiting 3 similar positions. Note that we do not accept any applications via e-mail.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...nnovative cars that make life **less** complicated for our consumers... |  | x |  | 4 | [LESS, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LrkB_soN_kaZ) |
| 2 | ...l as steady growth and record **sales**. Today, we’re expanding our g... |  | x |  | 5 | [Sales, **skill**](http://data.jobtechdev.se/taxonomy/concept/YTUj_vai_cLw) |
| 3 | ...nding our global footprint in **Europe**, China and the US, and we’re ... | x |  |  | 6 | [Europa/EU/EES/EURES, **continent**](http://data.jobtechdev.se/taxonomy/concept/HstV_nCB_W2i) |
| 4 | ...r global footprint in Europe, **China** and the US, and we’re on the ... | x |  |  | 5 | [Kina, **country**](http://data.jobtechdev.se/taxonomy/concept/hwyG_7pq_yHR) |
| 5 | ...rint in Europe, China and the **US**, and we’re on the lookout for... | x |  |  | 2 | [Förenta staterna (Amerikas förenta stater), **country**](http://data.jobtechdev.se/taxonomy/concept/GwGd_ukJ_qsx) |
| 6 | ...’ journey into the future.  **Software development** that makes a difference Nothi... | x |  |  | 20 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 7 | ...hat we offer  At Safe Vehicle **Automation** we create tomorrow’s solution... | x |  |  | 10 | [Automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/tDxT_7oT_NyU) |
| 8 | ...nd self-driving vehicles. Our **Agile** Release Train (ART) Autonomou... |  | x |  | 5 | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| 9 | ...ortunity for you, a dedicated **SW developer**, who want to be part of the t... | x |  |  | 12 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 10 | ...ous drive features powered by **Artificial Intelligence**. We develop the platform solu... | x | x | 23 | 23 | [Artificial Intelligence/Artificiell intelligens/AI, **skill**](http://data.jobtechdev.se/taxonomy/concept/SaTd_K5z_ex1) |
| 11 | ...s well as sensor perception & **fusion**, and we have several openings... |  | x |  | 6 | [Film/effekter-eyeon Fusion, **skill**](http://data.jobtechdev.se/taxonomy/concept/B83k_mVq_771) |
| 11 | ...s well as sensor perception & **fusion**, and we have several openings... |  | x |  | 6 | [Fusion, **skill**](http://data.jobtechdev.se/taxonomy/concept/CDbY_f36_emv) |
| 11 | ...s well as sensor perception & **fusion**, and we have several openings... |  | x |  | 6 | [Netobjects Fusion, HTML-editor, **skill**](http://data.jobtechdev.se/taxonomy/concept/W5Dy_xwG_V2k) |
| 12 | .... This is what you will do As **Software Developer** you will together with the te... | x |  |  | 18 | [Mjukvaruutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rQds_YGd_quU) |
| 13 | ...cases. You will be part of an **agile** development team that handles... | x | x | 5 | 5 | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| 14 | .... You will be part of an agile** development** team that handles its own bac... | x |  |  | 12 | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| 15 | ...development team that handles **its** own backlog and works closely... |  | x |  | 3 | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| 16 | ... safety critical software for **automotive** applications. While being a c... | x | x | 10 | 10 | [Automotive/Fordonsindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/jnLy_6Ns_KpH) |
| 17 | ...vironment where you can build **software development** communities through applying ... | x |  |  | 20 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 18 | ... communities through applying **open-source development** principles. You are a Softwar... | x |  |  | 23 | [Öppen källkod/Open Source, utvecklingsmodell, **skill**](http://data.jobtechdev.se/taxonomy/concept/sQgp_BKY_n43) |
| 19 | ...lopment principles. You are a **Software developer** with minimum 3 years of exper... | x |  |  | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 20 | ...um 3 years of experience from **automotive** embedded systems and C++/Pyth... | x | x | 10 | 10 | [Automotive/Fordonsindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/jnLy_6Ns_KpH) |
| 21 | ...of experience from automotive **embedded** systems and C++/Python develo... |  | x |  | 8 | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| 21 | ...of experience from automotive **embedded** systems and C++/Python develo... | x | x | 8 | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 22 | ...ience from automotive embedded** systems** and C++/Python development. Y... | x |  |  | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 23 | ...tomotive embedded systems and **C++**/Python development. Your acad... |  | x |  | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 24 | ...tive embedded systems and C++/**Python** development. Your academic ba... | x | x | 6 | 6 | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| 25 | ...Your academic background is a **M Sc degree** in Computer Science, Computer... | x |  |  | 11 | [Högskoleutbildning, generell, 5 år eller längre, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/mJj4_3Dz_V2z) |
| 26 | ...ackground is a M Sc degree in **Computer Science, Computer Engineering** or similar. You have experien... | x |  |  | 38 | (not found in taxonomy) |
| 27 | ...pplications, Computer vision, **Machine learning**/Deep learning, sensor percept... | x | x | 16 | 16 | [Machine learning/Maskininlärning, **skill**](http://data.jobtechdev.se/taxonomy/concept/XEhm_7YA_qKM) |
| 28 | ..., optimization of SW code and **test automation**. Tools such as Docker, JFrog,... | x | x | 15 | 15 | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| 29 | ...est automation. Tools such as **Docker**, JFrog, Artifactory, CANalyze... | x | x | 6 | 6 | [Docker, container-plattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZipR_ckJ_YQC) |
| 30 | ...s Docker, JFrog, Artifactory, **CANalyzer**, Wireshark are well-known to ... | x | x | 9 | 9 | [CANalyzer, analysverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/NtWF_zFu_Mhg) |
| 31 | ...Frog, Artifactory, CANalyzer, **Wireshark** are well-known to you, as wel... |  | x |  | 9 | [Wireshark, analysverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/mXkJ_Lui_LFU) |
| 31 | ...Frog, Artifactory, CANalyzer, **Wireshark** are well-known to you, as wel... | x | x | 9 | 9 | [Wireshark, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rzDG_nGj_vcF) |
| 32 | ...-known to you, as well as are **agile** methods and the SAFe framewor... |  | x |  | 5 | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| 33 | ...-known to you, as well as are **agile methods** and the SAFe framework. Havin... | x |  |  | 13 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 34 | ... as are agile methods and the **SAFe framework**. Having worked with ISO26262/... | x |  |  | 14 | [Scaled Agile Framework, SAFe, **skill**](http://data.jobtechdev.se/taxonomy/concept/eTW8_AY5_tpe) |
| 35 | ...262/DO178(Functional Safety), **Cyber Security** or Nvidia GPU development pla... | x |  |  | 14 | [cybersäkerhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cUku_oZH_AtX) |
| 36 | ...cation skills being fluent in **English** both verbally and in writing.... | x |  |  | 7 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| | **Overall** | | | **117** | **428** | 117/428 = **27%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| x |  |  | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
|  | x |  | [Film/effekter-eyeon Fusion, **skill**](http://data.jobtechdev.se/taxonomy/concept/B83k_mVq_771) |
|  | x |  | [Fusion, **skill**](http://data.jobtechdev.se/taxonomy/concept/CDbY_f36_emv) |
| x |  |  | [Förenta staterna (Amerikas förenta stater), **country**](http://data.jobtechdev.se/taxonomy/concept/GwGd_ukJ_qsx) |
| x |  |  | [Europa/EU/EES/EURES, **continent**](http://data.jobtechdev.se/taxonomy/concept/HstV_nCB_W2i) |
|  | x |  | [LESS, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LrkB_soN_kaZ) |
| x |  |  | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [CANalyzer, analysverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/NtWF_zFu_Mhg) |
| x | x | x | [Artificial Intelligence/Artificiell intelligens/AI, **skill**](http://data.jobtechdev.se/taxonomy/concept/SaTd_K5z_ex1) |
| x |  |  | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
|  | x |  | [Netobjects Fusion, HTML-editor, **skill**](http://data.jobtechdev.se/taxonomy/concept/W5Dy_xwG_V2k) |
| x | x | x | [Machine learning/Maskininlärning, **skill**](http://data.jobtechdev.se/taxonomy/concept/XEhm_7YA_qKM) |
|  | x |  | [Sales, **skill**](http://data.jobtechdev.se/taxonomy/concept/YTUj_vai_cLw) |
| x | x | x | [Docker, container-plattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZipR_ckJ_YQC) |
| x | x | x | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| x |  |  | [cybersäkerhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cUku_oZH_AtX) |
|  | x |  | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| x |  |  | [Scaled Agile Framework, SAFe, **skill**](http://data.jobtechdev.se/taxonomy/concept/eTW8_AY5_tpe) |
| x |  |  | [Kina, **country**](http://data.jobtechdev.se/taxonomy/concept/hwyG_7pq_yHR) |
| x |  |  | (not found in taxonomy) |
| x | x | x | [Automotive/Fordonsindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/jnLy_6Ns_KpH) |
| x |  |  | [Högskoleutbildning, generell, 5 år eller längre, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/mJj4_3Dz_V2z) |
|  | x |  | [Wireshark, analysverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/mXkJ_Lui_LFU) |
| x | x | x | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| x | x | x | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| x |  |  | [Mjukvaruutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rQds_YGd_quU) |
| x | x | x | [Wireshark, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rzDG_nGj_vcF) |
| x |  |  | [Öppen källkod/Open Source, utvecklingsmodell, **skill**](http://data.jobtechdev.se/taxonomy/concept/sQgp_BKY_n43) |
| x |  |  | [Automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/tDxT_7oT_NyU) |
| x | x | x | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
|  | x |  | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| | | **10** | 10/32 = **31%** |