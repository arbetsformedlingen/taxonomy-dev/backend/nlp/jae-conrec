# Results for 'd102243d476863003f4d9961b95f61ec9dbaecd8'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d102243d476863003f4d9961b95f61ec9dbaecd8](README.md) | 1 | 2794 | 27 | 23 | 108/281 = **38%** | 15/24 = **62%** |

## Source text

Java utvecklare Om programmering är din passion och du värdesätter möjligheter, utveckling och gemenskap så tror vi att du kommer trivas hos oss på ALTEN i Lund. Vi söker duktiga  Java utvecklare som tillsammans med oss vill bygga för en hållbar framtid, läs vidare och ansök idag!  VI ERBJUDER DIG Som Java utvecklare har du möjligheten att arbeta med det du brinner för och samtidigt utveckla och bredda din kompetens. Tillsammans med din coachande chef bygger du din karriär så att du kan utvecklas och förverkliga dina mål och drömmar! Genom våra interna nätverk ALTEN Sports och Women@ALTEN ges du också möjlighet att tillsammans med dina kollegor driva frågor och aktiviteter som du brinner för. På ALTEN tycker vi det är viktigt med balans mellan arbete och fritid, därför erbjuder vi tre extra lediga dagar per år. Vi erbjuder även förmåner som: Personlig utbildningsbudget Personalcykel Friskvårdsbidrag Kollektivavtal, tjänstepension och försäkringar    KONSULTROLLEN  Som Java utvecklare hos oss blir du en del av vårt härliga internationella konsultteam där våra konsulter besitter olika kompetenser inom IT. Du kommer att ha varierande arbetsuppgifter och får möjligheten att arbeta med nationella och internationella projekt hos någon av våra världsledande kunder eller inhouse. På ALTEN har vi ett stort utbud av uppdrag hos många av de främsta företagen inom flera olika branscher såsom fordon, telekom, energi, verkstadsindustri, läkemedel och medicinteknik, där våra konsulter är verksamma genom hela produktutvecklingskedjan. Varje medarbetare är viktig för ALTENs framgång och därför värdesätter vi dig som individ och arbetar aktivt med personlig utveckling.  Vi söker dig som trivs att arbeta i en kreativ miljö där du kan bygga på dina tidigare erfarenheter. Du är en lagspelare, fokuserad och tycker om problemlösning och utmaningar. Vi ser också att du som Java-utvecklare har följande färdigheter:   Krav: Java Spring eller Spring boot Microservices PostgreSQL Git JPA eller Hibernate  Meriterande: Azure, AWS eller Google cloud plattform React.js HTML DevOps Kubernetes Flytande kunskaper i svenska i tal och skrift  Urvalet görs löpande så ansök redan idag!   Har du några frågor mejla Yana Zubrytska, Talent Acquisition Partner IT på yana.zubrytska@alten.se  OM ALTEN  ALTEN är ett av Europas största teknik- och IT-konsultföretag med över 45 000 anställda i mer än 30 länder. Våra ingenjörer utför komplexa och mycket tekniska projekt genom hela produktutvecklingskedjan hos de främsta företagen inom flera olika branscher såsom Fordon, Telecom, Industri, Energi, Flyg och Försvar samt Life Science. I Sverige är vi över 1300 engagerade medarbetare med 11 kontor i 10 städer - från Lund i söder till Skellefteå i norr. Välkommen in att läsa mer om oss på alten.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Java** utvecklare Om programmering ä... |  | x |  | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 2 | **Java utvecklare** Om programmering är din passi... | x |  |  | 15 | [Javautvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F8PV_jpm_qwT) |
| 3 | Java utvecklare Om **programmering** är din passion och du värdesä... | x | x | 13 | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 4 | ...mer trivas hos oss på ALTEN i **Lund**. Vi söker duktiga  Java utvec... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 5 | ...TEN i Lund. Vi söker duktiga  **Java** utvecklare som tillsammans me... |  | x |  | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 6 | ...TEN i Lund. Vi söker duktiga  **Java utvecklare** som tillsammans med oss vill ... | x |  |  | 15 | [Javautvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F8PV_jpm_qwT) |
| 7 | ...ök idag!  VI ERBJUDER DIG Som **Java** utvecklare har du möjligheten... |  | x |  | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 8 | ...ök idag!  VI ERBJUDER DIG Som **Java utvecklare** har du möjligheten att arbeta... | x |  |  | 15 | [Javautvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F8PV_jpm_qwT) |
| 9 | ... tycker vi det är viktigt med **balans** mellan arbete och fritid, där... | x |  |  | 6 | [Balans, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y3fx_8wR_Zim) |
| 10 | ...ersonalcykel Friskvårdsbidrag **Kollektivavtal**, tjänstepension och försäkrin... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 11 | ...ringar    KONSULTROLLEN  Som **Java** utvecklare hos oss blir du en... |  | x |  | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 12 | ...ringar    KONSULTROLLEN  Som **Java utvecklare** hos oss blir du en del av vår... | x |  |  | 15 | [Javautvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F8PV_jpm_qwT) |
| 13 | ...sitter olika kompetenser inom **IT**. Du kommer att ha varierande ... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 14 | ...m, energi, verkstadsindustri, **läkemedel** och medicinteknik, där våra k... | x | x | 9 | 9 | [Läkemedel/Sjukvårdsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/vVXb_pYT_E1z) |
| 15 | ...kstadsindustri, läkemedel och **medicinteknik**, där våra konsulter är verksa... |  | x |  | 13 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 16 | ...ndivid och arbetar aktivt med **personlig utveckling**.  Vi söker dig som trivs att ... | x | x | 20 | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 17 | ...ngar. Vi ser också att du som **Java**-utvecklare har följande färdi... | x |  |  | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 18 | ...ngar. Vi ser också att du som **Java-utvecklare** har följande färdigheter:   K... |  | x |  | 15 | [Javautvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F8PV_jpm_qwT) |
| 19 | ...följande färdigheter:   Krav: **Java** Spring eller Spring boot Micr... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 20 | ...ler Spring boot Microservices **PostgreSQL** Git JPA eller Hibernate  Meri... | x | x | 10 | 10 | [PostgreSQL, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EXbL_wQe_tGp) |
| 21 | ...boot Microservices PostgreSQL **Git** JPA eller Hibernate  Meritera... | x | x | 3 | 3 | [Git, versionshanteringssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/2REh_Drn_nsh) |
| 22 | ... Microservices PostgreSQL Git **JPA** eller Hibernate  Meriterande:... | x | x | 3 | 3 | [JPA, Java Persistence API, ramverk, **skill**](http://data.jobtechdev.se/taxonomy/concept/VYpX_5pE_eSz) |
| 23 | ...ices PostgreSQL Git JPA eller **Hibernate**  Meriterande: Azure, AWS elle... | x | x | 9 | 9 | [Hibernate, ramverk/Java, ramverk, **skill**](http://data.jobtechdev.se/taxonomy/concept/pR2V_H8A_TGg) |
| 24 | ...eller Hibernate  Meriterande: **Azure**, AWS eller Google cloud platt... | x | x | 5 | 5 | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| 25 | ...ibernate  Meriterande: Azure, **AWS** eller Google cloud plattform ... | x | x | 3 | 3 | [Amazon Web Services (AWS), molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/KRDJ_nhm_wLb) |
| 26 | ... eller Google cloud plattform **React.js** HTML DevOps Kubernetes Flytan... | x |  |  | 8 | [React native, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/TdnY_1gB_9pZ) |
| 27 | ...ogle cloud plattform React.js **HTML** DevOps Kubernetes Flytande ku... | x | x | 4 | 4 | [HTML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/K8eq_sEz_eXV) |
| 28 | ...cloud plattform React.js HTML **DevOps** Kubernetes Flytande kunskaper... | x |  |  | 6 | [DevOps, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Bxx7_d5h_9qS) |
| 29 | ...bernetes Flytande kunskaper i **svenska** i tal och skrift  Urvalet gör... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 30 | ...a, Talent Acquisition Partner **IT** på yana.zubrytska@alten.se  O... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 31 | ...v Europas största teknik- och **IT**-konsultföretag med över 45 00... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 32 | ...Energi, Flyg och Försvar samt **Life Science**. I Sverige är vi över 1300 en... | x |  |  | 12 | [Life science, **keyword**](http://data.jobtechdev.se/taxonomy/concept/CpFD_aoS_pkZ) |
| 33 | ... Försvar samt Life Science. I **Sverige** är vi över 1300 engagerade me... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 34 | ...engagerade medarbetare med 11 **kontor** i 10 städer - från Lund i söd... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 35 | ... 11 kontor i 10 städer - från **Lund** i söder till Skellefteå i nor... |  | x |  | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 36 | ...äder - från Lund i söder till **Skellefteå** i norr. Välkommen in att läsa... |  | x |  | 10 | [Skellefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kicB_LgH_2Dk) |
| | **Overall** | | | **108** | **281** | 108/281 = **38%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Git, versionshanteringssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/2REh_Drn_nsh) |
| x |  |  | [DevOps, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Bxx7_d5h_9qS) |
| x |  |  | [Life science, **keyword**](http://data.jobtechdev.se/taxonomy/concept/CpFD_aoS_pkZ) |
| x | x | x | [PostgreSQL, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EXbL_wQe_tGp) |
| x | x | x | [Javautvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F8PV_jpm_qwT) |
| x | x | x | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [HTML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/K8eq_sEz_eXV) |
| x | x | x | [Amazon Web Services (AWS), molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/KRDJ_nhm_wLb) |
|  | x |  | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| x |  |  | [React native, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/TdnY_1gB_9pZ) |
| x | x | x | [JPA, Java Persistence API, ramverk, **skill**](http://data.jobtechdev.se/taxonomy/concept/VYpX_5pE_eSz) |
| x |  |  | [Balans, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y3fx_8wR_Zim) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x | x | x | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
|  | x |  | [Skellefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kicB_LgH_2Dk) |
| x | x | x | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| x | x | x | [Hibernate, ramverk/Java, ramverk, **skill**](http://data.jobtechdev.se/taxonomy/concept/pR2V_H8A_TGg) |
| x | x | x | [Läkemedel/Sjukvårdsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/vVXb_pYT_E1z) |
| x | x | x | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **15** | 15/24 = **62%** |