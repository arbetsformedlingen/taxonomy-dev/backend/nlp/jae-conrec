# Results for '3cc3b17ec336e06974d52cd998e4bb069a9ddf9d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [3cc3b17ec336e06974d52cd998e4bb069a9ddf9d](README.md) | 1 | 3161 | 18 | 28 | 152/404 = **38%** | 9/24 = **38%** |

## Source text

Systemtestare Vi är ett globalt team som är enade runt ett gemensamt mål; att skapa avancerade teknologiska lösningar för världens vattenutmaningar. Vårt arbete är inriktat på att utveckla nya tekniker som förbättrar hur vatten används, bevaras och återanvänds i framtiden. Våra produkter och tjänster förflyttar, behandlar, analyserar, övervakar och återför vatten till miljön för allmännyttiga företag, industri, bostäder och kommersiella byggnader. Xylem är också ledande inom smart mätutrustning, nätverksteknik och avancerade analytiska instrument för vatten-, el och gas. Vi har starka långvariga relationer med kunder i över 150 länder som känner oss genom vår starka kombination av ledande varumärken och applikationsexpertis med en kraftig inriktning på att utveckla mångsidiga, hållbara lösningar. Systemtestare I rollen som systemtestare blir du en del av systemintegrationstest teamet som testar att våra produkter fungerar tillsammans. Våra produkter är bland annat pumpar, mixrar, controller, monitorer, cloudtjänster samt olika typer av sensorer. Teamet är en del av avdelningen systems & validation som tillsammans ser till att våra produkter fungerar tillsammans på ett bra sätt och effektivt sätt.  Teamet består idag av fem personer med en gedigen erfarenhet samt en ständig vilja att utvecklas. Därför söker teamet någon som kommer med nya idéer och synvinklar. Rollen har en stor variation av uppgifter som bland annat test, testutveckling, testautomatisering och rigbyggnation. Så det är en fördel om du som person gillar att prova på nya saker. Detta eftersom att teamet testar att våra produkter funderar ihop innan det testas skarpt hos kund. Arbetsuppgifterna är varierande då du kommer att arbeta med mjukvaran men även ha mer praktiska inslag genom fysiska tester i vårt laboratorium. Exempel på arbetsuppgifter är: Testautomatisering (Främst i Python) Bygga testriggar Designa testfall samt granskning av teamets testfall Dokumentation och administration kring testresultat Aktivt deltagande i teamets planering    Vem är du Vi söker dig som som är intresserad av test och teknik. Du behöver även ha: Utbildning som civilingenjör/högskoleingenjör eller motsvarande erfarenheter Har kunskaper i programmering (Gärna Python och PLC programmering) Har ett genuint intresse för teknik Har goda kunskaper i både svenska och engelska.    Som person är du: Problemlösande Noggrann Initiativtagade    Allas lika värde Vi vill att människor, oavsett hudfärg, ålder, funktionshinder, sexuell läggning och könsidentitet ska känna sig hemma, inte bara på Xylem, utan överallt i världen. För oss innebär mångfald mer än bara en policy eller en uppsättning metoder. Mångfald är en grundläggande del av vår företagskultur och en nyckel till långsiktigt tillväxt. Besök oss gärna på https://www.xylem.com/sv-se/ Ansökan Vi tar endast emot ansökningar via denna länk,  https://www.xylem.com/en-us/careers/ . Vänligen kontakta Leila Sayyad, Corporate Recruiter, Leila.sayyad@xylem.com om du vill veta mer om rollen. Välkommen med din ansökan!! Fackrepresentant är Mikael Lövås, Unionen, och Martin Larsson, Sveriges Ingenjörer, tel: + 46 8 475 60 00.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Systemtestare** Vi är ett globalt team som är... |  | x |  | 13 | [Systemtestare och testledare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/D9SL_mtn_vGM) |
| 1 | **Systemtestare** Vi är ett globalt team som är... | x | x | 13 | 13 | [Systemtestare/Funktionstestare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ZtTD_4VC_f4i) |
| 1 | **Systemtestare** Vi är ett globalt team som är... |  | x |  | 13 | [Systemtestare och testledare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/oBNT_dDT_Wbu) |
| 2 | ...m är också ledande inom smart **mätutrustning**, nätverksteknik och avancerad... |  | x |  | 13 | [använda traditionell mätutrustning för vattendjup, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7Ur6_r9Q_EEB) |
| 2 | ...m är också ledande inom smart **mätutrustning**, nätverksteknik och avancerad... |  | x |  | 13 | [utveckla mätutrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/AeiK_WdF_Fy6) |
| 2 | ...m är också ledande inom smart **mätutrustning**, nätverksteknik och avancerad... |  | x |  | 13 | [montera mätutrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BSEY_soh_dVF) |
| 2 | ...m är också ledande inom smart **mätutrustning**, nätverksteknik och avancerad... |  | x |  | 13 | [använda mätutrustning för synundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Qs3Z_L4Q_jjT) |
| 3 | ...nde inom smart mätutrustning, **nätverksteknik** och avancerade analytiska ins... |  | x |  | 14 | [Annan utbildning i elektronik, datateknik och automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/3tq2_f1E_Lyh) |
| 3 | ...nde inom smart mätutrustning, **nätverksteknik** och avancerade analytiska ins... |  | x |  | 14 | [nätverkstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/gsaM_vT7_oJu) |
| 4 | ...nik och avancerade analytiska **instrument** för vatten-, el och gas. Vi h... |  | x |  | 10 | [Instrument, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/wLYs_CKS_wK8) |
| 5 | ...ngsidiga, hållbara lösningar. **Systemtestare** I rollen som systemtestare bl... |  | x |  | 13 | [Systemtestare och testledare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/D9SL_mtn_vGM) |
| 5 | ...ngsidiga, hållbara lösningar. **Systemtestare** I rollen som systemtestare bl... | x | x | 13 | 13 | [Systemtestare/Funktionstestare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ZtTD_4VC_f4i) |
| 5 | ...ngsidiga, hållbara lösningar. **Systemtestare** I rollen som systemtestare bl... |  | x |  | 13 | [Systemtestare och testledare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/oBNT_dDT_Wbu) |
| 6 | ...r. Systemtestare I rollen som **systemtestare** blir du en del av systeminteg... |  | x |  | 13 | [Systemtestare och testledare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/D9SL_mtn_vGM) |
| 6 | ...r. Systemtestare I rollen som **systemtestare** blir du en del av systeminteg... | x | x | 13 | 13 | [Systemtestare/Funktionstestare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ZtTD_4VC_f4i) |
| 6 | ...r. Systemtestare I rollen som **systemtestare** blir du en del av systeminteg... |  | x |  | 13 | [Systemtestare och testledare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/oBNT_dDT_Wbu) |
| 7 | ...stemtestare blir du en del av **systemintegrationstest** teamet som testar att våra pr... | x |  |  | 22 | [Systemintegrationstestare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/cq1S_yzi_bCm) |
| 8 | ...r bland annat pumpar, mixrar, **controller**, monitorer, cloudtjänster sam... |  | x |  | 10 | [Controller, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Uw4n_UB2_RCW) |
| 9 | ...d annat test, testutveckling, **testautomatisering** och rigbyggnation. Så det är ... | x | x | 18 | 18 | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| 10 | ...xempel på arbetsuppgifter är: **Testautomatisering** (Främst i Python) Bygga testr... | x | x | 18 | 18 | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| 11 | ... Testautomatisering (Främst i **Python**) Bygga testriggar Designa tes... | x | x | 6 | 6 | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| 12 | ...ranskning av teamets testfall **Dokumentation** och administration kring test... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 13 | ...h teknik. Du behöver även ha: **Utbildning** som civilingenjör/högskoleing... | x |  |  | 10 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 14 | ...höver även ha: Utbildning som **civilingenjör**/högskoleingenjör eller motsva... |  | x |  | 13 | [Civilingenjörsyrken, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/4jZs_twz_epW) |
| 14 | ...höver även ha: Utbildning som **civilingenjör**/högskoleingenjör eller motsva... | x |  |  | 13 | [Civilingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/idCW_nKR_Xnm) |
| 15 | ... Utbildning som civilingenjör/**högskoleingenjör** eller motsvarande erfarenhete... | x | x | 16 | 16 | [Högskoleingenjörsexamen, **skill**](http://data.jobtechdev.se/taxonomy/concept/Rf1n_6Sj_exu) |
| 16 | ... erfarenheter Har kunskaper i **programmering** (Gärna Python och PLC program... | x | x | 13 | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 17 | ...skaper i programmering (Gärna **Python** och PLC programmering) Har et... | x | x | 6 | 6 | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| 18 | ...ogrammering (Gärna Python och **PLC** programmering) Har ett genuin... | x |  |  | 3 | [Programmerbart styrsystem/PLC, **skill**](http://data.jobtechdev.se/taxonomy/concept/Uytu_hWi_5nh) |
| 19 | ...mmering (Gärna Python och PLC **programmering**) Har ett genuint intresse för... | x |  |  | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 20 | ...nik Har goda kunskaper i både **svenska** och engelska.    Som person ä... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 21 | ... kunskaper i både svenska och **engelska**.    Som person är du: Problem... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 22 | ... person är du: Problemlösande **Noggrann** Initiativtagade    Allas lika... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| | **Overall** | | | **152** | **404** | 152/404 = **38%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Annan utbildning i elektronik, datateknik och automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/3tq2_f1E_Lyh) |
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
|  | x |  | [Civilingenjörsyrken, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/4jZs_twz_epW) |
|  | x |  | [använda traditionell mätutrustning för vattendjup, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7Ur6_r9Q_EEB) |
|  | x |  | [utveckla mätutrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/AeiK_WdF_Fy6) |
|  | x |  | [montera mätutrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BSEY_soh_dVF) |
|  | x |  | [Systemtestare och testledare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/D9SL_mtn_vGM) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [använda mätutrustning för synundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Qs3Z_L4Q_jjT) |
| x | x | x | [Högskoleingenjörsexamen, **skill**](http://data.jobtechdev.se/taxonomy/concept/Rf1n_6Sj_exu) |
|  | x |  | [Controller, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Uw4n_UB2_RCW) |
| x |  |  | [Programmerbart styrsystem/PLC, **skill**](http://data.jobtechdev.se/taxonomy/concept/Uytu_hWi_5nh) |
| x | x | x | [Systemtestare/Funktionstestare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ZtTD_4VC_f4i) |
| x |  |  | [Systemintegrationstestare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/cq1S_yzi_bCm) |
|  | x |  | [nätverkstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/gsaM_vT7_oJu) |
| x | x | x | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x |  |  | [Civilingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/idCW_nKR_Xnm) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
|  | x |  | [Systemtestare och testledare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/oBNT_dDT_Wbu) |
| x | x | x | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
|  | x |  | [Instrument, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/wLYs_CKS_wK8) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **9** | 9/24 = **38%** |