# Results for 'db9c1b997e197772a5ddb73ce15d52cb85a0076e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [db9c1b997e197772a5ddb73ce15d52cb85a0076e](README.md) | 1 | 3250 | 11 | 10 | 53/238 = **22%** | 5/10 = **50%** |

## Source text

Undersköterska Ingsta Vård- och Omsorgsboende ligger i Rogsta, ca 1 mil från centrala Hudiksvall. Ingsta har 28 boendeplatser och även korttidsplatser/växelboende. Det finns en härlig utemiljö som lockar till avkoppling och återhämtning. Våra mål är att skapa en trygg miljö med god omvårdnad där vi är lyhörda för boende och anhörigas önskemål och behov.   Vi finns här för människor. Vi erbjuder olika typer av stöd till barn, unga, familjer och vuxna som på något sätt har behov av våra insatser. Vi ger personer det stöd som behövs för att sedan av egen kraft komma vidare till arbete, studier eller på annat sätt få hjälp för att utvecklas. Vi tar hand om våra gamla när de behöver stöd i sin vardag. Vi är en av få kommuner i Sverige som har en samlad socialtjänst. Det innebär att myndighetsutövning samt behandlande och stödjande insatser finns i en och samma förvaltning. Det ser vi som en enorm styrka. Vi skapar värde för våra kommuninvånare och bidrar till ett gott och värdigt liv – för alla, oavsett förutsättningar. Vi lägger stor vikt vid tidiga insatser och förebyggande arbete, professionellt och individuellt anpassat stöd. Vi är en lärande arbetsplats - Vi gör varandra bättre genom vårt kvalitetsarbete med fokus på dem vi är till för. Vi är social- och omsorgsförvaltningen – en del av Hudiksvalls kommun.     ARBETSUPPGIFTER Sedvanliga arbetsuppgifter för en undersköterska på ett vård- och omsorgsboende. Arbetspass är schemalagda dag och kväll.  KVALIFIKATIONER Utbildad undersköterska För att passa och trivas i rollen behöver du ha ett positivt förhållningssätt och har lätt för att samarbeta med andra. Du är också: - Flexibel - Ansvarstagande - Har bra bemötande - Lugn och trygg Vi lägger stor vikt vid personlig lämplighet.  God kunskap att hantera datasystem och dokumentation är meriterande. Du behöver ha goda kunskaper i det svenska språket, både i tal och skrift.  ÖVRIGT Mitt i Sverige, vid den vackra hälsingekusten, ligger Hudiksvalls kommun - eller kanske mer känt som Glada Hudik. Här lever 37 600 glada hudikbor, och mer än hälften av oss bor på landsbygden.  I Hudiksvalls kommun är avstånden korta till det mesta man vill göra och det man behöver, och du får tid över till det som är viktigt i livet. Här finns närhet till kontrastrika miljöer, ett aktivt föreningsliv och ett näringsliv med stor handlingskraft - många världsledande företag har sitt säte i vår kommun.  Hudiksvalls kommun är den största arbetsgivaren i kommunen. Tillsammans är vi 3 800 medarbetare inom 300 olika yrken som varje dag jobbar för att skapa värde för dem vi är till för - våra kommuninvånare. Det är ett stort och viktigt uppdrag som ska genomsyras av trygghet, öppenhet, delaktighet och jämlikhet. På så sätt hjälps vi åt att bygga ett samhälle där alla människor är lika mycket värda.  Läs mer om vad vi som arbetsgivare kan erbjuda dig: www.hudiksvall.se/jobbahososs  I och med att du ansöker om denna tjänst lämnar du också ditt samtycke till att Hudiksvalls kommun behandlar dina uppgifter i enlighet med Dataskyddsförordningen (GDPR). Läs mer om hur vi behandlar dina personuppgifter: https://www.hudiksvall.se/Ovrigt/Om-personuppgifter.html  Observera att vi endast tar emot ansökningar via www.offentligajobb.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Undersköterska** Ingsta Vård- och Omsorgsboend... |  | x |  | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 2 | ...ogsta, ca 1 mil från centrala **Hudiksvall**. Ingsta har 28 boendeplatser ... | x | x | 10 | 10 | [Hudiksvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Utks_mwF_axY) |
| 3 | ... skapa en trygg miljö med god **omvårdnad** där vi är lyhörda för boende ... | x | x | 9 | 9 | [Omvårdnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/MR4H_FFT_mTj) |
| 4 | ...ag. Vi är en av få kommuner i **Sverige** som har en samlad socialtjäns... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 5 | ...orgsförvaltningen – en del av **Hudiksvalls kommun**.     ARBETSUPPGIFTER Sedvan... | x |  |  | 18 | [Hudiksvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Utks_mwF_axY) |
| 6 | ...anliga arbetsuppgifter för en **undersköterska** på ett vård- och omsorgsboend... |  | x |  | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 7 | ...anliga arbetsuppgifter för en **undersköterska på ett vård- och omsorgsboende**. Arbetspass är schemalagda da... | x |  |  | 45 | [Undersköterska, hemtjänst, äldreboende och habilitering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3QA_5pk_uXd) |
| 8 | ....  KVALIFIKATIONER Utbildad **undersköterska** För att passa och trivas i ro... |  | x |  | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 8 | ....  KVALIFIKATIONER Utbildad **undersköterska** För att passa och trivas i ro... | x |  |  | 14 | [Undersköterska, hemtjänst, äldreboende och habilitering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3QA_5pk_uXd) |
| 9 | ...ingssätt och har lätt för att **samarbeta med andra**. Du är också: - Flexibel - An... | x |  |  | 19 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 10 | ...ra. Du är också: - Flexibel - **Ansvarstagande** - Har bra bemötande - Lugn oc... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 11 | ...ap att hantera datasystem och **dokumentation** är meriterande. Du behöver ha... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 12 | ...höver ha goda kunskaper i det **svenska** språket, både i tal och skrif... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 13 | ...höver ha goda kunskaper i det **svenska språket**, både i tal och skrift.  ÖVR... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 14 | ...l och skrift.  ÖVRIGT Mitt i **Sverige**, vid den vackra hälsingekuste... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 15 | ...vackra hälsingekusten, ligger **Hudiksvalls kommun** - eller kanske mer känt som G... | x |  |  | 18 | [Hudiksvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Utks_mwF_axY) |
| | **Overall** | | | **53** | **238** | 53/238 = **22%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| x | x | x | [Omvårdnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/MR4H_FFT_mTj) |
| x | x | x | [Hudiksvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Utks_mwF_axY) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Undersköterska, hemtjänst, äldreboende och habilitering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3QA_5pk_uXd) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/10 = **50%** |