# Results for '0d6b629f5ea77d17886e50f2cf63a67a58cea158'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0d6b629f5ea77d17886e50f2cf63a67a58cea158](README.md) | 1 | 5304 | 31 | 32 | 217/595 = **36%** | 19/38 = **50%** |

## Source text

Tillsynshandläggare till byggavdelningen Nu söker vi efter en tillsynshandläggare till byggavdelningen som ska jobba med att utreda och handlägga tillsynsärenden.  Välkommen med din ansökan till ett spännande arbete och en arbetsplats med underbar sammanhållning!  Det här erbjuder vi dig! Som tillsynshandläggare hos oss du kommer att självständigt arbeta med utredning och handläggning av tillsynsärenden enligt plan- och bygglagen. Du kommer även att handlägga ansökningar om dispens från strandskydd enligt Miljöbalken samt andra på kontoret förekommande arbetsuppgifter. Arbetet innebär att du kommer att ha daglig kontakt med allmänheten, byggherrar och konsulter och vara rådgivande inom gällande lagstiftning. Du kommer även att vara föredragande i egna ärenden på miljö- och stadsbyggnadsnämndens arbetsutskott och sammanträden.   Tjänsten är en tillsvidareanställning med start snarast eller efter överenskommelse. Arbetsplatsen är på Stadshuset, en tidstypisk 70-talsbyggnad belägen centralt på Nytorget 1 i Hässleholm, med närhet till Centralstationens utmärkta kommunikationsmöjligheter. Vi har flextid och vi erbjuder möjligheten att arbeta på distans i viss mån, när verksamheten så tillåter.  Hässleholms kommun vill skapa de bästa förutsättningarna för ett hållbart arbetsliv, för att du ska trivas, må bra och känna arbetsglädje. En viktig del är att erbjuda bra förmåner till våra medarbetare som till exempel friskvårdsbidrag och möjlighet till byte av semesterdagstillägg mot lediga dagar.  Vem är du? Du har ingenjörs-, arkitekt-, juristutbildning eller annan likvärdig högskoleutbildning eller yrkeshögskoleutbildning, till exempel bygglovshandläggare. Du har kunskaper om plan- och bygglagen, plan- och byggförordningen, miljöbalken och Boverkets byggregler. Vi tror att det är en fördel om du har erfarenhet av till exempel utredningsarbete som handläggare från annan offentligt likvärdig verksamhet.  All kommunikation i myndighetsutövning, muntlig och skriftlig, sker på svenska, det är därför viktigt att du är bra på att formulera krav och lagtext på ett lättförståeligt sätt så mottagaren förstår vad som krävs. Du behöver också ha god datorvana då vår ärendehantering är helt digital.  Vi söker dig som är samarbetsinriktad, lyhörd, engagerad, kvalitets- och resultatmedveten och som kan arbeta effektivt med alla typer av ärenden inom avdelningens ansvarsområde. Du ska kunna arbeta självständigt med dina egna ärenden men även bidra positivt till avdelningens och förvaltningens gemensamma arbete.  Du har ett gott bemötande, är serviceinriktad, noggrann och tar eget ansvar för ditt arbete. Arbetet medför stor del personlig kontakt med sökande och allmänhet och det är viktigt att du har lätt för att hålla dig objektiv i alla situationer. Engagemang för tillämpning av aktuell lagstiftning och att du är en god kommunikatör av myndighetskrav på ett sakligt och lättförståeligt sätt är viktigt för en utredare av tillsynsärenden.  För jobbet behöver du B-körkort.  Du delar värderingar och förhållningssätt som beskrivs i https://www.hassleholm.se/jobb-och-foretagande/jobba-hos-oss/medarbetar--och-ledarpolicy.html  Övrigt Vill du veta mer om hur det är att jobba på vår avdelning? Ring gärna Henry Åhs, vår juridiska expert som bland annat jobbar med tillsynsärenden eller Katarina Finyak som är avdelningschef för bygglov. Vi har semester delar av vecka 28 samt vecka 32 och kan då inte nås. Kontaktuppgifter hittar du längre ned.  Vi arbetar med kompetensbaserad och fördomsfri rekrytering vilket innebär att vi i denna rekrytering valt att ta bort det personliga brevet ur rekryteringsprocessen. När du söker jobbet kommer du att svara på ett antal urvalsfrågor kring bland annat hur väl du uppfyller kraven för jobbet, vad du kan bidra till och ditt intresse för jobbet. Varmt välkommen med din ansökan!  Om oss Miljö- och stadsbyggnadsförvaltningen är kommunens och miljö- och stadsbyggnadsnämndens verkställande funktion för planering, GIS-verksamhet och uppföljning av all fysisk verksamhet som berör markanvändning och byggande i kommunen. Vi ansvarar för tillsyn och rådgivning inom miljöbalksområdet och inom områdena livsmedel, tobak och receptfria läkemedel samt förmedlar kunskap om energieffektivisering. Förvaltningen är indelad i sex avdelningar: miljö, plan, bygg, mät- och GIS, lantmäteri samt verksamhetsstöd. Idag arbetar cirka 70 personer på miljö- och stadsbyggnadsförvaltningen. En bra arbetsmiljö och delaktighet för alla är en självklarhet för oss. Att värna om våra medarbetare och ge bra service har prioritet i vår vardag.  Byggavdelningen består av nio medarbetare med olika kompetenser. Avdelningen arbetar med lov, anmälan, förhandsbesked, strandskyddsdispenser samt all tillsyn enligt plan- och bygglagen. Vi ansvarar även för rådgivning till allmänhet, företag och andra förvaltningar inom gällande lagstiftning. Arbetet sker i stor utsträckning digitalt och vi strävar hela tiden efter att utveckla och förbättra våra interna processer.  Hässleholms kommun är en viktig knutpunkt i Sverige. Här möts människor för att leva, uppleva, arbeta och lära i en kommun präglad av nyfikenhet och utveckling. Våra medarbetare skapar kvalité som syns, visar respekt för individen och sätter medborgaren i fokus.   Välkommen att bli en av oss!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...läggare hos oss du kommer att **självständigt arbeta** med utredning och handläggnin... | x |  |  | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 2 | ...ing av tillsynsärenden enligt **plan- och** bygglagen. Du kommer även att... |  | x |  | 9 | [Plan- och bygglagen, PBL, **skill**](http://data.jobtechdev.se/taxonomy/concept/otrX_HCb_Vqx) |
| 3 | ...ning. Du kommer även att vara **föredragande** i egna ärenden på miljö- och ... | x |  |  | 12 | [Föredragande/Beredningsjurist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pfiK_oXH_eYR) |
| 4 | ...ammanträden.   Tjänsten är en **tillsvidareanställning** med start snarast eller efter... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 5 | ...ägen centralt på Nytorget 1 i **Hässleholm**, med närhet till Centralstati... | x | x | 10 | 10 | [Hässleholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bP5q_53x_aqJ) |
| 6 | ...h vi erbjuder möjligheten att **arbeta på distans** i viss mån, när verksamheten ... | x |  |  | 17 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 7 | ...är verksamheten så tillåter.  **Hässleholms kommun** vill skapa de bästa förutsätt... | x |  |  | 18 | [Hässleholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bP5q_53x_aqJ) |
| 8 | ...sättningarna för ett hållbart **arbetsliv**, för att du ska trivas, må br... | x | x | 9 | 9 | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| 9 | ...iga dagar.  Vem är du? Du har **ingenjörs**-, arkitekt-, juristutbildning... | x |  |  | 9 | [Ingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/SNvJ_i6V_DZH) |
| 10 | ...Vem är du? Du har ingenjörs-, **arkitekt**-, juristutbildning eller anna... | x |  |  | 8 | [Arkitekt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XMWR_V22_WS1) |
| 11 | ...Du har ingenjörs-, arkitekt-, **juristutbildning** eller annan likvärdig högskol... |  | x |  | 16 | [Juristutbildning inklusive fullgjord notarietjänstgöring, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UvmV_2KC_mtp) |
| 11 | ...Du har ingenjörs-, arkitekt-, **juristutbildning** eller annan likvärdig högskol... |  | x |  | 16 | [Juristutbildning (juristexamen), inklusive fullgjord notarietjänstgöring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/g6MJ_kcM_FLn) |
| 11 | ...Du har ingenjörs-, arkitekt-, **juristutbildning** eller annan likvärdig högskol... | x | x | 16 | 16 | [Juristutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/m6JS_nJx_Pdz) |
| 11 | ...Du har ingenjörs-, arkitekt-, **juristutbildning** eller annan likvärdig högskol... |  | x |  | 16 | [Juristutbildning (juristexamen), **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/xXVm_KH1_iGc) |
| 12 | ...ildning eller annan likvärdig **högskoleutbildning** eller yrkeshögskoleutbildning... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 12 | ...ildning eller annan likvärdig **högskoleutbildning** eller yrkeshögskoleutbildning... | x |  |  | 18 | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| 13 | ...rdig högskoleutbildning eller **yrkeshögskoleutbildning**, till exempel bygglovshandläg... | x |  |  | 23 | [Högskoleutbildning, yrkesinriktad, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/zdKB_pTL_HMS) |
| 14 | ...skoleutbildning, till exempel **bygglovshandläggare**. Du har kunskaper om plan- oc... |  | x |  | 19 | [Utbildning till bygglovshandläggare, **keyword**](http://data.jobtechdev.se/taxonomy/concept/8TCp_ehn_EhH) |
| 14 | ...skoleutbildning, till exempel **bygglovshandläggare**. Du har kunskaper om plan- oc... | x | x | 19 | 19 | [Bygglovshandläggare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GZ59_Q7K_6qB) |
| 15 | ...dläggare. Du har kunskaper om **plan- och** bygglagen, plan- och byggföro... | x | x | 9 | 9 | [Plan- och bygglagen, PBL, **skill**](http://data.jobtechdev.se/taxonomy/concept/otrX_HCb_Vqx) |
| 16 | ... Du har kunskaper om plan- och** bygglagen**, plan- och byggförordningen, ... | x |  |  | 10 | [Plan- och bygglagen, PBL, **skill**](http://data.jobtechdev.se/taxonomy/concept/otrX_HCb_Vqx) |
| 17 | ...en, miljöbalken och Boverkets **byggregler**. Vi tror att det är en fördel... |  | x |  | 10 | [Bygg, **occupation-collection**](http://data.jobtechdev.se/taxonomy/concept/ja7J_P8X_YC9) |
| 18 | ...ar erfarenhet av till exempel **utredningsarbete** som handläggare från annan of... |  | x |  | 16 | [Utredningsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/QAaJ_QeY_Q6P) |
| 19 | ...untlig och skriftlig, sker på **svenska**, det är därför viktigt att du... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 20 | ...digital.  Vi söker dig som är **samarbetsinriktad**, lyhörd, engagerad, kvalitets... | x | x | 17 | 17 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 21 | ...s ansvarsområde. Du ska kunna **arbeta självständigt** med dina egna ärenden men äve... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 22 | ...emötande, är serviceinriktad, **noggrann** och tar eget ansvar för ditt ... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 23 | ...tiftning och att du är en god **kommunikatör** av myndighetskrav på ett sakl... | x | x | 12 | 12 | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| 24 | ...åeligt sätt är viktigt för en **utredare** av tillsynsärenden.  För jobb... |  | x |  | 8 | [Handläggare, offentlig förvaltning/Utredare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YG1s_tUg_jWJ) |
| 25 | ...enden.  För jobbet behöver du **B-körkort**.  Du delar värderingar och fö... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 26 | ...yak som är avdelningschef för **bygglov**. Vi har semester delar av vec... | x | x | 7 | 7 | [Bygglov, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4oH_peY_AMJ) |
| 27 | ...lande funktion för planering, **GIS**-verksamhet och uppföljning av... | x |  |  | 3 | [Geografiskt informationssystem/GIS, **skill**](http://data.jobtechdev.se/taxonomy/concept/CWfM_RL9_V4t) |
| 28 | ...l fysisk verksamhet som berör **markanvändning** och byggande i kommunen. Vi a... |  | x |  | 14 | [ge råd om markanvändning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ujJa_Ze5_Xte) |
| 28 | ...l fysisk verksamhet som berör **markanvändning** och byggande i kommunen. Vi a... |  | x |  | 14 | [Hållbar markanvändning, **skill**](http://data.jobtechdev.se/taxonomy/concept/uw7i_bUK_eex) |
| 28 | ...l fysisk verksamhet som berör **markanvändning** och byggande i kommunen. Vi a... |  | x |  | 14 | [markanvändning vid flygplatsplanering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xCpA_A5j_KW1) |
| 28 | ...l fysisk verksamhet som berör **markanvändning** och byggande i kommunen. Vi a... | x | x | 14 | 14 | [Markanvändning, **skill**](http://data.jobtechdev.se/taxonomy/concept/yK4r_ToR_zg7) |
| 29 | ...alksområdet och inom områdena **livsmedel**, tobak och receptfria läkemed... | x | x | 9 | 9 | [Livsmedel, **skill**](http://data.jobtechdev.se/taxonomy/concept/tdbu_fUL_MnC) |
| 30 | ...områdena livsmedel, tobak och **receptfria läkemedel** samt förmedlar kunskap om ene... | x | x | 20 | 20 | [receptfria läkemedel, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/y4cM_Tfq_ntG) |
| 31 | ...del samt förmedlar kunskap om **energieffektivisering**. Förvaltningen är indelad i s... | x |  |  | 21 | [Energieffektiviseringsingenjörer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/iytn_JSh_MUJ) |
| 32 | ...: miljö, plan, bygg, mät- och **GIS**, lantmäteri samt verksamhetss... | x | x | 3 | 3 | [Geografiskt informationssystem/GIS, **skill**](http://data.jobtechdev.se/taxonomy/concept/CWfM_RL9_V4t) |
| 33 | ...jö, plan, bygg, mät- och GIS, **lantmäteri** samt verksamhetsstöd. Idag ar... | x | x | 10 | 10 | [lantmäteri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zkyk_hbC_LHg) |
| 34 | ...byggnadsförvaltningen. En bra **arbetsmiljö** och delaktighet för alla är e... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 35 | ...enser samt all tillsyn enligt **plan- och** bygglagen. Vi ansvarar även f... |  | x |  | 9 | [Plan- och bygglagen, PBL, **skill**](http://data.jobtechdev.se/taxonomy/concept/otrX_HCb_Vqx) |
| 36 | ...ttra våra interna processer.  **Hässleholms kommun** är en viktig knutpunkt i Sver... | x |  |  | 18 | [Hässleholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bP5q_53x_aqJ) |
| 37 | ...mmun är en viktig knutpunkt i **Sverige**. Här möts människor för att l... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **217** | **595** | 217/595 = **36%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Utbildning till bygglovshandläggare, **keyword**](http://data.jobtechdev.se/taxonomy/concept/8TCp_ehn_EhH) |
| x | x | x | [Geografiskt informationssystem/GIS, **skill**](http://data.jobtechdev.se/taxonomy/concept/CWfM_RL9_V4t) |
| x | x | x | [Bygglov, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4oH_peY_AMJ) |
| x | x | x | [Bygglovshandläggare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GZ59_Q7K_6qB) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Utredningsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/QAaJ_QeY_Q6P) |
| x |  |  | [Ingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/SNvJ_i6V_DZH) |
| x | x | x | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
|  | x |  | [Juristutbildning inklusive fullgjord notarietjänstgöring, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UvmV_2KC_mtp) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Arkitekt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XMWR_V22_WS1) |
|  | x |  | [Handläggare, offentlig förvaltning/Utredare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YG1s_tUg_jWJ) |
| x | x | x | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| x | x | x | [Hässleholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bP5q_53x_aqJ) |
|  | x |  | [Juristutbildning (juristexamen), inklusive fullgjord notarietjänstgöring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/g6MJ_kcM_FLn) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Energieffektiviseringsingenjörer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/iytn_JSh_MUJ) |
|  | x |  | [Bygg, **occupation-collection**](http://data.jobtechdev.se/taxonomy/concept/ja7J_P8X_YC9) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x | x | x | [Juristutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/m6JS_nJx_Pdz) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Plan- och bygglagen, PBL, **skill**](http://data.jobtechdev.se/taxonomy/concept/otrX_HCb_Vqx) |
| x |  |  | [Föredragande/Beredningsjurist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pfiK_oXH_eYR) |
| x | x | x | [Livsmedel, **skill**](http://data.jobtechdev.se/taxonomy/concept/tdbu_fUL_MnC) |
| x |  |  | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
|  | x |  | [ge råd om markanvändning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ujJa_Ze5_Xte) |
|  | x |  | [Hållbar markanvändning, **skill**](http://data.jobtechdev.se/taxonomy/concept/uw7i_bUK_eex) |
| x |  |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
|  | x |  | [markanvändning vid flygplatsplanering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xCpA_A5j_KW1) |
|  | x |  | [Juristutbildning (juristexamen), **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/xXVm_KH1_iGc) |
| x | x | x | [receptfria läkemedel, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/y4cM_Tfq_ntG) |
| x | x | x | [Markanvändning, **skill**](http://data.jobtechdev.se/taxonomy/concept/yK4r_ToR_zg7) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x |  |  | [Högskoleutbildning, yrkesinriktad, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/zdKB_pTL_HMS) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| x | x | x | [lantmäteri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zkyk_hbC_LHg) |
| | | **19** | 19/38 = **50%** |