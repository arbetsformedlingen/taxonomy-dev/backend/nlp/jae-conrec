# Results for 'ce1638ffc2d7e729a837b516b1a570fe911e5d5f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ce1638ffc2d7e729a837b516b1a570fe911e5d5f](README.md) | 1 | 1587 | 10 | 15 | 58/165 = **35%** | 6/16 = **38%** |

## Source text

Kallskänka / restaurangbiträde sökes!  Stavsjö Krog & Kafé är en familjeägd restaurang, kafé och inredningsbutik. Vi värnar om det personliga mötet med gästerna, god och vällagad mat, hembakade kakor & en hemtrevlig miljö. Vi finns vid E4:an mellan Nyköping och Norrköping. Du tar dig hit med bil (ca 15 minuter från Norrköping och 25 minuter från Nyköping). Titta gärna in på hemsidan eller på Facebook @stavsjokrog för att få en bättre uppfattning om arbetsplatsen. Vi söker dig som har erfarenhet av att jobba med att göra smörgåsar och sallader. Meriterande om du har jobbat inom bageri. Vi vill hitta dig med det lilla extra, du som har det som är så viktigt inom branschen - en känsla för service, tempo i kroppen och som tycker det är roligt att jobba. Du bör också ha en vilja att utvecklas på arbetsplatsen för hos oss finns stora utvecklingsmöjligheter. Tjänsten är på 75 % (med möjlighet att ta extrapass som dyker upp) och innefattar en blandning av morgon, kväll och varannan helg. Vi tar även in extrapersonal som bara vill jobba helger (att du jobbar extra vid sidan av studier tex). Då det är mycket kundkontakt är det viktigt att du pratar både svenska och engelska. För att kunna ta dig till arbetsplatsen behöver du ha körkort och bil. Du jobbar i kallskänken men lägger även upp mat till dagens rätt, står i kassan och har hand om matsalen / borden mm För oss är det väldigt viktigt att ha en positiv inställning och ett leende på läpparna i mötet med gäster och kollegor. Att hålla rent och snyggt omkring sig är en självklarhet och vi är måna om fina uppläggningar.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kallskänka** / restaurangbiträde sökes!  S... | x | x | 10 | 10 | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| 2 | Kallskänka / **restaurangbiträde** sökes!  Stavsjö Krog & Kafé ä... | x | x | 17 | 17 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 3 | ...iträde sökes!  Stavsjö Krog & **Kafé** är en familjeägd restaurang, ... |  | x |  | 4 | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| 4 | ... Krog & Kafé är en familjeägd **restaurang**, kafé och inredningsbutik. Vi... |  | x |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 5 | ...d och vällagad mat, hembakade **kakor** & en hemtrevlig miljö. Vi fin... |  | x |  | 5 | [Tårtor/Bakelser, **skill**](http://data.jobtechdev.se/taxonomy/concept/UVj4_qZp_iqS) |
| 6 | ...jö. Vi finns vid E4:an mellan **Nyköping** och Norrköping. Du tar dig hi... |  | x |  | 8 | [Nyköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KzvD_ePV_DKQ) |
| 7 | ...vid E4:an mellan Nyköping och **Norrköping**. Du tar dig hit med bil (ca 1... |  | x |  | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 8 | ...t med bil (ca 15 minuter från **Norrköping** och 25 minuter från Nyköping)... |  | x |  | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 9 | ...orrköping och 25 minuter från **Nyköping**). Titta gärna in på hemsidan ... |  | x |  | 8 | [Nyköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KzvD_ePV_DKQ) |
| 10 | ...het av att jobba med att göra **smörgåsar** och sallader. Meriterande om ... | x | x | 9 | 9 | [Smörgåsar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ib92_FER_3Dd) |
| 11 | ...ba med att göra smörgåsar och **sallader**. Meriterande om du har jobbat... | x |  |  | 8 | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| 12 | ...terande om du har jobbat inom **bageri**. Vi vill hitta dig med det li... |  | x |  | 6 | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| 12 | ...terande om du har jobbat inom **bageri**. Vi vill hitta dig med det li... | x |  |  | 6 | [Bageriarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/btqh_hCy_bRi) |
| 13 | ...tudier tex). Då det är mycket **kundkontakt** är det viktigt att du pratar ... | x |  |  | 11 | [kommunicera med kunder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sPCy_icd_mxy) |
| 14 | ...et viktigt att du pratar både **svenska** och engelska. För att kunna t... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 15 | ...tt du pratar både svenska och **engelska**. För att kunna ta dig till ar... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 16 | ...l arbetsplatsen behöver du ha **körkort** och bil. Du jobbar i kallskän... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 17 | ... med gäster och kollegor. Att **hålla rent och snyggt** omkring sig är en självklarhe... | x |  |  | 21 | [hålla arbetsområdet rent, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6Kri_s2M_pdm) |
| | **Overall** | | | **58** | **165** | 58/165 = **35%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| x |  |  | [hålla arbetsområdet rent, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6Kri_s2M_pdm) |
| x | x | x | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
|  | x |  | [Nyköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KzvD_ePV_DKQ) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
|  | x |  | [Tårtor/Bakelser, **skill**](http://data.jobtechdev.se/taxonomy/concept/UVj4_qZp_iqS) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Bageriarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/btqh_hCy_bRi) |
|  | x |  | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| x | x | x | [Smörgåsar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ib92_FER_3Dd) |
|  | x |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x |  |  | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| x |  |  | [kommunicera med kunder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sPCy_icd_mxy) |
| x | x | x | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/16 = **38%** |