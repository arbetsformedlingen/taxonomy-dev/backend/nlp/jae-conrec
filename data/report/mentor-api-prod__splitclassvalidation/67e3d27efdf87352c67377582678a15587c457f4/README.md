# Results for '67e3d27efdf87352c67377582678a15587c457f4'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [67e3d27efdf87352c67377582678a15587c457f4](README.md) | 1 | 5709 | 59 | 30 | 129/817 = **16%** | 8/45 = **18%** |

## Source text

Enhetschef för ekonomikonsulttjänster i Stockholm Statens servicecenters uppdrag är att erbjuda effektiv service till medborgare och myndigheter. Vi ansvarar för Sveriges lokala servicekontor där vi ger information och service från Skatteverket, Pensionsmyndigheten, Försäkringskassan och Arbetsförmedlingen. Vi bidrar även till en effektivare statsförvaltning genom att leverera administrativa tjänster och konsultstöd till statliga myndigheter inom ekonomi, lön och HR. Tillsammans är vi cirka 1500 anställda.  Servicekontoren finns över hela landet, från Trelleborg i söder till Kiruna i norr. Vårt huvudkontor finns i Gävle.  Hos oss får du bidra med din kompetens i arbetet för en effektivare statsförvaltning nära medborgaren.   Motiveras du av att utveckla medarbetare och bygga team?  Vi söker nu en enhetschef till vår enhet för ekonomikonsulttjänster i Stockholm. Anställningen är placerad inom avdelningen Ekonomi- och konsulttjänster som har verksamhet i Gävle, Östersund, Stockholm och Sollefteå. Avdelningen ansvarar för alla tjänsteleveranser inom e-handel, redovisning liksom konsulttjänster inom ekonomi och HR.    Du kommer att ansvara för en enhet som levererar redovisnings- och konsulttjänster till omkring 50 myndigheter. I dagsläget finns 16 mycket kompetenta medarbetare på enheten.  Som ledare är du tydlig och trovärdig liksom en bra sparringpartner för enhetens medarbetare och övriga kollegor på avdelningen och inom myndigheten. Vi vill ha en ledare som motiveras av att utveckla sina medarbetare och bygga välfungerande team. Vi jobbar för att implementera, effektivisera och automatisera våra processer inom Statens servicecenter och som chef är det viktigt att driva standardisering med fokus på både effektivitet och kundnytta.  Huvudsakliga arbetsuppgifter  Som chef förväntas du fokusera medarbetarna och teamet och på så sätt skapa kvalitet i leveranser och utveckling av verksamheten. Processerna måste ständigt både utvärderas och utvecklas för att optimera nyttan av att samla statsförvaltningens redovisning hos Statens servicecenter. Du leder och fördelar arbetet och är leveransansvarig för ett antal kunder. Som chef för en konsultverksamhet har du också ansvar för att i nära dialog med kunder:   • kunna matcha konsulter med uppdrag • hitta lösningar som skapar nöjda kunder som samtidigt är utvecklande för medarbetarna.  En viktig aspekt i rollen är att ha god insikt i det statliga regelverket för att tillsammans med teamet kunna hitta effektiva lösningar. Tillsammans med kollegor inom avdelningen och från andra delar av divisionen säkerställer du en samordnad leverans av tjänster till Statens servicecenters kunder, vilket innebär stort fokus på samverkan både internt och med kund. Du har personal- och budgetansvar för enheten. Enhetschefen rapporterar till avdelningschefen och ingår i avdelningens ledningsgrupp, som består av totalt nio enhetschefer.  Kvalifikationer:  Vi söker dig som har:   • Relevant akademisk examen inom ekonomi eller motsvarande utbildning som arbetsgivaren bedömer likvärdig • Minst tre års erfarenhet som chef med personalansvar • Erfarenhet från konsultverksamhet i roller som säljansvarig, konsult och/eller kundansvarig • Drivit förändringar och arbetat med strukturerat processarbete • Arbetat med kvalificerad redovisning och uppföljning i en större organisation  Vi ser att du har följande förmågor   • God ledarskapsförmåga; där du är tydlig, motiverar, skapar engagemang och delaktighet • Systematisk och strukturerad • Kommunikativ och uttrycker dig väl på svenska i tal och skrift • Utvecklingsorienterad och trivs med att arbeta i en dynamisk och föränderlig miljö • God samarbetsförmåga  Dessutom är det meriterande om du har   • Erfarenhet av arbete i statlig verksamhet • Erfarenhet av servicecenterverksamhet inom det administrativa området • Genomgått Statens controllerutbildning  Statens servicecenters uppdrag är att erbjuda effektiv service till medborgare och myndigheter. Vi ansvarar för Sveriges lokala servicekontor där vi ger information och service från Skatteverket, Pensionsmyndigheten, Försäkringskassan och Arbetsförmedlingen. Vi bidrar även till en effektivare statsförvaltning genom att leverera administrativa tjänster och konsultstöd till statliga myndigheter inom ekonomi, lön och HR. Tillsammans är vi cirka 1500 anställda. Servicekontoren finns över hela landet, från Trelleborg i söder till Kiruna i norr. Vårt huvudkontor finns i Gävle. Hos oss får du bidra med din kompetens i arbetet för en effektivare statsförvaltning nära medborgaren.  Övrigt  Anställningen är 100 % och tillsvidareanställning. Vi tillämpar sex månaders provanställning.  Tjänsten är placerad i Stockholm Tillsättning enligt överenskommelse.  Välkommen med din ansökan innehållande CV samt personligt brev senast 14 augusti 2022.   De första intervjuerna planeras att hållas v. 33-34.   För att du ska få en så rättvis och korrekt bedömning som möjligt är det av stor vikt att du uppger år och månad för varje anställning samt en kort beskrivning av arbetsuppgifterna. Beskriv även tydligt hur du uppfyller kvalifikationerna för tjänsten. Om den informationen inte framgår kan vi inte göra en korrekt bedömning av din kompetens och erfarenhet och du kan därför riskera att bli bortvald i urvalsprocessen.  Statens servicecenter verkar för en diskrimineringsfri arbetsplats med ökad mångfald och jämställdhet. Vi har gjort medieval för våra annonser och undanber kontakter från medie- och rekryteringsföretag.   Statens servicecenter verkar för en diskrimineringsfri arbetsplats med ökad mångfald och jämställdhet.  Vi har gjort medieval för våra annonser och undanber kontakter från medie- och rekryteringsföretag.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Enhetschef** för ekonomikonsulttjänster i ... | x |  |  | 10 | [Enhetschef, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Noad_FWq_mY2) |
| 2 | ... för ekonomikonsulttjänster i **Stockholm** Statens servicecenters uppdra... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 3 | ... myndigheter. Vi ansvarar för **Sveriges** lokala servicekontor där vi g... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ...everket, Pensionsmyndigheten, **Försäkringskassan** och Arbetsförmedlingen. Vi bi... | x |  |  | 17 | [Försäkringskassan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/DusR_LM5_tog) |
| 5 | ...gheten, Försäkringskassan och **Arbetsförmedlingen**. Vi bidrar även till en effek... | x |  |  | 18 | [Arbetsförmedlingen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Mb2o_h3H_uGd) |
| 6 | ...drar även till en effektivare **statsförvaltning** genom att leverera administra... | x |  |  | 16 | [Statlig förvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/X9Kc_p8a_1FB) |
| 7 | ...a administrativa tjänster och **konsultstöd** till statliga myndigheter ino... | x |  |  | 11 | [konsultation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DR2h_oKb_D1Y) |
| 8 | ...ill statliga myndigheter inom **ekonomi**, lön och HR. Tillsammans är v... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 9 | ...iga myndigheter inom ekonomi, **lön** och HR. Tillsammans är vi cir... | x |  |  | 3 | [Lönesystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/hjUM_GZe_Bgd) |
| 10 | ...igheter inom ekonomi, lön och **HR**. Tillsammans är vi cirka 1500... | x |  |  | 2 | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
| 11 | ... finns över hela landet, från **Trelleborg** i söder till Kiruna i norr. V... | x |  |  | 10 | [Trelleborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/STvk_dra_M1X) |
| 12 | ... från Trelleborg i söder till **Kiruna** i norr. Vårt huvudkontor finn... | x |  |  | 6 | [Kiruna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/biN6_UiL_Qob) |
| 13 | ...orr. Vårt huvudkontor finns i **Gävle**.  Hos oss får du bidra med di... | x | x | 5 | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 14 | ...h bygga team?  Vi söker nu en **enhetschef** till vår enhet för ekonomikon... | x |  |  | 10 | [Enhetschef, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Noad_FWq_mY2) |
| 15 | ...enhetschef till vår enhet för **ekonomikonsult**tjänster i Stockholm. Anställn... | x |  |  | 14 | [Ekonomikonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/x6oS_7T1_8Rh) |
| 16 | ... för ekonomikonsulttjänster i **Stockholm**. Anställningen är placerad in... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 17 | ... är placerad inom avdelningen **Ekonomi**- och konsulttjänster som har ... | x |  |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 18 | ...inom avdelningen Ekonomi- och **konsulttjänster** som har verksamhet i Gävle, Ö... | x |  |  | 15 | [konsultation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DR2h_oKb_D1Y) |
| 19 | ...tjänster som har verksamhet i **Gävle**, Östersund, Stockholm och Sol... | x | x | 5 | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 20 | ...r som har verksamhet i Gävle, **Östersund**, Stockholm och Sollefteå. Avd... | x | x | 9 | 9 | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| 21 | ...erksamhet i Gävle, Östersund, **Stockholm** och Sollefteå. Avdelningen an... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 22 | ...vle, Östersund, Stockholm och **Sollefteå**. Avdelningen ansvarar för all... | x |  |  | 9 | [Sollefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/v5y4_YPe_TMZ) |
| 23 | ...r alla tjänsteleveranser inom **e-handel**, redovisning liksom konsulttj... | x |  |  | 8 | [E-handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yxpW_N32_YMK) |
| 24 | ...nsteleveranser inom e-handel, **redovisning** liksom konsulttjänster inom e... |  | x |  | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| 24 | ...nsteleveranser inom e-handel, **redovisning** liksom konsulttjänster inom e... | x |  |  | 11 | [redovisning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aAbn_jKY_u2g) |
| 25 | ... e-handel, redovisning liksom **konsulttjänster** inom ekonomi och HR.    Du ko... | x |  |  | 15 | [konsultation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DR2h_oKb_D1Y) |
| 26 | ...g liksom konsulttjänster inom **ekonomi** och HR.    Du kommer att ansv... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 27 | ...sulttjänster inom ekonomi och **HR**.    Du kommer att ansvara för... | x |  |  | 2 | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
| 28 | ...ra för en enhet som levererar **redovisnings**- och konsulttjänster till omk... | x |  |  | 12 | [Redovisningskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CeoS_Wzo_uz5) |
| 29 | ...m levererar redovisnings- och **konsulttjänster** till omkring 50 myndigheter. ... | x |  |  | 15 | [konsultation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DR2h_oKb_D1Y) |
| 30 | ...  Som ledare är du tydlig och **trovärdig** liksom en bra sparringpartner... |  | x |  | 9 | [agera trovärdigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nmJj_j6j_efT) |
| 31 | ...dsakliga arbetsuppgifter  Som **chef** förväntas du fokusera medarbe... | x |  |  | 4 | [Personal- och HR-chefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Agy9_ifj_dB6) |
| 32 | ... optimera nyttan av att samla **statsförvaltningens** redovisning hos Statens servi... | x |  |  | 19 | [Statlig förvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/X9Kc_p8a_1FB) |
| 33 | ...att samla statsförvaltningens **redovisning** hos Statens servicecenter. Du... |  | x |  | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| 33 | ...att samla statsförvaltningens **redovisning** hos Statens servicecenter. Du... | x |  |  | 11 | [redovisning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aAbn_jKY_u2g) |
| 34 | ...r och fördelar arbetet och är **leveransansvarig** för ett antal kunder. Som che... |  | x |  | 16 | [IT-leveransansvarig, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3AaC_tvH_7u1) |
| 35 | ... internt och med kund. Du har **personal**- och budgetansvar för enheten... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 36 | ... internt och med kund. Du har **personal- och **budgetansvar för enheten. Enhe... |  | x |  | 14 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| 37 | ...ed kund. Du har personal- och **budgetansvar** för enheten. Enhetschefen rap... | x | x | 12 | 12 | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| 38 | ...ker dig som har:   • Relevant **akademisk examen** inom ekonomi eller motsvarand... |  | x |  | 16 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 38 | ...ker dig som har:   • Relevant **akademisk examen** inom ekonomi eller motsvarand... |  | x |  | 16 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 39 | ...m har:   • Relevant akademisk **examen inom ekonomi** eller motsvarande utbildning ... | x |  |  | 19 | [Annan utbildning i industriell ekonomi och organisation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/m3Go_x9t_6xP) |
| 40 | ...elevant akademisk examen inom **ekonomi** eller motsvarande utbildning ... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 41 | ...ren bedömer likvärdig • Minst **tre års erfarenhet** som chef med personalansvar •... | x |  |  | 18 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 42 | ...e års erfarenhet som chef med **personalansvar** • Erfarenhet från konsultverk... | x | x | 14 | 14 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| 43 | ...sonalansvar • Erfarenhet från **konsultverksamhet** i roller som säljansvarig, ko... | x |  |  | 17 | [Konsulttjänster till företag, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/APkH_QHR_9X2) |
| 44 | ...onsultverksamhet i roller som **säljansvarig**, konsult och/eller kundansvar... | x |  |  | 12 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 45 | ...et i roller som säljansvarig, **konsult** och/eller kundansvarig • Driv... | x |  |  | 7 | [konsultation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DR2h_oKb_D1Y) |
| 46 | ...ljansvarig, konsult och/eller **kundansvarig** • Drivit förändringar och arb... | x |  |  | 12 | [kundansvarig, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/HHvS_eLn_VKV) |
| 47 | ...te • Arbetat med kvalificerad **redovisning** och uppföljning i en större o... |  | x |  | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| 47 | ...te • Arbetat med kvalificerad **redovisning** och uppföljning i en större o... | x |  |  | 11 | [redovisning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aAbn_jKY_u2g) |
| 48 | ...har följande förmågor   • God **ledarskapsförmåga**; där du är tydlig, motiverar,... |  | x |  | 17 | [Ledarskapsförmåga - Verkställande direktör (VD), **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/CfLz_Aex_PkG) |
| 48 | ...har följande förmågor   • God **ledarskapsförmåga**; där du är tydlig, motiverar,... |  | x |  | 17 | [ledarförmåga, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/SVVU_BC5_mQe) |
| 48 | ...har följande förmågor   • God **ledarskapsförmåga**; där du är tydlig, motiverar,... |  | x |  | 17 | [Ledarskapsförmåga - E-commerce manager, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/VevS_VKB_fpn) |
| 48 | ...har följande förmågor   • God **ledarskapsförmåga**; där du är tydlig, motiverar,... |  | x |  | 17 | [Ledarskapsförmåga - Butiksledare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/dXwA_wgg_k8x) |
| 49 | ...ativ och uttrycker dig väl på **svenska** i tal och skrift • Utveckling... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 50 | ...k och föränderlig miljö • God **samarbetsförmåga**  Dessutom är det meriterande ... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 51 | ...ar   • Erfarenhet av arbete i **statlig verksamhet** • Erfarenhet av servicecenter... |  | x |  | 18 | [samordna statlig verksamhet i utländska institutioner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fkuz_A7j_vLq) |
| 52 | ...a området • Genomgått Statens **controller**utbildning  Statens servicecen... | x |  |  | 10 | [Controller, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GrZU_ofe_QAx) |
| 53 | ...everket, Pensionsmyndigheten, **Försäkringskassan** och Arbetsförmedlingen. Vi bi... | x |  |  | 17 | [Försäkringskassan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/DusR_LM5_tog) |
| 54 | ...gheten, Försäkringskassan och **Arbetsförmedlingen**. Vi bidrar även till en effek... | x |  |  | 18 | [Arbetsförmedlingen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Mb2o_h3H_uGd) |
| 55 | ...ill statliga myndigheter inom **ekonomi**, lön och HR. Tillsammans är v... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 56 | ...iga myndigheter inom ekonomi, **lön** och HR. Tillsammans är vi cir... | x |  |  | 3 | [Lönesystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/hjUM_GZe_Bgd) |
| 57 | ...igheter inom ekonomi, lön och **HR**. Tillsammans är vi cirka 1500... | x |  |  | 2 | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
| 58 | ... finns över hela landet, från **Trelleborg** i söder till Kiruna i norr. V... | x |  |  | 10 | [Trelleborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/STvk_dra_M1X) |
| 59 | ... från Trelleborg i söder till **Kiruna** i norr. Vårt huvudkontor finn... | x |  |  | 6 | [Kiruna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/biN6_UiL_Qob) |
| 60 | ...orr. Vårt huvudkontor finns i **Gävle**. Hos oss får du bidra med din... | x | x | 5 | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 61 | ...en.  Övrigt  Anställningen är **100 %** och tillsvidareanställning. V... | x |  |  | 5 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 62 | ...t  Anställningen är 100 % och **tillsvidareanställning**. Vi tillämpar sex månaders pr... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 63 | ...ning.  Tjänsten är placerad i **Stockholm** Tillsättning enligt överensko... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 64 | ...ringsfri arbetsplats med ökad **mångfald** och jämställdhet. Vi har gjor... | x |  |  | 8 | [stödja kulturell mångfald, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ARp9_7Lk_uhS) |
| 65 | ...tsplats med ökad mångfald och **jämställdhet**. Vi har gjort medieval för vå... | x | x | 12 | 12 | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| 66 | ...ringsfri arbetsplats med ökad **mångfald** och jämställdhet.  Vi har gjo... | x |  |  | 8 | [stödja kulturell mångfald, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ARp9_7Lk_uhS) |
| 67 | ...tsplats med ökad mångfald och **jämställdhet**.  Vi har gjort medieval för v... | x | x | 12 | 12 | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| | **Overall** | | | **129** | **817** | 129/817 = **16%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [IT-leveransansvarig, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3AaC_tvH_7u1) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Konsulttjänster till företag, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/APkH_QHR_9X2) |
| x |  |  | [stödja kulturell mångfald, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ARp9_7Lk_uhS) |
| x |  |  | [Personal- och HR-chefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Agy9_ifj_dB6) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Redovisningskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CeoS_Wzo_uz5) |
|  | x |  | [Ledarskapsförmåga - Verkställande direktör (VD), **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/CfLz_Aex_PkG) |
| x |  |  | [konsultation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DR2h_oKb_D1Y) |
| x |  |  | [Försäkringskassan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/DusR_LM5_tog) |
|  | x |  | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| x |  |  | [Controller, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GrZU_ofe_QAx) |
| x | x | x | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| x |  |  | [kundansvarig, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/HHvS_eLn_VKV) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| x |  |  | [Arbetsförmedlingen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Mb2o_h3H_uGd) |
| x |  |  | [Enhetschef, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Noad_FWq_mY2) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| x |  |  | [Trelleborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/STvk_dra_M1X) |
|  | x |  | [ledarförmåga, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/SVVU_BC5_mQe) |
|  | x |  | [Ledarskapsförmåga - E-commerce manager, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/VevS_VKB_fpn) |
| x | x | x | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| x | x | x | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Statlig förvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/X9Kc_p8a_1FB) |
| x |  |  | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
| x | x | x | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| x |  |  | [redovisning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aAbn_jKY_u2g) |
| x |  |  | [Kiruna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/biN6_UiL_Qob) |
|  | x |  | [Ledarskapsförmåga - Butiksledare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/dXwA_wgg_k8x) |
|  | x |  | [samordna statlig verksamhet i utländska institutioner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fkuz_A7j_vLq) |
| x |  |  | [Lönesystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/hjUM_GZe_Bgd) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x |  |  | [Annan utbildning i industriell ekonomi och organisation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/m3Go_x9t_6xP) |
|  | x |  | [agera trovärdigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nmJj_j6j_efT) |
| x | x | x | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| x |  |  | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| x |  |  | [Sollefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/v5y4_YPe_TMZ) |
| x |  |  | [Ekonomikonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/x6oS_7T1_8Rh) |
| x |  |  | [E-handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yxpW_N32_YMK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/45 = **18%** |