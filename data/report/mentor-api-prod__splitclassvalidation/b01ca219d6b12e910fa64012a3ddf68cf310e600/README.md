# Results for 'b01ca219d6b12e910fa64012a3ddf68cf310e600'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b01ca219d6b12e910fa64012a3ddf68cf310e600](README.md) | 1 | 1904 | 11 | 6 | 26/133 = **20%** | 2/10 = **20%** |

## Source text

Kock till Axevalla folkhögskola Axevalla folkhögskola är känd för sin goda och varierande mat. Hos oss kan man äta frukost, lunch och ibland middag på vardagarna samt festmiddagar på beställning. Matgästerna består av skolans internatelever, personal och kortkursdeltagare och antalet kan variera mellan ett tjugotal och flera hundra per dag. Som arbetsgivare värnar vi om alla våra medarbetare och erbjuder bl.a. kompetensutveckling och friskvårdsbidrag. Vi är en demokratisk arbetsplats och här har du goda möjligheter att utveckla vår verksamhet. Nu söker vi en engagerad kock som på ett självständigt sätt och i god laganda med kollegorna kan möta gäster och ge bästa service.  ARBETSUPPGIFTER Du kommer att arbeta som kock inom vårt skolkök tillsammans med tre andra personer. Dina arbetsuppgifter består av matberedning och servering. OM ARBETET Denna tillsvidareanställning omfattar 100%  med start i september. Schema: Dagtid, måndag – fredag + helger Vi tillämpar Kommunals kollektivavtal.  KVALIFIKATIONER Du har relevant köksutbildning och/eller relevant arbetslivserfarenhet som arbetsgivaren bedömer likvärdig. Du behärskar svenska språket i såväl tal som skrift, du kan ta till dig skrivna instruktioner och har förmågan att kommunicera med ditt arbetsteam. Du kommer att använda dator i ditt arbete. Du bidrar gärna till ett gott arbetsklimat och du har ett fint bemötande gentemot din omgivning. Initiativtagande och ansvarstagande är viktiga egenskaper samt förmågan att kunna vara flexibel och lösningsfokuserad då verksamheten kräver detta. Stor vikt läggs vid personlig lämplighet. Utdrag från belastningsregister kommer att begäras. ÖVRIGT Skicka in din ansökan redan i dag till info@axevalla.fhsk.se  Ytterligare information om tjänsten: rektor Elina Majakari elina@axevalla.fhsk.se Och Eva Sandell, Kommunal 010-442 86 44 Vill du veta mer om oss? Gå gärna in www.axevalla.fhsk.se !

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kock** till Axevalla folkhögskola Ax... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 2 | Kock till Axevalla **folkhögskola** Axevalla folkhögskola är känd... | x |  |  | 12 | [Folkhögskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WT9H_nAT_wW7) |
| 3 | ...xevalla folkhögskola Axevalla **folkhögskola** är känd för sin goda och vari... | x |  |  | 12 | [Folkhögskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WT9H_nAT_wW7) |
| 4 | ...år av skolans internatelever, **personal** och kortkursdeltagare och ant... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 5 | ...het. Nu söker vi en engagerad **kock** som på ett självständigt sätt... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 6 | ... en engagerad kock som på ett **självständigt** sätt och i god laganda med ko... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 7 | ...FTER Du kommer att arbeta som **kock** inom vårt skolkök tillsammans... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 8 | ...h servering. OM ARBETET Denna **tillsvidareanställning** omfattar 100%  med start i se... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 9 | ...llsvidareanställning omfattar **100%**  med start i september. Schem... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 10 | ...helger Vi tillämpar Kommunals **kollektivavtal**.  KVALIFIKATIONER Du har rele... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 11 | ...dömer likvärdig. Du behärskar **svenska** språket i såväl tal som skrif... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 12 | ...dömer likvärdig. Du behärskar **svenska språket** i såväl tal som skrift, du ka... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 13 | ...givning. Initiativtagande och **ansvarstagande** är viktiga egenskaper samt fö... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| | **Overall** | | | **26** | **133** | 26/133 = **20%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x |  |  | [Folkhögskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WT9H_nAT_wW7) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **2** | 2/10 = **20%** |