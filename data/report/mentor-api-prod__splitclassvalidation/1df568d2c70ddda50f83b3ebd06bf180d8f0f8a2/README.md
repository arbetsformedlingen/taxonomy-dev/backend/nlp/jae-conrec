# Results for '1df568d2c70ddda50f83b3ebd06bf180d8f0f8a2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1df568d2c70ddda50f83b3ebd06bf180d8f0f8a2](README.md) | 1 | 964 | 5 | 11 | 21/80 = **26%** | 2/7 = **29%** |

## Source text

Husfru till Gränsö slott Vi söker dig som brinner för detaljer och ordning och reda!  Som Husfru på Gränsö slott sköter du om den dagliga städningen av slottet, du ser till att våra fönster är putsade och du har ett öga för detaljer. Du tvättar gardiner, putsar ljusstakar och ser till att våra utrymmen alltid skiner och blänker. Du organiserar upp dagen så att den passar verksamheten och du ser till att det är ordning och reda även bakom allmänna utrymmen.  Du behöver vara en person som inte är rädd för att ta i eller att strukturera upp saker. Du är självgående och tar egna initiativ utifrån verksamhetens behov. Du behöver ha god fysik och god planeringsförmåga. I gengäld får du arbeta på en fantastisk miljö och med trevliga och glada kollegor. Du kommer arbeta dagtid och vardagar. Enstaka helger kan förekomma vid behov.  Vi ser även att du har tidigare erfarenhet av liknande arbete och vi söker en person som ser detta som en långsiktig arbetsplats.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Husfru** till Gränsö slott Vi söker di... |  | x |  | 6 | [Städledare och husfruar, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/FtyZ_Joo_tB8) |
| 1 | **Husfru** till Gränsö slott Vi söker di... |  | x |  | 6 | [Städledare och husfruar, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/KoFE_X9a_VgB) |
| 1 | **Husfru** till Gränsö slott Vi söker di... |  | x |  | 6 | [Husmor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XjSL_FeM_gVc) |
| 1 | **Husfru** till Gränsö slott Vi söker di... | x | x | 6 | 6 | [Arbetsledare, städ/Husfru/Städledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cYP6_Tur_q6m) |
| 1 | **Husfru** till Gränsö slott Vi söker di... |  | x |  | 6 | [Husfar, **job-title**](http://data.jobtechdev.se/taxonomy/concept/eM7d_B2j_4U9) |
| 2 | ...er och ordning och reda!  Som **Husfru** på Gränsö slott sköter du om ... |  | x |  | 6 | [Städledare och husfruar, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/FtyZ_Joo_tB8) |
| 2 | ...er och ordning och reda!  Som **Husfru** på Gränsö slott sköter du om ... |  | x |  | 6 | [Städledare och husfruar, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/KoFE_X9a_VgB) |
| 2 | ...er och ordning och reda!  Som **Husfru** på Gränsö slott sköter du om ... |  | x |  | 6 | [Husmor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XjSL_FeM_gVc) |
| 2 | ...er och ordning och reda!  Som **Husfru** på Gränsö slott sköter du om ... | x | x | 6 | 6 | [Arbetsledare, städ/Husfru/Städledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cYP6_Tur_q6m) |
| 2 | ...er och ordning och reda!  Som **Husfru** på Gränsö slott sköter du om ... |  | x |  | 6 | [Husfar, **job-title**](http://data.jobtechdev.se/taxonomy/concept/eM7d_B2j_4U9) |
| 3 | ... strukturera upp saker. Du är **självgående** och tar egna initiativ utifrå... | x |  |  | 11 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 4 | ...amhetens behov. Du behöver ha **god fysik** och god planeringsförmåga. I ... | x | x | 9 | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| | **Overall** | | | **21** | **80** | 21/80 = **26%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Städledare och husfruar, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/FtyZ_Joo_tB8) |
|  | x |  | [Städledare och husfruar, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/KoFE_X9a_VgB) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Husmor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XjSL_FeM_gVc) |
| x | x | x | [Arbetsledare, städ/Husfru/Städledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cYP6_Tur_q6m) |
|  | x |  | [Husfar, **job-title**](http://data.jobtechdev.se/taxonomy/concept/eM7d_B2j_4U9) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| | | **2** | 2/7 = **29%** |