# Results for 'd41a066b2dfc3a2177b67d93c7cc70da2711f91a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d41a066b2dfc3a2177b67d93c7cc70da2711f91a](README.md) | 1 | 4856 | 33 | 34 | 93/666 = **14%** | 4/34 = **12%** |

## Source text

Intendent till offentlig konst Eskilstuna konstmuseum Kulturverksamheten i Eskilstuna är bred och mångsidig. Målet är att det ska finnas möjligheter för alla till eget skapande, att uttrycka sig i olika former och att ta del av professionellt skapad kultur.  Eskilstuna konstmuseum är beläget i Munktellstaden. I den gamla industribyggnaden, med anor från 1817 finns flera rum för utställningar, pedagogisk verkstad, samt butik och reception. Museets samling består av cirka 6200 verk från 1600-talet fram till i dag, med tyngdpunkten på svenskt 1900-talsmåleri. Utöver samlingen visas tillfälliga utställningar med samtida konst. Visningar och konstpedagogisk verksamhet erbjuds kontinuerligt för både skolor och allmänhet. Konstmuseet ansvarar även för den offentliga konsten i kommunen liksom för kommunens lånekonst. På konstmuseet finns även Svenskt barnbildarkiv. Eskilstuna konstmuseum ingår i enheten Eskilstuna konstmuseer, som förutom museet, den offentliga konsten och Svenskt barnbildarkiv även inbegriper Ebelingmuseet.  ARBETSUPPGIFTER Vi söker en intendent för offentlig konst med goda kunskaper om konstnärlig gestaltning och gestaltad livsmiljö. Du kommer ingå i Eskilstuna konstmuseums personalgrupp och arbeta med konstnärliga gestaltningsprojekt i samverkan med verksamheter som t.ex. skola, vård och omsorg, fastighetsbolag, stadsbyggnad, ung fritid och mötesplatser, konstnärer liksom andra aktörer inom konstområdet. I Eskilstuna kommun avsätts medel för konstnärlig gestaltning både av kultur- och fritidsnämnden och Eskilstuna Kommunfastighet AB (1 % - regeln).  Tjänsten innebär också förvaltning av den offentliga konstsamlingen i kommunen, med bland annat inventering och underhållsarbete.  Vi ser gärna att du vill vara med och utveckla verksamheten tillsammans med oss.  Huvudsakliga arbetsuppgifter: • Planering och projektledning av nya konstnärliga gestaltningsuppdrag. • Leda samrådsgrupper för pågående uppdrag. • Utformning av konstprogram. • Förvaltning och inköp av offentlig konst. Budgetplanering och uppföljning gentemot ansvarig chef. • Administration som hantering av fakturor, avtalshantering, diarieföring och dokumentation.  KVALIFIKATIONER Vi söker dig som uppfyller följande krav:  • Högskoleexamen inom konst- och kulturområdet eller motsvarande som arbetsgivaren finner likvärdig för uppdraget. • Erfarenhet av projektledande arbete.  Goda kunskaper om konstnärlig gestaltning och gestaltad livsmiljö.  Meriterande är om du har: • Erfarenhet av förvaltning av konst. • Kännedom om offentlig upphandling. • Erfarenhet av att arbeta i offentlig sektor. • B-körkort  För tjänsten krävs det att du kan leda och driva projekt, nätverka, skapa och upprätthålla relationer med olika externa aktörer och samarbetspartners. Det är viktigt att du kan arbeta självständigt, men också att kunna arbeta i team, både internt och extern. Du bör ha ett stort intresse för konstnärliga frågeställningar i utvecklingen av offentliga rum samt konstens roll i samhället. I tjänsten behövs kontinuerlig omvärldsbevakning. Vi ser att du har god kännedom om och brett kontaktnät inom samtidskonsten. Förutom att vara driven och att utveckla och hitta möjligheter inom området bör du vara noggrann, strukturerad, ha god administrativ förmåga, flexibel och lätt för att anpassa dig till ändrade omständigheter.  Låter det här intressant? Välkommen med din ansökan!  ÖVRIGT Kultur- och fritidsförvaltningens uppdrag är att skapa förutsättningar för goda livsvillkor för alla Eskilstunas invånare och besökare genom att erbjuda ett brett kulturutbud och möjligheter till en stimulerande fritid. Vi ansvarar för kommunens kulturskola, bibliotek, mötesplatser, arkiv, museer, fritidsgårdar, föreningsstöd samt arenor för kultur, idrott och friluftsliv.   Förvaltningen har ett förvaltningskontor och tre enheter: område arenor och friluftsliv, område Ung fritid och mötesplatser samt område kultur.   Vill du vara med och leda vägen?  Vi erbjuder en arbetsplats som går före och leder vägen i en av Sveriges mest hållbara kommuner. Tillsammans med kompetenta och engagerade medarbetare är du med och utvecklar Eskilstuna genom goda hållbara möten, vägledning och service. Genom ett aktivt och modigt medarbetarskap samt ett tillitsfullt och engagerat ledarskap är du med och driver förändring. Du gör skillnad för människor varje dag.    Vi välkomnar medarbetare med olika bakgrund och arbetar för att våra arbetsplatser ska vara tillgängliga för alla.   Vi ber dig söka tjänsten via länken i denna annons. Det är viktigt att du är noggrann när du fyller i din ansökan eftersom den ligger till grund för vårt urval.    Om du har skyddad identitet ansöker du genom att skicka in din ansökan skriftligen eller lämna den i vår reception på Alva Myrdals Gata 3 D. Märk din ansökan med Rekryteringsenheten och annonsens referensnummer.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Intendent** till offentlig konst Eskilstu... | x |  |  | 9 | [Museiintendent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DVVd_BqV_umM) |
| 2 | Intendent till offentlig **konst** Eskilstuna konstmuseum Kultur... |  | x |  | 5 | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| 3 | ...ntendent till offentlig konst **Eskilstuna** konstmuseum Kulturverksamhete... | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 4 | ...stmuseum Kulturverksamheten i **Eskilstuna** är bred och mångsidig. Målet ... | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 5 | ...rofessionellt skapad kultur.  **Eskilstuna** konstmuseum är beläget i Munk... | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 6 | ...ar, pedagogisk verkstad, samt **butik** och reception. Museets samlin... |  | x |  | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 7 | ...gisk verkstad, samt butik och **reception**. Museets samling består av ci... |  | x |  | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 8 | ...iga utställningar med samtida **konst**. Visningar och konstpedagogis... |  | x |  | 5 | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| 9 | ...s även Svenskt barnbildarkiv. **Eskilstuna** konstmuseum ingår i enheten E... | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 10 | ...a konstmuseum ingår i enheten **Eskilstuna** konstmuseer, som förutom muse... | x |  |  | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 11 | ... ARBETSUPPGIFTER Vi söker en **intendent** för offentlig konst med goda ... | x |  |  | 9 | [Museiintendent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DVVd_BqV_umM) |
| 12 | ...er en intendent för offentlig **konst** med goda kunskaper om konstnä... |  | x |  | 5 | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| 13 | ...g konst med goda kunskaper om **konstnärlig gestaltning** och gestaltad livsmiljö. Du k... | x |  |  | 23 | [Konstnärlig gestaltning i offentlig miljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/Eqx1_Q87_nV5) |
| 14 | ...d livsmiljö. Du kommer ingå i **Eskilstuna** konstmuseums personalgrupp oc... | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 15 | ... och omsorg, fastighetsbolag, **stadsbyggnad**, ung fritid och mötesplatser,... |  | x |  | 12 | [Stadsbyggnad, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RJAK_cSa_9ud) |
| 16 | ... ung fritid och mötesplatser, **konstnärer** liksom andra aktörer inom kon... |  | x |  | 10 | [Konstnärer, musiker och skådespelare m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/6ZV1_7Pb_ndQ) |
| 16 | ... ung fritid och mötesplatser, **konstnärer** liksom andra aktörer inom kon... |  | x |  | 10 | [Posering för konstnär, **skill**](http://data.jobtechdev.se/taxonomy/concept/E2tb_3Ba_cUF) |
| 16 | ... ung fritid och mötesplatser, **konstnärer** liksom andra aktörer inom kon... |  | x |  | 10 | [konstnär, keramik, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FQDH_Zzq_9tY) |
| 16 | ... ung fritid och mötesplatser, **konstnärer** liksom andra aktörer inom kon... |  | x |  | 10 | [förhandla med konstnärer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XmQU_ESV_Kiu) |
| 16 | ... ung fritid och mötesplatser, **konstnärer** liksom andra aktörer inom kon... |  | x |  | 10 | [Konstnärer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/ax6E_DYS_TSG) |
| 16 | ... ung fritid och mötesplatser, **konstnärer** liksom andra aktörer inom kon... |  | x |  | 10 | [sköta relationer med konstnärer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jTDd_DRk_vL8) |
| 16 | ... ung fritid och mötesplatser, **konstnärer** liksom andra aktörer inom kon... |  | x |  | 10 | [digital konstnär, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qE5h_Ue1_EXE) |
| 16 | ... ung fritid och mötesplatser, **konstnärer** liksom andra aktörer inom kon... |  | x |  | 10 | [Konstnär, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xaLh_X2Y_aAn) |
| 17 | ... aktörer inom konstområdet. I **Eskilstuna** kommun avsätts medel för kons... |  | x |  | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 18 | ...eln).  Tjänsten innebär också **förvaltning av den offentliga konstsamlingen** i kommunen, med bland annat i... | x |  |  | 44 | [hantering av samlingar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FKhh_cdN_yn4) |
| 19 | ...etsuppgifter: • Planering och **projektledning** av nya konstnärliga gestaltni... |  | x |  | 14 | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| 20 | ...ing och projektledning av nya **konstnärliga gestaltningsuppdrag.** • Leda samrådsgrupper för påg... | x |  |  | 33 | [Konstnärlig gestaltning i offentlig miljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/Eqx1_Q87_nV5) |
| 21 | ...ärliga gestaltningsuppdrag. • **Leda samrådsgrupper** för pågående uppdrag. • Utfor... | x |  |  | 19 | [utöva ledarskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DP6Z_cdB_jKo) |
| 22 | ...ende uppdrag. • Utformning av **konstprogram**. • Förvaltning och inköp av o... | x |  |  | 12 | [leda konstnärliga projekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iumb_kmL_C1y) |
| 23 | ...ltning och inköp av offentlig **konst**. Budgetplanering och uppföljn... |  | x |  | 5 | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| 24 | ...och inköp av offentlig konst. **Budgetplanering** och uppföljning gentemot ansv... | x |  |  | 15 | [utarbeta budgetar för konstnärliga projekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/y3r4_jRu_sjo) |
| 25 | ...av fakturor, avtalshantering, **diarieföring** och dokumentation.  KVALIFI... | x |  |  | 12 | [Diarieföringsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/bwjW_1SE_XWZ) |
| 26 | ...lshantering, diarieföring och **dokumentation**.  KVALIFIKATIONER Vi söker ... | x |  |  | 13 | [tillhandahålla dokumentation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5uMi_uQS_9Sd) |
| 26 | ...lshantering, diarieföring och **dokumentation**.  KVALIFIKATIONER Vi söker ... |  | x |  | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 27 | ...m uppfyller följande krav:  • **Högskoleexamen** inom konst- och kulturområdet... | x | x | 14 | 14 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 28 | ... krav:  • Högskoleexamen inom **konst- och kulturområdet** eller motsvarande som arbetsg... | x |  |  | 24 | [Kulturvård, kulturadministration och museivetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/L34L_eis_N5k) |
| 29 | ...ör uppdraget. • Erfarenhet av **projektledande arbete**.  Goda kunskaper om konstnärl... | x |  |  | 21 | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| 30 | ...de arbete.  Goda kunskaper om **konstnärlig gestaltning** och gestaltad livsmiljö.  Mer... | x |  |  | 23 | [Konstnärlig gestaltning i offentlig miljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/Eqx1_Q87_nV5) |
| 31 | ... Erfarenhet av förvaltning av **konst**. • Kännedom om offentlig upph... |  | x |  | 5 | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| 32 | ...tning av konst. • Kännedom om **offentlig upphandling**. • Erfarenhet av att arbeta i... | x |  |  | 21 | [utföra upphandlingsprocesser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DFY6_CRj_i2N) |
| 33 | ... arbeta i offentlig sektor. • **B-körkort**  För tjänsten krävs det att d... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 34 | ...tjänsten krävs det att du kan **leda och driva projekt**, nätverka, skapa och upprätth... | x |  |  | 22 | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| 35 | ...u kan leda och driva projekt, **nätverka**, skapa och upprätthålla relat... | x |  |  | 8 | [utveckla professionella nätverk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fS7r_bUJ_kF3) |
| 36 | ... och driva projekt, nätverka, **skapa och upprätthålla relationer** med olika externa aktörer och... | x |  |  | 33 | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| 37 | ...rs. Det är viktigt att du kan **arbeta självständigt**, men också att kunna arbeta i... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 38 | ...ständigt, men också att kunna **arbeta i team**, både internt och extern. Du ... | x |  |  | 13 | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| 39 | ...eter inom området bör du vara **noggrann**, strukturerad, ha god adminis... |  | x |  | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 40 | ...sstöd samt arenor för kultur, **idrott** och friluftsliv.   Förvaltnin... |  | x |  | 6 | [Idrott, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QV1z_Wdx_HaM) |
| 41 | ...etare är du med och utvecklar **Eskilstuna** genom goda hållbara möten, vä... |  | x |  | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 42 | ...ons. Det är viktigt att du är **noggrann** när du fyller i din ansökan e... |  | x |  | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 43 | ...ftligen eller lämna den i vår **reception** på Alva Myrdals Gata 3 D. Mär... |  | x |  | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| | **Overall** | | | **93** | **666** | 93/666 = **14%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| x |  |  | [tillhandahålla dokumentation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5uMi_uQS_9Sd) |
|  | x |  | [Konstnärer, musiker och skådespelare m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/6ZV1_7Pb_ndQ) |
| x |  |  | [utföra upphandlingsprocesser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DFY6_CRj_i2N) |
| x |  |  | [utöva ledarskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DP6Z_cdB_jKo) |
| x |  |  | [Museiintendent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DVVd_BqV_umM) |
|  | x |  | [Posering för konstnär, **skill**](http://data.jobtechdev.se/taxonomy/concept/E2tb_3Ba_cUF) |
| x |  |  | [Konstnärlig gestaltning i offentlig miljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/Eqx1_Q87_nV5) |
| x |  |  | [hantering av samlingar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FKhh_cdN_yn4) |
|  | x |  | [konstnär, keramik, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FQDH_Zzq_9tY) |
| x |  |  | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| x |  |  | [Kulturvård, kulturadministration och museivetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/L34L_eis_N5k) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
|  | x |  | [Idrott, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QV1z_Wdx_HaM) |
|  | x |  | [Stadsbyggnad, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RJAK_cSa_9ud) |
| x |  |  | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [förhandla med konstnärer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XmQU_ESV_Kiu) |
|  | x |  | [Konstnärer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/ax6E_DYS_TSG) |
| x |  |  | [Diarieföringsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/bwjW_1SE_XWZ) |
| x |  |  | [utveckla professionella nätverk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fS7r_bUJ_kF3) |
| x |  |  | [leda konstnärliga projekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iumb_kmL_C1y) |
|  | x |  | [sköta relationer med konstnärer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jTDd_DRk_vL8) |
|  | x |  | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x | x | x | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| x | x | x | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
|  | x |  | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
|  | x |  | [digital konstnär, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qE5h_Ue1_EXE) |
|  | x |  | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
|  | x |  | [Konstnär, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xaLh_X2Y_aAn) |
| x |  |  | [utarbeta budgetar för konstnärliga projekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/y3r4_jRu_sjo) |
|  | x |  | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| | | **4** | 4/34 = **12%** |