# Results for '923510140d7ce2ffdc5618fa84d80d1512e5d093'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [923510140d7ce2ffdc5618fa84d80d1512e5d093](README.md) | 1 | 930 | 3 | 3 | 10/58 = **17%** | 1/5 = **20%** |

## Source text

Bli en i vårt team! Vi söker nu servispersonal till vårt team på Restaurang, La Terrazza. Du kommer arbeta ihop med ett glatt team som alltid strävar efter att ge våra gäster toppservice varje dag!  La Terrazza är en mötesplats för den som söker sig till det italienska köket. Måltiden hos oss symboliserar som en plats där man samlas med familj och vänner i en genuin miljö.  Arbetsmoral är extremt viktigt hos oss då vi arbetar som ett team. Du är en social person som brinner för service och mötet med människor. Du är förstås punktlig och trivs i en miljö där det är mycket att göra.Vi ser att du har positiv inställning till nya utmaningar och att du tillför en god stämning till teamet och gäster. Det finns goda möjligheter att utvecklas hos oss både som individ & medarbetare.I gengäld ger vi dig kunskap och ett omväxlande arbete.   Tjänsten är mest förlagd helger samt veckodagar (kväll) & röda dagar (dag / kvällstid) .

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...i en i vårt team! Vi söker nu **servispersonal** till vårt team på Restaurang,... | x |  |  | 14 | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| 1 | ...i en i vårt team! Vi söker nu **servispersonal** till vårt team på Restaurang,... |  | x |  | 14 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | ...vispersonal till vårt team på **Restaurang**, La Terrazza. Du kommer arbet... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 3 | ...ör den som söker sig till det **italienska** köket. Måltiden hos oss symbo... |  | x |  | 10 | [Italienskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/T38R_QfY_piB) |
| 3 | ...ör den som söker sig till det **italienska** köket. Måltiden hos oss symbo... | x |  |  | 10 | [Italienska, **language**](http://data.jobtechdev.se/taxonomy/concept/b9cE_KXy_Yxv) |
| | **Overall** | | | **10** | **58** | 10/58 = **17%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Italienskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/T38R_QfY_piB) |
| x |  |  | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| x |  |  | [Italienska, **language**](http://data.jobtechdev.se/taxonomy/concept/b9cE_KXy_Yxv) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
|  | x |  | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| | | **1** | 1/5 = **20%** |