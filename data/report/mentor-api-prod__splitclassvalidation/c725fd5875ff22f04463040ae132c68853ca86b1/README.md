# Results for 'c725fd5875ff22f04463040ae132c68853ca86b1'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c725fd5875ff22f04463040ae132c68853ca86b1](README.md) | 1 | 2118 | 20 | 15 | 147/242 = **61%** | 6/10 = **60%** |

## Source text

Är du vår nästa tandhygienist i Hyltebruk? Kan vi erbjuda dig en riktigt bra tjänst som tandhygienist? Prata med oss!  Om oss Tandvårdscentralen Hylte ligger i mysiga orten Hyltebruk i Hallands län. Kliniken har bedrivit tandvård i över 30 år Vårt team består just nu av tandläkare och tandsköterskor och vi söker nu dig som är tandhygienist. Vi jobbar med allmäntandvård, implantatbehandlingar och en hel del protetik.  Vi söker dig som är legitimerad tandhygienist och tycker om att arbeta på en mindre klinik med korta beslutsvägar.    Varför ska du välja oss? Hos oss får du jobba med ett team bestående av motiverade kollegor, där alla hjälps åt och finns där för varandra. Vår arbetsmiljö är relativt stressfri om man jämför med många andra kliniker och vi har ingen omsättningskrav på dig som tandhygienist då vi förutsätter att du gör ditt bästa och fokuserar hellre på kvalitet än att jobba på rullande band. Våra patienter är lojala, återkommande och litar på oss och du slipper tjafs och strul.  Vidareutveckling är viktigt för oss alla i teamet och vi satsar på oss själva och varandra genom kurser och annat kul tillsammans.  Bra att veta Arbetsgivare: Tandvårdscentralen Hylte Ort: Hyltebruk i Hallands län Anställningsform: Tillsvidareanställning Väldigt bra lön och villkor. Omfattning: Heltid eller deltid enligt ök. Friskvårdsbidrag och bra lönevillkor Journalsystem: Alma Öppettider: Mån - Tors (Kan komma att ändras och enligt dina önskemål)   Ansökan ✅ I denna rekrytering samarbetar Tandvårdscentralen Hylte med  Tandfakta (https://tandfakta.se) för säkerställa att du får den bästa kandidatupplevelsen i rekryteringsprocessen.  Ansök med eller utan CV genom att besvara 3 snabba frågor!  Vid frågor om tjänsten vänligen kontakta:  Linus Zackrisson eller Jashar Samadi Rekryteringsansvarig hej@tandfakta.se 0709 220 253 / 0709220250   Obs! Kliniken tar inte emot ansökningar eller samtal direkt från kandidater då alla sökanden måste hanteras och genomgå samma urvalsprocess via Tandfakta. Detta säkerställer att din och övriga sökande behandlas rättvist, kvalitetssäkert och i enlighet med GDPR.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Är du vår nästa **tandhygienist** i Hyltebruk? Kan vi erbjuda d... | x | x | 13 | 13 | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
| 2 | ...dig en riktigt bra tjänst som **tandhygienist**? Prata med oss!  Om oss Tandv... | x | x | 13 | 13 | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
| 3 | ...enist? Prata med oss!  Om oss **Tandvård**scentralen Hylte ligger i mysi... | x |  |  | 8 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 4 | ...s!  Om oss Tandvårdscentralen **Hylte** ligger i mysiga orten Hyltebr... | x | x | 5 | 5 | [Hylte, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3XMe_nGt_RcU) |
| 5 | ...er i mysiga orten Hyltebruk i **Hallands län**. Kliniken har bedrivit tandvå... | x | x | 12 | 12 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 6 | ...ds län. Kliniken har bedrivit **tandvård** i över 30 år Vårt team består... | x | x | 8 | 8 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 7 | ...r Vårt team består just nu av **tandläkare** och tandsköterskor och vi sök... | x | x | 10 | 10 | [tandläkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/teYK_qML_UE3) |
| 8 | ...tår just nu av tandläkare och **tandsköterskor** och vi söker nu dig som är ta... | x |  |  | 14 | [Tandsköterskor, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/cRLD_LWf_a8p) |
| 8 | ...tår just nu av tandläkare och **tandsköterskor** och vi söker nu dig som är ta... |  | x |  | 14 | [Tandsköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ckkD_JhH_Sc6) |
| 9 | ...or och vi söker nu dig som är **tandhygienist**. Vi jobbar med allmäntandvård... | x | x | 13 | 13 | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
| 10 | ... tandhygienist. Vi jobbar med **allmäntandvård**, implantatbehandlingar och en... | x | x | 14 | 14 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 11 | ... söker dig som är legitimerad **tandhygienist** och tycker om att arbeta på e... | x | x | 13 | 13 | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
| 12 | ...h finns där för varandra. Vår **arbetsmiljö** är relativt stressfri om man ... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 13 | ...en omsättningskrav på dig som **tandhygienist** då vi förutsätter att du gör ... | x | x | 13 | 13 | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
| 14 | ....  Bra att veta Arbetsgivare: **Tandvårds**centralen Hylte Ort: Hyltebruk... | x |  |  | 9 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 15 | ...etsgivare: Tandvårdscentralen **Hylte** Ort: Hyltebruk i Hallands län... | x | x | 5 | 5 | [Hylte, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3XMe_nGt_RcU) |
| 16 | ...tralen Hylte Ort: Hyltebruk i **Hallands län** Anställningsform: Tillsvidare... | x | x | 12 | 12 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 17 | ...allands län Anställningsform: **Tillsvidareanställning** Väldigt bra lön och villkor. ... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 18 | ... lön och villkor. Omfattning: **Heltid eller deltid** enligt ök. Friskvårdsbidrag o... | x |  |  | 19 | (not found in taxonomy) |
| 19 | ... denna rekrytering samarbetar **Tandvårds**centralen Hylte med  Tandfakta... | x |  |  | 9 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 20 | ...samarbetar Tandvårdscentralen **Hylte** med  Tandfakta (https://tandf... | x | x | 5 | 5 | [Hylte, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3XMe_nGt_RcU) |
| | **Overall** | | | **147** | **242** | 147/242 = **61%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Hylte, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3XMe_nGt_RcU) |
| x | x | x | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
| x | x | x | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| x |  |  | [Tandsköterskor, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/cRLD_LWf_a8p) |
|  | x |  | [Tandsköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ckkD_JhH_Sc6) |
| x |  |  | (not found in taxonomy) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [tandläkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/teYK_qML_UE3) |
| x | x | x | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **6** | 6/10 = **60%** |