# Results for '56df2674736aa8dfa308f8e1e05a46255101626e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [56df2674736aa8dfa308f8e1e05a46255101626e](README.md) | 1 | 335 | 5 | 3 | 31/54 = **57%** | 2/2 = **100%** |

## Source text

Stensättare sökes Vi söker nu en duktig stensättare med erfarenhet! /  we are looking for a skilled stonemason with experience vi söker dig som / we are looking for you as  har god fysik/ have good physique är social och positiv / is social and positive  lösnings orienterad / solution-oriented  kan arbeta i grupp / can work in groups

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Stensättare** sökes Vi söker nu en duktig s... | x | x | 11 | 11 | [Stensättare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Xkw4_BSP_dad) |
| 2 | ...e sökes Vi söker nu en duktig **stensättare** med erfarenhet! /  we are loo... | x | x | 11 | 11 | [Stensättare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Xkw4_BSP_dad) |
| 3 | ... we are looking for a skilled **stonemason** with experience vi söker dig ... | x |  |  | 10 | [Stensättare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Xkw4_BSP_dad) |
| 4 | ...e are looking for you as  har **god fysik**/ have good physique är social... | x | x | 9 | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 5 | ...r you as  har god fysik/ have **good physique** är social och positiv / is so... | x |  |  | 13 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| | **Overall** | | | **31** | **54** | 31/54 = **57%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Stensättare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Xkw4_BSP_dad) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| | | **2** | 2/2 = **100%** |