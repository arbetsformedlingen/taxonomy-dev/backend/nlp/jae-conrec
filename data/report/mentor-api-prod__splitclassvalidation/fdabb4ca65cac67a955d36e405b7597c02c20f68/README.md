# Results for 'fdabb4ca65cac67a955d36e405b7597c02c20f68'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [fdabb4ca65cac67a955d36e405b7597c02c20f68](README.md) | 1 | 3476 | 16 | 23 | 63/348 = **18%** | 4/18 = **22%** |

## Source text

Mät- och kartingenjör Vill du arbeta på Sveriges största VA-bolag med spännande projekt och arbetsuppgifter i en lärande miljö? Då ska du söka tjänsten hos oss på enheten Geodata. Du kommer att få arbeta med inmätning och kartering av bolagets VA-nät med tillhörande anläggningar och sopsugar.  Om rollen  Arbetet som Mät- och Kartingenjör innebär att du arbetar både på kontor och ute i fält. Ditt arbete är självständigt, där du ansvarar för olika typer av mät- och kartjobb. Ditt arbetsområde täcker in både Stockholm och Huddinge kommun, där du utgår från vårt huvudkontor i Ulvsunda.  För att trivas i den här rollen söker vi dig som:  Vill arbeta med inmätning och utsättning av vårt ledningsnät och anläggningar i fält Vill arbeta med kartering av mätdata för att erhålla en uppdaterad ledningsnätsdatabas Vill arbeta med inmätning och kartering av bolaget sopsugar Vill jobba på en kreativ enhet med höga målsättningar och med fokus på utveckling- och förbättringsarbete Gillar att jobba digitalt och via digitala tavlor för att visa hur arbetet fortgår Kan sprida goda exempel till övriga delar av bolaget och fånga upp nya arbetssätt från omvärlden    Vi använder Leicas mätinstrument i det dagliga arbetet. Vi karterar i en ArcGIS-baserad VA-produkt som heter GEOSECMA Ledning VA.  Rollen är en tillsvidareanställning med placering i Ulvsunda, med tillträde enligt överenskommelse. Provanställning kan ev. vara aktuellt.  I din tjänst kommer du arbeta med säkerhetsklassad information. Säkerhetsprövning och registerkontroll kommer därför att genomföras i samband med anställning. Om dig Vi söker dig som är utbildad Mät- och Kartingenjör eller motsvarande med några års erfarenhet av mätning i fält och kartering i en GIS-baserad miljö. Vi ser gärna att du har några års erfarenhet av utredningsarbete, team- eller projekteldning och är van av att arbeta i en ArcGIS-miljö. B-körkort är ett krav för tjänsten.  För att kunna bidra till enhetens utveckling ser vi gärna att du är kreativ, lyhörd samt positiv till förändringar och teknisk utveckling. Vi jobbar enligt LEAN-filosofin, så det är en merit om du har erfarenhet av detta sätt att arbeta. Det är viktigt att du har förmåga att arbeta både självständigt och i grupp!  I din roll kommer du få tillgång till olika utbildningar och fortlöpande få möjlighet att utvecklas internt. Hos oss finns mycket erfarenhet och kunskap - vi kommer hjälpa dig att lyckas med ditt uppdrag!  Om oss Stockholm Vatten och Avfall utvecklar och levererar vatten- och avfallstjänster som får vår fantastiska stad att fungera, växa och vara en plats där både människor och natur trivs. Tillsammans med invånare, företag och andra intressenter arbetar vi för att Stockholm ska bli världens mest hållbara stad. Hos oss får du ett roligt och omväxlande jobb med intressanta arbetsuppgifter, duktiga kollegor och ett gott arbetsklimat. Du får goda anställningsvillkor, får jobba med digitala och flexibla arbetssätt, tillgång till kontinuerlig kompetensutveckling tillsammans med en rad hälsofrämjande fördelar. Läs mer om oss och träffa några av våra medarbetare på svoa.se/jobb Ansökan Om du tycker att du känner igen dig i beskrivningen och att jobbet ser intressant ut så ser vi fram emot din ansökan senast den 14 augusti 2022!  För ytterligare information om tjänsten kontaktar du enhetschef Marianne Isoz Henriksson genom att mejla: marianne.isoz-henriksson@svoa.se Vi på Stockholm Vatten och Avfall ser fram emot din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Mät- och kartingenjör** Vill du arbeta på Sveriges st... |  | x |  | 21 | [Mät- och kartingenjör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Cc67_7CT_gBS) |
| 2 | Mät- och **kartingenjör** Vill du arbeta på Sveriges st... | x | x | 12 | 12 | [Kartingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/R2hT_88E_wx6) |
| 2 | Mät- och **kartingenjör** Vill du arbeta på Sveriges st... |  | x |  | 12 | [GIS- och kartingenjörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/ketV_G9W_GeR) |
| 2 | Mät- och **kartingenjör** Vill du arbeta på Sveriges st... |  | x |  | 12 | [Kartingenjörer m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/oAe9_rYM_DyC) |
| 3 | ...artingenjör Vill du arbeta på **Sveriges** största VA-bolag med spännand... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ...ing och kartering av bolagets **VA-nät** med tillhörande anläggningar ... |  | x |  | 6 | [Underhållsarbete, VA-nät/Underhållsarbete, gatunät, **skill**](http://data.jobtechdev.se/taxonomy/concept/2SCx_2dX_M79) |
| 5 | ...ugar.  Om rollen  Arbetet som **Mät- och Kartingenjör** innebär att du arbetar både p... |  | x |  | 21 | [Mät- och kartingenjör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Cc67_7CT_gBS) |
| 6 | ... rollen  Arbetet som Mät- och **Kartingenjör** innebär att du arbetar både p... | x | x | 12 | 12 | [Kartingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/R2hT_88E_wx6) |
| 6 | ... rollen  Arbetet som Mät- och **Kartingenjör** innebär att du arbetar både p... |  | x |  | 12 | [GIS- och kartingenjörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/ketV_G9W_GeR) |
| 6 | ... rollen  Arbetet som Mät- och **Kartingenjör** innebär att du arbetar både p... |  | x |  | 12 | [Kartingenjörer m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/oAe9_rYM_DyC) |
| 7 | ...nnebär att du arbetar både på **kontor** och ute i fält. Ditt arbete ä... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 8 | ...å kontor och ute i fält. Ditt **arbete** är självständigt, där du ansv... | x |  |  | 6 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 9 | ...ch ute i fält. Ditt arbete är **självständigt**, där du ansvarar för olika ty... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 10 | ...t arbetsområde täcker in både **Stockholm** och Huddinge kommun, där du u... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 11 | ... täcker in både Stockholm och **Huddinge** kommun, där du utgår från vår... | x | x | 8 | 8 | [Huddinge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g1Gc_aXK_EKu) |
| 12 | ...in både Stockholm och Huddinge** kommun**, där du utgår från vårt huvud... | x |  |  | 7 | [Huddinge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g1Gc_aXK_EKu) |
| 13 | ...Vill arbeta med inmätning och **utsättning** av vårt ledningsnät och anläg... | x | x | 10 | 10 | [Utsättning, **skill**](http://data.jobtechdev.se/taxonomy/concept/gdTC_3Eh_PMv) |
| 14 | ...ålsättningar och med fokus på **utveckling- och **förbättringsarbete Gillar att ... |  | x |  | 16 | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| 15 | ...CMA Ledning VA.  Rollen är en **tillsvidareanställning** med placering i Ulvsunda, med... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 16 | ... Vi söker dig som är utbildad **Mät- och Kartingenjör** eller motsvarande med några å... |  | x |  | 21 | [Mät- och kartingenjör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Cc67_7CT_gBS) |
| 17 | ... dig som är utbildad Mät- och **Kartingenjör** eller motsvarande med några å... | x | x | 12 | 12 | [Kartingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/R2hT_88E_wx6) |
| 17 | ... dig som är utbildad Mät- och **Kartingenjör** eller motsvarande med några å... |  | x |  | 12 | [GIS- och kartingenjörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/ketV_G9W_GeR) |
| 17 | ... dig som är utbildad Mät- och **Kartingenjör** eller motsvarande med några å... |  | x |  | 12 | [Kartingenjörer m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/oAe9_rYM_DyC) |
| 18 | ...ing i fält och kartering i en **GIS**-baserad miljö. Vi ser gärna a... | x |  |  | 3 | [Geografiskt informationssystem/GIS, **skill**](http://data.jobtechdev.se/taxonomy/concept/CWfM_RL9_V4t) |
| 19 | ...u har några års erfarenhet av **utredningsarbete**, team- eller projekteldning o... |  | x |  | 16 | [Utredningsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/QAaJ_QeY_Q6P) |
| 20 | ...att arbeta i en ArcGIS-miljö. **B-körkort** är ett krav för tjänsten.  Fö... |  | x |  | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 21 | ... utveckling. Vi jobbar enligt **LEAN**-filosofin, så det är en merit... | x |  |  | 4 | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| 22 | ...u har förmåga att arbeta både **självständigt** och i grupp!  I din roll komm... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 23 | ...  Om oss Stockholm Vatten och **Avfall** utvecklar och levererar vatte... | x |  |  | 6 | [Avfall, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Kx4Q_7TA_MuR) |
| 24 | ...tressenter arbetar vi för att **Stockholm** ska bli världens mest hållbar... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 25 | ...se Vi på Stockholm Vatten och **Avfall** ser fram emot din ansökan! | x |  |  | 6 | [Avfall, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Kx4Q_7TA_MuR) |
| | **Overall** | | | **63** | **348** | 63/348 = **18%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Underhållsarbete, VA-nät/Underhållsarbete, gatunät, **skill**](http://data.jobtechdev.se/taxonomy/concept/2SCx_2dX_M79) |
| x |  |  | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Geografiskt informationssystem/GIS, **skill**](http://data.jobtechdev.se/taxonomy/concept/CWfM_RL9_V4t) |
|  | x |  | [Mät- och kartingenjör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Cc67_7CT_gBS) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x |  |  | [Avfall, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Kx4Q_7TA_MuR) |
|  | x |  | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Utredningsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/QAaJ_QeY_Q6P) |
| x | x | x | [Kartingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/R2hT_88E_wx6) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Huddinge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g1Gc_aXK_EKu) |
| x | x | x | [Utsättning, **skill**](http://data.jobtechdev.se/taxonomy/concept/gdTC_3Eh_PMv) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [GIS- och kartingenjörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/ketV_G9W_GeR) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Kartingenjörer m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/oAe9_rYM_DyC) |
| | | **4** | 4/18 = **22%** |