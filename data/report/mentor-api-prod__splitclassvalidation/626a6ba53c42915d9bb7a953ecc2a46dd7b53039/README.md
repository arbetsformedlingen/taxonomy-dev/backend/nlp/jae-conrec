# Results for '626a6ba53c42915d9bb7a953ecc2a46dd7b53039'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [626a6ba53c42915d9bb7a953ecc2a46dd7b53039](README.md) | 1 | 1363 | 12 | 11 | 75/226 = **33%** | 4/10 = **40%** |

## Source text

Barnmorskor med ultraljudskompetens Om oss Bemanning med omsorg.  Ofelia vård månar om den svenska sjukvården, våra medborgare och om våra sjuksköterskor. Vi är Sveriges tryggaste bemanningsföretag för dig som är sjuksköterska. Vi har uppdragen som du söker och villkoren som du förtjänar. Ofelia vård är ett auktoriserat bemanningsföretag med medlemskap i Kompetensföretagen. Detta är en garanti att vi följer arbetsmarknadens lagar och regler. Vi har kollektivavtal med Vårdförbundet, Läkarförbundet och Unionen. Detta är en självklarhet för oss! Med en VD med lång erfarenhet som narkossjuksköterska och erfarna konsultchefer med vårderfarenhet så vet vi vad som är viktigt för dig som konsult ute på uppdrag. Vi är upphandlade i hela Sverige och finns tillgängliga för dig dygnet runt. Ofelia vård finns här för dig som sjuksköterska som vill arbeta för de villkor som du förtjänar, den arbetsmiljö som du vill ha och den flexibilitet som livet ibland kräver.   Vi har utöver detta en mängd förmånlig rabatter för alla våra anställda  Dina arbetsuppgifter Sedvanligt arbete för barnmorska med ultraljudskompetens.  Kontakta oss gärn för mer information på info@ofelia.se 010-178 36 20  Vi ordnar självfallet med resa och boende vid behov.   Din profil Vi söker dig som är barnmorska med ultraljudskompetens.  Varmt välkommen med dina och önskemål och ansökan.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Barnmorskor** med ultraljudskompetens Om os... | x | x | 11 | 11 | [Barnmorska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DEDi_wap_ntF) |
| 2 | Barnmorskor med **ultraljudskompetens** Om oss Bemanning med omsorg. ... | x |  |  | 19 | [utföra ultraljudsundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pieF_CWC_fUx) |
| 3 | ...rg.  Ofelia vård månar om den **svenska** sjukvården, våra medborgare o... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 4 | ..., våra medborgare och om våra **sjuksköterskor**. Vi är Sveriges tryggaste bem... |  | x |  | 14 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 4 | ..., våra medborgare och om våra **sjuksköterskor**. Vi är Sveriges tryggaste bem... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 5 | ...anningsföretag för dig som är **sjuksköterska**. Vi har uppdragen som du söke... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 5 | ...anningsföretag för dig som är **sjuksköterska**. Vi har uppdragen som du söke... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 6 | ...dens lagar och regler. Vi har **kollektivavtal** med Vårdförbundet, Läkarförbu... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 7 | ...en VD med lång erfarenhet som **narkossjuksköterska** och erfarna konsultchefer med... | x | x | 19 | 19 | [Narkossjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jCqK_dxK_w65) |
| 8 | ...rag. Vi är upphandlade i hela **Sverige** och finns tillgängliga för di... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 9 | ...ia vård finns här för dig som **sjuksköterska** som vill arbeta för de villko... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 9 | ...ia vård finns här för dig som **sjuksköterska** som vill arbeta för de villko... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 10 | ...villkor som du förtjänar, den **arbetsmiljö** som du vill ha och den flexib... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 11 | ...pgifter Sedvanligt arbete för **barnmorska** med ultraljudskompetens.  Kon... | x | x | 10 | 10 | [Barnmorska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DEDi_wap_ntF) |
| 12 | ...igt arbete för barnmorska med **ultraljudskompetens**.  Kontakta oss gärn för mer i... | x |  |  | 19 | [utföra ultraljudsundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pieF_CWC_fUx) |
| 13 | ...in profil Vi söker dig som är **barnmorska** med ultraljudskompetens.  Var... | x | x | 10 | 10 | [Barnmorska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DEDi_wap_ntF) |
| 14 | ...ker dig som är barnmorska med **ultraljudskompetens**.  Varmt välkommen med dina oc... | x |  |  | 19 | [utföra ultraljudsundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pieF_CWC_fUx) |
| | **Overall** | | | **75** | **226** | 75/226 = **33%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Barnmorska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DEDi_wap_ntF) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Narkossjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jCqK_dxK_w65) |
| x |  |  | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| x |  |  | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| x |  |  | [utföra ultraljudsundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pieF_CWC_fUx) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **4** | 4/10 = **40%** |