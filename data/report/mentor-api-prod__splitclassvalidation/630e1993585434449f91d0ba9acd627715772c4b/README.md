# Results for '630e1993585434449f91d0ba9acd627715772c4b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [630e1993585434449f91d0ba9acd627715772c4b](README.md) | 1 | 1967 | 3 | 0 | 0/35 = **0%** | 0/1 = **0%** |

## Source text

Mingelaktör - Halloween Tycker du om att skrämmas? Inför hösten söker Unibros production AB nya talangfulla mingelaktörer som vill hjälpa oss att skapa magi på Kolmårdens djurpark och i Furuviksparken! Ta chansen att få jobba med vårt kreativa och välkomnande team under halloween-säsongen 2022. Jobbet Som Mingelaktör kommer du vara med och ge liv till parkernas olika tematiserade områden. Tillsammans med dina kollegor befolkar ni parken med spännande, läskiga och/eller roliga karaktärer som gästerna får se och interagera med under hela öppetdagen. Ni gör varje gästs upplevelse unik och minnesvärd. Du jobbar fram en egen karaktär med kostym, smink och personlighet utefter områdets tematik och som också passar din spelstil. Vissa karaktärer tas fram av Unibros där vi står för kostym och sminkmall och du i din tur fyller karaktären med liv. Arbetsuppgiften för dessa roller är densamma, men för aktörer som skapar egen kostym tilldelas ett lönepåslag. Horror Night Under några kvällar under arbetsperioden kommer båda parkerna ha kvällsöppet för Horror Night. Detta konceptet är nytt sedan förra året och är till för de modigare besökarna. Här ampas skräcknivån upp både i bemötande och kostym. Tid ges för mer avancerade sminkningar. Det är ett kortare men mer intensivt pass som går ut på att skrämmas ordentligt.  Intervjuer Vi kommer att hålla intervjuer från och med mitten av augusti och september ut. Intervjuerna kommer att hållas i anslutning till båda parkerna samt online. Intervjuerna är till för att vi ska få en bättre uppfattning om dig samt att du ska få träffa oss. rekrytering sker löpande så skicka in din anmälan så snart du kan. Lön När du jobbar hos oss får du ett dagsgage på 1 290 kr. För dig som gör egen karaktär och kostym tillkommer ett kostymtillägg på 300 kr per arbetsdag.    Läs mer om jobbet och hur det är att jobba med oss på vår hemsida. Länk hittar du här nedanför. Vi ser fram emot att lära känna dig! /Teamet på unibros

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Mingelaktör** - Halloween Tycker du om att ... | x |  |  | 11 | [Skådespelare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/iBUL_s38_izZ) |
| 2 | ...production AB nya talangfulla **mingelaktörer** som vill hjälpa oss att skapa... | x |  |  | 13 | [Skådespelare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/iBUL_s38_izZ) |
| 3 | ...een-säsongen 2022. Jobbet Som **Mingelaktör** kommer du vara med och ge liv... | x |  |  | 11 | [Skådespelare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/iBUL_s38_izZ) |
| | **Overall** | | | **0** | **35** | 0/35 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Skådespelare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/iBUL_s38_izZ) |
| | | **0** | 0/1 = **0%** |