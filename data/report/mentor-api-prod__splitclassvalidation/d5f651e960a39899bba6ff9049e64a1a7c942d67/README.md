# Results for 'd5f651e960a39899bba6ff9049e64a1a7c942d67'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d5f651e960a39899bba6ff9049e64a1a7c942d67](README.md) | 1 | 461 | 5 | 6 | 50/91 = **55%** | 3/6 = **50%** |

## Source text

Kassabiträde/serveringspersonal Därmedpasta är en väletablerad pasta- och pizzarestaurang som mättat magar på Hornstull i snart 20 år.  Vi söker dig som är utåtriktad och social. Meriterande är tidigare erfarenhet inom restaurangbranschen. Du talar flytande svenska. Som kassabiträde hos därmedpasta har du närmast kontakt med gästerna. Arbetsuppgifterna består i huvudsak av mottagande av gäster vid kassa samt stöd till köket. Kontakta oss så berättar vi mer!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kassabiträde**/serveringspersonal Därmedpast... | x | x | 12 | 12 | [Kassapersonal, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/j8X2_KhG_6ty) |
| 2 | Kassabiträde/**serveringspersonal** Därmedpasta är en väletablera... | x |  |  | 18 | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| 2 | Kassabiträde/**serveringspersonal** Därmedpasta är en väletablera... |  | x |  | 18 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 3 | ...e är tidigare erfarenhet inom **restaurangbranschen**. Du talar flytande svenska. S... | x | x | 19 | 19 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 4 | ...gbranschen. Du talar flytande **svenska**. Som kassabiträde hos därmedp... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 5 | ...u talar flytande svenska. Som **kassabiträde** hos därmedpasta har du närmas... | x | x | 12 | 12 | [Kassapersonal, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/j8X2_KhG_6ty) |
| 6 | ...k av mottagande av gäster vid **kassa** samt stöd till köket. Kontakt... |  | x |  | 5 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| | **Overall** | | | **50** | **91** | 50/91 = **55%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x |  |  | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| x | x | x | [Kassapersonal, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/j8X2_KhG_6ty) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
|  | x |  | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/6 = **50%** |