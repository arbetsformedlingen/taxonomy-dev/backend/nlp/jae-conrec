# Results for 'ed68e1a8829cc7b1cbb8bd6cf0122c5ad22839cb'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ed68e1a8829cc7b1cbb8bd6cf0122c5ad22839cb](README.md) | 1 | 824 | 6 | 7 | 25/70 = **36%** | 3/8 = **38%** |

## Source text

Kock Vi söker en kock på 100% med några års erfarenhet till vårt härliga team. Vill du ta nästa steg i din utveckling? Vi söker en glad, engagerad och professionell kollega med förmåga att laga mat i högt tempo. Vi serverar lunch och à la carte. Hos oss lagar vi mat från grunden med stort engagemang för kvalitet och lokala råvaror. Du sätter gästen i fokus, är stresstålig och bra på att samarbeta. Tjänsten innebär arbete både dagtid, kvällstid och helger. Anställningen är tillsvidare. Lön enligt överenskommelse. Tillträde snarast. Rekrytering kommer att ske löpande, sista ansökningsdag är 24 juli. Har du frågor kan du kontakta köksmästare Peter Nilsson på 0431-25520. Skicka CV och personligt brev till info@valhallparkhotell.se. Välkommen med din ansökan!  Valhall Park Hotell, Stjernsvärds allé 66, 26274 ÄNGELHOLM

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kock** Vi söker en kock på 100% med ... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 2 | Kock Vi söker en **kock** på 100% med några års erfaren... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 3 | Kock Vi söker en kock på **100%** med några års erfarenhet till... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 4 | ... tempo. Vi serverar lunch och **à la carte**. Hos oss lagar vi mat från gr... |  | x |  | 10 | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| 5 | ... Du sätter gästen i fokus, är **stresstålig** och bra på att samarbeta. Tjä... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 6 | ... och helger. Anställningen är **tillsvidare**. Lön enligt överenskommelse. ... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 7 | ...Har du frågor kan du kontakta **köksmästare** Peter Nilsson på 0431-25520. ... | x | x | 11 | 11 | [köksmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tNA4_ywP_j5q) |
| 8 | ...ed din ansökan!  Valhall Park **Hotell**, Stjernsvärds allé 66, 26274 ... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 9 | ..., Stjernsvärds allé 66, 26274 **ÄNGELHOLM** |  | x |  | 9 | [Ängelholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/pCuv_P5A_9oh) |
| | **Overall** | | | **25** | **70** | 25/70 = **36%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x | x | x | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Ängelholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/pCuv_P5A_9oh) |
| x | x | x | [köksmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tNA4_ywP_j5q) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **3** | 3/8 = **38%** |