# Results for '8acd799aec55bc26190fa69f58a54c5ebafccb92'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8acd799aec55bc26190fa69f58a54c5ebafccb92](README.md) | 1 | 982 | 7 | 4 | 41/66 = **62%** | 4/7 = **57%** |

## Source text

Kassapersonal till SEN Street kitchen - Värnamo SEN STREET KTICHEN är ett restaurangkoncept som återskapar de autentiska smakerna, dofterna och känslorna vid de livliga gatumarknader och precis som alla andra gatukök över hela östra Asien. Enkelt, snabbt, nyttigt och fräscht.  Vem söker vi?  Vi söker dig som är glad, utåtriktad och brinner för att kunna leverera service på högsta nivå. Du ska kunna hålla flera bollar i luften under stressiga situationer, ha god samarbetsförmåga och det är viktigt att du trivs att arbeta i restaurangmiljön.  Vem är du?  Oavsett om du är erfaren eller mindre erfaren, så är du välkommen att lämna in en ansökan. Vi ger utbildning på plats, det viktiga är att du har ett starkt engagemang och viljan att lära dig nya saker. Social och lätt för att prata.  Arbetsuppgifter:  - Ta beställningar av gäst. - Vara ansiktet utåt för företaget och våra kunder. - Kassahantering. - Städ och disk.   Välkommen med din ansökan så berättar jag mer för dig!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kassapersonal** till SEN Street kitchen - Vär... | x | x | 13 | 13 | [Kassapersonal, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/j8X2_KhG_6ty) |
| 2 | ...nal till SEN Street kitchen - **Värnamo** SEN STREET KTICHEN är ett res... | x | x | 7 | 7 | [Värnamo, **municipality**](http://data.jobtechdev.se/taxonomy/concept/6bS8_fzf_xpW) |
| 3 | ...amo SEN STREET KTICHEN är ett **restaurangkoncept** som återskapar de autentiska ... | x |  |  | 17 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 4 | ...der och precis som alla andra **gatukök** över hela östra Asien. Enkelt... | x | x | 7 | 7 | [Gatukök, **skill**](http://data.jobtechdev.se/taxonomy/concept/dmi6_Jfs_pso) |
| 5 | ... företaget och våra kunder. - **Kassahantering**. - Städ och disk.   Välkommen... | x | x | 14 | 14 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 6 | ...a kunder. - Kassahantering. - **Städ** och disk.   Välkommen med din... | x |  |  | 4 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 7 | ... - Kassahantering. - Städ och **disk**.   Välkommen med din ansökan ... | x |  |  | 4 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| | **Overall** | | | **41** | **66** | 41/66 = **62%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Värnamo, **municipality**](http://data.jobtechdev.se/taxonomy/concept/6bS8_fzf_xpW) |
| x | x | x | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x | x | x | [Gatukök, **skill**](http://data.jobtechdev.se/taxonomy/concept/dmi6_Jfs_pso) |
| x | x | x | [Kassapersonal, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/j8X2_KhG_6ty) |
| x |  |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x |  |  | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| | | **4** | 4/7 = **57%** |