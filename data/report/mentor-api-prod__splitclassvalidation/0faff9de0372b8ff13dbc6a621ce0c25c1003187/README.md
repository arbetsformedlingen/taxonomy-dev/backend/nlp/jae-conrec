# Results for '0faff9de0372b8ff13dbc6a621ce0c25c1003187'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0faff9de0372b8ff13dbc6a621ce0c25c1003187](README.md) | 1 | 2721 | 22 | 21 | 135/406 = **33%** | 10/22 = **45%** |

## Source text

Klimattekniker/kyltekniker till Indoor Energy 🔧 Vill du bidra till en bättre miljö och arbeta med problemlösning? Som Kyltekniker till Indoor Energy får du en viktig och meningsfull roll med stor frihet i ett företag som arbetar med att förbättra sina kunders inomhusklimat med lägre energiförbrukning.  Om tjänsten Indoor Energy är kompetensmässigt Sveriges femte största kylföretag. De hanterar allt från glassfrysarna på Preem till de gigantiska industriella applikationer som styr kylan i nya Karolinska Sjukhuset. Deras metod utgår från en helhetssyn där kyla, värme, ventilation och styr- och reglerteknik samverkar för ett inomhusklimat med lägre energiförbrukning.  Indoor Energy önskar nu utöka sitt nuvarande team i södra Stockholm med en positiv person med stort eget driv. Tjänsten kommer innebära att du självständigt ska planera, samordna och utföra serviceuppdrag och driftunderhåll med fokus på kyla åt Indoor Energys kunder vilket innebär stor frihet under ansvar. Kontoret ligger i Skarpnäck men du kommer att få en servicebil som du kan utgå hemifrån med. Majoriteten av uppdragen är i södra Stockholm, men det kan förekomma att du ibland stöttar upp på andra upptagningsområden. På Indoor är det viktigt med personlig utveckling och det finns stora möjligheter till både vidareutbildningar och att utvecklas internt.  Detta är en direktrekrytering till Indoor Energy där Wrknest sköter rekryteringsprocessen.  Dina framtida arbetsuppgifter Rollen som Klimattekniker med inriktning kyla kommer innebära att ta hand om Indoor Energys befintliga serviceavtalskunder. Arbetsuppgifterna kommer exempelvis att innebära:  - Planera, samordna och utföra serviceuppdrag och övrigt driftunderhåll åt Indoor Energys kunder i såväl förebyggande som felavhjälpande syfte. - Felsökning och mindre montage - Stötta kollegor med din specialistkompetens inom kylteknik - Proaktivt föreslå förbättringar i kundernas anläggningar för att minska energianvändningen samt förebygga icke planerade stopp.   Vi söker dig som har  - Kylcertifikat - B-körkort - Flytande kunskaper i svenska i tal och skrift   Vid denna rekrytering lägger vi stor vikt vid dina personliga egenskaper, då de är avgörande för hur du kommer att lyckas i rollen och företaget. Vi förstår att man inte kan allting från början - men du ska ha en vilja att lära dig! Därför söker vi dig som är social, flexibel och ansvarstagande. Som person är du även noggrann i ditt arbete och serviceinriktad i din kontakt med kunder och kollegor.  Övrig information  Start: Enligt överenskommelse  Plats: Stockholm, Skarpnäck  Omfattning: Heltid, 40h/ vecka  Anställningsform: Tillsvidareanställning  Sök gärna så snart som möjligt då vi jobbar med löpande urval.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Klimattekniker**/kyltekniker till Indoor Energ... | x |  |  | 14 | [Klimatteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/VvvT_DJ3_aPH) |
| 2 | Klimattekniker/**kyltekniker** till Indoor Energy 🔧 Vill du ... | x | x | 11 | 11 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 3 | ...rbeta med problemlösning? Som **K**yltekniker till Indoor Energy ... |  | x |  | 1 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 4 | ...beta med problemlösning? Som K**yltekniker** till Indoor Energy får du en ... | x | x | 10 | 10 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 5 | ...roblemlösning? Som Kyltekniker** **till Indoor Energy får du en v... | x |  |  | 1 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 6 | ...nders inomhusklimat med lägre **energiförbrukning**.  Om tjänsten Indoor Energy ä... |  | x |  | 17 | [analysera energiförbrukning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1Hex_aoZ_k2x) |
| 6 | ...nders inomhusklimat med lägre **energiförbrukning**.  Om tjänsten Indoor Energy ä... |  | x |  | 17 | [råda kunder om produkters energiförbrukning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6WRj_YAf_unv) |
| 6 | ...nders inomhusklimat med lägre **energiförbrukning**.  Om tjänsten Indoor Energy ä... |  | x |  | 17 | [bedöma ventilationssystems energiförbrukning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/vVK1_gM2_CKi) |
| 7 | ...r Energy är kompetensmässigt S**veriges **femte största kylföretag. De h... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 8 | ... kyla, värme, ventilation och **s**tyr- och reglerteknik samverka... |  | x |  | 1 | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
| 9 | ...kyla, värme, ventilation och s**tyr- och reglerteknik** samverkar för ett inomhusklim... | x | x | 21 | 21 | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
| 10 | ...ion och styr- och reglerteknik** **samverkar för ett inomhusklima... | x |  |  | 1 | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
| 11 | ...r ett inomhusklimat med lägre **energiförbrukning**.  Indoor Energy önskar nu utö... |  | x |  | 17 | [analysera energiförbrukning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1Hex_aoZ_k2x) |
| 11 | ...r ett inomhusklimat med lägre **energiförbrukning**.  Indoor Energy önskar nu utö... |  | x |  | 17 | [råda kunder om produkters energiförbrukning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6WRj_YAf_unv) |
| 11 | ...r ett inomhusklimat med lägre **energiförbrukning**.  Indoor Energy önskar nu utö... |  | x |  | 17 | [bedöma ventilationssystems energiförbrukning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/vVK1_gM2_CKi) |
| 12 | ...a sitt nuvarande team i södra **S**tockholm med en positiv person... |  | x |  | 1 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 13 | ... sitt nuvarande team i södra S**tockholm** med en positiv person med sto... | x | x | 8 | 8 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 14 | ...varande team i södra Stockholm** **med en positiv person med stor... | x |  |  | 1 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 15 | ...nsten kommer innebära att du s**jälvständigt **ska planera, samordna och utfö... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 16 | ...ch utföra serviceuppdrag och d**riftunderhåll **med fokus på kyla åt Indoor En... | x |  |  | 14 | [Installation, drift, underhåll, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/yhCP_AqT_tns) |
| 17 | ...teten av uppdragen är i södra **S**tockholm, men det kan förekomm... |  | x |  | 1 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 18 | ...eten av uppdragen är i södra S**tockholm**, men det kan förekomma att du... | x | x | 8 | 8 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 19 | ...uppdragen är i södra Stockholm**,** men det kan förekomma att du ... | x |  |  | 1 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 20 | ... På Indoor är det viktigt med **p**ersonlig utveckling och det fi... |  | x |  | 1 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 21 | ...På Indoor är det viktigt med p**ersonlig utveckling** och det finns stora möjlighet... | x | x | 19 | 19 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 22 | ...ktigt med personlig utveckling** **och det finns stora möjlighete... | x |  |  | 1 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 23 | ...a arbetsuppgifter Rollen som K**limattekniker **med inriktning kyla kommer inn... | x |  |  | 14 | [Klimatteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/VvvT_DJ3_aPH) |
| 24 | ...ra serviceuppdrag och övrigt d**riftunderhåll **åt Indoor Energys kunder i såv... | x |  |  | 14 | [Energi, drift och underhåll, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6V78_5GY_XTu) |
| 25 | ...e som felavhjälpande syfte. - **F**elsökning och mindre montage -... |  | x |  | 1 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 26 | ... som felavhjälpande syfte. - F**elsökning** och mindre montage - Stötta k... | x | x | 9 | 9 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 27 | ...vhjälpande syfte. - Felsökning** **och mindre montage - Stötta ko... | x |  |  | 1 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 28 | ... din specialistkompetens inom **k**ylteknik - Proaktivt föreslå f... |  | x |  | 1 | [Kylteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/11L4_Q2Z_8tM) |
| 29 | ...din specialistkompetens inom k**ylteknik** - Proaktivt föreslå förbättri... | x | x | 8 | 8 | [Kylteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/11L4_Q2Z_8tM) |
| 30 | ...ialistkompetens inom kylteknik** **- Proaktivt föreslå förbättrin... | x |  |  | 1 | [Kylteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/11L4_Q2Z_8tM) |
| 31 | ...pp.   Vi söker dig som har  - **K**ylcertifikat - B-körkort - Fly... |  | x |  | 1 | [Kylcertifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/VWmQ_9fd_GLL) |
| 32 | ...p.   Vi söker dig som har  - K**ylcertifikat** - B-körkort - Flytande kunska... | x | x | 12 | 12 | [Kylcertifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/VWmQ_9fd_GLL) |
| 33 | ...r dig som har  - Kylcertifikat** **- B-körkort - Flytande kunskap... | x |  |  | 1 | [Kylcertifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/VWmQ_9fd_GLL) |
| 34 | ...ig som har  - Kylcertifikat - **B**-körkort - Flytande kunskaper ... |  | x |  | 1 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 35 | ...g som har  - Kylcertifikat - B**-körkort** - Flytande kunskaper i svensk... | x | x | 8 | 8 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 36 | ...r  - Kylcertifikat - B-körkort** **- Flytande kunskaper i svenska... | x |  |  | 1 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 37 | ...örkort - Flytande kunskaper i **s**venska i tal och skrift   Vid ... |  | x |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 38 | ...rkort - Flytande kunskaper i s**venska** i tal och skrift   Vid denna ... | x | x | 6 | 6 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 39 | ...- Flytande kunskaper i svenska** **i tal och skrift   Vid denna r... | x |  |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 40 | ...g som är social, flexibel och **ansvarstagande**. Som person är du även noggra... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 41 | ... som är social, flexibel och a**nsvarstagande.** Som person är du även noggran... | x |  |  | 14 | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
| 42 | ...agande. Som person är du även **n**oggrann i ditt arbete och serv... |  | x |  | 1 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 43 | ...gande. Som person är du även n**oggrann** i ditt arbete och serviceinri... | x | x | 7 | 7 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 44 | ...Som person är du även noggrann** **i ditt arbete och serviceinrik... | x |  |  | 1 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 45 | ...nligt överenskommelse  Plats: **S**tockholm, Skarpnäck  Omfattnin... |  | x |  | 1 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 46 | ...ligt överenskommelse  Plats: S**tockholm**, Skarpnäck  Omfattning: Helti... | x | x | 8 | 8 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 47 | ...renskommelse  Plats: Stockholm**,** Skarpnäck  Omfattning: Heltid... | x |  |  | 1 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 48 | ...holm, Skarpnäck  Omfattning: H**eltid, 40h/ vecka ** Anställningsform: Tillsvidare... | x |  |  | 18 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 49 | ...0h/ vecka  Anställningsform: T**illsvidareanställning ** Sök gärna så snart som möjlig... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| | **Overall** | | | **135** | **406** | 135/406 = **33%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Kylteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/11L4_Q2Z_8tM) |
|  | x |  | [analysera energiförbrukning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1Hex_aoZ_k2x) |
| x |  |  | [Energi, drift och underhåll, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6V78_5GY_XTu) |
|  | x |  | [råda kunder om produkters energiförbrukning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6WRj_YAf_unv) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
| x | x | x | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| x | x | x | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Kylcertifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/VWmQ_9fd_GLL) |
| x |  |  | [Klimatteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/VvvT_DJ3_aPH) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [bedöma ventilationssystems energiförbrukning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/vVK1_gM2_CKi) |
| x | x | x | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| x |  |  | [Installation, drift, underhåll, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/yhCP_AqT_tns) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **10** | 10/22 = **45%** |