# Results for '533608935f7abb7526c90cc116cc9faba09b0634'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [533608935f7abb7526c90cc116cc9faba09b0634](README.md) | 1 | 1766 | 8 | 2 | 0/135 = **0%** | 0/5 = **0%** |

## Source text

Njur- och Hematologiavdelning 20-26 Augusti! Är du en legitimerad sjuksköterska som vill ha en trygg anställning med bra villkor? Perfekt! Då har du kommit helt rätt. Just nu söker vi Magnifiqa sjuksköterskor till spännande uppdrag på en njur- och hematologiavdelning i Ö-vik.  Uppdraget pågår från 22-08-20 till 22-08-26.  Kontakta någon av våra medarbetare för mer information om uppdraget.  Legitimerad sjuksköterska som vill bli en del av en växande verksamhet - den vi söker!  Du som läser detta hoppas vi är legitimerad sjuksköterska och har några års erfarenhet från yrket. Var du har jobbat innan, var du bor, eller vilken (om någon) specialitet du har är inte det viktigaste! Vi har garanterat uppdrag som passar alla.  Som person tror vi att du är glad, trivs med att träffa nya människor och vill axla rollen som ambassadör ute på uppdrag hos våra kunder. Vidare är vi övertygade om att våra medarbetare är de som bäst vet hur vår verksamhet borde utvecklas, så om du har smarta idéer, tankar eller förslag på vad vi bör göra härnäst välkomnar vi det!  Magnifiq - det familjära vårdbemanningsbolaget med den starka lokala kopplingen  Magnifiq är ingen vårdjätte. Istället bygger vår framgång på nöjda medarbetare med trygga anställningar och tjänster av högsta kvalitet. Vi förmedlar bemanningsuppdrag över hela Sverige, men har vårt huvudkontor i Umeå och känner stolthet över att vara norrlandsexperter när det kommer till vårdbemanning och uppdrag för sjuksköterskor. Som medarbetare hos Magnifiq blir du en del av en familj där ideal som långsiktighet, transparens och omtanke styr.  Låter detta som något för dig? Det både tror och hoppas vi.  Vänta i så fall inte - sök redan idag!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Njur- och **Hematologiavdelning** 20-26 Augusti! Är du en legi... | x |  |  | 19 | [Hematologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/y9uS_oYh_4Kr) |
| 2 | ...ugusti! Är du en legitimerad **sjuksköterska** som vill ha en trygg anställ... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 3 | .... Just nu söker vi Magnifiqa **sjuksköterskor** till spännande uppdrag på en... | x |  |  | 15 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 4 | ...nande uppdrag på en njur- och **hematologiavdelning** i Ö-vik.  Uppdraget pågår frå... | x |  |  | 19 | [Hematologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/y9uS_oYh_4Kr) |
| 5 | ...ur- och hematologiavdelning i **Ö-vik**.  Uppdraget pågår från 22-08-... | x |  |  | 5 | [Örnsköldsvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zBmE_n6s_MnQ) |
| 6 | ...on om uppdraget.  Legitimerad **sjuksköterska** som vill bli en del av en vä... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 7 | ...tta hoppas vi är legitimerad **sjuksköterska** och har några års erfarenhe... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 8 | ... bemanningsuppdrag över hela **Sverige**, men har vårt huvudkontor i ... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 9 | ...vårdbemanning och uppdrag för **sjuksköterskor**. Som medarbetare hos Magnifiq... |  | x |  | 14 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 9 | ...vårdbemanning och uppdrag för **sjuksköterskor**. Som medarbetare hos Magnifiq... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| | **Overall** | | | **0** | **135** | 0/135 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| x |  |  | [Hematologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/y9uS_oYh_4Kr) |
| x |  |  | [Örnsköldsvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zBmE_n6s_MnQ) |
| | | **0** | 0/5 = **0%** |