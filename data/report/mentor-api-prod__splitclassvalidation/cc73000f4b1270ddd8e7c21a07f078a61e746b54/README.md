# Results for 'cc73000f4b1270ddd8e7c21a07f078a61e746b54'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [cc73000f4b1270ddd8e7c21a07f078a61e746b54](README.md) | 1 | 3969 | 29 | 26 | 127/390 = **33%** | 10/21 = **48%** |

## Source text

Testledare IT Som medarbetare på Trafikverket är du med och underlättar vardagen för invånare och företagare i hela Sverige. Det gör du genom att vara med och bidra till framtidens infrastruktur.   Testledare IT  Arbetsuppgifter  Som testledare på någon av våra enheter inom avdelning Anläggningsinformation IT kommer du att arbeta teambaserat i utvecklings- och förvaltningsuppdrag. Här får du möjlighet att arbeta strukturerat med kvalitetshöjande insatser inom krav- och testområdet. Du är med och påverkar dagens och framtidens utveckling av systemstöd för användare internt inom Trafikverket och i samhället i stort. Arbetet präglas av samarbete och agila arbetssätt.  Andra uppgifter som ingår i rollen som Testledare kan vara att:  planera samtliga testaktiviteter inom de olika uppdragen, allt ifrån funktionella tester till tekniska tester och acceptanstester utföra analys och specificering av krav och testfall, testgenomförande, utvärdering och uppföljning av test vara acceptanstestledare, där du som testledare är stöd till verksamheten och leder arbetet med acceptanstester vid mottagande av IT-lösningar från externa leverantörer ha en öppen dialog med systemanvändare och externa intressenter  Hos oss får du också möjlighet att vara med och påverka hur vi vidareutvecklar våra arbetssätt för test och kravhantering. Vi arbetar i Microsoft-miljö med Azure DevOps/TFS (Team Foundation Server) och för testarbetet använder vi testdelarna inom Visual Studio. Är du intresserad av att arbeta med modern teknik, utveckla system som påverkar många människors vardag och ha goda utvecklingsmöjligheter med många karriärvägar - sök!  Övrig information  Valbara placeringsorter: Borlänge, Gävle, Örebro I den här rekryteringen kan urvalstest och arbetsprov komma att appliceras.    Kvalifikationer  Som person för den här rollen viktar vi att du är en strukturerad och kvalitetsmedveten problemlösare som gillar utmaningar och nytänk. Du drivs av att samarbeta, är lyhörd och har god ledarskapsförmåga. Formella krav: Vi söker dig som Du har högskoleexamen om 180hp inom IT-området. Alternativt har du aktuell och relevant arbetslivserfarenhet inom IT-området som vi ser motsvarar utbildningskravet för tjänsten. Du har erfarenhet inom minst tre av följande områden: - testledning inom systemutvecklingsprojekt, - att leda en grupp testare, - tidsestimering för testarbete, - testverktyg, - agilt arbetssätt Du förstår och har lätt för att uttrycka dig på svenska, både i tal och skrift.  Meriterande Om du har erfarenhet av acceptanstestledning Om du har erfarenhet av testautomation.    Ansökan  De frågor som du får besvara när du skickar in din ansökan kommer att ligga till grund för det första urvalet vi gör.  Som sökande till Trafikverket kan du eventuellt behöva gå igenom en säkerhetsprövning. Den innehåller säkerhetsprövningssamtal och registerkontroll innan anställning, om tjänsten är placerad i säkerhetsklass. I vissa fall krävs svenskt medborgarskap för säkerhetsklassade tjänster. Inom vår IT-organisation är du med och utvecklar Sverige med hjälp av moderna och innovativa lösningar. Vi är mitt i en spännande förändring där vi försöker ta vara på digitaliseringens möjligheter till fullo. Digitaliseringen ställer allt större krav och vi vill därför anpassa oss vad gäller kompetens, arbetssätt och organisation i takt med utvecklingen. Vi har ett helhetsansvar för IT och telekom, en verksamhet som pågår dygnet runt, året om - precis som Sveriges transportsystem. På Trafikverket jobbar vi med att göra Sverige närmare. Vi tror på att en förening av olika erfarenheter, bakgrunder och perspektiv är den bästa grunden för att bygga en arbetsplats fri från diskriminering och en miljö där alla trivs och gör sitt bästa. Vi tror på mångfald där den gemensamma nämnaren är pålitlighet, engagemang och mod. För det är så vi bäst representerar vårt samhälle.  Läs mer här  Att jobba hos oss  https://www.trafikverket.se/om-oss/jobba-hos-oss/

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Testledare** IT Som medarbetare på Trafikv... | x | x | 10 | 10 | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| 2 | Testledare **IT** Som medarbetare på Trafikverk... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 3 | ...nvånare och företagare i hela **Sverige**. Det gör du genom att vara me... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ...framtidens infrastruktur.   **Testledare** IT  Arbetsuppgifter  Som te... | x | x | 10 | 10 | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| 5 | ...infrastruktur.   Testledare **IT**  Arbetsuppgifter  Som testl... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 6 | ...re IT  Arbetsuppgifter  Som **testledare** på någon av våra enheter inom... | x | x | 10 | 10 | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| 7 | ...elning Anläggningsinformation **IT** kommer du att arbeta teambase... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 8 | ...etet präglas av samarbete och **agila arbetssätt**.  Andra uppgifter som ingår i... | x |  |  | 16 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 9 | ...gifter som ingår i rollen som **Testledare** kan vara att:  planera samtli... | x | x | 10 | 10 | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| 10 | ...ster till tekniska tester och **acceptanstester** utföra analys och specificeri... | x | x | 15 | 15 | [Acceptanstestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/jYaA_Cwa_Yyf) |
| 11 | ...öljning av test vara acceptans**testledare**, där du som testledare är stö... | x | x | 10 | 10 | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| 12 | ...ceptanstestledare, där du som **testledare** är stöd till verksamheten och... | x |  |  | 10 | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| 13 | ...amheten och leder arbetet med **acceptanstester** vid mottagande av IT-lösninga... |  | x |  | 15 | [Acceptanstestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/jYaA_Cwa_Yyf) |
| 14 | ...ptanstester vid mottagande av **IT**-lösningar från externa levera... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 15 | ...anstester vid mottagande av IT**-lösningar** från externa leverantörer ha ... |  | x |  | 10 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 16 | ...arbetar i Microsoft-miljö med **Azure** DevOps/TFS (Team Foundation S... | x | x | 5 | 5 | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| 17 | ...r i Microsoft-miljö med Azure **DevOps**/TFS (Team Foundation Server) ... | x |  |  | 6 | [DevOps, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Bxx7_d5h_9qS) |
| 18 | ... använder vi testdelarna inom **Visual Studio**. Är du intresserad av att arb... |  | x |  | 13 | [Visual Studio.NET, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/ocZE_Fi7_kTv) |
| 19 | ...ion  Valbara placeringsorter: **Borlänge**, Gävle, Örebro I den här rekr... | x | x | 8 | 8 | [Borlänge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cpya_jJg_pGp) |
| 20 | ...ra placeringsorter: Borlänge, **Gävle**, Örebro I den här rekrytering... | x | x | 5 | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 21 | ...eringsorter: Borlänge, Gävle, **Örebro** I den här rekryteringen kan u... | x | x | 6 | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 22 | ...arbeta, är lyhörd och har god **ledarskapsförmåga**. Formella krav: Vi söker dig ... |  | x |  | 17 | [Ledarskapsförmåga - Verkställande direktör (VD), **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/CfLz_Aex_PkG) |
| 22 | ...arbeta, är lyhörd och har god **ledarskapsförmåga**. Formella krav: Vi söker dig ... |  | x |  | 17 | [ledarförmåga, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/SVVU_BC5_mQe) |
| 22 | ...arbeta, är lyhörd och har god **ledarskapsförmåga**. Formella krav: Vi söker dig ... |  | x |  | 17 | [Ledarskapsförmåga - E-commerce manager, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/VevS_VKB_fpn) |
| 22 | ...arbeta, är lyhörd och har god **ledarskapsförmåga**. Formella krav: Vi söker dig ... |  | x |  | 17 | [Ledarskapsförmåga - Butiksledare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/dXwA_wgg_k8x) |
| 23 | ...krav: Vi söker dig som Du har **högskoleexamen** om 180hp inom IT-området. Alt... |  | x |  | 14 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 23 | ...krav: Vi söker dig som Du har **högskoleexamen** om 180hp inom IT-området. Alt... | x |  |  | 14 | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| 24 | ... högskoleexamen om 180hp inom **IT**-området. Alternativt har du a... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 25 | ...ögskoleexamen om 180hp inom IT**-området**. Alternativt har du aktuell o... |  | x |  | 8 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 26 | ...ant arbetslivserfarenhet inom **IT**-området som vi ser motsvarar ... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 27 | ...t arbetslivserfarenhet inom IT**-området** som vi ser motsvarar utbildni... |  | x |  | 8 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 28 | ...st tre av följande områden: - **testledning** inom systemutvecklingsprojekt... | x | x | 11 | 11 | [Testledning, datasystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/dexT_Znd_Yrs) |
| 29 | ... testarbete, - testverktyg, - **agilt arbetssätt** Du förstår och har lätt för a... | x |  |  | 16 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 30 | ... lätt för att uttrycka dig på **svenska**, både i tal och skrift.  Meri... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 31 | ...rande Om du har erfarenhet av **acceptanstest**ledning Om du har erfarenhet a... | x |  |  | 13 | [Acceptanstestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/jYaA_Cwa_Yyf) |
| 32 | ...dning Om du har erfarenhet av **testautomation**.    Ansökan  De frågor som du... |  | x |  | 14 | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| 33 | ...hetsklass. I vissa fall krävs **svenskt medborgarskap** för säkerhetsklassade tjänste... | x |  |  | 21 | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| 34 | ...tsklassade tjänster. Inom vår **IT**-organisation är du med och ut... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 35 | ...ation är du med och utvecklar **Sverige** med hjälp av moderna och inno... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 36 | ... Vi har ett helhetsansvar för **IT** och telekom, en verksamhet so... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 37 | ...verket jobbar vi med att göra **Sverige** närmare. Vi tror på att en fö... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **127** | **390** | 127/390 = **33%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [DevOps, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Bxx7_d5h_9qS) |
|  | x |  | [Ledarskapsförmåga - Verkställande direktör (VD), **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/CfLz_Aex_PkG) |
| x | x | x | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
|  | x |  | [ledarförmåga, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/SVVU_BC5_mQe) |
| x |  |  | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
|  | x |  | [Ledarskapsförmåga - E-commerce manager, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/VevS_VKB_fpn) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [Testledare/QA lead, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bbSi_hWq_H98) |
| x |  |  | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| x | x | x | [Borlänge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cpya_jJg_pGp) |
|  | x |  | [Ledarskapsförmåga - Butiksledare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/dXwA_wgg_k8x) |
| x | x | x | [Testledning, datasystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/dexT_Znd_Yrs) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Acceptanstestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/jYaA_Cwa_Yyf) |
| x | x | x | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
|  | x |  | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
|  | x |  | [Visual Studio.NET, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/ocZE_Fi7_kTv) |
| x | x | x | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| x |  |  | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **10** | 10/21 = **48%** |