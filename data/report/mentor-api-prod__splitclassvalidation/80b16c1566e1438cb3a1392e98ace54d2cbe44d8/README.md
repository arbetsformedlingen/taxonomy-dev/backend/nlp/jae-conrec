# Results for '80b16c1566e1438cb3a1392e98ace54d2cbe44d8'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [80b16c1566e1438cb3a1392e98ace54d2cbe44d8](README.md) | 1 | 4988 | 25 | 12 | 64/361 = **18%** | 4/20 = **20%** |

## Source text

Västra militärregionen söker stf Gruppchef MR Säkerhetsgrupp Huvudsakliga arbetsuppgifter  Som ställföreträdande gruppchef MR säkerhetsgrupp är dinahuvudsakliga uppgifter att planera, genomföra, rapportera och utvärdera arbete som stödjer den regionala säkerhetstjänsten. Din initiala uppgift blir att, tillsammans med gruppchefen, rekrytera, planera och leda gruppens utveckling från noll till en fullt fungerande enhet. Din uppgift kommer i denna initiala utveckling ha fokus på materiel, fordon och lokaler. Detta är en helt ny förbandstyp inom säkerhetstjänsten på regional nivå. Gruppen består av två OR 6 samt sex GSSK.  Du kommer att arbeta med att utveckla och följa upp säkerhetsskyddsfunktionen i samverkan med övriga delar inom FM samt genomföra planeringsuppgifter inom staben.  Gruppens huvudsakliga uppgifter kommer att vara inhämtning och bearbetning av information, bedriva utredningar avsäkerhetshotande verksamhet riktad mot Försvarsmakten, samt delge olika underlag för beslut i den militära säkerhetstjänsten inom Västra militärregionens markterritoriella ansvarsområdeförekommer. Gruppen kommer att ingå initialt i MR Väst stabs underrättelse och säkerhetsavdelning, J 2.  Kvalifikationer   • Svenskt medborgarskap • Yrkesofficer, specialistofficer OR6/7 • Körkort lägst klass B  Meriterande   • Tidigare tjänstgöring vid Säkbat, Undbat, MP bat, TK bat eller FbjS • Tidigare tjänstgöring i säkerhetsbefattning i FM • Utbildad inom FM i säkerhetstjänst • Utbildad inom IT-systemet IS Undsäk • Erfarenhet av att utbilda och föreläsa • Erfarenhet av stabsarbete   Personliga egenskaper Vi söker dig som har vilja att utvecklas och lära dig nya delar inom den militära säkerhetstjänsten. Du är en person med ett gott omdöme, stor integritet och har ett högt säkerhets- och sekretessmedvetande. Du har en hög social förmåga och du finner det naturligt att arbeta utåtriktat tillsammans med andra och har god förmåga att bygga och upprätthålla relationer. I linje med det har du en god förmåga att uttrycka dig i tal och i skrift. Du är pedagogiskt lagd och har god samarbetsförmåga är avstor vikt. Du är en analytisk person som kan identifiera fel, brister och problem i dess olika beståndsdelar. Du är kreativ och ser lösningar på problem. Du har hög arbetskapacitet och arbetar strukturerat med förmåga att självständigt hantera arbetsuppgifter. Ditt engagemang och kunskap behövs för att tillsammans med övriga medarbetare vidareutveckla enheten. Vi tror därför att du trivs i en miljö som ständigt utvecklas och uppskattar omväxlingen i att arbetabåde självständigt såväl som i samarbete med kollegor.  Tidvis är arbetsbelastningen hög vilket innebär att du ska vara stresstålig.  Stor vikt kommer läggas vid personliga egenskaper.  Övrigt: Anställningsform: Yrkesofficer, specialistofficer  Tjänstgöringens omfattning: 100% Arbetsort: Skövde Tillträde: Snarast enligt överenskommelse Lön: Individuell lönesättning  Vill du veta mer om befattningen kontakta C J2, övlt Kenny Sörquist, 0500-46 50 00 (växel).   Fackliga representanter: (Samtliga nås via växel 0500-465000)  OFR/O, Göran Hjert SACO, Charlotta Edman Torstensson OFR/S, Helena Astanius SEKO-F, Lola Ahlgren   Sista ansökningsdag: 2022-08-20    Ansökningar till denna befattning kommer endast tas emot via Försvarsmaktens webbplats.  -------------------------------------------------------- Information om Försvarsmakten och det rekryterande förbandet:  Västra militärregionens område omfattar de fyra länen Halland, Västra Götaland, Värmland och Örebro. Vi utbildar krigsförband samt leder och samordnar territoriell verksamhet över hela hotskalan. Stabens och hemvärnsförbandens höga tillgänglighet samt vår totalförsvarssamverkan med det civila försvaret skapar förutsättningar för insatser med övriga Försvarsmakten, hög funktionalitet och säkerhet i regionen. Läs gärna mer på https://www.forsvarsmakten.se/mrv  I Försvarsmakten finns en stark värdegrund som bygger på öppenhet, resultat och ansvar. Professionell utveckling och personlig hälsa värdesätts och uppmuntras. Det finns goda förutsättningar för intern karriärrörlighet, friskvård och bra balans mellan arbete och privatliv.  Försvarsmakten tillvaratar de kvaliteter som mångfald och jämn könsfördelning tillför verksamheten. Vi välkomnar därför sökanden med olika bakgrund och erfarenheter i våra rekryteringar.   En anställning hos oss innebär placering i säkerhetsklass. Vanligtvis krävs svenskt medborgarskap. Säkerhetsprövning med registerkontroll kommer att genomföras före anställning enligt 3 kap i säkerhetsskyddslagen. Med anställning följer en skyldighet att krigsplaceras. I anställningen ingår även en skyldighet att tjänstgöra utomlands. Innebörden av detta varierar beroende på typ av befattning.  Till ansökan om anställning ska CV och personligt brev bifogas. Om du går vidare i anställningsprocessen ska alltid vidimerade kopior av betyg och intyg uppvisas.  Samtal från externa rekryteringsföretag och säljare undanbedes.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... tillsammans med gruppchefen, **rekrytera**, planera och leda gruppens ut... | x |  |  | 9 | [rekrytera personal, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Q9bs_TeK_AMr) |
| 2 | ...hotande verksamhet riktad mot **Försvarsmakten**, samt delge olika underlag fö... | x |  |  | 14 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 3 | ...ng, J 2.  Kvalifikationer   • **Svenskt medborgarskap** • Yrkesofficer, specialistoff... | x |  |  | 21 | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| 4 | ...r   • Svenskt medborgarskap • **Yrkesofficer**, specialistofficer OR6/7 • Kö... | x | x | 12 | 12 | [Yrkesofficer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/PPsR_fcZ_JCd) |
| 5 | ...medborgarskap • Yrkesofficer, **specialistofficer** OR6/7 • Körkort lägst klass B... | x | x | 17 | 17 | [Specialistofficer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EsHf_niD_7PB) |
| 6 | ...er, specialistofficer OR6/7 • **Körkort** lägst klass B  Meriterande   ... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 6 | ...er, specialistofficer OR6/7 • **Körkort** lägst klass B  Meriterande   ... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 7 | ...officer OR6/7 • Körkort lägst **klass B**  Meriterande   • Tidigare tjä... | x |  |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 8 | ...öring i säkerhetsbefattning i **FM** • Utbildad inom FM i säkerhet... | x |  |  | 2 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 9 | ...fattning i FM • Utbildad inom **FM** i säkerhetstjänst • Utbildad ... | x |  |  | 2 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 10 | ...kerhetstjänst • Utbildad inom **IT**-systemet IS Undsäk • Erfarenh... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 11 | ...IS Undsäk • Erfarenhet av att **utbilda** och föreläsa • Erfarenhet av ... | x |  |  | 7 | [utbilda anställda, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WTx5_vFm_aFM) |
| 12 | ...Erfarenhet av att utbilda och **föreläsa** • Erfarenhet av stabsarbete  ... | x |  |  | 8 | [hålla föreläsningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qnnH_5z6_bfM) |
| 13 | ... strukturerat med förmåga att **självständigt hantera arbetsuppgifter**. Ditt engagemang och kunskap ... | x |  |  | 37 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 14 | ... uppskattar omväxlingen i att **arbeta**både självständigt såväl som i... | x |  |  | 6 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ... omväxlingen i att arbetabåde **självständigt** såväl som i samarbete med kol... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 16 | ...åde självständigt såväl som i **samarbete med kollegor**.  Tidvis är arbetsbelastninge... | x |  |  | 22 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 17 | ...ilket innebär att du ska vara **stresstålig**.  Stor vikt kommer läggas vid... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 18 | ...r.  Övrigt: Anställningsform: **Yrkesofficer**, specialistofficer  Tjänstgör... | x | x | 12 | 12 | [Yrkesofficer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/PPsR_fcZ_JCd) |
| 19 | ...ställningsform: Yrkesofficer, **specialistofficer**  Tjänstgöringens omfattning: ... | x | x | 17 | 17 | [Specialistofficer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EsHf_niD_7PB) |
| 20 | ...  Tjänstgöringens omfattning: **100%** Arbetsort: Skövde Tillträde: ... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 21 | ...s omfattning: 100% Arbetsort: **Skövde** Tillträde: Snarast enligt öve... | x | x | 6 | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 22 | ...-------------- Information om **Försvarsmakten** och det rekryterande förbande... | x |  |  | 14 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 23 | ...---------- Information om Förs**varsmakt**en och det rekryterande förban... |  | x |  | 8 | [Värmlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/EVVp_h6U_GSZ) |
| 24 | ...---------- Information om Förs**varsmakten och **det rekryterande förbandet:  V... |  | x |  | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 25 | ...formation om Försvarsmakten oc**h det r**ekryterande förbandet:  Västra... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 26 | ...smakten och det rekryterande f**örband**et:  Västra militärregionens o... |  | x |  | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 27 | ...ingar för insatser med övriga **Försvarsmakten**, hög funktionalitet och säker... | x |  |  | 14 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 28 | .../www.forsvarsmakten.se/mrv  I **Försvarsmakten** finns en stark värdegrund som... | x |  |  | 14 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 29 | ...värdegrund som bygger på öppen**het, **resultat och ansvar. Professio... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 30 | ...mellan arbete och privatliv.  **Försvarsmakten** tillvaratar de kvaliteter som... | x |  |  | 14 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 31 | ...erhetsklass. Vanligtvis krävs **svenskt medborgarskap**. Säkerhetsprövning med regist... | x |  |  | 21 | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| | **Overall** | | | **64** | **361** | 64/361 = **18%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
|  | x |  | [Värmlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/EVVp_h6U_GSZ) |
| x | x | x | [Specialistofficer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EsHf_niD_7PB) |
| x |  |  | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Yrkesofficer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/PPsR_fcZ_JCd) |
| x |  |  | [rekrytera personal, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Q9bs_TeK_AMr) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [utbilda anställda, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WTx5_vFm_aFM) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| x | x | x | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
|  | x |  | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| x |  |  | [hålla föreläsningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qnnH_5z6_bfM) |
|  | x |  | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
|  | x |  | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| | | **4** | 4/20 = **20%** |