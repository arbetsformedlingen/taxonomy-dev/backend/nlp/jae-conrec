# Results for 'b3bb118dd3a8537524f8e9d8c5bc0824573c0e81'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b3bb118dd3a8537524f8e9d8c5bc0824573c0e81](README.md) | 1 | 2304 | 18 | 16 | 63/297 = **21%** | 5/18 = **28%** |

## Source text

Swedavia söker administrativ utredare inom skadereglering! Om företaget Vi är alltid vakna, dygnet runt årets alla dagar. Det är här resan börjar. Swedavia vill göra mötet mellan människor enkelt och vi vill göra stora avstånd små. Vi bidrar till den tillgänglighet som Sverige behöver för att underlätta resande, affärer och möten. Via våra flygplatser kommer Sverige ut i världen och världen till Sverige. Vi vill att människor ska flyga med gott samvete och därför tar vi sikte på att vara världsledande i hållbarhet och på att driva klimatsmarta flygplatsen.  Om uppdraget  Swedavia Claims hanterar ersättningsanspråk som riktas till någon av Swedavias 10 flygplatser med anledning av en inträffad skada inom bolagets verksamhet.Typen av skador varierar och kan innefatta person-, ansvars-, fordons- eller egendomsskada.  Som skadereglerare inom Claims jobbar du nära verksamheten och ingår i enheten Governance & Compliance som, utöver Claims, inkluderar bolagets legala enhet samt internrevision.I rollen ingår att utreda skador, förhandla och fastställa ersättning inom givna mandat. Personen ifråga ska självständigt hantera och driva skadefrågor där Swedavias ansvar utreds samt reglera fastställda skadeanspråk.  Anställningen är ett konsultuppdrag via oss på Adecco med start omgående, som kommer sträcka sig fram till slutet på december.  Om dig  Personen ifråga ska ha erfarenhet inom utredning och skadereglering från exempelvis försäkringsbolag. Personen ska ha mycket god administrativ och kommunikativ förmåga samt hög förmåga att komma till avslut.God svenska och engelska i tal och skrift. Ha lätt att lära, personen måste efter introduktion snabbt kunna arbeta självständigt med de ärenden som kommer in.  Om ansökan Rekryteringsarbetet till tjänsterna sker löpande. Du ansöker genom att registrera dig via formuläret nedan. Bifoga CV och personligt brev.  Kontaktuppgifter Har du frågor om tjänsten eller rekryteringsprocessen är du varmt välkommen att kontakta ansvarig rekryterare: aziza.mehho@adecco.se via Adecco 020-12 53 88.  Har du frågor angående registrering, var god kontakta supporten via info@adecco.se OBS! Vi tar inte emot ansökningar via mail!  Välkommen med din ansökan!  Sökord Skadereglerare, Utredare, Utredning, Swedavia, Arlanda, Sigtuna, Märsta, Uppsala, Adecco

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Swedavia söker administrativ **utredare** inom skadereglering! Om föret... | x |  |  | 8 | [Skadeutredare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/A4sm_pXR_PaU) |
| 1 | Swedavia söker administrativ **utredare** inom skadereglering! Om föret... |  | x |  | 8 | [Handläggare, offentlig förvaltning/Utredare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YG1s_tUg_jWJ) |
| 2 | ...r till den tillgänglighet som **Sverige** behöver för att underlätta re... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 3 | .... Via våra flygplatser kommer **Sverige** ut i världen och världen till... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ...ut i världen och världen till **Sverige**. Vi vill att människor ska fl... | x |  |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 5 | ...ns- eller egendomsskada.  Som **skadereglerare** inom Claims jobbar du nära ve... |  | x |  | 14 | [Skadereglerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4HnZ_K23_35y) |
| 5 | ...ns- eller egendomsskada.  Som **skadereglerare** inom Claims jobbar du nära ve... | x |  |  | 14 | [skadereglerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/F5rx_vvX_pG4) |
| 6 | ...ar bolagets legala enhet samt **internrevision**.I rollen ingår att utreda ska... |  | x |  | 14 | [Internrevision, **skill**](http://data.jobtechdev.se/taxonomy/concept/TGBA_zbs_yvD) |
| 6 | ...ar bolagets legala enhet samt **internrevision**.I rollen ingår att utreda ska... |  | x |  | 14 | [Internrevision (skyddsföreskrifter), **skill**](http://data.jobtechdev.se/taxonomy/concept/UFmx_AbD_645) |
| 6 | ...ar bolagets legala enhet samt **internrevision**.I rollen ingår att utreda ska... | x | x | 14 | 14 | [internrevision, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Y2Wj_FNn_Msc) |
| 6 | ...ar bolagets legala enhet samt **internrevision**.I rollen ingår att utreda ska... |  | x |  | 14 | [Chef internrevision, **job-title**](http://data.jobtechdev.se/taxonomy/concept/hR5y_37T_feL) |
| 7 | ...rnrevision.I rollen ingår att **utreda skador**, förhandla och fastställa ers... | x |  |  | 13 | [Skadeutredningar, **skill**](http://data.jobtechdev.se/taxonomy/concept/E9eu_hC6_nue) |
| 8 | ...a mandat. Personen ifråga ska **självständigt** hantera och driva skadefrågor... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 9 | ...nspråk.  Anställningen är ett **konsultuppdrag** via oss på Adecco med start o... | x |  |  | 14 | [Konsulttjänster till företag, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/APkH_QHR_9X2) |
| 10 | ...ifråga ska ha erfarenhet inom **utredning** och skadereglering från exemp... | x |  |  | 9 | [Skadeutredare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/A4sm_pXR_PaU) |
| 11 | ...erfarenhet inom utredning och **skadereglering** från exempelvis försäkringsbo... | x |  |  | 14 | [Skadereglering, personskadeförsäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/rSKd_BLd_ACL) |
| 12 | ...åga att komma till avslut.God **svenska** och engelska i tal och skrift... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 13 | ...a till avslut.God svenska och **engelska** i tal och skrift. Ha lätt att... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 14 | ...ter introduktion snabbt kunna **arbeta självständigt** med de ärenden som kommer in.... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ...lkommen att kontakta ansvarig **rekryterare**: aziza.mehho@adecco.se via Ad... | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| 16 | ...mmen med din ansökan!  Sökord **Skadereglerare**, Utredare, Utredning, Swedavi... |  | x |  | 14 | [Skadereglerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4HnZ_K23_35y) |
| 16 | ...mmen med din ansökan!  Sökord **Skadereglerare**, Utredare, Utredning, Swedavi... | x |  |  | 14 | [skadereglerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/F5rx_vvX_pG4) |
| 17 | ...ökan!  Sökord Skadereglerare, **Utredare**, Utredning, Swedavia, Arlanda... | x |  |  | 8 | [Skadeutredare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/A4sm_pXR_PaU) |
| 17 | ...ökan!  Sökord Skadereglerare, **Utredare**, Utredning, Swedavia, Arlanda... |  | x |  | 8 | [Handläggare, offentlig förvaltning/Utredare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YG1s_tUg_jWJ) |
| 18 | ...ord Skadereglerare, Utredare, **Utredning**, Swedavia, Arlanda, Sigtuna, ... | x |  |  | 9 | [Skadeutredningar, **skill**](http://data.jobtechdev.se/taxonomy/concept/E9eu_hC6_nue) |
| 19 | ...Utredning, Swedavia, Arlanda, **Sigtuna**, Märsta, Uppsala, Adecco |  | x |  | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 20 | ...ia, Arlanda, Sigtuna, Märsta, **Uppsala**, Adecco |  | x |  | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| | **Overall** | | | **63** | **297** | 63/297 = **21%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Skadereglerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4HnZ_K23_35y) |
|  | x |  | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| x |  |  | [Skadeutredare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/A4sm_pXR_PaU) |
| x |  |  | [Konsulttjänster till företag, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/APkH_QHR_9X2) |
| x |  |  | [Skadeutredningar, **skill**](http://data.jobtechdev.se/taxonomy/concept/E9eu_hC6_nue) |
| x |  |  | [skadereglerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/F5rx_vvX_pG4) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
|  | x |  | [Internrevision, **skill**](http://data.jobtechdev.se/taxonomy/concept/TGBA_zbs_yvD) |
|  | x |  | [Internrevision (skyddsföreskrifter), **skill**](http://data.jobtechdev.se/taxonomy/concept/UFmx_AbD_645) |
| x | x | x | [internrevision, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Y2Wj_FNn_Msc) |
|  | x |  | [Handläggare, offentlig förvaltning/Utredare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YG1s_tUg_jWJ) |
|  | x |  | [Chef internrevision, **job-title**](http://data.jobtechdev.se/taxonomy/concept/hR5y_37T_feL) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| x |  |  | [Skadereglering, personskadeförsäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/rSKd_BLd_ACL) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/18 = **28%** |