# Results for 'cb8bcf4a36bfa87414b28fb5a57f5caa9a38860b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [cb8bcf4a36bfa87414b28fb5a57f5caa9a38860b](README.md) | 1 | 2562 | 13 | 20 | 60/315 = **19%** | 5/20 = **25%** |

## Source text

Mio söker en originalare till servicekontoret i Tibro. Mio söker alltid de bästa medarbetarna. Varje dag strävar vi efter att tillsammans med våra butiker leverera ett bättre resultat än föregående dag. Vi vet att stolta medarbetare som trivs med arbetet har stor betydelse för hur kunden upplever Mio. Med gemensamma värderingar, tydligt ledarskap, individuellt ansvarstagande och stimulerande arbetsuppgifter erbjuder vi en attraktiv arbetsplats.     Har du erfarenhet inom grafisk formgivning eller arbetat som originalare? Då har vi tjänsten för dig!  Som originalare på Mio kommer du att bli en del av Mios marknadsavdelning på kontoret i Tibro.  Rollen som originalare kommer att bl.a. innebära följande arbetsuppgifter:   - Ta fram olika tryckoriginal som till exempel skyltar, faktablad, vepor med mera i enlighet med företagets grafiska profil.  - Producera annonser  - Vidareutveckla företagets grafiska profil och ge support, service och information till företagets butiker gällande marknadsfrågor.  Som person är det viktigt att du har ett gott öga för grafisk form och typografi. Du kommer att arbeta i ett team om sju medarbetare och för att trivas i rollen tror vi att du ser samarbete och deadlines som naturliga delar i arbetet, liksom stresstålighet vid arbetstoppar.  Struktur, ordningssinne och leveranssäkerhet och att driva ditt arbete framåt självständigt är viktiga egenskaper då du arbetar med flera projekt parallellt och att beställaren får kvalitativt material i tid är en självklarhet för dig.  Kvalifikationer   - Relevant eftergymnasial utbildning eller arbetserfarenhet. • Mycket goda kunskaper i Illustrator, InDesign och Photoshop. • Mycket goda kunskaper och förståelse kring trycksaksproduktion. • Du kan uttrycka dig mycket väl i det svenska språket. • Ett stort plus om du tidigare har jobbat med PIM-system.  Tjänsten är ett vikariat på minst sex månader.  Intervjuer kommer att ske löpande så vänta inte med att skicka din ansökan.      Mio är en ledande svensk detaljhandelskedja för möbler och heminredning som i 60 år har levererat inredningslösningar för hemmet. Vi finns med butiker på över 70 platser i Sverige samt på nätet med e-handel. Mio ägs av handlarna som driver sina butiker framåt utifrån Mios koncept och den lokala marknadens behov. Grunden är en stark och positiv entreprenörskultur. Vi brinner för att förgylla kundernas liv hemma med riktiga möbler och prisvärd kvalitet. Vi vill vara det spännande alternativet som inspirerar kunderna att förnya sina hem som de ska vara stolta över och trivas i.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Mio söker en **originalare** till servicekontoret i Tibro.... | x |  |  | 11 | [Originalare/Final Art, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/SxqC_BSK_D3u) |
| 1 | Mio söker en **originalare** till servicekontoret i Tibro.... |  | x |  | 11 | [Layoutare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSoX_VSS_SpZ) |
| 2 | ...nalare till servicekontoret i **Tibro**. Mio söker alltid de bästa me... |  | x |  | 5 | [Tibro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aLFZ_NHw_atB) |
| 3 | ...dligt ledarskap, individuellt **ansvarstagande** och stimulerande arbetsuppgif... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 4 | ...     Har du erfarenhet inom **grafisk formgivning** eller arbetat som originalare... |  | x |  | 19 | [Grafisk formgivare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/VpYo_k5f_N9R) |
| 4 | ...     Har du erfarenhet inom **grafisk formgivning** eller arbetat som originalare... | x |  |  | 19 | [Grafisk design, **skill**](http://data.jobtechdev.se/taxonomy/concept/dTK5_R7G_p56) |
| 5 | ...formgivning eller arbetat som **originalare**? Då har vi tjänsten för dig! ... | x |  |  | 11 | [Originalare/Final Art, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/SxqC_BSK_D3u) |
| 5 | ...formgivning eller arbetat som **originalare**? Då har vi tjänsten för dig! ... |  | x |  | 11 | [Layoutare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSoX_VSS_SpZ) |
| 6 | ...har vi tjänsten för dig!  Som **originalare** på Mio kommer du att bli en d... | x |  |  | 11 | [Originalare/Final Art, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/SxqC_BSK_D3u) |
| 6 | ...har vi tjänsten för dig!  Som **originalare** på Mio kommer du att bli en d... |  | x |  | 11 | [Layoutare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSoX_VSS_SpZ) |
| 7 | ...rknadsavdelning på kontoret i **Tibro**.  Rollen som originalare ko... |  | x |  | 5 | [Tibro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aLFZ_NHw_atB) |
| 8 | ...ntoret i Tibro.  Rollen som **originalare** kommer att bl.a. innebära föl... | x |  |  | 11 | [Originalare/Final Art, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/SxqC_BSK_D3u) |
| 8 | ...ntoret i Tibro.  Rollen som **originalare** kommer att bl.a. innebära föl... |  | x |  | 11 | [Layoutare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSoX_VSS_SpZ) |
| 9 | ...rliga delar i arbetet, liksom **stresstålighet** vid arbetstoppar.  Struktur, ... |  | x |  | 14 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 10 | ... Kvalifikationer   - Relevant **eftergymnasial utbildning** eller arbetserfarenhet. • Myc... | x | x | 25 | 25 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 11 | ...et. • Mycket goda kunskaper i **Illustrator**, InDesign och Photoshop. • My... | x | x | 11 | 11 | [2D-grafik-Adobe Illustrator, **skill**](http://data.jobtechdev.se/taxonomy/concept/FKub_QFH_Btk) |
| 11 | ...et. • Mycket goda kunskaper i **Illustrator**, InDesign och Photoshop. • My... |  | x |  | 11 | [Adobe Illustrator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bGA6_xqh_Cvv) |
| 12 | ...goda kunskaper i Illustrator, **InDesign** och Photoshop. • Mycket goda ... | x | x | 8 | 8 | [Layout-Adobe InDesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2t4_aS8_tGo) |
| 13 | ...r i Illustrator, InDesign och **Photoshop**. • Mycket goda kunskaper och ... | x | x | 9 | 9 | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
| 14 | ...unskaper och förståelse kring **trycksaksproduktion**. • Du kan uttrycka dig mycket... | x |  |  | 19 | [Trycksaksframställning, **skill**](http://data.jobtechdev.se/taxonomy/concept/PU6Y_hXy_MEA) |
| 15 | ...uttrycka dig mycket väl i det **svenska** språket. • Ett stort plus om ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 16 | ...om du tidigare har jobbat med **PIM-system**.  Tjänsten är ett vikariat på... | x |  |  | 10 | [produktkunskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FC5o_4w6_cLJ) |
| 17 | ...svensk detaljhandelskedja för **möbler** och heminredning som i 60 år ... |  | x |  | 6 | [Möbler, **skill**](http://data.jobtechdev.se/taxonomy/concept/J91r_GLJ_qoa) |
| 18 | ...ljhandelskedja för möbler och **heminredning** som i 60 år har levererat inr... |  | x |  | 12 | [Reparation av möbler och heminredning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/3WSc_qeV_73P) |
| 18 | ...ljhandelskedja för möbler och **heminredning** som i 60 år har levererat inr... |  | x |  | 12 | [Reparation av möbler och heminredning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/tYtX_2Le_g2x) |
| 19 | ... butiker på över 70 platser i **Sverige** samt på nätet med e-handel. M... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 20 | ...r i Sverige samt på nätet med **e-handel**. Mio ägs av handlarna som dri... | x |  |  | 8 | [E-handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yxpW_N32_YMK) |
| 21 | ...ndernas liv hemma med riktiga **möbler** och prisvärd kvalitet. Vi vil... |  | x |  | 6 | [Möbler, **skill**](http://data.jobtechdev.se/taxonomy/concept/J91r_GLJ_qoa) |
| | **Overall** | | | **60** | **315** | 60/315 = **19%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Reparation av möbler och heminredning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/3WSc_qeV_73P) |
| x |  |  | [produktkunskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FC5o_4w6_cLJ) |
| x | x | x | [2D-grafik-Adobe Illustrator, **skill**](http://data.jobtechdev.se/taxonomy/concept/FKub_QFH_Btk) |
|  | x |  | [Möbler, **skill**](http://data.jobtechdev.se/taxonomy/concept/J91r_GLJ_qoa) |
| x |  |  | [Trycksaksframställning, **skill**](http://data.jobtechdev.se/taxonomy/concept/PU6Y_hXy_MEA) |
| x |  |  | [Originalare/Final Art, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/SxqC_BSK_D3u) |
|  | x |  | [Grafisk formgivare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/VpYo_k5f_N9R) |
| x | x | x | [Layout-Adobe InDesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2t4_aS8_tGo) |
|  | x |  | [Tibro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aLFZ_NHw_atB) |
|  | x |  | [Adobe Illustrator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bGA6_xqh_Cvv) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| x |  |  | [Grafisk design, **skill**](http://data.jobtechdev.se/taxonomy/concept/dTK5_R7G_p56) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Layoutare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSoX_VSS_SpZ) |
|  | x |  | [Reparation av möbler och heminredning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/tYtX_2Le_g2x) |
| x | x | x | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
| x |  |  | [E-handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yxpW_N32_YMK) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/20 = **25%** |