# Results for '0ef1b400d10262c171ea0c7f355d6e6769a833a9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0ef1b400d10262c171ea0c7f355d6e6769a833a9](README.md) | 1 | 2390 | 13 | 13 | 93/171 = **54%** | 7/11 = **64%** |

## Source text

Skolkurator Om oss Petrusgruppen består av Stefanskolan i Bromma och Johan Movingers gymnasium i Solna. Skolorna har gemensam elevhälsa där många har tjänst på båda skolorna. Nu söker vi en specialpedagog. Arbetet kommer förläggas på båda skolorna. Stefanskolan är en kristen friskola med årskurs F-9. Skolan är belägen i det trevliga området Ängbyplan i Bromma. Johan Movingers Gymnasium ligger i Råsunda, Solna. Skolan har tre linjer, Natur, Samhälle och Barn & fritid. På Petrusgruppen arbetar vi aktivt med likabehandlingsplanen och har nolltolerans mot mobbning för att skapa en trevlig atmosfär och personlig omsorg för elevernas bästa. Elevhälsoarbetet är högt prioriterat och vi har ett härligt och engagerat elevhälsoteam som älskar att göra skillnad för våra elever och som dessutom ges förutsättning för det.  Vi söker Vi söker en utbildad kurator som brinner för elevhälsa. Tjänsten är deltid och omfattningen 60%. Mindre justeringar av omfattning kan diskuteras.   Elevhälsan arbetar tätt tillsammans och har en strategisk roll i att utveckla, förbättra och att tillsammans göra skolan än mer fantastisk. Vi har höga ambitioner och vill se våra elever lyckas. Som kurator måste du kunna arbeta självständigt. Du måste även kunna arbeta med större strategiska frågor såväl som individnära i kontakten med den enskilde eleven. Du förväntas  Skolans arbetsklimat präglas av gott klimat, engagemang och värme.     Dina kvalifikationer Vi lägger stor vikt vid personliga egenskaper och ser gärna att du delar vår värdegrund. Du ska ha ett stort engagemang och tålamod, vara välplanerad och strukturerad. Du bör/ska: ·       vara utbildad ·       ha erfarenhet av arbete med barn och ungdom ·       vara trygg och tydlig i din roll. ·       vara strukturerad, driven och vara van att ansvara för ett arbetsområde ·       tycka om och ha förmåga att skapa bra relationer med elever och vuxna ·       orka hantera att vara både kravställare och bollplank för lärare och föräldrar ·       älska att göra skillnad för våra elever, att ge dem en god grund och alltid värna deras potential och integritet.     Lön Månads- eller timlön enligt överenskommelse.   Arbetstid Deltid 60%. Vi tillämpar provanställning.   Ansökan Vi tar endast emot ansökningar via mail. Skicka ansökan till: jobb@stefanskolan.se   OBS. Skriv ”Ansökan Skolkurator” i mailrubriken Rekryteringsarbetet sker löpande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Skolkurator** Om oss Petrusgruppen består a... | x | x | 11 | 11 | [Skolkurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/31sh_tuy_pwG) |
| 2 | ... i Bromma och Johan Movingers **gymnasium** i Solna. Skolorna har gemensa... |  | x |  | 9 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 3 | ...h Johan Movingers gymnasium i **Solna**. Skolorna har gemensam elevhä... | x | x | 5 | 5 | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| 4 | ... Solna. Skolorna har gemensam **elevhälsa** där många har tjänst på båda ... |  | x |  | 9 | [Elevhälsa, **skill**](http://data.jobtechdev.se/taxonomy/concept/Sj2n_Kcr_3hm) |
| 5 | ...båda skolorna. Nu söker vi en **specialpedagog**. Arbetet kommer förläggas på ... | x | x | 14 | 14 | [Specialpedagog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/f1zq_tpA_QtN) |
| 6 | ...lan i Bromma. Johan Movingers **Gymnasium** ligger i Råsunda, Solna. Skol... |  | x |  | 9 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 7 | ...s Gymnasium ligger i Råsunda, **Solna**. Skolan har tre linjer, Natur... | x | x | 5 | 5 | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| 8 | ...Vi söker Vi söker en utbildad **kurator** som brinner för elevhälsa. Tj... | x | x | 7 | 7 | [Kurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YpRs_ybt_47a) |
| 9 | ...ildad kurator som brinner för **elevhälsa**. Tjänsten är deltid och omfat... | x | x | 9 | 9 | [Elevhälsa, **skill**](http://data.jobtechdev.se/taxonomy/concept/Sj2n_Kcr_3hm) |
| 10 | ...er för elevhälsa. Tjänsten är **deltid** och omfattningen 60%. Mindre ... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 11 | ...ll se våra elever lyckas. Som **kurator** måste du kunna arbeta självst... | x | x | 7 | 7 | [Kurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YpRs_ybt_47a) |
| 12 | ...s. Som kurator måste du kunna **arbeta självständigt**. Du måste även kunna arbeta m... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 13 | ...rad. Du bör/ska: ·       vara **utbildad** ·       ha erfarenhet av arbe... | x |  |  | 8 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 14 | ...  ha erfarenhet av arbete med **barn och ungdom** ·       vara trygg och tydlig... | x | x | 15 | 15 | [Barn och ungdom, **skill**](http://data.jobtechdev.se/taxonomy/concept/qkyH_MC1_YKU) |
| 15 | ...  tycka om och ha förmåga att **skapa bra relationer** med elever och vuxna ·       ... | x |  |  | 20 | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| 16 | ... överenskommelse.   Arbetstid **Deltid** 60%. Vi tillämpar provanställ... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 17 | ...olan.se   OBS. Skriv ”Ansökan **Skolkurator**” i mailrubriken Rekryteringsa... |  | x |  | 11 | [Skolkurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/31sh_tuy_pwG) |
| | **Overall** | | | **93** | **171** | 93/171 = **54%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Skolkurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/31sh_tuy_pwG) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x |  |  | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Elevhälsa, **skill**](http://data.jobtechdev.se/taxonomy/concept/Sj2n_Kcr_3hm) |
| x | x | x | [Kurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YpRs_ybt_47a) |
| x | x | x | [Specialpedagog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/f1zq_tpA_QtN) |
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x | x | x | [Barn och ungdom, **skill**](http://data.jobtechdev.se/taxonomy/concept/qkyH_MC1_YKU) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x | x | x | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| | | **7** | 7/11 = **64%** |