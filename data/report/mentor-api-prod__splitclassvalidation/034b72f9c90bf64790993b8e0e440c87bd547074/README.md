# Results for '034b72f9c90bf64790993b8e0e440c87bd547074'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [034b72f9c90bf64790993b8e0e440c87bd547074](README.md) | 1 | 1980 | 31 | 26 | 83/392 = **21%** | 4/21 = **19%** |

## Source text

Fräsare till Melament AB i Älmhult Till vårt trevliga kundföretag Melament AB i Älmhult söker vi nu en nyfiken och utvecklingsbar Fräsare med programmeringskunskaper. Arbetet innebär programmering, ställ och körning av en fleroperationsmaskin med heidenhain som styrsystem.  Du fräser mindre serier och ansvarar för allt från ställ till mätning och slutkontroll. Du ingår i ett team med kompetenta och samarbetsvilliga kollegor. Hos Melament får du möjlighet att utvecklas i rollen som Fräsare och kan på sikt även får möjlighet att lära dig fler arbetsmoment.      Arbetstiden är förlagd till dagtid, måndag till torsdag med ledig fredag.  Är du redo för en spännande utmaning på ett mindre och familjärt bolag? Läs gärna mer om vår kund på www.melament.se  Din profil Vi söker dig som har någon form av teknisk utbildning, gärna inom CNC. Utöver detta har du minst ett par års praktisk erfarenhet som Fräsare med goda kunskaper i programmering. Du är van vid att ställa och köra maskiner för kortare serier och har ett högt kvalitetstänk.  Likaså har du ett genuint intresse för att arbeta med ständiga förbättringar, Lean och 5S. Har du erfarenhet av att svarva är det meriterande.     Som person är du en god lagspelare som tar ett stort ansvar för arbetet och som gärna ställer upp och hjälper andra. Lojalitet, nyfikenhet och engagemang, är andra goda egenskaper som kännetecknar ditt sätt att vara och arbeta. Hos vår kund får du kontinuerlig personlig utveckling, något som även du värdesätter högt.  Truck- och traverskort är meriterande, dock inget krav.  Tjänsten är en direktrekrytering till Melament AB i Älmhult!  Vi rekryterar löpande och tjänsten kan komma att tillsättas innan ansökningstiden har gått ut. Du är varmt välkommen med din ansökan redan idag! Du ansöker direkt på www.empleo.se  Uppstart: Omgående eller enligt överenskommelse.   Ort/Placering: Älmhult. Omfattning: Heltid, dagtid.     Lön: Individuell lönesättning tillämpas. Ansökningstid: Snarast.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Fräsare** till Melament AB i Älmhult Ti... |  | x |  | 7 | [Fräsare, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/HMMU_CDh_boZ) |
| 1 | **Fräsare** till Melament AB i Älmhult Ti... |  | x |  | 7 | [fräsare, metall, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/P4jM_K2S_cqH) |
| 1 | **Fräsare** till Melament AB i Älmhult Ti... |  | x |  | 7 | [fräsare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Zd4F_Wyv_6hz) |
| 1 | **Fräsare** till Melament AB i Älmhult Ti... | x | x | 7 | 7 | [Fräsare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sYfN_TaY_zXX) |
| 2 | Fräsare till Melament AB i **Älmhult** Till vårt trevliga kundföreta... | x |  |  | 7 | [Älmhult, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EK6X_wZq_CQ8) |
| 3 | ...iga kundföretag Melament AB i **Älmhult** söker vi nu en nyfiken och ut... | x |  |  | 7 | [Älmhult, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EK6X_wZq_CQ8) |
| 4 | ...en nyfiken och utvecklingsbar **Fräsare** med programmeringskunskaper. ... |  | x |  | 7 | [Fräsare, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/HMMU_CDh_boZ) |
| 4 | ...en nyfiken och utvecklingsbar **Fräsare** med programmeringskunskaper. ... |  | x |  | 7 | [fräsare, metall, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/P4jM_K2S_cqH) |
| 4 | ...en nyfiken och utvecklingsbar **Fräsare** med programmeringskunskaper. ... |  | x |  | 7 | [fräsare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Zd4F_Wyv_6hz) |
| 4 | ...en nyfiken och utvecklingsbar **Fräsare** med programmeringskunskaper. ... | x | x | 7 | 7 | [Fräsare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sYfN_TaY_zXX) |
| 5 | ...ch utvecklingsbar Fräsare med **programmeringskunskaper**. Arbetet innebär programmerin... | x |  |  | 23 | [CNC-programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/JFDz_Uyg_Nmy) |
| 5 | ...ch utvecklingsbar Fräsare med **programmeringskunskaper**. Arbetet innebär programmerin... |  | x |  | 23 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 6 | ...ngskunskaper. Arbetet innebär **programmering**, ställ och körning av en fler... | x |  |  | 13 | [CNC-programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/JFDz_Uyg_Nmy) |
| 6 | ...ngskunskaper. Arbetet innebär **programmering**, ställ och körning av en fler... |  | x |  | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 7 | ...ring, ställ och körning av en **fleroperationsmaskin** med heidenhain som styrsystem... | x |  |  | 20 | [Fleroperationsmaskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/6feF_emv_eji) |
| 8 | ...v en fleroperationsmaskin med **heidenhain** som styrsystem.  Du fräser mi... | x |  |  | 10 | [CNC-programmering, Heidenhain, **skill**](http://data.jobtechdev.se/taxonomy/concept/fgNG_pGF_8Tf) |
| 9 | ...eidenhain som styrsystem.  Du **fräser** mindre serier och ansvarar fö... | x |  |  | 6 | [Fräsning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/wpVJ_UXf_uSb) |
| 10 | ...et att utvecklas i rollen som **Fräsare** och kan på sikt även får möjl... |  | x |  | 7 | [Fräsare, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/HMMU_CDh_boZ) |
| 10 | ...et att utvecklas i rollen som **Fräsare** och kan på sikt även får möjl... |  | x |  | 7 | [fräsare, metall, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/P4jM_K2S_cqH) |
| 10 | ...et att utvecklas i rollen som **Fräsare** och kan på sikt även får möjl... |  | x |  | 7 | [fräsare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Zd4F_Wyv_6hz) |
| 10 | ...et att utvecklas i rollen som **Fräsare** och kan på sikt även får möjl... | x | x | 7 | 7 | [Fräsare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sYfN_TaY_zXX) |
| 11 | ...ker dig som har någon form av **teknisk utbildning**, gärna inom CNC. Utöver detta... | x | x | 18 | 18 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 12 | ...eknisk utbildning, gärna inom **CNC**. Utöver detta har du minst et... | x |  |  | 3 | [Industri och verkstadsteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/B5uL_xe6_kMn) |
| 12 | ...eknisk utbildning, gärna inom **CNC**. Utöver detta har du minst et... |  | x |  | 3 | [CNC, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zXvT_Gvb_mPj) |
| 13 | ...NC. Utöver detta har du minst **ett par års** praktisk erfarenhet som Fräsa... | x |  |  | 11 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 14 | ...r års praktisk erfarenhet som **Fräsare** med goda kunskaper i programm... |  | x |  | 7 | [Fräsare, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/HMMU_CDh_boZ) |
| 14 | ...r års praktisk erfarenhet som **Fräsare** med goda kunskaper i programm... |  | x |  | 7 | [fräsare, metall, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/P4jM_K2S_cqH) |
| 14 | ...r års praktisk erfarenhet som **Fräsare** med goda kunskaper i programm... |  | x |  | 7 | [fräsare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Zd4F_Wyv_6hz) |
| 14 | ...r års praktisk erfarenhet som **Fräsare** med goda kunskaper i programm... | x | x | 7 | 7 | [Fräsare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sYfN_TaY_zXX) |
| 15 | ... Fräsare med goda kunskaper i **programmering**. Du är van vid att ställa och... | x |  |  | 13 | [CNC-programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/JFDz_Uyg_Nmy) |
| 15 | ... Fräsare med goda kunskaper i **programmering**. Du är van vid att ställa och... |  | x |  | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 16 | ...t intresse för att arbeta med **ständiga förbättringar**, Lean och 5S. Har du erfarenh... | x | x | 22 | 22 | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| 17 | ...a med ständiga förbättringar, **Lean** och 5S. Har du erfarenhet av ... | x | x | 4 | 4 | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| 18 | ... 5S. Har du erfarenhet av att **svarva** är det meriterande.     Som p... | x |  |  | 6 | [Svarvning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/d2yX_cZM_fSy) |
| 19 | ... vår kund får du kontinuerlig **personlig utveckling**, något som även du värdesätte... |  | x |  | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 20 | ...om även du värdesätter högt.  **Truck**- och traverskort är meriteran... | x |  |  | 5 | [Truckvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/pGts_PKK_c8W) |
| 21 | ...om även du värdesätter högt.  **Truck- oc**h traverskort är meriterande, ... |  | x |  | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 22 | ...värdesätter högt.  Truck- och **traverskort** är meriterande, dock inget kr... | x | x | 11 | 11 | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
| 23 | ...ekrytering till Melament AB i **Älmhult**!  Vi rekryterar löpande och t... | x |  |  | 7 | [Älmhult, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EK6X_wZq_CQ8) |
| 24 | ...enskommelse.   Ort/Placering: **Älmhult**. Omfattning: Heltid, dagtid. ... | x |  |  | 7 | [Älmhult, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EK6X_wZq_CQ8) |
| 25 | ...acering: Älmhult. Omfattning: **Heltid**, dagtid.     Lön: Individuell... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **83** | **392** | 83/392 = **21%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Fleroperationsmaskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/6feF_emv_eji) |
| x | x | x | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| x |  |  | [Industri och verkstadsteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/B5uL_xe6_kMn) |
| x |  |  | [Älmhult, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EK6X_wZq_CQ8) |
|  | x |  | [Fräsare, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/HMMU_CDh_boZ) |
| x |  |  | [CNC-programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/JFDz_Uyg_Nmy) |
| x | x | x | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
| x | x | x | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
|  | x |  | [fräsare, metall, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/P4jM_K2S_cqH) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
|  | x |  | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
|  | x |  | [fräsare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Zd4F_Wyv_6hz) |
| x |  |  | [Svarvning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/d2yX_cZM_fSy) |
| x |  |  | [CNC-programmering, Heidenhain, **skill**](http://data.jobtechdev.se/taxonomy/concept/fgNG_pGF_8Tf) |
|  | x |  | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x |  |  | [Truckvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/pGts_PKK_c8W) |
| x | x | x | [Fräsare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sYfN_TaY_zXX) |
|  | x |  | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| x |  |  | [Fräsning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/wpVJ_UXf_uSb) |
|  | x |  | [CNC, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zXvT_Gvb_mPj) |
| | | **4** | 4/21 = **19%** |