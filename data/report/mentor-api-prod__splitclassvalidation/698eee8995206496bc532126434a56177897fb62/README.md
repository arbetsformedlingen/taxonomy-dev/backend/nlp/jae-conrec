# Results for '698eee8995206496bc532126434a56177897fb62'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [698eee8995206496bc532126434a56177897fb62](README.md) | 1 | 2469 | 11 | 14 | 56/159 = **35%** | 6/17 = **35%** |

## Source text

Content Scheduler TV/Film Till teamet Product Management TV söker bolaget nu en Content Scheduler som kan genomföra kurering av innehåll i tv-tjänsterna med målet att erbjuda en förbättrad kundupplevelse. Tjänsten är ett konsultuppdrag.    Huvudsakliga ansvarsområden: •	Säkerställa att rätt programinformation (inkl bilder) finns på rätt titel, samt höjning av nivån på metadata. •	Säkerställa att ingest av premiärer och avsnitt i populära serier blir ingestade i tid och med rätt kvalitet (t.ex. undertexter, ljud och bild). •	Kurera presentation av innehåll i TV-tjänster överlag, dvs. skapa och förvalta promoytor, medialistor. •	Planera presentation av nya, viktiga titlar samt kampanjer tillsammans Produktchef TV, Content Presentation. •	Vid behov kommunicera med innehålls- och metadataleverantörer om felaktigheter i ingestat material. •	Upprätthålla lanseringsplan ("Monthly Content highlights") och kommunicera med Marketing och Säljstöd. •	Omvärldsbevakning, ta intryck och inspiration från hur andra tjänster i Sverige och internationellt presenterar innehåll. •	Kravställning på utveckling av redaktionella verktyg samt automatiserade rekommendationsflöden. •	Koordinera hyrfilmskampanjer samt frivisningar, etc. •	Med hjälp av data och A/B-testning optimera relevansen av presentation för kunder och därmed öka tittande.    Arbetet kommer ske i nära samarbete med övriga personer inom produktteamet samt med teknikteamet.    Vi söker någon som har: •	Ett mycket stort intresse för tv och streaming. •	Koll på omvärlden för att veta vilka aktuella skeenden som driver tittande (t.ex. stora val, Nobelpris, boksläpp, etc.). •	Förmåga att se tjänsten ur olika kundsegments perspektiv. •	En dataorienterad inställning. •	Mycket goda svenskakunskaper (skriftligen och muntligen). •	Viss erfarenhet av bildbehandling.   Den person vi söker är även: •	Noggrann och detaljorienterad. •	En teamplayer med mycket god förmåga att samarbeta med UX-utvecklare, content partners, teknikavdelning.   Fördelaktigt är även: •	Erfarenhet av liknande arbetsuppgifter •	Erfarenhet av planerings- och presentationssystem •	Erfarenhet av att jobba med XML och Json. •	Akademisk examen inom relevant område.   Start: Augusti/September Slut: 6 månader Plats: Stockholm Arbetsbelastning: 100% Språk: Goda kunskaper i Svenska. Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...a att ingest av premiärer och **avsnitt** i populära serier blir ingest... | x | x | 7 | 7 | [Avsnitt/Dramaserier, **skill**](http://data.jobtechdev.se/taxonomy/concept/gNjv_tRy_1RK) |
| 2 | ...ar samt kampanjer tillsammans **Produktchef** TV, Content Presentation. •	V... |  | x |  | 11 | [produktchef, marknadsföring, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/B8gQ_9Td_wft) |
| 2 | ...ar samt kampanjer tillsammans **Produktchef** TV, Content Presentation. •	V... |  | x |  | 11 | [produktchef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/TYqb_9cj_yLi) |
| 2 | ...ar samt kampanjer tillsammans **Produktchef** TV, Content Presentation. •	V... |  | x |  | 11 | [Produktchef, marknadsföring, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ooRg_ZrL_uVU) |
| 2 | ...ar samt kampanjer tillsammans **Produktchef** TV, Content Presentation. •	V... |  | x |  | 11 | [produktchef inom turism, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tLEs_6mD_fa3) |
| 3 | ...ion från hur andra tjänster i **Sverige** och internationellt presenter... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ...isningar, etc. •	Med hjälp av **data** och A/B-testning optimera rel... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 5 | ...Ett mycket stort intresse för **tv** och streaming. •	Koll på omvä... | x |  |  | 2 | [TV, **skill**](http://data.jobtechdev.se/taxonomy/concept/YBuN_Wgv_19X) |
| 6 | ...ad inställning. •	Mycket goda **svenskakunskaper** (skriftligen och muntligen). ... | x | x | 16 | 16 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 7 | ...tligen). •	Viss erfarenhet av **bildbehandling**.   Den person vi söker är äve... |  | x |  | 14 | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
| 8 | ...en person vi söker är även: •	**Noggrann** och detaljorienterad. •	En te... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 9 | ...god förmåga att samarbeta med **UX**-utvecklare, content partners,... | x |  |  | 2 | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| 10 | ...•	Erfarenhet av att jobba med **XML** och Json. •	Akademisk examen ... | x | x | 3 | 3 | [XML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/s8Te_rH8_RMU) |
| 11 | ...att jobba med XML och Json. •	**Akademisk **examen inom relevant område.  ... |  | x |  | 10 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 12 | ...att jobba med XML och Json. •	**Akademisk examen** inom relevant område.   Start... |  | x |  | 16 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 13 | ...med XML och Json. •	Akademisk **examen** inom relevant område.   Start... | x | x | 6 | 6 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 14 | ...tember Slut: 6 månader Plats: **Stockholm** Arbetsbelastning: 100% Språk:... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 15 | ...: Stockholm Arbetsbelastning: **100%** Språk: Goda kunskaper i Svens... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 16 | ... 100% Språk: Goda kunskaper i **Svenska**. Öppen för alla Vi fokuserar ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **56** | **159** | 56/159 = **35%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [produktchef, marknadsföring, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/B8gQ_9Td_wft) |
| x | x | x | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
|  | x |  | [produktchef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/TYqb_9cj_yLi) |
| x |  |  | [TV, **skill**](http://data.jobtechdev.se/taxonomy/concept/YBuN_Wgv_19X) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
|  | x |  | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
| x | x | x | [Avsnitt/Dramaserier, **skill**](http://data.jobtechdev.se/taxonomy/concept/gNjv_tRy_1RK) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [Produktchef, marknadsföring, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ooRg_ZrL_uVU) |
| x | x | x | [XML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/s8Te_rH8_RMU) |
|  | x |  | [produktchef inom turism, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tLEs_6mD_fa3) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/17 = **35%** |