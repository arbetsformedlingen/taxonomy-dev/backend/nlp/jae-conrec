# Results for '42334249caccbe4098740846ba1757bbb3d3395f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [42334249caccbe4098740846ba1757bbb3d3395f](README.md) | 1 | 3474 | 20 | 27 | 84/410 = **20%** | 4/29 = **14%** |

## Source text

Lagermedarbetare - INTERSPORT Centrallager Till Intersports centrallager i Nässjö söker vi nu drivna, engagerade och motiverade lagermedarbetare vars attityd och kunskap kommer vara avgörande i vår strävan mot att skapa en köpupplevelse i världsklass – hela vägen från centrallagret till våra kunder.   Är du redo att ge dig in i matchen?   Tjänsterna är tillsvidareanställningar (provanställning 6 första månaderna) på 100%.   Arbetsplatsen  Lagret organiseras utifrån tre olika avdelningar (Inleverans, Utleverans samt Textiltryckeri) som leds av arbetsledare och gruppledare. De huvudsakliga arbetsuppgifterna på de olika avdelningarna är enligt nedan:  Inleverans: Lossning av bil eller container, sortering av gods (både kolli och pall) samt inkörning av gods antingen till hylla eller vår automation.   Utleverans: Orderplock med plocktruck, orderplock i automationen, sortering samt utlastning.   Tryckeri: Arbete med textiltryck av föreningskläder, väskor och annan utrustning vid tryckpress.   Som lagermedarbetare kommer du ha ett varierat arbete då flexibiliteten är väldigt viktig och man arbetar på olika avdelningar beroende på säsong.   Kvalifikationer  Har du truckkort, arbetat vid en Autostore eller jobbat med textiltryck tidigare är detta ett stort plus.   Bakgrund från annat lagerarbete är meriterande, särskilt om det har varit inom detaljhandeln (textil). Har du arbetat i butik är det en stor fördel, i synnerhet om du har erfarenhet och grundkunskap i Intersports sortiment.   Personliga egenskaper  Vilja, Våga, Vinna, Vi är ett Intersport. Detta är ord som präglar vår vardag och funderar du på att söka den här tjänsten är det viktigt att de passar in på dig. Vi ska våga utmana oss själva och varandra, vi ska vara stolta över det vi gör och framförallt ska vi jobba som ett lag.   Vi på Intersport är passionerade, engagerade och tävlingsinriktade. Vi har alltid ett kund- och resultatfokus samt strävar efter ständig utveckling genom att utmana oss själva och teamet vi arbetar inom. Utifrån våran värdegrund och målbild strävar vi alltid efter att skapa en köpupplevelse i världsklass!  Vi söker dig som är en lagspelare, som vill framåt samt uppmuntrar och lyfter dina kollegor. Du är ett föredöme i arbetet, skapar förtroende och bidrar till ett öppet arbetsklimat där människor inspireras och vågar utmana. På Intersport är personliga egenskaper och rätt inställning minst lika viktigt som erfarenhet och utbildning.  Din ansökan  Vill du bli en av oss? Ansök redan idag! I din ansökan ber vi dig inkludera CV och personligt brev. Vi tillämpar provanställning.   Har du frågor angående tjänsten är du välkommen att maila ansvarig chef joakim.nielsen@intersport.se (vi tar inte emot ansökningar och CV via mail).   Välkommen med din ansökan!  Om Intersport  Intersport Sverige AB är licenstagare och aktieägare i Intersport International Corporation som är världens största sportkedja med 5 500 butiker i 44 länder. 2018 omsattes över 11,5 miljarder euro globalt. Intersport Sverige samarbetar med över 1200 föreningar inom bl.a. fotboll, hockey och friidrott och uppmuntrar till så mycket sport och gemenskap som möjligt.  Intersport Sverige bildades 1968 och är ett av Sveriges mest kända varumärken med ca 100 butiker. Sedan 2014 ingår även Löplabbet i Intersport-koncernen med 8 butiker över landet. Intersport är den ledande omnikanal- återförsäljaren inom sport genom en köpupplevelse oavsett om den startar digitalt eller i butiken.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Lagermedarbetare** - INTERSPORT Centrallager Til... | x | x | 16 | 16 | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
| 2 | ...ll Intersports centrallager i **Nässjö** söker vi nu drivna, engagerad... | x | x | 6 | 6 | [Nässjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KfXT_ySA_do2) |
| 3 | ...na, engagerade och motiverade **lagermedarbetare** vars attityd och kunskap komm... | x | x | 16 | 16 | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
| 4 | ...in i matchen?   Tjänsterna är **tillsvidareanställningar** (provanställning 6 första mån... | x |  |  | 24 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 5 | ...llning 6 första månaderna) på **100%**.   Arbetsplatsen  Lagret orga... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 6 | ...är enligt nedan:  Inleverans: **Lossning** av bil eller container, sorte... | x | x | 8 | 8 | [utföra lastning och lossning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VVCW_HBb_BLF) |
| 7 | ...t nedan:  Inleverans: Lossning** av bil eller container**, sortering av gods (både koll... | x |  |  | 23 | [utföra lastning och lossning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VVCW_HBb_BLF) |
| 8 | ...sning av bil eller container, **sortering** av gods (både kolli och pall)... |  | x |  | 9 | [sortering av fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/CKTg_SGv_vEB) |
| 8 | ...sning av bil eller container, **sortering** av gods (både kolli och pall)... |  | x |  | 9 | [traditioner för sortering av djurdelar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DrvX_LVa_Qzf) |
| 8 | ...sning av bil eller container, **sortering** av gods (både kolli och pall)... |  | x |  | 9 | [sköta sortering och uppläggning av virke, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/djQw_ZkM_xEH) |
| 9 | ...ds (både kolli och pall) samt **inkörning av gods** antingen till hylla eller vår... | x |  |  | 17 | [placera gods i lager med precision, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/oiGy_b8D_8rA) |
| 10 | ...vår automation.   Utleverans: **Orderplock** med plocktruck, orderplock i ... | x |  |  | 10 | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
| 11 | ...s: Orderplock med plocktruck, **orderplock** i automationen, sortering sam... | x |  |  | 10 | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
| 12 | ...k, orderplock i automationen, **sortering** samt utlastning.   Tryckeri: ... |  | x |  | 9 | [sortering av fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/CKTg_SGv_vEB) |
| 12 | ...k, orderplock i automationen, **sortering** samt utlastning.   Tryckeri: ... |  | x |  | 9 | [traditioner för sortering av djurdelar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DrvX_LVa_Qzf) |
| 12 | ...k, orderplock i automationen, **sortering** samt utlastning.   Tryckeri: ... |  | x |  | 9 | [sköta sortering och uppläggning av virke, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/djQw_ZkM_xEH) |
| 13 | ... automationen, sortering samt **utlastning**.   Tryckeri: Arbete med texti... | x |  |  | 10 | [lasta gods, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kPtM_QXs_APM) |
| 14 | ...tning.   Tryckeri: Arbete med **textiltryck** av föreningskläder, väskor oc... | x | x | 11 | 11 | [Textiltryck, **skill**](http://data.jobtechdev.se/taxonomy/concept/vSry_1wJ_PPt) |
| 15 | ...xtiltryck av föreningskläder, **väskor** och annan utrustning vid tryc... |  | x |  | 6 | [Specialiserad butikshandel med väskor, reseffekter och lädervaror, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/dWLs_ftH_1JN) |
| 15 | ...xtiltryck av föreningskläder, **väskor** och annan utrustning vid tryc... |  | x |  | 6 | [Mode, skor och väskor/Design, skor och väskor, **skill**](http://data.jobtechdev.se/taxonomy/concept/k1Rq_EmU_nbj) |
| 16 | ...ustning vid tryckpress.   Som **lagermedarbetare** kommer du ha ett varierat arb... | x | x | 16 | 16 | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
| 17 | ...ng.   Kvalifikationer  Har du **truckkort**, arbetat vid en Autostore ell... |  | x |  | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 17 | ...ng.   Kvalifikationer  Har du **truckkort**, arbetat vid en Autostore ell... | x |  |  | 9 | [Truckvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/pGts_PKK_c8W) |
| 18 | ...en Autostore eller jobbat med **textiltryck** tidigare är detta ett stort p... | x | x | 11 | 11 | [Textiltryck, **skill**](http://data.jobtechdev.se/taxonomy/concept/vSry_1wJ_PPt) |
| 19 | ...re är detta ett stort plus.   **Bakgrund från annat lagerarbete** är meriterande, särskilt om d... | x |  |  | 31 | [Terminal- och lagerarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/ijHY_aHk_ADE) |
| 20 | ...t plus.   Bakgrund från annat **lagerarbete** är meriterande, särskilt om d... |  | x |  | 11 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 21 | ...etaljhandeln (textil). Har du **arbetat i butik** är det en stor fördel, i synn... | x |  |  | 15 | [Butiksvana, fackhandel, **skill**](http://data.jobtechdev.se/taxonomy/concept/Td4F_YtH_u6S) |
| 22 | ...ln (textil). Har du arbetat i **butik** är det en stor fördel, i synn... |  | x |  | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 23 | ...rhet om du har erfarenhet och **grundkunskap** i Intersports sortiment.   Pe... |  | x |  | 12 | [RTL-kompetens, grundkompetens, **skill**](http://data.jobtechdev.se/taxonomy/concept/ucBi_odb_sAy) |
| 24 | ... miljarder euro globalt. Inter**sport** Sverige samarbetar med över 1... |  | x |  | 5 | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
| 25 | ...er 1200 föreningar inom bl.a. **fotboll**, hockey och friidrott och upp... |  | x |  | 7 | [Fotboll, instruktör/Fotboll, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/6f5o_GNH_RsW) |
| 25 | ...er 1200 föreningar inom bl.a. **fotboll**, hockey och friidrott och upp... |  | x |  | 7 | [Fotboll, professionell utövning, **skill**](http://data.jobtechdev.se/taxonomy/concept/e18k_813_76y) |
| 25 | ...er 1200 föreningar inom bl.a. **fotboll**, hockey och friidrott och upp... |  | x |  | 7 | [fotboll, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tf5i_LdS_UBa) |
| 26 | ...öreningar inom bl.a. fotboll, **hockey** och friidrott och uppmuntrar ... |  | x |  | 6 | [Hockey, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1iYr_HSS_4XR) |
| 27 | ...nom bl.a. fotboll, hockey och **friidrott** och uppmuntrar till så mycket... |  | x |  | 9 | [Friidrott, instruktör/Friidrott, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/PKu8_rPy_C7A) |
| 27 | ...nom bl.a. fotboll, hockey och **friidrott** och uppmuntrar till så mycket... |  | x |  | 9 | [Friidrott, professionell utövning, **skill**](http://data.jobtechdev.se/taxonomy/concept/rXJg_kya_ktB) |
| 28 | ...skap som möjligt.  Intersport **Sverige** bildades 1968 och är ett av S... | x |  |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 29 | ...e bildades 1968 och är ett av **Sveriges** mest kända varumärken med ca ... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 30 | ...d 8 butiker över landet. Inter**sport** är den ledande omnikanal- åte... |  | x |  | 5 | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
| | **Overall** | | | **84** | **410** | 84/410 = **20%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Hockey, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1iYr_HSS_4XR) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Fotboll, instruktör/Fotboll, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/6f5o_GNH_RsW) |
|  | x |  | [sortering av fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/CKTg_SGv_vEB) |
|  | x |  | [traditioner för sortering av djurdelar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DrvX_LVa_Qzf) |
|  | x |  | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x | x | x | [Nässjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KfXT_ySA_do2) |
|  | x |  | [Friidrott, instruktör/Friidrott, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/PKu8_rPy_C7A) |
| x |  |  | [Butiksvana, fackhandel, **skill**](http://data.jobtechdev.se/taxonomy/concept/Td4F_YtH_u6S) |
| x | x | x | [utföra lastning och lossning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VVCW_HBb_BLF) |
|  | x |  | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x |  |  | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
|  | x |  | [Specialiserad butikshandel med väskor, reseffekter och lädervaror, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/dWLs_ftH_1JN) |
|  | x |  | [sköta sortering och uppläggning av virke, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/djQw_ZkM_xEH) |
|  | x |  | [Fotboll, professionell utövning, **skill**](http://data.jobtechdev.se/taxonomy/concept/e18k_813_76y) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Terminal- och lagerarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/ijHY_aHk_ADE) |
|  | x |  | [Mode, skor och väskor/Design, skor och väskor, **skill**](http://data.jobtechdev.se/taxonomy/concept/k1Rq_EmU_nbj) |
| x |  |  | [lasta gods, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kPtM_QXs_APM) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [placera gods i lager med precision, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/oiGy_b8D_8rA) |
|  | x |  | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
| x |  |  | [Truckvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/pGts_PKK_c8W) |
|  | x |  | [Friidrott, professionell utövning, **skill**](http://data.jobtechdev.se/taxonomy/concept/rXJg_kya_ktB) |
| x | x | x | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
|  | x |  | [fotboll, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tf5i_LdS_UBa) |
|  | x |  | [RTL-kompetens, grundkompetens, **skill**](http://data.jobtechdev.se/taxonomy/concept/ucBi_odb_sAy) |
| x | x | x | [Textiltryck, **skill**](http://data.jobtechdev.se/taxonomy/concept/vSry_1wJ_PPt) |
|  | x |  | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| | | **4** | 4/29 = **14%** |