# Results for 'c67d49ffa3e0f878f903a65e2a9d5fcecac44167'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c67d49ffa3e0f878f903a65e2a9d5fcecac44167](README.md) | 1 | 3373 | 28 | 19 | 122/338 = **36%** | 10/20 = **50%** |

## Source text

Kock/måltidspersonal 100 % till Bemanningspoolen Tillsammans bygger vi Piteå. Det händer mycket positivt i Piteå. Befolkningen ökar, stadskärnan utvecklas och flera intressanta utvecklingsprojekt är på gång. I arbetet för ett attraktivt och hållbart samhälle erbjuder Piteå kommun följande tjänst:  Måltidsservice uppdrag är att producera och leverera välsmakande och näringsriktiga måltider till kommunens kärnverksamheter. Vi utför detta med etiska och miljömässiga värderingar på ett professionellt sätt och till rätt kvalitet. I Piteå kommuns förskolor, skolor och inom äldreomsorgen produceras ca   10 000 portioner varje dag.  Som anställd i Måltidsservice bemanningspool får du tillfälle att arbeta i olika kök med varierande arbetsuppgifter under kortare perioder. Du får ett omväxlande och intressant uppdrag med möjlighet att aktivt bidra till utvecklingen av Måltidsservice verksamhet. Alla arbetsuppgifter är förekommande i köken. Du roterar i första hand på våra slutberedningskök inom skola och förskola. Vid behov ska du även kunna arbeta i produktions- och tillagningskök med tillhörande elevrestauranger.   Som anställd i bemanningspoolen täcker du upp för kortare frånvaro i Måltidsservice kök. Du kommer att arbeta ensam eller i grupp. Arbetsuppgifter och arbetsplatser kan variera från dag till dag.  Arbetsuppgifter/arbetsbeskrivning  I arbetsuppgifterna ingår att slutbereda och servera dagens olika lunchalternativ, tillreda en varierad salladsbuffé samt tillreda/förbereda och servera frukost och mellanmål (även alternativ anpassade för barn med behov av specialkost). Du ansvarar för en säker livsmedelshantering samt för egenkontroll, god hygien, disk och städ i köket. Vidare ingår beställning av mat från produktionsköket samt beställning av varor i e-handel efter gällande avtal och inom ramen för fastställd budget. I det dagliga arbetet sker dialog med såväl pedagoger som barn; ett gott bemötande är därför betydelsefullt.  Kvalifikationer  Restaurangutbildning på gymnasienivå eller likvärdig utbildning som arbetsgivaren finner lämplig och/eller att du har dokumenterad erfarenhet som kock eller måltidspersonal inom storkök och/eller restaurang. Tillräckliga kunskaper i svenska, både i tal och i skrift, för säker hantering i storkök. Goda kunskaper inom data och de program som används i arbetet är meriterande. Köksarbete är ett fysiskt och ibland tungt arbete och ställer därför krav på god fysik.  Du har god kunskap om:   • Slutberedning • Råvaror • Specialkost • Hygien • Egenkontroll • Städ • Ergonomiskt arbete i verksamheten • Storköksutrustning  Personlig lämplighet  Du har förmåga att anpassa arbetet efter verksamhetens behov och har en helhetssyn på uppdraget. Du följer de kostpolitiska riktlinjerna och gällande rutiner samt arbetar mot uppsatta mål. Du är professionell och serviceinriktad samt lyhörd för gästernas önskemål. Du är van att organisera och utföra ditt arbete självständigt och bidrar aktivt till ett gott arbetsklimat samt till erfarenhetsutbyte både inom arbetsgruppen och med andra yrkesgrupper.  Mer information och kontakt  Semestertjänst mån-fre. Heltidsmått 40 timmar/vecka.  Tillträde 2022-10-01 eller enligt överenskommelse.  För tjänsten krävs B-körkort och tillgång till bil.  För ytterligare information kontakta: Petra Rankvist, enhetschef 0911-696507   Veronica Lundgren Kommunal 0911-934 18

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kock**/måltidspersonal 100 % till Be... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 2 | Kock/**måltidspersonal** 100 % till Bemanningspoolen T... |  | x |  | 15 | [Måltidspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/QSfa_6x5_9y7) |
| 3 | Kock/måltidspersonal **100 %** till Bemanningspoolen Tillsam... | x |  |  | 5 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 4 | ...spoolen Tillsammans bygger vi **Piteå**. Det händer mycket positivt i... | x | x | 5 | 5 | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| 5 | ... Det händer mycket positivt i **Piteå**. Befolkningen ökar, stadskärn... | x | x | 5 | 5 | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| 6 | ...ch hållbart samhälle erbjuder **Piteå** kommun följande tjänst:  Målt... | x | x | 5 | 5 | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| 7 | ...llbart samhälle erbjuder Piteå** kommun** följande tjänst:  Måltidsserv... | x |  |  | 7 | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| 8 | ... näringsriktiga måltider till **kommunens** kärnverksamheter. Vi utför de... | x |  |  | 9 | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| 9 | ...ätt och till rätt kvalitet. I **Piteå** kommuns förskolor, skolor och... | x | x | 5 | 5 | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| 10 | ...ch till rätt kvalitet. I Piteå** kommuns** förskolor, skolor och inom äl... | x |  |  | 8 | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| 11 | ...ätt kvalitet. I Piteå kommuns **förskolor**, skolor och inom äldreomsorge... | x |  |  | 9 | [Förskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/6o2S_cvR_GwA) |
| 12 | ...ns förskolor, skolor och inom **äldreomsorgen** produceras ca   10 000 portio... | x | x | 13 | 13 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 13 | ...tberedningskök inom skola och **förskola**. Vid behov ska du även kunna ... | x |  |  | 8 | [Förskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/6o2S_cvR_GwA) |
| 13 | ...tberedningskök inom skola och **förskola**. Vid behov ska du även kunna ... |  | x |  | 8 | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| 14 | ...passade för barn med behov av **specialkost**). Du ansvarar för en säker li... | x | x | 11 | 11 | [Specialkost, **skill**](http://data.jobtechdev.se/taxonomy/concept/JYKn_PRX_tif) |
| 15 | ...för egenkontroll, god hygien, **disk** och städ i köket. Vidare ingå... | x |  |  | 4 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| 16 | ...ontroll, god hygien, disk och **städ** i köket. Vidare ingår beställ... | x |  |  | 4 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 17 | ...t samt beställning av varor i **e-handel** efter gällande avtal och inom... | x |  |  | 8 | [E-handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yxpW_N32_YMK) |
| 18 | ...delsefullt.  Kvalifikationer  **Restaurang**utbildning på gymnasienivå ell... |  | x |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 19 | ...delsefullt.  Kvalifikationer  **Restaurangutbildning** på gymnasienivå eller likvärd... | x | x | 20 | 20 | [Restaurangutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QZWP_ef1_KWF) |
| 20 | ...oner  Restaurangutbildning på **gymnasienivå** eller likvärdig utbildning so... | x |  |  | 12 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 21 | ...r dokumenterad erfarenhet som **kock** eller måltidspersonal inom st... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 22 | ...rad erfarenhet som kock eller **måltidspersonal** inom storkök och/eller restau... |  | x |  | 15 | [Måltidspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/QSfa_6x5_9y7) |
| 23 | ...ck eller måltidspersonal inom **storkök** och/eller restaurang. Tillräc... | x | x | 7 | 7 | [Storkök, **skill**](http://data.jobtechdev.se/taxonomy/concept/GQ9R_2Za_Awt) |
| 24 | ...rsonal inom storkök och/eller **restaurang**. Tillräckliga kunskaper i sve... | x |  |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 25 | ...ang. Tillräckliga kunskaper i **svenska**, både i tal och i skrift, för... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 26 | ...skrift, för säker hantering i **storkök**. Goda kunskaper inom data och... | x | x | 7 | 7 | [Storkök, **skill**](http://data.jobtechdev.se/taxonomy/concept/GQ9R_2Za_Awt) |
| 27 | ...te och ställer därför krav på **god fysik**.  Du har god kunskap om:   • ... | x | x | 9 | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 28 | ...  • Slutberedning • Råvaror • **Specialkost** • Hygien • Egenkontroll • Stä... | x | x | 11 | 11 | [Specialkost, **skill**](http://data.jobtechdev.se/taxonomy/concept/JYKn_PRX_tif) |
| 29 | ...ost • Hygien • Egenkontroll • **Städ** • Ergonomiskt arbete i verksa... | x |  |  | 4 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 30 | ...ygien • Egenkontroll • Städ • **Ergonomiskt arbete i verksamheten** • Storköksutrustning  Personl... | x |  |  | 33 | [arbeta ergonomiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Z5ew_DGT_av8) |
| 31 | ...tt organisera och utföra ditt **arbete självständigt** och bidrar aktivt till ett go... | x |  |  | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 32 | ...takt  Semestertjänst mån-fre. **Heltidsmått 40 timmar/vecka**.  Tillträde 2022-10-01 eller ... | x |  |  | 27 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 33 | ...kommelse.  För tjänsten krävs **B-körkort** och tillgång till bil.  För y... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| | **Overall** | | | **122** | **338** | 122/338 = **36%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Förskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/6o2S_cvR_GwA) |
| x | x | x | [Storkök, **skill**](http://data.jobtechdev.se/taxonomy/concept/GQ9R_2Za_Awt) |
| x | x | x | [Specialkost, **skill**](http://data.jobtechdev.se/taxonomy/concept/JYKn_PRX_tif) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Måltidspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/QSfa_6x5_9y7) |
| x | x | x | [Restaurangutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QZWP_ef1_KWF) |
| x | x | x | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| x |  |  | [arbeta ergonomiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Z5ew_DGT_av8) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
|  | x |  | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| x |  |  | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| x | x | x | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| x |  |  | [E-handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yxpW_N32_YMK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **10** | 10/20 = **50%** |