# Results for '41d77150d02d0fe8ef9b73e408cb6c6aa3842612'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [41d77150d02d0fe8ef9b73e408cb6c6aa3842612](README.md) | 1 | 2067 | 20 | 16 | 126/219 = **58%** | 8/12 = **67%** |

## Source text

Bilmekaniker/-tekniker på Jonsson Bil & Trailer AB Jonsson Bil & Trailer AB rekryterar: Bilmekaniker/-tekniker   Vi söker en Fordonstekniker Jonsson Bil stärker nu upp inför framtiden och söker tekniker/mekaniker. Bilbranschen står inför en häftig omställning och en ny tids mobilitet i både kundperspektiv och kundupplevelse, digitalisering fortskrider i hög takt, nya drivmedel med elektrifiering som fokus, nya sätt att äga sina bilar på, och vikten av enkelhet och stöttning i bil-ägandet.  Vi växer, utökar med fler nya varumärken och breddar vårt utbud och behöver stärka personalstyrkan i Örebro. Är du erfaren bil- / lastbilsmekaniker eller är du nyutbildad och vill växa och utvecklas är du välkommen till oss på Jonsson Bil.  Som fordonstekniker hos oss på Jonsson Bil förstärker du serviceteamet och blir en viktig del i vår målsättning ”Örebros mest nöjda kunder”. Som person är du noggrann, strukturerad och en lagspelare. Arbetet är mångsidigt med arbete såväl med nybilar som begagnade bilar och flera moment som hör bilen till.  Tjänsten är i Örebro. Tjänsten är tillsvidare och 100%  Till din hjälp har du ett härligt och mycket strukturerat team i en välskött och fin bilanläggning. Du har stor möjlighet till utveckling och arbete med hög frihetskänsla. Jonsson Bil ingår idag i en företagsgrupp med totalt 5 stora bilanläggningar i Örebro, Norrköping, Jönköping, Eskilstuna, Köping och representerar nybilsmärken Peugeot, DS, Citroen, Opel och Mitsubishi.   Är du den vi söker? För eventuella frågor kontakta Verkstadschef Johanna Helisten på 0704-453614 eller mail johanna.helisten@jonssonbil.se  Ansök vi länken eller skicka ditt personliga brev och CV till johanna.helisten@jonssonbil.se  Vi är en auktoriserad Peugeot, DS Automobiles, Opel och Mitsubishi återförsäljare med 30 medarbetare och vår verksamhet består av försäljning, service och reparationer samt reservdelar. En stor del av vår försäljning är även begagnade bilar av alla märken. Vårt distrikt är Örebro Län. Jonsson Bil har funnits på samma plats i Örebro sedan starten 1977.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bilmekaniker**/-tekniker på Jonsson Bil & Tr... | x | x | 12 | 12 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 2 | Bilmekaniker/-**tekniker** på Jonsson Bil & Trailer AB J... | x |  |  | 8 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 3 | ... Bil & Trailer AB rekryterar: **Bilmekaniker**/-tekniker   Vi söker en For... | x | x | 12 | 12 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 4 | ... AB rekryterar: Bilmekaniker/-**tekniker**   Vi söker en Fordonsteknik... | x |  |  | 8 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 5 | ...ker/-tekniker   Vi söker en **Fordonstekniker** Jonsson Bil stärker nu upp in... | x | x | 15 | 15 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 6 | ...upp inför framtiden och söker **tekniker**/mekaniker. Bilbranschen står ... | x |  |  | 8 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 7 | ... framtiden och söker tekniker/**mekaniker**. Bilbranschen står inför en h... | x |  |  | 9 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 8 | ...över stärka personalstyrkan i **Örebro**. Är du erfaren bil- / lastbil... | x | x | 6 | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 9 | ...yrkan i Örebro. Är du erfaren **bil-** / lastbilsmekaniker eller är ... | x |  |  | 4 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 10 | ... Örebro. Är du erfaren bil- / **lastbilsmekaniker** eller är du nyutbildad och vi... | x | x | 17 | 17 | [Lastbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/o7LP_duC_u9Z) |
| 11 | ...till oss på Jonsson Bil.  Som **fordonstekniker** hos oss på Jonsson Bil förstä... | x | x | 15 | 15 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 12 | ...jda kunder”. Som person är du **noggrann**, strukturerad och en lagspela... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 13 | ...ör bilen till.  Tjänsten är i **Örebro**. Tjänsten är tillsvidare och ... | x | x | 6 | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 14 | ...sten är i Örebro. Tjänsten är **tillsvidare** och 100%  Till din hjälp har ... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 15 | .... Tjänsten är tillsvidare och **100%**  Till din hjälp har du ett hä... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 16 | ...alt 5 stora bilanläggningar i **Örebro**, Norrköping, Jönköping, Eskil... | x | x | 6 | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 17 | ...ora bilanläggningar i Örebro, **Norrköping**, Jönköping, Eskilstuna, Köpin... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 18 | ...bilanläggningar i Örebro, Norr**köping**, Jönköping, Eskilstuna, Köpin... |  | x |  | 6 | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| 19 | ...gningar i Örebro, Norrköping, **Jönköping**, Eskilstuna, Köping och repre... | x | x | 9 | 9 | [Jönköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KURg_KJF_Lwc) |
| 20 | ...rebro, Norrköping, Jönköping, **Eskilstuna**, Köping och representerar nyb... | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 21 | ...öping, Jönköping, Eskilstuna, **Köping** och representerar nybilsmärke... | x |  |  | 6 | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| 22 | ...ör eventuella frågor kontakta **Verkstadschef** Johanna Helisten på 0704-4536... |  | x |  | 13 | [Verkstadschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/azkV_of2_7GG) |
| 23 | ...alla märken. Vårt distrikt är **Örebro Län**. Jonsson Bil har funnits på s... |  | x |  | 10 | [Örebro län, **region**](http://data.jobtechdev.se/taxonomy/concept/xTCk_nT5_Zjm) |
| 24 | ... har funnits på samma plats i **Örebro** sedan starten 1977. |  | x |  | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| | **Overall** | | | **126** | **219** | 126/219 = **58%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Köping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4Taz_AuG_tSm) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Jönköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KURg_KJF_Lwc) |
| x | x | x | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
|  | x |  | [Verkstadschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/azkV_of2_7GG) |
| x | x | x | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| x | x | x | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Lastbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/o7LP_duC_u9Z) |
|  | x |  | [Örebro län, **region**](http://data.jobtechdev.se/taxonomy/concept/xTCk_nT5_Zjm) |
| | | **8** | 8/12 = **67%** |