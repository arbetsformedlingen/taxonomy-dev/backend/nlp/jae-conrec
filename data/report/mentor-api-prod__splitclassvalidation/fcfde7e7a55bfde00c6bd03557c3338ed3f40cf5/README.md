# Results for 'fcfde7e7a55bfde00c6bd03557c3338ed3f40cf5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [fcfde7e7a55bfde00c6bd03557c3338ed3f40cf5](README.md) | 1 | 516 | 9 | 6 | 50/100 = **50%** | 4/7 = **57%** |

## Source text

Pizzabagare sökes  Vi söker nu dig som pizzabagare! Du ska ha erfarenhet av baka pizza, vara pålitlig och sträva efter att göra ett bra jobb. Du behöver vara stresstålig, trevlig och anpassningsbar. Du ska även kunna följa instruktioner, vara självständig men även kunna samarbeta. Uppgifter är att Baka pizzor, göra deg, Städa, Diska, hantera stress och Servera. Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Pizzabagare** sökes  Vi söker nu dig som pi... | x | x | 11 | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 2 | ...re sökes  Vi söker nu dig som **pizzabagare**! Du ska ha erfarenhet av baka... | x | x | 11 | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 3 | ...gare! Du ska ha erfarenhet av **baka pizza**, vara pålitlig och sträva eft... | x |  |  | 10 | [Pizzabakning, **skill**](http://data.jobtechdev.se/taxonomy/concept/1abT_LRk_fg2) |
| 3 | ...gare! Du ska ha erfarenhet av **baka pizza**, vara pålitlig och sträva eft... |  | x |  | 10 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 4 | ...ett bra jobb. Du behöver vara **stresstålig**, trevlig och anpassningsbar. ... | x | x | 11 | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 5 | ...nna följa instruktioner, vara **självständig** men även kunna samarbeta. Upp... | x | x | 12 | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 6 | ...a samarbeta. Uppgifter är att **Baka pizzor**, göra deg, Städa, Diska, hant... | x |  |  | 11 | [Pizzabakning, **skill**](http://data.jobtechdev.se/taxonomy/concept/1abT_LRk_fg2) |
| 7 | ...är att Baka pizzor, göra deg, **Städa**, Diska, hantera stress och Se... | x |  |  | 5 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 8 | ...Baka pizzor, göra deg, Städa, **Diska**, hantera stress och Servera. ... | x | x | 5 | 5 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 9 | ...zzor, göra deg, Städa, Diska, **hantera stress** och Servera. Öppen för alla V... | x |  |  | 14 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | **Overall** | | | **50** | **100** | 50/100 = **50%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Pizzabakning, **skill**](http://data.jobtechdev.se/taxonomy/concept/1abT_LRk_fg2) |
| x | x | x | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
|  | x |  | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| x | x | x | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **4** | 4/7 = **57%** |