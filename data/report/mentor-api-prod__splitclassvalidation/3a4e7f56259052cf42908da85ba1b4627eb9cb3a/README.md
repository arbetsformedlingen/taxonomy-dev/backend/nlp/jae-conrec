# Results for '3a4e7f56259052cf42908da85ba1b4627eb9cb3a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [3a4e7f56259052cf42908da85ba1b4627eb9cb3a](README.md) | 1 | 1285 | 9 | 12 | 42/154 = **27%** | 3/10 = **30%** |

## Source text

Kantor till Falkenbergs församling Till Falkenbergs församling i Falkenbergs pastorat söker vi dig som vill bli en del av våra goda musikaliska förmågor.  Som kantor i Falkenbergs församling arbetar du i våra gudstjänster och vid kyrkliga handlingar. Du kommer också att leda kör för både barn och vuxna där du står för planering och genomförande. Tjänsten lämnar utrymme för orgelelever i linje med vår målsättning att erbjuda fler möjligheter till undervisning i orgel.  Församlingen har ett nära samarbete med Skrea församling och bildar tillsammans ett av pastoratets fyra organisatoriska områden. Församlingarna rymmer en stor musikalisk bredd med många möjligheter i kreativa miljöer. I vårt arbetslag har vi organister, kantorer och musikpedagog som tillsammans med dig erbjuder en mängd olika musikaliska sammanhang som kör, lovsångskör, kompgrupp och konserter.  Kvalifikationer  Du har en kantorsexamen med några års erfarenhet av att arbeta i församling. Du är medlem i Svenska kyrkan och delar vår tro. Som person har du en väl utvecklad social och pedagogisk förmåga samtidigt som du är en van och trygg ledare. Vi ser det som meriterande om du tidigare haft orgelelever.  Vi tillämpar löpande urval och tjänsten kan därför komma att tillsättas före sista ansökningsdag.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kantor** till Falkenbergs församling T... | x | x | 6 | 6 | [Kantor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WoT4_oqQ_5Fa) |
| 2 | Kantor till Falkenbergs **församling** Till Falkenbergs församling i... |  | x |  | 10 | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
| 3 | ...s församling Till Falkenbergs **församling** i Falkenbergs pastorat söker ... |  | x |  | 10 | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
| 4 | ... musikaliska förmågor.  Som **kantor** i Falkenbergs församling arbe... | x | x | 6 | 6 | [Kantor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WoT4_oqQ_5Fa) |
| 5 | ....  Som kantor i Falkenbergs **församling** arbetar du i våra gudstjänste... |  | x |  | 10 | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
| 6 | ...ndlingar. Du kommer också att **leda kör** för både barn och vuxna där d... | x |  |  | 8 | [Körledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/cGaM_QVm_wL5) |
| 7 | .... Tjänsten lämnar utrymme för **orgel**elever i linje med vår målsätt... |  | x |  | 5 | [Orgel, **skill**](http://data.jobtechdev.se/taxonomy/concept/7Y8z_fyF_TpK) |
| 8 | ...r till undervisning i orgel.  **Församling**en har ett nära samarbete med ... |  | x |  | 10 | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
| 9 | ...jöer. I vårt arbetslag har vi **organister**, kantorer och musikpedagog so... | x | x | 10 | 10 | [Organist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DNDp_1sY_ZrR) |
| 10 | ... arbetslag har vi organister, **kantorer** och musikpedagog som tillsamm... | x | x | 8 | 8 | [Kantor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WoT4_oqQ_5Fa) |
| 11 | ...r vi organister, kantorer och **musikpedagog** som tillsammans med dig erbju... | x | x | 12 | 12 | [Musikpedagog/Musiklärare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zrQ9_1z2_kT3) |
| 12 | ....  Kvalifikationer  Du har en **kantorsexamen** med några års erfarenhet av a... | x |  |  | 13 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 13 | ...rs erfarenhet av att arbeta i **församling**. Du är medlem i Svenska kyrka... |  | x |  | 10 | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
| 14 | ... i församling. Du är medlem i **Svenska** kyrkan och delar vår tro. Som... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 15 | ...u en väl utvecklad social och **pedagogisk förmåga** samtidigt som du är en van oc... | x |  |  | 18 | [Pedagogiskt förhållningssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/qMfa_U8e_Lto) |
| 16 | ...riterande om du tidigare haft **orgelelever**.  Vi tillämpar löpande urval ... | x |  |  | 11 | [Övriga musiklärare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/c7pv_Ahd_J8r) |
| | **Overall** | | | **42** | **154** | 42/154 = **27%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Orgel, **skill**](http://data.jobtechdev.se/taxonomy/concept/7Y8z_fyF_TpK) |
| x | x | x | [Organist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DNDp_1sY_ZrR) |
| x |  |  | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| x | x | x | [Kantor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WoT4_oqQ_5Fa) |
| x |  |  | [Övriga musiklärare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/c7pv_Ahd_J8r) |
| x |  |  | [Körledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/cGaM_QVm_wL5) |
| x |  |  | [Pedagogiskt förhållningssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/qMfa_U8e_Lto) |
|  | x |  | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [Musikpedagog/Musiklärare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zrQ9_1z2_kT3) |
| | | **3** | 3/10 = **30%** |