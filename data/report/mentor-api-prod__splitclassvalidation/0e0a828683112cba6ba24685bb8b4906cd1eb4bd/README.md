# Results for '0e0a828683112cba6ba24685bb8b4906cd1eb4bd'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0e0a828683112cba6ba24685bb8b4906cd1eb4bd](README.md) | 1 | 1060 | 10 | 9 | 83/126 = **66%** | 2/5 = **40%** |

## Source text

Nagelteknolog Motala till avancerad skönhetsklinik Dermani växer så det knakar och vi vi planerar därför att öppna en skönhetsklinik i Motala. Vi söker dig som är öppen, social och som kan hålla ordning och reda samt har ett intresse för skönhet och de egenskaper som är av värde för kunderna. Vi kommer kontinuerligt att vidareutbilda oss i takt med att vi tar in fler maskiner och nya protokoll så det är viktigt att du vill lära dig och utvecklas i din roll som nagelteknolog. Just nu så söker vi dig som är erfaren nagelteknolog som kan ingå i vårt team.  Dermani erbjuder en professionell men också avslappnad arbetsmiljö med trevliga kollegor som alla har nära till skratt. Vi önskar primärt en heltidsanställd nagelteknolog men tjänsten är anpassningsbar och du kan välja del- eller heltid i samråd med övriga Dermani-teamet. Tycker du att det verkar intressant och vill veta mer? Skicka ett mail med CV, personligt brev henrik@dermani.se och märk mailet med "Nagelteknolog Motala". Tjänsterna startar Q4 så vi ber dig redan nu att ta en första kontakt.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Nagelteknolog** Motala till avancerad skönhet... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 2 | Nagelteknolog **Motala** till avancerad skönhetsklinik... | x | x | 6 | 6 | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| 3 | ...att öppna en skönhetsklinik i **Motala**. Vi söker dig som är öppen, s... | x | x | 6 | 6 | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| 4 | ... och utvecklas i din roll som **nagelteknolog**. Just nu så söker vi dig som ... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 5 | ...å söker vi dig som är erfaren **nagelteknolog** som kan ingå i vårt team.  De... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 6 | ...ssionell men också avslappnad **arbetsmiljö** med trevliga kollegor som all... |  | x |  | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 7 | ... skratt. Vi önskar primärt en **heltidsanställd** nagelteknolog men tjänsten är... | x |  |  | 15 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 8 | ...ar primärt en heltidsanställd **nagelteknolog** men tjänsten är anpassningsba... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 9 | ...passningsbar och du kan välja **del- eller heltid** i samråd med övriga Dermani-t... | x |  |  | 17 | (not found in taxonomy) |
| 10 | ...rmani.se och märk mailet med "**Nagelteknolog** Motala". Tjänsterna startar Q... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 11 | ...ärk mailet med "Nagelteknolog **Motala**". Tjänsterna startar Q4 så vi... | x | x | 6 | 6 | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| | **Overall** | | | **83** | **126** | 83/126 = **66%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| x | x | x | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| x |  |  | (not found in taxonomy) |
|  | x |  | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **2** | 2/5 = **40%** |