# Results for '302ec3944e8160f054efb425020a361b5eacebcd'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [302ec3944e8160f054efb425020a361b5eacebcd](README.md) | 1 | 3448 | 19 | 11 | 62/265 = **23%** | 5/22 = **23%** |

## Source text

Ledigt Jobb som industriarbetare i Sandviken \| Lediga jobb Just nu söker vi på Manpower dig som vill ta dig an ett jobb inom industri hos en av våra attraktiva kunder i Sandviken. Förutom det faktum att du får jobba för en spännande aktör är det bästa med det här jobbet att ständigt ställas inför nya och spännande utmaningar. Låter det intressant? Varmt välkommen med din ansökan! #JobbJustNu  Det här är en visstidsanställning med goda chanser till förlängning. Vi har löpande urval vilket innebär att vi kallar till intervjuer löpande under tiden i stället för att vänta tills ansökningstiden har gått ut.  Arbetsuppgifter  Du kommer jobba för ett globalt och världsledande företag inom industri för metallbearbetning, maskiner och verktyg, tjänster och tekniska lösningar för Gruv och anläggningsindustrin. Du kommer kunna arbeta på olika avdelningar, där arbetsuppgifterna kan variera. Det kan exempelvis vara montering, svetsning, målning eller att man jobbar som processoperatör. Det förekommer även truckkörning.  Att arbeta som konsult via Manpower innebär både spännande och lärorika arbetsdagar, och vi lägger stor vikt vid att alla konsulter blir en del av företaget från dag ett.  Är du rätt person för jobbet?  Det här jobbet passar dig som är driven, engagerad och motiveras av att få ta eget ansvar. Det dagliga arbetet kräver att du är samarbetsvillig, har viljan att lära dig nytt och är flexibel. Vidare är det väldigt viktigt att du besitter ett säkerhetstänk. För att komma in i rollen så snabbt som möjligt är det ett stort plus om du har erfarenhet av industriarbete, men det är inget krav.  Vi söker dig som   * har gymnasieexamen  * har goda kunskaper i svenska i tal och skrift  * har körkort och tillgång till bil  * har truckkort  Det är meriterande om du   * har traverskort  * gått en teknisk utbildning  * har erfarenhet från industri  Du ska vara flexibel och kunna jobba allt från 2-skift till 5 skift.   Erbjudande     Rent formellt bygger en anställning hos oss på samma lagar och regler som på den övriga arbetsmarknaden. Vi har kollektivavtal, med allt vad det innebär vad gäller försäkringar, tjänstepension och fackliga relationer. Det som skiljer sig är att du blir anställd hos oss, men arbetar på olika uppdrag hos våra kunder. Du får chansen att arbeta i olika branscher, med olika arbetsuppgifter och med olika människor. Som konsult erbjuds du även friskvårdsbidrag samt utöver det får alla Manpowers anställda fri tillgång till över 4500 interaktiva utbildningar via vår e-learningportal PowerYOU.  Givetvis har du en konsultchef som finns tillgänglig för dig under hela din anställningsperiod och som är ansvarig för dina uppdrag, att du trivs på din arbetsplats och att du utvecklas i din yrkesroll.   Sök tjänsten idag!  Den här tjänsten innebär att du kommer att bli anställd av Manpower men du kommer att tillbringa dina arbetsdagar hos vår kund. Vid frågor är du välkommen att kontakta ansvarig rekryterare Payman Abdullah via e-post: Payman.abdullah@manpower.se  Vi ser fram emot att höra från dig!  Om Manpower  Manpower är Sveriges ledande rekryterings- och bemanningsföretag med tiotusentals attraktiva arbetsgivare över hela landet som våra kunder. Med vårt omfattande nätverk, lokalt såväl som globalt, kan vi erbjuda mängder av spännande lediga jobb som hjälper dig att bygga din karriär, både på kort och lång sikt. Hos oss kan du söka heltidsjobb eller extrajobb vid sidan av studierna.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...t Jobb som industriarbetare i **Sandviken** \| Lediga jobb Just nu söker v... | x | x | 9 | 9 | [Sandviken, **municipality**](http://data.jobtechdev.se/taxonomy/concept/BbdN_xLB_k6s) |
| 2 | ...n av våra attraktiva kunder i **Sandviken**. Förutom det faktum att du få... | x | x | 9 | 9 | [Sandviken, **municipality**](http://data.jobtechdev.se/taxonomy/concept/BbdN_xLB_k6s) |
| 3 | ...n! #JobbJustNu  Det här är en **visstidsanställning** med goda chanser till förläng... | x |  |  | 19 | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| 4 | ...nde företag inom industri för **metallbearbetning**, maskiner och verktyg, tjänst... | x |  |  | 17 | [metallbearbetning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LFCL_RzT_xoF) |
| 5 | ...dustri för metallbearbetning, **maskiner** och verktyg, tjänster och tek... | x |  |  | 8 | [maskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Ka3C_YyD_qTA) |
| 6 | ...tallbearbetning, maskiner och **verktyg**, tjänster och tekniska lösnin... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 7 | ...er och tekniska lösningar för **Gruv** och anläggningsindustrin. Du ... | x |  |  | 4 | [Gruvarbetare, gruvor och stenbrott, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/VE9W_SnC_Uk2) |
| 8 | ...an exempelvis vara montering, **svetsning**, målning eller att man jobbar... |  | x |  | 9 | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| 8 | ...an exempelvis vara montering, **svetsning**, målning eller att man jobbar... | x |  |  | 9 | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| 9 | ...is vara montering, svetsning, **målning** eller att man jobbar som proc... | x |  |  | 7 | [Målare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/QKvX_v7r_PNL) |
| 10 | ...operatör. Det förekommer även **truckkörning**.  Att arbeta som konsult via ... | x |  |  | 12 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 11 | ... plus om du har erfarenhet av **industriarbete**, men det är inget krav.  Vi s... | x | x | 14 | 14 | [Industriarbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fpA8_XFc_122) |
| 12 | ...av.  Vi söker dig som   * har **gymnasieexamen**  * har goda kunskaper i svens... |  | x |  | 14 | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| 12 | ...av.  Vi söker dig som   * har **gymnasieexamen**  * har goda kunskaper i svens... | x |  |  | 14 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 13 | ...xamen  * har goda kunskaper i **svenska** i tal och skrift  * har körko... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 14 | ...enska i tal och skrift  * har **körkort** och tillgång till bil  * har ... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 14 | ...enska i tal och skrift  * har **körkort** och tillgång till bil  * har ... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 15 | ... och tillgång till bil  * har **truckkort**  Det är meriterande om du   *... | x | x | 9 | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 16 | ... är meriterande om du   * har **traverskort**  * gått en teknisk utbildning... |  | x |  | 11 | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
| 16 | ... är meriterande om du   * har **traverskort**  * gått en teknisk utbildning... | x |  |  | 11 | [Traversförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/mBA1_h3T_Aaw) |
| 17 | ... * har traverskort  * gått en **teknisk utbildning**  * har erfarenhet från indust... |  | x |  | 18 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 17 | ... * har traverskort  * gått en **teknisk utbildning**  * har erfarenhet från indust... | x |  |  | 18 | (not found in taxonomy) |
| 18 | ...vriga arbetsmarknaden. Vi har **kollektivavtal**, med allt vad det innebär vad... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 19 | ...lkommen att kontakta ansvarig **rekryterare** Payman Abdullah via e-post: P... | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| | **Overall** | | | **62** | **265** | 62/265 = **23%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| x | x | x | [Sandviken, **municipality**](http://data.jobtechdev.se/taxonomy/concept/BbdN_xLB_k6s) |
|  | x |  | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| x |  |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| x |  |  | [maskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Ka3C_YyD_qTA) |
| x |  |  | [metallbearbetning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LFCL_RzT_xoF) |
|  | x |  | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
|  | x |  | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| x |  |  | [Målare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/QKvX_v7r_PNL) |
| x |  |  | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| x |  |  | [Gruvarbetare, gruvor och stenbrott, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/VE9W_SnC_Uk2) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | (not found in taxonomy) |
| x | x | x | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x | x | x | [Industriarbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fpA8_XFc_122) |
| x |  |  | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| x |  |  | [Traversförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/mBA1_h3T_Aaw) |
| x |  |  | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/22 = **23%** |