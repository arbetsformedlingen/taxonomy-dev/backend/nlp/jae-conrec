# Results for 'f065abb6d137b1b4caf11072c4309811e8086a91'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f065abb6d137b1b4caf11072c4309811e8086a91](README.md) | 1 | 1463 | 12 | 7 | 14/288 = **5%** | 1/11 = **9%** |

## Source text

Som lärarassistent arbetar du tillsammans med lärare i ett arbetslag. Du möter många olika människor, förstår deras behov och vet på vilket sätt du bäst hjälper till, avlastar och underlättar. Det kan också handla om att förebygga, medla i och lösa konflikter. På så sätt får du bidra till ett trivsamt arbetsklimat som är gynnsamt för alla i skolverksamheten.  Lärarassistentens administrativa arbetsuppgifter är bland annat att ansvara för dokumentation samt rapport- och resultatåtergivning och registrera frånvaro. Gentemot elever kan det handla om att stödja en elev under en skoldag, eller vara stöttande i mindre grupper under lektioner samt kontakta vårdnadshavare och hjälpa läraren i förberedelsen av lektioner och i efterarbetet. Dessutom kan lärarassistenten arbeta med skolans pedagogiska program, appar och molntjänster, sköta rapportering, dokumentation och följa upp likabehandlingsarbete och kränkningsärenden som ett led i det systematiska kvalitetsarbetet.  Kurser i utbildningen:        Elevhälsan och funktionsvarierade barns förutsättningar och behov,  20 p      Examensarbete,  10 p      Inkludering och integration,  10 p      Kommunikation,  15 p      Ledarskap, pedagogiska metoder och didaktiska grunder,  30 p      Likabehandlingsarbete och konflikthantering,  20 p      Pedagogiska program och appar och skoladministrativa system,  30 p      Skolans organisation och styrning,  15 p      LIA – Utveckling av och i yrkesrollen,  50 p  

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Som **lärarassistent** arbetar du tillsammans med lä... | x | x | 14 | 14 | [Lärarassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Zfgc_BYU_HM2) |
| 1 | Som **lärarassistent** arbetar du tillsammans med lä... |  | x |  | 14 | [lärarassistent, gymnasieskola, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/aYJA_MPb_VYM) |
| 1 | Som **lärarassistent** arbetar du tillsammans med lä... |  | x |  | 14 | [lärarassistent, förskola, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bQv3_LXN_Hbi) |
| 1 | Som **lärarassistent** arbetar du tillsammans med lä... |  | x |  | 14 | [lärarassistent, grundskola, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/pGQu_Cng_kae) |
| 2 | ...r bland annat att ansvara för **dokumentation** samt rapport- och resultatåte... | x |  |  | 13 | [tillhandahålla dokumentation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5uMi_uQS_9Sd) |
| 2 | ...r bland annat att ansvara för **dokumentation** samt rapport- och resultatåte... |  | x |  | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 3 | ...nsvara för dokumentation samt **rapport- och resultatåtergivning** och registrera frånvaro. Gent... | x |  |  | 32 | [rapporter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/a1ma_sf5_oi2) |
| 4 | ...- och resultatåtergivning och **registrera frånvaro**. Gentemot elever kan det hand... | x |  |  | 19 | [registrera närvaro, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mKBh_6Nh_kNX) |
| 5 | ...tjänster, sköta rapportering, **dokumentation** och följa upp likabehandlings... |  | x |  | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 6 | ...Kurser i utbildningen:        **Elevhälsan och funktionsvarierade barns förutsättningar och behov**,  20 p      Examensarbete,  1... | x |  |  | 65 | [Barn och ungdom, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/eqNC_6Yu_GTs) |
| 7 | ...    Examensarbete,  10 p      **Inkludering och integration**,  10 p      Kommunikation,  1... | x |  |  | 27 | [Barn och ungdom, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/eqNC_6Yu_GTs) |
| 8 | ...    Likabehandlingsarbete och **konflikthantering**,  20 p      Pedagogiska progr... |  | x |  | 17 | [konflikthantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/N47j_j55_R2J) |
| 9 | ...nistrativa system,  30 p      **Skolans organisation och styrning**,  15 p      LIA – Utveckling ... | x |  |  | 33 | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
| | **Overall** | | | **14** | **288** | 14/288 = **5%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [tillhandahålla dokumentation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5uMi_uQS_9Sd) |
| x |  |  | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
|  | x |  | [konflikthantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/N47j_j55_R2J) |
| x | x | x | [Lärarassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Zfgc_BYU_HM2) |
| x |  |  | [rapporter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/a1ma_sf5_oi2) |
|  | x |  | [lärarassistent, gymnasieskola, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/aYJA_MPb_VYM) |
|  | x |  | [lärarassistent, förskola, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bQv3_LXN_Hbi) |
| x |  |  | [Barn och ungdom, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/eqNC_6Yu_GTs) |
|  | x |  | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x |  |  | [registrera närvaro, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mKBh_6Nh_kNX) |
|  | x |  | [lärarassistent, grundskola, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/pGQu_Cng_kae) |
| | | **1** | 1/11 = **9%** |