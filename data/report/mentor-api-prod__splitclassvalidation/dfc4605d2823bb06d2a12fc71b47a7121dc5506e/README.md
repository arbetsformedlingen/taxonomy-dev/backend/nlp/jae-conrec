# Results for 'dfc4605d2823bb06d2a12fc71b47a7121dc5506e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [dfc4605d2823bb06d2a12fc71b47a7121dc5506e](README.md) | 1 | 3380 | 35 | 24 | 170/362 = **47%** | 13/21 = **62%** |

## Source text

IT-Supporttekniker till Nexer Infrastructure i Stockholm Har du något års arbetslivserfarenhet från en helpdesk eller supportrelaterad roll och vill fördjupa dina kunskaper samt utvecklas inom IT? Nexer Infrastructure söker fler konsulter inom IT-support för roller inom både 1st line, 2nd line eller är du kanske en on-site tekniker som vill jobba på välkända bolag i Stockholm?  Vi erbjuder dig   • En chans att utvecklas inom IT • En engagerad konsultchef som stöttar dig i din karriär • En familjär stämning och gemensamma aktiviteter med dina Nexer-kollegor • Ett community med like-minded kollegor att dela kunskap och frågor med • Tillsvidareanställning med fast månadslön och goda förmåner  Om tjänsten  Som konsult inom IT-support arbetar du brett inom IT och användarstöd, och mer specifikt mot ramverk och verktyg så som Active Directory, Windows 10, Office 365, grundläggande nätverksteknologier och ärendehanteringssystem såsom ServiceNow med flera. De flesta uppdragen innebär att du hjälper någon av våra kunder med IT-support på klientsidan och supporterar internt anställda genom telefon, mail, chatt och ärendehanteringssystem. I vissa uppdrag kan även onsite-support förekomma samt hårdvarusupport för datorer, kringutrustning, skrivare och AV utrustning.  Vi söker dig som   • Har ett genuint IT-intresse (kanske byggde du din första PC innan du lärde dig gå?) • Har tidigare erfarenhet av arbete i servicedesk/first line support • Motiveras av att lösa problem och lämna användaren med ett leende på läpparna • Trivs i att bemöta människor och ge stöttning på ett pedagogiskt sätt • Har obehindrade kunskaper i svenska i både tal och skrift  Vi ser gärna att du är bekant med följande tekniker: Active Directory, Windows 7 & 10, Office 365/Azure, Windows Remote Desktop, SCCM, arbete i ärendehanteringssystem samt grundläggande kunskap om nätverksfelsökning. Det är också ett plus om du har supporterat Mac klienter tidigare.     Dina personliga egenskaper väger tungt och en våra konsulter kännetecknas av en prestigelös attityd där du ställer upp för dina kollegor. Att bidra till en bra stämning på arbetsplatsen är viktigt för dig. Du tar snabbt till dig ny information och är inte rädd för att ta egna initiativ när du ser att det behövs. Vidare tror vi att du är nyfiken och vill alltid lära dig mer och ser därför behovet av att göra saker innan det faktiskt brinner i knutarna.     Om Nexer Infrastructures  Nexer Infrastructures startade sin verksamhet 2018 och finns i Stockholm, Göteborg och Malmö. Vi är specialister på en sak – talanger inom drift, support och infrastruktur. Bland våra kunder finns allt från stora välkända företag och mindre, snabbväxande bolag eller kommuner.    Vi är en del av Nexer-koncernen, som är Sveriges största privatägda IT-konsultbolag med över 2000 medarbetare. Inom Nexer har vi visionen ”Promising Future” Vi är framtidens techbolag. Tillsammans tar vi sikte mot en morgondag som bär på ett löfte. Om företag som blomstrar, karriärer som tar fart. Om ett bättre liv för alla. Det löftet tänker vi uppfylla. Detta medför att vi jobbar aktivt med socialt ansvarstagande och har bland annat ett stort engagemang i Star for Life och andra viktiga samhällsinitiativ. Läs gärna mer om dessa i länken nedan:  https://nexergroup.com/sv/csr/   Ansökan  Urval sker löpande, vid frågor kontakta elin.lidstrom@nexergroup.com

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **IT-Supporttekniker** till Nexer Infrastructure i S... | x | x | 18 | 18 | [IT-supporttekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZBUk_h8D_KZA) |
| 2 | ...r till Nexer Infrastructure i **Stockholm** Har du något års arbetslivser... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 3 | ... arbetslivserfarenhet från en **helpdesk** eller supportrelaterad roll o... | x |  |  | 8 | [Helpdesk-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/hRJ7_Us8_x6a) |
| 4 | ...renhet från en helpdesk eller **supportrelaterad** roll och vill fördjupa dina k... | x |  |  | 16 | [Supportfunktion, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3ejN_ycL_sdm) |
| 5 | ...kunskaper samt utvecklas inom **IT**? Nexer Infrastructure söker f... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 6 | ...ure söker fler konsulter inom **IT-support** för roller inom både 1st line... | x | x | 10 | 10 | [IT-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1Nz1_z46_39V) |
| 6 | ...ure söker fler konsulter inom **IT-support** för roller inom både 1st line... |  | x |  | 10 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 7 | ...port för roller inom både 1st **line**, 2nd line eller är du kanske ... | x |  |  | 4 | [Line-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/k2U3_c4q_FjL) |
| 8 | ...oller inom både 1st line, 2nd **line** eller är du kanske en on-site... | x |  |  | 4 | [Line-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/k2U3_c4q_FjL) |
| 9 | ...ill jobba på välkända bolag i **Stockholm**?  Vi erbjuder dig   • En chan... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 10 | ...• En chans att utvecklas inom **IT** • En engagerad konsultchef so... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 11 | ...dela kunskap och frågor med • **Tillsvidareanställning** med fast månadslön och goda f... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 12 | ...Om tjänsten  Som konsult inom **IT-support** arbetar du brett inom IT och ... | x | x | 10 | 10 | [IT-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1Nz1_z46_39V) |
| 12 | ...Om tjänsten  Som konsult inom **IT-support** arbetar du brett inom IT och ... |  | x |  | 10 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 13 | ...support arbetar du brett inom **IT** och användarstöd, och mer spe... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 14 | ...arstöd, och mer specifikt mot **ramverk** och verktyg så som Active Dir... | x |  |  | 7 | [Ramverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/srsj_GQb_CE1) |
| 15 | ...ot ramverk och verktyg så som **Active Directory**, Windows 10, Office 365, grun... | x | x | 16 | 16 | [Active Directory, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/xGPG_PpL_zWN) |
| 16 | ...ktyg så som Active Directory, **Windows** 10, Office 365, grundläggande... | x | x | 7 | 7 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 17 | ...Active Directory, Windows 10, **Office 365**, grundläggande nätverksteknol... | x | x | 10 | 10 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 18 | ...10, Office 365, grundläggande **nätverk**steknologier och ärendehanteri... | x |  |  | 7 | [Nätverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/nHDU_dpj_R9H) |
| 19 | ...lper någon av våra kunder med **IT-support** på klientsidan och supportera... | x | x | 10 | 10 | [IT-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1Nz1_z46_39V) |
| 19 | ...lper någon av våra kunder med **IT-support** på klientsidan och supportera... |  | x |  | 10 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 20 | ...IT-support på klientsidan och **supporterar** internt anställda genom telef... | x |  |  | 11 | [Supportfunktion, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3ejN_ycL_sdm) |
| 21 | ...r dig som   • Har ett genuint **IT**-intresse (kanske byggde du di... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 22 | ... • Har tidigare erfarenhet av **arbete** i servicedesk/first line supp... | x |  |  | 6 | [Servicedeskmedarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F96z_ghp_Gja) |
| 23 | ...digare erfarenhet av arbete i **servicedesk**/first line support • Motivera... | x |  |  | 11 | [Servicedeskmedarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F96z_ghp_Gja) |
| 24 | ...enhet av arbete i servicedesk/**first line support** • Motiveras av att lösa probl... |  | x |  | 18 | [Line-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/k2U3_c4q_FjL) |
| 25 | ...• Har obehindrade kunskaper i **svenska** i både tal och skrift  Vi ser... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 26 | ...bekant med följande tekniker: **Active Directory**, Windows 7 & 10, Office 365/A... | x | x | 16 | 16 | [Active Directory, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/xGPG_PpL_zWN) |
| 27 | ...e tekniker: Active Directory, **Windows** 7 & 10, Office 365/Azure, Win... | x | x | 7 | 7 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 28 | ...ker: Active Directory, Windows** 7** & 10, Office 365/Azure, Windo... |  | x |  | 2 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 29 | ...ve Directory, Windows 7 & 10, **Office 365**/Azure, Windows Remote Desktop... | x | x | 10 | 10 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 30 | ...y, Windows 7 & 10, Office 365/**Azure**, Windows Remote Desktop, SCCM... | x | x | 5 | 5 | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| 31 | ...ows 7 & 10, Office 365/Azure, **Windows** Remote Desktop, SCCM, arbete ... | x |  |  | 7 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 32 | ...zure, Windows Remote Desktop, **SCCM**, arbete i ärendehanteringssys... | x | x | 4 | 4 | [SCCM, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/67V1_6cE_VAt) |
| 33 | ...t är också ett plus om du har **supporterat Mac** klienter tidigare.     Dina p... | x |  |  | 15 | [Macintosh-datorer, installation och underhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/q2v1_YcS_Dff) |
| 34 | ...n verksamhet 2018 och finns i **Stockholm**, Göteborg och Malmö. Vi är sp... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 35 | ...t 2018 och finns i Stockholm, **Göteborg** och Malmö. Vi är specialister... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 36 | ...nns i Stockholm, Göteborg och **Malmö**. Vi är specialister på en sak... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 37 | ...r Sveriges största privatägda **IT**-konsultbolag med över 2000 me... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 38 | ... vi jobbar aktivt med socialt **ansvarstagande** och har bland annat ett stort... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| | **Overall** | | | **170** | **362** | 170/362 = **47%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [IT-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1Nz1_z46_39V) |
| x |  |  | [Supportfunktion, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3ejN_ycL_sdm) |
| x | x | x | [SCCM, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/67V1_6cE_VAt) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Servicedeskmedarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F96z_ghp_Gja) |
| x | x | x | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| x | x | x | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [IT-supporttekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZBUk_h8D_KZA) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Helpdesk-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/hRJ7_Us8_x6a) |
| x | x | x | [Line-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/k2U3_c4q_FjL) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [Nätverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/nHDU_dpj_R9H) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x |  |  | [Macintosh-datorer, installation och underhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/q2v1_YcS_Dff) |
| x |  |  | [Ramverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/srsj_GQb_CE1) |
| x | x | x | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| x | x | x | [Active Directory, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/xGPG_PpL_zWN) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **13** | 13/21 = **62%** |