# Results for '57c0a750530ab09b439ab41cb538a54f3fad81b4'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [57c0a750530ab09b439ab41cb538a54f3fad81b4](README.md) | 1 | 378 | 8 | 7 | 56/81 = **69%** | 5/8 = **62%** |

## Source text

Snickare sökes Swedish unik villa växer och behöver anställa en snickare / husmontör. vi söker dej som har bra erfarenhet från nybyggnation, husmontering och takläggning. jobbet är utomlands: Tyskland, Österikke och Schweiz.  Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Snickare** sökes Swedish unik villa växe... | x | x | 8 | 8 | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| 2 | ...växer och behöver anställa en **snickare** / husmontör. vi söker dej som... | x | x | 8 | 8 | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| 3 | ...ehöver anställa en snickare / **husmontör**. vi söker dej som har bra erf... |  | x |  | 9 | [Husmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/R6F8_Zzw_Ta6) |
| 3 | ...ehöver anställa en snickare / **husmontör**. vi söker dej som har bra erf... | x | x | 9 | 9 | [husmontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qgft_xCU_YQT) |
| 4 | ...erfarenhet från nybyggnation, **husmontering** och takläggning. jobbet är ut... | x | x | 12 | 12 | [Husmontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/VdRu_b8H_vb8) |
| 5 | ...ybyggnation, husmontering och **takläggning**. jobbet är utomlands: Tysklan... | x | x | 11 | 11 | [Takläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/3Zeh_4RX_kA4) |
| 6 | ...äggning. jobbet är utomlands: **Tyskland**, Österikke och Schweiz.  Öppe... | x | x | 8 | 8 | [Tyskland, **country**](http://data.jobtechdev.se/taxonomy/concept/G3M7_959_8Pp) |
| 7 | ...obbet är utomlands: Tyskland, **Österikke** och Schweiz.  Öppen för alla ... | x |  |  | 9 | [Österrike, **country**](http://data.jobtechdev.se/taxonomy/concept/uZex_hVg_jPf) |
| 8 | ...ands: Tyskland, Österikke och **Schweiz**.  Öppen för alla Vi fokuserar... | x |  |  | 7 | [Schweiz, **country**](http://data.jobtechdev.se/taxonomy/concept/Q78b_oCw_Yq2) |
| | **Overall** | | | **56** | **81** | 56/81 = **69%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Takläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/3Zeh_4RX_kA4) |
| x | x | x | [Tyskland, **country**](http://data.jobtechdev.se/taxonomy/concept/G3M7_959_8Pp) |
| x |  |  | [Schweiz, **country**](http://data.jobtechdev.se/taxonomy/concept/Q78b_oCw_Yq2) |
|  | x |  | [Husmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/R6F8_Zzw_Ta6) |
| x | x | x | [Husmontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/VdRu_b8H_vb8) |
| x | x | x | [husmontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qgft_xCU_YQT) |
| x | x | x | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| x |  |  | [Österrike, **country**](http://data.jobtechdev.se/taxonomy/concept/uZex_hVg_jPf) |
| | | **5** | 5/8 = **62%** |