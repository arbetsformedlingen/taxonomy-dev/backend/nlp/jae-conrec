# Results for 'f429f242ec18ffab856f54cbeaf35e3d105a8e17'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f429f242ec18ffab856f54cbeaf35e3d105a8e17](README.md) | 1 | 1567 | 11 | 8 | 27/162 = **17%** | 3/14 = **21%** |

## Source text

Rörmontör Vivab, Vatten & Miljö i Väst AB, jobbar för en hållbar framtid med friskare hav, ett rent och gott dricksvatten och en hållbar konsumtion med minskat avfall. Vivab är en viktig aktör inom grön samhällsutveckling på västkustens mittpunkt. Vi har ett hjärta för en hållbar framtid och nu söker vi dig som vill vara med och göra det möjligt!   Vill du vara en viktig del i att förse mer än 100 000 personer med rent och friskt vatten? Då ska du söka tjänsten som rörmontör hos oss!  ARBETSUPPGIFTER Som rörmontör arbetar du och dina kollegor med installationer vid ny- och ombyggnation och med reparationer och service i ledningsnätet. Arbetet sker utomhus med omväxlande arbetsuppgifter. Beredskapstjänst kan ingå.  Tjänsten är placerad i Varberg.  KVALIFIKATIONER Du trivs med ett omväxlande arbete, är lösningsfokuserad och gillar att jobba i team.  Att arbeta utomhus passar dig bra, du är hjälpsam och gillar ordning och reda.  Du är handlingskraftig, tar initiativ och ser till att slutföra det du påbörjar.  Du har: • en relevant utbildning inom rörteknik eller anläggning • datorvana • B-körkort  Erfarenhet från diverse rör-, VVS- och anläggningsarbeten värdesätts.  ÖVRIGT En bra arbetsmiljö ligger högt upp på vår agenda och vi vill att våra medarbetare har en bra balans mellan arbete och fritid.  Vivab vill främja en hälsosam livsstil och erbjuder bland annat en friskvårdstimme i veckan, friskvårdsbidrag och möjlighet till att hyra förmånscykel.   OBS! Vi undanber oss alla erbjudanden om ytterligare annonsering både i tryck och digitalt.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...iljö i Väst AB, jobbar för en **hållbar** framtid med friskare hav, ett... | x |  |  | 7 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 2 | ...iskare hav, ett rent och gott **dricksvatten** och en hållbar konsumtion med... |  | x |  | 12 | [Vattenförsörjning, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/VF43_B8N_hZb) |
| 3 | ... och gott dricksvatten och en **hållbar** konsumtion med minskat avfall... | x |  |  | 7 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 4 | ...t dricksvatten och en hållbar **konsumtion** med minskat avfall. Vivab är ... | x |  |  | 10 | [Konsumtion, **keyword**](http://data.jobtechdev.se/taxonomy/concept/9m5g_y5G_Ajx) |
| 5 | ...ållbar konsumtion med minskat **avfall**. Vivab är en viktig aktör ino... | x |  |  | 6 | [Avfall, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Kx4Q_7TA_MuR) |
| 6 | ...nkt. Vi har ett hjärta för en **hållbar** framtid och nu söker vi dig s... | x |  |  | 7 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 7 | ...ingå.  Tjänsten är placerad i **Varberg**.  KVALIFIKATIONER Du trivs ... | x | x | 7 | 7 | [Varberg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AkUx_yAq_kGr) |
| 8 | ...gillar att jobba i team.  Att **arbeta utomhus** passar dig bra, du är hjälpsa... | x |  |  | 14 | [arbeta utomhus, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Jvu2_Ain_MsJ) |
| 9 | ...ller anläggning • datorvana • **B-körkort**  Erfarenhet från diverse rör-... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 10 | ...Erfarenhet från diverse rör-, **VVS**- och anläggningsarbeten värde... | x |  |  | 3 | [Vvsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Thzd_jiq_7SF) |
| 11 | ...Erfarenhet från diverse rör-, **VVS- och an**läggningsarbeten värdesätts. ... |  | x |  | 11 | [VVS-arbeten, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/6s34_ExZ_SbG) |
| 11 | ...Erfarenhet från diverse rör-, **VVS- och an**läggningsarbeten värdesätts. ... |  | x |  | 11 | [Övriga VVS-arbeten, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/kdDU_YsL_xEu) |
| 11 | ...Erfarenhet från diverse rör-, **VVS- och an**läggningsarbeten värdesätts. ... |  | x |  | 11 | [Elinstallationer, VVS-arbeten och andra bygginstallationer, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/pFjT_UM8_kfs) |
| 12 | ...t från diverse rör-, VVS- och **anläggningsarbeten** värdesätts.  ÖVRIGT En bra a... |  | x |  | 18 | [Anläggningsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/myMG_ygC_kDW) |
| 12 | ...t från diverse rör-, VVS- och **anläggningsarbeten** värdesätts.  ÖVRIGT En bra a... | x |  |  | 18 | [Anläggningsarbeten, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/pSzC_2YX_dkE) |
| 13 | ...n värdesätts.  ÖVRIGT En bra **arbetsmiljö** ligger högt upp på vår agenda... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | **Overall** | | | **27** | **162** | 27/162 = **17%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [VVS-arbeten, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/6s34_ExZ_SbG) |
| x |  |  | [Konsumtion, **keyword**](http://data.jobtechdev.se/taxonomy/concept/9m5g_y5G_Ajx) |
| x | x | x | [Varberg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AkUx_yAq_kGr) |
| x |  |  | [arbeta utomhus, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Jvu2_Ain_MsJ) |
| x |  |  | [Avfall, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Kx4Q_7TA_MuR) |
| x |  |  | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| x |  |  | [Vvsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Thzd_jiq_7SF) |
|  | x |  | [Vattenförsörjning, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/VF43_B8N_hZb) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [Övriga VVS-arbeten, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/kdDU_YsL_xEu) |
|  | x |  | [Anläggningsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/myMG_ygC_kDW) |
|  | x |  | [Elinstallationer, VVS-arbeten och andra bygginstallationer, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/pFjT_UM8_kfs) |
| x |  |  | [Anläggningsarbeten, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/pSzC_2YX_dkE) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **3** | 3/14 = **21%** |