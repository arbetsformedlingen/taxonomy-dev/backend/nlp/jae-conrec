# Results for '51477697c59e0af70090001ae6a891995b61b46b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [51477697c59e0af70090001ae6a891995b61b46b](README.md) | 1 | 2123 | 15 | 6 | 48/181 = **27%** | 3/13 = **23%** |

## Source text

CNC-operatör Vi söker nu fler duktiga Maskinoperatörer!  Arbetsuppgifter  • Ställa och köra cnc-svarvar eller fräsar.  • Producera enligt fastställda produktionsplaner som hämtas från körplanering i Monitor.  • Utföra underhåll på maskiner efter givna instruktioner.  • Identifiera förbättringar. 5S.  • Mätkunskaper. Skall kunna hantera de vanligaste handmätdonen.   Personliga egenskaper  • Noggrann  • Nyfiken  • Lösningsorienterad   Avdelningen består av 6 maskingrupper. 2-skift och ständig natt är de vanligaste skifttyperna.  Maskinerna är handmatade svarvar och fräsar med Okuma eller Fanuc styrsystem.  Produktfloran är ganska bred med många olika detaljer. Ställ/rigg till nya ordrar görs flera gånger i veckan. ERP systemet är Monitor.  Villkor  Industriqompetens är ett industriföretag som arbetar med bemanning. Vi arbetar med både stora och små aktörer runt om Mälardalen. Som anställd hos oss på IQ har du möjlighet att påverka din egen utveckling. Vi erbjuder en trygg anställning då vi går efter Teknikavtalet som är de kollektivavtal vi och de flesta av våra kunder är anslutna till. På IQ tar vi vara på den kompetens du har och matchar med kundens behov.  Vi börjar normalt med en visstidsanställning för att både du och vi ska få chans att lära känna varandra ordentligt innan vi eventuellt går över till en tillsvidareanställning hos IQ eller något av våra kundföretag. Beroende på din bakgrund och referenser kan det naturligtvis vara aktuellt med en tillsvidareanställning redan från början. Du får en fast månadslön och övriga ersättningar enligt teknikavtalet som är det kollektivavtal vi och de flesta av våra kunder än anslutna till.  Vi ser långsiktigt på dig som anställd och vårt mål är alltid att du som har rätt förutsättningar ska kunna erbjudas en tillsvidareanställning. Idag är ungefär hälften av våra medarbetare tillsvidareanställda hos oss. Stämmer ovan in på dig? Då är just du den vi behöver!  Sök tjänsten direkt på www.iqjobb.se, där kan du även läsa mer om oss på IQ och hur det är att arbeta hos oss. Urval sker löpande varför det är viktigt att du inte väntar med din ansökan.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **CNC-operatör** Vi söker nu fler duktiga Mask... | x | x | 12 | 12 | [CNC-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ezxx_3Jf_Li6) |
| 2 | ...atör Vi söker nu fler duktiga **Maskinoperatörer**!  Arbetsuppgifter  • Ställa o... | x |  |  | 16 | [Process- och maskinoperatörer, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/XnLr_FQp_c82) |
| 2 | ...atör Vi söker nu fler duktiga **Maskinoperatörer**!  Arbetsuppgifter  • Ställa o... |  | x |  | 16 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 3 | ...suppgifter  • Ställa och köra **cnc-svarvar** eller fräsar.  • Producera en... | x |  |  | 11 | [Svarvning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/d2yX_cZM_fSy) |
| 4 | ...la och köra cnc-svarvar eller **fräsar**.  • Producera enligt faststäl... | x |  |  | 6 | [CNC-fräsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gyLb_7Kk_FG1) |
| 5 | ... Producera enligt fastställda **produktionsplaner** som hämtas från körplanering ... |  | x |  | 17 | [Produktionsplanering, tillverkning, **skill**](http://data.jobtechdev.se/taxonomy/concept/kvLG_4uo_M5p) |
| 6 | ...om hämtas från körplanering i **Monitor**.  • Utföra underhåll på maski... | x |  |  | 7 | [Monitor, affärssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/1FbS_vYp_DC3) |
| 7 | ...lanering i Monitor.  • Utföra **underhåll** på maskiner efter givna instr... | x |  |  | 9 | [Maskinunderhåll, processindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/tLci_gYE_gHR) |
| 8 | ...nitor.  • Utföra underhåll på **maskiner** efter givna instruktioner.  •... | x |  |  | 8 | [Maskinunderhåll, processindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/tLci_gYE_gHR) |
| 9 | ...n.   Personliga egenskaper  • **Noggrann**  • Nyfiken  • Lösningsoriente... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 10 | ...na.  Maskinerna är handmatade **svarvar** och fräsar med Okuma eller Fa... | x |  |  | 7 | [Svarvning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/d2yX_cZM_fSy) |
| 11 | ...matade svarvar och fräsar med **Okuma** eller Fanuc styrsystem.  Prod... | x |  |  | 5 | [CNC-programmering, Okuma, **skill**](http://data.jobtechdev.se/taxonomy/concept/yYvR_uTu_o6c) |
| 12 | ...ar och fräsar med Okuma eller **Fanuc** styrsystem.  Produktfloran är... | x |  |  | 5 | [CNC-programmering, Fanuc, **skill**](http://data.jobtechdev.se/taxonomy/concept/fSBi_ZAL_sKa) |
| 13 | ...ger i veckan. ERP systemet är **Monitor**.  Villkor  Industriqompetens ... | x |  |  | 7 | [Monitor, affärssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/1FbS_vYp_DC3) |
| 14 | ...efter Teknikavtalet som är de **kollektivavtal** vi och de flesta av våra kund... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 15 | ...ov.  Vi börjar normalt med en **visstidsanställning** för att både du och vi ska få... | x |  |  | 19 | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| 16 | ...ligt teknikavtalet som är det **kollektivavtal** vi och de flesta av våra kund... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **48** | **181** | 48/181 = **27%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Monitor, affärssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/1FbS_vYp_DC3) |
| x |  |  | [Process- och maskinoperatörer, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/XnLr_FQp_c82) |
| x |  |  | [Svarvning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/d2yX_cZM_fSy) |
| x | x | x | [CNC-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ezxx_3Jf_Li6) |
| x |  |  | [CNC-programmering, Fanuc, **skill**](http://data.jobtechdev.se/taxonomy/concept/fSBi_ZAL_sKa) |
| x |  |  | [CNC-fräsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gyLb_7Kk_FG1) |
| x |  |  | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
|  | x |  | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
|  | x |  | [Produktionsplanering, tillverkning, **skill**](http://data.jobtechdev.se/taxonomy/concept/kvLG_4uo_M5p) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [Maskinunderhåll, processindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/tLci_gYE_gHR) |
| x |  |  | [CNC-programmering, Okuma, **skill**](http://data.jobtechdev.se/taxonomy/concept/yYvR_uTu_o6c) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **3** | 3/13 = **23%** |