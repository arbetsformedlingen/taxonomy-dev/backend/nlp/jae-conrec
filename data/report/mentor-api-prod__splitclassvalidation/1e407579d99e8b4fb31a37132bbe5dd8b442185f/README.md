# Results for '1e407579d99e8b4fb31a37132bbe5dd8b442185f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1e407579d99e8b4fb31a37132bbe5dd8b442185f](README.md) | 1 | 1594 | 14 | 14 | 61/277 = **22%** | 6/19 = **32%** |

## Source text

Lokalvårdare/Receptionist Vilka är vi? Lyckåhem Hotell & Studiolägenheter är ett bekvämt och flexibelt hotell med uthyrning av både hotell- och vandrarhemsrum samt mindre studiolägenheter. Hotellet har under de senaste åren genomgått en omfattande renovering, vilket gör att vi kan erbjuda fräscha och moderna rum. Våra gäster utgörs av alltifrån semesterfirare som bor här ett par dagar, till studenter och företagskunder som stannar längre perioder. Här bor våra gäster omgivna av hagar med betande hästar och många uppskattar under sommaren att koppla av i vår stora trädgård.  Vem är du? Inför sommarens högsäsong och ordinarie personals semestrar söker vi omgående dig som vill ha ett trevligt och fartfyllt extraarbete inom både städ och reception. Vi söker dig som är glad, positiv och har lätt för att samarbeta. Då arbetet innebär mycket kontakt med våra gäster både på plats och i telefon lägger vi stor vikt vid att du har god social kompetens och att du behärskar både svenska och engelska väl. Har du tidigare vana av kassaarbete samt erfarenhet av arbete med bokningssystem är det ett plus men inget krav. Du ska trivas med att arbeta aktivt och stundvis i ett högt tempo då sommaren är vår mest fartfyllda period.  Dina arbetsuppgifter:  ·        Städning av hotellrummen och de gemensamma utrymmena ·        Bemanning av vår reception  ·        Tvätt och mangling ·        Beredning av frukostbuffé Tjänsten är inledningsvis en behovsanställning under sommarmånaderna, med möjlighet till förlängning för rätt person. Tillträde omgående.  Maila din ansökan till: info@lyckahem.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Lokalvårdare**/Receptionist Vilka är vi? Lyc... | x | x | 12 | 12 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 2 | Lokalvårdare/**Receptionist** Vilka är vi? Lyckåhem Hotell ... | x |  |  | 12 | [receptionist, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/CuKc_tvM_gzc) |
| 2 | Lokalvårdare/**Receptionist** Vilka är vi? Lyckåhem Hotell ... |  | x |  | 12 | [Receptionist, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gAAz_Lo5_WRR) |
| 3 | ...tionist Vilka är vi? Lyckåhem **Hotell** & Studiolägenheter är ett bek... |  | x |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 4 | ...ren att koppla av i vår stora **trädgård**.  Vem är du? Inför sommarens ... |  | x |  | 8 | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| 5 | ...xtraarbete inom både städ och **reception**. Vi söker dig som är glad, po... |  | x |  | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 6 | ... som är glad, positiv och har **lätt för att samarbeta**. Då arbetet innebär mycket ko... | x |  |  | 22 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 7 | ...ens och att du behärskar både **svenska** och engelska väl. Har du tidi... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 8 | ...du behärskar både svenska och **engelska** väl. Har du tidigare vana av ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 9 | ...engelska väl. Har du tidigare **vana av kassaarbete** samt erfarenhet av arbete med... | x |  |  | 19 | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| 10 | ... väl. Har du tidigare vana av **kassaarbete** samt erfarenhet av arbete med... |  | x |  | 11 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 11 | ...samt erfarenhet av arbete med **bokningssystem** är det ett plus men inget kra... | x | x | 14 | 14 | [Bokningssystem, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/kRoQ_fPR_8oK) |
| 12 | ...na arbetsuppgifter:  ·        **Städning** av hotellrummen och de gemens... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 13 | ...na arbetsuppgifter:  ·        **Städning av hotellrummen och de gemensamma utrymmena** ·        Bemanning av vår rec... | x |  |  | 52 | [Städvana, hotell, **skill**](http://data.jobtechdev.se/taxonomy/concept/6XRu_MQm_4NK) |
| 14 | ...gemensamma utrymmena ·        **Bemanning av vår reception**  ·        Tvätt och mangling ... | x |  |  | 26 | [sköta reception, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/K6XL_6aL_AQ6) |
| 15 | ...ena ·        Bemanning av vår **reception**  ·        Tvätt och mangling ... |  | x |  | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 16 | ...ng av vår reception  ·        **Tvätt** och mangling ·        Beredni... | x |  |  | 5 | [använda tvättmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qf5n_zc6_Bda) |
| 17 | ...reception  ·        Tvätt och **mangling** ·        Beredning av frukost... | x | x | 8 | 8 | [Mangling, **skill**](http://data.jobtechdev.se/taxonomy/concept/kA7s_yHL_nJ2) |
| 18 | ...angling ·        Beredning av **frukostbuffé** Tjänsten är inledningsvis en ... | x | x | 12 | 12 | [Frukostbuffé, **skill**](http://data.jobtechdev.se/taxonomy/concept/sqd6_4Kk_b4w) |
| 19 | ... Tjänsten är inledningsvis en **behovsanställning** under sommarmånaderna, med mö... | x |  |  | 17 | [Behovsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/1paU_aCR_nGn) |
| | **Overall** | | | **61** | **277** | 61/277 = **22%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Behovsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/1paU_aCR_nGn) |
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x |  |  | [Städvana, hotell, **skill**](http://data.jobtechdev.se/taxonomy/concept/6XRu_MQm_4NK) |
| x |  |  | [receptionist, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/CuKc_tvM_gzc) |
|  | x |  | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x |  |  | [sköta reception, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/K6XL_6aL_AQ6) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
|  | x |  | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
|  | x |  | [Receptionist, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gAAz_Lo5_WRR) |
| x |  |  | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| x | x | x | [Mangling, **skill**](http://data.jobtechdev.se/taxonomy/concept/kA7s_yHL_nJ2) |
| x | x | x | [Bokningssystem, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/kRoQ_fPR_8oK) |
| x |  |  | [använda tvättmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qf5n_zc6_Bda) |
| x | x | x | [Frukostbuffé, **skill**](http://data.jobtechdev.se/taxonomy/concept/sqd6_4Kk_b4w) |
|  | x |  | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/19 = **32%** |