# Results for 'b76503dd7531a1357cfd1f89510033201661306f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b76503dd7531a1357cfd1f89510033201661306f](README.md) | 1 | 2860 | 17 | 7 | 76/184 = **41%** | 7/16 = **44%** |

## Source text

Systemutvecklare Future Ordering Är du driven och nyfiken på framtidens IT lösningar? Då kan du vara den vi söker!  Future Ordering är en arbetsgivare för dig som är nyfiken och tycker om att arbeta med ny-teknik, vill lära dig mer inom modern systemutveckling och tycker om problemlösning. Tycker du precis som oss om att arbeta flexibelt, disponera din arbetstid själv, dela med dig av dina kunskaper och lära dig av andra för att tillsammans nå mål som ett team? Bli en del i det Team som vill förändra hela gästupplevelsen när du beställer mat i alla former i hela världen. Jobba med att utveckla vår moln-plattform som är aktiv i över tio länder, två kontinenter och som hjälper varumärken som Circle-K och Max Hamburgers.  Varför jobba på Future Ordering? - En rättvis och konkurrenskraftig lön - Trygg anställning med pensionssparande och pensionsplaner, privat sjukförsäkring och självförsörjningsförmåner - Flexibel arbetsplats med möjlighet att arbeta hemifrån och på plats i valfri kombination, beroende på din situation och preferenser - Träning på arbetstid och konkurrenskraftigt friskvårdsbidrag. - 30 dagars betald ledighet. - En samarbetskultur som uppmuntrar till nyfikenhet och ansvarstagande  Tjänsten hos oss innebär systemutvecklingsarbete tillsammans i ett team som följer modern systemutvecklings metodik.  Din profil Vi söker dig som har ett genuint intresse av modern teknik och drivs av att ligga i framkanten av modern utveckling. Vi ser att du som söker har god arbetsmoral och en vilja att ständigt utvecklas tillsammans med ditt team.  Krav: * Du är problemlösande * Tycker om att lära dig nya saker * Avskräcks inte av förändring  Det är meriterande om du: * Har mer än två års arbetserfarenhet inom området * Har erfarenhet av Microsoft Azure Clouds infrastruktur * Har erfarenhet av NET Core och/ellerNode.js * Erfarenhet av event- och köbaserad utveckling * Erfarenhet av Docker * Erfarenhet av React * Erfarenhet av att bygga distribuerade system  Stor vikt kommer att läggas vid personliga egenskaper.  Denna rekryteringsprocess hanteras av ansvarig rekryterare/konsultchef Linda Vallerström. Du når Linda via mail på linda.vallerstrom@studentconsulting.com. Vi tillsätter tjänsterna så fort vi hittat rätt person för jobbet. Välkommen med din ansökan redan idag!  Om företaget Mångfaldigt prisbelönta StudentConsulting är ett av Skandinaviens största och ledande rekryterings- och bemanningsföretag med fokus på studenter, akademiker och yrkesutbildade. Tack vare ett stort nätverk och lång erfarenhet har vi rekryterat över 11 000 personer det senaste året. Vi erbjuder intressanta och utmanande tjänster på både hel- och deltid inom områden som IT, teknik, ekonomi, administration, HR, marknadsföring, kundtjänst, försäljning, industri, produktion, logistik och transport. Hitta din framtid på www.studentconsulting.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Systemutvecklare** Future Ordering Är du driven ... | x |  |  | 16 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 2 | ...ven och nyfiken på framtidens **IT** lösningar? Då kan du vara den... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 3 | ...vill lära dig mer inom modern **systemutveckling** och tycker om problemlösning.... | x | x | 16 | 16 | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| 4 | ...n situation och preferenser - **Träning** på arbetstid och konkurrenskr... | x |  |  | 7 | [Träning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/n19Z_hzx_Apu) |
| 5 | ...etskultur som uppmuntrar till **nyfikenhet** och ansvarstagande  Tjänsten ... | x |  |  | 10 | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
| 6 | ...ppmuntrar till nyfikenhet och **ansvarstagande**  Tjänsten hos oss innebär sys... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 7 | ...riterande om du: * Har mer än **två års arbetserfarenhet** inom området * Har erfarenhet... | x |  |  | 24 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 8 | ...m området * Har erfarenhet av **Microsoft Azure** Clouds infrastruktur * Har er... | x | x | 15 | 15 | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| 9 | ... erfarenhet av Microsoft Azure** Clouds** infrastruktur * Har erfarenhe... | x |  |  | 7 | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| 10 | ...astruktur * Har erfarenhet av **NET Core** och/ellerNode.js * Erfarenhet... | x |  |  | 8 | [Core-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/uUz9_x1Z_w2e) |
| 11 | ...farenhet av NET Core och/eller**Node.js** * Erfarenhet av event- och kö... | x |  |  | 7 | [Node, exekveringsmiljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/McKJ_SG4_hCM) |
| 12 | ...ad utveckling * Erfarenhet av **Docker** * Erfarenhet av React * Erfar... | x | x | 6 | 6 | [Docker, container-plattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZipR_ckJ_YQC) |
| 13 | ...sprocess hanteras av ansvarig **rekryterare**/konsultchef Linda Vallerström... | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| 14 | ...- och deltid inom områden som **IT**, teknik, ekonomi, administrat... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 15 | ... inom områden som IT, teknik, **ekonomi**, administration, HR, marknads... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 16 | ... ekonomi, administration, HR, **marknadsföring**, kundtjänst, försäljning, ind... | x |  |  | 14 | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| 17 | ...stration, HR, marknadsföring, **kundtjänst**, försäljning, industri, produ... | x | x | 10 | 10 | [Kundtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/aMyW_hB7_TpU) |
| 18 | ...ljning, industri, produktion, **logistik** och transport. Hitta din fram... | x | x | 8 | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| | **Overall** | | | **76** | **184** | 76/184 = **41%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| x |  |  | [Node, exekveringsmiljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/McKJ_SG4_hCM) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| x | x | x | [Docker, container-plattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZipR_ckJ_YQC) |
| x | x | x | [Kundtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/aMyW_hB7_TpU) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| x |  |  | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| x |  |  | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
| x |  |  | [Träning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/n19Z_hzx_Apu) |
| x |  |  | [Core-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/uUz9_x1Z_w2e) |
| x | x | x | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| x |  |  | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| | | **7** | 7/16 = **44%** |