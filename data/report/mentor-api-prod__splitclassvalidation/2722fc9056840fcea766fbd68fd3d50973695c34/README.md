# Results for '2722fc9056840fcea766fbd68fd3d50973695c34'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2722fc9056840fcea766fbd68fd3d50973695c34](README.md) | 1 | 2174 | 19 | 17 | 106/284 = **37%** | 7/19 = **37%** |

## Source text

Orderplockare - Järna, Södertälje - Heltid Känner du att du är redo för nästa steg i karriären? Vi söker en kille eller tjej som vill jobba som lagermedarbetare hos vår kund i Södertälje Vi söker efter dig som är på jakt efter ny arbetslivserfarenhet. Du kanske tidigare aldrig har jobbat på lager eller i dagsläget jobbar på ett lager du inte känner att du når din fulla potential på. Som lagermedarbetare arbetar du självständigt med att plocka och packa på lagret. Arbetet innebär att packa varor utifrån en plocklista samt att arbeta nära med ut- och inleverans. Varje lagermedarbetare arbetar utifrån sin egna plocklista, där plockningen sker utifrån en given tidsram. Det är ett varierande och spännande jobb där man har chansen att utvecklas utifrån sin vilja och kompetens.    Arbetsuppgifter: Ansvara för inleveranser av gods och material Scanna in produkter i affärssystem Samordna produkter utifrån en plocklista Lastning av kurirbilar  Om dig:  Arbetsuppgifterna i den här rollen passar dig som är stresstålig, punktlig och ansvarsfull. Du uppskattar ett högt tempo och har goda kunskaper inom logistik och lagerhantering, vilka du erhållit antingen från studier eller arbetslivet. Om du har erfarenhet av liknande arbete och har erfarenhet av orderhantering är det meriterande Meriterande:  Körkort   Lagererfarenhet   Truckkort A+B  Lagret är beläget i Järna, Södertälje. Arbetet är förlagt mån-fre 07-16. Om oss: Simplex Bemanning grundades år 2014 och är verksam i branscher som lager, transport och service. Vi är ett ständigt utvecklande lag som strävar efter att ta nästa steg. Med ett stort fokus på att utbilda framtidens anställda jobbar vi väldigt mycket med ungdomar. Dessa får lära sig att ta del av det ansvar som tillkommer när du jobbar hos Simplex. Unga människor med drivkraften att lära sig saker och anta nya utmaningar ger oss ett försprång gentemot andra inom vår bransch. Vi vet att nyfikenhet och motivation är nycklarna för att utföra varje projekt med kvalitet. Vår personal ser varje dag som ett nytt tillfälle att samla på sig nya erfarenheter samtidigt som de applicerar sin breda kunskap för att utföra sina uppgifter på bästa sätt.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Orderplockare** - Järna, Södertälje - Heltid ... | x | x | 13 | 13 | [orderplockare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/yM9T_B1t_2pY) |
| 2 | Orderplockare - Järna, **Södertälje** - Heltid Känner du att du är ... | x | x | 10 | 10 | [Södertälje, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g6hK_M1o_hiU) |
| 3 | ...lockare - Järna, Södertälje - **Heltid** Känner du att du är redo för ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 4 | ...eller tjej som vill jobba som **lagermedarbetare** hos vår kund i Södertälje Vi ... | x | x | 16 | 16 | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
| 5 | ...germedarbetare hos vår kund i **Södertälje** Vi söker efter dig som är på ... | x | x | 10 | 10 | [Södertälje, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g6hK_M1o_hiU) |
| 6 | ...tidigare aldrig har jobbat på **lager** eller i dagsläget jobbar på e... |  | x |  | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 7 | ...r din fulla potential på. Som **lagermedarbetare** arbetar du självständigt med ... | x |  |  | 16 | [Lagermedarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/sSu6_qMg_nYu) |
| 7 | ...r din fulla potential på. Som **lagermedarbetare** arbetar du självständigt med ... |  | x |  | 16 | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
| 8 | ...tial på. Som lagermedarbetare **arbetar du självständigt** med att plocka och packa på l... | x |  |  | 24 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 9 | ...etar du självständigt med att **plocka** och packa på lagret. Arbetet ... | x |  |  | 6 | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
| 10 | ...lvständigt med att plocka och **packa** på lagret. Arbetet innebär at... | x |  |  | 5 | [packa varor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5YMq_iHL_wB1) |
| 11 | ...med ut- och inleverans. Varje **lagermedarbetare** arbetar utifrån sin egna ploc... | x | x | 16 | 16 | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
| 12 | ... här rollen passar dig som är **stresstålig**, punktlig och ansvarsfull. Du... | x |  |  | 11 | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
| 12 | ... här rollen passar dig som är **stresstålig**, punktlig och ansvarsfull. Du... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 13 | ... är stresstålig, punktlig och **ansvarsfull**. Du uppskattar ett högt tempo... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 14 | ...o och har goda kunskaper inom **logistik** och lagerhantering, vilka du ... | x | x | 8 | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 15 | ...a kunskaper inom logistik och **lagerhantering**, vilka du erhållit antingen f... |  | x |  | 14 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 15 | ...a kunskaper inom logistik och **lagerhantering**, vilka du erhållit antingen f... | x |  |  | 14 | [Terminal- och lagerarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/ijHY_aHk_ADE) |
| 16 | ... arbete och har erfarenhet av **orderhantering** är det meriterande Meriterand... | x |  |  | 14 | [hantera orderstock, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aB8m_LiV_GS5) |
| 17 | ...det meriterande Meriterande:  **Körkort**   Lagererfarenhet   Truckkort... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 18 | ...rande Meriterande:  Körkort   **Lagererfarenhet**   Truckkort A+B  Lagret är be... | x | x | 15 | 15 | [Terminal- och lagerarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/ijHY_aHk_ADE) |
| 19 | ...  Körkort   Lagererfarenhet   **Truckkort** A+B  Lagret är beläget i Järn... |  | x |  | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 20 | ...  Körkort   Lagererfarenhet   **Truckkort A**+B  Lagret är beläget i Järna,... | x |  |  | 11 | [A-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Zk4h_jP4_e85) |
| 21 | ...Lagererfarenhet   Truckkort A+**B**  Lagret är beläget i Järna, S... | x |  |  | 1 | [B-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/HeXq_amc_ABd) |
| 22 | ...B  Lagret är beläget i Järna, **Södertälje**. Arbetet är förlagt mån-fre 0... |  | x |  | 10 | [Södertälje, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g6hK_M1o_hiU) |
| 23 | ...ch är verksam i branscher som **lager**, transport och service. Vi är... |  | x |  | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| | **Overall** | | | **106** | **284** | 106/284 = **37%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [packa varor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5YMq_iHL_wB1) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [B-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/HeXq_amc_ABd) |
|  | x |  | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x |  |  | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
| x |  |  | [A-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Zk4h_jP4_e85) |
| x |  |  | [hantera orderstock, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aB8m_LiV_GS5) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Södertälje, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g6hK_M1o_hiU) |
| x | x | x | [Terminal- och lagerarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/ijHY_aHk_ADE) |
| x |  |  | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
| x |  |  | [Lagermedarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/sSu6_qMg_nYu) |
| x | x | x | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
| x | x | x | [orderplockare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/yM9T_B1t_2pY) |
| x | x | x | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **7** | 7/19 = **37%** |