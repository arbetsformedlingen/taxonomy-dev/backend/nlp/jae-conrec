# Results for 'b307407f8cf0efc70d1e7fdb87f7246e153ab247'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b307407f8cf0efc70d1e7fdb87f7246e153ab247](README.md) | 1 | 2719 | 27 | 19 | 178/396 = **45%** | 12/24 = **50%** |

## Source text

Legitimerad sjuksköterska till Sunderby sjukhus neurorehab/strokenheten Strokeenheten och Neurorehab tillhör Neurocentrum på Sunderby sjukhus. Vårdavdelningen har vårdplatser för inneliggande neurologisk vård och rehabilitering och ligger i tillgängliga lokaler på plan 4. I anslutning till avdelningen finns strokerehabteamet (SHR), strokemottagningen, baklofenmottagningen och rehabiliteringsmedicinska mottagningen.   Vi söker Strokeenheten/Neurorehab söker dig, en engagerad sjuksköterska som är intresserad av att jobba med personcentrerad vård och rehabilitering. Du tycker om att arbeta i team med patienter och kollegor och värdesätter att få vara en del i vårt pågående utvecklingsarbete.   Det här får du arbeta med Strokeenhetens målsättning är att arbeta med hela vårdkedjan, från det akuta insjuknandet i stroke tills det att patienten skrivs ut, ibland med stöd av vårt SHR-team. Hos strokeenheten arbetar du tillsammans med teamet utifrån tydliga rutiner som grundas i Socialstyrelsens Nationella riktlinjer för stroke. På Neurorehab har vi Norrbottens högsta rehabnivå och här vårdas och rehabiliteras patienter med bland annat ryggmärgsskada och förvärvad hjärnskada. Rehabiliteringen som bedrivs är på specialistnivå (ackrediterad enligt Carf) och hit kommer patienter från hela länet på remiss. På avdelningen arbetar vi i multiprofessionella team där sjuksköterska, arbetsterapeut, fysioterapeut, kurator, logoped, läkare, undersköterska, psykolog och rehabinstruktör ingår. Utöver detta har vi på avdelningen ett köksbiträde och utvecklingssjuksköterskor som bidrar till att du som sjuksköterska kan lägga ditt fokus på patienten. Hos oss är sjuksköterskans huvudsakliga ansvarsområde patientomvårdnaden. Där igår att dagligen leda, fördela, prioritera och utvärdera det behovsstyrda och rehabiliteringsfokuserade omvårdnadsarbetet.  Det här erbjuder vi dig - Utvecklings- och karriärmöjligheter där du får vara med och skapa framtidens hälsa och vård - Hälsofrämjande arbetsplatser med tillgång till friskvårdsaktiviteter och friskvårdsbidrag - Länk till våra förmåner: https://www.norrbotten.se/sv/Jobb-och-utbildning/Jobba-hos-oss/ -  4 veckors introduktion på avdelningen -  Att delta i ett basårsprogram tillsammans med andra nyanställda. - En personlig mentor - Deltagande i strokekompentensutbildning som genererar 3 högskolepoäng - Ett rehabkörkort som innebär en fördjupad kunskap i rehabiliteringsmetodik.  Information om tjänsten Tillsvidaretjänst på heltid. Arbetstid är dag, kväll och helg. Urval och intervjuer sker löpande och tjänsten kan tillsättas innan sista ansökningsdagen. Region Norrbotten tillämpar individuell lönesättning enligt kollektivavtal.  Välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Legitimerad **sjuksköterska** till Sunderby sjukhus neurore... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 2 | ... vårdplatser för inneliggande **neurologisk** vård och rehabilitering och l... | x |  |  | 11 | [Neurologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/Mtmj_FLt_uDk) |
| 3 | ...liggande neurologisk vård och **rehabilitering** och ligger i tillgängliga lok... | x | x | 14 | 14 | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| 4 | ...gen, baklofenmottagningen och **rehabiliteringsmedicinska** mottagningen.   Vi söker Stro... | x |  |  | 25 | [Rehabiliteringsmedicin, **skill**](http://data.jobtechdev.se/taxonomy/concept/URMC_Sxh_Lta) |
| 5 | ...rehab söker dig, en engagerad **sjuksköterska** som är intresserad av att job... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 6 | ... intresserad av att jobba med **personcentrerad vård** och rehabilitering. Du tycker... | x |  |  | 20 | [ge personcentrerad vård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/C53z_A9Z_djP) |
| 7 | ... med personcentrerad vård och **rehabilitering**. Du tycker om att arbeta i te... | x | x | 14 | 14 | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| 8 | ...å vara en del i vårt pågående **utvecklingsarbete**.   Det här får du arbeta med ... | x | x | 17 | 17 | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| 9 | ... stroke. På Neurorehab har vi **Norrbottens** högsta rehabnivå och här vård... | x |  |  | 11 | [Norrbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/9hXe_F4g_eTG) |
| 10 | ... multiprofessionella team där **sjuksköterska**, arbetsterapeut, fysioterapeu... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 10 | ... multiprofessionella team där **sjuksköterska**, arbetsterapeut, fysioterapeu... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 11 | ...nella team där sjuksköterska, **arbetsterapeut**, fysioterapeut, kurator, logo... | x | x | 14 | 14 | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| 12 | ...juksköterska, arbetsterapeut, **fysioterapeut**, kurator, logoped, läkare, un... | x | x | 13 | 13 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 12 | ...juksköterska, arbetsterapeut, **fysioterapeut**, kurator, logoped, läkare, un... |  | x |  | 13 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 13 | ...rbetsterapeut, fysioterapeut, **kurator**, logoped, läkare, undersköter... | x | x | 7 | 7 | [Kurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YpRs_ybt_47a) |
| 14 | ...peut, fysioterapeut, kurator, **logoped**, läkare, undersköterska, psyk... |  | x |  | 7 | [Logoped, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BnhE_hcg_H2c) |
| 14 | ...peut, fysioterapeut, kurator, **logoped**, läkare, undersköterska, psyk... | x |  |  | 7 | [logoped, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/mZpY_Kwy_mTi) |
| 15 | ...ioterapeut, kurator, logoped, **läkare**, undersköterska, psykolog och... | x | x | 6 | 6 | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| 16 | ...ut, kurator, logoped, läkare, **undersköterska**, psykolog och rehabinstruktör... | x | x | 14 | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 17 | ...oped, läkare, undersköterska, **psykolog** och rehabinstruktör ingår. Ut... |  | x |  | 8 | [Psykolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xBo9_V9m_bfg) |
| 17 | ...oped, läkare, undersköterska, **psykolog** och rehabinstruktör ingår. Ut... | x |  |  | 8 | [psykolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/yLc4_ZtB_oiN) |
| 18 | ...tta har vi på avdelningen ett **köksbiträde** och utvecklingssjuksköterskor... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 19 | ...or som bidrar till att du som **sjuksköterska** kan lägga ditt fokus på patie... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 20 | ...okus på patienten. Hos oss är **sjuksköterskans** huvudsakliga ansvarsområde pa... | x |  |  | 15 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 21 | ...vara med och skapa framtidens **hälsa** och vård - Hälsofrämjande arb... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 22 | ...betsplatser med tillgång till **friskvårdsaktiviteter** och friskvårdsbidrag - Länk t... | x |  |  | 21 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 23 | ...ill friskvårdsaktiviteter och **friskvårdsbidrag** - Länk till våra förmåner: ht... | x |  |  | 16 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 24 | ...dik.  Information om tjänsten **Tillsvidaretjänst** på heltid. Arbetstid är dag, ... | x |  |  | 17 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 25 | ...tjänsten Tillsvidaretjänst på **heltid**. Arbetstid är dag, kväll och ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 26 | ... innan sista ansökningsdagen. **Region **Norrbotten tillämpar individue... | x |  |  | 7 | [Norrbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/9hXe_F4g_eTG) |
| 27 | ...sista ansökningsdagen. Region **Norrbotten** tillämpar individuell lönesät... | x | x | 10 | 10 | [Norrbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/9hXe_F4g_eTG) |
| 28 | ...dividuell lönesättning enligt **kollektivavtal**.  Välkommen med din ansökan! | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **178** | **396** | 178/396 = **45%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Norrbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/9hXe_F4g_eTG) |
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
|  | x |  | [Logoped, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BnhE_hcg_H2c) |
| x |  |  | [ge personcentrerad vård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/C53z_A9Z_djP) |
| x | x | x | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| x |  |  | [Neurologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/Mtmj_FLt_uDk) |
| x |  |  | [Rehabiliteringsmedicin, **skill**](http://data.jobtechdev.se/taxonomy/concept/URMC_Sxh_Lta) |
| x | x | x | [Kurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YpRs_ybt_47a) |
| x | x | x | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| x | x | x | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| x |  |  | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [logoped, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/mZpY_Kwy_mTi) |
| x | x | x | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| x | x | x | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| x | x | x | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
|  | x |  | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| x | x | x | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
|  | x |  | [Psykolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xBo9_V9m_bfg) |
| x |  |  | [psykolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/yLc4_ZtB_oiN) |
| x |  |  | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **12** | 12/24 = **50%** |