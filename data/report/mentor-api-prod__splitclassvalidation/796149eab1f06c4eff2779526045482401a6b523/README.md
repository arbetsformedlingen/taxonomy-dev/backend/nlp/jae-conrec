# Results for '796149eab1f06c4eff2779526045482401a6b523'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [796149eab1f06c4eff2779526045482401a6b523](README.md) | 1 | 430 | 2 | 3 | 25/30 = **83%** | 2/3 = **67%** |

## Source text

M&Z kakel Bygg AB söker flera plattsättare VI växer och är i behov av fler duktiga medarbetare. Vi ser att du jobbat med plattsättning dagligen minst 1 år, känner dig självsäker och du vill lämna bra jobb. Skicka ansökan idag så kan vi titta på det och boka en personligt möte. Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | M&Z **kakel** Bygg AB söker flera plattsätt... |  | x |  | 5 | [lägga kakel, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MU2G_UW6_ncL) |
| 2 | M&Z kakel Bygg AB söker flera **plattsättare** VI växer och är i behov av fl... | x | x | 12 | 12 | [Plattsättare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Qv4e_brq_Ca5) |
| 3 | ...are. Vi ser att du jobbat med **plattsättning** dagligen minst 1 år, känner d... | x | x | 13 | 13 | [Plattsättning, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzuK_jNE_XCG) |
| | **Overall** | | | **25** | **30** | 25/30 = **83%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [lägga kakel, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MU2G_UW6_ncL) |
| x | x | x | [Plattsättare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Qv4e_brq_Ca5) |
| x | x | x | [Plattsättning, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzuK_jNE_XCG) |
| | | **2** | 2/3 = **67%** |