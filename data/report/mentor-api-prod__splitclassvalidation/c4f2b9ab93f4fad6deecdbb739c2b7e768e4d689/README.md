# Results for 'c4f2b9ab93f4fad6deecdbb739c2b7e768e4d689'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c4f2b9ab93f4fad6deecdbb739c2b7e768e4d689](README.md) | 1 | 5113 | 35 | 20 | 174/397 = **44%** | 12/26 = **46%** |

## Source text

Arbetsförmedlingen söker webbredaktör Välkommen till jobbet som får Sverige i arbete! Här arbetar du på regeringens uppdrag för en arbetsmarknad som hjälper människor framåt. Vi frigör möjligheter så att arbetssökande hittar sysselsättning och arbetsgivare sin nästa medarbetare. För att nå målet behöver vi samarbeta, i myndigheten men också med olika externa aktörer i hela landet. Tillsammans driver vi förändring, utvecklar nya arbetssätt och skapar lösningar som gör att Sverige kan fortsätta framåt. Vill du bli en av oss?     Är du vår nya webbredaktör? Är du en duktig webbredaktör som älskar innehåll i digitala kanaler? Är du alltid mån om att innehållet optimeras på bästa sätt för en bra kundupplevelse? Är du en trygg lagspelare som inte är rädd att kavla upp ärmarna när det behövs? Då ska du söka det här jobbet.  Om tjänsten Du kommer att tillhöra kommunikationsavdelningen och den sektion som ansvarar för innehållet i våra digitala kanaler. I rollen som webbredaktör får du möjlighet att vara med och driva Arbetsförmedlingens digitala utveckling framåt med fokus på kundens upplevelse.  Du kommer att ha en central roll i utvecklingsarbetet med vårt nya intranät såväl som vår webbplats där du såväl skapar innehåll som samarbetar med andra kompetenser som UX, AD, utvecklare och sakkunniga i team. Vi jobbar i Sitevision.  Arbetsuppgifter  - Skriva egna webbanpassade texter utifrån tillgänglighet, användbarhet och klarspråk  - Bearbeta andras texter utifrån klarspråk, tillgänglighet och sökmotoroptimering  - Ingå i kommunikationsteam som jobbar i ett specifikt uppdrag och utifrån din yrkesroll se till att det arbete som handlar om webbkanalen följer våra uppställda mål och riktlinjer  - Ansvara för egna områden som du som redaktör driver och utvecklar tillsammans med andra kompetenser som UX, AD och sakkunniga.  - Kvalitetsgranska innehåll och hålla kontakt med sakkunniga samt lokala redaktörer  Kvalifikationskrav  - Universitets- eller högskoleutbildning, 180hp/120p inom kommunikation, journalistik eller motsvarande yrkeshögskoleutbildning eller erfarenhet som vi bedömer likvärdig  - Minst tre års erfarenhet av webbredaktörsarbete och liknande arbete att driva redaktionell utveckling av webbplatser/intranät  - God kunskap i ett eller flera publiceringsverktyg och erfarenhet av att följa upp arbetet i något webbanalysverktyg, exempelvis GA, eller Matomo  - Kunskap om DOS-lagen, WCAG och språklagen  Meriterande  - Erfarenhet av arbete inom statliga myndigheter eller i stora organisationer  - Arbete med översättning på andra språk än svenska och/eller kunskaper i fler språk  - Erfarenhet av sökmotoroptimering och att skriva för responsiv webb  Personliga egenskaper  - Du är lösningsfokuserad, nyfiken och har ett starkt driv att nå resultat  - Du är genuint intresserad av att skapa bra innehåll som ger både kund- och verksamhetsnytta  - Du är drivande och tar egna initiativ samt har en förmåga att tänka nytt  - Du trivs med att entusiasmera andra och konkretisera vilka effekter som uppstår när man lyckas med bra kommunikation  - Du tycker om att ha många kontaktytor, att samarbeta i team och att dela med dig av din kunskap till andra.  Vi kommer att lägga stor vikt vid personlig lämplighet.  Utöver dessa kvalifikationer har du förmåga och fallenhet att arbeta i enlighet med Arbetsförmedlingens medarbetarskap och chefskap:  Medarbetare på Arbetsförmedlingen:   - Tar ansvar för sina arbetsprestationer och sitt bidrag till helheten  - Anpassar sig till förändring och oförutsedda händelser  - Tar initiativ till utveckling och bidrar till förbättring     Vårt erbjudande  Anställningsformen är en tillsvidareanställning på heltid som inleds med 6 månaders provanställning.  Placeringsorten är Solna. Möjlighet att arbeta på distans upp till 49 procent på årsbasis finns.  Vi erbjuder förmånliga semesteravtal, möjlighet till flextid, friskvård på arbetstid samt friskvårdsbidrag. Vår arbetsplats består av över 11 000 kompetenta och engagerade medarbetare. Genom att samarbeta och vara öppna för nya perspektiv utvecklas vi som individer och verksamhet. Tillsammans frigör vi möjligheter som får Sverige i arbete.  Läs mer om vår organisation och vårt uppdrag på vår webbplats https://arbetsformedlingen.se/om-oss  Ansökan Välkommen med din ansökan senast 2022-08-01  Du ansöker genom att klicka på knappen ”ansök” Bifoga ditt CV och ett personligt brev där du beskriver varför du söker jobbet, samt betyg/intyg som styrker kraven om utbildning och/eller arbetslivserfarenhet i din ansökan.  På Arbetsförmedlingen tillämpas en evidensbaserad rekryteringsprocess. Du kan läsa mer om vår rekryteringsprocess på vår hemsida https://arbetsformedlingen.se/om-oss/jobba-hos-oss  Urvalstest kan förekomma i vår rekryteringsprocess och vi kommer ge dig mer information om processen via e-post efter ansökningstiden gått ut.  Vi arbetar aktivt för att vara en myndighet fri från diskriminering. Det är viktigt för oss att all kompetens tas tillvara och vi välkomnar alla sökande. Vi ser positivt på om du har kunskaper i något av våra nationella minoritetsspråk.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Arbetsförmedlingen** söker webbredaktör Välkommen ... | x |  |  | 18 | [Arbetsförmedlingen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Mb2o_h3H_uGd) |
| 2 | Arbetsförmedlingen söker **webbredaktör** Välkommen till jobbet som får... | x | x | 12 | 12 | [Webbredaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b3Jk_Gfs_oo9) |
| 3 | ...Välkommen till jobbet som får **Sverige** i arbete! Här arbetar du på r... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ... skapar lösningar som gör att **Sverige** kan fortsätta framåt. Vill du... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 5 | ...n av oss?     Är du vår nya **webbredaktör**? Är du en duktig webbredaktör... | x | x | 12 | 12 | [Webbredaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b3Jk_Gfs_oo9) |
| 6 | ...webbredaktör? Är du en duktig **webbredaktör** som älskar innehåll i digital... | x | x | 12 | 12 | [Webbredaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b3Jk_Gfs_oo9) |
| 7 | ...igitala kanaler. I rollen som **webbredaktör** får du möjlighet att vara med... | x | x | 12 | 12 | [Webbredaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b3Jk_Gfs_oo9) |
| 8 | ...tar med andra kompetenser som **UX**, AD, utvecklare och sakkunnig... | x |  |  | 2 | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| 9 | ...med andra kompetenser som UX, **AD**, utvecklare och sakkunniga i ... | x | x | 2 | 2 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 10 | ...akkunniga i team. Vi jobbar i **Sitevision**.  Arbetsuppgifter  - Skriva e... | x | x | 10 | 10 | [Sitevision, CMS, **skill**](http://data.jobtechdev.se/taxonomy/concept/hicr_zWz_vaa) |
| 11 | ...exter utifrån tillgänglighet, **användbarhet** och klarspråk  - Bearbeta and... | x | x | 12 | 12 | [Användbarhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/wYxH_RXX_ZYx) |
| 12 | ...klarspråk, tillgänglighet och **sökmotoroptimering**  - Ingå i kommunikationsteam ... | x |  |  | 18 | [sökmotoroptimering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XvjF_hY7_aVy) |
| 13 | ...a för egna områden som du som **redaktör** driver och utvecklar tillsamm... | x | x | 8 | 8 | [redaktör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tRiS_ixp_5bD) |
| 14 | ...ans med andra kompetenser som **UX**, AD och sakkunniga.  - Kvalit... | x |  |  | 2 | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| 15 | ...med andra kompetenser som UX, **AD** och sakkunniga.  - Kvalitetsg... | x | x | 2 | 2 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 16 | ...som UX, AD och sakkunniga.  - **Kvalitetsgranska** innehåll och hålla kontakt me... | x |  |  | 16 | [utföra kvalitetsgranskningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/YYiw_EqD_owK) |
| 17 | ...kt med sakkunniga samt lokala **redaktörer**  Kvalifikationskrav  - Univer... |  | x |  | 10 | [redaktör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tRiS_ixp_5bD) |
| 18 | ...ktörer  Kvalifikationskrav  - **Universitets- eller högskoleutbildning**, 180hp/120p inom kommunikatio... | x | x | 38 | 38 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 19 | ...ts- eller högskoleutbildning, **180hp**/120p inom kommunikation, jour... | x |  |  | 5 | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| 20 | ...ler högskoleutbildning, 180hp/**120p** inom kommunikation, journalis... | x |  |  | 4 | [Högskoleutbildning, generell, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/rqLj_s2C_LTJ) |
| 21 | ...leutbildning, 180hp/120p inom **kommunikation**, journalistik eller motsvaran... | x |  |  | 13 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 22 | ...80hp/120p inom kommunikation, **journalistik** eller motsvarande yrkeshögsko... | x | x | 12 | 12 | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| 23 | ...vi bedömer likvärdig  - Minst **tre års erfarenhet** av webbredaktörsarbete och li... | x |  |  | 18 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 24 | ...ell utveckling av webbplatser/**intranät**  - God kunskap i ett eller fl... | x |  |  | 8 | [Intranätredaktör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3UXa_Fjr_dvz) |
| 25 | ...tomo  - Kunskap om DOS-lagen, **WCAG** och språklagen  Meriterande  ... | x | x | 4 | 4 | [Web Content Accessibility Guidelines/WCAG, **skill**](http://data.jobtechdev.se/taxonomy/concept/4Dqo_LPf_qrU) |
| 26 | ... organisationer  - Arbete med **översättning** på andra språk än svenska och... | x | x | 12 | 12 | [Översättning, **skill**](http://data.jobtechdev.se/taxonomy/concept/SLAe_RtF_RJZ) |
| 27 | ...versättning på andra språk än **svenska** och/eller kunskaper i fler sp... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 28 | ...i fler språk  - Erfarenhet av **sökmotoroptimering** och att skriva för responsiv ... | x |  |  | 18 | [sökmotoroptimering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XvjF_hY7_aVy) |
| 29 | ... och att skriva för responsiv **webb**  Personliga egenskaper  - Du ... | x |  |  | 4 | [Webb, **skill**](http://data.jobtechdev.se/taxonomy/concept/KKQV_aGF_bWq) |
| 30 | ...r  - Du är lösningsfokuserad, **nyfiken** och har ett starkt driv att n... | x |  |  | 7 | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
| 31 | ...och chefskap:  Medarbetare på **Arbetsförmedlingen**:   - Tar ansvar för sina arbe... | x |  |  | 18 | [Arbetsförmedlingen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Mb2o_h3H_uGd) |
| 32 | ...nde  Anställningsformen är en **tillsvidareanställning** på heltid som inleds med 6 må... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 33 | ... en tillsvidareanställning på **heltid** som inleds med 6 månaders pro... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 34 | ...tällning.  Placeringsorten är **Solna**. Möjlighet att arbeta på dist... | x | x | 5 | 5 | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| 35 | ...vtal, möjlighet till flextid, **friskvård** på arbetstid samt friskvårdsb... | x |  |  | 9 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 36 | ...frigör vi möjligheter som får **Sverige** i arbete.  Läs mer om vår org... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 37 | ...erfarenhet i din ansökan.  På **Arbetsförmedlingen** tillämpas en evidensbaserad r... | x |  |  | 18 | [Arbetsförmedlingen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Mb2o_h3H_uGd) |
| | **Overall** | | | **174** | **397** | 174/397 = **44%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Intranätredaktör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3UXa_Fjr_dvz) |
| x | x | x | [Web Content Accessibility Guidelines/WCAG, **skill**](http://data.jobtechdev.se/taxonomy/concept/4Dqo_LPf_qrU) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| x | x | x | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| x |  |  | [Webb, **skill**](http://data.jobtechdev.se/taxonomy/concept/KKQV_aGF_bWq) |
| x |  |  | [Arbetsförmedlingen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Mb2o_h3H_uGd) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| x | x | x | [Översättning, **skill**](http://data.jobtechdev.se/taxonomy/concept/SLAe_RtF_RJZ) |
| x |  |  | [sökmotoroptimering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XvjF_hY7_aVy) |
| x |  |  | [utföra kvalitetsgranskningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/YYiw_EqD_owK) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x | x | x | [Webbredaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b3Jk_Gfs_oo9) |
| x | x | x | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| x | x | x | [Sitevision, CMS, **skill**](http://data.jobtechdev.se/taxonomy/concept/hicr_zWz_vaa) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x |  |  | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
| x |  |  | [Högskoleutbildning, generell, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/rqLj_s2C_LTJ) |
| x | x | x | [redaktör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tRiS_ixp_5bD) |
| x |  |  | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| x | x | x | [Användbarhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/wYxH_RXX_ZYx) |
| x |  |  | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| x | x | x | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **12** | 12/26 = **46%** |