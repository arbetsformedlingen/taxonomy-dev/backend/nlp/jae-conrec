# Results for '13c65c752f14f43ef31d5e9a30ee723e47d8d89f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [13c65c752f14f43ef31d5e9a30ee723e47d8d89f](README.md) | 1 | 4932 | 43 | 37 | 214/673 = **32%** | 18/44 = **41%** |

## Source text

Naturbruksskolan Sötåsen söker timanställd kock/måltidsbiträde Med målsättningen - Vi utvecklar kompetens i världsklass för en hållbar framtid och det goda livet, driver Naturbruksförvaltningen utbildning inom ungdomsgymnasium, särskola, vuxenutbildning och yrkeshögskola. Naturbruksskolorna Svenljunga, Sötåsen, Uddetorp och Biologiska yrkeshögskolan fungerar som kompetenscentrum för de gröna näringarnas tillväxt och utveckling. Tillsammans med näringsliv och forskare driver förvaltningen energiprojekt och försöksverksamhet i framkant på skolorna. Naturbruksförvaltningen är en del av Västra Götalandsregionen.    Beskrivning På natursköna Sötåsen utanför Töreboda hittar du ett centrum för ekologiskt och hållbart naturbruk.  Här bedrivs ekologiskt lantbruk, kravgodkänd djurhållning med kor, grisar och får och storskaliga ekologiska trädgårdsodlingar. Här har vi även en anläggning för hundträning och ett djurhus med allt från ormar och reptiler till gnagare och fåglar. På Sötåsen finns helt enkelt det mesta inom djur och natur. Här kan man läsa Naturbruksprogrammet med inriktning lantbruk, djurvård och trädgård med totalt 11 olika yrkesutgångar. Vi driver vårt eget lantbruk där eleverna får utvecklas och lära sig i en verklig miljö. Det finns ca 190 gymnasieelever på Sötåsen där flertalet bor på skolans internat. Här finns också gymnasiesärskolans program för Skog, mark och djur (SMD). Det bedrivs också vuxenutbildning på skolan. På Sötåsen utbildas nästa generation inom den gröna näringen och vi strävar alltid efter att ligga i framkant med ny teknik och hållbara arbetssätt. Hos oss har du möjlighet att göra skillnad och bidra till utveckling av den gröna näringen.  Vi söker nu ett serviceinriktat måltidsbiträde till vår måltidsverksamhet!  Arbetsuppgifter Vi söker vikarie som vid behov kan arbeta inom alla delar av skolrestaurangen. Du kommer arbeta i måltidsproduktionen, servering av skolmåltider, disk, städ samt med egenkontroll.  Du får ta del av alla för arbetslaget förekommande arbetsuppgifter. Vi arbetar efter VGR:s miljömål för hållbar utveckling. Målet är att servera tilltalande, näringsriktiga och hållbara måltider med stor andel egenproducerade råvaror.   Kvalifikationer Vi ser gärna att du har en gymnasial utbildning  inom restaurang och storhushåll/ restaurang och livsmedel samt erfarenhet från måltidsservice eller restaurangservering. Meriterande är erfarenhet av egenkontroll, kundservice samt hantering av kassasystem.    Vi söker en engagerade, serviceinriktad person med hög social förmåga. Du ska tycka om att arbeta med ungdomar och vill verka för att alla ska trivas under sin skoltid hos oss. Du har en hög servicekänsla med gästen i fokus, och arbetar flexibilitet för att lösa uppgifter tillsammans med övriga i teamet. Du har god samarbetsförmåga men kan också själv ta initiativ för att kunna lösa problem som uppstår.    B-körkort och grundläggande datakunskap erfordras. Vi fäster stor vikt vid personlig lämplighet.  Villkor Timanställning, inringning vid behov   Arbetstiden är förlagd dag- och kvällstid måndag-fredag  För arbete inom skola krävs ett utdrag ur belastningsregistret för skola, som du ansöker om via https://polisen.se/tjanster-tillstand/belastningsregistret/skola-eller-forskola/. Urval- och intervjuarbete kommer att ske löpande under ansökningstiden så sök redan idag!  Vi på Naturbruksförvaltningen ser mångfald som en styrka och ser gärna en jämn könsfördelning på våra enheter.  Vi ser framemot din ansökan!  Om Västra Götalandsregionen Västra Götalandsregionen finns till för människorna i Västra Götaland. Vi ser till att det finns god hälso- och sjukvård för alla. Vi arbetar för en hållbar utveckling och tillväxt, bra miljö, förbättrad folkhälsa, ett rikt kulturliv och goda kommunikationer i hela Västra Götaland. Västra Götalandsregionen arbetar aktivt för att digitalisera fler arbetssätt. För att vara en del av den digitala omvandlingen har du med dig grundläggande färdigheter och kan använda digitala verktyg och tjänster. Du kan söka information, kommunicera, interagera digitalt, är riskmedveten och har motivation att delta i utvecklingen för att lära nytt.   Vill du veta mer om Västra Götalandsregionen kan du besöka vår introduktion till nya medarbetare på länken https://www.vgregion.se/introduktion   Ansökan Västra Götalandsregionen ser helst att du registrerar din ansökan via rekryteringssystemet. Om du som sökande har frågor om den utannonserade tjänsten eller av särskilda och speciella skäl inte kan registrera dina uppgifter i ett offentligt system - kontakta kontaktperson för respektive annons.   Till bemannings-, förmedlings- och rekryteringsföretag och till dig som är försäljare: Vi undanber oss vänligen men bestämt direktkontakt med bemannings-, förmedlings- och rekryteringsföretag samt andra externa aktörer och försäljare av ytterligare jobbannonser. Västra Götalandsregionen har upphandlade avtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...aturbruksskolan Sötåsen söker **timanställd** kock/måltidsbiträde Med målsä... | x |  |  | 11 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 2 | ...lan Sötåsen söker timanställd **kock**/måltidsbiträde Med målsättnin... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 3 | ...ötåsen söker timanställd kock/**måltidsbiträde** Med målsättningen - Vi utveck... | x | x | 14 | 14 | [Måltidsbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Tson_Zgt_zhT) |
| 4 | ...ldning inom ungdomsgymnasium, **särskola**, vuxenutbildning och yrkeshög... | x | x | 8 | 8 | [Särskola, **skill**](http://data.jobtechdev.se/taxonomy/concept/tcgM_8bo_yiv) |
| 5 | ...m ungdomsgymnasium, särskola, **vuxenutbildning** och yrkeshögskola. Naturbruks... | x |  |  | 15 | [vuxenutbildning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kvm2_qrF_3zc) |
| 6 | ...shögskola. Naturbruksskolorna **Svenljunga**, Sötåsen, Uddetorp och Biolog... |  | x |  | 10 | [Svenljunga, **municipality**](http://data.jobtechdev.se/taxonomy/concept/rZWC_pXf_ySZ) |
| 7 | ...forskare driver förvaltningen **energiprojekt** och försöksverksamhet i framk... |  | x |  | 13 | [samarbeta inom internationella energiprojekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Kt6M_PVt_Kaf) |
| 8 | ...uksförvaltningen är en del av **Västra Götalandsregionen**.    Beskrivning På naturskö... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 9 | ...På natursköna Sötåsen utanför **Töreboda** hittar du ett centrum för eko... |  | x |  | 8 | [Töreboda, **municipality**](http://data.jobtechdev.se/taxonomy/concept/a15F_gAH_pn6) |
| 10 | ...m för ekologiskt och hållbart **naturbruk**.  Här bedrivs ekologiskt la... |  | x |  | 9 | [Naturbruk, allmän, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4LeU_szT_aXo) |
| 10 | ...m för ekologiskt och hållbart **naturbruk**.  Här bedrivs ekologiskt la... |  | x |  | 9 | [Naturbruk, hästhållning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4ZhW_so3_jNF) |
| 10 | ...m för ekologiskt och hållbart **naturbruk**.  Här bedrivs ekologiskt la... |  | x |  | 9 | [Naturbruk, djurvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/5uJJ_RNE_x6c) |
| 10 | ...m för ekologiskt och hållbart **naturbruk**.  Här bedrivs ekologiskt la... | x | x | 9 | 9 | [Naturbruk, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/VuuL_7CH_adj) |
| 10 | ...m för ekologiskt och hållbart **naturbruk**.  Här bedrivs ekologiskt la... |  | x |  | 9 | [Naturbruk, undervisning högskolenivå, **skill**](http://data.jobtechdev.se/taxonomy/concept/ddzL_9as_rZt) |
| 10 | ...m för ekologiskt och hållbart **naturbruk**.  Här bedrivs ekologiskt la... |  | x |  | 9 | [Naturbruk, trädgård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/nHgb_vSN_dx2) |
| 10 | ...m för ekologiskt och hållbart **naturbruk**.  Här bedrivs ekologiskt la... |  | x |  | 9 | [Lärarutbildning, naturbruk, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/qPMH_VjX_thE) |
| 11 | ...uk.  Här bedrivs ekologiskt **lantbruk**, kravgodkänd djurhållning med... | x |  |  | 8 | [Lantbruk, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/RXMT_9Ui_TJJ) |
| 12 | ...logiskt lantbruk, kravgodkänd **djurhållning** med kor, grisar och får och s... | x | x | 12 | 12 | [Djurhållning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/DNtU_pnw_uZT) |
| 13 | ...urhus med allt från ormar och **reptiler** till gnagare och fåglar. På S... | x | x | 8 | 8 | [Reptiler, **skill**](http://data.jobtechdev.se/taxonomy/concept/69FX_FxA_6b4) |
| 13 | ...urhus med allt från ormar och **reptiler** till gnagare och fåglar. På S... |  | x |  | 8 | [föda upp reptiler, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ShYA_VMf_L1W) |
| 13 | ...urhus med allt från ormar och **reptiler** till gnagare och fåglar. På S... |  | x |  | 8 | [Reptiler, konservering, **skill**](http://data.jobtechdev.se/taxonomy/concept/fnKp_TdM_HEp) |
| 14 | ...och reptiler till gnagare och **fåglar**. På Sötåsen finns helt enkelt... |  | x |  | 6 | [Fåglar, konservering, **skill**](http://data.jobtechdev.se/taxonomy/concept/kzLE_9UL_tDt) |
| 15 | ...ns helt enkelt det mesta inom **djur** och natur. Här kan man läsa N... | x | x | 4 | 4 | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| 16 | ...ruksprogrammet med inriktning **lantbruk**, djurvård och trädgård med to... | x |  |  | 8 | [Lantbruk, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/RXMT_9Ui_TJJ) |
| 17 | ...mmet med inriktning lantbruk, **djurvård** och trädgård med totalt 11 ol... |  | x |  | 8 | [Utbildning i djurvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/88zZ_fdd_nJE) |
| 17 | ...mmet med inriktning lantbruk, **djurvård** och trädgård med totalt 11 ol... | x | x | 8 | 8 | [Djurvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hX6q_xhz_eei) |
| 18 | ...ktning lantbruk, djurvård och **trädgård** med totalt 11 olika yrkesutgå... | x | x | 8 | 8 | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| 19 | ...utgångar. Vi driver vårt eget **lantbruk** där eleverna får utvecklas oc... | x |  |  | 8 | [Lantbruk, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/RXMT_9Ui_TJJ) |
| 20 | ...ymnasiesärskolans program för **Skog**, mark och djur (SMD). Det bed... | x |  |  | 4 | [Skog, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/UAAw_NYh_3uY) |
| 21 | ...esärskolans program för Skog, **mark** och djur (SMD). Det bedrivs o... | x |  |  | 4 | [Mark/infrastruktur-Bentley InRoads Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/tCER_yDZ_XnW) |
| 22 | ...ns program för Skog, mark och **djur** (SMD). Det bedrivs också vuxe... | x | x | 4 | 4 | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| 23 | ... söker nu ett serviceinriktat **måltidsbiträde** till vår måltidsverksamhet!  ... | x | x | 14 | 14 | [Måltidsbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Tson_Zgt_zhT) |
| 24 | ...et!  Arbetsuppgifter Vi söker **vikarie** som vid behov kan arbeta inom... | x |  |  | 7 | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| 25 | ...idsproduktionen, servering av **skolmåltider**, disk, städ samt med egenkont... | x | x | 12 | 12 | [Skolmåltider, **skill**](http://data.jobtechdev.se/taxonomy/concept/ogdH_rrn_NWT) |
| 26 | ...etar efter VGR:s miljömål för **hållbar utveckling**. Målet är att servera tilltal... | x |  |  | 18 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 27 | ...er Vi ser gärna att du har en **gymnasial utbildning**  inom restaurang och storhush... | x | x | 20 | 20 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 28 | ...en gymnasial utbildning  inom **restaurang** och storhushåll/ restaurang o... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 29 | ...bildning  inom restaurang och **storhushåll**/ restaurang och livsmedel sam... | x | x | 11 | 11 | [Storhushåll, **keyword**](http://data.jobtechdev.se/taxonomy/concept/VfLr_eEg_ABZ) |
| 30 | ...m restaurang och storhushåll/ **restaurang** och livsmedel samt erfarenhet... | x |  |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 31 | ...h storhushåll/ restaurang och **livsmedel** samt erfarenhet från måltidss... | x | x | 9 | 9 | [Livsmedel, **skill**](http://data.jobtechdev.se/taxonomy/concept/tdbu_fUL_MnC) |
| 32 | ...het från måltidsservice eller **restaurangservering**. Meriterande är erfarenhet av... | x |  |  | 19 | [Restaurang, servering, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GqXD_ArB_Mmg) |
| 33 | ...r erfarenhet av egenkontroll, **kundservice** samt hantering av kassasystem... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 34 | ...genkontroll, kundservice samt **hantering av kassasystem**.    Vi söker en engagerade, s... | x |  |  | 24 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 35 | ...kundservice samt hantering av **kassasystem**.    Vi söker en engagerade, s... |  | x |  | 11 | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| 36 | ... lösa problem som uppstår.    **B-körkort** och grundläggande datakunskap... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 37 | ...ersonlig lämplighet.  Villkor **Timanställning**, inringning vid behov   Arbet... | x |  |  | 14 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 38 | ...ser framemot din ansökan!  Om **Västra Götalandsregionen** Västra Götalandsregionen finn... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 39 | ...  Om Västra Götalandsregionen **Västra Götaland**sregionen finns till för männi... | x | x | 15 | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 40 | ...alandsregionen Västra Götaland**sregionen** finns till för människorna i ... | x |  |  | 9 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 41 | ... finns till för människorna i **Västra Götaland**. Vi ser till att det finns go... | x |  |  | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 42 | ...Vi ser till att det finns god **hälso**- och sjukvård för alla. Vi ar... | x |  |  | 5 | [Hälso, **keyword**](http://data.jobtechdev.se/taxonomy/concept/5TsD_dN8_Uco) |
| 43 | ...Vi ser till att det finns god **hälso- oc**h sjukvård för alla. Vi arbeta... |  | x |  | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 44 | ...d för alla. Vi arbetar för en **hållbar utveckling** och tillväxt, bra miljö, förb... | x |  |  | 18 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 45 | ...llväxt, bra miljö, förbättrad **folkhälsa**, ett rikt kulturliv och goda ... | x | x | 9 | 9 | [folkhälsa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jEdA_1Fe_Jji) |
| 46 | ...h goda kommunikationer i hela **Västra Götaland**. Västra Götalandsregionen arb... | x | x | 15 | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 47 | ...ioner i hela Västra Götaland. **Västra Götalandsregionen** arbetar aktivt för att digita... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 48 | ...eter och kan använda digitala **verktyg** och tjänster. Du kan söka inf... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 49 | ...a nytt.   Vill du veta mer om **Västra Götalandsregionen** kan du besöka vår introduktio... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 50 | ...ion.se/introduktion   Ansökan **Västra Götalandsregionen** ser helst att du registrerar ... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| | **Overall** | | | **214** | **673** | 214/673 = **32%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Naturbruk, allmän, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4LeU_szT_aXo) |
|  | x |  | [Naturbruk, hästhållning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4ZhW_so3_jNF) |
| x |  |  | [Hälso, **keyword**](http://data.jobtechdev.se/taxonomy/concept/5TsD_dN8_Uco) |
|  | x |  | [Naturbruk, djurvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/5uJJ_RNE_x6c) |
| x | x | x | [Reptiler, **skill**](http://data.jobtechdev.se/taxonomy/concept/69FX_FxA_6b4) |
|  | x |  | [Utbildning i djurvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/88zZ_fdd_nJE) |
| x | x | x | [Djurhållning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/DNtU_pnw_uZT) |
| x |  |  | [Restaurang, servering, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GqXD_ArB_Mmg) |
| x |  |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
|  | x |  | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
|  | x |  | [samarbeta inom internationella energiprojekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Kt6M_PVt_Kaf) |
| x |  |  | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| x |  |  | [Lantbruk, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/RXMT_9Ui_TJJ) |
|  | x |  | [föda upp reptiler, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ShYA_VMf_L1W) |
| x | x | x | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x | x | x | [Måltidsbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Tson_Zgt_zhT) |
| x |  |  | [Skog, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/UAAw_NYh_3uY) |
| x |  |  | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Storhushåll, **keyword**](http://data.jobtechdev.se/taxonomy/concept/VfLr_eEg_ABZ) |
| x | x | x | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| x | x | x | [Naturbruk, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/VuuL_7CH_adj) |
| x | x | x | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
|  | x |  | [Töreboda, **municipality**](http://data.jobtechdev.se/taxonomy/concept/a15F_gAH_pn6) |
|  | x |  | [Naturbruk, undervisning högskolenivå, **skill**](http://data.jobtechdev.se/taxonomy/concept/ddzL_9as_rZt) |
|  | x |  | [Reptiler, konservering, **skill**](http://data.jobtechdev.se/taxonomy/concept/fnKp_TdM_HEp) |
| x |  |  | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| x | x | x | [Djurvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hX6q_xhz_eei) |
|  | x |  | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| x | x | x | [folkhälsa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jEdA_1Fe_Jji) |
| x |  |  | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| x |  |  | [vuxenutbildning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kvm2_qrF_3zc) |
|  | x |  | [Fåglar, konservering, **skill**](http://data.jobtechdev.se/taxonomy/concept/kzLE_9UL_tDt) |
|  | x |  | [Naturbruk, trädgård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/nHgb_vSN_dx2) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Skolmåltider, **skill**](http://data.jobtechdev.se/taxonomy/concept/ogdH_rrn_NWT) |
|  | x |  | [Lärarutbildning, naturbruk, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/qPMH_VjX_thE) |
|  | x |  | [Svenljunga, **municipality**](http://data.jobtechdev.se/taxonomy/concept/rZWC_pXf_ySZ) |
| x |  |  | [Mark/infrastruktur-Bentley InRoads Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/tCER_yDZ_XnW) |
| x | x | x | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| x | x | x | [Särskola, **skill**](http://data.jobtechdev.se/taxonomy/concept/tcgM_8bo_yiv) |
| x | x | x | [Livsmedel, **skill**](http://data.jobtechdev.se/taxonomy/concept/tdbu_fUL_MnC) |
| x | x | x | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x | x | x | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| | | **18** | 18/44 = **41%** |