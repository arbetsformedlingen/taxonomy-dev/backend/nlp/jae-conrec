# Results for '1eef6beebb71dca2e2e4e1af244e8cbb4e2209a9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1eef6beebb71dca2e2e4e1af244e8cbb4e2209a9](README.md) | 1 | 3261 | 21 | 14 | 119/265 = **45%** | 9/18 = **50%** |

## Source text

Uppdragsledande Arkitekt Att utveckla ett hållbart samhälle kräver engagemang, helhetssyn och förmåga att knyta ihop enskilda expertkunskaper till en komplett lösning. Detta är en del av vardagen för oss på Norconsult. Vi är en av Nordens största rådgivare inom samhällsplanering och projektering. Med helhetsperspektiv och tydlig miljöprofil hjälper vi våra uppdragsgivare att lyckas på resan från vision till verklighet.  Job DescriptionHar du en god gestaltningsförmåga och engageras av hållbar arkitektur? Värdesätts ditt engagemang och din energi där du är idag? Norconsult är ett medarbetarägt bolag med platt organisation och schyssta värderingar.     Din roll  Team Arkitektur & Byggkonstruktion består av 60 medarbetare, fördelade på fyra studios i Luleå, Piteå och Umeå. Vi erbjuder en stor bredd av projekttyper och söker nu dig som vill utvecklas som handläggande arkitekt och vara delaktig i den spännande utvecklingsfas vi befinner oss i. Vi hjälper våra kunder att förverkliga sina visioner och projekt med fokus på hållbar, funktionell och vacker arkitektur. Vi arbetar med allt ifrån tidiga skisser i detaljplaneskede fram till bygghandlingsprojektering.  Just nu har vi mycket att göra och på ritbordet finns bl.a. skolor, förskolor, badhus, kontor, bostäder, vård- och omsorgsprojekt, industri och intressanta stadsutvecklingsprojekt. Förutom samarbetet inom teamet ger Norconsults bredd, förutsättningar att tidigt i projekt arbeta tvärfackligt med våra egna konstruktörer, akustiker, VVS-ingenjörer, trafikplanerare, miljökonsulter m.fl.    Din profil  Du är utbildad arkitekt, har arbetat minst 5 år som handläggande och är uppdragsledande, alternativt redo att kliva in i en uppdragsledande roll. Du har erfarenhet från byggprojektets alla faser, från utredningar och tidig gestaltning till projektering. Vi tror att du gillar att samarbeta med andra och att du brinner för god gestaltning och hållbarhet. Som person är du öppen, engagerad och kreativ eftersom vår strävan är att ha de trevligaste kontoren som finns i landet!    Vårt erbjudande  På Norconsult har vi som ambition att spegla det samhälle vi lever i, det gör oss till en samhällsbyggare med helhetssyn. Därför eftersträvar vi rekryteringar som leder till ökad mångfald - såväl inom bolaget som stort som i aktuellt team.  Norconsult har ett stort fokus på hållbarhetsfrågor, både ur ett ekologiskt, socialt och ekonomiskt perspektiv. Vi är 5000 medarbetare i koncernen och 1000 medarbetare i Sverige utspridda på 36 kontor.  Att arbeta tillsammans med oss här på Norconsult innebär att du får vara med och forma företagets och din egen framtid då vi är ett 100 % medarbetarägt företag. Med ägarengagemang, erfarenhet och intresse för samhällsbyggnad, utvecklar du tillsammans med övriga kompetenser hållbara lösningar för våra kunder.   Bekanta dig med din blivande ledare: https://www.linkedin.com/in/magnus-kieri-24554ab/ https://www.linkedin.com/in/josefine-lindblom-6b287580/  Välkommen med din ansökan!  Our OfferEn anställning i ett företag som till 100 % ägs av sina medarbetare Spännande och intressanta projekt Bra anställningsvillkor och goda karriärmöjligheter Kompetensutveckling Samarbete med många olika kompetenser över hela Sverige och i olika länder NA

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Uppdragsledande **Arkitekt** Att utveckla ett hållbart sam... | x |  |  | 8 | [Arkitekt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XMWR_V22_WS1) |
| 2 | ...ordens största rådgivare inom **samhällsplanering** och projektering. Med helhets... | x | x | 17 | 17 | [Samhällsplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5FMU_Dt4_2hF) |
| 3 | ...re inom samhällsplanering och **projektering**. Med helhetsperspektiv och ty... | x | x | 12 | 12 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 4 | ...måga och engageras av hållbar **arkitektur**? Värdesätts ditt engagemang o... | x |  |  | 10 | [Arkitekter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/41KB_bfk_5jR) |
| 5 | ...rderingar.     Din roll  Team **Arkitektur** & Byggkonstruktion består av ... | x |  |  | 10 | [Arkitekter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/41KB_bfk_5jR) |
| 6 | ..., fördelade på fyra studios i **Luleå**, Piteå och Umeå. Vi erbjuder ... | x | x | 5 | 5 | [Luleå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CXbY_gui_14v) |
| 7 | ...lade på fyra studios i Luleå, **Piteå** och Umeå. Vi erbjuder en stor... | x | x | 5 | 5 | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| 8 | ...ra studios i Luleå, Piteå och **Umeå**. Vi erbjuder en stor bredd av... | x | x | 4 | 4 | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| 9 | ...nu dig som vill utvecklas som **handläggande arkitekt** och vara delaktig i den spänn... | x | x | 21 | 21 | [Handläggande arkitekt, **job-title**](http://data.jobtechdev.se/taxonomy/concept/bGXH_bxX_z7S) |
| 10 | ...a. skolor, förskolor, badhus, **kontor**, bostäder, vård- och omsorgsp... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 11 | ... med våra egna konstruktörer, **akustiker**, VVS-ingenjörer, trafikplaner... | x |  |  | 9 | [akustik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pzSz_jBn_Jo3) |
| 12 | ...gna konstruktörer, akustiker, **VVS-ingenjörer**, trafikplanerare, miljökonsul... | x | x | 14 | 14 | [VVS-ingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8wPx_j4J_ZTh) |
| 13 | ...r, akustiker, VVS-ingenjörer, **trafikplanerare**, miljökonsulter m.fl.    Din ... |  | x |  | 15 | [Trafikplanerare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1awg_QKB_bkw) |
| 13 | ...r, akustiker, VVS-ingenjörer, **trafikplanerare**, miljökonsulter m.fl.    Din ... | x |  |  | 15 | [transportplanerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ngxm_jjS_hXk) |
| 13 | ...r, akustiker, VVS-ingenjörer, **trafikplanerare**, miljökonsulter m.fl.    Din ... |  | x |  | 15 | [Trafikplanerare, transportföretag, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b7PE_DjA_ypp) |
| 14 | ...-ingenjörer, trafikplanerare, **miljökonsulter** m.fl.    Din profil  Du är ut... |  | x |  | 14 | [Miljökonsult, **job-title**](http://data.jobtechdev.se/taxonomy/concept/214y_LZd_XxU) |
| 15 | ...   Din profil  Du är utbildad **arkitekt**, har arbetat minst 5 år som h... | x |  |  | 8 | [Arkitekt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XMWR_V22_WS1) |
| 16 | ... Du är utbildad arkitekt, har **arbetat minst 5 år** som handläggande och är uppdr... | x |  |  | 18 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 17 | ...t, har arbetat minst 5 år som **handläggande** och är uppdragsledande, alter... | x |  |  | 12 | [Handläggande arkitekt, **job-title**](http://data.jobtechdev.se/taxonomy/concept/bGXH_bxX_z7S) |
| 18 | ...ar och tidig gestaltning till **projektering**. Vi tror att du gillar att sa... | x | x | 12 | 12 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 19 | ...cernen och 1000 medarbetare i **Sverige** utspridda på 36 kontor.  Att ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 20 | ...are i Sverige utspridda på 36 **kontor**.  Att arbeta tillsammans med ... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 21 | ..., erfarenhet och intresse för **samhällsbyggnad**, utvecklar du tillsammans med... | x | x | 15 | 15 | [Samhällsbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/KEPE_tsb_fB8) |
| 22 | ...a olika kompetenser över hela **Sverige** och i olika länder NA | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **119** | **265** | 119/265 = **45%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Trafikplanerare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1awg_QKB_bkw) |
|  | x |  | [Miljökonsult, **job-title**](http://data.jobtechdev.se/taxonomy/concept/214y_LZd_XxU) |
| x |  |  | [Arkitekter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/41KB_bfk_5jR) |
| x | x | x | [Samhällsplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5FMU_Dt4_2hF) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| x | x | x | [VVS-ingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8wPx_j4J_ZTh) |
| x | x | x | [Luleå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CXbY_gui_14v) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [Samhällsbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/KEPE_tsb_fB8) |
| x |  |  | [transportplanerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ngxm_jjS_hXk) |
| x | x | x | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| x |  |  | [Arkitekt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XMWR_V22_WS1) |
|  | x |  | [Trafikplanerare, transportföretag, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b7PE_DjA_ypp) |
| x | x | x | [Handläggande arkitekt, **job-title**](http://data.jobtechdev.se/taxonomy/concept/bGXH_bxX_z7S) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [akustik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pzSz_jBn_Jo3) |
| x | x | x | [Piteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/umej_bP2_PpK) |
| x | x | x | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| | | **9** | 9/18 = **50%** |