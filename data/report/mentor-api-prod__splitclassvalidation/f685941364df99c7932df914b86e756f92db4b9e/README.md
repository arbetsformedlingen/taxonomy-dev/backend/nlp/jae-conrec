# Results for 'f685941364df99c7932df914b86e756f92db4b9e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f685941364df99c7932df914b86e756f92db4b9e](README.md) | 1 | 4853 | 48 | 29 | 179/513 = **35%** | 13/38 = **34%** |

## Source text

Utvecklare av kommunikationslösningar till pre-print avdelning Har du ett intresse för Design, kommunikation och IT? Då kan detta vara rollen för dig! Här kommer du att utföra det förberedande arbetet inför att grafiskt material ska printas. Du kommer arbeta varierat med allt från enklare programmering till design och det visuella, allt som behövs innan materialet trycks för att skickas till kund! Du kommer att ta stort ansvar och arbeta med hela processen från beställning till färdigställt tryck.   OM TJÄNSTEN  I denna roll så kommer du att bli en del av ett Preprint-team om 3 personer. Ni arbetar tillsammans med att förbereda digitalt material och grafisk kommunikation inför print, utskick eller vidare digital distribution. Som "dokumentutvecklare" i Preprint- gruppen arbetar du med en bred variation av arbetsuppgifter. Företaget arbetar på uppdrag av kunder och har ofta flera uppdrag i gång samtidigt. Främst kommer ditt arbete innebära programmering av variabeldata kopplade till databaser (SQL, Access), men även andra datakällor som exempelvis. xml-, textfiler och Excel. Vi utvecklar och administrerar dokumentmallar som ligger som grund i våra system ut mot kunder. Utöver detta ingår även en hel del layout och originalarbete av dokument åt kunder. Du kommer att arbeta med utveckling av lösningar för dynamisk kundkommunikation och i denna roll så krävs det både ett stort intresse för det tekniska men även design.  Huvudsakligen kommer du att arbeta i Indesign och därmed också i XMPie, men även i de andra program i Adobe CC-sviten.  #  Du erbjuds   * En möjlighet att arbeta på ett stortframgångsrikt bolag * Ett mindre team där ni störrar varandra och hjälps åt * En dedikerad konsultchef hos Academic Work hos stöttar dig i karriären   Som konsult för Academic Work erbjuder vi stora möjligheter för dig att växa professionellt, bygga ditt nätverk och skapa värdefulla kontakter för framtiden. Läs mer om vårt konsulterbjudande.  VI SÖKER DIG SOM  I denna roll så söker vi dig som :   * Gynmasial eller meriterande eftergymnasial utbildning inom media eller IT * Stort tekniskt intresse och erfarenhet av att arbeta i en IT-intensiv miljö * God förståelse för grafiska processer och i en logisk förståelse för flöden och datakällor * Hanterar enklare prorgrammering och script, främst i SQL * God tidigare erfarenhet av InDesign och övriga Adobe paketet * Erfarenget och kunskap i SQL & Access   I denna roll så kommer du att arbeta i flertalet program och system, nedan så listas de vanligaste programmen du kommer att arbeta i. Det är meriterande om du har utbildning och tidigare erfarenhet i följande, specificera gärna vilka porgram du använt tidigare i din ansökan :  Xerox FreeFlow Print, EFI Fiery, Onyx, Xerox Process Manager Workflow Software, Xerox FreeFlow, Core Workflow Software, XmPie, Haikom (E-handel) mycket bra om erfarenhet kring HiKom finns.  Utöver detta ser vi främst att du har erfarenhet av Adobe Indesign, men gärna även :  Adobe Photoshop, Illustrator, Adobe Acrobat Pro, Enfocus Pitstop, Adobe Dreamweaver  Som person så är du är noggrann, strukturerad och stresstålig. Vi söker dig som uppskattar att arbeta i team men samtidigt kan driva ditt eget arbete. Du är intresserad av att vara med och utveckla nya lösningar som ger värde för både fötrtaget och för kunden. Då vi arbetar i en variationsrik verksamhet med stundtals högt tempo är det oerhört viktigt att kan du hantera flera olika uppgifter parallellt utan att tappa fokus. Eftersom du kommer att ha kontakt med våra kunder och leverantörer är det ett krav att du behärskar svenska och engelska obehindrat i både tal och skrift.  #  #  Övrig information   * Start: I augusti, med flexibelt startdatum * Omfattning: Heltid, 08:00-17:00 * Placering: Flemingsberg, Stockholm   Rekryteringsprocessen hanteras av Academic Work och kundens önskemål är att alla frågor rörande tjänsten hanteras av Academic Work.  Vi går igenom urvalet löpande och annonsen kan stängas ner innan tjänsten är tillsatt om vi gått över till urvals- och intervjufasen.  INFORMATION OM FÖRETAGET  Med lika delar passion, intelligens och engagemang hjälper kund sina våra kunder med logistik- kommunikation och trycklösningar. Kund tar med glädje hand om hela flödet, allt från systemutveckling och lagerhållning till plock och pack, distribution och svarshantering.  All vår verksamhet kretsar kring att leda och säkra våra kunders behov av effektiva lösningar för logistik, lagerhantering och distribution. För att leva upp till löftet och leveransen har vi ca 120 engagerade medarbetare i vår koncern i norden, en väl utbyggd maskinpark, 30 000 pallplatser, utvecklade lagersystem och marknadsverktyg, samt egna serverhallar.  Vi bryr oss. Vi vågar. Vi ser inga problem och Vi lever ordning och reda.  Vi älskar det vi gör. Och vi gör allt för att det ska märkas.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...arierat med allt från enklare **programmering** till design och det visuella,... | x | x | 13 | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 2 | ...ån enklare programmering till **design** och det visuella, allt som be... | x |  |  | 6 | [Grafisk design, **skill**](http://data.jobtechdev.se/taxonomy/concept/dTK5_R7G_p56) |
| 3 | ...re digital distribution. Som "**dokumentutvecklare**" i Preprint- gruppen arbetar ... |  | x |  | 18 | [Dokumentutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/A9nu_Lbz_zBK) |
| 4 | ...t kommer ditt arbete innebära **programmering** av variabeldata kopplade till... | x | x | 13 | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 5 | ...av variabeldata kopplade till **databaser** (SQL, Access), men även andra... | x |  |  | 9 | [använda databaser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GbAF_v3z_PeS) |
| 5 | ...av variabeldata kopplade till **databaser** (SQL, Access), men även andra... |  | x |  | 9 | [Databaser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/Uj5W_dft_Ssg) |
| 6 | ...data kopplade till databaser (**SQL**, Access), men även andra data... |  | x |  | 3 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 6 | ...data kopplade till databaser (**SQL**, Access), men även andra data... | x |  |  | 3 | [SQL-Base, databashanterare, **skill**](http://data.jobtechdev.se/taxonomy/concept/r6iV_q4u_a7B) |
| 7 | ...kopplade till databaser (SQL, **Access**), men även andra datakällor s... | x |  |  | 6 | [Access, databashanterare, **skill**](http://data.jobtechdev.se/taxonomy/concept/aroP_kTr_WGc) |
| 8 | ...empelvis. xml-, textfiler och **Excel**. Vi utvecklar och administrer... | x | x | 5 | 5 | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| 9 | ...r detta ingår även en hel del **layout** och originalarbete av dokumen... | x |  |  | 6 | [Layout, **skill**](http://data.jobtechdev.se/taxonomy/concept/rPMH_yr9_h7d) |
| 10 | ...der. Du kommer att arbeta med **utveckling av lösningar** för dynamisk kundkommunikatio... | x |  |  | 23 | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| 11 | ...kligen kommer du att arbeta i **Indesign** och därmed också i XMPie, men... | x | x | 8 | 8 | [Layout-Adobe InDesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2t4_aS8_tGo) |
| 12 | ...men även i de andra program i **Adobe CC**-sviten.  #  Du erbjuds   * En... | x |  |  | 8 | [använda Creative Suite, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UVR1_J8S_BiS) |
| 13 | ...oll så söker vi dig som :   * **Gynmasial** eller meriterande eftergymnas... | x |  |  | 9 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 14 | ...* Gynmasial eller meriterande **eftergymnasial utbildning** inom media eller IT * Stort t... | x | x | 25 | 25 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 15 | ...ftergymnasial utbildning inom **media** eller IT * Stort tekniskt int... | x |  |  | 5 | [Kultur, media, design, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/9puE_nYg_crq) |
| 16 | ...l utbildning inom media eller **IT** * Stort tekniskt intresse och... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 17 | ...erfarenhet av att arbeta i en **IT**-intensiv miljö * God förståel... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 18 | ...iv miljö * God förståelse för **grafiska processer** och i en logisk förståelse fö... | x |  |  | 18 | [Grafisk design, **skill**](http://data.jobtechdev.se/taxonomy/concept/dTK5_R7G_p56) |
| 19 | ...r grafiska processer och i en **logisk** förståelse för flöden och dat... |  | x |  | 6 | [tillämpa logisk programmering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XgJ7_LyX_Xw4) |
| 19 | ...r grafiska processer och i en **logisk** förståelse för flöden och dat... |  | x |  | 6 | [använda logiskt tänkande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rdhB_SJE_x3U) |
| 20 | ...datakällor * Hanterar enklare **prorgrammering** och script, främst i SQL * Go... | x |  |  | 14 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 21 | ...ammering och script, främst i **SQL** * God tidigare erfarenhet av ... | x | x | 3 | 3 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 22 | ... * God tidigare erfarenhet av **InDesign** och övriga Adobe paketet * Er... | x | x | 8 | 8 | [Layout-Adobe InDesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2t4_aS8_tGo) |
| 23 | ...et * Erfarenget och kunskap i **SQL** & Access   I denna roll så ko... |  | x |  | 3 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 23 | ...et * Erfarenget och kunskap i **SQL** & Access   I denna roll så ko... | x |  |  | 3 | [SQL-Base, databashanterare, **skill**](http://data.jobtechdev.se/taxonomy/concept/r6iV_q4u_a7B) |
| 24 | ...rfarenget och kunskap i SQL & **Access**   I denna roll så kommer du a... | x |  |  | 6 | [Access, databashanterare, **skill**](http://data.jobtechdev.se/taxonomy/concept/aroP_kTr_WGc) |
| 25 | ...ämst att du har erfarenhet av **Adobe Indesign**, men gärna även :  Adobe Phot... | x | x | 14 | 14 | [Layout-Adobe InDesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2t4_aS8_tGo) |
| 26 | ...e Indesign, men gärna även :  **Adobe Photoshop**, Illustrator, Adobe Acrobat P... | x | x | 15 | 15 | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
| 27 | ...ärna även :  Adobe Photoshop, **Illustrator**, Adobe Acrobat Pro, Enfocus P... | x | x | 11 | 11 | [2D-grafik-Adobe Illustrator, **skill**](http://data.jobtechdev.se/taxonomy/concept/FKub_QFH_Btk) |
| 27 | ...ärna även :  Adobe Photoshop, **Illustrator**, Adobe Acrobat Pro, Enfocus P... |  | x |  | 11 | [Adobe Illustrator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bGA6_xqh_Cvv) |
| 28 | ...Adobe Photoshop, Illustrator, **Adobe Acrobat** Pro, Enfocus Pitstop, Adobe D... | x |  |  | 13 | [Adobe Acrobat, **skill**](http://data.jobtechdev.se/taxonomy/concept/RB7E_jE5_MZ3) |
| 29 | ...Acrobat Pro, Enfocus Pitstop, **Adobe Dreamweaver**  Som person så är du är noggr... |  | x |  | 17 | [Webbdesign-Adobe Dreamweaver, **skill**](http://data.jobtechdev.se/taxonomy/concept/F4nq_ZH3_s3u) |
| 29 | ...Acrobat Pro, Enfocus Pitstop, **Adobe Dreamweaver**  Som person så är du är noggr... | x | x | 17 | 17 | [Adobe Dreamweaver, **skill**](http://data.jobtechdev.se/taxonomy/concept/FiV2_Gk2_bG9) |
| 30 | ...eaver  Som person så är du är **noggrann**, strukturerad och stresstålig... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 31 | ...är noggrann, strukturerad och **stresstålig**. Vi söker dig som uppskattar ... | x |  |  | 11 | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
| 31 | ...är noggrann, strukturerad och **stresstålig**. Vi söker dig som uppskattar ... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 32 | ... söker dig som uppskattar att **arbeta i team** men samtidigt kan driva ditt ... | x |  |  | 13 | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| 33 | ...et oerhört viktigt att kan du **hantera flera olika uppgifter parallellt** utan att tappa fokus. Efterso... | x |  |  | 40 | [göra flera saker samtidigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/b2nY_5aQ_FFv) |
| 34 | ...det ett krav att du behärskar **svenska** och engelska obehindrat i båd... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 35 | ... att du behärskar svenska och **engelska** obehindrat i både tal och skr... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 36 | ...00 * Placering: Flemingsberg, **Stockholm**   Rekryteringsprocessen hante... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 37 | ...AGET  Med lika delar passion, **intelligens** och engagemang hjälper kund s... |  | x |  | 11 | [ha emotionell intelligens, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bfd9_A4v_LDb) |
| 38 | ...per kund sina våra kunder med **logistik**- kommunikation och trycklösni... | x |  |  | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 39 | ...and om hela flödet, allt från **systemutveckling** och lagerhållning till plock ... | x | x | 16 | 16 | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| 40 | ...ov av effektiva lösningar för **logistik**, lagerhantering och distribut... | x | x | 8 | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 41 | ...ktiva lösningar för logistik, **lagerhantering** och distribution. För att lev... |  | x |  | 14 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 42 | ...0 000 pallplatser, utvecklade **lagersystem** och marknadsverktyg, samt egn... |  | x |  | 11 | [Lagerhanteringsprogram, **skill**](http://data.jobtechdev.se/taxonomy/concept/1oA3_GfA_82Y) |
| | **Overall** | | | **179** | **513** | 179/513 = **35%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Lagerhanteringsprogram, **skill**](http://data.jobtechdev.se/taxonomy/concept/1oA3_GfA_82Y) |
| x | x | x | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| x |  |  | [Kultur, media, design, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/9puE_nYg_crq) |
|  | x |  | [Dokumentutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/A9nu_Lbz_zBK) |
|  | x |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [Webbdesign-Adobe Dreamweaver, **skill**](http://data.jobtechdev.se/taxonomy/concept/F4nq_ZH3_s3u) |
| x | x | x | [2D-grafik-Adobe Illustrator, **skill**](http://data.jobtechdev.se/taxonomy/concept/FKub_QFH_Btk) |
| x | x | x | [Adobe Dreamweaver, **skill**](http://data.jobtechdev.se/taxonomy/concept/FiV2_Gk2_bG9) |
| x |  |  | [använda databaser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GbAF_v3z_PeS) |
|  | x |  | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [Adobe Acrobat, **skill**](http://data.jobtechdev.se/taxonomy/concept/RB7E_jE5_MZ3) |
| x |  |  | [använda Creative Suite, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UVR1_J8S_BiS) |
|  | x |  | [Databaser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/Uj5W_dft_Ssg) |
| x |  |  | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
|  | x |  | [tillämpa logisk programmering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XgJ7_LyX_Xw4) |
| x | x | x | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| x | x | x | [Layout-Adobe InDesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2t4_aS8_tGo) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Access, databashanterare, **skill**](http://data.jobtechdev.se/taxonomy/concept/aroP_kTr_WGc) |
| x |  |  | [göra flera saker samtidigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/b2nY_5aQ_FFv) |
|  | x |  | [Adobe Illustrator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bGA6_xqh_Cvv) |
|  | x |  | [ha emotionell intelligens, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bfd9_A4v_LDb) |
| x | x | x | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| x | x | x | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| x |  |  | [Grafisk design, **skill**](http://data.jobtechdev.se/taxonomy/concept/dTK5_R7G_p56) |
| x | x | x | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
| x |  |  | [SQL-Base, databashanterare, **skill**](http://data.jobtechdev.se/taxonomy/concept/r6iV_q4u_a7B) |
| x |  |  | [Layout, **skill**](http://data.jobtechdev.se/taxonomy/concept/rPMH_yr9_h7d) |
|  | x |  | [använda logiskt tänkande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rdhB_SJE_x3U) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| x |  |  | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| x | x | x | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
| x | x | x | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **13** | 13/38 = **34%** |