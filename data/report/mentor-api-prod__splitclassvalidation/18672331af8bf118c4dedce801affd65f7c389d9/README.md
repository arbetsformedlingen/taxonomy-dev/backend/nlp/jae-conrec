# Results for '18672331af8bf118c4dedce801affd65f7c389d9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [18672331af8bf118c4dedce801affd65f7c389d9](README.md) | 1 | 3348 | 34 | 40 | 216/416 = **52%** | 10/12 = **83%** |

## Source text

myNanny barnvakt Skövde, nanny / barnpassning, sporadiska tillfällen På myNanny barnvakt är vårt mål är att erbjuda Sveriges bästa barnpassning. Var med och dela glädjen av att passa barn och göra skillnad för familjer runtom i Sverige! Som nanny hos myNanny har du ett flexibelt, givande & glädjefyllt extrajobb hos som ett av Sveriges ledande företag för barnpassning. Just nu vill rekordmånga familjer ha nannies från myNanny och det är med glädje som vi nu utökar myNanny-familjen med flera duktiga och passionerade barnvakter!   Läs mer om myNanny barnvakt: https://my-nanny.se/ och ansök redan idag på: https://my-nanny.se/extrajobb-barnvakt/   Varför arbeta med barnpassning på myNanny? Mycket flexibelt extrajobb, vanligtvis arbetar du 2-4 timmar / veckan Bra lön, 90 till 130 kr / timmen beroende på avstånd till familjen Viktig och relevant merit för framtida jobb Du är alltid försäkrad under barnpassningen Kontinuerligt stöd från din personliga kontaktperson som är specialist på barnpassning på myNanny   Passar du som barnpassare på myNanny? Vi söker dig som: Är ansvarsfull och lekfull med ett genuint engagemang för barn Tidigare har passat ett eller flera barn (0 - 6 år) helt självständigt Kan arbeta flexibelt både vardagar och helger som nanny   Hur ansöker jag till nanny på myNanny? Enkelt och smidigt på vår hemsida Ansökan tar endast några minuter och du blir sedan kallad till intervju för jobbet som nanny på myNanny    En av våra många familjer som behöver hjälp nära dig:  Vad som krävs av dig som söker arbetet: Erfarenhet av barnpassning i åldrarna 0-6 år. Antal barn och ålder: 1 barn, 3 år. Dagar och tider: Familjen vill ha hjälp vid sporadiska tillfällen (ca 1 kväll i månaden) med start så snart som möjligt. De kan tänka sig passning på vilka veckodagar som helst. Önskat startdatum för barnpassningen: Omgående! Barnpassningen sker hemma hos familjen Plats är myNanny barnvakt Skövde Ref.nr: RN92999    Om du inte kan hjälpa just den här familjen, så rekommenderar vi dig att söka en generell tjänst som barnvakt på myNanny direkt på vår hemsida. Då får du sedan erbjudanden om fler uppdrag som passar dig.   En annan fördel med för alla nannies på myNanny är att du kan arbeta i hela Sverige, oavsett om du bor i storstad eller på landsbygd. Bland annat:   myNanny barnvakt Stockholm myNanny barnvakt Göteborg myNanny barnvakt Malmö myNanny barnvakt Uppsala myNanny barnvakt Västerås Hitta fler städer på vår hemsida    Vårt mål är att erbjuda Sveriges bästa barnpassning med myNanny. Var med och dela glädjen av att passa barn och göra skillnad för familjer runtom i Sverige! Just nu vill rekordmånga familjer få hjälp av en myNanny barnpassning och vi ser fram emot att få utöka myNanny-familjen med flera duktiga och passionerade myNanny barnvakter!   Många möjligheter: Utöver att arbeta hos oss på https://my-nanny.se/barnvakt/ kan du även få fler uppdrag som nanny via våra kollegor på https://nannypoppins.se/. Dessutom kan du om du vill även ge läxhjälp via https://www.allakando.se/laxhjalp/ och https://smartstudies.se/laxhjalp/. En förmån är även att du som har arbetat en längre period hos oss även får tillgång till t.ex. en kurs inför högskoleprovet kostnadsfritt: https://www.allakando.se/hogskoleprovet/   Ansök till extrajobbet som barnvakt på myNanny: https://my-nanny.se/extrajobb-barnvakt/

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | my**Nanny** barnvakt Skövde, nanny / barn... |  | x |  | 5 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 2 | myNanny **barnvakt** Skövde, nanny / barnpassning,... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 3 | myNanny barnvakt **Skövde**, nanny / barnpassning, sporad... | x | x | 6 | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 4 | myNanny barnvakt Skövde, **nanny** / barnpassning, sporadiska ti... | x |  |  | 5 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 5 | ...anny barnvakt Skövde, nanny / **barnpassning**, sporadiska tillfällen På myN... | x | x | 12 | 12 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 6 | ...radiska tillfällen På myNanny **barnvakt** är vårt mål är att erbjuda Sv... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 7 | ...kt är vårt mål är att erbjuda **Sveriges** bästa barnpassning. Var med o... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 8 | ...är att erbjuda Sveriges bästa **barnpassning**. Var med och dela glädjen av ... |  | x |  | 12 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 9 | ...killnad för familjer runtom i **Sverige**! Som nanny hos myNanny har du... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 10 | ...amiljer runtom i Sverige! Som **nanny** hos myNanny har du ett flexib... | x | x | 5 | 5 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 11 | ...yllt extrajobb hos som ett av **Sveriges** ledande företag för barnpassn... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 12 | ... Sveriges ledande företag för **barnpassning**. Just nu vill rekordmånga fam... |  | x |  | 12 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 13 | ... vill rekordmånga familjer ha **nannies** från myNanny och det är med g... | x |  |  | 7 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 14 | ...lera duktiga och passionerade **barnvakter**!   Läs mer om myNanny barnvak... | x | x | 10 | 10 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 15 | ...nvakter!   Läs mer om myNanny **barnvakt**: https://my-nanny.se/ och ans... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 16 | ...barnvakt/   Varför arbeta med **barnpassning** på myNanny? Mycket flexibelt ... |  | x |  | 12 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 17 | ...ktperson som är specialist på **barnpassning** på myNanny   Passar du som ba... |  | x |  | 12 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 18 | ...ng på myNanny   Passar du som **barnpassare** på myNanny? Vi söker dig som:... |  | x |  | 11 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 19 | ...myNanny? Vi söker dig som: Är **ansvarsfull** och lekfull med ett genuint e... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 20 | ...agemang för barn Tidigare har **passat ett eller flera barn** (0 - 6 år) helt självständigt... | x |  |  | 27 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 21 | ...er flera barn (0 - 6 år) helt **självständigt** Kan arbeta flexibelt både var... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 22 | ... både vardagar och helger som **nanny**   Hur ansöker jag till nanny ... | x | x | 5 | 5 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 23 | ... nanny   Hur ansöker jag till **nanny** på myNanny? Enkelt och smidig... | x | x | 5 | 5 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 24 | ... till intervju för jobbet som **nanny** på myNanny    En av våra mång... | x | x | 5 | 5 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 25 | ...ävs av dig som söker arbetet: **Erfarenhet av **barnpassning i åldrarna 0-6 år... | x |  |  | 14 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 26 | ... söker arbetet: Erfarenhet av **barnpassning** i åldrarna 0-6 år. Antal barn... | x | x | 12 | 12 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 27 | ...hos familjen Plats är myNanny **barnvakt** Skövde Ref.nr: RN92999    Om ... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 28 | ...jen Plats är myNanny barnvakt **Skövde** Ref.nr: RN92999    Om du inte... | x | x | 6 | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 29 | ...t söka en generell tjänst som **barnvakt** på myNanny direkt på vår hems... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 30 | ...y är att du kan arbeta i hela **Sverige**, oavsett om du bor i storstad... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 31 | ...sbygd. Bland annat:   myNanny **barnvakt** Stockholm myNanny barnvakt Gö... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 32 | ...and annat:   myNanny barnvakt **Stockholm** myNanny barnvakt Göteborg myN... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 33 | ...ny barnvakt Stockholm myNanny **barnvakt** Göteborg myNanny barnvakt Mal... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 34 | ...kt Stockholm myNanny barnvakt **Göteborg** myNanny barnvakt Malmö myNann... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 35 | ...nny barnvakt Göteborg myNanny **barnvakt** Malmö myNanny barnvakt Uppsal... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 36 | ...akt Göteborg myNanny barnvakt **Malmö** myNanny barnvakt Uppsala myNa... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 37 | ...yNanny barnvakt Malmö myNanny **barnvakt** Uppsala myNanny barnvakt Väst... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 38 | ...rnvakt Malmö myNanny barnvakt **Uppsala** myNanny barnvakt Västerås Hit... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 39 | ...anny barnvakt Uppsala myNanny **barnvakt** Västerås Hitta fler städer på... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 40 | ...vakt Uppsala myNanny barnvakt **Västerås** Hitta fler städer på vår hems... | x | x | 8 | 8 | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| 41 | ...är att erbjuda Sveriges bästa **barnpassning** med myNanny. Var med och dela... |  | x |  | 12 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 42 | ...killnad för familjer runtom i **Sverige**! Just nu vill rekordmånga fam... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 43 | ...miljer få hjälp av en myNanny **barnpassning** och vi ser fram emot att få u... |  | x |  | 12 | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| 44 | ...tiga och passionerade myNanny **barnvakter**!   Många möjligheter: Utöver ... |  | x |  | 10 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 45 | ... arbeta hos oss på https://my-**nanny**.se/barnvakt/ kan du även få f... |  | x |  | 5 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 46 | ...tom kan du om du vill även ge **läxhjälp** via https://www.allakando.se/... |  | x |  | 8 | [ge läxhjälp åt barn, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/p32M_H4P_cEK) |
| 47 | ...   Ansök till extrajobbet som **barnvakt** på myNanny: https://my-nanny.... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| | **Overall** | | | **216** | **416** | 216/416 = **52%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [barnpassning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m5G2_tuy_2uZ) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
|  | x |  | [ge läxhjälp åt barn, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/p32M_H4P_cEK) |
| x | x | x | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| | | **10** | 10/12 = **83%** |