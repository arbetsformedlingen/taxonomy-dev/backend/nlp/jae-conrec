# Results for '5892bf11fda649d176599e0fef802ed71fc7241c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [5892bf11fda649d176599e0fef802ed71fc7241c](README.md) | 1 | 2137 | 16 | 6 | 46/216 = **21%** | 3/7 = **43%** |

## Source text

Servispersonal till Lisebergs restauranger 2022 (säsongsanställning) Liseberg är en av Nordens ledande turistattraktioner med 3,4 miljoner gäster årligen. Verksamheten består av nöjespark, restauranger, teatrar och boende, och sysselsätter omkring 350 årsanställda och ca 2300 säsongsanställda. Liseberg omsätter årligen ca 1,2 miljard kronor och ingår i koncernen Stadshus AB som är helägt av Göteborgs Stad.     Vi söker servis- & barpersonal till restaurangerna på Liseberg, kanske är du en av våra nya kollegor?  På våra restauranger får du arbeta med service när den är som bäst i en av Europas ledande Nöjesparker.  Du får arbeta med likasinnade och kommer få vänner för livet. Som Lisebergare kan du njuta av vår vackra park med allt vad det innebär. När du är ledig kan du provåka våra attraktioner, äta på våra restauranger och se på framträdanden som sker på våra olika scener.  Att vara Lisebergare på våra restauranger  Arbetar du hos oss är du med och skapar den goda stämning som våra restauranger är kända för. Du blir i din roll viktig för våra gästers helhetsupplevelse och gör skillnad för såväl gäster som dina kollegor. Hos oss kommer du att tillhöra ett gott gäng som tillsammans ger våra gäster restaurangupplevelser att minnas efter deras enskilda behov.   Vem är du som söker dessa tjänster?  Vi tror att du har tidigare erfarenhet från restaurangbranschen som exempelvis servis- eller barpersonal. Du är uthållig till din natur och slutför det du åtagit dig. Du uppskattar att ge ett gott bemötande och känner dig trygg med tempoväxlingar.   Har du inte erfarenhet sedan tidigare men har ett brinnande intresse för restaurang? Inga problem! Skicka in din ansökan ändå och våra duktiga medarbetare lär upp dig.   Ett krav för att jobba hos oss är att du har fyllt 18 år. Vi ser gärna sökanden med olika bakgrund och erfarenheter då vi strävar mot en bred representation i organisationen.  Vill du veta mer om våra restauranger, gå in på http://liseberg.se.  Vi rekryterar löpande så vänta inte med att skicka in din ansökan men senast den 31/7.  Varmt välkommen att anmäla ditt intresse för att bli en av oss!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Servispersonal** till Lisebergs restauranger 2... | x | x | 14 | 14 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | Servispersonal till Lisebergs **restauranger** 2022 (säsongsanställning) Lis... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 3 | ... Lisebergs restauranger 2022 (**säsongsanställning**) Liseberg är en av Nordens le... | x |  |  | 18 | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
| 4 | ...samheten består av nöjespark, **restauranger**, teatrar och boende, och syss... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 5 | ...r av nöjespark, restauranger, **teatrar** och boende, och sysselsätter ... | x |  |  | 7 | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| 6 | ... Stadshus AB som är helägt av **Göteborgs Stad**.     Vi söker servis- & bar... | x |  |  | 14 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 7 | ...öteborgs Stad.     Vi söker **servis**- & barpersonal till restauran... | x |  |  | 6 | [Servis, **job-title**](http://data.jobtechdev.se/taxonomy/concept/7coY_HT4_rJc) |
| 8 | ...tad.     Vi söker servis- & **barpersonal** till restaurangerna på Lisebe... | x | x | 11 | 11 | [Barpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3dar_69H_gjG) |
| 9 | ...er servis- & barpersonal till **restaurangerna** på Liseberg, kanske är du en ... | x |  |  | 14 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 10 | ...v våra nya kollegor?  På våra **restauranger** får du arbeta med service när... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 11 | ...åra attraktioner, äta på våra **restauranger** och se på framträdanden som s... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 12 | ... Att vara Lisebergare på våra **restauranger**  Arbetar du hos oss är du med... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 13 | ...ar den goda stämning som våra **restauranger** är kända för. Du blir i din r... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 14 | ... har tidigare erfarenhet från **restaurangbranschen** som exempelvis servis- eller ... |  | x |  | 19 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 15 | ...urangbranschen som exempelvis **servis**- eller barpersonal. Du är uth... | x |  |  | 6 | [Servis, **job-title**](http://data.jobtechdev.se/taxonomy/concept/7coY_HT4_rJc) |
| 16 | ...urangbranschen som exempelvis **servis- eller **barpersonal. Du är uthållig ti... |  | x |  | 14 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 17 | ... som exempelvis servis- eller **barpersonal**. Du är uthållig till din natu... | x | x | 11 | 11 | [Barpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3dar_69H_gjG) |
| 18 | ...ar ett brinnande intresse för **restaurang**? Inga problem! Skicka in din ... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| | **Overall** | | | **46** | **216** | 46/216 = **21%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Barpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3dar_69H_gjG) |
| x |  |  | [Servis, **job-title**](http://data.jobtechdev.se/taxonomy/concept/7coY_HT4_rJc) |
| x |  |  | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
| x |  |  | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x |  |  | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| | | **3** | 3/7 = **43%** |