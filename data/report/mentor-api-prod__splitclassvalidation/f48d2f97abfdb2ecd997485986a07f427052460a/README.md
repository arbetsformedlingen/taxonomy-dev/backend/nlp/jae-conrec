# Results for 'f48d2f97abfdb2ecd997485986a07f427052460a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f48d2f97abfdb2ecd997485986a07f427052460a](README.md) | 1 | 1777 | 13 | 4 | 15/117 = **13%** | 2/5 = **40%** |

## Source text

Studiochef till nytt massagekoncept Nu kommer Refresh till storsatsningen Arninge Hälsan Alla älskar massage. Det är en fantastisk produkt med endast positiva associationer. Hur vi ska få massage grumlar intrycket och försvårar. Refresh är ett nytt koncept som förändrar kundens upplevelse totalt. Genom att leverera en high-end produkt med tillgänglighet och enkelhet på en ny nivå. Vårt mål är att skapa en ny rutin för välmående genom regelbunden massage … paketerat till ett livsstilsval. Nu etablerar vi oss i Täby och slår oss samman med Arninge Hälsan.  Vi söker efter duktiga massörer som vill vara kärnan i en ny massageupplevelse. Var med från början och utforma morgondagens massagebransch tillsammans med oss.  Detta är en unik möjlighet för dig som brinner för massage och har entreprenörens hjärta. Vill du forma morgondagens massagebransch? Älskar du förändring och har svårt att ens tänka i boxen? Då kommer du trivas med oss. Vi letar efter dig som: Är passionerad om massage och älskar att ha kunden i fokus Drivs av utmaningen och vill vara med att förändra massagebranchen  Vågar stå i fokus och vara en frontfigur för en massagestudio Kan enkelt slutföra vår 50 minuters signaturmassage, handleda och stå för kvaliteten hos dina medarbetare Trives i en miljö med hög takt och snabb förändring   Vi erbjuder: En nytänkande och innovativ arbetsmiljö Konkurrenskraftig fast lön med bonustillägg  Möjlighet att avancera i en snabbt växande konceptutveckling  Var med i vårt team och vår spännande resa till att förändra massagebranchen. Skicka din intresseanmälan så tar vi direkt personlig kontakt med dig. Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Studiochef till nytt **massage**koncept Nu kommer Refresh till... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 2 | ...en Arninge Hälsan Alla älskar **massage**. Det är en fantastisk produkt... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 3 | ... associationer. Hur vi ska få **massage** grumlar intrycket och försvår... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 4 | ...r välmående genom regelbunden **massage** … paketerat till ett livsstil... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 5 | ...ilsval. Nu etablerar vi oss i **Täby** och slår oss samman med Arnin... | x | x | 4 | 4 | [Täby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/onpA_B5a_zfv) |
| 6 | ...lsan.  Vi söker efter duktiga **massörer** som vill vara kärnan i en ny ... |  | x |  | 8 | [Massör/Massageterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/mC28_vEv_AVx) |
| 7 | ... som vill vara kärnan i en ny **massage**upplevelse. Var med från börja... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 8 | ...rjan och utforma morgondagens **massage**bransch tillsammans med oss.  ... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 9 | ...ighet för dig som brinner för **massage** och har entreprenörens hjärta... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 10 | ...a. Vill du forma morgondagens **massage**bransch? Älskar du förändring ... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 11 | ...er dig som: Är passionerad om **massage** och älskar att ha kunden i fo... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 12 | ...ch vill vara med att förändra **massage**branchen  Vågar stå i fokus oc... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 13 | ...och vara en frontfigur för en **massage**studio Kan enkelt slutföra vår... | x |  |  | 7 | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
| 14 | ...: En nytänkande och innovativ **arbetsmiljö** Konkurrenskraftig fast lön me... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 15 | ... avancera i en snabbt växande **konceptutveckling**  Var med i vårt team och vår ... |  | x |  | 17 | [Konceptutveckling, UX, **skill**](http://data.jobtechdev.se/taxonomy/concept/ceUs_fcn_miS) |
| | **Overall** | | | **15** | **117** | 15/117 = **13%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Hårvård, skönhetsvård och massage, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/8xVi_MZF_dQL) |
|  | x |  | [Konceptutveckling, UX, **skill**](http://data.jobtechdev.se/taxonomy/concept/ceUs_fcn_miS) |
|  | x |  | [Massör/Massageterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/mC28_vEv_AVx) |
| x | x | x | [Täby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/onpA_B5a_zfv) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **2** | 2/5 = **40%** |