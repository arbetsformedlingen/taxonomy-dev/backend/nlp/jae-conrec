# Results for '9511f2c1644b8eaede25a996d2281e125f3032e9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9511f2c1644b8eaede25a996d2281e125f3032e9](README.md) | 1 | 734 | 5 | 5 | 73/73 = **100%** | 4/4 = **100%** |

## Source text

Anestesisjuksköterska, bemanning Jämför och chatta med bemanningsbolagen enkelt via Vårdföretag App (vardforetag.se) Skapa din profil på mindre än 1 minut och lägg upp dina önskemål. Få erbjudanden på uppdrag som kan passa dina önskemål och chatta med flera av de ledande bemanningsbolagen inom vården. Enkelt och kostnadsfritt! Just nu finns ett ledigt uppdrag som anestesisjuksköterska inom operation i Oskarshamn, Kalmar län. Uppdraget finns att söka bland flera bemanningsbolag i Vårdföretag.se   Fördelar: -Hög ersättning -Inga onödiga Mail Eller telefonsamtal -Enkelt och kostnadsfritt -Inget bindande   Vill du starta eget aktiebolag? Vi på vardforetag.se hjälper dig att starta aktiebolag och redovisning till fasta kostnader.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Anestesisjuksköterska**, bemanning Jämför och chatta ... | x | x | 21 | 21 | [Anestesisjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4qYQ_tWb_QjD) |
| 2 | ... finns ett ledigt uppdrag som **anestesisjuksköterska** inom operation i Oskarshamn, ... | x | x | 21 | 21 | [Anestesisjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4qYQ_tWb_QjD) |
| 3 | ...juksköterska inom operation i **Oskarshamn**, Kalmar län. Uppdraget finns ... | x | x | 10 | 10 | [Oskarshamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/tUP8_hRE_NcF) |
| 4 | ... inom operation i Oskarshamn, **Kalmar län**. Uppdraget finns att söka bla... | x | x | 10 | 10 | [Kalmar län, **region**](http://data.jobtechdev.se/taxonomy/concept/9QUH_2bb_6Np) |
| 5 | ...dig att starta aktiebolag och **redovisning** till fasta kostnader. | x | x | 11 | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| | **Overall** | | | **73** | **73** | 73/73 = **100%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Anestesisjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4qYQ_tWb_QjD) |
| x | x | x | [Kalmar län, **region**](http://data.jobtechdev.se/taxonomy/concept/9QUH_2bb_6Np) |
| x | x | x | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| x | x | x | [Oskarshamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/tUP8_hRE_NcF) |
| | | **4** | 4/4 = **100%** |