# Results for '2aed92af507384817ecdd325075f821ea090630d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2aed92af507384817ecdd325075f821ea090630d](README.md) | 1 | 366 | 7 | 5 | 40/79 = **51%** | 2/8 = **25%** |

## Source text

Restaurangbiträde Sökes Warma West Shawarma öppnar ett unikt koncept i Marieberg Galleria I Örebro och söker just nu omgående Restaurangbiträde.  Arbetsuppgifterna kommer variera allt från beredning, servering, ta emot beställningar, kassa samt städning och disk.  Vi söker dig som är ung och driven i ditt arbete och bidrar med en god utveckling inom arbetsplatser.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Restaurangbiträde** Sökes Warma West Shawarma öpp... | x | x | 17 | 17 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 2 | ...oncept i Marieberg Galleria I **Örebro** och söker just nu omgående Re... | x | x | 6 | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 3 | ...ro och söker just nu omgående **Restaurangbiträde**.  Arbetsuppgifterna kommer va... | x | x | 17 | 17 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 4 | ... variera allt från beredning, **servering**, ta emot beställningar, kassa... | x |  |  | 9 | [Restaurang, servering, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GqXD_ArB_Mmg) |
| 5 | ...ering, ta emot beställningar, **kassa** samt städning och disk.  Vi s... |  | x |  | 5 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 5 | ...ering, ta emot beställningar, **kassa** samt städning och disk.  Vi s... | x |  |  | 5 | [sköta en kassaapparat, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tG6R_L5T_NTv) |
| 6 | ...mot beställningar, kassa samt **städning** och disk.  Vi söker dig som ä... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 6 | ...mot beställningar, kassa samt **städning** och disk.  Vi söker dig som ä... | x |  |  | 8 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 7 | ...ngar, kassa samt städning och **disk**.  Vi söker dig som är ung och... | x |  |  | 4 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| | **Overall** | | | **40** | **79** | 40/79 = **51%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x |  |  | [Restaurang, servering, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GqXD_ArB_Mmg) |
|  | x |  | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x | x | x | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| x |  |  | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| x |  |  | [sköta en kassaapparat, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tG6R_L5T_NTv) |
| x | x | x | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| | | **2** | 2/8 = **25%** |