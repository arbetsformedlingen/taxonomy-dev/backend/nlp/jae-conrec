# Results for 'd6243dfcb2fa8c8c503d188b6c665ccca4c5238e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d6243dfcb2fa8c8c503d188b6c665ccca4c5238e](README.md) | 1 | 3458 | 18 | 23 | 118/353 = **33%** | 9/21 = **43%** |

## Source text

Maskinförare/Miljötekniker till Gräfsåsens avfallsanläggning Enheten Avfall Avloppsanläggningar inom teknisk förvaltning Östersunds kommun driver och utvecklar avfalls- och avloppsanläggningar. Enheten omfattar 8 avloppsreningsverk, ett 40-tal pumpstationer, en uppgraderingsanläggning för fordonsgas och en avfallsanläggning. Enheten kontrollerar också statusen i ett antal deponier och jordtippar runt om i kommunen där verksamheten sedan tidigare upphört.  Östersunds Kommun har mer än 5 600 personer anställda i sju förvaltningar. Vi verkar inom 400 olika yrken och jobbar mot ett gemensamt mål: att göra medborgarnas liv rikare, enklare och tryggare.  Arbetsuppgifter  Som miljötekniker har du tillsammans med dina kollegor ansvar för det operativa arbetet vid Gräfsåsens avfallsanläggning. Gräfsåsen tar emot avfall från kommuner och verksamheter i hela länet. På anläggningen sker mottagning, sortering, mellanlagring, förbehandling och behandling av avfallet. Du får en roll kring avfallsanläggningens olika materialflöden och deltar i den operativa driften av avfallsanläggningen. Tillsammans ansvarar vi för att anläggningen uppfyller villkoren kopplade till anläggningens tillstånd och att aktuell lagstiftning efterlevs. Rollen innebär många kontaktytor och verksamheten bedrivs i nära samarbete med övriga delar på kommunen, tillsynsmyndigheten, anlitade entreprenörer, och våra kunder.   I arbetsuppgifterna ingår: .Mottagning, kontroll, omlastning, förbehandling och behandling av allt inkommet avfall .Anläggning och underhåll av samtliga hårdgjorda ytor samt planering av deponier .Tillverkning av tätskikt .Kort- och långsiktig planering av samtliga arbetsuppgifter på anläggningen .I förekommande fall drift av gasstation .Underhåll och daglig kontroll av maskinpark .Daglig kontroll av anläggningens funktioner utifrån gällande tillstånd och kommunens arbete med miljö, arbetsmiljö, kvalitet och säkerhet .I förekommande fall provtagning vatten och drift av pumpstation .Aktivt delta och hålla sig uppdaterad i säkerhetsarbetet .Avvikelserapportering .Vara delaktig vid utredningar  .Delta i de utbildningar som arbetsgivaren anser nödvändiga för att utföra arbetet .Underhåll av maskinpark .Daglig kontroll av anläggningen och maskinernas funktioner utifrån miljö och säkerhet .Ta hand om praktikanter och ferieanställda  Kvalifikationer  Vi söker dig som har miljöintresse och förmåga att arbeta självständigt under eget ansvar. Du är en självgående, strukturerad och anpassningsbar person. För att trivas hos oss ska du trivas med arbeten som ställer höga krav på flexibilitet och förmåga att planera och prioritera ditt arbete. Du har god förmåga att skapa relationer med kunder och leverantörer. Du är trygg i din roll och agerar serviceinriktat med stort kundfokus. Vi lägger stor vikt vid den personliga lämpligheten.  Det är meriterande om du har erfarenhet av svetsning och service av fordon. Du har god förmåga att uttrycka dig i tal och skrift på svenska.  B och C -körkort är ett krav samt förarbevis på hjullastare och grävmaskin.  Välkommen med din ansökan med CV och personligt brev senast 2022-08-19. Betyg/intyg behöver inte skickas med. Uppvisande av detta blir aktuellt vid eventuell intervju. Urval och intervjuer sker fortlöpande under ansökningstiden.  Inför rekryteringsarbetet har Östersunds kommun tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför kontakt med mediesäljare.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Maskinförare**/Miljötekniker till Gräfsåsens... | x | x | 12 | 12 | [Maskinförare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/UbR1_PxX_ntU) |
| 2 | Maskinförare/**Miljötekniker** till Gräfsåsens avfallsanlägg... |  | x |  | 13 | [Vatten- och miljötekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/5hEn_oVW_d3s) |
| 2 | Maskinförare/**Miljötekniker** till Gräfsåsens avfallsanlägg... |  | x |  | 13 | [Miljötekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/HVvy_Vhs_r1e) |
| 2 | Maskinförare/**Miljötekniker** till Gräfsåsens avfallsanlägg... | x | x | 13 | 13 | [miljötekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tWYC_CCV_VKh) |
| 3 | ...Miljötekniker till Gräfsåsens **avfallsanläggning** Enheten Avfall Avloppsanläggn... |  | x |  | 17 | [inspektera avfallsanläggningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WDWm_CvT_TrL) |
| 4 | ...ens avfallsanläggning Enheten **Avfall Avloppsanläg**gningar inom teknisk förvaltni... |  | x |  | 19 | [inspektera avfallsanläggningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WDWm_CvT_TrL) |
| 5 | ...ngar inom teknisk förvaltning **Östersunds kommun** driver och utvecklar avfalls-... | x |  |  | 17 | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| 6 | ...äggning för fordonsgas och en **avfallsanläggning**. Enheten kontrollerar också s... |  | x |  | 17 | [inspektera avfallsanläggningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WDWm_CvT_TrL) |
| 7 | ...eten sedan tidigare upphört.  **Östersunds Kommun** har mer än 5 600 personer ans... | x |  |  | 17 | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| 8 | ...re.  Arbetsuppgifter  Som **miljötekniker** har du tillsammans med dina k... |  | x |  | 13 | [Vatten- och miljötekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/5hEn_oVW_d3s) |
| 8 | ...re.  Arbetsuppgifter  Som **miljötekniker** har du tillsammans med dina k... |  | x |  | 13 | [Miljötekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/HVvy_Vhs_r1e) |
| 8 | ...re.  Arbetsuppgifter  Som **miljötekniker** har du tillsammans med dina k... | x | x | 13 | 13 | [miljötekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tWYC_CCV_VKh) |
| 9 | ...rativa arbetet vid Gräfsåsens **avfallsanläggning**. Gräfsåsen tar emot avfall fr... |  | x |  | 17 | [inspektera avfallsanläggningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WDWm_CvT_TrL) |
| 10 | ...nläggning. Gräfsåsen tar emot **avfall** från kommuner och verksamhete... | x |  |  | 6 | [Avfall, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Kx4Q_7TA_MuR) |
| 11 | ...a länet. På anläggningen sker **mottagning**, sortering, mellanlagring, fö... | x | x | 10 | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 12 | ...anläggningen sker mottagning, **sortering**, mellanlagring, förbehandling... |  | x |  | 9 | [sortering av fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/CKTg_SGv_vEB) |
| 12 | ...anläggningen sker mottagning, **sortering**, mellanlagring, förbehandling... |  | x |  | 9 | [traditioner för sortering av djurdelar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DrvX_LVa_Qzf) |
| 12 | ...anläggningen sker mottagning, **sortering**, mellanlagring, förbehandling... |  | x |  | 9 | [sköta sortering och uppläggning av virke, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/djQw_ZkM_xEH) |
| 12 | ...anläggningen sker mottagning, **sortering**, mellanlagring, förbehandling... | x |  |  | 9 | [sortera avfall, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r7jH_qjm_NF5) |
| 13 | ...  I arbetsuppgifterna ingår: .**Mottagning**, kontroll, omlastning, förbeh... | x | x | 10 | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 14 | ...h behandling av allt inkommet **avfall** .Anläggning och underhåll av ... | x |  |  | 6 | [Avfall, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Kx4Q_7TA_MuR) |
| 15 | ...r samt planering av deponier .**Tillverkning** av tätskikt .Kort- och långsi... | x | x | 12 | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 16 | ...h kommunens arbete med miljö, **arbetsmiljö**, kvalitet och säkerhet .I för... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 17 | ...miljöintresse och förmåga att **arbeta självständigt** under eget ansvar. Du är en s... |  | x |  | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 18 | ...rande om du har erfarenhet av **svetsning** och service av fordon. Du har... | x | x | 9 | 9 | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| 19 | ...rycka dig i tal och skrift på **svenska**.  B och C -körkort är ett kra... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 20 | ...i tal och skrift på svenska.  **B** och C -körkort är ett krav sa... | x |  |  | 1 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 21 | ...och skrift på svenska.  B och **C -körkort** är ett krav samt förarbevis p... | x |  |  | 10 | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
| 22 | ...r ett krav samt förarbevis på **hjullastare** och grävmaskin.  Välkommen me... | x | x | 11 | 11 | [Hjullastare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/WUHH_Jpr_Zeh) |
| 23 | ...förarbevis på hjullastare och **grävmaskin**.  Välkommen med din ansökan m... | x | x | 10 | 10 | [Grävmaskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/swHZ_UXk_UaE) |
| | **Overall** | | | **118** | **353** | 118/353 = **33%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Vatten- och miljötekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/5hEn_oVW_d3s) |
| x |  |  | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
|  | x |  | [sortering av fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/CKTg_SGv_vEB) |
|  | x |  | [traditioner för sortering av djurdelar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DrvX_LVa_Qzf) |
| x | x | x | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
|  | x |  | [Miljötekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/HVvy_Vhs_r1e) |
| x |  |  | [Avfall, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Kx4Q_7TA_MuR) |
|  | x |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| x | x | x | [Maskinförare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/UbR1_PxX_ntU) |
| x |  |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
|  | x |  | [inspektera avfallsanläggningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WDWm_CvT_TrL) |
| x | x | x | [Hjullastare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/WUHH_Jpr_Zeh) |
|  | x |  | [sköta sortering och uppläggning av virke, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/djQw_ZkM_xEH) |
| x |  |  | [sortera avfall, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r7jH_qjm_NF5) |
| x | x | x | [Grävmaskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/swHZ_UXk_UaE) |
| x | x | x | [miljötekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tWYC_CCV_VKh) |
| x | x | x | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **9** | 9/21 = **43%** |