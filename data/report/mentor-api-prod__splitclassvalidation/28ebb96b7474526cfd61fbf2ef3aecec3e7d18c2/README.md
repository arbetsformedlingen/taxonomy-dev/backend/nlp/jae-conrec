# Results for '28ebb96b7474526cfd61fbf2ef3aecec3e7d18c2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [28ebb96b7474526cfd61fbf2ef3aecec3e7d18c2](README.md) | 1 | 1000 | 7 | 6 | 24/85 = **28%** | 1/5 = **20%** |

## Source text

Frisör sökes! Beskrivning av tjänsten: Swed Com söker dig som vill arbeta som frisör i Sundbyberg. Som frisör är det viktigt att du leverar bra service och kundsbemötande. Du ska ha tidigare erfarenhet av att arbeta som frisör samt utföra effektiv och kvalitetsäker hårklippningar. Som person är det viktigt att du brinner för yrket samt är motiverad för nya utmaningar och lärande.  Anställning, arbetstider och lön: Anställningen börjar med en provanställning med möjlighet till en tillsvidareanställning. Arbetstiden är förlagd, mån-fre mellan kl. 10:00-19:00  Ansökningsalternativ: Du är mer än välkommen att skicka ditt CV och personliga brev till oss via mejl: info@swedcom.se Markera gärna din ansökan med "Frisör Stockholm". Hjärtligt välkomna med era ansökningar.  Observera: Läs gärna noga annonsen innan ni skickar mejl eller ringer.    Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Frisör** sökes! Beskrivning av tjänste... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 2 | ...söker dig som vill arbeta som **frisör** i Sundbyberg. Som frisör är d... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 3 | ... som vill arbeta som frisör i **Sundbyberg**. Som frisör är det viktigt at... | x |  |  | 10 | [Sundbyberg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UTJZ_zHH_mJm) |
| 4 | ... som frisör i Sundbyberg. Som **frisör** är det viktigt att du leverar... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 5 | ... erfarenhet av att arbeta som **frisör** samt utföra effektiv och kval... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 6 | ...ra effektiv och kvalitetsäker **hårklippningar**. Som person är det viktigt at... | x |  |  | 14 | [hårklippningstekniker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ctoB_kAr_kEo) |
| 7 | ...ällning med möjlighet till en **tillsvidareanställning**. Arbetstiden är förlagd, mån-... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 8 | ...arkera gärna din ansökan med "**Frisör** Stockholm". Hjärtligt välkomn... |  | x |  | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 9 | ...gärna din ansökan med "Frisör **Stockholm**". Hjärtligt välkomna med era ... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| | **Overall** | | | **24** | **85** | 24/85 = **28%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Sundbyberg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UTJZ_zHH_mJm) |
| x |  |  | [hårklippningstekniker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ctoB_kAr_kEo) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| | | **1** | 1/5 = **20%** |