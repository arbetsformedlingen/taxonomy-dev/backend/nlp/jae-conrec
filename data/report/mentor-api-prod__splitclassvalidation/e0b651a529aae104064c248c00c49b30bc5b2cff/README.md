# Results for 'e0b651a529aae104064c248c00c49b30bc5b2cff'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e0b651a529aae104064c248c00c49b30bc5b2cff](README.md) | 1 | 891 | 6 | 6 | 34/97 = **35%** | 3/8 = **38%** |

## Source text

Servitris/Servitör Nu söker vi fler medarbetare för heltid/deltid samt för extra vid behov. Är du intresserad av och kunnig inom mat och dryck? Och vill leverera en minnesvärd upplevelse för våra gäster? Skicka in er ansökan till oss.  Vi söker dig som är kunnig, engagerad och en lagspelare, som har passion för service, mat och dryck. Huvudsakliga arbetsuppgifter servering av a la carte och gruppmiddagar under lunch och middag. förberedelser och återställande utav restaurangen. Dryckeskunskap är meriterande. Vi kan erbjuda en tillsvidareanställning eller deltidstjänst samt extra. Arbetstiderna är varierande, både i tid, dagar, kvällar och helger. Krav: körkort och svenska i tal och skrift. Är du intresserad? Tveka inte att höra av dig till oss! Skicka in din ansökan redan idag. Rekryteringsarbetet sker löpande under ansökningstiden.  Varmt välkommen med din ansökan!  #jobbjustnu

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Servitris**/Servitör Nu söker vi fler med... | x | x | 9 | 9 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | Servitris**/**Servitör Nu söker vi fler meda... | x |  |  | 1 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 3 | Servitris/**Servitör** Nu söker vi fler medarbetare ... | x | x | 8 | 8 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 4 | ...söker vi fler medarbetare för **heltid/deltid** samt för extra vid behov. Är ... | x |  |  | 13 | (not found in taxonomy) |
| 5 | ... arbetsuppgifter servering av **a la carte** och gruppmiddagar under lunch... | x | x | 10 | 10 | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| 6 | ...eriterande. Vi kan erbjuda en **tillsvidareanställning** eller deltidstjänst samt extr... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 7 | ... tillsvidareanställning eller **deltidstjänst** samt extra. Arbetstiderna är ... |  | x |  | 13 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 8 | ...ar, kvällar och helger. Krav: **körkort** och svenska i tal och skrift.... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 8 | ...ar, kvällar och helger. Krav: **körkort** och svenska i tal och skrift.... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 9 | ...och helger. Krav: körkort och **svenska** i tal och skrift. Är du intre... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **34** | **97** | 34/97 = **35%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | (not found in taxonomy) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/8 = **38%** |