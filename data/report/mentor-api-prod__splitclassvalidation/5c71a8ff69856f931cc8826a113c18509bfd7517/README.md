# Results for '5c71a8ff69856f931cc8826a113c18509bfd7517'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [5c71a8ff69856f931cc8826a113c18509bfd7517](README.md) | 1 | 2290 | 22 | 9 | 45/354 = **13%** | 4/16 = **25%** |

## Source text

Drivs du av att hålla ihop och driva projekt?    Som IT-projektledare ser du både detaljer och helhet, tar ansvar lika mycket som du är lyhörd och lyssnar. Din inre trygghet och ditt stora IT-intresse ger dig rätt förutsättningar.    SNABBFAKTA  Examen Yrkeshögskoleexamen  Start 31 augusti 2021 (preliminärt)  Längd 320 YH-poäng (ca 1,5 år)  Studietakt 100%  Studieort Mölndal  Antal platser 20 st  LIA 16 veckor (praktik)    UNDER STUDIETIDEN  Som studerande på Yrkeshögskolan Campus Mölndal förbereder vi dig för arbetsmarknaden. Vi gör detta genom att lägga fokus på din bakgrund, dina intressen och dina drivkrafter. Utbildningen lägger fokus på gruppen och personlig utveckling då yrkesrollen bland annat kräver kompetens för att leda andra individer och grupper.    Utbildningens introduktion ger dig möjlighet att landa i din nya verklighet. För att du ska känna dig välkommen får du här träffa utbildningsledare, lärare, övrig viktig personal och ledningsgruppsrepresentanter. Vi lägger stor vikt vid att säkerställa att du får det stöd du behöver under utbildningen för att nå utbildningens mål.    Vi ser ditt inflytande som studerande som en grundsten i våra utbildningar. Utan de studerandes inflytande och återkoppling kan vi inte säkerställa utbildningens kvalitet. Genom ett kontinuerligt samarbete med utbildningens ledningsgrupp ger vi både arbetsliv och studerande möjligheten att bygga relationer och nätverk.    Idag finns IT mer och mer so men del av alla verksamheter, det är inte längre en egen bransch. Med detta sagt är IT-branschen fortfarande stor och möjligtvis den bransch där utvecklingen sker i snabbast takt. I utbildningen förbereder vi dig för en föränderlig arbetsmarknad och yrkesroll. Vårt mål är att du ska ha med dig de verktyg som branschen kräver för att du ska klara av dessa förändringar efter att du tagit din examen.    KURSER SOM DU LÄSER (YH-poäng*):  *5 YH-poäng motsvarar 1 veckas helstudier    Grupputveckling, 20 Yh-poäng  Projektmetodik, 40 Yh-poäng  Teknisk grundkurs, 20 Yh-poäng  Ledarskap, 25 Yh-poäng  Affärsmannaskap, 25 Yh-poäng  Teknisk fördjupning, 25 Yh-poäng  IT Service Management och ITIL, 15 Yh-poäng  Förändringsledning, 20 Yh-poäng  Agila arbetssätt, 20 Yh-poäng  Lärande i arbete, 80 Yh-poäng  Examensarbete, 30 Yh-poäng

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...hop och driva projekt?    Som **IT-projektledare** ser du både detaljer och helh... | x | x | 16 | 16 | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
| 2 | ...rutsättningar.    SNABBFAKTA  **Examen** Yrkeshögskoleexamen  Start 31... | x |  |  | 6 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 3 | ...ningar.    SNABBFAKTA  Examen **Yrkeshögskoleexamen**  Start 31 augusti 2021 (preli... | x |  |  | 19 | [Högskoleutbildning, yrkesinriktad, kortare än 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/oroB_8GN_Fe5) |
| 4 | ...)  Studietakt 100%  Studieort **Mölndal**  Antal platser 20 st  LIA 16 ... | x | x | 7 | 7 | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
| 5 | ...ande på Yrkeshögskolan Campus **Mölndal** förbereder vi dig för arbetsm... | x |  |  | 7 | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
| 6 | ...n lägger fokus på gruppen och **personlig utveckling** då yrkesrollen bland annat kr... | x | x | 20 | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 7 | ...nnat kräver kompetens för att **leda andra individer och grupper**.    Utbildningens introduktio... | x |  |  | 32 | [utöva ledarskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DP6Z_cdB_jKo) |
| 8 | ...ens ledningsgrupp ger vi både **arbetsliv** och studerande möjligheten at... |  | x |  | 9 | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| 9 | ...ch studerande möjligheten att **bygga relationer och nätverk**.    Idag finns IT mer och mer... | x |  |  | 28 | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| 10 | ...er och nätverk.    Idag finns **IT** mer och mer so men del av all... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 11 | ...en bransch. Med detta sagt är **IT**-branschen fortfarande stor oc... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 12 | ... bransch. Med detta sagt är IT**-branschen** fortfarande stor och möjligtv... |  | x |  | 10 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 13 | ...ringar efter att du tagit din **examen**.    KURSER SOM DU LÄSER (YH-p... | x |  |  | 6 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 14 | ...svarar 1 veckas helstudier    **Grupputveckling**, 20 Yh-poäng  Projektmetodik,... | x |  |  | 15 | [Personlig utveckling, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/finP_hST_DPd) |
| 15 | ...Grupputveckling, 20 Yh-poäng  **Projektmetodik**, 40 Yh-poäng  Teknisk grundku... | x |  |  | 14 | [Annan utbildning i ledning och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/CGF1_o4T_wCo) |
| 16 | ... Projektmetodik, 40 Yh-poäng  **Teknisk grundkurs**, 20 Yh-poäng  Ledarskap, 25 Y... | x |  |  | 17 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 17 | ...knisk grundkurs, 20 Yh-poäng  **Ledarskap**, 25 Yh-poäng  Affärsmannaskap... | x |  |  | 9 | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
| 18 | ...oäng  Ledarskap, 25 Yh-poäng  **Affärsmannaskap**, 25 Yh-poäng  Teknisk fördjup... | x |  |  | 15 | [Annan utbildning i ledning och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/CGF1_o4T_wCo) |
| 18 | ...oäng  Ledarskap, 25 Yh-poäng  **Affärsmannaskap**, 25 Yh-poäng  Teknisk fördjup... |  | x |  | 15 | [Affärsmannaskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zgV5_X3h_GgS) |
| 19 | ...Affärsmannaskap, 25 Yh-poäng  **Teknisk fördjupning**, 25 Yh-poäng  IT Service Mana... | x |  |  | 19 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 20 | ...isk fördjupning, 25 Yh-poäng  **IT Service Management och ITIL**, 15 Yh-poäng  Förändringsledn... | x |  |  | 30 | [Annan utbildning i ledning och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/CGF1_o4T_wCo) |
| 21 | ...ng  IT Service Management och **ITIL**, 15 Yh-poäng  Förändringsledn... |  | x |  | 4 | [arbeta i en ITIL-baserad miljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZJS4_Han_D8x) |
| 22 | ...gement och ITIL, 15 Yh-poäng  **Förändringsledning**, 20 Yh-poäng  Agila arbetssät... | x |  |  | 18 | [Annan utbildning i ledning och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/CGF1_o4T_wCo) |
| 22 | ...gement och ITIL, 15 Yh-poäng  **Förändringsledning**, 20 Yh-poäng  Agila arbetssät... |  | x |  | 18 | [Förändringsledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LLQd_jDh_gdd) |
| 23 | ...ändringsledning, 20 Yh-poäng  **Agila arbetssätt**, 20 Yh-poäng  Lärande i arbet... | x |  |  | 16 | [Annan utbildning i ledning och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/CGF1_o4T_wCo) |
| | **Overall** | | | **45** | **354** | 45/354 = **13%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
| x |  |  | [Annan utbildning i ledning och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/CGF1_o4T_wCo) |
| x |  |  | [utöva ledarskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DP6Z_cdB_jKo) |
| x |  |  | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| x |  |  | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
|  | x |  | [Förändringsledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LLQd_jDh_gdd) |
| x |  |  | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
|  | x |  | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
|  | x |  | [arbeta i en ITIL-baserad miljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZJS4_Han_D8x) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Personlig utveckling, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/finP_hST_DPd) |
| x | x | x | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
| x |  |  | [Högskoleutbildning, yrkesinriktad, kortare än 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/oroB_8GN_Fe5) |
| x | x | x | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| x | x | x | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
|  | x |  | [Affärsmannaskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zgV5_X3h_GgS) |
| | | **4** | 4/16 = **25%** |