# Results for '57361d3d0641c59e2bd3827b83fe8d6c51b77282'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [57361d3d0641c59e2bd3827b83fe8d6c51b77282](README.md) | 1 | 215 | 3 | 3 | 18/38 = **47%** | 2/3 = **67%** |

## Source text

Bartenders till Bonfire Festival Vi söker bartender till Bonfire Festival 2022, 9-10 september, som kan arbeta i ett högt tempo med serviceinriktning. Arbetet består i att sälja mat och dryck. Kassavana är ett krav.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bartenders** till Bonfire Festival Vi söke... | x |  |  | 10 | [Bartender, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fLps_qRU_1CC) |
| 1 | **Bartenders** till Bonfire Festival Vi söke... |  | x |  | 10 | [bartender, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tr5M_uUa_wdz) |
| 2 | ...ill Bonfire Festival Vi söker **bartender** till Bonfire Festival 2022, 9... | x | x | 9 | 9 | [bartender, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tr5M_uUa_wdz) |
| 3 | ...år i att sälja mat och dryck. **Kassavana** är ett krav. | x | x | 9 | 9 | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| | **Overall** | | | **18** | **38** | 18/38 = **47%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Bartender, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fLps_qRU_1CC) |
| x | x | x | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| x | x | x | [bartender, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tr5M_uUa_wdz) |
| | | **2** | 2/3 = **67%** |