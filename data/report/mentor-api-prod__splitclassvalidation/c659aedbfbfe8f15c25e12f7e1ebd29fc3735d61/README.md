# Results for 'c659aedbfbfe8f15c25e12f7e1ebd29fc3735d61'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c659aedbfbfe8f15c25e12f7e1ebd29fc3735d61](README.md) | 1 | 3576 | 12 | 14 | 27/281 = **10%** | 3/17 = **18%** |

## Source text

Sömnadstekniker Delar du vår passion om en aktiv livsstil? Brinner du för sömnad och textil? Nu finns möjligheten för dig som vill arbeta i ett härligt team och vara med i utvecklingen av högkvalitativa produkter som inspirerar till att leva ett aktivt liv.  Vi söker just nu efter vår nästa Sömnadstekniker till vårt Globala utvecklingscenter i Hillerstorp.   Som sömnadstekniker ansvarar du för att producera mindre förserier utifrån 2D-mönster. Du kommer stödja utvecklingsprojekten med dina kunskaper inom sömnad och textil produktion. Utöver detta kommer du även vara med och planera materialbeställningar till sömnadsverkstaden utifrån projektens behov. I rollen som sömnadstekniker kommer du arbeta i nära samarbete med vår utvecklingsavdelning och framförallt dina kollegor på textilavdelningen.     Vi har investerat i en ny modern sömnadsverkstad med flertalet industrisymaskiner och tillskärningsmaskin. Vårt textila team består idag av soft goods leads, materialutvecklare, mönsterkonstruktörer och sömnadstekniker.  Din profil För att lyckas i din nya roll tror vi att du har minst fem års relevant erfarenhet av att sy och skapa prototyper i textila material. Du har minst en avklarad gymnasieutbildning samt goda kunskaper i engelska, både i tal och skrift. Utöver detta har du goda kunskaper om mönsterkonstruktion, olika sömnadstekniker och kan hantera diverse industrisymaskiner. Har du erfarenhet av att själv skapa mönster är det en merit (vi jobbar i Gerber Accumark).  För att trivas i din nya organisation behöver du vara en proaktiv och praktisk person som trivs i en miljö präglad av snabba beslut och teamwork. Du är vidare en innovativ person som ständigt strävar efter förbättringar. Vi tror på starkt lagarbete, både inom den enskilda arbetsgruppen men också genom hela företaget. Vi strävar efter att vara en öppen och nyfiken organisation, där vi delar vår kunskap och där vi inspirerar varandra. Inom Thule Group hittar du människor som har en passion för de produkter vi gör och för det outdoor företag vi är. Vi delar samma värderingar och vi gillar att ha kul. Alla våra medarbetare har ett gemensamt ansvar för att upprätthålla denna anda och bidra till den på bästa sätt.   Ansökan Tjänsten är placerad i Hillerstorp och rapporterar till vår Manager Soft Goods Development Region Europe, Johanna Malmklint. Om du finner denna utmaning intressant vill vi ha din ansökan senast den 14 augusti. Du ansöker via vår hemsida www.thulegroup.com/open-positions med CV och personligt brev. Vi ser fram emot att höra av dig!  Om Thule Group   Thule Group är ett globalt sport- och fritidsföretag. Vi erbjuder produkter av hög kvalitet, med smarta funktioner och hållbar design som gör det lätt för människor världen över att leva ett aktivt liv. Under mottot Active Life, Simplified. – och med ett fokus på konsumentdriven innovation och ett långsiktigt hållbarhetsperspektiv – utvecklar,  tillverkar och marknadsför vi produkter inom produktkategorierna Sport&amp;Cargo Carriers (takräcken, takboxar, hållare för cykel-, vatten- och vintersporter, samt taktält för montering på bilen), Active with Kids (barnvagnar, cykelvagnar och cykelbarnstolar), RV Products (markiser, cykelhållare och tält för husbilar och husvagnar) och Packs, Bags &amp; Luggage (vandringsryggsäckar, resväskor och kameraväskor).   Thule Group har cirka 3.300 anställda vid 9 produktionsanläggningar och 35 försäljningskontor över hela världen. Produkterna säljs på 140 marknader och försäljningen under 2021 uppgick till 10,4 miljarder kronor.   http://www.thulegroup.com

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ktiv livsstil? Brinner du för **sömnad** och textil? Nu finns möjlighe... |  | x |  | 6 | [Sömnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/2H6W_q67_ygJ) |
| 2 | ...ekten med dina kunskaper inom **sömnad** och textil produktion. Utöver... |  | x |  | 6 | [Sömnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/2H6W_q67_ygJ) |
| 3 | ...tår idag av soft goods leads, **materialutvecklare**, mönsterkonstruktörer och söm... |  | x |  | 18 | [Materialutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/aKJk_JJF_anz) |
| 4 | ...ds leads, materialutvecklare, **mönsterkonstruktörer** och sömnadstekniker.  Din pro... |  | x |  | 20 | [Direktris/Garment technician/Mönsterkonstruktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/M9iy_1ua_7UK) |
| 5 | ...n nya roll tror vi att du har **minst fem års relevant erfarenhet** av att sy och skapa prototype... | x |  |  | 33 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 6 | ...rs relevant erfarenhet av att **sy** och skapa prototyper i textil... | x |  |  | 2 | [Sömnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/2H6W_q67_ygJ) |
| 7 | ...vant erfarenhet av att sy och **skapa prototyper** i textila material. Du har mi... | x |  |  | 16 | [designa prototyper, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qX8a_Lyo_wbV) |
| 8 | ...ial. Du har minst en avklarad **gymnasieutbildning** samt goda kunskaper i engelsk... | x |  |  | 18 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 8 | ...ial. Du har minst en avklarad **gymnasieutbildning** samt goda kunskaper i engelsk... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 9 | ...ildning samt goda kunskaper i **engelska**, både i tal och skrift. Utöve... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 10 | ...etta har du goda kunskaper om **mönsterkonstruktion**, olika sömnadstekniker och ka... | x | x | 19 | 19 | [Mönsterkonstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/haMy_QRx_7vg) |
| 11 | ...om mönsterkonstruktion, olika **sömnadstekniker** och kan hantera diverse indus... | x |  |  | 15 | [Sömnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/2H6W_q67_ygJ) |
| 12 | ...niker och kan hantera diverse **industrisymaskiner**. Har du erfarenhet av att sjä... | x |  |  | 18 | [Symaskiner, **skill**](http://data.jobtechdev.se/taxonomy/concept/M2cJ_Jwf_ute) |
| 13 | ...ar du erfarenhet av att själv **skapa mönster** är det en merit (vi jobbar i ... | x |  |  | 13 | [Mönsterkonstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/haMy_QRx_7vg) |
| 14 | ... är det en merit (vi jobbar i **Gerber Accumark**).  För att trivas i din nya o... | x |  |  | 15 | [Mönsterkonstruktion-Gerber, **skill**](http://data.jobtechdev.se/taxonomy/concept/D9SR_j4J_K9u) |
| 15 | ...torp och rapporterar till vår **Manager** Soft Goods Development Region... |  | x |  | 7 | [Google Analytics, **skill**](http://data.jobtechdev.se/taxonomy/concept/oCKN_G9k_zTj) |
| 16 | ...kter inom produktkategorierna **Sport**&amp;Cargo Carriers (takräcken... |  | x |  | 5 | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
| 17 | ... på bilen), Active with Kids (**barnvagnar**, cykelvagnar och cykelbarnsto... |  | x |  | 10 | [Barnvagnar/Bilbarnstolar, **skill**](http://data.jobtechdev.se/taxonomy/concept/1Bou_FhG_MdL) |
| 18 | ...er, cykelhållare och tält för **husbilar** och husvagnar) och Packs, Bag... |  | x |  | 8 | [Handel med husvagnar, husbilar, släpfordon och påhängsvagnar, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/tdTY_qM8_Zux) |
| 18 | ...er, cykelhållare och tält för **husbilar** och husvagnar) och Packs, Bag... |  | x |  | 8 | [Husbilar, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/wzAA_SXS_jz7) |
| 19 | ...are och tält för husbilar och **husvagnar**) och Packs, Bags &amp; Luggag... |  | x |  | 9 | [Husvagnar, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/53Uq_j5u_vme) |
| 19 | ...are och tält för husbilar och **husvagnar**) och Packs, Bags &amp; Luggag... |  | x |  | 9 | [Handel med husvagnar, husbilar, släpfordon och påhängsvagnar, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/tdTY_qM8_Zux) |
| | **Overall** | | | **27** | **281** | 27/281 = **10%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Barnvagnar/Bilbarnstolar, **skill**](http://data.jobtechdev.se/taxonomy/concept/1Bou_FhG_MdL) |
| x | x | x | [Sömnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/2H6W_q67_ygJ) |
|  | x |  | [Husvagnar, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/53Uq_j5u_vme) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| x |  |  | [Mönsterkonstruktion-Gerber, **skill**](http://data.jobtechdev.se/taxonomy/concept/D9SR_j4J_K9u) |
| x |  |  | [Symaskiner, **skill**](http://data.jobtechdev.se/taxonomy/concept/M2cJ_Jwf_ute) |
|  | x |  | [Direktris/Garment technician/Mönsterkonstruktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/M9iy_1ua_7UK) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Materialutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/aKJk_JJF_anz) |
| x | x | x | [Mönsterkonstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/haMy_QRx_7vg) |
|  | x |  | [Google Analytics, **skill**](http://data.jobtechdev.se/taxonomy/concept/oCKN_G9k_zTj) |
|  | x |  | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
| x |  |  | [designa prototyper, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qX8a_Lyo_wbV) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
|  | x |  | [Handel med husvagnar, husbilar, släpfordon och påhängsvagnar, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/tdTY_qM8_Zux) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
|  | x |  | [Husbilar, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/wzAA_SXS_jz7) |
| | | **3** | 3/17 = **18%** |