# Results for '160d8342af45cdb1484e274875acb1484548dbfc'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [160d8342af45cdb1484e274875acb1484548dbfc](README.md) | 1 | 831 | 10 | 7 | 57/85 = **67%** | 6/7 = **86%** |

## Source text

Receptionist extra till Hotel Bellora Älskar du kundkontakt, många frågor och många bollar i luften?  Det är vad vi gör varje dag och natt i receptionen på hotell Bellora. Vi ligger mitt på Avenyn och vi jobbar nära vår restaurang som har fullt varje dag och natt. Vi älskar att ge våra kunder det lilla extra och hjälpa till med vad som behövs.  Vi söker extra personal med start omgående, 30-50%.  Vi ser gärna att du har arbetat med service i reception eller liknande innan, är minst 18 år och är bra på att sätta dig in i system snabbt. Vi arbetar med Protel så det är en fördel om du har arbetat i det innan, om du inte har det - så lär vi dig.  Vi pratar engelska och svenska i receptionen. Arbetstiderna är varierande 06:45-14:45, 10:00-18:00 eller 14:00-22:00.  Intresserad? Perfekt, svara på frågorna och sänd med ditt CV.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Receptionist** extra till Hotel Bellora Älsk... | x | x | 12 | 12 | [Receptionist, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gAAz_Lo5_WRR) |
| 2 | Receptionist extra till **Hotel** Bellora Älskar du kundkontakt... | x | x | 5 | 5 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 3 | ...d vi gör varje dag och natt i **receptionen** på hotell Bellora. Vi ligger ... | x |  |  | 11 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 4 | ...dag och natt i receptionen på **hotell** Bellora. Vi ligger mitt på Av... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 5 | ...Avenyn och vi jobbar nära vår **restaurang** som har fullt varje dag och n... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 6 | ... personal med start omgående, **30-50%**.  Vi ser gärna att du har arb... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 7 | ... du har arbetat med service i **reception** eller liknande innan, är mins... | x | x | 9 | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 8 | ...t - så lär vi dig.  Vi pratar **engelska** och svenska i receptionen. Ar... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 9 | ... dig.  Vi pratar engelska och **svenska** i receptionen. Arbetstiderna ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 10 | ...pratar engelska och svenska i **receptionen**. Arbetstiderna är varierande ... | x |  |  | 11 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| | **Overall** | | | **57** | **85** | 57/85 = **67%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Receptionist, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gAAz_Lo5_WRR) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/7 = **86%** |