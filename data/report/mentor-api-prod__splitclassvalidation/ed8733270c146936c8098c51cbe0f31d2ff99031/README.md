# Results for 'ed8733270c146936c8098c51cbe0f31d2ff99031'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ed8733270c146936c8098c51cbe0f31d2ff99031](README.md) | 1 | 266 | 2 | 4 | 27/49 = **55%** | 2/4 = **50%** |

## Source text

Förbereder för en anställning inom vården som servicevärd. Syftet är att underlätta för vårdpersonalen genom att servicepersonal tar hand om transporter av patienter, köksuppgifter, matservering, städning av patientrummen och sängarna samt förrådshantering med mera.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...n anställning inom vården som **servicevärd**. Syftet är att underlätta för... | x | x | 11 | 11 | [Servicevärd, **job-title**](http://data.jobtechdev.se/taxonomy/concept/YxVc_fg8_5MK) |
| 2 | ... Syftet är att underlätta för **vårdpersonalen** genom att servicepersonal tar... |  | x |  | 14 | [Övrig vård- och omsorgspersonal, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/GiNX_ESA_AL1) |
| 3 | ... köksuppgifter, matservering, **städning** av patientrummen och sängarna... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 4 | ...tientrummen och sängarna samt **förrådshantering** med mera. | x | x | 16 | 16 | [Förrådshantering, **skill**](http://data.jobtechdev.se/taxonomy/concept/vvPg_scR_Pp6) |
| | **Overall** | | | **27** | **49** | 27/49 = **55%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
|  | x |  | [Övrig vård- och omsorgspersonal, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/GiNX_ESA_AL1) |
| x | x | x | [Servicevärd, **job-title**](http://data.jobtechdev.se/taxonomy/concept/YxVc_fg8_5MK) |
| x | x | x | [Förrådshantering, **skill**](http://data.jobtechdev.se/taxonomy/concept/vvPg_scR_Pp6) |
| | | **2** | 2/4 = **50%** |