# Results for '5783c9dc622bad380151f4e18b44d6643668d527'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [5783c9dc622bad380151f4e18b44d6643668d527](README.md) | 1 | 3049 | 14 | 19 | 121/288 = **42%** | 6/17 = **35%** |

## Source text

Läkemedelstekniker, Kungsholmen Arbetsbeskrivning Vi söker dig som vill arbeta som läkemedelstekniker! Vår kund söker nu fler medarbetare till sin produktion mitt på Kungsholmen. Har du en avslutad gymnasieutbildning med inriktning mot kemi/processteknik eller erfarenhet av arbete med cellodling i stor skala ska du söka den här tjänsten.   Uppdraget är på heltid, måndag till fredag och arbetstiden är förlagd till 2-skift morgon/kväll. Arbete på röda dagar och helg förekommer på ett rullande schema. Start 1 september  Som konsult hos Randstad är du anställd hos oss och jobbar ute hos någon av våra kunder. Du har samma fördelar hos Randstad som hos andra arbetsgivare med kollektivavtal och förmåner som friskvårdsbidrag, företagshälsovård, försäkringar och rabatt på träningskort. Tjänsten kan innebära många kontakter och då är det är viktigt att du har en bra samarbetsförmåga. På Randstad strävar vi efter att erbjuda dig en mängd karriärmöjligheter, så att du kan utveckla din kompetens och få ett välfyllt CV. Söker du en arbetsgivare som erbjuder varierande uppdrag och nya kontaktnät kommer du att trivas hos oss.  Ansvarsområden Du deltar i det operativa produktionsarbetet och dokumenterar ditt arbete i batchprotokoll och loggböcker.   En vanlig dag på jobbet består bland annat av att du,    Bereder och filtrerar odlingsmedium Förbereder, ångsteriliserar och rengör bioreaktorer och odlingsutrustning Provtar och analyserar odlingsprover Ställer in styrparametrar i odlingsutrustning enligt instruktioner Bevakar odlingsbatcher och vidtar åtgärder i samråd med arbetsledare Avbokar och inventerar material på sektionens lagerplatser Utför kontroller, kalibreringar, service och underhållsarbete av mindre utrustning och instrument inom sektionen    Kvalifikationer För att vara kvalificerad för den här tjänsten behöver du ha en gymnasieutbildning med inriktning kemi/processteknik alternativt en lång erfarenhet av cellodling i stor skala   För oss är det viktigt att all kompetens på arbetsmarknaden tillvaratas. Vi välkomnar alla sökande och eftersträvar mångfald.  Erfarenhet Erfarenhet av arbete i enlighet med GMP är starkt meriterande  Ansökan Välkommen med din ansökan senast 2022-08-31, urval och intervjuer kommer ske löpande och tjänsterna kan komma bli tillsatta innan sista datum för ansökan.   Obs, vi hanterar inga ansökningar eller cv via mail.  Om företaget Med över 600 000 anställda i omkring 40 länder är Randstad världsledande inom HR-tjänster och erbjuder bemannings-, konsult- och rekryteringslösningar inom alla kompetensområden. Vi erbjuder även interim management, executive search och omställningstjänster. Vi har ett stort nätverk av bolag och kandidater vilket innebär att vi förmedlar hundratals jobb inom olika branscher, från Kiruna i norr till Malmö i söder. Vår ambition är att vara den bästa arbetsgivaren på marknaden. Genom att kombinera vår passion för människor med kraften i dagens teknologi hjälper vi människor och organisationer att nå deras sanna potential. Vi kallar det Human Forward.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Läkemedelstekniker**, Kungsholmen Arbetsbeskrivnin... | x | x | 18 | 18 | [Läkemedelstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/WGEx_rgb_Czn) |
| 2 | ...söker dig som vill arbeta som **läkemedelstekniker**! Vår kund söker nu fler medar... | x | x | 18 | 18 | [Läkemedelstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/WGEx_rgb_Czn) |
| 3 | ...ngsholmen. Har du en avslutad **gymnasieutbildning** med inriktning mot kemi/proce... | x | x | 18 | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 4 | ...utbildning med inriktning mot **kemi**/processteknik eller erfarenhe... | x | x | 4 | 4 | [Kemi, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/vpZY_6iv_NPL) |
| 5 | ...dning med inriktning mot kemi/**processteknik** eller erfarenhet av arbete me... |  | x |  | 13 | [Processteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Jgmc_3hD_JEt) |
| 5 | ...dning med inriktning mot kemi/**processteknik** eller erfarenhet av arbete me... | x |  |  | 13 | [Gymnasieskolans industritekniska program, inriktning processteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/gavY_KF9_kfS) |
| 6 | ...ller erfarenhet av arbete med **cellodling** i stor skala ska du söka den ... |  | x |  | 10 | [analysera cellodlingar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PecV_rD7_PyS) |
| 7 | ...r tjänsten.   Uppdraget är på **heltid**, måndag till fredag och arbet... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 8 | ...om hos andra arbetsgivare med **kollektivavtal** och förmåner som friskvårdsbi... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 9 | ...örmåner som friskvårdsbidrag, **företagshälsovård**, försäkringar och rabatt på t... | x | x | 17 | 17 | [Företagshälsovård, **skill**](http://data.jobtechdev.se/taxonomy/concept/ULfW_rVS_Vpr) |
| 10 | ...t arbete i batchprotokoll och **loggböcker**.   En vanlig dag på jobbet be... | x |  |  | 10 | [föra loggböcker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aF7g_4yc_iKU) |
| 11 | ...r, ångsteriliserar och rengör **bioreaktorer** och odlingsutrustning Provtar... |  | x |  | 12 | [underhålla bioreaktorer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1gpw_Fgd_aV7) |
| 12 | ...r och rengör bioreaktorer och **odlingsutrustning** Provtar och analyserar odling... |  | x |  | 17 | [odlingsutrustning för vattenbruk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7WpJ_iif_1o4) |
| 13 | ...r Ställer in styrparametrar i **odlingsutrustning** enligt instruktioner Bevakar ... |  | x |  | 17 | [odlingsutrustning för vattenbruk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7WpJ_iif_1o4) |
| 14 | ... vidtar åtgärder i samråd med **arbetsledare** Avbokar och inventerar materi... | x |  |  | 12 | [Arbetsledare, tillverkning kemiska produkter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Ja9V_x6C_mTt) |
| 15 | ...agerplatser Utför kontroller, **kalibreringar**, service och underhållsarbete... |  | x |  | 13 | [Kalibrering, **skill**](http://data.jobtechdev.se/taxonomy/concept/1BKz_4at_cuy) |
| 16 | ...bete av mindre utrustning och **instrument** inom sektionen    Kvalifikati... | x | x | 10 | 10 | [Instrument, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/wLYs_CKS_wK8) |
| 17 | ...här tjänsten behöver du ha en **gymnasieutbildning** med inriktning kemi/processte... | x | x | 18 | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 18 | ...asieutbildning med inriktning **kemi**/processteknik alternativt en ... | x | x | 4 | 4 | [Kemi, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/vpZY_6iv_NPL) |
| 19 | ...tbildning med inriktning kemi/**processteknik** alternativt en lång erfarenhe... |  | x |  | 13 | [Processteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Jgmc_3hD_JEt) |
| 19 | ...tbildning med inriktning kemi/**processteknik** alternativt en lång erfarenhe... | x |  |  | 13 | [Gymnasieskolans industritekniska program, inriktning processteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/gavY_KF9_kfS) |
| 20 | ...rnativt en lång erfarenhet av **cellodling** i stor skala   För oss är det... |  | x |  | 10 | [analysera cellodlingar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PecV_rD7_PyS) |
| 21 | ...nhet av arbete i enlighet med **GMP** är starkt meriterande  Ansöka... |  | x |  | 3 | [GXP (t.ex. GMP, GCP, GLP), **skill**](http://data.jobtechdev.se/taxonomy/concept/Q6DB_h8V_mc7) |
| 22 | ...cher, från Kiruna i norr till **Malmö** i söder. Vår ambition är att ... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| | **Overall** | | | **121** | **288** | 121/288 = **42%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Kalibrering, **skill**](http://data.jobtechdev.se/taxonomy/concept/1BKz_4at_cuy) |
|  | x |  | [underhålla bioreaktorer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1gpw_Fgd_aV7) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [odlingsutrustning för vattenbruk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7WpJ_iif_1o4) |
| x |  |  | [Arbetsledare, tillverkning kemiska produkter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Ja9V_x6C_mTt) |
|  | x |  | [Processteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Jgmc_3hD_JEt) |
|  | x |  | [analysera cellodlingar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PecV_rD7_PyS) |
|  | x |  | [GXP (t.ex. GMP, GCP, GLP), **skill**](http://data.jobtechdev.se/taxonomy/concept/Q6DB_h8V_mc7) |
| x | x | x | [Företagshälsovård, **skill**](http://data.jobtechdev.se/taxonomy/concept/ULfW_rVS_Vpr) |
| x | x | x | [Läkemedelstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/WGEx_rgb_Czn) |
| x |  |  | [föra loggböcker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aF7g_4yc_iKU) |
| x |  |  | [Gymnasieskolans industritekniska program, inriktning processteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/gavY_KF9_kfS) |
|  | x |  | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x | x | x | [Kemi, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/vpZY_6iv_NPL) |
| x | x | x | [Instrument, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/wLYs_CKS_wK8) |
| x | x | x | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **6** | 6/17 = **35%** |