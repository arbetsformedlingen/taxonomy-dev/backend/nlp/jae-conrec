# Results for '184009bd955113ffa048164676625c64841fa127'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [184009bd955113ffa048164676625c64841fa127](README.md) | 1 | 353 | 4 | 14 | 28/207 = **14%** | 1/10 = **10%** |

## Source text

Lastbilsförare C-KORT Partille Kyltransport är en nyare transport, bud och distributions partner lokaliserade främst inom Göteborgsområdet. . Vi har en modern fordonsflotta för alla transporter. Vi söker nu två stycken lastbilsförare för updrag åt postnord! Lön enligt kollektivavtal. Välkommen med din ansökan! skickas till info@partillekyltransport.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Lastbilsförare** C-KORT Partille Kyltransport ... |  | x |  | 14 | [Lastbils- och bussförare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/2zd4_YNR_xoM) |
| 1 | **Lastbilsförare** C-KORT Partille Kyltransport ... | x | x | 14 | 14 | [Lastbilsförare m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/3MBw_pDA_P2F) |
| 1 | **Lastbilsförare** C-KORT Partille Kyltransport ... |  | x |  | 14 | [Lastbilschaufför, **job-title**](http://data.jobtechdev.se/taxonomy/concept/9wnn_PAV_q12) |
| 1 | **Lastbilsförare** C-KORT Partille Kyltransport ... |  | x |  | 14 | [Lastbilsförare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/DrhD_263_rSc) |
| 1 | **Lastbilsförare** C-KORT Partille Kyltransport ... |  | x |  | 14 | [Lastbilsförare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/d68E_e74_a59) |
| 1 | **Lastbilsförare** C-KORT Partille Kyltransport ... |  | x |  | 14 | [Lastbilsförare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/ikwG_YRm_1Xk) |
| 2 | Lastbilsförare **C-KORT** Partille Kyltransport är en n... | x |  |  | 6 | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
| 3 | ...nsport är en nyare transport, **bud** och distributions partner lok... |  | x |  | 3 | [Budbilsförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PtiU_6mm_yyL) |
| 4 | ...tner lokaliserade främst inom **Göteborgsområdet**. . Vi har en modern fordonsfl... | x |  |  | 16 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 5 | ...rter. Vi söker nu två stycken **lastbilsförare** för updrag åt postnord! Lön e... |  | x |  | 14 | [Lastbils- och bussförare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/2zd4_YNR_xoM) |
| 5 | ...rter. Vi söker nu två stycken **lastbilsförare** för updrag åt postnord! Lön e... | x | x | 14 | 14 | [Lastbilsförare m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/3MBw_pDA_P2F) |
| 5 | ...rter. Vi söker nu två stycken **lastbilsförare** för updrag åt postnord! Lön e... |  | x |  | 14 | [Lastbilschaufför, **job-title**](http://data.jobtechdev.se/taxonomy/concept/9wnn_PAV_q12) |
| 5 | ...rter. Vi söker nu två stycken **lastbilsförare** för updrag åt postnord! Lön e... |  | x |  | 14 | [Lastbilsförare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/DrhD_263_rSc) |
| 5 | ...rter. Vi söker nu två stycken **lastbilsförare** för updrag åt postnord! Lön e... |  | x |  | 14 | [Lastbilsförare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/d68E_e74_a59) |
| 5 | ...rter. Vi söker nu två stycken **lastbilsförare** för updrag åt postnord! Lön e... |  | x |  | 14 | [Lastbilsförare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/ikwG_YRm_1Xk) |
| 6 | ...pdrag åt postnord! Lön enligt **kollektivavtal**. Välkommen med din ansökan! s... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **28** | **207** | 28/207 = **14%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Lastbils- och bussförare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/2zd4_YNR_xoM) |
| x | x | x | [Lastbilsförare m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/3MBw_pDA_P2F) |
|  | x |  | [Lastbilschaufför, **job-title**](http://data.jobtechdev.se/taxonomy/concept/9wnn_PAV_q12) |
| x |  |  | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
|  | x |  | [Lastbilsförare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/DrhD_263_rSc) |
| x |  |  | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
|  | x |  | [Budbilsförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PtiU_6mm_yyL) |
|  | x |  | [Lastbilsförare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/d68E_e74_a59) |
|  | x |  | [Lastbilsförare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/ikwG_YRm_1Xk) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **1** | 1/10 = **10%** |