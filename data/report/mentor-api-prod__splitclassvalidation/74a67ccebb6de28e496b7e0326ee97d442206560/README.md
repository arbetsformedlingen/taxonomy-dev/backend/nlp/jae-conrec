# Results for '74a67ccebb6de28e496b7e0326ee97d442206560'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [74a67ccebb6de28e496b7e0326ee97d442206560](README.md) | 1 | 430 | 7 | 3 | 7/103 = **7%** | 1/8 = **12%** |

## Source text

Hel/deltids gatuköksäljare Nu vill jag utöka min personalstyrka med en till personal. Jag söker hel/deltids anställd. Gärna med restaurang/säljare erfarenhet, men det är inget krav. Jag söker en social, stresståligt person som kan prata svenska. Gärna med arbetsförmedlingsstöd Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Hel/deltids** gatuköksäljare Nu vill jag ut... | x |  |  | 11 | (not found in taxonomy) |
| 2 | Hel/deltids **gatuköksäljare** Nu vill jag utöka min persona... | x |  |  | 14 | [gatumatsförsäljare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/X1uv_tun_h5L) |
| 3 | ...d en till personal. Jag söker **hel/deltids anställd**. Gärna med restaurang/säljare... | x |  |  | 20 | (not found in taxonomy) |
| 4 | ...l/deltids anställd. Gärna med **restaurang**/säljare erfarenhet, men det ä... | x |  |  | 10 | [ansvara för restaurangservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gcNi_bMv_tTG) |
| 4 | ...l/deltids anställd. Gärna med **restaurang**/säljare erfarenhet, men det ä... |  | x |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 5 | ...nställd. Gärna med restaurang/**säljare** erfarenhet, men det är inget ... | x |  |  | 7 | [sälja produkter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zVth_nYb_sxr) |
| 6 | ...et krav. Jag söker en social, **stresståligt** person som kan prata svenska.... | x |  |  | 12 | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
| 6 | ...et krav. Jag söker en social, **stresståligt** person som kan prata svenska.... |  | x |  | 12 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 7 | ...sståligt person som kan prata **svenska**. Gärna med arbetsförmedlingss... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **7** | **103** | 7/103 = **7%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [gatumatsförsäljare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/X1uv_tun_h5L) |
| x |  |  | [ansvara för restaurangservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gcNi_bMv_tTG) |
| x |  |  | (not found in taxonomy) |
|  | x |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x |  |  | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x |  |  | [sälja produkter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zVth_nYb_sxr) |
| | | **1** | 1/8 = **12%** |