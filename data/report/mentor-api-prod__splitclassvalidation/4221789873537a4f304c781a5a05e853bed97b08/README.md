# Results for '4221789873537a4f304c781a5a05e853bed97b08'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4221789873537a4f304c781a5a05e853bed97b08](README.md) | 1 | 1037 | 3 | 3 | 5/35 = **14%** | 1/4 = **25%** |

## Source text

Liveproduktion Hej! Vi på Digital Video Design söker dig som är i behov av ett extra arbete på helger men också vissa kvällar i veckan. Arbetet gäller liveproduktioner inom sport. Din uppgift blir att filma olika sporter men främst innebandy. I arbetsuppgiften ingår också koppling av all utrustning. Du arbetar tätt med en producent som alltid finns med dig på distans via headset. Fråga gärna mer via mail! Kontakt@digitalvideodesign.se Hello! We on Digital Video Design are looking for someone that has the need for a part time work. It will mostly be weekends but also evenings on the weekday. You will be working with livestreaming broadcast on location. Mainly for sportsevent. Your task will be cameraoperator on site. You will be working together with a producer from distance on each event. If you find this interesting, please send us an email! Kontakt@digitalvideodesign.se Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... gäller liveproduktioner inom **sport**. Din uppgift blir att filma o... | x | x | 5 | 5 | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
| 2 | ...m sport. Din uppgift blir att **filma** olika sporter men främst inne... | x |  |  | 5 | (not found in taxonomy) |
| 3 | ... uppgift blir att filma olika **sporter** men främst innebandy. I arbet... | x |  |  | 7 | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
| 4 | ...ilma olika sporter men främst **innebandy**. I arbetsuppgiften ingår ocks... |  | x |  | 9 | [Innebandy, instruktör/Innebandy, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/tzQv_YHB_TrS) |
| 5 | ...tning. Du arbetar tätt med en **producent** som alltid finns med dig på d... |  | x |  | 9 | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| | **Overall** | | | **5** | **35** | 5/35 = **14%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| x |  |  | (not found in taxonomy) |
| x | x | x | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
|  | x |  | [Innebandy, instruktör/Innebandy, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/tzQv_YHB_TrS) |
| | | **1** | 1/4 = **25%** |