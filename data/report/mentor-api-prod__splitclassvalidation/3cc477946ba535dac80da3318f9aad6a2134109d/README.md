# Results for '3cc477946ba535dac80da3318f9aad6a2134109d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [3cc477946ba535dac80da3318f9aad6a2134109d](README.md) | 1 | 1956 | 25 | 21 | 88/437 = **20%** | 8/29 = **28%** |

## Source text

Undersköterska till Kattegattkliniken Om Praktikertjänst AB Från Karesuando i norr till Ystad i söder, och med verksamheter i fler än 200 kommuner, är Praktikertjänst hela Sveriges välfärd. En av fem vårdpatienter går till någon av våra mottagningar – och en av tre tandvårdspatienter väljer oss.  Vår affärsidé är att skapa förutsättningar för att vi som arbetar inom vården ska kunna göra ett så bra jobb som möjligt – och skapa högsta värde för patienten. Det är vi som utför vården som gemensamt äger Praktikertjänst.  Vår unika affärsmodell har funnits i 60 år, och innebär att företagets 1 300 aktieägare själva arbetar i verksamheterna som läkare, tandläkare, tandtekniker, tandhygienister, fysioterapeuter, dietister, psykologer, psykoanalytiker/terapeuter, sjuksköterskor, barnmorskor och logopeder. Det gör oss till Sveriges största företag inom privat tandvård och hälso- och sjukvård.  Praktikertjänst omsätter cirka 8,5 miljarder kronor och har drygt 7 100 medarbetare.  Om Kattegattkliniken Vi är en komplett vårdcentral nere på Söder i Halmstad. Vår strävan är att ge våra patienter en professionell och trygg vård. Vi är inte Halmstads största vårdcentral och det innebär möjlighet till ett personligt bemötande, både gentemot våra patienter och arbetskamrater. Teamkänsla och ett gott arbetsklimat är viktigt för oss.  Arbetsuppgifter Som undersköterska på vår läkarmottagning ansvarar du för provtagning på laboratoriet samt assisterar läkarna under mottagningstid. I tjänsten ingår även receptionsarbete.  Övrig information Intervjuer sker löpande och tjänsten kan komma att tillsättas innan ansökningstidens utgång.  Kvalifikationer Utbildad undersköterska, gärna med erfarenhet från vårdcentralsarbete.  Personliga egenskaper Vi söker dig som är glad och positiv och som har framåtanda. Du gillar ett omväxlande arbete, är ansvarstagande och tycker om att arbeta tvärprofessionellt. Vi lägger stor vikt vid personlig lämplighet.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Undersköterska** till Kattegattkliniken Om Pra... |  | x |  | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 1 | **Undersköterska** till Kattegattkliniken Om Pra... | x |  |  | 14 | [Undersköterska, vård- och specialavdelning och mottagning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PQkQ_Dmk_ZF8) |
| 2 | ...B Från Karesuando i norr till **Ystad** i söder, och med verksamheter... | x |  |  | 5 | [Ystad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hdYk_hnP_uju) |
| 3 | ...uner, är Praktikertjänst hela **Sveriges** välfärd. En av fem vårdpatien... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ... mottagningar – och en av tre **tandvårds**patienter väljer oss.  Vår aff... | x |  |  | 9 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 5 | ... arbetar i verksamheterna som **läkare**, tandläkare, tandtekniker, ta... | x | x | 6 | 6 | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| 6 | ... i verksamheterna som läkare, **tandläkare**, tandtekniker, tandhygieniste... | x | x | 10 | 10 | [tandläkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/teYK_qML_UE3) |
| 7 | ...terna som läkare, tandläkare, **tandtekniker**, tandhygienister, fysioterape... |  | x |  | 12 | [Tandtekniker och ortopedingenjörer m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/39yB_xPQ_ozd) |
| 7 | ...terna som läkare, tandläkare, **tandtekniker**, tandhygienister, fysioterape... |  | x |  | 12 | [Tandtekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9fEz_9ji_74Y) |
| 7 | ...terna som läkare, tandläkare, **tandtekniker**, tandhygienister, fysioterape... | x |  |  | 12 | [tandtekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Q95S_LZx_Aqx) |
| 8 | ...re, tandläkare, tandtekniker, **tandhygienister**, fysioterapeuter, dietister, ... | x | x | 15 | 15 | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
| 9 | ...andtekniker, tandhygienister, **fysioterapeuter**, dietister, psykologer, psyko... | x |  |  | 15 | [fysioterapeut, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/42X5_SVW_dxY) |
| 9 | ...andtekniker, tandhygienister, **fysioterapeuter**, dietister, psykologer, psyko... |  | x |  | 15 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 9 | ...andtekniker, tandhygienister, **fysioterapeuter**, dietister, psykologer, psyko... |  | x |  | 15 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 10 | ...hygienister, fysioterapeuter, **dietister**, psykologer, psykoanalytiker/... |  | x |  | 9 | [Dietist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/C1FR_RzT_hzP) |
| 10 | ...hygienister, fysioterapeuter, **dietister**, psykologer, psykoanalytiker/... | x |  |  | 9 | [Dietister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Pq5C_SPC_bRA) |
| 11 | ..., fysioterapeuter, dietister, **psykologer**, psykoanalytiker/terapeuter, ... | x | x | 10 | 10 | [Psykolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xBo9_V9m_bfg) |
| 12 | ...euter, dietister, psykologer, **psykoanalytiker**/terapeuter, sjuksköterskor, b... | x |  |  | 15 | (not found in taxonomy) |
| 13 | ..., psykoanalytiker/terapeuter, **sjuksköterskor**, barnmorskor och logopeder. D... |  | x |  | 14 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 13 | ..., psykoanalytiker/terapeuter, **sjuksköterskor**, barnmorskor och logopeder. D... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 14 | ...r/terapeuter, sjuksköterskor, **barnmorskor** och logopeder. Det gör oss ti... |  | x |  | 11 | [Barnmorska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DEDi_wap_ntF) |
| 14 | ...r/terapeuter, sjuksköterskor, **barnmorskor** och logopeder. Det gör oss ti... | x |  |  | 11 | [Barnmorskor, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/dSc4_snH_7WR) |
| 15 | ...uksköterskor, barnmorskor och **logopeder**. Det gör oss till Sveriges st... | x | x | 9 | 9 | [Logoped, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BnhE_hcg_H2c) |
| 16 | ...h logopeder. Det gör oss till **Sveriges** största företag inom privat t... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 17 | ...s största företag inom privat **tandvård** och hälso- och sjukvård.  Pra... | x | x | 8 | 8 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 18 | ...etag inom privat tandvård och **hälso- oc**h sjukvård.  Praktikertjänst o... |  | x |  | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 19 | ...etag inom privat tandvård och **hälso- och sjukvård**.  Praktikertjänst omsätter ci... | x |  |  | 19 | [Hälso- och sjukvård, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/NYW6_mP6_vwf) |
| 20 | ...attkliniken Vi är en komplett **vårdcentral** nere på Söder i Halmstad. Vår... | x | x | 11 | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 21 | ...t vårdcentral nere på Söder i **Halmstad**. Vår strävan är att ge våra p... | x | x | 8 | 8 | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| 22 | ...ll och trygg vård. Vi är inte **Halmstads** största vårdcentral och det i... | x |  |  | 9 | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| 23 | ... Vi är inte Halmstads största **vårdcentral** och det innebär möjlighet til... | x | x | 11 | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 24 | ...r oss.  Arbetsuppgifter Som **undersköterska** på vår läkarmottagning ansvar... |  | x |  | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 24 | ...r oss.  Arbetsuppgifter Som **undersköterska** på vår läkarmottagning ansvar... | x |  |  | 14 | [Undersköterska, vård- och specialavdelning och mottagning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PQkQ_Dmk_ZF8) |
| 25 | ...ns utgång.  Kvalifikationer **Utbildad undersköterska**, gärna med erfarenhet från vå... | x |  |  | 23 | [Undersköterskeutbildning inom vuxenutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/nXZ3_v4W_WCS) |
| 26 | ....  Kvalifikationer Utbildad **undersköterska**, gärna med erfarenhet från vå... |  | x |  | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 27 | ...ka, gärna med erfarenhet från **vårdcentral**sarbete.  Personliga egenskap... | x |  |  | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 28 | ...lar ett omväxlande arbete, är **ansvarstagande** och tycker om att arbeta tvär... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| | **Overall** | | | **88** | **437** | 88/437 = **20%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Tandtekniker och ortopedingenjörer m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/39yB_xPQ_ozd) |
|  | x |  | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [fysioterapeut, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/42X5_SVW_dxY) |
|  | x |  | [Tandtekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9fEz_9ji_74Y) |
| x | x | x | [Logoped, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BnhE_hcg_H2c) |
|  | x |  | [Dietist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/C1FR_RzT_hzP) |
|  | x |  | [Barnmorska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DEDi_wap_ntF) |
| x | x | x | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| x |  |  | (not found in taxonomy) |
|  | x |  | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| x |  |  | [Hälso- och sjukvård, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/NYW6_mP6_vwf) |
| x |  |  | [Undersköterska, vård- och specialavdelning och mottagning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PQkQ_Dmk_ZF8) |
| x |  |  | [Dietister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Pq5C_SPC_bRA) |
| x |  |  | [tandtekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Q95S_LZx_Aqx) |
| x | x | x | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| x | x | x | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Barnmorskor, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/dSc4_snH_7WR) |
| x |  |  | [Ystad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hdYk_hnP_uju) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| x | x | x | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| x |  |  | [Undersköterskeutbildning inom vuxenutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/nXZ3_v4W_WCS) |
|  | x |  | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
|  | x |  | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| x | x | x | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| x | x | x | [tandläkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/teYK_qML_UE3) |
| x | x | x | [Psykolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xBo9_V9m_bfg) |
| | | **8** | 8/29 = **28%** |