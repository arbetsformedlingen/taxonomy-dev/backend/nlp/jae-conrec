# Results for '7f341c008dbdd1db6280cb8b7fe34c552e80a7b9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [7f341c008dbdd1db6280cb8b7fe34c552e80a7b9](README.md) | 1 | 3429 | 23 | 16 | 144/308 = **47%** | 11/21 = **52%** |

## Source text

Webbredaktör till HBS Nordic AB i Malmö Vi söker en webbredaktör med start i september! Tjänsten är ett vikariat där du har möjlighet att arbeta deltid eller heltid. Jobbet sträcker sig under 9 månader. Du utgår från vårt fina kontor i Malmö och ingår i ett team på 6 personer. Som webbredaktör hos oss… kommer du att arbeta med marknadsföring av hudvårdsmärkena Exuviance, Neostrata och Heliocare till både slutkunder och återförsäljare. Du kommer att ansvara för våra olika webbplatser så som externa hemsidor och återförsäljarsidor för alla de nordiska länderna. Exempel på arbetsuppgifter: • skapa, redigera och publicera innehåll på webbplatserna • proaktivt utveckla och förbättra samtliga webbplatser, samt skapa nya vid behov • anpassa och ta bort inaktuellt material • löpande arbeta med analys, uppföljning, sökmotoroptimering och rapportering • producera nyhetsbrev   För att axla rollen har du… tidigare erfarenhet av en liknande bred roll där du är mer generalist än specialist inom olika de olika områden som marknadsteamet arbetar med. Du har stor erfarenhet av att arbeta i olika CMS-verktyg, har förståelse för webbdesign och hanterar såväl Google Analytics och sökordsoptimering som content marketing. Du har stor förståelse för användarbeteende i digitala kanaler och behärskar svenska på hög nivå i både tal och skrift. Du har ett brinnande intresse för att skapa relevant innehåll, driva trafik, arbeta med konvertering och uppnå en överlägsen upplevelse för användaren. Det är meriterande om du har erfarenhet av att jobba i t ex Indesign och Photoshop samt vana av att jobba med sociala medier. Intresse eller kunskap om hudvård är ett plus. Som person tror vi att du har… en god samarbetsförmåga, är ansvarsfull, noggrann och strukturerad. Du är driven, lösningsorienterad och har god kommunikationsförmåga. Du får energi av att ge god service och få saker gjorda. Vi lägger stor vikt vid dina personliga egenskaper, din inställning och hur du tar dig an uppgifter och problem. Vi på HBS har en stor glädje och passion för hudvård och skönhet… vilket genomsyrar vår framgångsrika verksamhet. Våra värdeord Personligt, Professionell och Passionerad styr allt vi gör och ligger till grund för vårt positiva arbete. Hos oss blir du en del av vårt sammansvetsade team där vi tillsammans ser till att vi har nöjda och återkommande kunder. Då vi är ett litet kontor sätter vi stort värde på vår förmåga att samarbeta och ha kul tillsammans! Besök gärna vår hemsida www.hbsnordic.se för mer information om oss. HBS Nordic AB arbetar med flera av de starkaste salongs- och klinikmärkena på den nordiska hudvårdsmarknaden – Exuviance, NeoStrata och Heliocare. Vi har gedigen kunskap och stor passion för hälsa och skönhet vilket har gett oss framgång i över 20 år. Tillsammans med våra kunder tillika ambassadörer – hudvårdssalonger, estetiska kliniker och skönhetsbutiker - har vi som mål att hjälpa människor till ett ökat välmående. Om detta låter som en spännande utmaning för dig… är du varmt välkommen med din ansökan! Sök tjänsten och bifoga ditt CV och personliga brev. Vi tillämpar löpande urval så vänta inte med att skicka in din ansökan. Det kommer dock inte att ske någonting i rekryteringsprocessen vecka 29, 30 och 31 på grund av semester. Har du frågor eller funderingar övrig tid är du varmt välkommen att kontakta Jenny-Maria Lindström på Roi Rekrytering – jenny-maria.lindstrom@roirekrytering.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Webbredaktör** till HBS Nordic AB i Malmö Vi... | x | x | 12 | 12 | [Webbredaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b3Jk_Gfs_oo9) |
| 2 | ...redaktör till HBS Nordic AB i **Malmö** Vi söker en webbredaktör med ... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 3 | ...Nordic AB i Malmö Vi söker en **webbredaktör** med start i september! Tjänst... | x | x | 12 | 12 | [Webbredaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b3Jk_Gfs_oo9) |
| 4 | ... i september! Tjänsten är ett **vikariat** där du har möjlighet att arbe... | x |  |  | 8 | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| 5 | ...r du har möjlighet att arbeta **deltid eller heltid**. Jobbet sträcker sig under 9 ... | x |  |  | 19 | (not found in taxonomy) |
| 6 | ...utgår från vårt fina kontor i **Malmö** och ingår i ett team på 6 per... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 7 | ...i ett team på 6 personer. Som **webbredaktör** hos oss… kommer du att arbeta... | x | x | 12 | 12 | [Webbredaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b3Jk_Gfs_oo9) |
| 8 | ...oss… kommer du att arbeta med **marknadsföring** av hudvårdsmärkena Exuviance,... | x |  |  | 14 | [Marknadsföring/PR, **skill**](http://data.jobtechdev.se/taxonomy/concept/5yDE_FiQ_bMn) |
| 9 | ...arenhet av att arbeta i olika **CMS-verktyg**, har förståelse för webbdesig... |  | x |  | 11 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 10 | ...S-verktyg, har förståelse för **webbdesign** och hanterar såväl Google Ana... | x | x | 10 | 10 | [Webbdesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/YNXq_yH4_fhK) |
| 11 | ...webbdesign och hanterar såväl **Google Analytics** och sökordsoptimering som con... | x | x | 16 | 16 | [Google Analytics, **skill**](http://data.jobtechdev.se/taxonomy/concept/oCKN_G9k_zTj) |
| 12 | ...ar såväl Google Analytics och **sökordsoptimering** som content marketing. Du har... | x | x | 17 | 17 | [Sökordsoptimering/SEO, **skill**](http://data.jobtechdev.se/taxonomy/concept/zYHW_Mtu_jqq) |
| 13 | ...ics och sökordsoptimering som **content marketing**. Du har stor förståelse för a... | x | x | 17 | 17 | [Content marketing, **skill**](http://data.jobtechdev.se/taxonomy/concept/qTCs_crJ_Lzw) |
| 14 | ...g. Du har stor förståelse för **användarbeteende i digitala kanaler** och behärskar svenska på hög ... | x |  |  | 35 | [hantering av sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MgRt_szJ_YYJ) |
| 15 | ...igitala kanaler och behärskar **svenska** på hög nivå i både tal och sk... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 16 | ...tt brinnande intresse för att **skapa relevant innehåll**, driva trafik, arbeta med kon... | x |  |  | 23 | [utveckla digitalt innehåll, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6P3N_8vS_3Ba) |
| 17 | ...rfarenhet av att jobba i t ex **Indesign** och Photoshop samt vana av at... | x | x | 8 | 8 | [Layout-Adobe InDesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2t4_aS8_tGo) |
| 18 | ...att jobba i t ex Indesign och **Photoshop** samt vana av att jobba med so... | x | x | 9 | 9 | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
| 19 | ...op samt vana av att jobba med **sociala medier**. Intresse eller kunskap om hu... | x |  |  | 14 | [hantering av sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MgRt_szJ_YYJ) |
| 20 | ...er. Intresse eller kunskap om **hudvård** är ett plus. Som person tror ... | x |  |  | 7 | [kosmetisk hudvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/YANe_Lox_8Gg) |
| 21 | ...on tror vi att du har… en god **samarbetsförmåga**, är ansvarsfull, noggrann och... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 22 | ...… en god samarbetsförmåga, är **ansvarsfull**, noggrann och strukturerad. D... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 23 | ...rbetsförmåga, är ansvarsfull, **noggrann** och strukturerad. Du är drive... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 24 | ...n stor glädje och passion för **hudvård** och skönhet… vilket genomsyra... | x |  |  | 7 | [Hudvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Drzy_zPc_H4q) |
| 25 | ... kunskap och stor passion för **hälsa** och skönhet vilket har gett o... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| | **Overall** | | | **144** | **308** | 144/308 = **47%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Marknadsföring/PR, **skill**](http://data.jobtechdev.se/taxonomy/concept/5yDE_FiQ_bMn) |
| x |  |  | [utveckla digitalt innehåll, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6P3N_8vS_3Ba) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [Hudvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Drzy_zPc_H4q) |
|  | x |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| x |  |  | [hantering av sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MgRt_szJ_YYJ) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Layout-Adobe InDesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2t4_aS8_tGo) |
| x |  |  | [kosmetisk hudvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/YANe_Lox_8Gg) |
| x | x | x | [Webbdesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/YNXq_yH4_fhK) |
| x | x | x | [Webbredaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/b3Jk_Gfs_oo9) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| x |  |  | (not found in taxonomy) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Google Analytics, **skill**](http://data.jobtechdev.se/taxonomy/concept/oCKN_G9k_zTj) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x | x | x | [Content marketing, **skill**](http://data.jobtechdev.se/taxonomy/concept/qTCs_crJ_Lzw) |
| x | x | x | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [Sökordsoptimering/SEO, **skill**](http://data.jobtechdev.se/taxonomy/concept/zYHW_Mtu_jqq) |
| | | **11** | 11/21 = **52%** |