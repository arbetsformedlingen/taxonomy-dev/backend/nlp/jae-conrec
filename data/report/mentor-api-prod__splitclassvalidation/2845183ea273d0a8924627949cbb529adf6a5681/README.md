# Results for '2845183ea273d0a8924627949cbb529adf6a5681'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2845183ea273d0a8924627949cbb529adf6a5681](README.md) | 1 | 391 | 7 | 6 | 42/113 = **37%** | 3/6 = **50%** |

## Source text

Pizzabagare och köksbiträde Vi på pizzeria Oliveros söker en duktig pizzabagare eller köksbiträde omgående.   Du som söker behöver ha minst 6 månaders erfarenhet av Pizzabakning, köks förbreding övriga uppgifter som förekommer kan vi lära dig men aktiv och samarbete måste du ha.   Du ska vara trevlig , aktiv , mogen , seriös och mm. viktigast av allt renhet och repektera våra kära kunder.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Pizzabagare** och köksbiträde Vi på pizzeri... | x |  |  | 11 | [Pizzabagare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9YaE_Syg_5ZB) |
| 1 | **Pizzabagare** och köksbiträde Vi på pizzeri... |  | x |  | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 2 | Pizzabagare och **köksbiträde** Vi på pizzeria Oliveros söker... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 3 | ...abagare och köksbiträde Vi på **pizzeria** Oliveros söker en duktig pizz... | x | x | 8 | 8 | [Pizzeria, **skill**](http://data.jobtechdev.se/taxonomy/concept/DmFL_vHQ_j7e) |
| 4 | ...eria Oliveros söker en duktig **pizzabagare** eller köksbiträde omgående.  ... | x |  |  | 11 | [Pizzabagare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9YaE_Syg_5ZB) |
| 4 | ...eria Oliveros söker en duktig **pizzabagare** eller köksbiträde omgående.  ... |  | x |  | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 5 | ...r en duktig pizzabagare eller **köksbiträde** omgående.   Du som söker behö... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 6 | ...de.   Du som söker behöver ha **minst 6 månaders erfarenhet** av Pizzabakning, köks förbred... | x |  |  | 27 | [Mindre än 1 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/yrAe_Fzi_E6u) |
| 7 | ...inst 6 månaders erfarenhet av **Pizzabakning**, köks förbreding övriga uppgi... | x | x | 12 | 12 | [Pizzabakning, **skill**](http://data.jobtechdev.se/taxonomy/concept/1abT_LRk_fg2) |
| | **Overall** | | | **42** | **113** | 42/113 = **37%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Pizzabakning, **skill**](http://data.jobtechdev.se/taxonomy/concept/1abT_LRk_fg2) |
| x |  |  | [Pizzabagare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9YaE_Syg_5ZB) |
| x | x | x | [Pizzeria, **skill**](http://data.jobtechdev.se/taxonomy/concept/DmFL_vHQ_j7e) |
|  | x |  | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| x | x | x | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| x |  |  | [Mindre än 1 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/yrAe_Fzi_E6u) |
| | | **3** | 3/6 = **50%** |