import unittest
from jae_conrec.string_aligner import StringMappingProblem, jae_map_sentences, StringMappingSettings, jae_map_string

class TestStringAligner(unittest.TestCase):
    def test_problem0(self):
        sol0 = StringMappingProblem([[3], [4], [5]]).solve()
        self.assertEqual(sol0.cost, 0)
        self.assertEqual(sol0.inds, [3, 4, 5])

    def test_problem1(self):
        sol = StringMappingProblem([[3, 10], [24, 4], [99, 5]]).solve()
        self.assertEqual(sol.cost, 0)
        self.assertEqual(sol.inds, [3, 4, 5])

    def test_problem2(self):
        sol = StringMappingProblem([[3, 10], [24, 4], [99, 6]]).solve()
        self.assertEqual(sol.cost, 1)
        self.assertEqual(sol.inds, [3, 4, 6])

    def test_problem3(self):
        problem = StringMappingProblem([[3, 10], [24, 40000], [99, 6]])
        problem.settings.unassigned_cost = 10
        sol = problem.solve()
        self.assertEqual(10, sol.cost)
        self.assertEqual([3, None, 99], sol.inds)

    def test_map_sentences(self):
        source = "Vi söker sjuk- och vårdpersonal"
        sentences = ["Vi söker sjukpersonal och vårdpersonal"]

        mapping = jae_map_sentences(source, sentences, StringMappingSettings().with_unassigned_cost(20))

        self.assertEqual(
            mapping.sentence_inds, [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 23, 24, 25, 26, 27, 28, 29, 30, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]])

    def test_map_string(self):
        result = jae_map_string("Vi söker programmerare i Java", "programmerare")
        self.assertEqual(0, result.cost)
        self.assertEqual(9, result.inds[0])





