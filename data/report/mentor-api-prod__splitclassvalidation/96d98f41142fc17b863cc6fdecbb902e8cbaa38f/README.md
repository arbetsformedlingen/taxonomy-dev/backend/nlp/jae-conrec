# Results for '96d98f41142fc17b863cc6fdecbb902e8cbaa38f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [96d98f41142fc17b863cc6fdecbb902e8cbaa38f](README.md) | 1 | 2066 | 21 | 7 | 73/263 = **28%** | 3/16 = **19%** |

## Source text

Bilrekonditionerare till Håbo Bilvård - Kungsängen Håbo Bilvård expanderar och söker en bilrekonditionerare till Kungsängen. Tjänsten är på heltid med provanställning.  Håbo bilvård är familjeföretaget sedan 1982 som har mellan 15-20 anställda och hjälper kunden med allt som rör bilen! Vår personal strävar alltid efter att tillgodose kundens behov, vare sig det är nya däck eller en vanlig handtvätt av bilen eller om kunden bara vill ha tips och råd om hur man ska uppnå bra resultat när man själv tvättar bilen. Detta görs med fokus på kvalitet och miljö, exempelvis genom svanen-märkta proffsprodukter och miljödiplom från Svensk Miljöbas.  www.håbobilvård.com (http://www.xn--hbobilvrd-52ag.com/)    Som Bilrekonditionerare kommer du bland annat:  - utföra all form av bilvård från utvändig tvätt till lackskydsbehandlingar  Krav:  - minst 6 månader arbetslivserfarenhet inom bilvård - B-körkort - God svenska i tal och skrift   Vi söker dig som:  gillar att ta initiativ och har ett starkt engagemang för både kunden och dina kollegor. Du är social kompetent och självgående på ett strukturerat sätt. Du gillar kundkontakten och är trygg i att arbeta aktivt med bearbetning av kunden och även marknadsföring. Arbetslivserfarenhet inom bilbranschen exempelvis mekaniker, däckverkstad, foliering är meriterande. Dessutom är erfarenhet som arbetsledare meriterande!  Övrigt:  Start: Omgående  Arbetstider: 7-16  Placering: Brunna, Kungsängen  Anställningsform: Heltid med 6 månaders provanställning    Urval sker löpande och tjänsten kan komma att tillsättas innan sista ansökningsdag. Dina svar på ansökningsfrågorna ligger till grund för bedömning av din ansökan.  Pga teknikaliteter hos Arbetsförmedlingen står Tillväxt Väsbys partner Thread AB som arbetsgivare i annonsens faktatext längst ner på annonsen i platsbanken. Tjänsten som beskrivs ovan är hos den arbetsgivare som omnämns i ovanstående text och det är också de som blir din arbetsgivare om det blir du som får tjänsten. Arbetsgivaren får rekryteringsstöd av Tillväxt Väsby för att hitta just dig.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bilrekonditionerare** till Håbo Bilvård - Kungsänge... | x | x | 19 | 19 | [Bilrekonditionerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/gP3r_Q2H_ht9) |
| 2 | Bilrekonditionerare till **Håbo** Bilvård - Kungsängen Håbo Bil... | x |  |  | 4 | [Håbo, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Bbs5_JUs_Qh5) |
| 3 | ...lvård expanderar och söker en **bilrekonditionerare** till Kungsängen. Tjänsten är ... | x | x | 19 | 19 | [Bilrekonditionerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/gP3r_Q2H_ht9) |
| 4 | ...ll Kungsängen. Tjänsten är på **heltid** med provanställning.  Håbo bi... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 5 | ... heltid med provanställning.  **Håbo** bilvård är familjeföretaget s... | x |  |  | 4 | [Håbo, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Bbs5_JUs_Qh5) |
| 6 | ...--hbobilvrd-52ag.com/)    Som **Bilrekonditionerare** kommer du bland annat:  - utf... | x | x | 19 | 19 | [Bilrekonditionerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/gP3r_Q2H_ht9) |
| 7 | ... annat:  - utföra all form av **bilvård** från utvändig tvätt till lack... | x |  |  | 7 | [Fordonsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/cq2h_mQV_qhi) |
| 8 | ...föra all form av bilvård från **utvändig tvätt** till lackskydsbehandlingar  K... | x |  |  | 14 | [tvätta fordon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/E4Xx_zfC_fAQ) |
| 9 | ...vård från utvändig tvätt till **lackskydsbehandlingar**  Krav:  - minst 6 månader arb... | x |  |  | 21 | [Fordonsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/cq2h_mQV_qhi) |
| 10 | ...ckskydsbehandlingar  Krav:  - **minst 6 månader arbetslivserfarenhet** inom bilvård - B-körkort - Go... | x |  |  | 36 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| 11 | ...der arbetslivserfarenhet inom **bilvård** - B-körkort - God svenska i t... | x |  |  | 7 | [Fordonsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/cq2h_mQV_qhi) |
| 12 | ...livserfarenhet inom bilvård - **B-körkort** - God svenska i tal och skrif... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 13 | ...nom bilvård - B-körkort - God **svenska** i tal och skrift   Vi söker d... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 14 | ...r. Du är social kompetent och **självgående** på ett strukturerat sätt. Du ... | x |  |  | 11 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ...earbetning av kunden och även **marknadsföring**. Arbetslivserfarenhet inom bi... | x |  |  | 14 | [Marknadsföring/PR, **skill**](http://data.jobtechdev.se/taxonomy/concept/5yDE_FiQ_bMn) |
| 16 | ... inom bilbranschen exempelvis **mekaniker**, däckverkstad, foliering är m... | x |  |  | 9 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 17 | ...anschen exempelvis mekaniker, **däckverkstad**, foliering är meriterande. De... | x |  |  | 12 | [Däckmontör/Däck- och hjulmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KRGZ_uFp_LuK) |
| 18 | ...lvis mekaniker, däckverkstad, **foliering** är meriterande. Dessutom är e... |  | x |  | 9 | [Foliering/Wrapping, **skill**](http://data.jobtechdev.se/taxonomy/concept/2PSK_NAG_sDa) |
| 18 | ...lvis mekaniker, däckverkstad, **foliering** är meriterande. Dessutom är e... | x |  |  | 9 | [Folierare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EMSh_67p_cSp) |
| 18 | ...lvis mekaniker, däckverkstad, **foliering** är meriterande. Dessutom är e... |  | x |  | 9 | [Foliering, **skill**](http://data.jobtechdev.se/taxonomy/concept/jK9j_m1s_vxD) |
| 19 | ...e. Dessutom är erfarenhet som **arbetsledare** meriterande!  Övrigt:  Start:... | x |  |  | 12 | [Arbetsledare, maskintillverkning, verkstadsprodukter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rJih_KYS_qUK) |
| 20 | ...Kungsängen  Anställningsform: **Heltid** med 6 månaders provanställnin... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **73** | **263** | 73/263 = **28%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Foliering/Wrapping, **skill**](http://data.jobtechdev.se/taxonomy/concept/2PSK_NAG_sDa) |
| x |  |  | [Marknadsföring/PR, **skill**](http://data.jobtechdev.se/taxonomy/concept/5yDE_FiQ_bMn) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Håbo, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Bbs5_JUs_Qh5) |
| x |  |  | [tvätta fordon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/E4Xx_zfC_fAQ) |
| x |  |  | [Folierare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EMSh_67p_cSp) |
| x |  |  | [Däckmontör/Däck- och hjulmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KRGZ_uFp_LuK) |
| x |  |  | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| x |  |  | [Fordonsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/cq2h_mQV_qhi) |
| x | x | x | [Bilrekonditionerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/gP3r_Q2H_ht9) |
|  | x |  | [Foliering, **skill**](http://data.jobtechdev.se/taxonomy/concept/jK9j_m1s_vxD) |
| x |  |  | [Arbetsledare, maskintillverkning, verkstadsprodukter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rJih_KYS_qUK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/16 = **19%** |