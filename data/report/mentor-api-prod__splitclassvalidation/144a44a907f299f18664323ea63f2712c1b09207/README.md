# Results for '144a44a907f299f18664323ea63f2712c1b09207'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [144a44a907f299f18664323ea63f2712c1b09207](README.md) | 1 | 2274 | 22 | 21 | 136/388 = **35%** | 9/22 = **41%** |

## Source text

Undersköterska till Korsnäsgårdens äldreboende i Falun A&O är ett vårdföretag som funnits sedan 1993. Företaget har verksamhet i flera kommuner, har ca 1 600 vårdplatser och sysselsätter omkring 2 500 medarbetare inom äldreomsorg. A&O vill skapa en positiv miljö och omsorg för människor i behov av särskilt boende och vård. Vårt mål är att man ska känna värme, omtanke och trygghet och ha kvar sin personliga integritet hela livet.    Vi söker nu en undersköterska till vår verksamhet Korsnäsgården i Falun.  Korsnäsgården är ett demensboende med 33 lägenheter beläget fem kilometer utanför centrala Falun. Huset har fyra avdelningar i markplan där det är enkelt att nå innergården. Lägenheterna är ca 37 kvadratmeter och har eget badrum och pentry.  Verksamheten är bemannad med undersköterskor dygnet runt för att kunna tillgodose de omvårdnadsbehov som finns. Hälso- och sjukvårdsinsatser tillgodoses med sjuksköterskor som finns tillgängliga dygnet runt samt av arbetsterapeuter och fysioterapeuter efter behov. Läkarinsatser tillgodoses via vårdcentral i samarbete med landstinget.  Kvalifikationer:  Som person är du självständig, ansvarsfull, god initiativförmåga, samarbetsvillig och har lätt för att kommunicera i tal och skrift. Du är flexibel och lösningsorienterad samt öppen för de senaste rönen inom demensvård/äldreomsorg. Du ska ha förmåga att skapa och upprätthålla goda relationer till alla boende, närstående samt medarbetare. Ett gott bemötande samt en god kvalité av den tjänst/insats som utförs dagligen är en självklarhet.  Vi vill att våra medarbetare ska vara positiva, engagerade och vilja bidra till att utveckla verksamheten genom delaktighet och ansvar.  Meriter:  Erfarenhet av äldreomsorg är meriterande  Övrigt:  Arbetstid/Varaktighet 100 % med tjänstgöring dag/kväll och helg. Tillsvidare, provanställning tillämpas. Tillträdesdatum: enligt ök  Alla anställningar inom företaget omfattas av kollektivavtal.  Intervjuer kan ske fortlöpande och tjänster kan tillsättas innan ansökningstidens slut. Vi lägger stor vikt vid personlig lämplighet.  Välkommen med din ansökan!  Har du frågor angående anställning på Korsnäsgården kontakta: Verksamhetschef, Kristina Almgren Angeria Mail: kristina.a.almgren@ansvarochomsorg.se  Telefon: 076-0165591

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Undersköterska** till Korsnäsgårdens äldreboen... |  | x |  | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 1 | **Undersköterska** till Korsnäsgårdens äldreboen... | x |  |  | 14 | [Undersköterska, hemtjänst, äldreboende och habilitering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3QA_5pk_uXd) |
| 2 | ...sköterska till Korsnäsgårdens **äldreboende** i Falun A&O är ett vårdföreta... |  | x |  | 11 | [Äldreboende, **skill**](http://data.jobtechdev.se/taxonomy/concept/tykJ_BZQ_Wer) |
| 3 | ... Korsnäsgårdens äldreboende i **Falun** A&O är ett vårdföretag som fu... | x | x | 5 | 5 | [Falun, **municipality**](http://data.jobtechdev.se/taxonomy/concept/N1wJ_Cuu_7Cs) |
| 4 | ...mkring 2 500 medarbetare inom **äldreomsorg**. A&O vill skapa en positiv mi... | x | x | 11 | 11 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 5 | ...sorg för människor i behov av **särskilt boende** och vård. Vårt mål är att man... | x | x | 15 | 15 | [Sjukhem/Särskilt boende, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZpXZ_PG5_B9s) |
| 6 | ...hela livet.    Vi söker nu en **undersköterska** till vår verksamhet Korsnäsgå... |  | x |  | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 6 | ...hela livet.    Vi söker nu en **undersköterska** till vår verksamhet Korsnäsgå... | x |  |  | 14 | [Undersköterska, hemtjänst, äldreboende och habilitering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3QA_5pk_uXd) |
| 7 | ...år verksamhet Korsnäsgården i **Falun**.  Korsnäsgården är ett demens... | x | x | 5 | 5 | [Falun, **municipality**](http://data.jobtechdev.se/taxonomy/concept/N1wJ_Cuu_7Cs) |
| 8 | ...em kilometer utanför centrala **Falun**. Huset har fyra avdelningar i... | x | x | 5 | 5 | [Falun, **municipality**](http://data.jobtechdev.se/taxonomy/concept/N1wJ_Cuu_7Cs) |
| 9 | ... Verksamheten är bemannad med **undersköterskor** dygnet runt för att kunna til... |  | x |  | 15 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 9 | ... Verksamheten är bemannad med **undersköterskor** dygnet runt för att kunna til... | x |  |  | 15 | [Undersköterska, hemtjänst, äldreboende och habilitering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3QA_5pk_uXd) |
| 10 | ...de omvårdnadsbehov som finns. **Hälso**- och sjukvårdsinsatser tillgo... | x |  |  | 5 | [Hälso, **keyword**](http://data.jobtechdev.se/taxonomy/concept/5TsD_dN8_Uco) |
| 11 | ...dsbehov som finns. Hälso- och **sjukvårdsinsatser** tillgodoses med sjukskötersko... |  | x |  | 17 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 12 | ...vårdsinsatser tillgodoses med **sjuksköterskor** som finns tillgängliga dygnet... |  | x |  | 14 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 12 | ...vårdsinsatser tillgodoses med **sjuksköterskor** som finns tillgängliga dygnet... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 13 | ...lgängliga dygnet runt samt av **arbetsterapeuter** och fysioterapeuter efter beh... | x |  |  | 16 | [Arbetsterapeuter, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/TPH4_2AM_isT) |
| 13 | ...lgängliga dygnet runt samt av **arbetsterapeuter** och fysioterapeuter efter beh... |  | x |  | 16 | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| 14 | ... samt av arbetsterapeuter och **fysioterapeuter** efter behov. Läkarinsatser ti... |  | x |  | 15 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 14 | ... samt av arbetsterapeuter och **fysioterapeuter** efter behov. Läkarinsatser ti... | x | x | 15 | 15 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 15 | ...Läkarinsatser tillgodoses via **vårdcentral** i samarbete med landstinget. ... | x | x | 11 | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 16 | ...fikationer:  Som person är du **självständig**, ansvarsfull, god initiativfö... | x | x | 12 | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 17 | ...om person är du självständig, **ansvarsfull**, god initiativförmåga, samarb... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 18 | ...rsfull, god initiativförmåga, **samarbetsvillig** och har lätt för att kommunic... | x |  |  | 15 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 19 | ...pen för de senaste rönen inom **demensvård**/äldreomsorg. Du ska ha förmåg... | x | x | 10 | 10 | [Demensvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/hqYJ_jyZ_Ks8) |
| 20 | ...senaste rönen inom demensvård/**äldreomsorg**. Du ska ha förmåga att skapa ... | x | x | 11 | 11 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 21 | ...var.  Meriter:  Erfarenhet av **äldreomsorg** är meriterande  Övrigt:  Arbe... | x | x | 11 | 11 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 22 | ...vrigt:  Arbetstid/Varaktighet **100 %** med tjänstgöring dag/kväll oc... | x |  |  | 5 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 23 | ...nstgöring dag/kväll och helg. **Tillsvidare, provanställning tillämpas**. Tillträdesdatum: enligt ök  ... | x |  |  | 38 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 24 | ...ar inom företaget omfattas av **kollektivavtal**.  Intervjuer kan ske fortlöpa... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **136** | **388** | 136/388 = **35%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [Hälso, **keyword**](http://data.jobtechdev.se/taxonomy/concept/5TsD_dN8_Uco) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
|  | x |  | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| x | x | x | [Falun, **municipality**](http://data.jobtechdev.se/taxonomy/concept/N1wJ_Cuu_7Cs) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Arbetsterapeuter, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/TPH4_2AM_isT) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Undersköterska, hemtjänst, äldreboende och habilitering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3QA_5pk_uXd) |
| x | x | x | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| x | x | x | [Sjukhem/Särskilt boende, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZpXZ_PG5_B9s) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| x | x | x | [Demensvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/hqYJ_jyZ_Ks8) |
| x |  |  | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| x | x | x | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
|  | x |  | [Äldreboende, **skill**](http://data.jobtechdev.se/taxonomy/concept/tykJ_BZQ_Wer) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **9** | 9/22 = **41%** |