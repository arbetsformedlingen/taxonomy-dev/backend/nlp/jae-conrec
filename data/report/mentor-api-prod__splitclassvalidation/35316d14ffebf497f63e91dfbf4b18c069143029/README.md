# Results for '35316d14ffebf497f63e91dfbf4b18c069143029'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [35316d14ffebf497f63e91dfbf4b18c069143029](README.md) | 1 | 675 | 6 | 4 | 40/58 = **69%** | 4/6 = **67%** |

## Source text

Nagelteknolog sökes Vi på nagelsalong Sam Nails, belägen i borås centrum söker nu en ny medarbetare. Vi eftersträvar att göra alla våra kunder så nöjda som möjligt. Vi har bra och god erfarenhet inom branschen. Du skall arbeta med de moment som ingår i yrket. Det innebär bland annat att du skall arbeta med naglar och både använda dig av gel och akryl. Du skall utföra manikyr och pedikyr. Du skall hålla ordning och reda på din arbetsplats. Vara tillmötesgående och serviceinriktad gentemot kunderna. Du skall ha erfarenhet eller motsvarande utbildning inom yrket. Du skall vara lätt lärd för nya moment och kunskaper inom branschen. Att kunna Vietnamesiska är meriterande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Nagelteknolog** sökes Vi på nagelsalong Sam N... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 2 | ...elsalong Sam Nails, belägen i **borås** centrum söker nu en ny medarb... | x |  |  | 5 | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| 3 | ...aglar och både använda dig av **gel och akryl**. Du skall utföra manikyr och ... | x |  |  | 13 | [applicera nagellack, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ERy3_6nd_Wpb) |
| 4 | ...el och akryl. Du skall utföra **manikyr** och pedikyr. Du skall hålla o... | x | x | 7 | 7 | [Handvård/Manikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/RVLi_2no_Eyg) |
| 5 | .... Du skall utföra manikyr och **pedikyr**. Du skall hålla ordning och r... | x | x | 7 | 7 | [Fotvård/Pedikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/5APZ_gLs_AB4) |
| 6 | ...per inom branschen. Att kunna **Vietnamesiska** är meriterande. | x | x | 13 | 13 | [Vietnamesiska, **language**](http://data.jobtechdev.se/taxonomy/concept/RqHU_aEE_tJA) |
| | **Overall** | | | **40** | **58** | 40/58 = **69%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Fotvård/Pedikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/5APZ_gLs_AB4) |
| x |  |  | [applicera nagellack, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ERy3_6nd_Wpb) |
| x | x | x | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| x | x | x | [Handvård/Manikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/RVLi_2no_Eyg) |
| x | x | x | [Vietnamesiska, **language**](http://data.jobtechdev.se/taxonomy/concept/RqHU_aEE_tJA) |
| x |  |  | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| | | **4** | 4/6 = **67%** |