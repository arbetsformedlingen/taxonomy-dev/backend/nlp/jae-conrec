# Results for 'ff377ae11f8348350d79da46521414aa736ed98a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ff377ae11f8348350d79da46521414aa736ed98a](README.md) | 1 | 731 | 6 | 5 | 64/81 = **79%** | 4/5 = **80%** |

## Source text

Specialistläkare, Bemanning Jämför och chatta med bemanningsbolagen enkelt via Vårdföretag App (vardforetag.se) Skapa din profil på mindre än 1 minut och lägg upp dina önskemål. Få erbjudanden på uppdrag som kan passa dina önskemål och chatta med flera av de ledande bemanningsbolagen inom vården. Enkelt och kostnadsfritt! Just nu finns ett ledigt uppdrag som specialistläkare på vårdcentral i Eskilstuna, Södermanlands län. Uppdraget finns att söka bland flera bemanningsbolag i Vårdföretag.se   Fördelar: -Hög ersättning -Inga onödiga Mail Eller telefonsamtal -Enkelt och kostnadsfritt -Inget bindande   Vill du starta eget aktiebolag? Vi på vardforetag.se hjälper dig att starta aktiebolag och redovisning till fasta kostnader.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Specialistläkare**, Bemanning Jämför och chatta ... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 2 | ... finns ett ledigt uppdrag som **specialistläkare** på vårdcentral i Eskilstuna, ... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 3 | ...pdrag som specialistläkare på **vårdcentral** i Eskilstuna, Södermanlands l... | x | x | 11 | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 4 | ...ialistläkare på vårdcentral i **Eskilstuna**, Södermanlands län. Uppdraget... | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 5 | ... på vårdcentral i Eskilstuna, **Södermanlands län**. Uppdraget finns att söka bla... | x |  |  | 17 | [Södermanlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/s93u_BEb_sx2) |
| 6 | ...dig att starta aktiebolag och **redovisning** till fasta kostnader. | x | x | 11 | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| | **Overall** | | | **64** | **81** | 64/81 = **79%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| x | x | x | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| x | x | x | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| x | x | x | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| x |  |  | [Södermanlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/s93u_BEb_sx2) |
| | | **4** | 4/5 = **80%** |