# Results for '6de9f296d2c24eea893ecd7bfe232b54ab76da53'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [6de9f296d2c24eea893ecd7bfe232b54ab76da53](README.md) | 1 | 3443 | 23 | 21 | 166/319 = **52%** | 9/17 = **53%** |

## Source text

Matkreatör till rollen som köksmästare på Smådalarö Gård Hotell & Spa Är du en matkreatör med passion för god mat och kvalité? Välkommen med din ansökan redan idag!   Vi söker en köksmästare till Smådalarö Gård Hotell & Spa -ett av Sveriges största hotelldestinationer i Stockholms skärgård ca 40 min med bil från Stockholms innerstad. Här njuter du av en bekymmersfri tillvaro, oavsett om du längtar efter avkoppling eller en stund av eskapism erbjuder vi noga utvalda upplevelser som möter dina tillflyktsönskemål på ett varmt och personligt sätt.   Om rollen  Som köksmästare är du med och skapar, förädlar och utvecklar hotellets matkoncept. Tillsammans med teamet stöttar ni varandra i det dagliga arbetet och är med på resan att göra hotellet till en av Stockholms självklara destinationer. Du ansvarar för planeringen i köket och för att skapa en matupplevelse i världsklass.   Exempel på arbetsuppgifter som köksmästare:     • Leda och fördela arbetet på din avdelning följt av schemaläggning, personalplanering och kvalitetssäkring.  • Du ansvarar för att maten håller god kvalité.  • Konceptutveckling, menyplanering, produktion av maten, ekonomiskt ansvar, inventering, inköp och livsmedelshygien.  • Hållbarhetsutveckling enligt företagets miljömål samt säkerställa att samtliga av företagets framtagna policys följs.     Din profil och krav för tjänsten:   • Du är en trendmedveten matkreatör med passion för god mat, har ett öga för finess och gedigen kunskap om mat    • Du är en riktigt duktig och kreativ matlagare som är van att producera mat med högsta standard, kvalité och kvantitet    • Du har mycket goda ledaregenskaper och brinner för att utveckla samt coacha dina medarbetare   • Du har dokumenterad erfarenhet från tidigare tjänst som köksmästare  • Du har tidigare erfarenhet av budget- och resultatansvar samt relevant utbildning inom kök så som köksbudget, egenkontroll, miljö och hälsa  • Körkort och tillgång till bil   Som person ser vi att du är:    • Ansvarsfull, strukturerad och prestigelös i ditt arbete  • Lösningsorienterad, kvalitetsmedveten och har en förmåga att se helheten såväl som detaljer  • Du uppmuntrar till samarbete och engagemang  • Modig, vågar driva dina frågor och ta initiativ    • Kreativ, driftig och flexibel.    • Vi på Sabis strävar efter ständig utveckling och därför behöver du som Köksmästare hela tiden arbeta för att skapa bättre kundupplevelser för dina gäster!     Ledare inom Sabis har förmåga att:    • Vara förebild   • Visa vägen   • Involvera & engagera     • Leverera resultat    Vi erbjuder  Ett roligt jobb i vacker skärgårdsmiljö på ett av Sveriges ledande destinationer inom aktivitet och avkoppling. Sabis är ett stabilt familjeföretag där vi arbetar mot en tydlig vision och tydliga mål. Med en platt organisation där vi har nära till beslut skapar vi ett starkt fokus på kunden. Kulturen präglas av utvecklingsvilja, frihet under ansvar, samarbete, kommunikation och glädje!     Kontakt I denna rekrytering har vi valt att samarbeta med Growisit - Vi rekryterar för besöksnäringen För frågor om tjänsten:  Kontaktperson: Niclas Ingwall Tel: 0706-48 62 47 niclas.ingwall@growisit.se Sista ansökningsdag är 2022-09-05 men vi genomför urval och intervjuer löpande.     En unik och naturnära destination med kurerade upplevelser för alla sinnen. Spa, aktiviteter, närodlad mat och personlig service.    Krav:  Körkort: * B (Personbil)  Tillgång till: * Personbil

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Matkreatör** till rollen som köksmästare p... |  | x |  | 10 | [Matkreatör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/vK9c_qBY_mHW) |
| 2 | Matkreatör till rollen som **köksmästare** på Smådalarö Gård Hotell & Sp... | x | x | 11 | 11 | [köksmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tNA4_ywP_j5q) |
| 3 | ...köksmästare på Smådalarö Gård **Hotell** & Spa Är du en matkreatör med... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 4 | ...re på Smådalarö Gård Hotell & **Spa** Är du en matkreatör med passi... | x |  |  | 3 | [Spa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YV8X_9Bt_23p) |
| 5 | ...rö Gård Hotell & Spa Är du en **matkreatör** med passion för god mat och k... |  | x |  | 10 | [Matkreatör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/vK9c_qBY_mHW) |
| 6 | ...kan redan idag!   Vi söker en **köksmästare** till Smådalarö Gård Hotell & ... | x | x | 11 | 11 | [köksmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tNA4_ywP_j5q) |
| 7 | ...ksmästare till Smådalarö Gård **Hotell** & Spa -ett av Sveriges störst... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 8 | ...arö Gård Hotell & Spa -ett av **Sveriges** största hotelldestinationer i... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 9 | ...största hotelldestinationer i **Stockholms** skärgård ca 40 min med bil fr... | x |  |  | 10 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 10 | ...ärgård ca 40 min med bil från **Stockholms** innerstad. Här njuter du av e... | x |  |  | 10 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 11 | ...onligt sätt.   Om rollen  Som **köksmästare** är du med och skapar, förädla... | x | x | 11 | 11 | [köksmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tNA4_ywP_j5q) |
| 12 | ... att göra hotellet till en av **Stockholms** självklara destinationer. Du ... | x |  |  | 10 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 13 | ...xempel på arbetsuppgifter som **köksmästare**:     • Leda och fördela arbet... | x | x | 11 | 11 | [köksmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tNA4_ywP_j5q) |
| 14 | ...ning följt av schemaläggning, **personalplanering** och kvalitetssäkring.  • Du a... | x | x | 17 | 17 | [Personalplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eThQ_HCv_mY6) |
| 15 | ...ggning, personalplanering och **kvalitetssäkring**.  • Du ansvarar för att maten... | x | x | 16 | 16 | [Kvalitetssäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/a1Sf_mey_gVk) |
| 16 | ... maten håller god kvalité.  • **Konceptutveckling**, menyplanering, produktion av... |  | x |  | 17 | [Konceptutveckling, UX, **skill**](http://data.jobtechdev.se/taxonomy/concept/ceUs_fcn_miS) |
| 17 | ...nsvar, inventering, inköp och **livsmedelshygien**.  • Hållbarhetsutveckling enl... | x | x | 16 | 16 | [Livsmedelshygien, **skill**](http://data.jobtechdev.se/taxonomy/concept/kj4Q_nLh_u8Q) |
| 18 | ...nköp och livsmedelshygien.  • **Hållbarhetsutveckling** enligt företagets miljömål sa... | x | x | 21 | 21 | [Hållbarhetsutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uGaX_6vC_5Tq) |
| 19 | ...n:   • Du är en trendmedveten **matkreatör** med passion för god mat, har ... |  | x |  | 10 | [Matkreatör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/vK9c_qBY_mHW) |
| 20 | ...nhet från tidigare tjänst som **köksmästare**  • Du har tidigare erfarenhet... | x | x | 11 | 11 | [köksmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tNA4_ywP_j5q) |
| 21 | ...Du har tidigare erfarenhet av **budget**- och resultatansvar samt rele... | x | x | 6 | 6 | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| 22 | ... tidigare erfarenhet av budget**- och **resultatansvar samt relevant u... |  | x |  | 6 | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| 23 | ...dget, egenkontroll, miljö och **hälsa**  • Körkort och tillgång till ... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 24 | ...nkontroll, miljö och hälsa  • **Körkort** och tillgång till bil   Som p... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 24 | ...nkontroll, miljö och hälsa  • **Körkort** och tillgång till bil   Som p... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 25 | ...person ser vi att du är:    • **Ansvarsfull**, strukturerad och prestigelös... |  | x |  | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 26 | ...ing och därför behöver du som **Köksmästare** hela tiden arbeta för att ska... | x | x | 11 | 11 | [köksmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tNA4_ywP_j5q) |
| 27 | ...cker skärgårdsmiljö på ett av **Sveriges** ledande destinationer inom ak... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 28 | ...es ledande destinationer inom **aktivitet** och avkoppling. Sabis är ett ... | x |  |  | 9 | [Idrott/aktivitet, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/LeJk_Xa1_TuS) |
| 29 | ...ersonlig service.    Krav:  **Körkort**: * B (Personbil)  Tillgång ti... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 30 | ...g service.    Krav:  Körkort**: * B (Personbil)**  Tillgång till: * Personbil | x |  |  | 17 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| | **Overall** | | | **166** | **319** | 166/319 = **52%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x |  |  | [Idrott/aktivitet, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/LeJk_Xa1_TuS) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Spa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YV8X_9Bt_23p) |
| x | x | x | [Kvalitetssäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/a1Sf_mey_gVk) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Konceptutveckling, UX, **skill**](http://data.jobtechdev.se/taxonomy/concept/ceUs_fcn_miS) |
| x | x | x | [Personalplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eThQ_HCv_mY6) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Livsmedelshygien, **skill**](http://data.jobtechdev.se/taxonomy/concept/kj4Q_nLh_u8Q) |
| x | x | x | [köksmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tNA4_ywP_j5q) |
| x | x | x | [Hållbarhetsutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uGaX_6vC_5Tq) |
|  | x |  | [Matkreatör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/vK9c_qBY_mHW) |
| | | **9** | 9/17 = **53%** |