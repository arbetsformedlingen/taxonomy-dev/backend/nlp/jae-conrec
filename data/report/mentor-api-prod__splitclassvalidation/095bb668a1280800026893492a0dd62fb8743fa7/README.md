# Results for '095bb668a1280800026893492a0dd62fb8743fa7'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [095bb668a1280800026893492a0dd62fb8743fa7](README.md) | 1 | 4118 | 21 | 24 | 145/324 = **45%** | 8/19 = **42%** |

## Source text

Content Manager till Intersolia Göteborg! Är du en vass skribent och affärsorienterad med expertis inom riktad content? Vill du ta ditt nästa karriärsteg och arbeta i en utvecklande roll med stort ansvar? Då är rollen som Content Manager något för dig!  På Intersolia blir du en del av ett växande internationellt kemikaliehanteringsföretag. Du kommer bli en av de första att tillhöra marknadsavdelningen som står inför en spännande resa framåt, en resa som du kommer att spela en viktig roll inom. Du rapporterar till marknadschefen och tillsammans kommer ni skapa förutsättningar för att få avdelningen och bolaget att växa.  Arbetsuppgifter  I rollen som Content Manager på Intersolia kommer dina kunskaper inom copywriting till användning och du ansvarar för hela processen d.v.s skapa innehåll, publicera, följa upp, optimera och experimentera. I rollen drar du stor nytta av ditt strategiska sinnelag och du kommer ges utrymme att komma med idéer och delta i förändringsarbete kopplat till avdelningens utveckling.  Dina arbetsuppgifter kommer bland annat bestå av att:   Ta ansvar från idé till publicering av innehåll som till exempel webbinarium, white papers, artiklar, kundcase, nyhetsinlägg och email utskick.  Utveckla och optimera copy för hemsida, sociala medier, landningssidor, betald marknadsföring och marknadsmaterial.  Implementera och följa upp content som skapas i Sverige till Intersolias andra kontor i Danmark, Tyskland och UK.  Bana väg och rusta upp inför en redan påbörjad tillväxtresa inom Inbound marketing.  Vara delaktig i att skapa processer och nya strategier för ett i framtiden växande team men också bana väg för att nå företagets expansionsmål.  I samarbete med dina kollegor kommer du få möjlighet att delta i arbetet med skapande av kampanjer och/eller andra områden du är nyfiken på. Bakgrund  Vi söker dig som har arbetat några år och då helst inom copywriting/content marketing eller varför inte på byrå inom SEO? Du har ett driv och brinner för att skapa riktat content till ett flertal olika målgrupper så att rätt budskap når rätt person vid rätt tillfälle i köpresan. Kombinationen av syfte, kanal, målgrupp och tonalitet gör din content till din expertis! Vi lägger stor vikt vid din förmåga att skriva och ”måla” med ord. Vidare har du även ett affärstänk, är innovativ och målinriktad.  Vi söker dig som   Har minst 4 års arbetslivserfarenhet inom copywriting eller content marketing  Har jobbat med B2B content  Arbetar självständigt och strukturerat med att skapa kvalitativt och engagerande innehåll likväl på svenska som engelska  Har mycket goda kunskaper i svenska och engelska, både skriftligt och muntligt. Du har professionell erfarenhet av att skriva på engelska på en hög nivå.  Är skicklig på storytelling.  Driven av att utvecklas och nyfiken på att lära sig nytt.  Initiativtagande och drivande. Det är meriterande om du har erfarenhet av att arbeta i HubSpot, Adobe Suite och Googles produkter.  Om Intersolia  Intersolia har starten 1999 varit inriktat på att hjälpa företag att hantera sina kemikalier på ett säkrare och mer miljövänligt sätt. Idag finns vi i Sverige, Tyskland, UK och Danmark. Med ett brett utbud av konsulttjänster och webbaserade system, iChemistry, iPublisher och iDistributor för att hantera, skapa och distribuera kemikalier, erbjuder vi effektiva lösningar som hjälper våra kunder att säkerställa sitt arbete med att uppfylla aktuell lagstiftning – utifrån sina unika behov.  Övrig information  I den här rekryteringen samarbetar Intersolia med Barona. Tjänsten är en tillsvidaretjänst på heltid som börjar med en provanställning på 6 månader, med tillträde enligt överenskommelse.  Du kommer att utgå från något av Intersolias kontor i Halmstad, Göteborg eller Malmö. Intersolia erbjuder flexibla arbetstider och möjlighet att arbeta hemifrån någon/några dagar i veckan.  Intervjuer hålls löpande och tjänsten kan komma att tillsättas innan sista ansökningsdag, så skicka in din ansökan redan nu! Vid frågor om tjänsten är du välkommen att kontakta Emma Fitzsimons, emma.fitzsimons@barona.se  Varmt välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Content Manager** till Intersolia Göteborg! Är ... | x | x | 15 | 15 | [Content manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yDfK_MAc_L1d) |
| 2 | ...ntent Manager till Intersolia **Göteborg**! Är du en vass skribent och a... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 3 | ...solia Göteborg! Är du en vass **skribent** och affärsorienterad med expe... | x | x | 8 | 8 | [Skribent, **job-title**](http://data.jobtechdev.se/taxonomy/concept/bbKc_hAw_snP) |
| 4 | ...tort ansvar? Då är rollen som **Content Manager** något för dig!  På Intersolia... | x | x | 15 | 15 | [Content manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yDfK_MAc_L1d) |
| 5 | ...Arbetsuppgifter  I rollen som **Content Manager** på Intersolia kommer dina kun... | x | x | 15 | 15 | [Content manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yDfK_MAc_L1d) |
| 6 | ...ia kommer dina kunskaper inom **copywriting** till användning och du ansvar... |  | x |  | 11 | [Copywriter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7mFy_ghh_iL6) |
| 7 | ...ölja upp content som skapas i **Sverige** till Intersolias andra kontor... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 8 | ...olias andra kontor i Danmark, **Tyskland** och UK.  Bana väg och rusta u... |  | x |  | 8 | [Tyskland, **country**](http://data.jobtechdev.se/taxonomy/concept/G3M7_959_8Pp) |
| 9 | ...at några år och då helst inom **copywriting**/content marketing eller varfö... | x | x | 11 | 11 | [Copywriter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7mFy_ghh_iL6) |
| 10 | ...och då helst inom copywriting/**content marketing** eller varför inte på byrå ino... | x | x | 17 | 17 | [Content marketing, **skill**](http://data.jobtechdev.se/taxonomy/concept/qTCs_crJ_Lzw) |
| 11 | ...ller varför inte på byrå inom **SEO**? Du har ett driv och brinner ... | x | x | 3 | 3 | [Sökordsoptimering/SEO, **skill**](http://data.jobtechdev.se/taxonomy/concept/zYHW_Mtu_jqq) |
| 12 | ...års arbetslivserfarenhet inom **copywriting** eller content marketing  Har ... | x | x | 11 | 11 | [Copywriter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7mFy_ghh_iL6) |
| 13 | ...renhet inom copywriting eller **content marketing**  Har jobbat med B2B content  ... | x | x | 17 | 17 | [Content marketing, **skill**](http://data.jobtechdev.se/taxonomy/concept/qTCs_crJ_Lzw) |
| 14 | ...ent marketing  Har jobbat med **B2B** content  Arbetar självständig... | x | x | 3 | 3 | [B2B, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jrLT_gTj_vxj) |
| 15 | ...digt och strukturerat med att **skapa kvalitativt och engagerande innehåll** likväl på svenska som engelsk... | x |  |  | 42 | [utveckla digitalt innehåll, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6P3N_8vS_3Ba) |
| 16 | ...ngagerande innehåll likväl på **svenska** som engelska  Har mycket goda... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 17 | ...nnehåll likväl på svenska som **engelska**  Har mycket goda kunskaper i ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 18 | ...  Har mycket goda kunskaper i **svenska** och engelska, både skriftligt... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 19 | ... goda kunskaper i svenska och **engelska**, både skriftligt och muntligt... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 20 | ...ofessionell erfarenhet av att **skriva på engelska** på en hög nivå.  Är skicklig ... |  | x |  | 18 | [skriva på engelska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4Brb_ptA_Qjr) |
| 21 | ...l erfarenhet av att skriva på **engelska** på en hög nivå.  Är skicklig ... | x |  |  | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 22 | ...ar erfarenhet av att arbeta i **HubSpot**, Adobe Suite och Googles prod... | x |  |  | 7 | [Annonstjänster, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/cDgT_iEt_XKn) |
| 23 | ...nhet av att arbeta i HubSpot, **Adobe Suite** och Googles produkter.  Om In... | x |  |  | 11 | [Layout-Adobe Creative Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/qK2A_9oq_zx6) |
| 24 | ...ta i HubSpot, Adobe Suite och **Googles produkter**.  Om Intersolia  Intersolia h... | x |  |  | 17 | [produktkunskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FC5o_4w6_cLJ) |
| 25 | ...vänligt sätt. Idag finns vi i **Sverige**, Tyskland, UK och Danmark. Me... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 26 | ...ätt. Idag finns vi i Sverige, **Tyskland**, UK och Danmark. Med ett bret... |  | x |  | 8 | [Tyskland, **country**](http://data.jobtechdev.se/taxonomy/concept/G3M7_959_8Pp) |
| 27 | ...tgå från något av Intersolias **kontor** i Halmstad, Göteborg eller Ma... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 28 | ...något av Intersolias kontor i **Halmstad**, Göteborg eller Malmö. Inters... |  | x |  | 8 | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| 29 | ...ntersolias kontor i Halmstad, **Göteborg** eller Malmö. Intersolia erbju... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 30 | ...or i Halmstad, Göteborg eller **Malmö**. Intersolia erbjuder flexibla... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| | **Overall** | | | **145** | **324** | 145/324 = **45%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [skriva på engelska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4Brb_ptA_Qjr) |
| x |  |  | [utveckla digitalt innehåll, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6P3N_8vS_3Ba) |
| x | x | x | [Copywriter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7mFy_ghh_iL6) |
| x |  |  | [produktkunskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FC5o_4w6_cLJ) |
|  | x |  | [Tyskland, **country**](http://data.jobtechdev.se/taxonomy/concept/G3M7_959_8Pp) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Skribent, **job-title**](http://data.jobtechdev.se/taxonomy/concept/bbKc_hAw_snP) |
| x |  |  | [Annonstjänster, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/cDgT_iEt_XKn) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [B2B, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jrLT_gTj_vxj) |
|  | x |  | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
|  | x |  | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x |  |  | [Layout-Adobe Creative Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/qK2A_9oq_zx6) |
| x | x | x | [Content marketing, **skill**](http://data.jobtechdev.se/taxonomy/concept/qTCs_crJ_Lzw) |
| x | x | x | [Content manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yDfK_MAc_L1d) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [Sökordsoptimering/SEO, **skill**](http://data.jobtechdev.se/taxonomy/concept/zYHW_Mtu_jqq) |
| | | **8** | 8/19 = **42%** |