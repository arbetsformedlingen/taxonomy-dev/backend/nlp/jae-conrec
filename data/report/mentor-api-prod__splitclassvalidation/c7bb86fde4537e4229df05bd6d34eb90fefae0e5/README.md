# Results for 'c7bb86fde4537e4229df05bd6d34eb90fefae0e5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c7bb86fde4537e4229df05bd6d34eb90fefae0e5](README.md) | 1 | 622 | 5 | 1 | 13/65 = **20%** | 1/4 = **25%** |

## Source text

Utbildningen är Sveriges mest erkända för fibertekniker och från hösten 2021 är den utvecklad med nya kurser inom effektiv felavhjälpning och 5G/Smart City. Med ett mycket gott rykte i hela branschen är efterfrågan på utexaminerade studenter konstant hög och utbildningen ger värdefulla kunskaper för att snabbt komma in i yrkesrollen.    Utbildningen utgår från Sveriges två dominerande standarder; Robust Fiber och Telia/Skanova. Upplägget för utbildningen baseras på 2-5 dagar/vecka på plats samt självstudier. Utbildningen varieras med föreläsningar, laborationer och projektuppgifter som utförs enskilt eller i grupp.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Utbildningen är **Sveriges** mest erkända för fiberteknike... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 2 | ... är Sveriges mest erkända för **fibertekniker** och från hösten 2021 är den u... | x | x | 13 | 13 | [Fibertekniker/Optotekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/r6AA_UrV_qw3) |
| 3 | ...utvecklad med nya kurser inom **effektiv felavhjälpning** och 5G/Smart City. Med ett my... | x |  |  | 23 | [Reparation av elektronisk och optisk utrustning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/8sSY_xiV_vZT) |
| 4 | ...m effektiv felavhjälpning och **5G/Smart City**. Med ett mycket gott rykte i ... | x |  |  | 13 | [Elektronik-, tele- och datatekniskt arbete, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/rbPL_MYk_xDF) |
| 5 | ...n.    Utbildningen utgår från **Sveriges** två dominerande standarder; R... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **13** | **65** | 13/65 = **20%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Reparation av elektronisk och optisk utrustning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/8sSY_xiV_vZT) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Fibertekniker/Optotekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/r6AA_UrV_qw3) |
| x |  |  | [Elektronik-, tele- och datatekniskt arbete, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/rbPL_MYk_xDF) |
| | | **1** | 1/4 = **25%** |