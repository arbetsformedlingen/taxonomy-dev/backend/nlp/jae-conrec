# Results for '9dae89296b0a802f295f8b6c0a36246d261bb03b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9dae89296b0a802f295f8b6c0a36246d261bb03b](README.md) | 1 | 2619 | 28 | 22 | 173/235 = **74%** | 15/19 = **79%** |

## Source text

Backendutvecklare (Java 8, Java EE, JAXB, Oracle, JSON, SQL, XML), Malmö... Om konsultuppdraget  - Ort: Malmö, Göteborg, Visby, Stockholm, Västerås, Östersund eller Umeå - Uppdragslängd: Ca. 5 månader - Sista ansökningsdagen: ansök snarast - Omfattning: 100% - OBS! Det är viktigt att du uppfyller skall-kraven för att vi överhuvudtaget ska kunna offerera dig, annars förkastar beställaren vårt anbud omgående där både din tid resp. vår tid går till spillo.   Uppdragsbeskrivning  Kunden söker en Backendutvecklare för vidareutveckling av teknisk lösning. Dina arbetsuppgifter innebär att du ingår i ett team som arbetar med kundens förändring för att möta digitaliseringen.  Teamet arbetar tillsammans med att lösa uppgifter från ”ax till limpa” där olika discipliner såsom krav, utveckling, test, testautomation, dokumentation, release och driftsättning ingår. I arbete ingår även arbete med datatransformering. Arbetet bedrivs agilt med frekventa leveranser, stor flexibilitet och förändringsbenägenhet.  Obligatoriska kompetenser och erfarenhet (skallkrav):  Erfarenhet av följande verktyg:  - Java 8 - Java EE - JAXB, Oracle - JSON - SQL - XML   Meriterande kompetenser och erfarenhet (börkrav):  - Arbetslivserfarenhet av systemutveckling i agil utvecklingsmiljö, SAFe - Arbetslivserfarenhet av datatransformering   ______________________  Hur du kommer vidare  - Sök uppdraget genom denna annons - Lägg in ett CV i word-format - Vi återkopplar om något behöver kompletteras eller förtydligas. - Återkoppling sker vanligtvis från Kunden till oss inom 10 arbetsdagar från det att ansökningstiden utgått. Vi försöker återkoppla omgående till dig som kandidat snarast vi har ny information avseende din ansökan eller uppdraget. Skulle återkoppling dröja, vänligen kontakta oss genom att svara på bekräftande mailutskicket i samband med din ansökan.   På uppmaning från beställare/slutkunder vill de inte att vi lämnar ut information om dem. En annan anledning är att Shaya Solutions lägger stor mängd tid som man på förhand inte får betalt för såvida inte uppdraget tillsätts, och därav kan vara återhållsamma med informationsdelningen av naturliga skäl.  Inför en eventuell intervju meddelas du om vilken Kunden är i god tid.  Om Shaya Solutions  Konsult- och kompetenspartner inom IT, Management och Teknik.  Vi lägger ett stort fokus på kund-/konsultpartnernöjdhet och kvalité i våra leveranser och verkar idag på 13 orter i Sverige med utgångspunkt i Stockholm. Teamets motto är Ödmjukhet, Ihärdighet samt Flexibilitet.  Varmt välkommen att höra av dig vid frågor eller funderingar.  Annonsförsäljare undanbedes.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Backendutvecklare** (Java 8, Java EE, JAXB, Oracl... | x | x | 17 | 17 | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| 2 | Backendutvecklare (**Java** 8, Java EE, JAXB, Oracle, JSO... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 3 | Backendutvecklare (Java 8, **Java** EE, JAXB, Oracle, JSON, SQL, ... | x |  |  | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 4 | ...klare (Java 8, Java EE, JAXB, **Oracle**, JSON, SQL, XML), Malmö... Om... | x | x | 6 | 6 | [Oracle, affärssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/s6Ui_k2K_kf2) |
| 5 | ... Java EE, JAXB, Oracle, JSON, **SQL**, XML), Malmö... Om konsultupp... | x | x | 3 | 3 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 6 | ... EE, JAXB, Oracle, JSON, SQL, **XML**), Malmö... Om konsultuppdrage... | x | x | 3 | 3 | [XML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/s8Te_rH8_RMU) |
| 7 | ...AXB, Oracle, JSON, SQL, XML), **Malmö**... Om konsultuppdraget  - Ort... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 8 | .... Om konsultuppdraget  - Ort: **Malmö**, Göteborg, Visby, Stockholm, ... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 9 | ...nsultuppdraget  - Ort: Malmö, **Göteborg**, Visby, Stockholm, Västerås, ... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 10 | ... Ort: Malmö, Göteborg, Visby, **Stockholm**, Västerås, Östersund eller Um... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 11 | ..., Göteborg, Visby, Stockholm, **Västerås**, Östersund eller Umeå - Uppdr... | x | x | 8 | 8 | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| 12 | ..., Visby, Stockholm, Västerås, **Östersund** eller Umeå - Uppdragslängd: C... | x | x | 9 | 9 | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| 13 | ...lm, Västerås, Östersund eller **Umeå** - Uppdragslängd: Ca. 5 månade... | x | x | 4 | 4 | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| 14 | ...: ansök snarast - Omfattning: **100%** - OBS! Det är viktigt att du ... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 15 | ...sbeskrivning  Kunden söker en **Backendutvecklare** för vidareutveckling av tekni... | x | x | 17 | 17 | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| 16 | ...såsom krav, utveckling, test, **testautomation**, dokumentation, release och d... | x | x | 14 | 14 | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| 17 | ...ckling, test, testautomation, **dokumentation**, release och driftsättning in... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 18 | ...rbete med datatransformering. **Arbetet bedrivs agilt** med frekventa leveranser, sto... | x |  |  | 21 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 19 | ...enhet av följande verktyg:  - **Java** 8 - Java EE - JAXB, Oracle - ... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 20 | ...följande verktyg:  - Java 8 - **Java** EE - JAXB, Oracle - JSON - SQ... | x |  |  | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 21 | ...:  - Java 8 - Java EE - JAXB, **Oracle** - JSON - SQL - XML   Meritera... | x | x | 6 | 6 | [Oracle, affärssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/s6Ui_k2K_kf2) |
| 22 | ...va EE - JAXB, Oracle - JSON - **SQL** - XML   Meriterande kompetens... | x | x | 3 | 3 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 23 | ...- JAXB, Oracle - JSON - SQL - **XML**   Meriterande kompetenser och... | x | x | 3 | 3 | [XML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/s8Te_rH8_RMU) |
| 24 | ...):  - Arbetslivserfarenhet av **systemutveckling** i agil utvecklingsmiljö, SAFe... | x | x | 16 | 16 | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| 25 | ...arenhet av systemutveckling i **agil utvecklingsmiljö, SAFe** - Arbetslivserfarenhet av dat... | x |  |  | 27 | [Scaled Agile Framework, SAFe, **skill**](http://data.jobtechdev.se/taxonomy/concept/eTW8_AY5_tpe) |
| 26 | ...lt- och kompetenspartner inom **IT**, Management och Teknik.  Vi l... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 27 | ...och verkar idag på 13 orter i **Sverige** med utgångspunkt i Stockholm.... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 28 | ... i Sverige med utgångspunkt i **Stockholm**. Teamets motto är Ödmjukhet, ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| | **Overall** | | | **173** | **235** | 173/235 = **74%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| x | x | x | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| x |  |  | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| x | x | x | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| x | x | x | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| x |  |  | [Scaled Agile Framework, SAFe, **skill**](http://data.jobtechdev.se/taxonomy/concept/eTW8_AY5_tpe) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x | x | x | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x | x | x | [Oracle, affärssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/s6Ui_k2K_kf2) |
| x | x | x | [XML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/s8Te_rH8_RMU) |
| | | **15** | 15/19 = **79%** |