# Results for 'ac9c6f8c411eea6abb8ee1be5495ecec2b05bc09'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ac9c6f8c411eea6abb8ee1be5495ecec2b05bc09](README.md) | 1 | 5449 | 22 | 25 | 89/307 = **29%** | 6/27 = **22%** |

## Source text

2D Artist Do you like playing games? Do you understand what differentiates a good game from a great game? Can you see how YOU can have input with your expertise and experience? As a 2D artist at Relax Gaming, you should aim no smaller than to make the best casino games the world has ever seen. We are now expanding our team and we are looking for a creative individual with skills to amaze to join us. In this role you will create impactful artwork - creating characters, environments and game elements that stand out and catch the player’s eye. You will work closely with the Art Director, game producers, animators and game developers in a vibrant, fast paced and collaborative environment within a dedicated game development team. Our studio will have several game projects running simultaneously so expect the freedom to dynamically work with a wide variety of projects and art styles as well as contributing to several new and old IPs. As a game artist, you should have a profound understanding of shape and form, color theory and composition, perspective, proportions and scales. Be able to iterate on designs fast, make animated proof of concepts and produce highly detailed 2D renders for final implementation in the game. We are particularly keen to hear from artists who have a strong portfolio that demonstrate concept art skills, illustrations in a variety of art styles, game art assets creation and highly detailed final renders. We offer a nice office in central Malmö and an opportunity to work with other experienced game enthusiasts! Responsibilities: Producing 2D concept art in a variety of art styles in the early stages of development Create mood boards that can explain art styles and intentions behind your designs Create mock-ups and reference materials that game developers can use as a goal when implementing designs Create art assets, while keeping consistent visual language for a wide range of art styles Produce highly detailed final artwork for implementation in the game Actively participate in collaboration with studio’s art team Participate in game progress reviews and give/take feedback about the designs and implementation process Improve processes and best practices Learn about the online gaming industry and stay up to date on current trends and what is popular with players    Required skills and background: You have completed education, or have equivalent work experience, in visual arts An excellent understanding of graphic design and colour theory Excellent knowledge of lighting, perspective and composition Strong drawing and illustration skills, ability to visualize and design from abstract ideas Excellent knowledge of game art asset creation, preparing deliverables for animation and in-game implementation Skilled using industry standard digital painting software Eye for quality and detail Ability to present ideas and to receive/address constructive feedback, a positive team player that gladly speaks your mind Ability to work independently and as part of a team Excellent communication skills, self-driven, ongoing professional development, willingness to learn    Bonus points for… It's very desirable to have skills and knowledge from game art development. Passionate about games, with an interest in game-play mechanics Interest in advanced trends in digital art Animation, VFX or 3D skills    Apply with… Motivational letter with some words about yourself Resume and/or portfolio highlighting your previous work, education and experience    About Relax Gaming Relax Gaming Group was founded in 2010 with the goal of creating games for the modern iGaming landscape. Always staying true to the Relax core values - Driven, Adaptable, Supportive and Respectful - the recent and rapid expansion has been conceived in order to deliver unparalleled global reach. Via a quick one-time integration, Relax Gaming now provides access to a roster of 1500+ casino games and a diverse range of proprietary products, including Poker, Bingo, and its own rapidly expanding slot portfolio. The high-quality aggregated content is provided through its selected Silver Bullet (commercially represented) and Powered By Relax (commercially independent) partners. Regulated markets are also at the heart of its growth strategy, with both fully supported regulated markets as well as licences held in multiple jurisdictions. Relax Gaming has offices in Malta, Estonia, Serbia, Finland, Sweden, and Gibraltar - and game studios in Belgrade, Malmö and Stockholm. Learn more about us here. Life at Relax This role is placed in Malmö, Sweden. The Relax game studio in Malmö is perfectly located on Lilla Torg, right in the middle of the beautiful old parts of the city. We span across 2 floors with a terrace and plenty of space to “relax”. A real home from home. The studio is a five-minute walk from the central station and all types of shopping and restaurants can be found right outside our doorstep. Relax is at a very exciting growth stage as a company, however, we cherish our informal and flexible core atmosphere where self-initiative, taking pride in what you do and ambition to deliver the best results are key components. For the right person, there are endless opportunities for personal and professional development. We are starting with interviews right away, so be quick to apply! Please link to your portfolio when submitting your application. All applications are handled in full confidentiality.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **2D Artist** Do you like playing games? Do... | x | x | 9 | 9 | [2D artist/2D-grafiker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/t8wc_LMK_pCM) |
| 2 | ...xpertise and experience? As a **2D artist** at Relax Gaming, you should a... | x | x | 9 | 9 | [2D artist/2D-grafiker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/t8wc_LMK_pCM) |
| 3 | ...ou will work closely with the **Art Director**, game producers, animators an... | x | x | 12 | 12 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 4 | ...losely with the Art Director, **game producers**, animators and game developer... | x |  |  | 14 | [Producent: kultur, media, film, dataspel, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FcaP_vhz_Cuy) |
| 5 | ...Art Director, game producers, **animators** and game developers in a vibr... | x |  |  | 9 | [Animator/Animatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NmF5_vMj_Mjj) |
| 6 | ...game producers, animators and **game developers** in a vibrant, fast paced and ... | x |  |  | 15 | [Spelutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2TYW_oUc_vdg) |
| 7 | ...nvironment within a dedicated **game development** team. Our studio will have se... | x |  |  | 16 | [Spelutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2TYW_oUc_vdg) |
| 8 | ...ng portfolio that demonstrate **concept art** skills, illustrations in a va... | x | x | 11 | 11 | [Concept art, **skill**](http://data.jobtechdev.se/taxonomy/concept/SuJz_U6P_sjm) |
| 9 | ...led final renders. We offer a **nice** office in central Malmö and a... |  | x |  | 4 | [Nice, hänvisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/xHdj_p3H_7QM) |
| 10 | ...inal renders. We offer a nice **office** in central Malmö and an oppor... |  | x |  | 6 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 11 | ...ffer a nice office in central **Malmö** and an opportunity to work wi... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 12 | ...esponsibilities: Producing 2D **concept art** in a variety of art styles in... | x | x | 11 | 11 | [Concept art, **skill**](http://data.jobtechdev.se/taxonomy/concept/SuJz_U6P_sjm) |
| 13 | ... art team Participate in game **progress** reviews and give/take feedbac... | x | x | 8 | 8 | [Progress, databashanterare, **skill**](http://data.jobtechdev.se/taxonomy/concept/d3Sd_Hpf_T2x) |
| 14 | ...An excellent understanding of **graphic design** and colour theory Excellent k... | x | x | 14 | 14 | [Grafisk design, **skill**](http://data.jobtechdev.se/taxonomy/concept/dTK5_R7G_p56) |
| 15 | ...omposition Strong drawing and **illustration** skills, ability to visualize ... |  | x |  | 12 | [Teknisk illustration, **skill**](http://data.jobtechdev.se/taxonomy/concept/2dAT_mTz_Meb) |
| 15 | ...omposition Strong drawing and **illustration** skills, ability to visualize ... |  | x |  | 12 | [Illustration, **skill**](http://data.jobtechdev.se/taxonomy/concept/E44v_wYZ_wjx) |
| 15 | ...omposition Strong drawing and **illustration** skills, ability to visualize ... | x |  |  | 12 | [Illustration, **keyword**](http://data.jobtechdev.se/taxonomy/concept/cC1g_uMb_Lyh) |
| 16 | ...n, preparing deliverables for **animation** and in-game implementation Sk... | x |  |  | 9 | [Animering, **skill**](http://data.jobtechdev.se/taxonomy/concept/dwm2_1V3_MpP) |
| 17 | ...quality and detail Ability to **present** ideas and to receive/address ... |  | x |  | 7 | [Present, hänvisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/6fA8_Pag_kiG) |
| 18 | ...y speaks your mind Ability to **work independently** and as part of a team Excelle... | x |  |  | 18 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 19 | ...dvanced trends in digital art **Animation**, VFX or 3D skills    Apply wi... | x |  |  | 9 | [Animering, **skill**](http://data.jobtechdev.se/taxonomy/concept/dwm2_1V3_MpP) |
| 20 | ...nds in digital art Animation, **VFX** or 3D skills    Apply with… M... | x |  |  | 3 | [Visual Effects Artist/VFX Artist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XHRR_4kg_Upi) |
| 21 | ...oprietary products, including **Poker**, Bingo, and its own rapidly e... |  | x |  | 5 | [Poker, croupierutbildad, **skill**](http://data.jobtechdev.se/taxonomy/concept/sKWc_1r1_yve) |
| 22 | ...ry products, including Poker, **Bingo**, and its own rapidly expandin... |  | x |  | 5 | [Bingo, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/KGfG_5HT_T1c) |
| 23 | ..., including Poker, Bingo, and **its** own rapidly expanding slot po... |  | x |  | 3 | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| 24 | ...d content is provided through **its** selected Silver Bullet (comme... |  | x |  | 3 | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| 25 | ...kets are also at the heart of **its** growth strategy, with both fu... |  | x |  | 3 | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| 26 | .... Relax Gaming has offices in **Malta**, Estonia, Serbia, Finland, Sw... |  | x |  | 5 | [Malta, **country**](http://data.jobtechdev.se/taxonomy/concept/u8qF_qpq_R5W) |
| 26 | .... Relax Gaming has offices in **Malta**, Estonia, Serbia, Finland, Sw... |  | x |  | 5 | [Malta, **region**](http://data.jobtechdev.se/taxonomy/concept/yRWq_iuL_6w8) |
| 27 | ...ta, Estonia, Serbia, Finland, **Sweden**, and Gibraltar - and game stu... | x |  |  | 6 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 28 | ... Serbia, Finland, Sweden, and **Gibraltar** - and game studios in Belgrad... |  | x |  | 9 | [Gibraltar, **country**](http://data.jobtechdev.se/taxonomy/concept/9Lj6_Lrk_83M) |
| 29 | ...and game studios in Belgrade, **Malmö** and Stockholm. Learn more abo... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 30 | ...tudios in Belgrade, Malmö and **Stockholm**. Learn more about us here. Li... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 31 | ... Relax This role is placed in **Malmö**, Sweden. The Relax game studi... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 32 | ...This role is placed in Malmö, **Sweden**. The Relax game studio in Mal... | x |  |  | 6 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 33 | ...den. The Relax game studio in **Malmö** is perfectly located on Lilla... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 34 | ...are endless opportunities for **personal** and professional development.... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| | **Overall** | | | **89** | **307** | 89/307 = **29%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Spelutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2TYW_oUc_vdg) |
|  | x |  | [Teknisk illustration, **skill**](http://data.jobtechdev.se/taxonomy/concept/2dAT_mTz_Meb) |
|  | x |  | [Present, hänvisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/6fA8_Pag_kiG) |
|  | x |  | [Gibraltar, **country**](http://data.jobtechdev.se/taxonomy/concept/9Lj6_Lrk_83M) |
|  | x |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [Illustration, **skill**](http://data.jobtechdev.se/taxonomy/concept/E44v_wYZ_wjx) |
| x | x | x | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| x |  |  | [Producent: kultur, media, film, dataspel, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FcaP_vhz_Cuy) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [Bingo, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/KGfG_5HT_T1c) |
| x |  |  | [Animator/Animatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NmF5_vMj_Mjj) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Concept art, **skill**](http://data.jobtechdev.se/taxonomy/concept/SuJz_U6P_sjm) |
| x |  |  | [Visual Effects Artist/VFX Artist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XHRR_4kg_Upi) |
| x |  |  | [Illustration, **keyword**](http://data.jobtechdev.se/taxonomy/concept/cC1g_uMb_Lyh) |
| x | x | x | [Progress, databashanterare, **skill**](http://data.jobtechdev.se/taxonomy/concept/d3Sd_Hpf_T2x) |
| x | x | x | [Grafisk design, **skill**](http://data.jobtechdev.se/taxonomy/concept/dTK5_R7G_p56) |
| x |  |  | [Animering, **skill**](http://data.jobtechdev.se/taxonomy/concept/dwm2_1V3_MpP) |
|  | x |  | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
|  | x |  | [Poker, croupierutbildad, **skill**](http://data.jobtechdev.se/taxonomy/concept/sKWc_1r1_yve) |
| x | x | x | [2D artist/2D-grafiker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/t8wc_LMK_pCM) |
|  | x |  | [Malta, **country**](http://data.jobtechdev.se/taxonomy/concept/u8qF_qpq_R5W) |
|  | x |  | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
|  | x |  | [Nice, hänvisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/xHdj_p3H_7QM) |
|  | x |  | [Malta, **region**](http://data.jobtechdev.se/taxonomy/concept/yRWq_iuL_6w8) |
| | | **6** | 6/27 = **22%** |