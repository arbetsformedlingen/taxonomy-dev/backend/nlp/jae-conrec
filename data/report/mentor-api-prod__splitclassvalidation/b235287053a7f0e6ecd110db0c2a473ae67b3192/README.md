# Results for 'b235287053a7f0e6ecd110db0c2a473ae67b3192'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b235287053a7f0e6ecd110db0c2a473ae67b3192](README.md) | 1 | 1858 | 12 | 11 | 66/184 = **36%** | 4/12 = **33%** |

## Source text

Frisör IDANA Beauty  Vi söker en positiv och glad person som har stor passion för yrket, och brinner för att utvecklas tillsammans med oss. Du ska kunna grunderna i frisöryrket dam och ett stort plus om du kan hårförlängning.  Service är viktigt och därför vill vi ge våra kunder bästa möjliga upplevelsen hos oss. Vi har en hög kundnöjdhet och växer som salong och söker därför en person som har hög ambitionsnivå. Stor vikt läggs vid personliga egenskaper vid rekrytering. Hos oss erbjuds du även interna utbildningar inom olika hårförlängning tekniker samt hur du utvecklas som frisör.  Har du det som krävs för att bli vår nästa frisör?  Du ska ha intresse för frisering Du har en positiv inställning och nära till skratt. Du är ansvarstagande, lojal, flexibel och gillar att arbeta självständigt. Du kan bygga och utveckla starka kundrelationer. Du är kreativ och serviceinriktad med kunden i fokus. Du är noggrann och håller ordning och reda. Du gillar att arbeta mot högt uppsatta mål.    Våran Salong & Butik finns i Skärholmen Centrum. Vi söker både heltid och deltid. Tillträde enligt överenskommelse. Lite om oss! IDANA Beauty är en expanderande Frisör salong  som brinner för skönhet och hälsa. Vi är även specialister på löshår och har mer än 15 års erfarenhet inom hårförlängning. Vi erbjuder även äkta kvalitets löshår och exklusiva skönhetsprodukter direkt i webbshop eller i våran salong & butik. Kolla gärna in våran Instagram: https://www.instagram.com/idana_beauty/ Skicka ditt CV till angelina@idana.se Adress: Bredholmsgatan 3,127 48 SKÄRHOLMEN Vid frågor angående tjänsten så kan ni kontakta oss på följande: Tel: 0738004928 eller mejla era frågor till angelina@idana.se #jobbjustnu Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Frisör** IDANA Beauty  Vi söker en pos... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 2 | ... och ett stort plus om du kan **hårförlängning**.  Service är viktigt och därf... | x |  |  | 14 | [styla hår, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sQfi_ypW_X6x) |
| 3 | ...ker samt hur du utvecklas som **frisör**.  Har du det som krävs för at... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 4 | ...m krävs för att bli vår nästa **frisör**?  Du ska ha intresse för fris... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 5 | ...g och nära till skratt. Du är **ansvarstagande**, lojal, flexibel och gillar a... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 6 | ...ojal, flexibel och gillar att **arbeta självständigt**. Du kan bygga och utveckla st... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 7 | ... arbeta självständigt. Du kan **bygga och utveckla starka kundrelationer**. Du är kreativ och serviceinr... | x |  |  | 40 | [upprätthålla kundrelationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LBLP_5VX_s6T) |
| 8 | ...kan bygga och utveckla starka **kundrelationer**. Du är kreativ och serviceinr... |  | x |  | 14 | [Kundrelationer, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hPWL_HNB_UFP) |
| 9 | ...tad med kunden i fokus. Du är **noggrann** och håller ordning och reda. ... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 10 | ...n i fokus. Du är noggrann och **håller ordning och reda**. Du gillar att arbeta mot hög... | x |  |  | 23 | [hålla arbetsområdet rent, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6Kri_s2M_pdm) |
| 11 | ...psatta mål.    Våran Salong & **Butik** finns i Skärholmen Centrum. V... |  | x |  | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 12 | ...holmen Centrum. Vi söker både **heltid** och deltid. Tillträde enligt ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 13 | ...rum. Vi söker både heltid och **deltid**. Tillträde enligt överenskomm... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 14 | ...ANA Beauty är en expanderande **Frisör** salong  som brinner för skönh... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 15 | ...  som brinner för skönhet och **hälsa**. Vi är även specialister på l... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 16 | ...bbshop eller i våran salong & **butik**. Kolla gärna in våran Instagr... |  | x |  | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| | **Overall** | | | **66** | **184** | 66/184 = **36%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [hålla arbetsområdet rent, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6Kri_s2M_pdm) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [upprätthålla kundrelationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LBLP_5VX_s6T) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Kundrelationer, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hPWL_HNB_UFP) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [styla hår, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sQfi_ypW_X6x) |
| x | x | x | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
|  | x |  | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| | | **4** | 4/12 = **33%** |