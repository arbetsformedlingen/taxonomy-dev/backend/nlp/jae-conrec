# Results for '3e301624d4de621004aad762ba2b9f4fdf8fc4bc'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [3e301624d4de621004aad762ba2b9f4fdf8fc4bc](README.md) | 1 | 2691 | 31 | 13 | 73/268 = **27%** | 8/18 = **44%** |

## Source text

Senior Webutvecklare - fullstack med fokus på frontend Är du en Fullstack-utvecklare som har extra passion för Frontend och design?  Vi söker nu ytterligare en medarbetare till ett av våra utvecklingsteam i Stockholm. Hos oss får du arbeta med några av Sveriges största företag där vi tillsammans utvecklar smarta produkter i en inspirerande miljö.  Som Fullstackutvecklare på Coor kommer du ingå i ett agilt och prestigelöst team med erfarna utvecklare. Tillsammans jobbar ni med att bygga Coors digitala framtid. Hos oss blir du en del av en tillväxtresa där du har stora möjligheter att forma, designa och påverka våra tjänster.  Hos oss kommer du trivas om du gillar att  - Koda och bygga UI  - Utveckla, testa och produktionssätta kod  - Automatisera så mycket som möjligt med CI/CD pipelines och smart monitorering  - Jobba med UX och design  De tekniker vi i nuläget jobbar med vid nyutveckling är bl.a.; .Net Core 6, React, NextJS, SQL, Docker, Redis, OpenID/Oauth och Azure. Vi använder Figma för att skapa och validera UX-prototyper.  För att passa vårt team tror vi att du  - Är strukturerad och initiativrik  - Ser dig själv som en lagspelare som gillar att dela kunskap med dina kollegor  - Gillar att arbeta i en agil miljö  Bra att veta om tjänsten  Denna roll arbetar i nära samarbete med IT Service Manager, IT Service Specialister, representanter från verksamheten och kommer delta i både strategiska projekt såväl som mindre initiativ. Dessutom är rollen en del av ett tvärfunktionellt team där det finns kollegor att samarbeta med inom till exempel kvalitetssäkring, säkerhet och integration.  Tjänsten är på heltid med placering på vårt kontor i Stockholm, Kista. Remote-arbete är möjligt utifrån en hybridmodell, beroende på teamets preferenser.  Kontakt  För mer information om tjänsten är du välkommen att kontakta Elin Kothe på elin.kothe@coor.com alt Thobias Björk på thobias.bjork@coor.com.  Vi ser fram emot din ansökan!   Om Coor Coor är Nordens ledande facility management-leverantör och är med och skapar Nordens bästa arbetsmiljöer – smarta, hållbara och fulla av glädje. Vi erbjuder alla de tjänster som krävs för att ett företag eller offentlig verksamhet ska fungera bra och effektivt. Vi strävar ständigt efter att bygga team och servicelösningar som gör det möjligt för våra kunder att fokusera på vad de gör bäst. Coor är verksamt främst i Norden, men har även viss närvaro i Belgien och Estland. Hos oss arbetar fler än 12 000 personer med olika bakgrunder och erfarenheter. Under 2022 blev Coor utnämnda till att vara det mest jämställda företaget i Sverige enligt SHE Index powered by EY och vi är fortsatt ett karriärföretag enligt Karriärföretagen.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Senior **Webutvecklare** - fullstack med fokus på fron... | x | x | 13 | 13 | [webbutvecklare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/AkVP_op7_dSC) |
| 2 | Senior Webutvecklare - **fullstack** med fokus på frontend Är du e... | x |  |  | 9 | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
| 3 | ...lare - fullstack med fokus på **frontend** Är du en Fullstack-utvecklare... | x | x | 8 | 8 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 4 | ...ed fokus på frontend Är du en **Fullstack-utvecklare** som har extra passion för Fro... | x | x | 20 | 20 | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
| 5 | ...are som har extra passion för **Frontend** och design?  Vi söker nu ytte... |  | x |  | 8 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 6 | ...ett av våra utvecklingsteam i **Stockholm**. Hos oss får du arbeta med nå... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 7 | ...i en inspirerande miljö.  Som **Fullstackutvecklare** på Coor kommer du ingå i ett ... |  | x |  | 19 | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
| 8 | ...du trivas om du gillar att  - **Koda** och bygga UI  - Utveckla, tes... | x |  |  | 4 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 9 | ... om du gillar att  - Koda och **bygga UI**  - Utveckla, testa och produk... | x |  |  | 8 | [UI designer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/GdC7_Bzv_S21) |
| 10 | ...r att  - Koda och bygga UI  - **Utveckla, testa och produktionssätta kod**  - Automatisera så mycket som... | x |  |  | 40 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 11 | ...era så mycket som möjligt med **CI**/CD pipelines och smart monito... | x | x | 2 | 2 | [Continuous integration/CI, **skill**](http://data.jobtechdev.se/taxonomy/concept/xT8A_n4a_Zji) |
| 12 | ...a så mycket som möjligt med CI**/CD** pipelines och smart monitorer... |  | x |  | 3 | [Continuous integration/CI, **skill**](http://data.jobtechdev.se/taxonomy/concept/xT8A_n4a_Zji) |
| 13 | ...art monitorering  - Jobba med **UX** och design  De tekniker vi i ... | x |  |  | 2 | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| 14 | ...ckling är bl.a.; .Net Core 6, **React**, NextJS, SQL, Docker, Redis, ... | x |  |  | 5 | [React native, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/TdnY_1gB_9pZ) |
| 15 | ...; .Net Core 6, React, NextJS, **SQL**, Docker, Redis, OpenID/Oauth ... | x | x | 3 | 3 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 16 | ...t Core 6, React, NextJS, SQL, **Docker**, Redis, OpenID/Oauth och Azur... | x | x | 6 | 6 | [Docker, container-plattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZipR_ckJ_YQC) |
| 17 | ...cker, Redis, OpenID/Oauth och **Azure**. Vi använder Figma för att sk... | x | x | 5 | 5 | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| 18 | ...ivrik  - Ser dig själv som en **lagspelare** som gillar att dela kunskap m... | x |  |  | 10 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 19 | ... en lagspelare som gillar att **dela kunskap med dina kollegor**  - Gillar att arbeta i en agi... | x |  |  | 30 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 20 | ... arbetar i nära samarbete med **IT** Service Manager, IT Service S... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 21 | ...nära samarbete med IT Service **Manager**, IT Service Specialister, rep... | x |  |  | 7 | [Manager, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/X9jv_K2b_m48) |
| 22 | ...rbete med IT Service Manager, **IT Service Specialister**, representanter från verksamh... | x |  |  | 23 | [Övriga IT-specialister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/UxT1_tPF_Kbg) |
| 23 | ...marbeta med inom till exempel **kvalitetssäkring**, säkerhet och integration.  T... | x | x | 16 | 16 | [Kvalitetssäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/a1Sf_mey_gVk) |
| 24 | ...ed placering på vårt kontor i **Stockholm**, Kista. Remote-arbete är möjl... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 25 | ...t mest jämställda företaget i **Sverige** enligt SHE Index powered by E... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **73** | **268** | 73/268 = **27%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
| x |  |  | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| x | x | x | [webbutvecklare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/AkVP_op7_dSC) |
|  | x |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| x |  |  | [UI designer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/GdC7_Bzv_S21) |
| x | x | x | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| x |  |  | [React native, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/TdnY_1gB_9pZ) |
| x |  |  | [Övriga IT-specialister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/UxT1_tPF_Kbg) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Manager, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/X9jv_K2b_m48) |
| x | x | x | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| x | x | x | [Docker, container-plattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZipR_ckJ_YQC) |
| x | x | x | [Kvalitetssäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/a1Sf_mey_gVk) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x | x | x | [Continuous integration/CI, **skill**](http://data.jobtechdev.se/taxonomy/concept/xT8A_n4a_Zji) |
| | | **8** | 8/18 = **44%** |