# Results for 'ce4d62ca1c5a086c8f109d388ca79059e330539e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ce4d62ca1c5a086c8f109d388ca79059e330539e](README.md) | 1 | 202 | 2 | 1 | 0/30 = **0%** | 0/3 = **0%** |

## Source text

Kockar till sommar i Roslagen Kom och jobba en sommar i härliga Roslagen. Vi söker både dig med erfarenhet och dig som är ny i vår härliga bransch. Vid intresse finns även chans till heltidsanställning.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kockar** till sommar i Roslagen Kom oc... | x |  |  | 6 | [Kock, **job-title**](http://data.jobtechdev.se/taxonomy/concept/RTQf_1Qm_YhD) |
| 1 | **Kockar** till sommar i Roslagen Kom oc... |  | x |  | 6 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 2 | ...ntresse finns även chans till **heltidsanställning**. | x |  |  | 18 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **0** | **30** | 0/30 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Kock, **job-title**](http://data.jobtechdev.se/taxonomy/concept/RTQf_1Qm_YhD) |
|  | x |  | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| | | **0** | 0/3 = **0%** |