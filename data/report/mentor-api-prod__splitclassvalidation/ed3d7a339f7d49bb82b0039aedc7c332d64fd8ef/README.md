# Results for 'ed3d7a339f7d49bb82b0039aedc7c332d64fd8ef'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ed3d7a339f7d49bb82b0039aedc7c332d64fd8ef](README.md) | 1 | 1649 | 12 | 12 | 70/182 = **38%** | 8/16 = **50%** |

## Source text

Stockholms bästa kockar till skola och förskola sökes. Är du trött på att jobba kvällar och helger? Känner du att du har fastnat i din utveckling? Vill du kunna jobba just de dagar som passar dig? Då kan detta vara något för dig!  Kvalifikationer  Vi söker dig som har 3 års restaurangskola eller relevant lärlingsutbildning och några år i branschen. Du har kunskap och utbildning i specialkost, egenkontroll och livsmedelshygien (HACCP).  Du är är social och trevlig, ansvarstagande och organiserad. Du trivs att arbeta hos olika kunder både i kortare perioder samt vid längre vikariat. Du gillar ditt arbete och du har lätt för att jobba med nya människor i olika miljöer.  För denna tjänst är det ett krav att du talar flytande svenska.  Urval och intervjuer sker löpande och tjänsten kan komma att tillsättas före sista ansökningsdag. Blir du kallad till intervju vill vi att du kan uppvisa ett nytt registerutdrag från polisen. Du kan skicka efter ett sådant via denna länk  https://polisen.se/tjanster-ti... (https://polisen.se/tjanster-tillstand/belastningsregistret/skola-eller-forskola/)  Observera att tjänsten är en behovsanställning.  RestaurangAssistans är Sveriges största bemanningsföretag inom Hotell och Restaurang med kontor i Stockholm, Malmö, Göteborg och Oslo. RestaurangAssistans är ett auktoriserat bemanningsföretag och vi har kollektivavtal med HRF/Visita. Våra kunder är allt från erkänt bra innerstadskrogar och guldkantade galor till rikstäckande hotell- och företagsrestauranger, konferensanläggningar, kursgårdar och cateringföretag.  Ansök endast via länken nedan. Vi tar inte emot ansökningar via epost eller telefon.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Stockholms** bästa kockar till skola och f... | x |  |  | 10 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 2 | Stockholms bästa **kockar** till skola och förskola sökes... |  | x |  | 6 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 3 | ...s bästa kockar till skola och **förskola** sökes. Är du trött på att job... | x | x | 8 | 8 | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| 4 | ... har kunskap och utbildning i **specialkost**, egenkontroll och livsmedelsh... | x | x | 11 | 11 | [Specialkost, **skill**](http://data.jobtechdev.se/taxonomy/concept/JYKn_PRX_tif) |
| 5 | ...specialkost, egenkontroll och **livsmedelshygien** (HACCP).  Du är är social och... | x | x | 16 | 16 | [Livsmedelshygien, **skill**](http://data.jobtechdev.se/taxonomy/concept/kj4Q_nLh_u8Q) |
| 6 | ... Du är är social och trevlig, **ansvarstagande** och organiserad. Du trivs att... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 7 | ...tare perioder samt vid längre **vikariat**. Du gillar ditt arbete och du... | x |  |  | 8 | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| 8 | ...tt krav att du talar flytande **svenska**.  Urval och intervjuer sker l... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 9 | ... Observera att tjänsten är en **behovsanställning**.  RestaurangAssistans är Sver... | x |  |  | 17 | [Behovsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/1paU_aCR_nGn) |
| 10 | ...ten är en behovsanställning.  **Restaurang**Assistans är Sveriges största ... |  | x |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 11 | ...törsta bemanningsföretag inom **Hotell** och Restaurang med kontor i S... |  | x |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 12 | ...törsta bemanningsföretag inom **Hotell och Restaurang** med kontor i Stockholm, Malmö... | x |  |  | 21 | [Hotell och restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/F5kB_F4h_yc5) |
| 13 | ...nom Hotell och Restaurang med **kontor** i Stockholm, Malmö, Göteborg ... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 14 | ...l och Restaurang med kontor i **Stockholm**, Malmö, Göteborg och Oslo. Re... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 15 | ...urang med kontor i Stockholm, **Malmö**, Göteborg och Oslo. Restauran... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 16 | ...ed kontor i Stockholm, Malmö, **Göteborg** och Oslo. RestaurangAssistans... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 17 | ... bemanningsföretag och vi har **kollektivavtal** med HRF/Visita. Våra kunder ä... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 18 | ...ntade galor till rikstäckande **hotell**- och företagsrestauranger, ko... | x |  |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| | **Overall** | | | **70** | **182** | 70/182 = **38%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Behovsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/1paU_aCR_nGn) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Hotell och restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/F5kB_F4h_yc5) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [Specialkost, **skill**](http://data.jobtechdev.se/taxonomy/concept/JYKn_PRX_tif) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
|  | x |  | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
|  | x |  | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| x | x | x | [Livsmedelshygien, **skill**](http://data.jobtechdev.se/taxonomy/concept/kj4Q_nLh_u8Q) |
|  | x |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
|  | x |  | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x | x | x | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/16 = **50%** |