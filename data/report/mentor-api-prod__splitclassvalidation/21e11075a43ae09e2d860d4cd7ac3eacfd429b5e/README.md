# Results for '21e11075a43ae09e2d860d4cd7ac3eacfd429b5e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [21e11075a43ae09e2d860d4cd7ac3eacfd429b5e](README.md) | 1 | 2241 | 18 | 41 | 115/387 = **30%** | 10/26 = **38%** |

## Source text

Takmontör till Protan Entreprenad  Om tjänsten Vi hjälper Protan Entreprenad AB att hitta dig som kan tak och vill bli en del av ett härligt och drivet gäng!  Som takmontör på Protan Entreprenad AB blir du en del av ett stort internationellt företag med norska ägare och totalt cirka 900 anställda i 13 länder. Protan Entreprenad ägs av Protan AS, som är en internationellt erkänd producent av membran för en rad olika ändamål, däribland miljövänlig takbeläggning.    Dina arbetsuppgifter i huvudsak Som takmontör kommer du att utgå i huvudsak från ditt hem och jobba med takläggning på projekt runt om i hela Stockholm. Vi söker dig som drivs av stark laganda och resultat. Arbetet utförs till största del i team om två eller tre personer.   Arbetet är stundtals fysiskt och det kan förekomma en del tunga lyft. Vi ser att du är flexibel med tanke på att arbetsplatserna varierar från projekt till projekt, samt att du är ansvarsfull, effektiv och noggrann i ditt arbete.   Vem är du? Kravbild •	Besitter en relevant arbetslivserfarenhet inom liknande roll  •	Har mycket goda kunskaper inom bygg- och fastighetsbranschen, meriterande är om du har jobbat med takentreprenader •	Har B-körkort •	Kan uttrycka dig i svenska och/eller engelska   Meriterande: •	Besitter erfarenhet av både duk och papp •	Besitter erfarenhet av isolering  Vi söker dig som arbetar strukturerat och effektivt. Du är gärna nyfiken, lyhörd och social. För att lyckas bra i rollen så är du kvalitetsmedveten, professionell och noga med att alltid göra ett bra arbete. Låter det som en beskrivning av dig? Tveka inte på att skicka in din ansökan!    Om verksamheten Protan Entreprenad AB med företag i Norge, Sverige och Danmark, är Nordens största takläggningsverksamhet. Protan Entreprenad AB är totalleverantörer inom tätning av tak och terrasser och levererar säkra lösningar för radontätning. Kvalitet, flexibilitet och tillförlitlighet är vårt varumärke. I denna rekrytering samarbetar Protan Entreprenad AB med Uniflex. Tjänsten är på heltid med direkt anställning hos Protan Entreprenad AB. Placeringsort: Stockholm   Urval och rekryteringsprocessen kommer pågå löpande under sommaren, så vänta inte med din ansökan. Tillträde är planerat så snart som möjligt.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Takmontör** till Protan Entreprenad  Om t... |  | x |  | 9 | [Takmontörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/3g5x_Tm7_8QH) |
| 1 | **Takmontör** till Protan Entreprenad  Om t... |  | x |  | 9 | [Takmontörer, golvläggare och VVS-montörer m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Av49_oZm_gKG) |
| 1 | **Takmontör** till Protan Entreprenad  Om t... |  | x |  | 9 | [Yrkesbevis, tak- och tätskiktsmontör, **skill**](http://data.jobtechdev.se/taxonomy/concept/GrwT_qSP_Jed) |
| 1 | **Takmontör** till Protan Entreprenad  Om t... |  | x |  | 9 | [Tak- och tätskiktsmontör, utan yrkesbevis, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Mj9N_W8j_bqg) |
| 1 | **Takmontör** till Protan Entreprenad  Om t... |  | x |  | 9 | [Tak- och tätskiktsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QM63_e85_CUC) |
| 1 | **Takmontör** till Protan Entreprenad  Om t... | x | x | 9 | 9 | [takmontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/SxqE_h7k_ZBn) |
| 1 | **Takmontör** till Protan Entreprenad  Om t... |  | x |  | 9 | [Takmontörer, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/aVx1_Ku5_DbH) |
| 1 | **Takmontör** till Protan Entreprenad  Om t... |  | x |  | 9 | [Lärling, tak- och tätskiktsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bETF_k3e_2U2) |
| 1 | **Takmontör** till Protan Entreprenad  Om t... |  | x |  | 9 | [Takmontörer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/sjoJ_aWB_NRS) |
| 2 | ...härligt och drivet gäng!  Som **takmontör** på Protan Entreprenad AB blir... |  | x |  | 9 | [Takmontörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/3g5x_Tm7_8QH) |
| 2 | ...härligt och drivet gäng!  Som **takmontör** på Protan Entreprenad AB blir... |  | x |  | 9 | [Takmontörer, golvläggare och VVS-montörer m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Av49_oZm_gKG) |
| 2 | ...härligt och drivet gäng!  Som **takmontör** på Protan Entreprenad AB blir... |  | x |  | 9 | [Yrkesbevis, tak- och tätskiktsmontör, **skill**](http://data.jobtechdev.se/taxonomy/concept/GrwT_qSP_Jed) |
| 2 | ...härligt och drivet gäng!  Som **takmontör** på Protan Entreprenad AB blir... |  | x |  | 9 | [Tak- och tätskiktsmontör, utan yrkesbevis, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Mj9N_W8j_bqg) |
| 2 | ...härligt och drivet gäng!  Som **takmontör** på Protan Entreprenad AB blir... |  | x |  | 9 | [Tak- och tätskiktsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QM63_e85_CUC) |
| 2 | ...härligt och drivet gäng!  Som **takmontör** på Protan Entreprenad AB blir... | x | x | 9 | 9 | [takmontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/SxqE_h7k_ZBn) |
| 2 | ...härligt och drivet gäng!  Som **takmontör** på Protan Entreprenad AB blir... |  | x |  | 9 | [Takmontörer, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/aVx1_Ku5_DbH) |
| 2 | ...härligt och drivet gäng!  Som **takmontör** på Protan Entreprenad AB blir... |  | x |  | 9 | [Lärling, tak- och tätskiktsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bETF_k3e_2U2) |
| 2 | ...härligt och drivet gäng!  Som **takmontör** på Protan Entreprenad AB blir... |  | x |  | 9 | [Takmontörer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/sjoJ_aWB_NRS) |
| 3 | ...t internationellt företag med **norska** ägare och totalt cirka 900 an... |  | x |  | 6 | [Norska, **language**](http://data.jobtechdev.se/taxonomy/concept/pnjj_2JX_Fub) |
| 3 | ...t internationellt företag med **norska** ägare och totalt cirka 900 an... |  | x |  | 6 | [norska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/x1hR_rDo_UUz) |
| 4 | ... är en internationellt erkänd **producent** av membran för en rad olika ä... |  | x |  | 9 | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| 5 | ...rbetsuppgifter i huvudsak Som **takmontör** kommer du att utgå i huvudsak... |  | x |  | 9 | [Takmontörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/3g5x_Tm7_8QH) |
| 5 | ...rbetsuppgifter i huvudsak Som **takmontör** kommer du att utgå i huvudsak... |  | x |  | 9 | [Takmontörer, golvläggare och VVS-montörer m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Av49_oZm_gKG) |
| 5 | ...rbetsuppgifter i huvudsak Som **takmontör** kommer du att utgå i huvudsak... |  | x |  | 9 | [Yrkesbevis, tak- och tätskiktsmontör, **skill**](http://data.jobtechdev.se/taxonomy/concept/GrwT_qSP_Jed) |
| 5 | ...rbetsuppgifter i huvudsak Som **takmontör** kommer du att utgå i huvudsak... |  | x |  | 9 | [Tak- och tätskiktsmontör, utan yrkesbevis, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Mj9N_W8j_bqg) |
| 5 | ...rbetsuppgifter i huvudsak Som **takmontör** kommer du att utgå i huvudsak... |  | x |  | 9 | [Tak- och tätskiktsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QM63_e85_CUC) |
| 5 | ...rbetsuppgifter i huvudsak Som **takmontör** kommer du att utgå i huvudsak... | x | x | 9 | 9 | [takmontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/SxqE_h7k_ZBn) |
| 5 | ...rbetsuppgifter i huvudsak Som **takmontör** kommer du att utgå i huvudsak... |  | x |  | 9 | [Takmontörer, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/aVx1_Ku5_DbH) |
| 5 | ...rbetsuppgifter i huvudsak Som **takmontör** kommer du att utgå i huvudsak... |  | x |  | 9 | [Lärling, tak- och tätskiktsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bETF_k3e_2U2) |
| 5 | ...rbetsuppgifter i huvudsak Som **takmontör** kommer du att utgå i huvudsak... |  | x |  | 9 | [Takmontörer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/sjoJ_aWB_NRS) |
| 6 | ...k från ditt hem och jobba med **takläggning** på projekt runt om i hela Sto... | x | x | 11 | 11 | [Takläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/3Zeh_4RX_kA4) |
| 7 | ...ing på projekt runt om i hela **Stockholm**. Vi söker dig som drivs av st... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 8 | ... och det kan förekomma en del **tunga lyft**. Vi ser att du är flexibel me... |  | x |  | 10 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 9 | ... till projekt, samt att du är **ansvarsfull**, effektiv och noggrann i ditt... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 10 | ... är ansvarsfull, effektiv och **noggrann** i ditt arbete.   Vem är du? K... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 11 | ...at med takentreprenader •	Har **B-körkort** •	Kan uttrycka dig i svenska ... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 12 | ...-körkort •	Kan uttrycka dig i **svenska** och/eller engelska   Meritera... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 13 | ...rycka dig i svenska och/eller **engelska**   Meriterande: •	Besitter erf... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 14 | ...•	Besitter erfarenhet av både **duk** och papp •	Besitter erfarenhe... | x |  |  | 3 | [takläggningsmetoder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/v7RE_J5L_2vA) |
| 15 | ...er erfarenhet av både duk och **papp** •	Besitter erfarenhet av isol... | x |  |  | 4 | [takläggningsmetoder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/v7RE_J5L_2vA) |
| 16 | ...papp •	Besitter erfarenhet av **isolering**  Vi söker dig som arbetar str... | x | x | 9 | 9 | [Isolering, **skill**](http://data.jobtechdev.se/taxonomy/concept/7jFS_6oq_ZGv) |
| 17 | ... Entreprenad AB med företag i **Norge**, Sverige och Danmark, är Nord... | x |  |  | 5 | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
| 18 | ...renad AB med företag i Norge, **Sverige** och Danmark, är Nordens störs... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 19 | ... företag i Norge, Sverige och **Danmark**, är Nordens största takläggni... | x |  |  | 7 | [Danmark, **country**](http://data.jobtechdev.se/taxonomy/concept/Co3h_1xq_Cwb) |
| 20 | ...B med Uniflex. Tjänsten är på **heltid** med direkt anställning hos Pr... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 21 | ...ntreprenad AB. Placeringsort: **Stockholm**   Urval och rekryteringsproce... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| | **Overall** | | | **115** | **387** | 115/387 = **30%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Takläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/3Zeh_4RX_kA4) |
|  | x |  | [Takmontörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/3g5x_Tm7_8QH) |
|  | x |  | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Isolering, **skill**](http://data.jobtechdev.se/taxonomy/concept/7jFS_6oq_ZGv) |
|  | x |  | [Takmontörer, golvläggare och VVS-montörer m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Av49_oZm_gKG) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Danmark, **country**](http://data.jobtechdev.se/taxonomy/concept/Co3h_1xq_Cwb) |
|  | x |  | [Yrkesbevis, tak- och tätskiktsmontör, **skill**](http://data.jobtechdev.se/taxonomy/concept/GrwT_qSP_Jed) |
|  | x |  | [Tak- och tätskiktsmontör, utan yrkesbevis, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Mj9N_W8j_bqg) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
|  | x |  | [Tak- och tätskiktsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QM63_e85_CUC) |
| x | x | x | [takmontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/SxqE_h7k_ZBn) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [Takmontörer, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/aVx1_Ku5_DbH) |
|  | x |  | [Lärling, tak- och tätskiktsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bETF_k3e_2U2) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [Norska, **language**](http://data.jobtechdev.se/taxonomy/concept/pnjj_2JX_Fub) |
|  | x |  | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
|  | x |  | [Takmontörer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/sjoJ_aWB_NRS) |
| x |  |  | [takläggningsmetoder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/v7RE_J5L_2vA) |
|  | x |  | [norska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/x1hR_rDo_UUz) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **10** | 10/26 = **38%** |