# Results for '4824fbf4bd5339f88c2bef671c10b01cdf937b8b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4824fbf4bd5339f88c2bef671c10b01cdf937b8b](README.md) | 1 | 720 | 5 | 5 | 58/58 = **100%** | 4/4 = **100%** |

## Source text

Sjuksköterska, bemanning Jämför och chatta med bemanningsbolagen enkelt via Vårdföretag App (vardforetag.se) Skapa din profil på mindre än 1 minut och lägg upp dina önskemål. Få erbjudanden på uppdrag som kan passa dina önskemål och chatta med flera av de ledande bemanningsbolagen inom vården. Enkelt och kostnadsfritt! Just nu finns ett ledigt uppdrag som sjuksköterska på hälsocentral i Karlshamn, Blekinge län. Uppdraget finns att söka bland flera bemanningsbolag i Vårdföretag.se   Fördelar: -Hög ersättning -Inga onödiga Mail Eller telefonsamtal -Enkelt och kostnadsfritt -Inget bindande   Vill du starta eget aktiebolag? Vi på vardforetag.se hjälper dig att starta aktiebolag och redovisning till fasta kostnader.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sjuksköterska**, bemanning Jämför och chatta ... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 2 | ... finns ett ledigt uppdrag som **sjuksköterska** på hälsocentral i Karlshamn, ... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 3 | ...uksköterska på hälsocentral i **Karlshamn**, Blekinge län. Uppdraget finn... | x | x | 9 | 9 | [Karlshamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/HtGW_WgR_dpE) |
| 4 | ... på hälsocentral i Karlshamn, **Blekinge län**. Uppdraget finns att söka bla... | x | x | 12 | 12 | [Blekinge län, **region**](http://data.jobtechdev.se/taxonomy/concept/DQZd_uYs_oKb) |
| 5 | ...dig att starta aktiebolag och **redovisning** till fasta kostnader. | x | x | 11 | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| | **Overall** | | | **58** | **58** | 58/58 = **100%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Blekinge län, **region**](http://data.jobtechdev.se/taxonomy/concept/DQZd_uYs_oKb) |
| x | x | x | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| x | x | x | [Karlshamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/HtGW_WgR_dpE) |
| x | x | x | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| | | **4** | 4/4 = **100%** |