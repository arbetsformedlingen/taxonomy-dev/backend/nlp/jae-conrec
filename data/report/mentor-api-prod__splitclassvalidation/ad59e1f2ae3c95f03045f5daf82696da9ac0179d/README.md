# Results for 'ad59e1f2ae3c95f03045f5daf82696da9ac0179d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ad59e1f2ae3c95f03045f5daf82696da9ac0179d](README.md) | 1 | 2462 | 16 | 16 | 60/254 = **24%** | 3/18 = **17%** |

## Source text

Vi söker truckförare (Skjutstativ) till vår kund i Jordbro - Heltid Känner du att du är redo för nästa steg i karriären? Vi söker en kille eller tjej som vill jobba som lagermedarbetare hos vår kund i Södra Stockholm Vi söker efter dig som är på jakt efter ny arbetslivserfarenhet. Du kanske vill tillbaka till logistikbranschen eller i dagsläget jobbar på ett lager du inte känner att du når din fulla potential på. Som lagermedarbetare arbetar du självständigt med att plocka och packa på lagret. Vår kund har ett automatiserat lager där du plockar och packar samt arbetar nära med godsmottagning och returavdelning. I detta arbete ingår det även att köra truck dagligen, främst skjutstativ. Det är ett varierande och spännande jobb där man har chansen att utvecklas utifrån sin vilja och kompetens. Arbetsuppgifter: · Plocka och packa ordrar · Returhantering · Hantering av inleveranser och godsmottagning. Om dig: Du gillar att arbeta fysiskt i ett högt tempo samtidigt som du har kunden i fokus och håller hög kvalité genom samtliga arbetsmoment. Du är en lagspelare som instruerar och inspirerar dina kollegor genom att föregå med gott exempel samt att du kommer med nya idéer och tankesätt. Du informerar och kommer med förslag till din teamleader kring möjligheter, problem och framsteg. Krav: · Truckkort A+B · Erfarenhet av lagerarbete  Meriterande: · Erfarenhet utav skjutstativtruck Bra att veta: Det går enkelt att ta sig till arbetsplatsen med kollektivtrafik Vi gör alltid en bakgrundskontroll på våra kandidater och vi kan begära utdrag ur belastningsregistret. Lagret är beläget i Södra Stockholm Arbetet är förlagt måndag - fredag 07:00 - 16:00. Helgarbete och övertid kan förekomma Om oss: Det är vi som är Simplex. Framtidens bemanningsföretag med siktet inställt på utveckling. För det är precis vad vi vill skapa, utveckling för företag och människor. Med rätt person, på rätt plats skapas nytta för alla parter. Idag ser ni våra konsulter köra truck på terminaler, leverera pallar med tunga fordon, leda logistiken på byggarbetsplatser eller packa orders på lager. Men det är bara en liten del av vad vi erbjuder. Hos oss arbetar människor som vill framåt och som driver på logistiken i hela Sverige. Svårare eller enklare behöver det inte vara.   Är du redo för nästa steg i karriären, öppnar vi dörrar till nya möjligheter. Vårt mål är att ta vara på varje individs unika kompetens och förädla med erfarenheter.   Välkommen till Simplex!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Vi söker **truckförare** (Skjutstativ) till vår kund i... | x |  |  | 11 | [Truckförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GQSf_fnq_kjF) |
| 1 | Vi söker **truckförare** (Skjutstativ) till vår kund i... |  | x |  | 11 | [truckförare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/RFLP_VeX_uCR) |
| 2 | ...iv) till vår kund i Jordbro - **Heltid** Känner du att du är redo för ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 3 | ...eller tjej som vill jobba som **lagermedarbetare** hos vår kund i Södra Stockhol... | x | x | 16 | 16 | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
| 4 | ...arbetare hos vår kund i Södra **Stockholm** Vi söker efter dig som är på ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 5 | ... Du kanske vill tillbaka till **logistikbranschen** eller i dagsläget jobbar på e... |  | x |  | 17 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 6 | ...ler i dagsläget jobbar på ett **lager** du inte känner att du når din... |  | x |  | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 7 | ...r din fulla potential på. Som **lagermedarbetare** arbetar du självständigt med ... | x | x | 16 | 16 | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
| 8 | ...tial på. Som lagermedarbetare **arbetar du självständigt** med att plocka och packa på l... | x |  |  | 24 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 9 | ...etar du självständigt med att **plocka** och packa på lagret. Vår kund... | x |  |  | 6 | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
| 10 | ...lvständigt med att plocka och **packa** på lagret. Vår kund har ett a... | x |  |  | 5 | [packa varor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5YMq_iHL_wB1) |
| 11 | ...år kund har ett automatiserat **lager** där du plockar och packar sam... |  | x |  | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 12 | ...tta arbete ingår det även att **köra truck** dagligen, främst skjutstativ.... | x |  |  | 10 | [köra gaffeltruck, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dDJ1_K9K_6aU) |
| 13 | ...rbete ingår det även att köra **truck** dagligen, främst skjutstativ.... |  | x |  | 5 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 14 | ...h kommer med förslag till din **teamleader** kring möjligheter, problem oc... | x | x | 10 | 10 | [Teamledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FC86_VDF_rqB) |
| 15 | ...problem och framsteg. Krav: · **Truckkort** A+B · Erfarenhet av lagerarbe... |  | x |  | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 16 | ...problem och framsteg. Krav: · **Truckkort A**+B · Erfarenhet av lagerarbete... | x |  |  | 11 | [A-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Zk4h_jP4_e85) |
| 17 | ...framsteg. Krav: · Truckkort A+**B** · Erfarenhet av lagerarbete  ... | x |  |  | 1 | [B-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/HeXq_amc_ABd) |
| 18 | ...steg. Krav: · Truckkort A+B · **Erfarenhet av lagerarbete**  Meriterande: · Erfarenhet ut... | x |  |  | 25 | [Terminal- och lagerarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/ijHY_aHk_ADE) |
| 19 | ...Truckkort A+B · Erfarenhet av **lagerarbete**  Meriterande: · Erfarenhet ut... |  | x |  | 11 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 20 | ...ta sig till arbetsplatsen med **kollektivtrafik** Vi gör alltid en bakgrundskon... |  | x |  | 15 | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| 21 | ...et. Lagret är beläget i Södra **Stockholm** Arbetet är förlagt måndag - f... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 22 | ...ag ser ni våra konsulter köra **truck** på terminaler, leverera palla... |  | x |  | 5 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 23 | ...platser eller packa orders på **lager**. Men det är bara en liten del... |  | x |  | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 24 | ...m driver på logistiken i hela **Sverige**. Svårare eller enklare behöve... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **60** | **254** | 60/254 = **24%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [packa varor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5YMq_iHL_wB1) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Teamledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FC86_VDF_rqB) |
| x |  |  | [Truckförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GQSf_fnq_kjF) |
| x |  |  | [B-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/HeXq_amc_ABd) |
|  | x |  | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [truckförare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/RFLP_VeX_uCR) |
|  | x |  | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
|  | x |  | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| x |  |  | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
| x |  |  | [A-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Zk4h_jP4_e85) |
| x |  |  | [köra gaffeltruck, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dDJ1_K9K_6aU) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Terminal- och lagerarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/ijHY_aHk_ADE) |
| x | x | x | [Lagerarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sizv_uPq_nWf) |
|  | x |  | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| | | **3** | 3/18 = **17%** |