# Results for 'ded39da0ee6ccaef66a37739a2398a5aad1bc15e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ded39da0ee6ccaef66a37739a2398a5aad1bc15e](README.md) | 1 | 1309 | 13 | 14 | 30/205 = **15%** | 1/12 = **8%** |

## Source text

Kyrkomusiker Svenska kyrkan Götene pastorat söker en kyrkomusiker. Tjänsten är heltid tillsvidaretjänst med arbete i Källby församling. Arbetet innebär även tjänstgöring på gudstjänster och kyrkliga handlingar i hela pastoratet enligt schema och efter behov. Vi söker dig som är behörig kyrkomusiker nivå C i Svenska kyrkan eller annan likvärdig utbildning och som vill bli en av sex kyrkomusiker i Götene pastorat. Du kommer att ha din utgångspunkt i Källby församling med placering och arbetsrum i Pastoratsgården Källby, där det finns ett arbetslag bestående av präst, diakonassistent, församlingsvärdinna och pedagog.  Tjänsten innebär ansvar för kyrkokör i Källby församling och utveckling av barnkör. Källby är en växande ort i pastoratet med många barnfamiljer och en grundskola för 0-12 år. Sommartid är Kinnekulle ett stort besöksmål. I området finns det även många fritidsboenden vilket påverkar det kyrkomusikaliska arbetet.  Vi vill ta vara på och uppmuntra dina personliga gåvor. Götene pastorat vill vara en arbetsgivare där man kan växa som medarbetare. Inför eventuell anställning kommer utdrag ur polisens belastningsregister att efterfrågas. Körkort och tillgång till egen bil är nödvändigt.  Tillträde: Enligt överenskommelse Sista ansökningsdag: 19 september 2022 - Intervjuer sker löpande

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kyrkomusiker** Svenska kyrkan Götene pastora... |  | x |  | 12 | [Musiker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/UCXb_LjK_pHv) |
| 1 | **Kyrkomusiker** Svenska kyrkan Götene pastora... | x |  |  | 12 | [Kantor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WoT4_oqQ_5Fa) |
| 2 | Kyrkomusiker **Svenska** kyrkan Götene pastorat söker ... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 3 | Kyrkomusiker Svenska **kyrkan** Götene pastorat söker en kyrk... | x |  |  | 6 | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
| 4 | Kyrkomusiker Svenska kyrkan **Götene** pastorat söker en kyrkomusike... |  | x |  | 6 | [Götene, **municipality**](http://data.jobtechdev.se/taxonomy/concept/txzq_PQY_FGi) |
| 5 | ...rkan Götene pastorat söker en **kyrkomusiker**. Tjänsten är heltid tillsvida... |  | x |  | 12 | [Musiker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/UCXb_LjK_pHv) |
| 5 | ...rkan Götene pastorat söker en **kyrkomusiker**. Tjänsten är heltid tillsvida... | x |  |  | 12 | [Kantor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WoT4_oqQ_5Fa) |
| 6 | ... en kyrkomusiker. Tjänsten är **heltid** tillsvidaretjänst med arbete ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 7 | ...komusiker. Tjänsten är heltid **tillsvidaretjänst** med arbete i Källby församlin... | x |  |  | 17 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 8 | ...aretjänst med arbete i Källby **församling**. Arbetet innebär även tjänstg... | x | x | 10 | 10 | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
| 9 | .... Vi söker dig som är behörig **kyrkomusiker** nivå C i Svenska kyrkan eller... |  | x |  | 12 | [Musiker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/UCXb_LjK_pHv) |
| 9 | .... Vi söker dig som är behörig **kyrkomusiker** nivå C i Svenska kyrkan eller... | x |  |  | 12 | [Kantor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WoT4_oqQ_5Fa) |
| 10 | ...behörig kyrkomusiker nivå C i **Svenska** kyrkan eller annan likvärdig ... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 11 | ...kyrkomusiker nivå C i Svenska **kyrkan** eller annan likvärdig utbildn... | x |  |  | 6 | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
| 12 | ...ng och som vill bli en av sex **kyrkomusiker** i Götene pastorat. Du kommer ... | x |  |  | 12 | [Kantor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WoT4_oqQ_5Fa) |
| 13 | ... bli en av sex kyrkomusiker i **Götene** pastorat. Du kommer att ha di... |  | x |  | 6 | [Götene, **municipality**](http://data.jobtechdev.se/taxonomy/concept/txzq_PQY_FGi) |
| 14 | ... ha din utgångspunkt i Källby **församling** med placering och arbetsrum i... | x | x | 10 | 10 | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
| 15 | ...ns ett arbetslag bestående av **präst**, diakonassistent, församlings... |  | x |  | 5 | [Präster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/2L2s_EU8_PzR) |
| 15 | ...ns ett arbetslag bestående av **präst**, diakonassistent, församlings... | x |  |  | 5 | [Präst, annat trossamfund, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PzLV_Vcy_RJG) |
| 16 | ... ansvar för kyrkokör i Källby **församling** och utveckling av barnkör. Kä... | x | x | 10 | 10 | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
| 17 | ...muntra dina personliga gåvor. **Götene** pastorat vill vara en arbetsg... |  | x |  | 6 | [Götene, **municipality**](http://data.jobtechdev.se/taxonomy/concept/txzq_PQY_FGi) |
| 18 | ...ingsregister att efterfrågas. **Körkort** och tillgång till egen bil är... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 18 | ...ingsregister att efterfrågas. **Körkort** och tillgång till egen bil är... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| | **Overall** | | | **30** | **205** | 30/205 = **15%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Präster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/2L2s_EU8_PzR) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Präst, annat trossamfund, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PzLV_Vcy_RJG) |
|  | x |  | [Musiker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/UCXb_LjK_pHv) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Kantor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WoT4_oqQ_5Fa) |
| x |  |  | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
|  | x |  | [Götene, **municipality**](http://data.jobtechdev.se/taxonomy/concept/txzq_PQY_FGi) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **1** | 1/12 = **8%** |