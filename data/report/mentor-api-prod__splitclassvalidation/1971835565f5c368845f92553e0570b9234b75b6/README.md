# Results for '1971835565f5c368845f92553e0570b9234b75b6'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1971835565f5c368845f92553e0570b9234b75b6](README.md) | 1 | 3257 | 38 | 40 | 150/887 = **17%** | 10/46 = **22%** |

## Source text

Resande Servicetekniker med instrumentkunskap till SICK Vi söker en resande Servicetekniker med instrumentkunskap till SICK i Stockholm, Uppsala eller Sundsvall.    Dina arbetsuppgifter Som resande servicetekniker kommer du arbeta med installation, teknisk service och förebyggande underhåll på optiska instrument för gasanalys.  Vi söker dig som tidigare arbetat som Instrumenttekniker, automationstekniker eller liknande och besitter stor instrumentkunskap samt el-kunskap då arbetet även innefattar att läsa el-ritningar. Du kommer även sköta dokumentation av arbetsorderna i CRM samt programmera och felsöka i PLC.  För att lyckas bra i rollen är det viktigt att du besitter god social kompetens då mycket av arbetet sker ute hos kund. Det är viktigt att lyssna till kundernas behov och önskemål och alltid arbeta mot målet nöjd kund.  Detta är en direktrekrytering till SICK där vi på A-Talent Tech sköter rekryteringsprocessen. Arbetstiderna är förlagda mån-fre 7-16.30. Resor med övernattningar uppskattas vara ca 50 st per år beroende på vad du bor. Vi söker dig som bor i anslutning till Stockholm, Uppsala eller Sundsvall.    Din profil Krav  -  - Gymnasieexamen från tekniskt gymnasium - Tidigare arbetslivserfarenhet som Servicetekniker, Instrumenttekniker eller liknande - Mycket bra förmåga att läsa och förstå mekaniska ritningar och el-ritningar - Flytande svenska kunskaper i både tal och skrift - Mycket bra kunskaper i Engelska både tal och skrift - B-körkort   Meriterande  -  - El-kompetens - Erfarenhet och/eller kunskap om PLC-programmering - Signalhantering - Tidigare erfarenhet från CRM-system   Önskade personliga egenskaper  söker vi dig som är en problemlösare, du är van att arbeta målstyrt och är flexibel, strukturerad och ordningsam i ditt arbete. Du är en teknisk intresserad lagspelare som är van vid serviceyrket och att arbeta mot målet nöjda kunder.  Om företaget     SICK AB och SICK AS ingår i koncernen SICK som grundades 1946. Med ca 10 000 anställda och en global verksamhet är de idag en världsledande tillverkare av sensorer, säkerhetssystem och produkter för automatisk identifiering. Företagets primära marknadsfokus ligger i Europa. De är idag 75 anställda med en omsättning på ca 400 miljoner SEK. Verksamheten är idag indelad i affärsområdena Factory Automation, Logistics Automation och Process Automation. SICK har ett stort produktutbud med egen utveckling och klassas som marknadsledande på sensorer.      Om A-Talent Tech A-Talent Tech arbetar nischat med rekrytering och konsultuthyrning av teknisk kompetens. Våra rekryteringsspecialister har god teknisk förståelse och tillsammans med experter inom executive search hittar och rekryterar vi även de mest svårfunna kompetenserna till er organisation. Vi är en del av A-Talent Group, en koncern med specialistföretag inom kompetensförsörjning.  Låter det intressant?   Då är du välkommen att söka tjänsten via formuläret nedan! Intervjuer sker löpande. Sista ansökningsdag är 14 augusti. Vid frågor kontakta ansvarig rekryterare Lena Sköld på telefon 08-562 159 19 eller på lena.skold@atalent.se.  Under semestertider nås vi lättast på mail och svarstiden kan vara något längre än vanligt.  Vänligen observera att du inte kan göra en ansökan via mail.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Resande **Servicetekniker** med instrumentkunskap till SI... |  | x |  | 15 | [servicetekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8ow6_pyZ_6zC) |
| 1 | Resande **Servicetekniker** med instrumentkunskap till SI... | x |  |  | 15 | [Servicetekniker, elektronik, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vtJ5_bZL_tgR) |
| 2 | Resande Servicetekniker med **instrumentkunskap** till SICK Vi söker en resande... |  | x |  | 17 | [Instrument, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/wLYs_CKS_wK8) |
| 3 | ...till SICK Vi söker en resande **Servicetekniker** med instrumentkunskap till SI... |  | x |  | 15 | [servicetekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8ow6_pyZ_6zC) |
| 3 | ...till SICK Vi söker en resande **Servicetekniker** med instrumentkunskap till SI... | x |  |  | 15 | [Servicetekniker, elektronik, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vtJ5_bZL_tgR) |
| 4 | ...n resande Servicetekniker med **instrumentkunskap** till SICK i Stockholm, Uppsal... |  | x |  | 17 | [Instrument, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/wLYs_CKS_wK8) |
| 5 | ...instrumentkunskap till SICK i **Stockholm**, Uppsala eller Sundsvall.    ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 6 | ...unskap till SICK i Stockholm, **Uppsala** eller Sundsvall.    Dina arbe... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 7 | ...CK i Stockholm, Uppsala eller **Sundsvall**.    Dina arbetsuppgifter Som ... | x | x | 9 | 9 | [Sundsvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dJbx_FWY_tK6) |
| 8 | ...a arbetsuppgifter Som resande **servicetekniker** kommer du arbeta med installa... |  | x |  | 15 | [servicetekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8ow6_pyZ_6zC) |
| 8 | ...a arbetsuppgifter Som resande **servicetekniker** kommer du arbeta med installa... | x |  |  | 15 | [Servicetekniker, elektronik, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vtJ5_bZL_tgR) |
| 9 | ...tekniker kommer du arbeta med **installation**, teknisk service och förebygg... | x |  |  | 12 | [Installationsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/1VVe_BBY_rpQ) |
| 10 | ...r du arbeta med installation, **teknisk service** och förebyggande underhåll på... | x |  |  | 15 | [Servicearbete, elektronik, **skill**](http://data.jobtechdev.se/taxonomy/concept/6dUR_Jvp_y35) |
| 11 | ...nisk service och förebyggande **underhåll** på optiska instrument för gas... | x |  |  | 9 | [underhåll och reparation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i19U_G6R_t19) |
| 12 | ...och förebyggande underhåll på **optiska instrument** för gasanalys.  Vi söker dig ... |  | x |  | 18 | [kalibrera optiska instrument, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/58FX_36M_bgU) |
| 12 | ...och förebyggande underhåll på **optiska instrument** för gasanalys.  Vi söker dig ... |  | x |  | 18 | [Tillverkning av optiska instrument och fotoutrustning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/Acnu_xgL_EWR) |
| 12 | ...och förebyggande underhåll på **optiska instrument** för gasanalys.  Vi söker dig ... |  | x |  | 18 | [Tillverkning av optiska instrument och fotoutrustning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/AdMZ_JJW_zeZ) |
| 12 | ...och förebyggande underhåll på **optiska instrument** för gasanalys.  Vi söker dig ... |  | x |  | 18 | [Tillverkning av optiska instrument och fotoutrustning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/BbrS_HU1_zkX) |
| 12 | ...och förebyggande underhåll på **optiska instrument** för gasanalys.  Vi söker dig ... | x | x | 18 | 18 | [optiska instrument, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvxY_ciG_PZ4) |
| 12 | ...och förebyggande underhåll på **optiska instrument** för gasanalys.  Vi söker dig ... |  | x |  | 18 | [arbetsledare, tillverkning, optiska instrument, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/dqxW_H2j_kB2) |
| 12 | ...och förebyggande underhåll på **optiska instrument** för gasanalys.  Vi söker dig ... |  | x |  | 18 | [råda kunder om underhåll av optiska instrument, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kVX1_noo_7Dh) |
| 13 | ... dig som tidigare arbetat som **Instrumenttekniker**, automationstekniker eller li... | x | x | 18 | 18 | [Instrumentelektriker/Instrumenttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3NQt_h54_Qy7) |
| 14 | ...betat som Instrumenttekniker, **automationstekniker** eller liknande och besitter s... |  | x |  | 19 | [Automationstekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CC4H_YCh_77T) |
| 14 | ...betat som Instrumenttekniker, **automationstekniker** eller liknande och besitter s... | x | x | 19 | 19 | [Automationstekniker, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/LrrC_U7A_Xus) |
| 14 | ...betat som Instrumenttekniker, **automationstekniker** eller liknande och besitter s... |  | x |  | 19 | [Automationstekniker, maskin, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/UiDW_ncp_spU) |
| 14 | ...betat som Instrumenttekniker, **automationstekniker** eller liknande och besitter s... |  | x |  | 19 | [automationstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/beLd_VF7_rzM) |
| 14 | ...betat som Instrumenttekniker, **automationstekniker** eller liknande och besitter s... |  | x |  | 19 | [Automationstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/mTT5_xgE_sRx) |
| 14 | ...betat som Instrumenttekniker, **automationstekniker** eller liknande och besitter s... |  | x |  | 19 | [Automationstekniker, elektronik, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oC9M_Bqu_WZH) |
| 15 | ...er liknande och besitter stor **instrumentkunskap** samt el-kunskap då arbetet äv... | x |  |  | 17 | [instrumentteknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/o1EC_DWQ_rQp) |
| 15 | ...er liknande och besitter stor **instrumentkunskap** samt el-kunskap då arbetet äv... |  | x |  | 17 | [Instrument, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/wLYs_CKS_wK8) |
| 16 | ...å arbetet även innefattar att **läsa el-ritningar**. Du kommer även sköta dokumen... | x |  |  | 17 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 17 | ... el-ritningar. Du kommer även **sköta dokumentation** av arbetsorderna i CRM samt p... | x |  |  | 19 | [tillhandahålla dokumentation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5uMi_uQS_9Sd) |
| 18 | ...tningar. Du kommer även sköta **dokumentation** av arbetsorderna i CRM samt p... |  | x |  | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 19 | ...umentation av arbetsorderna i **CRM** samt programmera och felsöka ... | x | x | 3 | 3 | [Customer Relationship Management/CRM, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQre_aBF_3dE) |
| 20 | ...n av arbetsorderna i CRM samt **programmera** och felsöka i PLC.  För att l... | x |  |  | 11 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 21 | ...na i CRM samt programmera och **felsöka** i PLC.  För att lyckas bra i ... | x |  |  | 7 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 22 | ...amt programmera och felsöka i **PLC**.  För att lyckas bra i rollen... | x |  |  | 3 | [Programmerbart styrsystem/PLC, **skill**](http://data.jobtechdev.se/taxonomy/concept/Uytu_hWi_5nh) |
| 23 | ...dig som bor i anslutning till **Stockholm**, Uppsala eller Sundsvall.    ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 24 | ... i anslutning till Stockholm, **Uppsala** eller Sundsvall.    Din profi... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 25 | ...till Stockholm, Uppsala eller **Sundsvall**.    Din profil Krav  -  - Gym... | x | x | 9 | 9 | [Sundsvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dJbx_FWY_tK6) |
| 26 | ...all.    Din profil Krav  -  - **Gymnasieexamen** från tekniskt gymnasium - Tid... |  | x |  | 14 | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| 26 | ...all.    Din profil Krav  -  - **Gymnasieexamen** från tekniskt gymnasium - Tid... | x |  |  | 14 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 27 | ...rav  -  - Gymnasieexamen från **tekniskt gymnasium** - Tidigare arbetslivserfarenh... | x |  |  | 18 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 28 | ... Gymnasieexamen från tekniskt **gymnasium** - Tidigare arbetslivserfarenh... |  | x |  | 9 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 29 | ...gare arbetslivserfarenhet som **Servicetekniker**, Instrumenttekniker eller lik... |  | x |  | 15 | [servicetekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8ow6_pyZ_6zC) |
| 29 | ...gare arbetslivserfarenhet som **Servicetekniker**, Instrumenttekniker eller lik... | x |  |  | 15 | [Servicetekniker, elektronik, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vtJ5_bZL_tgR) |
| 30 | ...farenhet som Servicetekniker, **Instrumenttekniker** eller liknande - Mycket bra f... | x | x | 18 | 18 | [Instrumentelektriker/Instrumenttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3NQt_h54_Qy7) |
| 31 | ...ande - Mycket bra förmåga att **läsa och förstå mekaniska ritningar och el-ritningar** - Flytande svenska kunskaper ... | x |  |  | 52 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 32 | ...r och el-ritningar - Flytande **svenska** kunskaper i både tal och skri... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 33 | ...rift - Mycket bra kunskaper i **Engelska** både tal och skrift - B-körko... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 34 | ...ngelska både tal och skrift - **B-körkort**   Meriterande  -  - El-kompet... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 35 | ...farenhet och/eller kunskap om **PLC**-programmering - Signalhanteri... | x |  |  | 3 | [Programmerbart styrsystem/PLC, **skill**](http://data.jobtechdev.se/taxonomy/concept/Uytu_hWi_5nh) |
| 36 | ...nhet och/eller kunskap om PLC-**programmering** - Signalhantering - Tidigare ... | x |  |  | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 37 | ...unskap om PLC-programmering - **Signalhantering** - Tidigare erfarenhet från CR... | x |  |  | 15 | [Signalbehandling, **skill**](http://data.jobtechdev.se/taxonomy/concept/ksNo_7bD_ez3) |
| 38 | ...ng - Tidigare erfarenhet från **CRM**-system   Önskade personliga e... | x |  |  | 3 | [Customer Relationship Management/CRM, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQre_aBF_3dE) |
| 39 | ...kaper  söker vi dig som är en **problemlösare**, du är van att arbeta målstyr... | x |  |  | 13 | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| 40 | ...ande tillverkare av sensorer, **säkerhetssystem** och produkter för automatisk ... |  | x |  | 15 | [övervaka planering av säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Dh2p_Fdc_4K8) |
| 40 | ...ande tillverkare av sensorer, **säkerhetssystem** och produkter för automatisk ... |  | x |  | 15 | [underhålla anläggningens säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EcC1_K6N_sVp) |
| 40 | ...ande tillverkare av sensorer, **säkerhetssystem** och produkter för automatisk ... |  | x |  | 15 | [underhålla säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gZSZ_3bG_GHL) |
| 40 | ...ande tillverkare av sensorer, **säkerhetssystem** och produkter för automatisk ... |  | x |  | 15 | [sköta tekniska säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qyk1_xqA_gid) |
| 40 | ...ande tillverkare av sensorer, **säkerhetssystem** och produkter för automatisk ... |  | x |  | 15 | [Lås- och säkerhetssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/txb5_eet_Huj) |
| 41 | ... Vid frågor kontakta ansvarig **rekryterare** Lena Sköld på telefon 08-562 ... | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| | **Overall** | | | **150** | **887** | 150/887 = **17%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Installationsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/1VVe_BBY_rpQ) |
| x | x | x | [Instrumentelektriker/Instrumenttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3NQt_h54_Qy7) |
|  | x |  | [kalibrera optiska instrument, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/58FX_36M_bgU) |
| x |  |  | [tillhandahålla dokumentation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5uMi_uQS_9Sd) |
| x |  |  | [Servicearbete, elektronik, **skill**](http://data.jobtechdev.se/taxonomy/concept/6dUR_Jvp_y35) |
|  | x |  | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
|  | x |  | [servicetekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8ow6_pyZ_6zC) |
|  | x |  | [Tillverkning av optiska instrument och fotoutrustning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/Acnu_xgL_EWR) |
|  | x |  | [Tillverkning av optiska instrument och fotoutrustning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/AdMZ_JJW_zeZ) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [Tillverkning av optiska instrument och fotoutrustning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/BbrS_HU1_zkX) |
|  | x |  | [Automationstekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CC4H_YCh_77T) |
|  | x |  | [övervaka planering av säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Dh2p_Fdc_4K8) |
|  | x |  | [underhålla anläggningens säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EcC1_K6N_sVp) |
| x |  |  | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| x | x | x | [Automationstekniker, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/LrrC_U7A_Xus) |
| x |  |  | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
|  | x |  | [Automationstekniker, maskin, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/UiDW_ncp_spU) |
| x | x | x | [optiska instrument, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvxY_ciG_PZ4) |
| x |  |  | [Programmerbart styrsystem/PLC, **skill**](http://data.jobtechdev.se/taxonomy/concept/Uytu_hWi_5nh) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| x |  |  | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
|  | x |  | [automationstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/beLd_VF7_rzM) |
| x | x | x | [Sundsvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dJbx_FWY_tK6) |
| x | x | x | [Customer Relationship Management/CRM, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQre_aBF_3dE) |
|  | x |  | [arbetsledare, tillverkning, optiska instrument, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/dqxW_H2j_kB2) |
|  | x |  | [underhålla säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gZSZ_3bG_GHL) |
| x |  |  | [underhåll och reparation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i19U_G6R_t19) |
| x |  |  | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
|  | x |  | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
|  | x |  | [råda kunder om underhåll av optiska instrument, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kVX1_noo_7Dh) |
| x |  |  | [Signalbehandling, **skill**](http://data.jobtechdev.se/taxonomy/concept/ksNo_7bD_ez3) |
|  | x |  | [Automationstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/mTT5_xgE_sRx) |
| x |  |  | [instrumentteknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/o1EC_DWQ_rQp) |
|  | x |  | [Automationstekniker, elektronik, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oC9M_Bqu_WZH) |
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
|  | x |  | [sköta tekniska säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qyk1_xqA_gid) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
|  | x |  | [Lås- och säkerhetssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/txb5_eet_Huj) |
| x |  |  | [Servicetekniker, elektronik, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vtJ5_bZL_tgR) |
|  | x |  | [Instrument, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/wLYs_CKS_wK8) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **10** | 10/46 = **22%** |