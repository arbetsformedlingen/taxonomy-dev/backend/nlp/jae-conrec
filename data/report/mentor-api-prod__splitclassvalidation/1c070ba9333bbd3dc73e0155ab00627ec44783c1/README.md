# Results for '1c070ba9333bbd3dc73e0155ab00627ec44783c1'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1c070ba9333bbd3dc73e0155ab00627ec44783c1](README.md) | 1 | 1578 | 7 | 6 | 20/90 = **22%** | 2/7 = **29%** |

## Source text

Jobba direkt efter studenten! Tar du studenten 2022 och brinner för personlig utveckling? Vi öppnar upp för fler tjänster inför sommaren! Är du vår nästa talang? ⭐  Om oss  Key Solutions är ett av Sveriges största konsultföretag med över 200 anställda. Sedan starten 2008 har vi hjälp några av världens bästa varumärken såsom Google, Telia, Netflix m.fl.  - Årets byrå 2019 (Telia) - Årets bästa återförsäljare 2018 & 2019 (EON) - Årets innovativa partner 2020 & 2021 (IP-Only) - En av Sveriges & Europas Bästa Arbetsplatser 10 år i rad (Great Place to Work)  Vad erbjuder vi ?   Garanterad månadslön på 30 000 kr inom sex månader  Goda chanser att klättra inom företaget ️ Konferensresor till t.ex. Barcelona, Miami och Sydafrika  Hjälp med att ta körkort samt möjlighet till tjänstebil och andra förmåner  Vi avsätter minst en timme om dagen för din personliga utveckling  Vi ser gärna att du som söker är...  - Är en sann glädjespridare och har god social kompetens - Är tävlingsinriktad och älskar att utvecklas - Pratar svenska obehindrat - Har körkort (ej ett krav men det är meriterande)  Har du ingen tidigare erfarenhet? Ingen fara!  Då vi har några av Sveriges absolut bästa säljare och coacher till förfogande, samt erbjuder säljutbildning i Key Business School, så har vi inget krav på tidigare säljerfarenhet. Vi tar ansvaret för att ge dig de bästa verktygen för att lyckas inom din karriär.  Låter det intressant?  Ansök nedan eller gör testet på 1 minut via vår hemsida för att se ifall just du skulle platsa i vårt team: https://www.keysolutions.se/jobb/ansok ⭐

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...tudenten 2022 och brinner för **personlig utveckling**? Vi öppnar upp för fler tjäns... | x | x | 20 | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 2 | ... oss  Key Solutions är ett av **Sveriges** största konsultföretag med öv... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 3 | ... att klättra inom företaget ️ **Konfere**nsresor till t.ex. Barcelona, ... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 4 | ...nn glädjespridare och har god **social kompetens** - Är tävlingsinriktad och äls... | x |  |  | 16 | [uppvisa social kompetens, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5AbQ_a6i_H1T) |
| 5 | ...älskar att utvecklas - Pratar **svenska** obehindrat - Har körkort (ej ... | x |  |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 6 | ...atar svenska obehindrat - Har **körkort** (ej ett krav men det är merit... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 7 | ...hindrat - Har körkort (ej ett **krav me**n det är meriterande)  Har du ... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 8 | ... Ingen fara!  Då vi har några **a**v Sveriges absolut bästa sälja... |  | x |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 9 | ...gen fara!  Då vi har några av **Sveriges** absolut bästa säljare och coa... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 10 | ...gra av Sveriges absolut bästa **s**äljare och coacher till förfog... |  | x |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 11 | ...gra av Sveriges absolut bästa **säljare** och coacher till förfogande, ... | x |  |  | 7 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 12 | ... Sveriges absolut bästa säljar**e** och coacher till förfogande, ... |  | x |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **20** | **90** | 20/90 = **22%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [uppvisa social kompetens, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5AbQ_a6i_H1T) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| x | x | x | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **2** | 2/7 = **29%** |