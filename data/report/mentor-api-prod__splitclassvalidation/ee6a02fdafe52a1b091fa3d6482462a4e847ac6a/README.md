# Results for 'ee6a02fdafe52a1b091fa3d6482462a4e847ac6a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ee6a02fdafe52a1b091fa3d6482462a4e847ac6a](README.md) | 1 | 3086 | 7 | 9 | 45/100 = **45%** | 4/10 = **40%** |

## Source text

junior ekonom i stockholm - future talent trainee. Arbetsbeskrivning Är du en ambitiös person som målmedvetet vill ta dig framåt i yrkeslivet? Nu har du en unik möjlighet att delta i Randstads traineeprogram med start våren 2023! Vi erbjuder tolv utvalda personer en plats i Randstad Future Talent Trainee Programme där du som deltagare får en säkrad karriärutveckling i form av ett personligt utvecklingsprogram i kombination med heltidsarbete inom ditt specialistområde. I samarbete med toppklassiga varumärken och företag kommer detta ge dig ett enormt försprång i din framtida karriär!  Ansvarsområden Under rekryteringsprocessen genomför vi matchningen mot våra aktuella kunder baserat på din utbildningsbakgrund, dina testresultat och våra intervjuer och samtal. Genom grundligt förarbete skapar vi förutsättningar för att du placeras i den yrkesroll med de ansvarsområden där du kommer att lyckas, trivas och utvecklas.  Kvalifikationer Du som söker har nyligen avslutat dina studier med mycket goda betyg, har arbetat hårt och målfokuserat under din studietid för att förbereda dig maximalt inför din utstakade karriärresa mot ledande positioner. Vi söker dig som levererat höga betyg på slutuppgifter och examensarbete, dig som entusiastiskt laddat kraft i startblocken under hösten och nu är redo att accelerera vidare in i yrkeslivet. Dina nära vänner och bekanta beskriver dig som en top performer och när du tar dig an en uppgift är endast det bästa resultat gott nog för dig. Precision och detaljnivå är ett av dina starkare vapen när du genomför det du tagit dig för. Det är aldrig något som lämnas åt slumpen och du lämnar motvilligt ifrån dig halvfärdiga uppgifter du inte känner dig tillfreds med.   För att du ska vara en av de utvalda bevisar du en stark inre motivation och drivkraft för att kunna hantera och genomföra vårt utmanande program. Genom att bevisa merit- och personlighetsmässigt att du är den vi söker är du med i en kompetensbaserad rekryteringsprocess i konkurrens om platserna. Programmet kommer vara en prövning, så ett starkt mindset och din inre drivkraft kommer vara avgörande för dig och din framgång. Tillsammans med de essentiella verktyg du får under programmets gång kommer du spränga gränser du aldrig trodde du skulle nå!  Ansökan Ansökan görs via den här annonsen. Har du frågor så hör av dig till Fredrik Löfgren, Consultant Manager, på fredrik.lofgren@randstad.se   Om företaget Med över 600 000 anställda i omkring 40 länder är Randstad världsledande inom HR-tjänster och erbjuder bemannings-, konsult- och rekryteringslösningar inom alla kompetensområden. Vi erbjuder även interim management, executive search och omställningstjänster. Vi har ett stort nätverk av bolag och kandidater vilket innebär att vi förmedlar hundratals jobb inom olika branscher, från Kiruna i norr till Malmö i söder. Vår ambition är att vara den bästa arbetsgivaren på marknaden. Genom att kombinera vår passion för människor med kraften i dagens teknologi hjälper vi människor och organisationer att nå deras sanna potential. Vi kallar det Human Forward.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | junior **ekonom** i stockholm - future talent t... | x |  |  | 6 | [ekonom, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Dn9q_RSA_aM3) |
| 1 | junior **ekonom** i stockholm - future talent t... |  | x |  | 6 | [Övriga ekonomer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Y6yY_SuR_hVh) |
| 2 | junior ekonom i **stockholm** - future talent trainee. Arbe... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 3 | ...m i stockholm - future talent **trainee**. Arbetsbeskrivning Är du en a... |  | x |  | 7 | [Trainee välfärd, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/dJxe_nVr_6Pq) |
| 3 | ...m i stockholm - future talent **trainee**. Arbetsbeskrivning Är du en a... |  | x |  | 7 | [Trainee brist, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/k7AF_NQa_TXm) |
| 4 | ...lats i Randstad Future Talent **Trainee** Programme där du som deltagar... |  | x |  | 7 | [Trainee välfärd, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/dJxe_nVr_6Pq) |
| 4 | ...lats i Randstad Future Talent **Trainee** Programme där du som deltagar... |  | x |  | 7 | [Trainee brist, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/k7AF_NQa_TXm) |
| 5 | ...ingsprogram i kombination med **heltidsarbete** inom ditt specialistområde. I... | x | x | 13 | 13 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 6 | ... av dig till Fredrik Löfgren, **Consultant Manager**, på fredrik.lofgren@randstad.... | x | x | 18 | 18 | [Consultant manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/m7gK_jFU_vY8) |
| 7 | ...bb inom olika branscher, från **Kiruna** i norr till Malmö i söder. Vå... | x |  |  | 6 | [Kiruna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/biN6_UiL_Qob) |
| 8 | ...cher, från Kiruna i norr till **Malmö** i söder. Vår ambition är att ... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 9 | ...änniskor med kraften i dagens **teknologi** hjälper vi människor och orga... | x |  |  | 9 | [Teknologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KQjv_TUZ_Hy9) |
| | **Overall** | | | **45** | **100** | 45/100 = **45%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [ekonom, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Dn9q_RSA_aM3) |
| x |  |  | [Teknologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KQjv_TUZ_Hy9) |
|  | x |  | [Övriga ekonomer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Y6yY_SuR_hVh) |
| x |  |  | [Kiruna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/biN6_UiL_Qob) |
|  | x |  | [Trainee välfärd, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/dJxe_nVr_6Pq) |
|  | x |  | [Trainee brist, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/k7AF_NQa_TXm) |
| x | x | x | [Consultant manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/m7gK_jFU_vY8) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| | | **4** | 4/10 = **40%** |