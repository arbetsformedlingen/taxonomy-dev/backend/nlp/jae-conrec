# Results for 'd048b1dc2fe1c0ca2be95caed63fa4e20bdc73f8'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d048b1dc2fe1c0ca2be95caed63fa4e20bdc73f8](README.md) | 1 | 1088 | 11 | 1 | 0/99 = **0%** | 0/6 = **0%** |

## Source text

UTBILDNINGSINFO OCH EXAMEN Utbildningen är tvåårig och omfattar 400 Yh-poäng uppdelade i två delar; Yh-kurser och LIA/praktik (Lärande i arbete). Efter genomförd utbildning erhålles ”Kvalificerad Yrkeshögskoleexamen”. OBS! Det finns två olika yrkesutgångar; fiske eller jakt.    VÄLJ INRIKTNING; FISKE ELLER JAKT Utöver den gemensamma kursplanen, med övergripande kurser samt kombinerade fiske- och jaktkurser, väljer studenten en fördjupning som riktar sig mot yrkesutgång fiske eller jakt.    STOR ERFARENHET AV BRANSCHEN Inom sportfiske har skolan varit verksam och ledande på yrkeshögskolenivå de senaste tjugo åren. ForshagaAkademin har ett nära samarbete med många företag och organisationer inom sportfiske- och jaktområdet. Detta säkerställer att studenterna får en utbildning med bra branschförankring och god möjlighet till nätverksbyggande.    FRAMTIDSUTSIKTER Fiske- och jaktturismbranschen växer rejält och därmed finns det mycket goda möjligheter att få anställning eller starta eget företag efter avslutad utbildning. Vår utbildning ger dig redskapen att möta efterfrågan! 

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | UTBILDNINGSINFO OCH EXAMEN **Utbildningen är tvåårig** och omfattar 400 Yh-poäng upp... | x |  |  | 23 | (not found in taxonomy) |
| 2 | ...inns två olika yrkesutgångar; **fiske** eller jakt.    VÄLJ INRIKTNIN... | x |  |  | 5 | [Fiske, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/gJ84_iFe_usj) |
| 3 | ...ka yrkesutgångar; fiske eller **jakt**.    VÄLJ INRIKTNING; FISKE EL... | x |  |  | 4 | [jakt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iVgY_4Wt_J8C) |
| 4 | ...ler jakt.    VÄLJ INRIKTNING; **FISKE** ELLER JAKT Utöver den gemensa... | x |  |  | 5 | [Fiske, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/gJ84_iFe_usj) |
| 5 | ... VÄLJ INRIKTNING; FISKE ELLER **JAKT** Utöver den gemensamma kurspla... | x |  |  | 4 | [jakt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iVgY_4Wt_J8C) |
| 6 | ...pande kurser samt kombinerade **fiske**- och jaktkurser, väljer stude... | x |  |  | 5 | [Fiske, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/gJ84_iFe_usj) |
| 7 | ...r samt kombinerade fiske- och **jaktkurser**, väljer studenten en fördjupn... | x |  |  | 10 | [jakt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iVgY_4Wt_J8C) |
| 8 | ...om riktar sig mot yrkesutgång **fiske** eller jakt.    STOR ERFARENHE... | x |  |  | 5 | [Fiske, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/gJ84_iFe_usj) |
| 9 | ...g mot yrkesutgång fiske eller **jakt**.    STOR ERFARENHET AV BRANSC... | x |  |  | 4 | [jakt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iVgY_4Wt_J8C) |
| 10 | ... ERFARENHET AV BRANSCHEN Inom **sportfiske** har skolan varit verksam och ... |  | x |  | 10 | [Sportfiske, instruktör, **skill**](http://data.jobtechdev.se/taxonomy/concept/auC1_LyU_JV1) |
| 11 | ...byggande.    FRAMTIDSUTSIKTER **Fiske**- och jaktturismbranschen växe... | x |  |  | 5 | [Fiske, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kpZm_ah5_KGQ) |
| 12 | ...  FRAMTIDSUTSIKTER Fiske- och **jaktturismbranschen** växer rejält och därmed finns... | x |  |  | 19 | [Jaktturism, **skill**](http://data.jobtechdev.se/taxonomy/concept/zR8a_F23_17p) |
| | **Overall** | | | **0** | **99** | 0/99 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | (not found in taxonomy) |
|  | x |  | [Sportfiske, instruktör, **skill**](http://data.jobtechdev.se/taxonomy/concept/auC1_LyU_JV1) |
| x |  |  | [Fiske, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/gJ84_iFe_usj) |
| x |  |  | [jakt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iVgY_4Wt_J8C) |
| x |  |  | [Fiske, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kpZm_ah5_KGQ) |
| x |  |  | [Jaktturism, **skill**](http://data.jobtechdev.se/taxonomy/concept/zR8a_F23_17p) |
| | | **0** | 0/6 = **0%** |