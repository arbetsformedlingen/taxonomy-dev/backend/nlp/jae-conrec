# Results for '5cad43f114653fa1718d6bb21c4b2549aaf2e0f6'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [5cad43f114653fa1718d6bb21c4b2549aaf2e0f6](README.md) | 1 | 1845 | 21 | 21 | 85/514 = **17%** | 7/27 = **26%** |

## Source text

Är du teknikintresserad?  Att arbeta som tågtekniker innebär arbete med såväl datateknik, automation som mekanik och elektronik. Tågbranschen genomgår just nu en stark teknisk utveckling. Den globala miljösituationen har lett till satsningar inom spårbunden trafik, och höghastighetståg införs i hela Europa. Efter utbildningen är chansen till jobb mycket stor då det är brist på utbildade tågtekniker.  Under utbildningen kommer du utveckla kunskaper inom bl.a underhållsteknik, mätteknik, elektriska system, elektronikstyrda mekaniska system, säkerhet, signalteknik, teknisk engelska, el- och elektronik i spårbundna fordon. I tågteknikers arbetsuppgifter ingår det bland annat att planera för ett effektivt underhåll, förebygga att fel uppstår och avhjälpa de vanligaste problemen. Kombinationen med praktisk förmåga och teoretiska kunskaper krävs för att kunna hantera dagens högteknologiska tåg. Tack vare bredden i sin kompentens så kan tågtekniker även jobba inom andra branscher och länder vilket ger utbildade tågtekniker stor flexibilitet och valfrihet.  LIA - Lärande i arbete  I utbildningen är det inbyggd praktik eller LIA (lärande i arbete) vilket innebär att man är ute i arbetslivet, på företag, och får testa sina kunskaper, färdigheter och kompetenser i en relevant miljö, men också lära sig nya saker i skarpt läge. LIA-perioden kan komma att ske på annan ort än studieorten, och det kan även förekomma skiftarbete under LIA-perioden. Praktiken är sammanlagt 10 veckor av totalt 60 utbildningsveckor. Viss del av kursen är examensarbete.  Om skolan  Falköping som studieort ligger bra till geografiskt med goda pendlingsmöjligheter. Utbildningsanordnaden är Lärcenter Falköping. Skolans verksamhet är väl förankrad i en aktiv ledningsgrupp.  Är du intresserad? Tveka inte att kontakta oss, så berättar vi mer om utbildningen.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...kintresserad?  Att arbeta som **tågtekniker** innebär arbete med såväl data... | x |  |  | 11 | [tekniker, fordonsindustrin, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/JD24_nCe_bzK) |
| 2 | ...iker innebär arbete med såväl **datateknik**, automation som mekanik och e... |  | x |  | 10 | [Annan utbildning i elektronik, datateknik och automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/3tq2_f1E_Lyh) |
| 2 | ...iker innebär arbete med såväl **datateknik**, automation som mekanik och e... |  | x |  | 10 | [Datavetenskap och systemvetenskap, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Ai2t_uy2_1KN) |
| 2 | ...iker innebär arbete med såväl **datateknik**, automation som mekanik och e... |  | x |  | 10 | [Datateknik (generell examen), **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z55j_wVq_N99) |
| 2 | ...iker innebär arbete med såväl **datateknik**, automation som mekanik och e... | x |  |  | 10 | [datateknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/a4bk_LA5_gtG) |
| 3 | ... arbete med såväl datateknik, **automation** som mekanik och elektronik. T... | x |  |  | 10 | [Automationsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/MT1a_Nyq_DGQ) |
| 4 | ...äl datateknik, automation som **mekanik** och elektronik. Tågbranschen ... | x | x | 7 | 7 | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| 5 | ...k, automation som mekanik och **elektronik**. Tågbranschen genomgår just n... | x | x | 10 | 10 | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| 6 | ... då det är brist på utbildade **tågtekniker**.  Under utbildningen kommer d... | x |  |  | 11 | [tekniker, fordonsindustrin, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/JD24_nCe_bzK) |
| 7 | ... utveckla kunskaper inom bl.a **underhållsteknik**, mätteknik, elektriska system... |  | x |  | 16 | [Underhållsarbete, VA-nät/Underhållsarbete, gatunät, **skill**](http://data.jobtechdev.se/taxonomy/concept/2SCx_2dX_M79) |
| 7 | ... utveckla kunskaper inom bl.a **underhållsteknik**, mätteknik, elektriska system... | x |  |  | 16 | [underhåll och reparation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i19U_G6R_t19) |
| 8 | ...r inom bl.a underhållsteknik, **mätteknik**, elektriska system, elektroni... | x | x | 9 | 9 | [Mätteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/wZpC_X9a_xzG) |
| 9 | ... underhållsteknik, mätteknik, **elektriska system**, elektronikstyrda mekaniska s... | x | x | 17 | 17 | [elektriska system som används vid transport, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6Qm6_CWU_Ce8) |
| 9 | ... underhållsteknik, mätteknik, **elektriska system**, elektronikstyrda mekaniska s... |  | x |  | 17 | [sköta drift och underhåll av fartygs elektriska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/JJmQ_3Hq_aoV) |
| 9 | ... underhållsteknik, mätteknik, **elektriska system**, elektronikstyrda mekaniska s... |  | x |  | 17 | [skapa modeller för elektriska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Jtbv_Qyz_u68) |
| 9 | ... underhållsteknik, mätteknik, **elektriska system**, elektronikstyrda mekaniska s... |  | x |  | 17 | [designa elektriska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/P3vY_qhv_qfq) |
| 9 | ... underhållsteknik, mätteknik, **elektriska system**, elektronikstyrda mekaniska s... |  | x |  | 17 | [schemalägga underhåll av elektriska system på flygplats, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dGyW_mS9_NeB) |
| 9 | ... underhållsteknik, mätteknik, **elektriska system**, elektronikstyrda mekaniska s... |  | x |  | 17 | [reparera fartygs elektriska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eotE_pQN_uXY) |
| 10 | ...mätteknik, elektriska system, **elektronik**styrda mekaniska system, säker... |  | x |  | 10 | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| 11 | ...mätteknik, elektriska system, **elektronikstyrda **mekaniska system, säkerhet, si... | x |  |  | 17 | [mekaniska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/3s2E_7qb_MYj) |
| 12 | ...iska system, elektronikstyrda **mekaniska system**, säkerhet, signalteknik, tekn... |  | x |  | 16 | [reparera fartygs mekaniska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1cFG_Ai2_nAn) |
| 12 | ...iska system, elektronikstyrda **mekaniska system**, säkerhet, signalteknik, tekn... | x | x | 16 | 16 | [mekaniska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/3s2E_7qb_MYj) |
| 12 | ...iska system, elektronikstyrda **mekaniska system**, säkerhet, signalteknik, tekn... |  | x |  | 16 | [mekaniska system i trådbussar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/9vjY_44b_AYe) |
| 12 | ...iska system, elektronikstyrda **mekaniska system**, säkerhet, signalteknik, tekn... |  | x |  | 16 | [mekaniska system i tåg, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dduK_922_hoz) |
| 13 | ...ronikstyrda mekaniska system, **säkerhet**, signalteknik, teknisk engels... | x |  |  | 8 | [hälso- och säkerhetsföreskrifter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mDow_hvd_qnX) |
| 14 | ...a mekaniska system, säkerhet, **signalteknik**, teknisk engelska, el- och el... | x |  |  | 12 | [Signalsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/4MQo_2b2_u1z) |
| 15 | ...stem, säkerhet, signalteknik, **teknisk **engelska, el- och elektronik i... | x |  |  | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 16 | ...kerhet, signalteknik, teknisk **engelska**, el- och elektronik i spårbun... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 17 | ...gnalteknik, teknisk engelska, **el- och elektronik i spårbundna fordon**. I tågteknikers arbetsuppgift... | x |  |  | 38 | [Fordonselektronik, felsökningsvana/Fordonselektronik, reparationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/moJc_yZC_sjN) |
| 18 | ...tronik i spårbundna fordon. I **tågteknikers** arbetsuppgifter ingår det bla... | x |  |  | 12 | [tekniker, fordonsindustrin, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/JD24_nCe_bzK) |
| 19 | ...and annat att planera för ett **effektivt underhåll**, förebygga att fel uppstår oc... | x |  |  | 19 | [underhåll och reparation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i19U_G6R_t19) |
| 20 | ... för ett effektivt underhåll, **förebygga att fel uppstår** och avhjälpa de vanligaste pr... | x |  |  | 25 | [underhåll och reparation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i19U_G6R_t19) |
| 21 | ...förebygga att fel uppstår och **avhjälpa de vanligaste problemen**. Kombinationen med praktisk f... | x |  |  | 32 | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| 22 | ...edden i sin kompentens så kan **tågtekniker** även jobba inom andra bransch... | x |  |  | 11 | [tekniker, fordonsindustrin, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/JD24_nCe_bzK) |
| 23 | ...är examensarbete.  Om skolan  **Falköping** som studieort ligger bra till... | x | x | 9 | 9 | [Falköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ZySF_gif_zE4) |
| 24 | ...dningsanordnaden är Lärcenter **Falköping**. Skolans verksamhet är väl fö... | x | x | 9 | 9 | [Falköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ZySF_gif_zE4) |
| | **Overall** | | | **85** | **514** | 85/514 = **17%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [reparera fartygs mekaniska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1cFG_Ai2_nAn) |
|  | x |  | [Underhållsarbete, VA-nät/Underhållsarbete, gatunät, **skill**](http://data.jobtechdev.se/taxonomy/concept/2SCx_2dX_M79) |
| x | x | x | [mekaniska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/3s2E_7qb_MYj) |
|  | x |  | [Annan utbildning i elektronik, datateknik och automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/3tq2_f1E_Lyh) |
| x |  |  | [Signalsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/4MQo_2b2_u1z) |
| x | x | x | [elektriska system som används vid transport, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6Qm6_CWU_Ce8) |
|  | x |  | [mekaniska system i trådbussar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/9vjY_44b_AYe) |
|  | x |  | [Datavetenskap och systemvetenskap, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Ai2t_uy2_1KN) |
| x |  |  | [tekniker, fordonsindustrin, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/JD24_nCe_bzK) |
|  | x |  | [sköta drift och underhåll av fartygs elektriska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/JJmQ_3Hq_aoV) |
|  | x |  | [skapa modeller för elektriska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Jtbv_Qyz_u68) |
| x |  |  | [Automationsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/MT1a_Nyq_DGQ) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [designa elektriska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/P3vY_qhv_qfq) |
| x | x | x | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| x |  |  | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
|  | x |  | [Datateknik (generell examen), **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z55j_wVq_N99) |
| x | x | x | [Falköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ZySF_gif_zE4) |
| x |  |  | [datateknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/a4bk_LA5_gtG) |
|  | x |  | [schemalägga underhåll av elektriska system på flygplats, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dGyW_mS9_NeB) |
|  | x |  | [mekaniska system i tåg, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dduK_922_hoz) |
|  | x |  | [reparera fartygs elektriska system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eotE_pQN_uXY) |
| x |  |  | [underhåll och reparation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i19U_G6R_t19) |
| x |  |  | [hälso- och säkerhetsföreskrifter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mDow_hvd_qnX) |
| x |  |  | [Fordonselektronik, felsökningsvana/Fordonselektronik, reparationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/moJc_yZC_sjN) |
| x | x | x | [Mätteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/wZpC_X9a_xzG) |
| x | x | x | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| | | **7** | 7/27 = **26%** |