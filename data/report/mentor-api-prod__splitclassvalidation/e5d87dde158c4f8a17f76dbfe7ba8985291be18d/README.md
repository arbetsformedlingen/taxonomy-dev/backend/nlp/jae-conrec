# Results for 'e5d87dde158c4f8a17f76dbfe7ba8985291be18d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e5d87dde158c4f8a17f76dbfe7ba8985291be18d](README.md) | 1 | 678 | 11 | 8 | 46/136 = **34%** | 4/8 = **50%** |

## Source text

Träarbetare Hej alla duktiga snickare! ExpanderaMera söker efter flera kunniga träarbetare till projekt hos en av våra kunder i Örebro. Arbetsuppgifterna hos kunden är varierande, bl.a. innerväggar, tak, golv, dörrar.  Det är önskvärt att du har yrkesbevis som snickare men det viktigaste är att du har erfarenhet av liknande arbete minst 5 år. Som person du är social samt håller tider och du har inget problem jobba i grupp eller självständigt.  Vi värnar om mångfald och det är viktigt för oss att alla känner sig välkomna. Detta uppdrag startar omgående, urval och intervjuer sker löpande. Vid frågor om tjänsten kontakta kontoret på 08 530 343 78 Välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Träarbetare** Hej alla duktiga snickare! Ex... | x | x | 11 | 11 | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| 1 | **Träarbetare** Hej alla duktiga snickare! Ex... |  | x |  | 11 | [träarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FmH5_Pqk_6k3) |
| 2 | Träarbetare Hej alla duktiga **snickare**! ExpanderaMera söker efter fl... | x |  |  | 8 | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| 2 | Träarbetare Hej alla duktiga **snickare**! ExpanderaMera söker efter fl... |  | x |  | 8 | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| 3 | ...era söker efter flera kunniga **träarbetare** till projekt hos en av våra k... | x | x | 11 | 11 | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| 3 | ...era söker efter flera kunniga **träarbetare** till projekt hos en av våra k... |  | x |  | 11 | [träarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FmH5_Pqk_6k3) |
| 4 | ...ojekt hos en av våra kunder i **Örebro**. Arbetsuppgifterna hos kunden... | x | x | 6 | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 5 | ....  Det är önskvärt att du har **yrkesbevis** som snickare men det viktigas... | x | x | 10 | 10 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 6 | ...ärt att du har yrkesbevis som **snickare** men det viktigaste är att du ... | x | x | 8 | 8 | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| 7 | ... det viktigaste är att du har **erfarenhet** av liknande arbete minst 5 år... | x |  |  | 10 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 8 | ...du har erfarenhet av liknande **arbete minst 5 år**. Som person du är social samt... | x |  |  | 17 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 9 | ...ider och du har inget problem **jobba** i grupp eller självständigt. ... | x |  |  | 5 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 10 | ...ch du har inget problem jobba **i grupp** eller självständigt.  Vi värn... | x |  |  | 7 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 11 | ...t problem jobba i grupp eller **självständigt**.  Vi värnar om mångfald och d... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| | **Overall** | | | **46** | **136** | 46/136 = **34%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
|  | x |  | [träarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FmH5_Pqk_6k3) |
| x | x | x | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| x | x | x | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| | | **4** | 4/8 = **50%** |