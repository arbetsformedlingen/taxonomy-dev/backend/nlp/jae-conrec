# Results for 'c07a4c5582ae590e2a85ac1d1531e56d405869b2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c07a4c5582ae590e2a85ac1d1531e56d405869b2](README.md) | 1 | 1601 | 16 | 17 | 102/140 = **73%** | 14/19 = **74%** |

## Source text

Vill du undvika att rufsa till frisyren på vägen till strandhänget men inte vill skada dig om olyckan trots allt skulle vara framme? Då kanske Prevas-utvecklade Hövding är något för dig. Världens säkraste cykelhjälm. Med 8000 produktlanseringar i ryggen blickar vi fram emot ännu fler, och söker därför nya medarbetare till vårt team som utvecklar nya produkter och system. Vi arbetar idag med över 50 bolag av varierande storlek och inriktning, vilket innebär att du som testutvecklare hos oss har möjlighet att välja mellan många olika typer av uppdrag och branscher.   Vill du jobba med detta?   • Testutveckling i spännande och utmanande projekt för att bidra till nya produkter som förändrar människors vardag  • Möjlighet att jobba med den senaste tekniken, antingen på plats hos oss eller hos kund.   Verkar nedan områden spännande för dig?   • Utveckla applikationer eller testmjukvara i något eller några av dessa språk: Python, Java, C/C++, C#  • Arbeta i Windows \|\| Linux - miljöer  • Automatiserade verktyg/ramverk för kodleverans, som t.ex. Jenkins, Selenium, Pytest, junit, Testrail, Robot Framework, Gradle, Gtest, Kubernetes  • Version- och ärendehanteringssystem, såsom Git och Jira  • Ett visst inslag av elektronik och mjukvara för inbyggda system   Vi och våra kunder använder svenska och engelska i tal som i skrift och ser gärna att du har åtminstone tre års relevant arbetslivserfarenhet.   Vi rekryterar kontinuerligt, så länge annonsen finns ute går tjänsten att söka!   Frågor? Vi svarar mer än gärna!   Rekryterande chef: Jens Barkvall 070- 379 06 87 Jens.barkvall@prevas.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ng, vilket innebär att du som **testutvecklare** hos oss har möjlighet att väl... | x | x | 14 | 14 | [Testutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TU7g_mwa_VzB) |
| 2 | ...mråden spännande för dig?   • **Utveckla applikationer** eller testmjukvara i något el... | x |  |  | 22 | [Applikationsutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CZkP_hCz_KM8) |
| 3 | ...t eller några av dessa språk: **Python**, Java, C/C++, C#  • Arbeta i ... | x | x | 6 | 6 | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| 4 | ...några av dessa språk: Python, **Java**, C/C++, C#  • Arbeta i Window... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 5 | ...av dessa språk: Python, Java, **C**/C++, C#  • Arbeta i Windows \|... | x |  |  | 1 | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| 6 | ... dessa språk: Python, Java, C/**C++**, C#  • Arbeta i Windows \|\| Li... |  | x |  | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 7 | ...a språk: Python, Java, C/C++, **C#**  • Arbeta i Windows \|\| Linux ... |  | x |  | 2 | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| 8 | ..., Java, C/C++, C#  • Arbeta i **Windows** \|\| Linux - miljöer  • Automat... | x | x | 7 | 7 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 9 | ...++, C#  • Arbeta i Windows \|\| **Linux** - miljöer  • Automatiserade v... | x | x | 5 | 5 | [Linux, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/z52H_f4y_4ZA) |
| 10 | ...rk för kodleverans, som t.ex. **Jenkins**, Selenium, Pytest, junit, Tes... | x | x | 7 | 7 | [Jenkins, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/tmyt_n6R_1Gc) |
| 11 | ...dleverans, som t.ex. Jenkins, **Selenium**, Pytest, junit, Testrail, Rob... | x | x | 8 | 8 | [Selenium, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/i6kH_axQ_nH8) |
| 12 | ...ins, Selenium, Pytest, junit, **Testrail**, Robot Framework, Gradle, Gte... | x | x | 8 | 8 | [Testrail, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/AKY2_EnS_eG4) |
| 13 | ...t, Testrail, Robot Framework, **Gradle**, Gtest, Kubernetes  • Version... | x | x | 6 | 6 | [Gradle, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/m8mi_pid_qin) |
| 14 | ...ärendehanteringssystem, såsom **Git** och Jira  • Ett visst inslag ... | x | x | 3 | 3 | [Git, versionshanteringssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/2REh_Drn_nsh) |
| 15 | ...nteringssystem, såsom Git och **Jira**  • Ett visst inslag av elektr... | x | x | 4 | 4 | [JIRA, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQoJ_GY7_n5Z) |
| 16 | ...h Jira  • Ett visst inslag av **elektronik** och mjukvara för inbyggda sys... |  | x |  | 10 | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| 17 | ...v elektronik och mjukvara för **inbyggda system**   Vi och våra kunder använder... | x | x | 15 | 15 | [Inbyggda system/Embedded system, **skill**](http://data.jobtechdev.se/taxonomy/concept/XThy_HCH_FFc) |
| 18 | ...  Vi och våra kunder använder **svenska** och engelska i tal som i skri... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 19 | ...a kunder använder svenska och **engelska** i tal som i skrift och ser gä... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| | **Overall** | | | **102** | **140** | 102/140 = **73%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Git, versionshanteringssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/2REh_Drn_nsh) |
| x | x | x | [Testrail, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/AKY2_EnS_eG4) |
| x |  |  | [Applikationsutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CZkP_hCz_KM8) |
| x | x | x | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| x | x | x | [Testutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TU7g_mwa_VzB) |
| x | x | x | [Inbyggda system/Embedded system, **skill**](http://data.jobtechdev.se/taxonomy/concept/XThy_HCH_FFc) |
| x | x | x | [JIRA, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQoJ_GY7_n5Z) |
| x | x | x | [Selenium, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/i6kH_axQ_nH8) |
|  | x |  | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| x | x | x | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| x | x | x | [Gradle, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/m8mi_pid_qin) |
| x |  |  | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| x | x | x | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| x | x | x | [Jenkins, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/tmyt_n6R_1Gc) |
|  | x |  | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| x | x | x | [Linux, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/z52H_f4y_4ZA) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **14** | 14/19 = **74%** |