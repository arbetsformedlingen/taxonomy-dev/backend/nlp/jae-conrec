# Results for '2e2ffd3f3692a1f4208697616f6e4fdcccc39532'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2e2ffd3f3692a1f4208697616f6e4fdcccc39532](README.md) | 1 | 162 | 5 | 5 | 42/42 = **100%** | 4/4 = **100%** |

## Source text

Sushikock Söker sushikock mer erfarenhet för att jobba i vårt kök med att tillaga mat och sköta kökets alla uppgifter. kassavana, skall kunna svenska och engelska

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sushikock** Söker sushikock mer erfarenhe... | x | x | 9 | 9 | [Sushikock, **job-title**](http://data.jobtechdev.se/taxonomy/concept/YYSm_Pv3_znL) |
| 2 | Sushikock Söker **sushikock** mer erfarenhet för att jobba ... | x | x | 9 | 9 | [Sushikock, **job-title**](http://data.jobtechdev.se/taxonomy/concept/YYSm_Pv3_znL) |
| 3 | ... sköta kökets alla uppgifter. **kassavana**, skall kunna svenska och enge... | x | x | 9 | 9 | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| 4 | ...ifter. kassavana, skall kunna **svenska** och engelska | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 5 | ...vana, skall kunna svenska och **engelska** | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| | **Overall** | | | **42** | **42** | 42/42 = **100%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Sushikock, **job-title**](http://data.jobtechdev.se/taxonomy/concept/YYSm_Pv3_znL) |
| x | x | x | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/4 = **100%** |