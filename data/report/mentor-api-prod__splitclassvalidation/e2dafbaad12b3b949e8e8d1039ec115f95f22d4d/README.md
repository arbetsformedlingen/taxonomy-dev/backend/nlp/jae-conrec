# Results for 'e2dafbaad12b3b949e8e8d1039ec115f95f22d4d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e2dafbaad12b3b949e8e8d1039ec115f95f22d4d](README.md) | 1 | 378 | 12 | 13 | 37/233 = **16%** | 4/17 = **24%** |

## Source text

Snickare/ hus tillverkning /byggnadsarbete Vi söker duktiga med arbetare till nyproduktion av villor i Sollentuna/Stockholm, Snickare , målare, träarbetare , hus byggare. byggnads arbetare,du bör ha körkort, fler årig erfarenhet av hus bygge, lätt att sammanarbeta med andra. svara endast med email skicka med referenser från tidigare arbetsgivare, och CV. rubrik Bygg snickare.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Snickare**/ hus tillverkning /byggnadsar... | x |  |  | 8 | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| 1 | **Snickare**/ hus tillverkning /byggnadsar... |  | x |  | 8 | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| 2 | Snickare/ hus **tillverkning** /byggnadsarbete Vi söker dukt... |  | x |  | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 3 | Snickare/ hus tillverkning /**byggnadsarbete** Vi söker duktiga med arbetare... |  | x |  | 14 | [Snickeriarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jB4B_uED_vQh) |
| 3 | Snickare/ hus tillverkning /**byggnadsarbete** Vi söker duktiga med arbetare... |  | x |  | 14 | [Bygg, **occupation-collection**](http://data.jobtechdev.se/taxonomy/concept/ja7J_P8X_YC9) |
| 4 | ...till nyproduktion av villor i **Sollentuna**/Stockholm, Snickare , målare,... | x | x | 10 | 10 | [Sollentuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Z5Cq_SgB_dsB) |
| 5 | ...uktion av villor i Sollentuna/**Stockholm**, Snickare , målare, träarbeta... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 6 | ...illor i Sollentuna/Stockholm, **Snickare** , målare, träarbetare , hus b... | x |  |  | 8 | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| 6 | ...illor i Sollentuna/Stockholm, **Snickare** , målare, träarbetare , hus b... |  | x |  | 8 | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| 7 | ...lentuna/Stockholm, Snickare , **målare**, träarbetare , hus byggare. b... | x |  |  | 6 | [Målare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rv19_K1B_Fuo) |
| 7 | ...lentuna/Stockholm, Snickare , **målare**, träarbetare , hus byggare. b... |  | x |  | 6 | [Målare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/sNjB_LTB_shP) |
| 8 | ...Stockholm, Snickare , målare, **träarbetare** , hus byggare. byggnads arbet... | x | x | 11 | 11 | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| 8 | ...Stockholm, Snickare , målare, **träarbetare** , hus byggare. byggnads arbet... |  | x |  | 11 | [träarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FmH5_Pqk_6k3) |
| 9 | ...ckare , målare, träarbetare , **hus byggare**. byggnads arbetare,du bör ha ... | x |  |  | 11 | [Byggnadsingenjörer och byggnadstekniker, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/7aWn_mbK_YFc) |
| 10 | ...e, träarbetare , hus byggare. **byggnads arbetare**,du bör ha körkort, fler årig ... | x |  |  | 17 | [byggnadsarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/6WbC_Cxz_uCY) |
| 11 | .... byggnads arbetare,du bör ha **körkort**, fler årig erfarenhet av hus ... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 12 | ...s arbetare,du bör ha körkort, **fler årig erfarenhet** av hus bygge, lätt att samman... | x |  |  | 20 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 13 | ...kort, fler årig erfarenhet av **hus bygge**, lätt att sammanarbeta med an... | x |  |  | 9 | [Husbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/X9ta_N3H_1is) |
| 14 | ..., fler årig erfarenhet av hus **bygge**, lätt att sammanarbeta med an... |  | x |  | 5 | [Nyproduktion, bygge, **skill**](http://data.jobtechdev.se/taxonomy/concept/v3d9_u2H_pnf) |
| 15 | ...årig erfarenhet av hus bygge, **lätt att sammanarbeta med andra**. svara endast med email skick... | x |  |  | 31 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 16 | ...tsgivare, och CV. rubrik Bygg **snickare**. |  | x |  | 8 | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| | **Overall** | | | **37** | **233** | 37/233 = **16%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| x |  |  | [byggnadsarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/6WbC_Cxz_uCY) |
| x |  |  | [Byggnadsingenjörer och byggnadstekniker, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/7aWn_mbK_YFc) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [träarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FmH5_Pqk_6k3) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Husbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/X9ta_N3H_1is) |
| x | x | x | [Sollentuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Z5Cq_SgB_dsB) |
|  | x |  | [Snickeriarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jB4B_uED_vQh) |
|  | x |  | [Bygg, **occupation-collection**](http://data.jobtechdev.se/taxonomy/concept/ja7J_P8X_YC9) |
|  | x |  | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| x |  |  | [Målare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rv19_K1B_Fuo) |
|  | x |  | [Målare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/sNjB_LTB_shP) |
|  | x |  | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
|  | x |  | [Nyproduktion, bygge, **skill**](http://data.jobtechdev.se/taxonomy/concept/v3d9_u2H_pnf) |
| | | **4** | 4/17 = **24%** |