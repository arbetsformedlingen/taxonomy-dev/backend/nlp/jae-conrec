from conrec_utils import spec
from conrec_utils import common

mapped_index_alternatives_spec = spec.CollSpec(spec.CollSpec(spec.int_spec))

class StringMappingSettings:
    def __init__(self):
        self.unassigned_cost = 20
        self.local_window = 100

    def with_unassigned_cost(self, c):
        self.unassigned_cost = c
        return self

class StringMappingSolution:
    def __init__(self, cost, inds):
        self.cost = cost
        self.inds = inds

    def __repr__(self):
        return "StringMappingSolution({:s}, {:s})".format(
            str(self.cost), str(self.inds))

class DynaprogRecord:
    def __init__(self, at_index, prev_index, cost):
        self.at_index = at_index
        self.prev_index = prev_index
        self.cost = cost

    def choose_best(self, other):
        return other if (self.cost == None) or other.cost < self.cost else self

    def __repr__(self):
        return "Cip(prev={:s}, cost={:s} at index={:s})".format(
            str(self.prev_index), str(self.cost), str(self.at_index))

    
class StringMappingProblem:
    def __init__(self, mapped_index_alternatives, settings=None):
        mapped_index_alternatives_spec.check(mapped_index_alternatives)
        self.mapped_index_alternatives = mapped_index_alternatives
        self.length = len(mapped_index_alternatives)
        self.settings = settings if settings != None else StringMappingSettings()

    def state_range(self, at):
        return range(-1, len(self.mapped_index_alternatives[at]))

    def transition_cost(self, at, src_state, dst_state):
        if (src_state == -1) or (dst_state == -1):
            return 0
        else:
            return abs(self.mapped_index_alternatives[at][src_state] + 1 - self.mapped_index_alternatives[at+1][dst_state])

    def state_cost(self, at, state_index):
        return self.settings.unassigned_cost if state_index == -1 else 0

    def solve(self):
        
        if self.length == 0:
            return []

        table = [{i:DynaprogRecord(i, None, self.state_cost(0, i))
                  for i in self.state_range(0)}]

        for at in range(1, self.length):
            last = table[-1]
            m = {}
            
            # Loop over target state
            for i in self.state_range(at):
                best = DynaprogRecord(i, None, None)
                
                cost_i = self.state_cost(at, i)
                
                # Loop over source state
                for j in self.state_range(at-1):
                    best = best.choose_best(DynaprogRecord(
                        i, j, last[j].cost + cost_i + self.transition_cost(at-1, j, i)))
                
                m[i] = best
            table.append(m)

        # Unwind the table
        final_best = DynaprogRecord(None, None, None)
        for i in self.state_range(self.length-1):
            final_best = final_best.choose_best(table[self.length-1][i])

        best = final_best
        rev_state_inds = [best.at_index]
        for i in range(1, self.length):
            best = table[self.length-1-i][best.prev_index]
            rev_state_inds.append(best.at_index)

        result = [self.mapped_index_alternatives[at][i] if i != -1 else None
                  for (at, i) in enumerate(reversed(rev_state_inds))]
                          
        return StringMappingSolution(final_best.cost, result)

def _make_char2inds(source_text):
    char2inds = {}
    for (i, c) in enumerate(source_text):
        if not(c in char2inds):
            char2inds[c] = []
        char2inds[c].append(i)
    return char2inds

    
class JaeSentenceMapping:
    def __init__(self, source_text, sentences, solution, sentence_inds):
        self.source_text = source_text
        self.sentences = sentences
        self.solution = solution
        self.sentence_inds = sentence_inds
    
        
def jae_map_sentences(source_text, sentences, settings=None):
    settings = common.or_some(settings, StringMappingSettings())

    n = len(source_text)
    
    char2inds = _make_char2inds(source_text)
    chars = [c
             for sentence in sentences
             for c in sentence]
    f = n/(len(chars) - 1)
    mapped_index_alternatives = [[j
                                  for j in char2inds.get(char, [])
                                  if abs(j - f*i) <= settings.local_window]
                                 for (i, char) in enumerate(chars)]

    
    problem = StringMappingProblem(mapped_index_alternatives, settings)
    sol = problem.solve()

    offset = 0
    dst = []
    for sentence in sentences:
        next_offset = offset + len(sentence)
        dst.append(sol.inds[offset:next_offset])
        offset = next_offset
    return JaeSentenceMapping(source_text, sentences, sol, dst)



def jae_map_string(source_text, s, settings=None):
    settings = common.or_some(settings, StringMappingSettings())
    char2inds = _make_char2inds(source_text)

    mapped_index_alternatives = [char2inds.get(char, []) for char in s]
    problem = StringMappingProblem(mapped_index_alternatives, settings)
    return problem.solve()
    
