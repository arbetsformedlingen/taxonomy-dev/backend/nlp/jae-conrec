# Results for 'c786e76273e2d2fb78f184da7453918ab0ef5d37'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c786e76273e2d2fb78f184da7453918ab0ef5d37](README.md) | 1 | 1290 | 12 | 8 | 58/169 = **34%** | 5/14 = **36%** |

## Source text

Serveringspersonal - Lunds Gård Mat & Bar #jobbjustnu Om oss Lunds Gård Mat och Bar är belägen vid Skavsta Jakthandel i Nyköping. Vi serverar god varierande lunch samt helgmeny. Vi har ambitionen att vara den självklara mötesplatsen för alla. Här ska vänner, familj, kollegor och bekanta kunna umgås i en lantlig och avslappnad miljö med en bit god mat och service. Kvalifikationer Vi söker en duktig medarbetare som tycker om att arbeta med service. Du ska vara ansvarsfull, stresstålig och ha en god arbetsmoral. Har du tidigare arbetat inom restaurangbranschen är det meriterande. Körkort är ett krav då det är svårt att komma ut med buss. Vi ser gärna att du är serviceinriktad och kan hantera stressiga situationer. Har en bra inställning till arbetet och är tillmötesgående samt har en förmåga att ta egna initiativ och kan arbeta såväl självständigt som i team. Dina arbetsuppgifter Hålla restaurangen inbjudande och representativ, trevligt bemötande, bordsservering, kassahantering, städ och disk. Lättare barhantering och ett glatt humör. OBS. Tjänsten är en 50% tjänst måndag -fredag ibland lördagar Varmt välkommen att ansöka! Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Serveringspersonal** - Lunds Gård Mat & Bar #jobbj... | x |  |  | 18 | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| 1 | **Serveringspersonal** - Lunds Gård Mat & Bar #jobbj... |  | x |  | 18 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | ...ägen vid Skavsta Jakthandel i **Nyköping**. Vi serverar god varierande l... | x | x | 8 | 8 | [Nyköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KzvD_ePV_DKQ) |
| 3 | ...beta med service. Du ska vara **ansvarsfull**, stresstålig och ha en god ar... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 4 | ...ice. Du ska vara ansvarsfull, **stresstålig** och ha en god arbetsmoral. Ha... | x | x | 11 | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 5 | ... Har du tidigare arbetat inom **restaurangbranschen** är det meriterande. Körkort ä... | x |  |  | 19 | [Restaurangverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/CK7M_gYV_9ea) |
| 5 | ... Har du tidigare arbetat inom **restaurangbranschen** är det meriterande. Körkort ä... |  | x |  | 19 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 6 | ...branschen är det meriterande. **Körkort** är ett krav då det är svårt a... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 7 | ...att ta egna initiativ och kan **arbeta** såväl självständigt som i tea... | x |  |  | 6 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 8 | ...nitiativ och kan arbeta såväl **självständigt** som i team. Dina arbetsuppgif... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 9 | ...sentativ, trevligt bemötande, **bordsservering**, kassahantering, städ och dis... | x | x | 14 | 14 | [Bordsservering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5fAq_Squ_o7p) |
| 10 | ...gt bemötande, bordsservering, **kassahantering**, städ och disk. Lättare barha... | x | x | 14 | 14 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 11 | ...rdsservering, kassahantering, **städ** och disk. Lättare barhanterin... | x |  |  | 4 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 12 | ...ing, kassahantering, städ och **disk**. Lättare barhantering och ett... | x |  |  | 4 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 13 | ...tt humör. OBS. Tjänsten är en **50%** tjänst måndag -fredag ibland ... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| | **Overall** | | | **58** | **169** | 58/169 = **34%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Bordsservering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5fAq_Squ_o7p) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x |  |  | [Restaurangverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/CK7M_gYV_9ea) |
| x |  |  | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x | x | x | [Nyköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KzvD_ePV_DKQ) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
|  | x |  | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x | x | x | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **5** | 5/14 = **36%** |