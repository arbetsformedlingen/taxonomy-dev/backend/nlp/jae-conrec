# Results for '28dc2838ac582dfabacbc983ea17bd6b5db1f271'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [28dc2838ac582dfabacbc983ea17bd6b5db1f271](README.md) | 1 | 1339 | 9 | 8 | 66/72 = **92%** | 6/7 = **86%** |

## Source text

Världens bästa diskare sökes Om tjänsten  Vi söker nu ett antal diskare i världsklass för att förgylla vårt team och jobba hos våra kunder runt om i Stockholm.  Vem är du?  Du har minst sex månaders erfarenhet från liknande jobb i Sverige eller Norden. Du är van vid högt tryck, höga krav och ett rasande tempo.  En trivsam arbetsplats  Att arbeta hos oss på RestaurangAssistans är väldigt roligt, otroligt lärorikt och du skaffar dig snabbt erfarenheter som det annars kan ta år att uppnå. För den nyfikne finns möjlighet att prova många olika arbetsplatser, lära känna nya branschkollegor och skaffa dig ett brett nätverk i branschen. Du får dessutom frihet att välja när du vill arbeta och om du vill arbeta heltid, deltid eller extra. I vårt team erbjuds du regelbundna personalaktiviteter och utbildning. Vi har kollektivavtal och friskvårdsmöjligheter.  Om oss  RestaurangAssistans är Nordens ledande bemanningsföretag inom besöksnäringen. Vi har funnits i över 20 år och har kontor i Stockholm, Göteborg, Malmö och Oslo. Vi bemannar, rekryterar och outsourcar till hotellkedjor, stjärnkrogar, företagsrestauranger, kommuner, konferensanläggningar, cateringföretag och stora arenor. Våra ledord är effektivitet, kvalitet, flexibilitet och omtanke.  Ansök endast via länken nedan. Vi tar inte emot ansökningar via epost eller telefon.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Världens bästa **diskare** sökes Om tjänsten  Vi söker n... | x | x | 7 | 7 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| 2 | ...änsten  Vi söker nu ett antal **diskare** i världsklass för att förgyll... | x | x | 7 | 7 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| 3 | ...bba hos våra kunder runt om i **Stockholm**.  Vem är du?  Du har minst se... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 4 | ...farenhet från liknande jobb i **Sverige** eller Norden. Du är van vid h... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 5 | ...iteter och utbildning. Vi har **kollektivavtal** och friskvårdsmöjligheter.  O... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 6 | ... funnits i över 20 år och har **kontor** i Stockholm, Göteborg, Malmö ... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 7 | ...i över 20 år och har kontor i **Stockholm**, Göteborg, Malmö och Oslo. Vi... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 8 | ...r och har kontor i Stockholm, **Göteborg**, Malmö och Oslo. Vi bemannar,... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 9 | ...kontor i Stockholm, Göteborg, **Malmö** och Oslo. Vi bemannar, rekryt... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| | **Overall** | | | **66** | **72** | 66/72 = **92%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x | x | x | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **6** | 6/7 = **86%** |