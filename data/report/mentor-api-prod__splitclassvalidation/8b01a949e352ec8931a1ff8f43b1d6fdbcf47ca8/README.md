# Results for '8b01a949e352ec8931a1ff8f43b1d6fdbcf47ca8'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8b01a949e352ec8931a1ff8f43b1d6fdbcf47ca8](README.md) | 1 | 615 | 2 | 3 | 0/61 = **0%** | 1/4 = **25%** |

## Source text

Häckklippning Om företaget Veterankraft är en jobbplattform för ålders- eller avtalspensionärer som vill fortsätta att arbeta efter pensionen. Våra veteraner är vår styrka och vi hyr ut veteraner till privatpersoner, företag, föreningar och offentlig sektor. Med vår stora bank av veteraner med livserfarenhet och olika kompetenser täcker vi i stort sett allt inom tjänstesektorn, oavsett omfattning och innehåll.  Om tjänsten Vi erbjuder dig att få njuta av naturen på din arbetstid. Vi söker trädgårdsveteraner till nya spännande uppdrag.  Dina arbetsuppgifter Klippa häckar  Ansökan Välkommen in med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Häckklippning** Om företaget Veterankraft är ... |  | x |  | 13 | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| 1 | **Häckklippning** Om företaget Veterankraft är ... |  | x |  | 13 | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
| 2 | ...fentlig sektor. Med vår stora **bank** av veteraner med livserfarenh... |  | x |  | 4 | [Bank, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xqgq_CRg_5mv) |
| 3 | ...en på din arbetstid. Vi söker **trädgårdsveteraner** till nya spännande uppdrag.  ... | x |  |  | 18 | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| 4 | ...ppdrag.  Dina arbetsuppgifter **Klippa häckar**  Ansökan Välkommen in med din... | x |  |  | 13 | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
| | **Overall** | | | **0** | **61** | 0/61 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| x | x | x | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
| x |  |  | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
|  | x |  | [Bank, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xqgq_CRg_5mv) |
| | | **1** | 1/4 = **25%** |