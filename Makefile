.PHONY: process-dataset compute-frequencies concept-synonym-table

# Variables

enriched_concept_labels = data/enriched_concept_labels/enriched_concept_labels.json
concepts_per_synonym_table = data/enriched_concept_labels/concepts_per_synonym_table.csv
synonym2concept_frequencies = data/synonym2concept_frequencies.json


# Testing
test:
	poetry run python3 -m unittest discover

specific-test:
	poetry run python3 -m unittest tests.test_string_aligner


##### Main targets
$(enriched_concept_labels):
	./call_api.sh 'enrich_concept_labels()'

$(concepts_per_synonym_table): $(enriched_concept_labels)
	./call_api.sh 'build_concepts_per_synonym_table()'

$(synonym2concept_frequencies):
	./call_api.sh 'build_synonym_to_concept_frequencies()'

concept-synonym-table: $(concepts_per_synonym_table)
compute-frequencies: $(synonym2concept_frequencies)


# Main target to perform a full enrichment
process-dataset: $(concepts_per_synonym_table) $(synonym2concept_frequencies)
	./call_api.sh 'process_dataset()'

report:
	./call_api.sh 'make_report()'

start-server:
	./call_api.sh 'start_server()'

##### Extra targets, just for debugging etc.

