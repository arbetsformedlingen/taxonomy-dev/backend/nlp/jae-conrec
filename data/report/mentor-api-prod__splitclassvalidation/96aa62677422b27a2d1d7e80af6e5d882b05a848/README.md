# Results for '96aa62677422b27a2d1d7e80af6e5d882b05a848'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [96aa62677422b27a2d1d7e80af6e5d882b05a848](README.md) | 1 | 1770 | 20 | 13 | 129/245 = **53%** | 10/18 = **56%** |

## Source text

Content Manager / Fotograf Är du den vi söker? Autocars behöver nu förstärka sitt team med en Content Manager som har ett brinnande intresse för bilar, fotografering, film samt redigering. Det är många moment kring en bilförsäljning och för att allt ska bli rätt hela vägen så startar alltid processen hos en fotograf / annonsproducent  Rollen är bred och varierande till sin karaktär, kräver hög arbetskapacitet och förmåga att prioritera saker i rätt ordning. Företaget är under kraftig expansion och lägger stor vikt i digitalisering. Vi ställer höga krav på bild/filmkvalité samt goda kunskaper i sociala medier.  Din roll kommer att bestå av sociala medier, fotografering, filmning, redigering samt annonsering.  Du ska ha god datavana samt erfarenhet av att jobba med olika redigeringsprogram så som Lightroom, Photoshop samt Final Cut Pro alt Premiere PRO.  Vi söker dig som är utåtriktad och tar stort ansvar för dina arbetsuppgifter. Du ska kunna ta egna initiativ och på egen hand driva ditt arbete framåt. Du är lättlärd och bra på att skapa effektivt samarbete med dina kollegor. Andra egenskaper som vi uppskattar är att du värdesätter god stämning på arbetsplatsen och förstår din egen roll i teamet. Krav  B-körkort Goda kunskaper i svenska tal som skrift. Anställningen är 100% Vi är ett företag som expanderar där du nu har möjlighet att påverka din egen roll samt utveckling i företaget. Våren 2022 stod vår nybyggda anläggning klar på bästa AA läge och vi ser fram emot fortsatt expandering. Låter detta intressant? Skicka då in din ansökan ihop med CV och gärna bilder eller videopresentation snarast. Ansökningar mottages enbart via mejl. Urval sker löpande  Mail : jobb@autocars.nu För frågor angående tjänsten kontakta Gustav Karnevi  0500 400 483

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Content Manager** / Fotograf Är du den vi söker... | x | x | 15 | 15 | [Content manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yDfK_MAc_L1d) |
| 2 | Content Manager / **Fotograf** Är du den vi söker? Autocars ... | x | x | 8 | 8 | [Fotograf, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1N2U_6xW_Tdx) |
| 3 | ...nu förstärka sitt team med en **Content Manager** som har ett brinnande intress... | x | x | 15 | 15 | [Content manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yDfK_MAc_L1d) |
| 4 | ...brinnande intresse för bilar, **fotografering**, film samt redigering. Det är... |  | x |  | 13 | [Fotografering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zpaf_mDu_foK) |
| 5 | ...artar alltid processen hos en **fotograf** / annonsproducent  Rollen är ... | x | x | 8 | 8 | [Fotograf, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1N2U_6xW_Tdx) |
| 6 | ...d processen hos en fotograf / **annonsproducent**  Rollen är bred och varierand... | x | x | 15 | 15 | [Annonsproducent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Cya6_5QD_RTL) |
| 7 | ...ring. Vi ställer höga krav på **bild/filmkvalité** samt goda kunskaper i sociala... | x |  |  | 16 | [skapa digitala bilder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KNi2_PZu_uk1) |
| 8 | ...kvalité samt goda kunskaper i **sociala medier**.  Din roll kommer att bestå a... | x |  |  | 14 | [hålla sig uppdaterad med sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1TCA_LMG_Kxr) |
| 9 | ... Din roll kommer att bestå av **sociala medier**, fotografering, filmning, red... | x |  |  | 14 | [hantering av sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MgRt_szJ_YYJ) |
| 10 | ... att bestå av sociala medier, **fotografering**, filmning, redigering samt an... | x | x | 13 | 13 | [Fotografering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zpaf_mDu_foK) |
| 11 | ...ociala medier, fotografering, **filmning**, redigering samt annonsering.... | x |  |  | 8 | (not found in taxonomy) |
| 12 | ...ier, fotografering, filmning, **redigering** samt annonsering.  Du ska ha ... | x |  |  | 10 | [Redigering, rörlig bild, **skill**](http://data.jobtechdev.se/taxonomy/concept/Eizh_YsE_amu) |
| 13 | ...ng, filmning, redigering samt **annonsering**.  Du ska ha god datavana samt... | x |  |  | 11 | [Annonsproduktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/nvM6_J6v_qKQ) |
| 14 | ...t annonsering.  Du ska ha god **datavana** samt erfarenhet av att jobba ... | x |  |  | 8 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 15 | ...renhet av att jobba med olika **redigeringsprogram** så som Lightroom, Photoshop s... | x |  |  | 18 | [redigeringsprogramvara, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nhKU_6Rj_aqy) |
| 16 | ...ika redigeringsprogram så som **Lightroom**, Photoshop samt Final Cut Pro... | x | x | 9 | 9 | [Adobe Lightroom, **skill**](http://data.jobtechdev.se/taxonomy/concept/puiy_Nb9_yUF) |
| 17 | ...ingsprogram så som Lightroom, **Photoshop** samt Final Cut Pro alt Premie... | x | x | 9 | 9 | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
| 18 | ...som Lightroom, Photoshop samt **Final Cut** Pro alt Premiere PRO.  Vi sök... | x | x | 9 | 9 | [Film/redigering-Apple Final Cut Pro, **skill**](http://data.jobtechdev.se/taxonomy/concept/VVj2_4Uu_LL9) |
| 19 | ...room, Photoshop samt Final Cut** Pro** alt Premiere PRO.  Vi söker d... | x |  |  | 4 | [Film/redigering-Apple Final Cut Pro, **skill**](http://data.jobtechdev.se/taxonomy/concept/VVj2_4Uu_LL9) |
| 20 | ...toshop samt Final Cut Pro alt **Premiere PRO**.  Vi söker dig som är utåtrik... | x | x | 12 | 12 | [Film/redigering-Adobe Premiere, **skill**](http://data.jobtechdev.se/taxonomy/concept/GCMj_xLD_mj6) |
| 21 | ...din egen roll i teamet. Krav  **B-körkort** Goda kunskaper i svenska tal ... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 22 | ...v  B-körkort Goda kunskaper i **svenska** tal som skrift. Anställningen... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **129** | **245** | 129/245 = **53%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Fotograf, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1N2U_6xW_Tdx) |
| x |  |  | [hålla sig uppdaterad med sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1TCA_LMG_Kxr) |
| x | x | x | [Annonsproducent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Cya6_5QD_RTL) |
| x |  |  | [Redigering, rörlig bild, **skill**](http://data.jobtechdev.se/taxonomy/concept/Eizh_YsE_amu) |
| x | x | x | [Film/redigering-Adobe Premiere, **skill**](http://data.jobtechdev.se/taxonomy/concept/GCMj_xLD_mj6) |
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x |  |  | [skapa digitala bilder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KNi2_PZu_uk1) |
| x |  |  | [hantering av sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MgRt_szJ_YYJ) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Film/redigering-Apple Final Cut Pro, **skill**](http://data.jobtechdev.se/taxonomy/concept/VVj2_4Uu_LL9) |
| x |  |  | (not found in taxonomy) |
| x |  |  | [redigeringsprogramvara, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nhKU_6Rj_aqy) |
| x |  |  | [Annonsproduktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/nvM6_J6v_qKQ) |
| x | x | x | [Adobe Lightroom, **skill**](http://data.jobtechdev.se/taxonomy/concept/puiy_Nb9_yUF) |
| x | x | x | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
| x | x | x | [Content manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yDfK_MAc_L1d) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [Fotografering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zpaf_mDu_foK) |
| | | **10** | 10/18 = **56%** |