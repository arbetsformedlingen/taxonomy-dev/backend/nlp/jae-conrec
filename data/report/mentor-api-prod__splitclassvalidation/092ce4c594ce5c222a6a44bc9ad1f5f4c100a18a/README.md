# Results for '092ce4c594ce5c222a6a44bc9ad1f5f4c100a18a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [092ce4c594ce5c222a6a44bc9ad1f5f4c100a18a](README.md) | 1 | 5303 | 45 | 29 | 88/700 = **13%** | 9/23 = **39%** |

## Source text

Projektledare systemhandling Göteborg-Borås, Nya Stambanor Som projektledare är du med och underlättar vardagen för invånare och företagare i hela Sverige. Det gör du genom att vara med och forma framtidens infrastruktur. De nya stambanorna ska i framtiden länka samman de tre storstadsregionerna Stockholm, Göteborg och Malmö. Sträckan Göteborg-Borås är ett av Sveriges största pendlingsstråk och en av de första delarna av en ny stambana. Med den nya järnvägen kan restiden mellan Göteborg och Borås kraftigt förbättras och kapaciteten öka. Det blir då mer självklart att välja den hållbara tågresan.   Projektledare systemhandling Göteborg-Borås, Nya Stambanor  Arbetsuppgifter  Om jobbet Göteborg-Borås växlar upp inför nästa skede i planeringen och vi söker nu en Projektledare systemhandling. Du kommer att ingå i en projektgrupp som leder en stor och omfattande järnvägsplan inklusive systemhandling och MKB med avseende på tid, kostnad och innehåll samt risker. I rollen ingår ansvar för att leda, styra och följa upp arbetet med framtagande, leverans och kvalitetssäkring av produkten systemhandling. I projektledningen finns även roller som projektledare järnvägsplan, projektledare miljöbedömning, samt en övergripande projektledare. Din projektgrupp är en av flera inom Göteborg-Borås och ingår i projektenheten. I Göteborg-Borås kommer du möta kompetenta och engagerade kollegor som alla arbetar för Trafikverkets vision - alla kommer fram smidigt, grönt och tryggt. Jag som blir din chef heter Janne Johansson. I mitt ledarskap lägger jag stor vikt vid att skapa förutsättningar för god samverkan och framdrift. Som en del av mitt team får du möta engagerade, positiva och drivande kollegor. I rollen som projektledare kommer du ha många olika kontaktytor och lära känna många nya människor. Jag hoppas att du vill vara med och utveckla ett modernt, effektivt och hållbart transportsystem tillsammans med oss! Din utveckling Som projektledare i Trafikverket är utvecklingsmöjligheterna stora. Vi vill att du ska nå din fulla potential, oavsett om du framåt vill bredda dina kunskaper, kliva in i en chefsroll eller specialisera dig. Du kommer att tillhöra en av Sveriges största beställarorganisationer och den samlade projektledarkompetensen hos våra cirka 1 000 projektledare är unik. Som projektledare har du tillgång till olika forum för erfarenhetsutbyten, nätverk, utbildningar samt talang- och utvecklingsprogram. Trafikverket kan erbjuda dig en modern arbetsplats där vi möter varandra med respekt och omsorg i en öppen och nyskapande kultur. Hos oss lönar sig alltid goda prestationer, men vi är samtidigt noggranna med att skapa förutsättningar för en bra balans mellan arbete, familj och fritid. Vi är stolta över att ha utsetts till en av Sveriges bästa arbetsgivare.   Övrig information  I denna rekrytering används webbaserade tester som en del i urvalet. Intervjuer kommer att genomföras digitalt under vecka 38. #Goteborgboras #LI-Hybrid #LI-PS1  Kvalifikationer  Ditt ledarskap Ditt ledarskap präglas av att du bygger förtroende, klargör riktning och levererar resultat. Genom en tydlig kommunikation om förväntningar och mål delegerar du och följer upp. Du inspirerar, skapar delaktighet och driver utvecklingen tillsammans med ditt team och i samverkan med andra. Du är proaktiv, strukturerad och kvalitetsmedveten i dina åtaganden och har en god analytisk förmåga.   Vi söker dig som har högskoleutbildning 180hp (120p) inom teknik eller miljö alternativt annan utbildning i kombination med erfarenhet som vi bedömer likvärdig har aktuell och flerårig relevant erfarenhet av projektledning inom anläggningsbranschen kan uttrycka sig väl i tal och skrift på svenska har körkort för personbil  Det är meriterande om du har erfarenhet av projektledning av stora infrastrukturprojekt har erfarenhet av både projekterings- och byggskede har kompetens inom järnvägsteknik    Ansökan  De frågor som du får besvara när du skickar in din ansökan kommer att ligga till grund för det första urvalet vi gör.  Som sökande till Trafikverket kan du eventuellt behöva gå igenom en säkerhetsprövning. Den innehåller säkerhetsprövningssamtal och registerkontroll innan anställning, om tjänsten är placerad i säkerhetsklass. I vissa fall krävs svenskt medborgarskap för säkerhetsklassade tjänster. På Nya Stambanor är du med och skriver historia genom att bygga ny järnväg som möjliggör ökad kapacitet för gods- och persontrafiken mellan knutpunkterna Stockholm, Göteborg och Malmö. Vi arbetar med helheten - från planering till genomförande och överlämning för drift och underhåll. Du jobbar i stora och komplexa projekt med kunniga kollegor och tillsammans skapar vi nya möjligheter att resa hållbart i Sverige och även ut i Europa. På Trafikverket jobbar vi med att göra Sverige närmare. Vi tror på att en förening av olika erfarenheter, bakgrunder och perspektiv är den bästa grunden för att bygga en arbetsplats fri från diskriminering och en miljö där alla trivs och gör sitt bästa. Vi tror på mångfald där den gemensamma nämnaren är pålitlighet, engagemang och mod. För det är så vi bäst representerar vårt samhälle.  Läs mer här  https://www.trafikverket.se/vara-projekt/projekt-som-stracker-sig-over-flera-lan/nya-stambanor-mellan-stockholm-goteborg-och-malmo/Goteborg-Boras/

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Projektledare** systemhandling Göteborg-Borås... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 1 | **Projektledare** systemhandling Göteborg-Borås... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 2 | Projektledare systemhandling **Göteborg**-Borås, Nya Stambanor Som proj... | x |  |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 3 | ...edare systemhandling Göteborg-**Borås**, Nya Stambanor Som projektled... | x |  |  | 5 | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| 4 | ...borg-Borås, Nya Stambanor Som **projektledare** är du med och underlättar var... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 4 | ...borg-Borås, Nya Stambanor Som **projektledare** är du med och underlättar var... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 5 | ...nvånare och företagare i hela **Sverige**. Det gör du genom att vara me... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 6 | ...an de tre storstadsregionerna **Stockholm**, Göteborg och Malmö. Sträckan... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 7 | ...torstadsregionerna Stockholm, **Göteborg** och Malmö. Sträckan Göteborg-... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 8 | ...nerna Stockholm, Göteborg och **Malmö**. Sträckan Göteborg-Borås är e... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 9 | ... Göteborg och Malmö. Sträckan **Göteborg**-Borås är ett av Sveriges stör... | x |  |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 10 | ... och Malmö. Sträckan Göteborg-**Borås** är ett av Sveriges största pe... | x |  |  | 5 | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| 11 | ...ckan Göteborg-Borås är ett av **Sveriges** största pendlingsstråk och en... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 12 | ...järnvägen kan restiden mellan **Göteborg** och Borås kraftigt förbättras... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 13 | ... restiden mellan Göteborg och **Borås** kraftigt förbättras och kapac... | x |  |  | 5 | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| 14 | ...ja den hållbara tågresan.   **Projektledare** systemhandling Göteborg-Borås... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 15 | ... Projektledare systemhandling **Göteborg**-Borås, Nya Stambanor  Arbet... | x |  |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 16 | ...edare systemhandling Göteborg-**Borås**, Nya Stambanor  Arbetsuppgi... | x |  |  | 5 | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| 17 | ...  Arbetsuppgifter  Om jobbet **Göteborg**-Borås växlar upp inför nästa ... | x |  |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 18 | ...uppgifter  Om jobbet Göteborg-**Borås** växlar upp inför nästa skede ... | x |  |  | 5 | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| 19 | ...laneringen och vi söker nu en **Projektledare** systemhandling. Du kommer att... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 19 | ...laneringen och vi söker nu en **Projektledare** systemhandling. Du kommer att... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 20 | ...med framtagande, leverans och **kvalitetssäkring** av produkten systemhandling. ... | x | x | 16 | 16 | [Kvalitetssäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/a1Sf_mey_gVk) |
| 21 | ...dningen finns även roller som **projektledare** järnvägsplan, projektledare m... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 21 | ...dningen finns även roller som **projektledare** järnvägsplan, projektledare m... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 22 | ...m projektledare järnvägsplan, **projektledare** miljöbedömning, samt en överg... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 23 | ...dömning, samt en övergripande **projektledare**. Din projektgrupp är en av fl... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 24 | ...jektgrupp är en av flera inom **Göteborg**-Borås och ingår i projektenhe... | x |  |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 25 | ... är en av flera inom Göteborg-**Borås** och ingår i projektenheten. I... | x |  |  | 5 | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| 26 | ...och ingår i projektenheten. I **Göteborg**-Borås kommer du möta kompeten... | x |  |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 27 | ... i projektenheten. I Göteborg-**Borås** kommer du möta kompetenta och... | x |  |  | 5 | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| 28 | ...ivande kollegor. I rollen som **projektledare** kommer du ha många olika kont... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 28 | ...ivande kollegor. I rollen som **projektledare** kommer du ha många olika kont... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 29 | ...s med oss! Din utveckling Som **projektledare** i Trafikverket är utvecklings... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 29 | ...s med oss! Din utveckling Som **projektledare** i Trafikverket är utvecklings... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 30 | ... Du kommer att tillhöra en av **Sveriges** största beställarorganisation... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 31 | ...petensen hos våra cirka 1 000 **projektledare** är unik. Som projektledare ha... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 32 | ...00 projektledare är unik. Som **projektledare** har du tillgång till olika fo... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 32 | ...00 projektledare är unik. Som **projektledare** har du tillgång till olika fo... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 33 | ...pa förutsättningar för en bra **balans** mellan arbete, familj och fri... | x |  |  | 6 | [Balans, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y3fx_8wR_Zim) |
| 34 | ...rar resultat. Genom en tydlig **kommunikation** om förväntningar och mål dele... | x |  |  | 13 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 35 | ...rmåga.   Vi söker dig som har **högskoleutbildning** 180hp (120p) inom teknik elle... | x |  |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 35 | ...rmåga.   Vi söker dig som har **högskoleutbildning** 180hp (120p) inom teknik elle... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 36 | ...ig som har högskoleutbildning **180hp** (120p) inom teknik eller milj... | x |  |  | 5 | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| 37 | ...har högskoleutbildning 180hp (**120p**) inom teknik eller miljö alte... | x |  |  | 4 | [Högskoleutbildning, generell, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/rqLj_s2C_LTJ) |
| 38 | ...mer likvärdig har aktuell och **flerårig relevant erfarenhet av projektledning** inom anläggningsbranschen kan... | x |  |  | 46 | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| 39 | ...erårig relevant erfarenhet av **projektledning** inom anläggningsbranschen kan... |  | x |  | 14 | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| 40 | ...renhet av projektledning inom **anläggningsbranschen** kan uttrycka sig väl i tal oc... | x |  |  | 20 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 41 | ...a sig väl i tal och skrift på **svenska** har körkort för personbil  De... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 42 | ...tal och skrift på svenska har **körkort** för personbil  Det är meriter... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 43 | ... skrift på svenska har körkort** för personbil**  Det är meriterande om du har... | x |  |  | 14 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 44 | ... Det är meriterande om du har **erfarenhet av projektledning** av stora infrastrukturprojekt... | x |  |  | 28 | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| 45 | ...rande om du har erfarenhet av **projektledning** av stora infrastrukturprojekt... |  | x |  | 14 | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| 46 | ...rojekt har erfarenhet av både **projekterings**- och byggskede har kompetens ... | x |  |  | 13 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 47 | ...et av både projekterings- och **byggskede** har kompetens inom järnvägste... | x |  |  | 9 | [Bygg och anläggning, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/j7Cq_ZJe_GkT) |
| 48 | ... byggskede har kompetens inom **järnvägsteknik**    Ansökan  De frågor som du ... | x | x | 14 | 14 | [Järnvägsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Etfq_CqE_Da5) |
| 49 | ...hetsklass. I vissa fall krävs **svenskt medborgarskap** för säkerhetsklassade tjänste... | x |  |  | 21 | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| 50 | ...r historia genom att bygga ny **järnväg** som möjliggör ökad kapacitet ... | x | x | 7 | 7 | [Järnväg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HSeG_zAZ_SpC) |
| 51 | ...trafiken mellan knutpunkterna **Stockholm**, Göteborg och Malmö. Vi arbet... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 52 | ...llan knutpunkterna Stockholm, **Göteborg** och Malmö. Vi arbetar med hel... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 53 | ...terna Stockholm, Göteborg och **Malmö**. Vi arbetar med helheten - fr... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 54 | ...jligheter att resa hållbart i **Sverige** och även ut i Europa. På Traf... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 55 | ...verket jobbar vi med att göra **Sverige** närmare. Vi tror på att en fö... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **88** | **700** | 88/700 = **13%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Järnvägsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Etfq_CqE_Da5) |
| x | x | x | [Järnväg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HSeG_zAZ_SpC) |
|  | x |  | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x |  |  | [Borås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/TpRZ_bFL_jhL) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Balans, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y3fx_8wR_Zim) |
| x | x | x | [Kvalitetssäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/a1Sf_mey_gVk) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x |  |  | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Bygg och anläggning, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/j7Cq_ZJe_GkT) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
|  | x |  | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x |  |  | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| x |  |  | [Högskoleutbildning, generell, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/rqLj_s2C_LTJ) |
| x |  |  | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| x |  |  | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **9** | 9/23 = **39%** |