# Results for '4163717d87b581f13937a547445551413bc0e27e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4163717d87b581f13937a547445551413bc0e27e](README.md) | 1 | 2680 | 6 | 8 | 8/140 = **6%** | 1/11 = **9%** |

## Source text

Vi söker en junior Sketchup Designer till Borås! Är du utbildad inom 3D design och vill utvecklas inom visualisering och arkitektur? Då kan detta vara en tjänst för dig! I rollen som Sketchup designer hos vår kund får du möjligheten att arbeta med kompetenta kollegor i en familjär miljö där kvalitet och utveckling är i fokus. Låter detta som en tjänst för dig? - Sök redan idag då vi arbetar med löpnade urval!  OM TJÄNSTEN  Vår kund är ett ledande globalt teknikbolag som driver omställningen av samhälle och industri för att uppnå en mer produktiv och hållbar framtid. Genom att koppla samman mjukvara med produkter inom electrification, robotics, automation och motion skapar ABB lösningar som driver teknikens möjligheter till nya höjder.  På kontoret i Borås hittar du 20 goa kollegor i roller som projektledare, industridesigners och säljare. De har en god sammanhållning och stöttar varandra där det behövs. För att trivas i denna roll ser vi att du har ett stort intresse för grafik och att du har en känsla för design och form. Du kommer att få en gedigen introduktion med en presentation av företaget och en intern upplärning för att sedan gå bredvid en kollega under din första period. Allt för att du ska få rätt förutsättningar för att lyckas i denna roll!  Du erbjuds   * Möjlighet att utvecklas inom området * En hängiven konsultchef och karriärspartner   Som konsult för Academic Work erbjuder vi stora möjligheter för dig att växa professionellt, bygga ditt nätverk och skapa värdefulla kontakter för framtiden. Läs mer om vårt konsulterbjudande. Detta är ett uppdrag på initialt 6 månader som eventuellt kan förlängas beroende på behov.  ARBETSUPPGIFTER  - Designa kontrollrum, rita upp väggar, fönster, mm. - En del av arbetet är inom arkitektur  Du kommer att förvandla idéer och skisser till visualiseringar som speglar hur koncepten kommer att se ut och utgör en viktig del i underlag till kunderna.  VI SÖKER DIG SOM  - Har en utbildning inom 3D design, exempelvis inom produkt eller arkitektur - Har någon erfarenhet av att arbeta med 3D visualisering i Sketchup eller liknande modellerings- och presentationsprogram - Trivs med att arbeta självständigt med datorn som främsta verktyg  Som person är du:   * Flexibel * Noggrann * Har en god samarbetsförmåga   Övrig information   * Start: Enligt överenskommelse, dock senast i början av augusti * Omfattning: Heltid * Placering: Borås * Rekryteringsprocessen hanteras av Academic Work och kundens önskemål är att alla frågor rörande tjänsten hanteras av Academic Work. * Vi går igenom urvalet löpande och annonsen kan stängas ner innan tjänsten är tillsatt om vi gått över till urvals- och intervjufasen.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Vi söker en junior **Sketchup** Designer till Borås! Är du ut... |  | x |  | 8 | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| 2 | ... tjänst för dig! I rollen som **Sketchup** designer hos vår kund får du ... |  | x |  | 8 | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| 3 | ...ion, robotics, automation och **motion** skapar ABB lösningar som driv... |  | x |  | 6 | [agera med motion capture-utrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r3Jg_Qvy_5vd) |
| 4 | ... 20 goa kollegor i roller som **projektledare**, industridesigners och säljar... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 5 | ...SOM  - Har en utbildning inom **3D design**, exempelvis inom produkt elle... | x |  |  | 9 | [Grafisk design och foto, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/Kn2j_2xo_sxz) |
| 6 | ...exempelvis inom produkt eller **arkitektur** - Har någon erfarenhet av att... | x |  |  | 10 | [Arkitektutbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/wKN6_w4L_aS6) |
| 7 | ...arbeta med 3D visualisering i **Sketchup** eller liknande modellerings- ... | x |  |  | 8 | [Arkitekt-SketchUp 3D, **skill**](http://data.jobtechdev.se/taxonomy/concept/g7bY_vJ5_XC8) |
| 7 | ...arbeta med 3D visualisering i **Sketchup** eller liknande modellerings- ... |  | x |  | 8 | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| 8 | ...er liknande modellerings- och **presentationsprogram** - Trivs med att arbeta självs... |  | x |  | 20 | [Presentationsprogram, undervisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/Meb7_yEj_U7b) |
| 9 | ...ationsprogram - Trivs med att **arbeta självständigt** med datorn som främsta verkty... |  | x |  | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 10 | ... att arbeta självständigt med **datorn** som främsta verktyg  Som pers... | x |  |  | 6 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 11 | ... person är du:   * Flexibel * **Noggrann** * Har en god samarbetsförmåga... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 12 | ...xibel * Noggrann * Har en god **samarbetsförmåga**   Övrig information   * Start... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| | **Overall** | | | **8** | **140** | 8/140 = **6%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x |  |  | [Grafisk design och foto, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/Kn2j_2xo_sxz) |
|  | x |  | [Presentationsprogram, undervisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/Meb7_yEj_U7b) |
|  | x |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Arkitekt-SketchUp 3D, **skill**](http://data.jobtechdev.se/taxonomy/concept/g7bY_vJ5_XC8) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
|  | x |  | [agera med motion capture-utrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r3Jg_Qvy_5vd) |
| x |  |  | [Arkitektutbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/wKN6_w4L_aS6) |
|  | x |  | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| | | **1** | 1/11 = **9%** |