# Results for '947cc26f2d0b57898bc4daa49d7bdc4d47643034'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [947cc26f2d0b57898bc4daa49d7bdc4d47643034](README.md) | 1 | 2672 | 23 | 13 | 117/279 = **42%** | 10/20 = **50%** |

## Source text

Producent till P3 Nyheter Producent till P3 Nyheter  Vill du jobba på en av Sveriges mest kreativa och utmanande nyhetsredaktioner? Då ska du söka dig till P3 Nyheter. Vi söker en nyskapande och kreativ producent så vänta inte med din ansökan!  P3 Nyheter är en rikstäckande nyhetsredaktion med den unga publiken i fokus. Som P3:s samhällsjournalistiska nav har P3 Nyheter Sveriges största nyhetskonto på Instagram, hundratusentals lyssnare i radion och nyhetspoddar i framkant. Som en del av vårt producentteam ansvarar du för nyhetsarbetet på redaktionen och arbetsleder reportrar, digitala redaktörer och programledare. Du navigerar genom världens nyhetsflöden och sätter agendan för P3:s publik, både i radion och i sociala medier. Du är placerad i Stockholm men har medarbetare även i Göteborg, Luleå, Umeå och Malmö. Du växlar mellan att jobba morgon- och dagpass.  Vi söker dig som vill utmana det klassiska nyhetsberättandet sett till vinklar, form och tilltal. Du som söker ska också ha intresse och erfarenhet av journalistiskt arbete i sociala medier. Mitt i händelsernas centrum blir du en viktig del av ett företag som värnar om demokrati, det fria ordet och alla människors lika värde.  Bakgrund och kvalifikationer   • Du har erfarenhet av nyhetsarbete och är van vid intensiva arbetssituationer  • Du har mycket god kännedom om P3:s målgrupp  • Du har journalistisk arbetsledarerfarenhet och van vid nyhetsbedömningar  • Du har förmåga att uttrycka dig väl i tal och skrift  • Du har dokumenterad erfarenhet av journalistiskt arbete och publicering i digitala och sociala medier  • Du har bred allmänbildning  • Du är strukturerad, kreativ och stabil  • Journalistisk utbildning är meriterande   Vi tycker att det är viktigt att Sveriges Radio speglar samhället och strävar hela tiden efter att öka antalet röster i vårt utbud. Det är en fördel om du har ett brett kontaktnät i olika delar av samhället och kan flera språk.  Anställning  Tjänsten är en tillsvidareanställning med start under hösten. Tillträde sker enligt överenskommelse. Tjänsten innebär delvis arbete tidiga morgnar, men inte helger.  Ansökan Vi ser gärna att du skickar in din ansökan omgående för vi arbetar med löpande urval. Registrera din ansökan senast 21 augusti 2022.  Vill du veta mer? För frågor kring tjänsten kontakta Isabelle Swahn, nyhetschef, P3 Nyheter, isabelle.swahn@sr.se  För frågor kring din ansökan kontakta Aleksandar Velevski, HR-Partner, aleksandar.velevski@sr.se eller tel. 08- 784 06 86  Fackliga frågor besvaras av: Pierre Martin - Journalistklubben, Linda Nordeman, Unionen. Båda nås via Sveriges Radios växel 08-784 50 00.   Varmt välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Producent** till P3 Nyheter Producent til... | x | x | 9 | 9 | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| 2 | Producent till P3 Nyheter **Producent** till P3 Nyheter  Vill du jobb... | x | x | 9 | 9 | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| 3 | ...heter  Vill du jobba på en av **Sveriges** mest kreativa och utmanande n... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ...ker en nyskapande och kreativ **producent** så vänta inte med din ansökan... | x | x | 9 | 9 | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| 5 | ...P3 Nyheter är en rikstäckande **nyhetsredaktion** med den unga publiken i fokus... | x | x | 15 | 15 | [Nyhetsredaktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/27SK_YN7_i8o) |
| 6 | ...nalistiska nav har P3 Nyheter **Sveriges** största nyhetskonto på Instag... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 7 | ...å redaktionen och arbetsleder **reportrar**, digitala redaktörer och prog... | x |  |  | 9 | [Journalist/Reporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BduY_UUY_pvK) |
| 8 | ...betsleder reportrar, digitala **redaktörer** och programledare. Du naviger... | x |  |  | 10 | [Redaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GFu2_s4q_9sr) |
| 8 | ...betsleder reportrar, digitala **redaktörer** och programledare. Du naviger... |  | x |  | 10 | [redaktör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tRiS_ixp_5bD) |
| 9 | ...trar, digitala redaktörer och **programledare**. Du navigerar genom världens ... | x | x | 13 | 13 | [programledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/NAsj_7ev_ReY) |
| 10 | ...iala medier. Du är placerad i **Stockholm** men har medarbetare även i Gö... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 11 | ...lm men har medarbetare även i **Göteborg**, Luleå, Umeå och Malmö. Du vä... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 12 | ... medarbetare även i Göteborg, **Luleå**, Umeå och Malmö. Du växlar me... | x | x | 5 | 5 | [Luleå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CXbY_gui_14v) |
| 13 | ...etare även i Göteborg, Luleå, **Umeå** och Malmö. Du växlar mellan a... | x | x | 4 | 4 | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| 14 | ...n i Göteborg, Luleå, Umeå och **Malmö**. Du växlar mellan att jobba m... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 15 | ...ha intresse och erfarenhet av **journalistiskt arbete** i sociala medier. Mitt i händ... | x |  |  | 21 | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| 16 | ...om om P3:s målgrupp  • Du har **journalistisk** arbetsledarerfarenhet och van... | x |  |  | 13 | [journalistik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LJcK_EFr_W8G) |
| 17 | ...grupp  • Du har journalistisk **arbetsledarerfarenhet** och van vid nyhetsbedömningar... | x | x | 21 | 21 | [Arbetsledarerfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/j1Yr_UhC_X16) |
| 18 | ...ar dokumenterad erfarenhet av **journalistiskt arbete** och publicering i digitala oc... | x |  |  | 21 | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| 19 | ...urerad, kreativ och stabil  • **Journalistisk utbildning** är meriterande   Vi tycker at... | x |  |  | 24 | [Journalistik och information, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/MwbE_Nvv_VN4) |
| 20 | ...t det är viktigt att Sveriges **Radio** speglar samhället och strävar... | x |  |  | 5 | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| 21 | ...ka delar av samhället och kan **flera språk**.  Anställning  Tjänsten är en... | x |  |  | 11 | [Flera språk, **language**](http://data.jobtechdev.se/taxonomy/concept/o8Xp_wvY_5pu) |
| 22 | ...  Anställning  Tjänsten är en **tillsvidareanställning** med start under hösten. Tillt... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 23 | ...kontakta Aleksandar Velevski, **HR-Partner**, aleksandar.velevski@sr.se el... | x | x | 10 | 10 | [HR-generalist/HR-partner, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ds7X_mdp_bPc) |
| | **Overall** | | | **117** | **279** | 117/279 = **42%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Nyhetsredaktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/27SK_YN7_i8o) |
| x | x | x | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| x |  |  | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Journalist/Reporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BduY_UUY_pvK) |
| x | x | x | [Luleå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CXbY_gui_14v) |
| x |  |  | [Redaktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GFu2_s4q_9sr) |
| x |  |  | [journalistik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LJcK_EFr_W8G) |
| x |  |  | [Journalistik och information, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/MwbE_Nvv_VN4) |
| x | x | x | [programledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/NAsj_7ev_ReY) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| x | x | x | [HR-generalist/HR-partner, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ds7X_mdp_bPc) |
| x |  |  | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Arbetsledarerfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/j1Yr_UhC_X16) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [Flera språk, **language**](http://data.jobtechdev.se/taxonomy/concept/o8Xp_wvY_5pu) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
|  | x |  | [redaktör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tRiS_ixp_5bD) |
| | | **10** | 10/20 = **50%** |