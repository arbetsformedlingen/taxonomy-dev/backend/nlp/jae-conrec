# Results for '52bbbefa4601c31531a9157314c135fa6134eda9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [52bbbefa4601c31531a9157314c135fa6134eda9](README.md) | 1 | 2518 | 21 | 34 | 196/397 = **49%** | 12/25 = **48%** |

## Source text

Finsnickare till Hallberg-Rassy Finsnickare till Hallberg-Rassy  Anrika och välrenommerade Hallberg-Rassy söker dig som vill arbeta med kvalitet.  På Hallberg-Rassy jobbar du i ett kunnigt team där leveransnivån är hög. Det är därför viktigt att du har lätt för att samarbeta och kan ta till dig av nya metoder samt kunna arbeta självständigt och vara öppen för lösningar.  Du kommer att vara med och bygga träinredningen till Hallberg-Rassys hypermoderna segelbåtar utifrån framtagna beredningsunderlag.  För att lyckas i rollen behöver du kunna läsa ritningar eller förstå muntlig genomgång samt gärna ha erfarenhet från liknade arbetsuppgifter i båtar. Du är yrkesutbildad som båtbyggare eller finsnickare och har känsla för kvalité och har yrkesstolthet.  Du kommer att ingå i ett team med skickliga båtbyggare som kommer att dela med sig av sina erfarenheter till dig och där samarbete och eget ansvar är nyckeln.  Hallberg-Rassy månar om sina kunder och det är därför viktigt att du har leveransförståelse och är serviceorienterad. De månar också om sina anställda och har förutom generöst friskvårdsbidrag även naprapat som kommer till varvet.  Med lyhördhet och öppet sinne kommer du få en bra start med dina nya kollegor och Hallberg-Rassys verksamhet.  För att lyckas i denna tjänst har du cirka 5 års erfarenhet av liknande arbetsuppgifter och du bor med pendlingsavstånd eller är villig att flytta till, eller nära, Orust.  Vi söker dig som har träbakgrund och arbetat som inredningssnickare, möbelsnickare, båtbyggare eller jämförbart.  Du har:  Hög kvalitetsnivå och yrkesmässig kompetens, en leveransförståelse samt är nyfiken och tycker om att arbeta på nya sätt utanför din bekvämlighetszon. Du har även lätt för att samarbeta med alla typer av människor.  Talar svenska och gärna viss förståelse för engelska     Kvalifikationer:   • Vana att arbeta med båtar  • Vana att arbeta på varv  • Talat flytande Svenska och Engelska    Det sägs att det alltid går att känna igen en Hallberg-Rassy oavsett ålder på båten och det stämmer! Välkommen att söka tjänsten redan idag.     Denna rekrytering sköts av Marinkraft - specialiserad på maritima proffs. Sökord: fartyg, passagerarfartyg, Motormekaniker, drev, mekaniker, marinmekaniker, marintekniker, tekniker, marin, varvsarbetare, varvsmekaniker, tungmekaniker, hydraultekniker, marinhydraulik, marina projekt, båtar, fordonsmekaniker, vattenjet, slagvattenfilter, styrsystem, örlogsfartyg, kustbevakningen, internationella rederier, privata rederier.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Fin**snickare** till Hallberg-Rassy Finsnicka... | x |  |  | 8 | (not found in taxonomy) |
| 2 | ...ickare till Hallberg-Rassy Fin**snickare** till Hallberg-Rassy  Anrika o... | x |  |  | 8 | (not found in taxonomy) |
| 3 | ...dig av nya metoder samt kunna **arbeta självständigt** och vara öppen för lösningar.... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 4 | ...kas i rollen behöver du kunna **läsa ritningar** eller förstå muntlig genomgån... |  | x |  | 14 | [läsa vanliga ritningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4eLk_Z9j_rJZ) |
| 4 | ...kas i rollen behöver du kunna **läsa ritningar** eller förstå muntlig genomgån... |  | x |  | 14 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 5 | ...rån liknade arbetsuppgifter i **båtar**. Du är yrkesutbildad som båtb... |  | x |  | 5 | [Båtar, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/XxwR_9MV_LCy) |
| 6 | ...åtar. Du är yrkesutbildad som **båtbyggare** eller finsnickare och har kän... |  | x |  | 10 | [Mästarbrev, båtbyggare, **skill**](http://data.jobtechdev.se/taxonomy/concept/PDJs_KCy_vt9) |
| 6 | ...åtar. Du är yrkesutbildad som **båtbyggare** eller finsnickare och har kän... | x | x | 10 | 10 | [Båtbyggare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/kBdt_K9U_vgC) |
| 6 | ...åtar. Du är yrkesutbildad som **båtbyggare** eller finsnickare och har kän... |  | x |  | 10 | [Gesällbrev, båtbyggare, **skill**](http://data.jobtechdev.se/taxonomy/concept/ukzr_kg6_tDz) |
| 7 | ...utbildad som båtbyggare eller **finsnickare** och har känsla för kvalité oc... | x |  |  | 11 | (not found in taxonomy) |
| 8 | ...ingå i ett team med skickliga **båtbyggare** som kommer att dela med sig a... |  | x |  | 10 | [Mästarbrev, båtbyggare, **skill**](http://data.jobtechdev.se/taxonomy/concept/PDJs_KCy_vt9) |
| 8 | ...ingå i ett team med skickliga **båtbyggare** som kommer att dela med sig a... | x | x | 10 | 10 | [Båtbyggare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/kBdt_K9U_vgC) |
| 8 | ...ingå i ett team med skickliga **båtbyggare** som kommer att dela med sig a... |  | x |  | 10 | [Gesällbrev, båtbyggare, **skill**](http://data.jobtechdev.se/taxonomy/concept/ukzr_kg6_tDz) |
| 9 | ...eneröst friskvårdsbidrag även **naprapat** som kommer till varvet.  Med ... | x | x | 8 | 8 | [Naprapat, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3EeV_kzS_zag) |
| 9 | ...eneröst friskvårdsbidrag även **naprapat** som kommer till varvet.  Med ... |  | x |  | 8 | [Kiropraktorer och naprapater m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/aWki_4uA_adn) |
| 9 | ...eneröst friskvårdsbidrag även **naprapat** som kommer till varvet.  Med ... |  | x |  | 8 | [Naprapater, sjukgymnaster och arbetsterapeuter m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/v89x_B1t_Pp9) |
| 10 | ...s i denna tjänst har du cirka **5 års erfarenhet** av liknande arbetsuppgifter o... | x |  |  | 16 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 11 | ... att flytta till, eller nära, **Orust**.  Vi söker dig som har träbak... |  | x |  | 5 | [Orust, **municipality**](http://data.jobtechdev.se/taxonomy/concept/tmAp_ykH_N6k) |
| 12 | ...r träbakgrund och arbetat som **inredningssnickare**, möbelsnickare, båtbyggare el... | x | x | 18 | 18 | [Inredningssnickare/Specialsnickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cyCt_LLE_qDy) |
| 13 | ...betat som inredningssnickare, **möbelsnickare**, båtbyggare eller jämförbart.... | x | x | 13 | 13 | [Möbelsnickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EVMd_5ES_dRT) |
| 14 | ...ningssnickare, möbelsnickare, **båtbyggare** eller jämförbart.  Du har:  H... |  | x |  | 10 | [Mästarbrev, båtbyggare, **skill**](http://data.jobtechdev.se/taxonomy/concept/PDJs_KCy_vt9) |
| 14 | ...ningssnickare, möbelsnickare, **båtbyggare** eller jämförbart.  Du har:  H... | x | x | 10 | 10 | [Båtbyggare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/kBdt_K9U_vgC) |
| 14 | ...ningssnickare, möbelsnickare, **båtbyggare** eller jämförbart.  Du har:  H... |  | x |  | 10 | [Gesällbrev, båtbyggare, **skill**](http://data.jobtechdev.se/taxonomy/concept/ukzr_kg6_tDz) |
| 15 | ...la typer av människor.  Talar **svenska** och gärna viss förståelse för... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 16 | ...och gärna viss förståelse för **engelska**     Kvalifikationer:   • Vana... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 17 | ...oner:   • Vana att arbeta med **båtar**  • Vana att arbeta på varv  •... |  | x |  | 5 | [Båtar, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/XxwR_9MV_LCy) |
| 18 | ...eta på varv  • Talat flytande **Svenska** och Engelska    Det sägs att ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 19 | ... • Talat flytande Svenska och **Engelska**    Det sägs att det alltid gå... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 20 | ...d på maritima proffs. Sökord: **fartyg**, passagerarfartyg, Motormekan... | x | x | 6 | 6 | [Fartyg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dRFw_Rcu_dX7) |
| 21 | ...itima proffs. Sökord: fartyg, **passagerarfartyg**, Motormekaniker, drev, mekani... | x | x | 16 | 16 | [Passagerarfartyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/M4ei_Uvu_Ks7) |
| 22 | ...rd: fartyg, passagerarfartyg, **Motormekaniker**, drev, mekaniker, marinmekani... |  | x |  | 14 | [Motormekaniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/z7qp_RLA_AjZ) |
| 23 | ...agerarfartyg, Motormekaniker, **drev**, mekaniker, marinmekaniker, m... |  | x |  | 4 | [hamra in bomull och drev i skarvar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NnAc_cgj_njD) |
| 24 | ...ormekaniker, drev, mekaniker, **marinmekaniker**, marintekniker, tekniker, mar... | x | x | 14 | 14 | [Marinmekaniker/Marintekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/M6Yt_ynM_FDQ) |
| 25 | ...v, mekaniker, marinmekaniker, **marintekniker**, tekniker, marin, varvsarbeta... | x | x | 13 | 13 | [Marinmekaniker/Marintekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/M6Yt_ynM_FDQ) |
| 26 | ...rintekniker, tekniker, marin, **varvsarbetare**, varvsmekaniker, tungmekanike... | x | x | 13 | 13 | [Varvsarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/NvyA_mwS_eB3) |
| 27 | ...arvsmekaniker, tungmekaniker, **hydraultekniker**, marinhydraulik, marina proje... | x | x | 15 | 15 | [hydraultekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/n8vC_84F_4rN) |
| 28 | ...rinhydraulik, marina projekt, **båtar**, fordonsmekaniker, vattenjet,... |  | x |  | 5 | [Båtar, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/XxwR_9MV_LCy) |
| 29 | ...aulik, marina projekt, båtar, **fordonsmekaniker**, vattenjet, slagvattenfilter,... |  | x |  | 16 | [Fordonsmekaniker och reparatörer m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/wdJT_znW_3Te) |
| | **Overall** | | | **196** | **397** | 196/397 = **49%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Naprapat, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3EeV_kzS_zag) |
|  | x |  | [läsa vanliga ritningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4eLk_Z9j_rJZ) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| x | x | x | [Möbelsnickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EVMd_5ES_dRT) |
| x | x | x | [Passagerarfartyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/M4ei_Uvu_Ks7) |
| x | x | x | [Marinmekaniker/Marintekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/M6Yt_ynM_FDQ) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [hamra in bomull och drev i skarvar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NnAc_cgj_njD) |
| x | x | x | [Varvsarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/NvyA_mwS_eB3) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Mästarbrev, båtbyggare, **skill**](http://data.jobtechdev.se/taxonomy/concept/PDJs_KCy_vt9) |
|  | x |  | [Båtar, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/XxwR_9MV_LCy) |
| x |  |  | (not found in taxonomy) |
|  | x |  | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
|  | x |  | [Kiropraktorer och naprapater m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/aWki_4uA_adn) |
| x | x | x | [Inredningssnickare/Specialsnickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cyCt_LLE_qDy) |
| x | x | x | [Fartyg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dRFw_Rcu_dX7) |
| x | x | x | [Båtbyggare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/kBdt_K9U_vgC) |
| x | x | x | [hydraultekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/n8vC_84F_4rN) |
|  | x |  | [Orust, **municipality**](http://data.jobtechdev.se/taxonomy/concept/tmAp_ykH_N6k) |
|  | x |  | [Gesällbrev, båtbyggare, **skill**](http://data.jobtechdev.se/taxonomy/concept/ukzr_kg6_tDz) |
|  | x |  | [Naprapater, sjukgymnaster och arbetsterapeuter m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/v89x_B1t_Pp9) |
|  | x |  | [Fordonsmekaniker och reparatörer m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/wdJT_znW_3Te) |
|  | x |  | [Motormekaniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/z7qp_RLA_AjZ) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **12** | 12/25 = **48%** |