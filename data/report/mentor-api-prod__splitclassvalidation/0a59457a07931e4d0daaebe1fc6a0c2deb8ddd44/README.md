# Results for '0a59457a07931e4d0daaebe1fc6a0c2deb8ddd44'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0a59457a07931e4d0daaebe1fc6a0c2deb8ddd44](README.md) | 1 | 752 | 7 | 6 | 27/87 = **31%** | 3/9 = **33%** |

## Source text

Skogsarbetare Vi på Lindberg skogskonsult AB söker personal. Vi är på jakt efter starka individer som klarar hårda/tuffa arbetsdagar. Stor frihet i kombination med stort ansvar. Skogsarbetet innebär manuell röjning, kvistning och manuell fällning främst i kraftledningsgator. Jobbet innebär ofta endel resor och ibland även trakt jobb. Du lär antingen ha bra fysik eller bra psyke för att klara av jobbet. Det är viktigt att du fungerar i gruppen. B-körkort, motorsågs körkort och grönt kort är meriterande.  Erfarenhet av röjsåg är meriterande. Skicka din ansökan med referens till: joakim@lskab.nu Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Skogsarbetare** Vi på Lindberg skogskonsult A... | x | x | 13 | 13 | [Skogsarbetare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/qouP_wtb_93q) |
| 2 | ...öjning, kvistning och manuell **fällning** främst i kraftledningsgator. ... |  | x |  | 8 | [välja ut träd för fällning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2suA_1Sk_SWb) |
| 3 | ...or. Jobbet innebär ofta endel **resor** och ibland även trakt jobb. D... | x |  |  | 5 | [Resor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/mj44_gFW_1X5) |
| 4 | ...rakt jobb. Du lär antingen ha **bra **fysik eller bra psyke för att ... | x |  |  | 4 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 5 | ... jobb. Du lär antingen ha bra **fysik** eller bra psyke för att klara... | x | x | 5 | 5 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 6 | ...gt att du fungerar i gruppen. **B-körkort**, motorsågs körkort och grönt ... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 7 | ...ungerar i gruppen. B-körkort, **motorsågs körkort** och grönt kort är meriterande... | x |  |  | 17 | [Utbildning på motorsåg, **skill**](http://data.jobtechdev.se/taxonomy/concept/ba8g_LRR_Sj2) |
| 8 | ...örkort, motorsågs körkort och **grönt kort** är meriterande.  Erfarenhet a... | x |  |  | 10 | [Grönt kort, skogsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/C9Ra_Vox_u2L) |
| 8 | ...örkort, motorsågs körkort och **grönt kort** är meriterande.  Erfarenhet a... |  | x |  | 10 | [Yrkesbevis, beskärning, trädvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/X51E_Edf_cfG) |
| 9 | ...r meriterande.  Erfarenhet av **röjsåg** är meriterande. Skicka din an... | x |  |  | 6 | [Utbildning på röjsåg, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Rxx_zRM_Dqc) |
| | **Overall** | | | **27** | **87** | 27/87 = **31%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [välja ut träd för fällning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2suA_1Sk_SWb) |
| x |  |  | [Utbildning på röjsåg, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Rxx_zRM_Dqc) |
| x |  |  | [Grönt kort, skogsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/C9Ra_Vox_u2L) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [Yrkesbevis, beskärning, trädvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/X51E_Edf_cfG) |
| x |  |  | [Utbildning på motorsåg, **skill**](http://data.jobtechdev.se/taxonomy/concept/ba8g_LRR_Sj2) |
| x |  |  | [Resor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/mj44_gFW_1X5) |
| x | x | x | [Skogsarbetare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/qouP_wtb_93q) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| | | **3** | 3/9 = **33%** |