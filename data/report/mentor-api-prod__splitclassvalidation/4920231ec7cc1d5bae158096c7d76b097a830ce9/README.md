# Results for '4920231ec7cc1d5bae158096c7d76b097a830ce9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4920231ec7cc1d5bae158096c7d76b097a830ce9](README.md) | 1 | 2583 | 17 | 17 | 101/217 = **47%** | 8/14 = **57%** |

## Source text

Måltidsbiträde till Lillestad Nu söker vi dig, som tillsammans med oss vill göra nytta för dem vi är till för! Växjö kommun är Kronobergs största arbetsgivare som varje dag, året runt, arbetar för våra invånare, företag och besökare. Tillsammans skapar vi en hållbar morgondag och en attraktiv kommun att bo, leva och verka i – idag och i framtiden. Det innebär ett arbete där vi utmanar oss att tänka nytt, lära och utvecklas med varandra. Vill du vara en del i ett stort sammanhang där du får vara med och göra skillnad på riktigt? Välkommen till oss på Växjö kommun!  ARBETSUPPGIFTER Vi söker nu ett måltidsbiträde till vårt kök på Lillestadskolan. Köket på Lillestadskolan är ett tillagningskök vilket innebär att du som måltidsbiträde kommer hjälpa tre kollegor att tillaga mat till elever och pedagoger. Dina arbetsuppgifter är att hjälpa till att tillaga tillbehör såsom pasta, tillreda salladsbuffé mm.  Vi arbetar med hållbarhet som svin och klimat smarta alternativ vi har även förmånen att bjuda gästerna på fina råvaror och livsmedel som är eko eller svenska.   Du kommer att arbeta tillsammans med en kollega och ni planerar och lägger upp dagen tillsammans samt samverkar med skolans personal och elever. Du utför vissa städ och diskuppgifter. Det förekommer även administrativa arbetsuppgifter. Du utför egenkontroll samt vissa disk- och städuppgifter. Tjänsten omfattar 75 % dagtid.   KVALIFIKATIONER Vi söker i första hand dig med erfarenhet av arbete inom storkök och/eller köksutbildning.  Vi vill att du har ett stort intresse för att ge god service och har ett trevligt bemötande mot våra kunder. Det är viktigt med en god samarbetsförmåga både i arbetsgruppen, och mot våra kunder och gäster. Du ska ha kunskaper i svenska språket i tal och skrift samt god datorvana.  Vi lägger stor vikt vid personlig lämplighet och förväntar oss att du är en god företrädare för Växjö kommun.  ÖVRIGT Växjö kommun erbjuder en attraktiv arbetsplats som är fri från diskriminering och ger lika möjligheter för alla.    För att bli aktuell för anställning behöver du visa två utdrag från belastningsregistret innan du anställs. Beställningsblanketter kan hämtas på www.polisen.se/Service/Belastningsregistret-begar-utdrag och du ska använda dels det som heter Arbete inom skola eller förskola (442.5) samt Kontroll av egna uppgifter i belastningsregistret (442.3).   Till bemannings- och rekryteringsföretag eller till dig som är försäljare: Vi undanber oss vänligen men bestämt direktkontakt med bemannings- och rekryteringsföretag samt försäljare av ytterligare annonser.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Måltidsbiträde** till Lillestad Nu söker vi di... | x | x | 14 | 14 | [Måltidsbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Tson_Zgt_zhT) |
| 2 | ...nytta för dem vi är till för! **Växjö** kommun är Kronobergs största ... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 3 | ... för dem vi är till för! Växjö** kommun** är Kronobergs största arbetsg... | x |  |  | 7 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 4 | ... är till för! Växjö kommun är **Kronobergs** största arbetsgivare som varj... | x |  |  | 10 | [Kronobergs län, **region**](http://data.jobtechdev.se/taxonomy/concept/tF3y_MF9_h5G) |
| 5 | ...iktigt? Välkommen till oss på **Växjö** kommun!  ARBETSUPPGIFTER Vi... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 6 | ...t? Välkommen till oss på Växjö** kommun**!  ARBETSUPPGIFTER Vi söker ... | x |  |  | 7 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 7 | ...BETSUPPGIFTER Vi söker nu ett **måltidsbiträde** till vårt kök på Lillestadsko... | x | x | 14 | 14 | [Måltidsbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Tson_Zgt_zhT) |
| 8 | ...kök vilket innebär att du som **måltidsbiträde** kommer hjälpa tre kollegor at... | x | x | 14 | 14 | [Måltidsbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Tson_Zgt_zhT) |
| 9 | ... gästerna på fina råvaror och **livsmedel** som är eko eller svenska.   D... | x | x | 9 | 9 | [Livsmedel, **skill**](http://data.jobtechdev.se/taxonomy/concept/tdbu_fUL_MnC) |
| 10 | ... råvaror och livsmedel som är **eko** eller svenska.   Du kommer at... | x | x | 3 | 3 | [Eko, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/cAPJ_ry5_AgS) |
| 11 | ...ch livsmedel som är eko eller **svenska**.   Du kommer att arbeta tills... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 12 | ...ns samt samverkar med skolans **personal** och elever. Du utför vissa st... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 13 | ...al och elever. Du utför vissa **städ** och diskuppgifter. Det föreko... | x |  |  | 4 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 14 | ...ever. Du utför vissa städ och **diskuppgifter**. Det förekommer även administ... |  | x |  | 13 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 15 | ...utför egenkontroll samt vissa **disk**- och städuppgifter. Tjänsten ... | x | x | 4 | 4 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 16 | ...r egenkontroll samt vissa disk**- och stä**duppgifter. Tjänsten omfattar ... |  | x |  | 9 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 17 | ...kontroll samt vissa disk- och **städuppgifter**. Tjänsten omfattar 75 % dagti... |  | x |  | 13 | [utföra städuppgifter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fcFF_i9N_xqx) |
| 17 | ...kontroll samt vissa disk- och **städuppgifter**. Tjänsten omfattar 75 % dagti... | x | x | 13 | 13 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 18 | ...duppgifter. Tjänsten omfattar **75 %** dagtid.   KVALIFIKATIONER V... | x |  |  | 4 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 19 | ...med erfarenhet av arbete inom **storkök** och/eller köksutbildning.  Vi... | x | x | 7 | 7 | [Storkök, **skill**](http://data.jobtechdev.se/taxonomy/concept/GQ9R_2Za_Awt) |
| 20 | ...gäster. Du ska ha kunskaper i **svenska** språket i tal och skrift samt... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 21 | ...gäster. Du ska ha kunskaper i **svenska språket** i tal och skrift samt god dat... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 22 | ... du är en god företrädare för **Växjö** kommun.  ÖVRIGT Växjö kommun... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 23 | ...r en god företrädare för Växjö** kommun**.  ÖVRIGT Växjö kommun erbjud... | x |  |  | 7 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 24 | ...re för Växjö kommun.  ÖVRIGT **Växjö** kommun erbjuder en attraktiv ... |  | x |  | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 25 | ...heter Arbete inom skola eller **förskola** (442.5) samt Kontroll av egna... | x | x | 8 | 8 | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| | **Overall** | | | **101** | **217** | 101/217 = **47%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| x | x | x | [Storkök, **skill**](http://data.jobtechdev.se/taxonomy/concept/GQ9R_2Za_Awt) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x | x | x | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x | x | x | [Måltidsbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Tson_Zgt_zhT) |
| x | x | x | [Eko, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/cAPJ_ry5_AgS) |
|  | x |  | [utföra städuppgifter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fcFF_i9N_xqx) |
| x | x | x | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| x | x | x | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| x | x | x | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| x |  |  | [Kronobergs län, **region**](http://data.jobtechdev.se/taxonomy/concept/tF3y_MF9_h5G) |
| x | x | x | [Livsmedel, **skill**](http://data.jobtechdev.se/taxonomy/concept/tdbu_fUL_MnC) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/14 = **57%** |