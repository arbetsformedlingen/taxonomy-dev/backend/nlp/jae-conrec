# Results for '4a2e97c33065f2517fb15a4d168acd6ae5a5c261'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4a2e97c33065f2517fb15a4d168acd6ae5a5c261](README.md) | 1 | 2180 | 13 | 8 | 60/146 = **41%** | 5/12 = **42%** |

## Source text

Supporttekniker CCTV (Deltid ca 50%) Om Övervakningsbutiken Övervakningsbutiken är en av Sveriges ledande leverantörer inom kameraövervakning, både som distributör och genom detaljhandel. Vi är specialister med mycket lång erfarenhet och stor kompetens. Företaget ligger ständigt i framkant vad gäller teknik och produkterna är alltid utrustade med de senaste funktionerna. Företaget har funnits sedan 2005.  Hur ser en arbetsdag ut? Som supporttekniker kommer du att arbeta tillsammans med kompetenta och erfarna kollegor på supportavdelningen. Dina dagliga arbetsuppgifter består huvudsakligen av att arbeta med support av kameraövervakningssystem och nätverk samt utföra felsökning och analyser. Du kommer även hjälpa till med konfigurering av system och få arbeta med innovativ AI-teknik. Du kommer att prata med både återförsäljare och slutkunder. Dina erfarna kollegor kommer att finnas till hands i alla svårlösta ärenden och ni jobbar alltid som ett team. Arbetet utförs framförallt i Windows-miljö.  Vilka arbetstider gäller? Vi har angivit halvtid men är flexibla med både antalet timmar och vilken tid på dagen arbetet utförs (förmiddag eller eftermiddag).  Vi söker dig som:  - gärna har någon form av arbetserfarenhet av kundsupport. - har goda datorkunskaper och ett stort intresse av teknik. - har goda kunskaper i svenska och engelska. - är organiserad, social, serviceinriktad, självgående och kan hålla många bollar i luften samtidigt. - är säljintresserad då rollen även erbjuder goda försäljningsmöjligheter. - tidigare erfarenhet av övervakningssystem är meriterande, men inte ett krav.   Varför vill du jobba hos oss? Du kommer ha ett spännande jobb där du handskas med mycket intressanta produkter för en teknikintresserad person. Du kommer ha en rolig arbetsdag med bra gemenskap. Vi har högt i tak och lättsam stämning. Möjlighet till viss flextid. Intressant bransch med goda framtidsutsikter inför kommande ”tuffare tider”.  Kontor: Göteborg (Högsbo) Möjligheter: Fast lön + extra provisionslön, viss flextid Omfattning: halvtid Tillträde: så snart som möjligt  Har du frågor om tjänsten är du välkommen att kontakta Adam på adam@overvakningsbutiken.se.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Supporttekniker** CCTV (Deltid ca 50%) Om Överv... | x | x | 15 | 15 | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| 2 | Supporttekniker CCTV (**Deltid ca 50%**) Om Övervakningsbutiken Överv... | x |  |  | 13 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 3 | ... Övervakningsbutiken är en av **Sveriges** ledande leverantörer inom kam... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ... Hur ser en arbetsdag ut? Som **supporttekniker** kommer du att arbeta tillsamm... | x | x | 15 | 15 | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| 5 | ...ystem och nätverk samt utföra **felsökning** och analyser. Du kommer även ... |  | x |  | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 6 | ...m och få arbeta med innovativ **AI**-teknik. Du kommer att prata m... | x |  |  | 2 | [Artificial Intelligence/Artificiell intelligens/AI, **skill**](http://data.jobtechdev.se/taxonomy/concept/SaTd_K5z_ex1) |
| 7 | ... Arbetet utförs framförallt i **Windows**-miljö.  Vilka arbetstider gäl... | x | x | 7 | 7 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 8 | ...t utförs framförallt i Windows**-miljö**.  Vilka arbetstider gäller? V... |  | x |  | 6 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 9 | ...stider gäller? Vi har angivit **halvtid** men är flexibla med både anta... | x |  |  | 7 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 10 | ...et av kundsupport. - har goda **datorkunskaper** och ett stort intresse av tek... |  | x |  | 14 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 11 | ...eknik. - har goda kunskaper i **svenska** och engelska. - är organisera... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 12 | ... goda kunskaper i svenska och **engelska**. - är organiserad, social, se... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 13 | ...ör kommande ”tuffare tider”.  **Kontor**: Göteborg (Högsbo) Möjlighete... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 14 | ...nde ”tuffare tider”.  Kontor: **Göteborg** (Högsbo) Möjligheter: Fast lö... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 15 | ...Möjligheter: Fast lön + extra **provisionslön**, viss flextid Omfattning: hal... | x |  |  | 13 | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
| 16 | ...lön, viss flextid Omfattning: **halvtid** Tillträde: så snart som möjli... | x |  |  | 7 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| | **Overall** | | | **60** | **146** | 60/146 = **41%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
|  | x |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
|  | x |  | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x |  |  | [Artificial Intelligence/Artificiell intelligens/AI, **skill**](http://data.jobtechdev.se/taxonomy/concept/SaTd_K5z_ex1) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/12 = **42%** |