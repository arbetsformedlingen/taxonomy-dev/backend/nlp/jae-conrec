# Results for 'aab9fe44d567b0c31b6e022428b74b117fad643c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [aab9fe44d567b0c31b6e022428b74b117fad643c](README.md) | 1 | 364 | 10 | 9 | 90/121 = **74%** | 7/9 = **78%** |

## Source text

Utbildad nagelteknolog sökes Vi söker en serviceinriktad och noggrann nagelteknolog. Det är även önskvärt att du kan göra ögonfransar. Du måste vara kunnig och utbildad inom området. Arbetsuppgifterna är manikyr, pedikyr, nagelförlängning, fransförlängning etc. Heltid. Lön enligt kollektivavtal. My's Nails Skönhetssalong AB, Cityhuset, Kungsgatan 24, Eskilstuna.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Utbildad **nagelteknolog** sökes Vi söker en serviceinri... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 2 | ... söker en serviceinriktad och **noggrann** nagelteknolog. Det är även ön... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 3 | ... serviceinriktad och noggrann **nagelteknolog**. Det är även önskvärt att du ... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 4 | ...även önskvärt att du kan göra **ögonfransar**. Du måste vara kunnig och utb... | x |  |  | 11 | [Fransförlängning, **skill**](http://data.jobtechdev.se/taxonomy/concept/kWPd_NUy_9Za) |
| 5 | ...området. Arbetsuppgifterna är **manikyr**, pedikyr, nagelförlängning, f... | x | x | 7 | 7 | [Handvård/Manikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/RVLi_2no_Eyg) |
| 6 | ...Arbetsuppgifterna är manikyr, **pedikyr**, nagelförlängning, fransförlä... | x | x | 7 | 7 | [Fotvård/Pedikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/5APZ_gLs_AB4) |
| 7 | ...gifterna är manikyr, pedikyr, **nagelförlängning**, fransförlängning etc. Heltid... | x | x | 16 | 16 | [Nagelförlängning, **skill**](http://data.jobtechdev.se/taxonomy/concept/VxS2_ZHa_Kag) |
| 8 | ...r, pedikyr, nagelförlängning, **fransförlängning** etc. Heltid. Lön enligt kolle... | x | x | 16 | 16 | [Fransförlängning, **skill**](http://data.jobtechdev.se/taxonomy/concept/kWPd_NUy_9Za) |
| 9 | ...ngning, fransförlängning etc. **Heltid**. Lön enligt kollektivavtal. M... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 10 | ...gning etc. Heltid. Lön enligt **kollektivavtal**. My's Nails Skönhetssalong AB... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 11 | ...AB, Cityhuset, Kungsgatan 24, **Eskilstuna**. | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| | **Overall** | | | **90** | **121** | 90/121 = **74%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Fotvård/Pedikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/5APZ_gLs_AB4) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| x | x | x | [Handvård/Manikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/RVLi_2no_Eyg) |
| x | x | x | [Nagelförlängning, **skill**](http://data.jobtechdev.se/taxonomy/concept/VxS2_ZHa_Kag) |
| x | x | x | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| x | x | x | [Fransförlängning, **skill**](http://data.jobtechdev.se/taxonomy/concept/kWPd_NUy_9Za) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **7** | 7/9 = **78%** |