# Results for '825e0fde57c3129676c6784a288e46ef2a4c2c63'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [825e0fde57c3129676c6784a288e46ef2a4c2c63](README.md) | 1 | 3027 | 16 | 10 | 65/161 = **40%** | 7/16 = **44%** |

## Source text

Entomolog OM BIOLOGISK MYGGKONTROLL Vi arbetar med bekämpning av översvämningsmyggor i Nedre Dalälven samt vid Klarälven och våra uppdragsgivare är åtta myggdrabbade kommuner. Biologisk Myggkontroll är en verksamhet inom Nedre Dalälven Utvecklings AB i Gysinge och en del av Biosfärområdet Älvlandskapet Nedre Dalälven. Vår värdegrund vilar på hänsyn till både människor och miljö. Vårt överbryggande mål är att med miljömässigt godkänd teknik reducera mängden översvämningsmyggor till acceptabla nivåer så att både människor och djur ska kunna bo, leva och vistas i tidigare stickmyggdrabbade områden. Bekämpningen är enbart riktad mot översvämningsmyggor och utförs i deras kläckningsområden. Vi använder helikopterbaserad spridning av det selektiva biologiska larvbekämpningsmedlet Bti. Vårt kontor finns i Uppsala och idag är vi sex heltidsanställda och en mygghund, samt ett 20-tal timanställda under sommaren. Mer information om vår verksamhet finns att läsa på vår hemsida www.mygg.se och www.nedredalalven.se. ARBETSUPPGIFTER Din huvudsakliga inriktning är inventering av stickmyggor och arbetsuppgifterna inkluderar att organisera och att utföra insamling av stickmygghonor i översvämningsområden vid Dalälven, Klarälven samt eventuellt ytterligare områden; skötsel av fällor och annan utrustning; identifiera stickmyggor till art (primärt morfologiskt, men även molekylärt); lära upp fältpersonal i stickmygginsamling; hålla sig uppdaterad om stickmyggornas taxonomi och biologi. Även identifiering av andra insekter till ordning och inom underordningen Nematocera till familj ingår i arbetet.  Under vår/sommarsäsongen kommer du också arbeta med bekämpning där arbetsuppgifter som inventeringar av mängden stickmygglarver, GPS-inmätningar, GIS-analyser och ritning av bekämpningsytor till helikoptern ingår.  Dessa arbetsuppgifter inkluderar arbete i fält i häftiga naturområden vid Dalälven och Klarälven.  Under de första åren ges internutbildning inom relevanta ämnesområden. Entomologen ingår i ett team och du hjälper självklart till även med andra delar av verksamheten. KVALIFIKATIONER Du har högskoleutbildning inom biologi med intresse för insekter och naturen, alternativt utbildning/arbetslivserfarenhet som vi bedömer som likvärdig. Erfarenhet av insektsidentifiering och speciellt inom Nematocera är meriterande, liksom erfarenhet av GIS.  Som person är du kommunikativ, noggrann och har en kreativ förmåga att se möjligheter till utveckling och förbättring. Du kan arbeta självständigt men gillar att samarbeta och ha kul på jobbet, precis som vi. Vi sätter stor vikt vid personlig lämplighet.  Körkort är ett krav.   För frågor om verksamheten och arbetsuppgifterna kontakta Verksamhetsledaren Jan O. Lundström (Jan.Lundstrom@mygg.se, 070-5690023).  Tjänsten är en tillsvidareanställning på 100%. Vi tillämpar 6 månaders provanställning.  Ansökan med personligt brev och CV skickas till mailadress Jan.Lundstrom@mygg.se . Sista ansökningsdag den 11 september 2022 och vi tillämpar löpande rekrytering.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Entomolog** OM BIOLOGISK MYGGKONTROLL Vi ... | x | x | 9 | 9 | [Entomolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BhJk_66z_inc) |
| 2 | ...åer så att både människor och **djur** ska kunna bo, leva och vistas... | x | x | 4 | 4 | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| 3 | ...rvbekämpningsmedlet Bti. Vårt **kontor** finns i Uppsala och idag är v... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 4 | ...dlet Bti. Vårt kontor finns i **Uppsala** och idag är vi sex heltidsans... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 5 | ... uppdaterad om stickmyggornas **taxonomi** och biologi. Även identifieri... | x |  |  | 8 | [organismers taxonomi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dRqA_SQQ_vTR) |
| 6 | ...m stickmyggornas taxonomi och **biologi**. Även identifiering av andra ... | x | x | 7 | 7 | [Biologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o6US_5HP_iDM) |
| 7 | ...r av mängden stickmygglarver, **GPS**-inmätningar, GIS-analyser och... | x |  |  | 3 | [Geografiskt positionssystem/GPS, **skill**](http://data.jobtechdev.se/taxonomy/concept/pTEz_GD2_J9b) |
| 8 | ...kmygglarver, GPS-inmätningar, **GIS**-analyser och ritning av bekäm... | x |  |  | 3 | [Geografiskt informationssystem/GIS, **skill**](http://data.jobtechdev.se/taxonomy/concept/CWfM_RL9_V4t) |
| 9 | ...heten. KVALIFIKATIONER Du har **högskoleutbildning** inom biologi med intresse för... | x |  |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 9 | ...heten. KVALIFIKATIONER Du har **högskoleutbildning** inom biologi med intresse för... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 10 | ...u har högskoleutbildning inom **biologi** med intresse för insekter och... | x | x | 7 | 7 | [Biologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o6US_5HP_iDM) |
| 11 | ...terande, liksom erfarenhet av **GIS**.  Som person är du kommunikat... | x | x | 3 | 3 | [Geografiskt informationssystem/GIS, **skill**](http://data.jobtechdev.se/taxonomy/concept/CWfM_RL9_V4t) |
| 12 | ...om person är du kommunikativ, **noggrann** och har en kreativ förmåga at... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 13 | ...kling och förbättring. Du kan **arbeta självständigt** men gillar att samarbeta och ... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 14 | ...kt vid personlig lämplighet.  **Körkort** är ett krav.   För frågor om ... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 14 | ...kt vid personlig lämplighet.  **Körkort** är ett krav.   För frågor om ... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 15 | ...070-5690023).  Tjänsten är en **tillsvidareanställning** på 100%. Vi tillämpar 6 månad... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 16 | ... en tillsvidareanställning på **100%**. Vi tillämpar 6 månaders prov... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **65** | **161** | 65/161 = **40%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Entomolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BhJk_66z_inc) |
| x | x | x | [Geografiskt informationssystem/GIS, **skill**](http://data.jobtechdev.se/taxonomy/concept/CWfM_RL9_V4t) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [organismers taxonomi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dRqA_SQQ_vTR) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Biologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o6US_5HP_iDM) |
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| x |  |  | [Geografiskt positionssystem/GPS, **skill**](http://data.jobtechdev.se/taxonomy/concept/pTEz_GD2_J9b) |
| x | x | x | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| | | **7** | 7/16 = **44%** |