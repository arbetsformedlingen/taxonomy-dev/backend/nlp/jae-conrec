# Results for '0e19694138c5764ea6238f7c1160b1d04fe7edd4'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0e19694138c5764ea6238f7c1160b1d04fe7edd4](README.md) | 1 | 2418 | 15 | 15 | 93/150 = **62%** | 8/11 = **73%** |

## Source text

Empleo söker nu Montör/staplare till Kund i Vislanda Till vår kund söker vi nu en händig staplare/montör med stort driv och engagemang.    Hos vår kund kommer du få chansen att vara delaktig i arbetet med att motverka cancer och vara med och bidra till spännande och intressanta forskningsprojekt tillsammans med kompetenta medarbetare.  Vår kund har genom åren varit ledande inom olika acceleratorrelaterade områden som cyklotroner, spektrometrar och synkrotronförstärkare. År 1980 startade de en ny enhet där de specialiserar sig på spole- och magnettillverkning. Med lång erfarenhet och med sina professionella och tekniska kunskaper för att designa och tillverka magneter för acceleratorer och andra applikationer så kan de skräddarsy magneter för att passa varje specifik applikation.    Dina huvudsakliga uppgifter som montör staplare innefattar bland annat:    Stapling av lameller   Mäta, packa, montera och banda lameller och spolar   Truckkörning   Testning av produkter  I tjänsten som montör/staplare kommer dina huvudsakliga arbetsuppgifter vara att stapla lameller i magnet- och spolningsprocessen. Viss delaktighet i testprocessen kring spolarna och magnettillverkningen.  Att löpande kontrollmäta produkter gentemot kravspecifikation med skjutmått.    Din Profil  Vi söker dig som är strukturerad, händig och noggrann i dina arbetsuppgifter. Har verkstadsvana och tidigare kunskap som montör.  Du brinner för nya utmaningar och känner stort ansvar och driv inför dina arbetsuppgifter.  Du är van att arbeta i verkstadsmiljö och värdesätter gott samarbete.  Arbetet som montör/staplare ställer krav på god fysik då produkterna är olika i både form och storlek och kräver olika nivå av handpåläggning.  Denna tjänst ställer krav på att du har: Truckkort samt traverskort Flytande svenska i tal och skrift Tidigare erfarenhet som montör  Meriterande är om man har tidigare erfarenhet av användning av skjutmått.    Uppdraget är ett inhyrningsuppdrag där du blir anställd hos Empleo för att sedan arbeta ute hos vårt kundföretag i Växjö. För rätt person kan detta bli ett långsiktigt arbete.   Vi rekryterar löpande och tjänsten kan komma att tillsättas innan ansökningstiden har gått ut. Du är varmt välkommen med din ansökan redan idag! Du ansöker direkt på www.empleo.se  Uppstart: Omgående eller enligt överenskommelse.  Ort/Placering: Vislanda Omfattning: Heltid.  Lön: Enligt gällande kollektivavtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Empleo söker nu **Montör**/staplare till Kund i Vislanda... | x | x | 6 | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 2 | ...öker vi nu en händig staplare/**montör** med stort driv och engagemang... | x | x | 6 | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 3 | ...na huvudsakliga uppgifter som **montör** staplare innefattar bland ann... | x | x | 6 | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 4 | ...re innefattar bland annat:    **Stapling av lameller **  Mäta, packa, montera och ban... | x |  |  | 21 | [stapla gods, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/SzHu_MJn_6cC) |
| 5 | ...er   Mäta, packa, montera och **banda** lameller och spolar   Truckkö... |  | x |  | 5 | [Banda, **language**](http://data.jobtechdev.se/taxonomy/concept/X5sT_hmN_m2S) |
| 6 | ...h banda lameller och spolar   **Truckkörning **  Testning av produkter  I tjä... | x |  |  | 13 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 7 | ... av produkter  I tjänsten som **montör**/staplare kommer dina huvudsak... | x | x | 6 | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 8 | ...m är strukturerad, händig och **noggrann** i dina arbetsuppgifter. Har v... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 9 | ...vana och tidigare kunskap som **montör**.  Du brinner för nya utmaning... |  | x |  | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 10 | ... gott samarbete.  Arbetet som **montör**/staplare ställer krav på god ... | x | x | 6 | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 11 | ...ntör/staplare ställer krav på **god fysik** då produkterna är olika i båd... | x | x | 9 | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 12 | ...t ställer krav på att du har: **Truckkort** samt traverskort Flytande sve... | x | x | 9 | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 13 | ...på att du har: Truckkort samt **traverskort** Flytande svenska i tal och sk... | x | x | 11 | 11 | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
| 14 | ...ort samt traverskort Flytande **svenska** i tal och skrift Tidigare erf... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 15 | ...krift Tidigare erfarenhet som **montör**  Meriterande är om man har ti... |  | x |  | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 16 | ...ta ute hos vårt kundföretag i **Växjö**. För rätt person kan detta bl... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 17 | ...acering: Vislanda Omfattning: **Heltid**.  Lön: Enligt gällande kollek... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 18 | ...Heltid.  Lön: Enligt gällande **kollektivavtal**. | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **93** | **150** | 93/150 = **62%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
| x |  |  | [stapla gods, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/SzHu_MJn_6cC) |
| x | x | x | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x | x | x | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
|  | x |  | [Banda, **language**](http://data.jobtechdev.se/taxonomy/concept/X5sT_hmN_m2S) |
| x | x | x | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/11 = **73%** |