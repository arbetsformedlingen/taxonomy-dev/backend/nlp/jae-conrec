# Results for '4165e816e4de0e8e450ba0d5019230d48d5f6e6d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4165e816e4de0e8e450ba0d5019230d48d5f6e6d](README.md) | 1 | 1648 | 24 | 46 | 7/924 = **1%** | 1/43 = **2%** |

## Source text

Som drifttekniker/processoperatör övervakar du produktionen, löser problem och utvecklar process och produkt.  En arbetsmarknad med enormt behov!    Utbildningen kommer att ge dig den kompetens du behöver för att arbeta som drifttekniker eller processoperatör i modern processindustri som värmeverk, pappersindustri, raffinaderi eller annan kemisk industri. Som drifttekniker/processoperatör arbetar du tillsammans med dina kollegor i skiftlaget med att övervaka produktionen, lösa problem och utveckla process och produkt. Yrkesrollen och skiftarbetet ger stor frihet och möjligheter till utveckling.De företag som kan vara din framtida arbetsgivare finns över hela Sverige.     Utbildningen är på två år och går som en modifierad distansutbildning. Vid tio tillfällen träffas alla 3-5 dagar för föreläsningar, diskussioner och övningar. Övrig tid studerar du på hemorten med stöd av vår lärplattform där du också har kontakt med lärare och andra studerande. I de flesta kurser finns böcker framtagna för just den här utbildningen. Dessutom kommer det att bildas studiegrupper på olika orter dit lärare kommer för handledning. Du kommer att få låna en dator under utbildningstiden. I början läser du kurser som är gemensamma för att få den grundkunskap som behövs i all processindustri:  - Processtekniskt arbete  - LIA I - Processtekniskt arbete  - Energiteknik  - Processteknik I  - LIA II - Industriella processer  - Processtyrning  - Underhållsteknik  - Processteknik II    Under det andra året specialiserar du dig mot antingen värmeproduktion, skogsindustri eller kemisk industri genom anpassade kurser, val av LIA-plats och examensarbete.  

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Som **drifttekniker**/processoperatör övervakar du ... |  | x |  | 13 | [Fastighetstekniker/Drifttekniker, fastighet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9NVZ_LfD_vqT) |
| 1 | Som **drifttekniker**/processoperatör övervakar du ... | x |  |  | 13 | [Drifttekniker och processövervakare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/nPFh_hia_3L5) |
| 2 | Som drifttekniker/**processoperatör** övervakar du produktionen, lö... | x |  |  | 15 | [Drifttekniker och processövervakare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/nPFh_hia_3L5) |
| 3 | ...drifttekniker/processoperatör **övervakar du produktionen**, löser problem och utvecklar ... | x |  |  | 25 | [övervaka anläggningens produktion, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ayjS_iRf_wHv) |
| 4 | ...ör övervakar du produktionen, **löser problem** och utvecklar process och pro... | x |  |  | 13 | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| 5 | ...du behöver för att arbeta som **drifttekniker** eller processoperatör i moder... |  | x |  | 13 | [Fastighetstekniker/Drifttekniker, fastighet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9NVZ_LfD_vqT) |
| 5 | ...du behöver för att arbeta som **drifttekniker** eller processoperatör i moder... | x |  |  | 13 | [Drifttekniker och processövervakare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/nPFh_hia_3L5) |
| 6 | ...rbeta som drifttekniker eller **processoperatör** i modern processindustri som ... | x |  |  | 15 | [Drifttekniker och processövervakare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/nPFh_hia_3L5) |
| 7 | ...ller processoperatör i modern **processindustri** som värmeverk, pappersindustr... |  | x |  | 15 | [Processindustriteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/i2VJ_K67_uie) |
| 8 | ... i modern processindustri som **värmeverk**, pappersindustri, raffinaderi... |  | x |  | 9 | [Drifttekniker vid värme- och vattenverk, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Cppd_9P7_HeR) |
| 8 | ... i modern processindustri som **värmeverk**, pappersindustri, raffinaderi... |  | x |  | 9 | [Verksamhetsledning, värmeverk, **skill**](http://data.jobtechdev.se/taxonomy/concept/FfJq_b4n_DMM) |
| 8 | ... i modern processindustri som **värmeverk**, pappersindustri, raffinaderi... |  | x |  | 9 | [Drifttekniker, värmeverk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GaC9_y4i_o8S) |
| 8 | ... i modern processindustri som **värmeverk**, pappersindustri, raffinaderi... |  | x |  | 9 | [Driftmaskinist, värmeverk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KDG5_eSG_PMz) |
| 8 | ... i modern processindustri som **värmeverk**, pappersindustri, raffinaderi... |  | x |  | 9 | [Drifttekniker vid värme- och vattenverk, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/ZmsL_fPS_5Jr) |
| 8 | ... i modern processindustri som **värmeverk**, pappersindustri, raffinaderi... |  | x |  | 9 | [Driftingenjör, värmeverk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ms98_iAJ_Cym) |
| 9 | ...rocessindustri som värmeverk, **pappersindustri**, raffinaderi eller annan kemi... |  | x |  | 15 | [Maskinskötsel, trä- och pappersindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/BGie_HPa_fNA) |
| 9 | ...rocessindustri som värmeverk, **pappersindustri**, raffinaderi eller annan kemi... |  | x |  | 15 | [Processoperatörer, trä- och pappersindustri, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MjXx_5mw_RNf) |
| 9 | ...rocessindustri som värmeverk, **pappersindustri**, raffinaderi eller annan kemi... |  | x |  | 15 | [Processoperatörer, trä- och pappersindustri, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Mu2E_RpA_86z) |
| 9 | ...rocessindustri som värmeverk, **pappersindustri**, raffinaderi eller annan kemi... |  | x |  | 15 | [ingenjör, pappersindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ZZbG_NWk_KVi) |
| 10 | ...m värmeverk, pappersindustri, **raffinaderi** eller annan kemisk industri. ... |  | x |  | 11 | [arbetsledare, raffinaderi, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/CnxK_rdw_U1N) |
| 11 | ...stri, raffinaderi eller annan **kemisk industri**. Som drifttekniker/processope... |  | x |  | 15 | [Processövervakare, kemisk industri, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/QEBd_qHu_vGr) |
| 11 | ...stri, raffinaderi eller annan **kemisk industri**. Som drifttekniker/processope... |  | x |  | 15 | [Processövervakare inom kemisk industri, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/UsoN_wah_kh4) |
| 11 | ...stri, raffinaderi eller annan **kemisk industri**. Som drifttekniker/processope... |  | x |  | 15 | [processoperatör, kemisk industri, kväve och syre, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/dndm_Wm1_uw5) |
| 11 | ...stri, raffinaderi eller annan **kemisk industri**. Som drifttekniker/processope... |  | x |  | 15 | [processoperatör, kemisk industri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/giE8_5TQ_1vS) |
| 11 | ...stri, raffinaderi eller annan **kemisk industri**. Som drifttekniker/processope... |  | x |  | 15 | [Kvarnskötsel, kemisk industri, **skill**](http://data.jobtechdev.se/taxonomy/concept/kfmY_4ux_JkM) |
| 11 | ...stri, raffinaderi eller annan **kemisk industri**. Som drifttekniker/processope... |  | x |  | 15 | [Processövervakare, kemisk industri, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/oJ1L_Daz_GQL) |
| 11 | ...stri, raffinaderi eller annan **kemisk industri**. Som drifttekniker/processope... |  | x |  | 15 | [Processoperatör, kemisk industri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xhPp_qTB_poK) |
| 11 | ...stri, raffinaderi eller annan **kemisk industri**. Som drifttekniker/processope... |  | x |  | 15 | [processoperatör, kemisk industri, gasanläggning, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ynmF_NsA_68a) |
| 12 | ...er annan kemisk industri. Som **drifttekniker**/processoperatör arbetar du ti... |  | x |  | 13 | [Fastighetstekniker/Drifttekniker, fastighet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9NVZ_LfD_vqT) |
| 12 | ...er annan kemisk industri. Som **drifttekniker**/processoperatör arbetar du ti... | x |  |  | 13 | [Drifttekniker och processövervakare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/nPFh_hia_3L5) |
| 13 | ...k industri. Som drifttekniker/**processoperatör** arbetar du tillsammans med di... | x |  |  | 15 | [Drifttekniker och processövervakare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/nPFh_hia_3L5) |
| 14 | ...kollegor i skiftlaget med att **övervaka produktionen**, lösa problem och utveckla pr... | x |  |  | 21 | [övervaka anläggningens produktion, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ayjS_iRf_wHv) |
| 15 | ...ed att övervaka produktionen, **lösa problem** och utveckla process och prod... | x |  |  | 12 | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| 16 | ...roduktionen, lösa problem och **utveckla process** och produkt. Yrkesrollen och ... | x |  |  | 16 | [Processutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuP9_SAc_iMs) |
| 17 | ...blem och utveckla process och **produkt**. Yrkesrollen och skiftarbetet... | x |  |  | 7 | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| 18 | ... arbetsgivare finns över hela **Sverige**.     Utbildningen är på två å... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 19 | ... finns över hela Sverige.     **Utbildningen** är på två år och går som en m... | x |  |  | 12 | [Högskoleutbildning, yrkesinriktad, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/zdKB_pTL_HMS) |
| 20 | ... år och går som en modifierad **distansutbildning**. Vid tio tillfällen träffas a... |  | x |  | 17 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 21 | ...nde. I de flesta kurser finns **böcker** framtagna för just den här ut... |  | x |  | 6 | [Böcker/Tidskrifter, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2VQ_P7u_GB4) |
| 22 | ... är gemensamma för att få den **grundkunskap** som behövs i all processindus... |  | x |  | 12 | [RTL-kompetens, grundkompetens, **skill**](http://data.jobtechdev.se/taxonomy/concept/ucBi_odb_sAy) |
| 23 | ...grundkunskap som behövs i all **processindustri**:  - Processtekniskt arbete  -... |  | x |  | 15 | [Processindustriteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/i2VJ_K67_uie) |
| 24 | ...övs i all processindustri:  - **Processtekniskt arbete**  - LIA I - Processtekniskt ar... | x |  |  | 22 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 25 | ...esstekniskt arbete  - LIA I - **Processtekniskt arbete**  - Energiteknik  - Processtek... | x |  |  | 22 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 26 | ...I - Processtekniskt arbete  - **Energiteknik**  - Processteknik I  - LIA II ... | x |  |  | 12 | [Energi, drift och underhåll, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6V78_5GY_XTu) |
| 26 | ...I - Processtekniskt arbete  - **Energiteknik**  - Processteknik I  - LIA II ... |  | x |  | 12 | [Energiteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/tNHP_38u_Pjt) |
| 27 | ...skt arbete  - Energiteknik  - **Processteknik** I  - LIA II - Industriella pr... |  | x |  | 13 | [Processteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Jgmc_3hD_JEt) |
| 27 | ...skt arbete  - Energiteknik  - **Processteknik** I  - LIA II - Industriella pr... | x |  |  | 13 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 28 | ...- Processteknik I  - LIA II - **Industriella processer**  - Processtyrning  - Underhål... | x |  |  | 22 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 29 | ...I - Industriella processer  - **Processtyrning**  - Underhållsteknik  - Proces... |  | x |  | 14 | [Processtyrning, **skill**](http://data.jobtechdev.se/taxonomy/concept/AQtr_C7f_Dyt) |
| 29 | ...I - Industriella processer  - **Processtyrning**  - Underhållsteknik  - Proces... | x |  |  | 14 | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
| 29 | ...I - Industriella processer  - **Processtyrning**  - Underhållsteknik  - Proces... |  | x |  | 14 | [programmerare för numeriska verktyg och numerisk processtyrning, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/DhnT_yH2_2n2) |
| 29 | ...I - Industriella processer  - **Processtyrning**  - Underhållsteknik  - Proces... |  | x |  | 14 | [tillämpa automatisk processtyrning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HaeY_HcG_TCi) |
| 29 | ...I - Industriella processer  - **Processtyrning**  - Underhållsteknik  - Proces... |  | x |  | 14 | [installera övervakare för processtyrning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KJ3m_NWd_337) |
| 29 | ...I - Industriella processer  - **Processtyrning**  - Underhållsteknik  - Proces... |  | x |  | 14 | [utföra processtyrning inom klädbranschen, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rPuE_AEf_vdS) |
| 30 | ...rocesser  - Processtyrning  - **Underhållsteknik**  - Processteknik II    Under ... |  | x |  | 16 | [Underhållsarbete, VA-nät/Underhållsarbete, gatunät, **skill**](http://data.jobtechdev.se/taxonomy/concept/2SCx_2dX_M79) |
| 30 | ...rocesser  - Processtyrning  - **Underhållsteknik**  - Processteknik II    Under ... | x |  |  | 16 | [Energi, drift och underhåll, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6V78_5GY_XTu) |
| 31 | ...yrning  - Underhållsteknik  - **Processteknik** II    Under det andra året sp... | x |  |  | 13 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 32 | ...mot antingen värmeproduktion, **skogsindustri** eller kemisk industri genom a... |  | x |  | 13 | [Högskoleingenjörsutbildning, skogsindustri, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KEZo_yVD_X4G) |
| 33 | ...oduktion, skogsindustri eller **kemisk industri** genom anpassade kurser, val a... |  | x |  | 15 | [Processövervakare, kemisk industri, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/QEBd_qHu_vGr) |
| 33 | ...oduktion, skogsindustri eller **kemisk industri** genom anpassade kurser, val a... |  | x |  | 15 | [Processövervakare inom kemisk industri, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/UsoN_wah_kh4) |
| 33 | ...oduktion, skogsindustri eller **kemisk industri** genom anpassade kurser, val a... |  | x |  | 15 | [processoperatör, kemisk industri, kväve och syre, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/dndm_Wm1_uw5) |
| 33 | ...oduktion, skogsindustri eller **kemisk industri** genom anpassade kurser, val a... |  | x |  | 15 | [processoperatör, kemisk industri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/giE8_5TQ_1vS) |
| 33 | ...oduktion, skogsindustri eller **kemisk industri** genom anpassade kurser, val a... |  | x |  | 15 | [Kvarnskötsel, kemisk industri, **skill**](http://data.jobtechdev.se/taxonomy/concept/kfmY_4ux_JkM) |
| 33 | ...oduktion, skogsindustri eller **kemisk industri** genom anpassade kurser, val a... |  | x |  | 15 | [Processövervakare, kemisk industri, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/oJ1L_Daz_GQL) |
| 33 | ...oduktion, skogsindustri eller **kemisk industri** genom anpassade kurser, val a... |  | x |  | 15 | [Processoperatör, kemisk industri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xhPp_qTB_poK) |
| 33 | ...oduktion, skogsindustri eller **kemisk industri** genom anpassade kurser, val a... |  | x |  | 15 | [processoperatör, kemisk industri, gasanläggning, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ynmF_NsA_68a) |
| | **Overall** | | | **7** | **924** | 7/924 = **1%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Underhållsarbete, VA-nät/Underhållsarbete, gatunät, **skill**](http://data.jobtechdev.se/taxonomy/concept/2SCx_2dX_M79) |
| x |  |  | [Energi, drift och underhåll, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6V78_5GY_XTu) |
|  | x |  | [Fastighetstekniker/Drifttekniker, fastighet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9NVZ_LfD_vqT) |
|  | x |  | [Processtyrning, **skill**](http://data.jobtechdev.se/taxonomy/concept/AQtr_C7f_Dyt) |
| x |  |  | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
|  | x |  | [Maskinskötsel, trä- och pappersindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/BGie_HPa_fNA) |
|  | x |  | [arbetsledare, raffinaderi, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/CnxK_rdw_U1N) |
|  | x |  | [Drifttekniker vid värme- och vattenverk, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Cppd_9P7_HeR) |
|  | x |  | [programmerare för numeriska verktyg och numerisk processtyrning, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/DhnT_yH2_2n2) |
|  | x |  | [Verksamhetsledning, värmeverk, **skill**](http://data.jobtechdev.se/taxonomy/concept/FfJq_b4n_DMM) |
|  | x |  | [Drifttekniker, värmeverk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GaC9_y4i_o8S) |
|  | x |  | [tillämpa automatisk processtyrning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HaeY_HcG_TCi) |
|  | x |  | [Processteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Jgmc_3hD_JEt) |
|  | x |  | [Driftmaskinist, värmeverk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KDG5_eSG_PMz) |
|  | x |  | [Högskoleingenjörsutbildning, skogsindustri, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KEZo_yVD_X4G) |
|  | x |  | [installera övervakare för processtyrning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KJ3m_NWd_337) |
|  | x |  | [Processoperatörer, trä- och pappersindustri, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MjXx_5mw_RNf) |
|  | x |  | [Processoperatörer, trä- och pappersindustri, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Mu2E_RpA_86z) |
| x |  |  | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
|  | x |  | [Processövervakare, kemisk industri, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/QEBd_qHu_vGr) |
| x |  |  | [Processutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuP9_SAc_iMs) |
|  | x |  | [Processövervakare inom kemisk industri, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/UsoN_wah_kh4) |
| x |  |  | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
|  | x |  | [Böcker/Tidskrifter, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2VQ_P7u_GB4) |
|  | x |  | [ingenjör, pappersindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ZZbG_NWk_KVi) |
|  | x |  | [Drifttekniker vid värme- och vattenverk, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/ZmsL_fPS_5Jr) |
| x |  |  | [övervaka anläggningens produktion, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ayjS_iRf_wHv) |
|  | x |  | [processoperatör, kemisk industri, kväve och syre, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/dndm_Wm1_uw5) |
|  | x |  | [processoperatör, kemisk industri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/giE8_5TQ_1vS) |
|  | x |  | [Processindustriteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/i2VJ_K67_uie) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Kvarnskötsel, kemisk industri, **skill**](http://data.jobtechdev.se/taxonomy/concept/kfmY_4ux_JkM) |
|  | x |  | [Driftingenjör, värmeverk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ms98_iAJ_Cym) |
| x |  |  | [Drifttekniker och processövervakare, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/nPFh_hia_3L5) |
|  | x |  | [Processövervakare, kemisk industri, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/oJ1L_Daz_GQL) |
|  | x |  | [utföra processtyrning inom klädbranschen, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rPuE_AEf_vdS) |
|  | x |  | [Energiteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/tNHP_38u_Pjt) |
|  | x |  | [RTL-kompetens, grundkompetens, **skill**](http://data.jobtechdev.se/taxonomy/concept/ucBi_odb_sAy) |
| x |  |  | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
|  | x |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
|  | x |  | [Processoperatör, kemisk industri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xhPp_qTB_poK) |
|  | x |  | [processoperatör, kemisk industri, gasanläggning, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ynmF_NsA_68a) |
| x |  |  | [Högskoleutbildning, yrkesinriktad, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/zdKB_pTL_HMS) |
| | | **1** | 1/43 = **2%** |