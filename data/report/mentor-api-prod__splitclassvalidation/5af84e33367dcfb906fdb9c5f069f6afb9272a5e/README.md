# Results for '5af84e33367dcfb906fdb9c5f069f6afb9272a5e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [5af84e33367dcfb906fdb9c5f069f6afb9272a5e](README.md) | 1 | 4047 | 26 | 16 | 32/250 = **13%** | 4/22 = **18%** |

## Source text

 Developer(677033) Exciting Opportunity We are currently looking for experienced C++ Software Developers! We have one open positions as Developer in our TrxCtrl team within the CRM Algorithms Function Control section placed in Stockholm. TrxCtrl is a separate software module in the Radio software. It is handling configuration of TX linearization, therefore competence in both software development and general radio functionality is essential. The development ranges from early studies to test activities and maintenance. The work will include integration activities for new HW or SW features as well as design and test systems. In this position you will work with software controlling and configuring the radio hardware. The goal is to develop a high quality software with low maintenance needs. Our organization works in Lean and Agile ways. We coordinate multi-functional development teams in which continuous improvement and knowledge exchange is part of the daily work.   You will Contribute in Implementing new functionality and test cases for TrxCtrl Provide troubleshooting and help other developers. Test your work in the lab with radio hardware and RF instruments    To be successful in the role you must have University degree in Computer or Electrical Engineering or equivalent Good programming skills in C++ (obligatory) and Java (nice to have) Minimum 2 years of commercial experience in a relevant position Good knowledge in Wireless Communication systems Good knowledge in Radio hardware and software Integration Knowledge in Radio HW- and SW- troubleshooting Experience of technical work within wireless communication systems and radio design is preferable Good communication skills and social ability Innovative thinking and problem solving skills Proficiency in English (speaking and writing)    What´s in it for you? Here at Ericsson, our culture is built on over a century of courageous decisions. With us, you will no longer be dreaming of what the future holds – you will be redefining it. You won’t develop for the status quo, but will build what replaces it. Joining us is a way to move your career in any direction you want; with hundreds of career opportunities in locations all over the world, in a place where co-creation and collaboration are embedded into the walls. You will find yourself in a speak-up environment where empathy and humanness serve as cornerstones for how we work, and where work-life balance is a priority. Welcome to an inclusive, global company where your chance to create an impact is endless.   What happens once you apply? To prepare yourself for next steps, please explore here: https://www.ericsson.com/en/careers/job-opportunities/hiring-process   Application: We look forward to receiving your application in English. Please note we cannot accept applications via email.   Primary location for this role: Kista, Sweden.   For specific questions please contact Recruiter: Aleksandra Rusa-Warda aleksandra.rusa-warda@ericsson.com You will report to Manager DNEW RA CRM ALG Algorithm.   We welcome the opportunity to meet you!   Curious to know more about the life at Ericsson? Meet some of your future colleagues and watch our People film. We will ensure that individuals with disabilities are provided reasonable accommodation to participate in the job application or interview process, to perform crucial job functions, and to receive other benefits and privileges of employment. Please contact us to request accommodation.   Encouraging a diverse and inclusive organization is core to our values at Ericsson, that's why we nurture it in everything we do. We truly believe that by collaborating with people with different experiences we drive innovation, which is essential for our future growth. We encourage people from all backgrounds to apply and realize their full potential as part of our Ericsson team. Ericsson is proud to be an Equal Opportunity and Affirmative Action employer, learn more. Primary country and city: Sweden (SE) \|\| Sweden : Stockholm : Stockholm  Req ID: 677033

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ently looking for experienced **C++** Software Developers! We have ... |  | x |  | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 2 | ...ently looking for experienced **C++ Software Developers**! We have one open positions a... | x |  |  | 23 | [C++-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/FUu4_NC9_YF9) |
| 3 | ...n our TrxCtrl team within the **CRM** Algorithms Function Control s... | x | x | 3 | 3 | [Customer Relationship Management/CRM, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQre_aBF_3dE) |
| 4 | ...ion Control section placed in **Stockholm**. TrxCtrl is a separate softwa... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 5 | ...parate software module in the **Radio** software. It is handling conf... | x |  |  | 5 | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| 6 | ...tware development and general **radio** functionality is essential. T... | x |  |  | 5 | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| 7 | ...ntrolling and configuring the **radio** hardware. The goal is to deve... | x |  |  | 5 | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| 8 | ...ds. Our organization works in **Lean** and Agile ways. We coordinate... | x | x | 4 | 4 | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| 9 | ...rganization works in Lean and **Agile** ways. We coordinate multi-fun... | x |  |  | 5 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 9 | ...rganization works in Lean and **Agile** ways. We coordinate multi-fun... |  | x |  | 5 | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| 10 | ...est your work in the lab with **radio** hardware and RF instruments  ... | x |  |  | 5 | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| 11 | ...e lab with radio hardware and **RF** instruments    To be successf... |  | x |  | 2 | [RF, radiotransmission, **skill**](http://data.jobtechdev.se/taxonomy/concept/mpEr_PgY_JSN) |
| 12 | ...ful in the role you must have **University degree** in Computer or Electrical Eng... | x |  |  | 17 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 13 | ...ust have University degree in **Computer** or Electrical Engineering or ... | x |  |  | 8 | [Civilingenjörsutbildning, programvaruteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/auQ9_2FZ_KXb) |
| 14 | ...versity degree in Computer or **Electrical Engineering** or equivalent Good programmin... | x |  |  | 22 | [Ingenjörsutbildning, energi- och elektroteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/P2vF_zbH_Nd8) |
| 15 | ...nt Good programming skills in **C++** (obligatory) and Java (nice t... |  | x |  | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 16 | ...kills in C++ (obligatory) and **Java** (nice to have) Minimum 2 year... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 17 | ...in C++ (obligatory) and Java (**nice** to have) Minimum 2 years of c... |  | x |  | 4 | [Nice, hänvisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/xHdj_p3H_7QM) |
| 18 | ...d Java (nice to have) Minimum **2 years of** commercial experience in a re... | x |  |  | 10 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 19 | ...Minimum 2 years of commercial **experience** in a relevant position Good k... | x |  |  | 10 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 20 | ...ion systems Good knowledge in **Radio** hardware and software Integra... | x |  |  | 5 | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| 21 | ...ware Integration Knowledge in **Radio** HW- and SW- troubleshooting E... | x |  |  | 5 | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| 22 | ...ess communication systems and **radio** design is preferable Good com... | x |  |  | 5 | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| 23 | ...solving skills Proficiency in **English** (speaking and writing)    Wha... | x |  |  | 7 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 24 | ...reation and collaboration are **embedded** into the walls. You will find... |  | x |  | 8 | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| 24 | ...reation and collaboration are **embedded** into the walls. You will find... |  | x |  | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 25 | ...ocation for this role: Kista, **Sweden**.   For specific questions ple... | x |  |  | 6 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 26 | ...icsson.com You will report to **Manager** DNEW RA CRM ALG Algorithm.   ... | x |  |  | 7 | [Manager, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/X9jv_K2b_m48) |
| 26 | ...icsson.com You will report to **Manager** DNEW RA CRM ALG Algorithm.   ... |  | x |  | 7 | [Google Analytics, **skill**](http://data.jobtechdev.se/taxonomy/concept/oCKN_G9k_zTj) |
| 27 | ...ill report to Manager DNEW RA **CRM** ALG Algorithm.   We welcome t... | x | x | 3 | 3 | [Customer Relationship Management/CRM, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQre_aBF_3dE) |
| 28 | ...employer, learn more. Primary **country** and city: Sweden (SE) \|\| Swed... |  | x |  | 7 | [Country, **skill**](http://data.jobtechdev.se/taxonomy/concept/LoLr_uQx_eKL) |
| 29 | ...re. Primary country and city: **Sweden** (SE) \|\| Sweden : Stockholm : ... | x |  |  | 6 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 30 | ...ntry and city: Sweden (SE) \|\| **Sweden** : Stockholm : Stockholm  Req ... | x |  |  | 6 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 31 | ...city: Sweden (SE) \|\| Sweden : **Stockholm** : Stockholm  Req ID: 677033 | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 32 | ... (SE) \|\| Sweden : Stockholm : **Stockholm**  Req ID: 677033 | x |  |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| | **Overall** | | | **32** | **250** | 32/250 = **13%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| x |  |  | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| x | x | x | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [C++-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/FUu4_NC9_YF9) |
|  | x |  | [Country, **skill**](http://data.jobtechdev.se/taxonomy/concept/LoLr_uQx_eKL) |
| x |  |  | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [Ingenjörsutbildning, energi- och elektroteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/P2vF_zbH_Nd8) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| x |  |  | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| x |  |  | [Manager, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/X9jv_K2b_m48) |
| x |  |  | [Civilingenjörsutbildning, programvaruteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/auQ9_2FZ_KXb) |
|  | x |  | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| x | x | x | [Customer Relationship Management/CRM, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQre_aBF_3dE) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
|  | x |  | [RF, radiotransmission, **skill**](http://data.jobtechdev.se/taxonomy/concept/mpEr_PgY_JSN) |
|  | x |  | [Google Analytics, **skill**](http://data.jobtechdev.se/taxonomy/concept/oCKN_G9k_zTj) |
|  | x |  | [Nice, hänvisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/xHdj_p3H_7QM) |
|  | x |  | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
|  | x |  | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| | | **4** | 4/22 = **18%** |