# Results for 'c5e7d0ed1075ca48e09b58e8d9a34e1317834260'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c5e7d0ed1075ca48e09b58e8d9a34e1317834260](README.md) | 1 | 2364 | 31 | 16 | 118/311 = **38%** | 7/17 = **41%** |

## Source text

Hundfrisör med möjlighet till deltid i butik VILL DU JOBBA HOS OSS? Vårt hundtrim växer så det knakar och vi söker dig som är utbildad hundfrisör! Vi är ett gäng härliga personer som söker dig som vill jobba i ett mindre team där allas insatser räknas. Vi söker dig som trivs med att arbeta självständigt i hundtrim samt jobba i butik, tycker att kundmöten och service är roligt, samt får energi av att hjälpa andra människor och djur. Du bör även vara ordningsam och ha lätt för att prioritera när arbetstempot är högre. Löpande uttagningar sker under ansökningsperioden. Det är ett krav att du är certifierad eller utbildad hundfrisör eller påbörjad utbildning.   OM TJÄNSTEN: Arbetsuppgifter Arbetet består i första hand hundfrisering ca en till två dagar i veckan samt tillhörande städning, renhållning och bokning, där du sköter störst del av kundkontakten. För butiksdelen gäller vanligt förekommande butiksuppgifter där du som medarbetare ger god service till våra kunder, plockar upp varor, frontar hyllor, håller fräscht och rent i butiken samt tar betalt av kunder. Det kan även bli aktuellt att hjälpa till med orderplock m.m. för vår e-handel. Hos oss är ingen dag den andra lik, det är därför viktigt att du är trygg i dig själv, tycker om att förändra och förbättra och samtidigt är redo att lösa problem. Vi ser gärna att du som söker tjänsten är: Över 20 år. Ordningsam. "Doer" Tidigare erfarenheter från butik. Stresstålig. Kunnig inom något djurslag. Serviceinriktad. Problemlösare. Ansvarstagande. Självgående. Stort plus om du har gått utbildningar inom något djurslag sedan tidigare. Vältalig & socialt kompetent i svenska språket i såväl skrift och tal.    Omfattning Hundfrisör: ca 1-2 dagar i veckan. Vi tillämpar provanställning om 6 månader. Lön Butik: Fast timlön enligt ö.k. Hundfrisering: Provisionsbaserad Intervjuer sker löpande och tjänsten kan bli tillsatt innan sista datum. Om oss: Vi är en härlig blandning av nördar inom olika djurslag som tycker det är kul att ge våra kunder en riktigt hög servicenivå med en personlig touch. Vi vill inte vara som alla andra, vi vill ge våra kunder personliga möten där vi vet vilka våra kunder är och verkligen bryr oss om våra kunder. Vi är ett familjeföretag i snart tre generationer med en fysisk butik i Sickla Köpkvarter och långt gångna planer för expansioner samt e-handel som växer.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Hundfrisör** med möjlighet till deltid i b... | x | x | 10 | 10 | [Hundfrisör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ATkc_FM3_GRB) |
| 2 | Hundfrisör med möjlighet till **deltid** i butik VILL DU JOBBA HOS OSS... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 3 | ...r med möjlighet till deltid i **butik** VILL DU JOBBA HOS OSS? Vårt h... | x | x | 5 | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 4 | ...k VILL DU JOBBA HOS OSS? Vårt **hundtrim** växer så det knakar och vi sö... | x |  |  | 8 | [Hundtrimning, **skill**](http://data.jobtechdev.se/taxonomy/concept/yuRa_gmr_9J6) |
| 5 | ... vi söker dig som är utbildad **hundfrisör**! Vi är ett gäng härliga perso... | x | x | 10 | 10 | [Hundfrisör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ATkc_FM3_GRB) |
| 6 | ...i söker dig som trivs med att **arbeta självständigt** i hundtrim samt jobba i butik... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 7 | ...ed att arbeta självständigt i **hundtrim** samt jobba i butik, tycker at... | x |  |  | 8 | [Hundtrimning, **skill**](http://data.jobtechdev.se/taxonomy/concept/yuRa_gmr_9J6) |
| 8 | ...ndigt i hundtrim samt jobba i **butik**, tycker att kundmöten och ser... | x | x | 5 | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 9 | ...amt jobba i butik, tycker att **kundmöten** och service är roligt, samt f... | x |  |  | 9 | [kommunicera med kunder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sPCy_icd_mxy) |
| 10 | ...tt hjälpa andra människor och **djur**. Du bör även vara ordningsam ... | x | x | 4 | 4 | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| 11 | ...är certifierad eller utbildad **hundfrisör** eller påbörjad utbildning.   ... | x | x | 10 | 10 | [Hundfrisör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ATkc_FM3_GRB) |
| 12 | ... Arbetet består i första hand **hundfrisering** ca en till två dagar i veckan... | x |  |  | 13 | [Hundfrisör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ATkc_FM3_GRB) |
| 13 | ...gar i veckan samt tillhörande **städning**, renhållning och bokning, där... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 13 | ...gar i veckan samt tillhörande **städning**, renhållning och bokning, där... | x |  |  | 8 | [Tvätt- och städningsansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/tXiX_tYU_gkE) |
| 14 | ...an samt tillhörande städning, **renhållning** och bokning, där du sköter st... | x | x | 11 | 11 | [Renhållning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1eVm_yZa_9yi) |
| 15 | ...nde städning, renhållning och **bokning**, där du sköter störst del av ... | x |  |  | 7 | [göra bokningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hcSG_xG4_WUt) |
| 16 | ..., där du sköter störst del av **kundkontakten**. För butiksdelen gäller vanli... | x |  |  | 13 | [kommunicera med kunder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sPCy_icd_mxy) |
| 17 | ...n gäller vanligt förekommande **butiksuppgifter** där du som medarbetare ger go... | x |  |  | 15 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 18 | ...l med orderplock m.m. för vår **e-handel**. Hos oss är ingen dag den and... | x |  |  | 8 | [E-handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yxpW_N32_YMK) |
| 19 | ...r" Tidigare erfarenheter från **butik**. Stresstålig. Kunnig inom någ... | x | x | 5 | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 20 | ...gare erfarenheter från butik. **Stresstålig**. Kunnig inom något djurslag. ... | x | x | 11 | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 21 | ...tresstålig. Kunnig inom något **djurslag**. Serviceinriktad. Problemlösa... | x |  |  | 8 | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| 22 | ...rviceinriktad. Problemlösare. **Ansvarstagande**. Självgående. Stort plus om d... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 23 | ... gått utbildningar inom något **djurslag** sedan tidigare. Vältalig & so... | x |  |  | 8 | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| 24 | ...ältalig & socialt kompetent i **svenska** språket i såväl skrift och ta... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 25 | ... & socialt kompetent i svenska** språket** i såväl skrift och tal.    Om... | x |  |  | 8 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 26 | ...skrift och tal.    Omfattning **Hundfrisör**: ca 1-2 dagar i veckan. Vi ti... | x | x | 10 | 10 | [Hundfrisör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ATkc_FM3_GRB) |
| 27 | ...anställning om 6 månader. Lön **Butik**: Fast timlön enligt ö.k. Hund... | x | x | 5 | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 28 | ...om 6 månader. Lön Butik: Fast **timlön** enligt ö.k. Hundfrisering: Pr... | x |  |  | 6 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 29 | ...utik: Fast timlön enligt ö.k. **Hundfrisering**: Provisionsbaserad Intervjuer... | x |  |  | 13 | [Hundfrisör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ATkc_FM3_GRB) |
| 30 | ...ön enligt ö.k. Hundfrisering: **Provisionsbaserad** Intervjuer sker löpande och t... | x |  |  | 17 | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
| 31 | ...landning av nördar inom olika **djurslag** som tycker det är kul att ge ... | x |  |  | 8 | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| 32 | ...re generationer med en fysisk **butik** i Sickla Köpkvarter och långt... | x | x | 5 | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 33 | ...a planer för expansioner samt **e-handel** som växer. | x |  |  | 8 | [E-handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yxpW_N32_YMK) |
| | **Overall** | | | **118** | **311** | 118/311 = **38%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Renhållning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1eVm_yZa_9yi) |
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Hundfrisör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ATkc_FM3_GRB) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [göra bokningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hcSG_xG4_WUt) |
| x |  |  | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| x |  |  | [kommunicera med kunder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sPCy_icd_mxy) |
| x |  |  | [Tvätt- och städningsansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/tXiX_tYU_gkE) |
| x | x | x | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| x |  |  | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
| x | x | x | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| x |  |  | [Hundtrimning, **skill**](http://data.jobtechdev.se/taxonomy/concept/yuRa_gmr_9J6) |
| x |  |  | [E-handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yxpW_N32_YMK) |
| x | x | x | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/17 = **41%** |