# Results for 'b142e1264b72aca5154d4981544fc0f765ebec04'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b142e1264b72aca5154d4981544fc0f765ebec04](README.md) | 1 | 1216 | 9 | 9 | 77/101 = **76%** | 4/6 = **67%** |

## Source text

Lärare i Sv, So och Sva år 4-6 till Bokelundaskolan Örkelljunga är en del av ”Familjen Helsingborg” där 11 kommuner i det expansiva Skåne Nordväst tillsammans arbetar för att gynna den gemensamma regionen. Örkelljunga är en kommun med stabil ekonomi och har idag 10 000 invånare. Örkelljunga kännetecknas av en närhet där ett starkt samarbete och korta beslutsvägar utgör styrkan i den lilla kommunen. Här har du som medarbetare större möjlighet att påverka din personliga utveckling likväl som verksamhetens utveckling i stort. I Örkelljunga kommun arbetar vi med ett professionellt bemötande präglat av våra tre värdeord; kunskap, omtanke och handling. Som en del av Örkelljunga kommun har du möjlighet att arbeta gränsöverskridande och göra skillnad.  ARBETSUPPGIFTER Undervisning i åk. 4-6  KVALIFIKATIONER Erfarenhet av att arbeta på mellanstadiet. Lärarlegitimation.  ÖVRIGT Som ett led i att bli en mer hälsobringande kommun tillämpar vi rökfri arbetstid. Vi bedriver både jämställdhets och mångfalds främjande insatser. Vi ber dig att söka digitalt för att säkerställa kvalitet och vår hantering av ansökningarna. Vi undanber oss vänligt men bestämt att bli kontaktade av annons och rekryteringsföretag.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Lärare** i Sv, So och Sva år 4-6 till ... | x |  |  | 6 | [Grundlärare, 4-6, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Nzmh_iTb_z7h) |
| 2 | ...a år 4-6 till Bokelundaskolan **Örkelljunga** är en del av ”Familjen Helsin... | x | x | 11 | 11 | [Örkelljunga, **municipality**](http://data.jobtechdev.se/taxonomy/concept/nBTS_Nge_dVH) |
| 3 | ...ljunga är en del av ”Familjen **Helsingborg**” där 11 kommuner i det expans... | x | x | 11 | 11 | [Helsingborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qj3q_oXH_MGR) |
| 4 | ...r 11 kommuner i det expansiva **Skåne** Nordväst tillsammans arbetar ... | x | x | 5 | 5 | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| 5 | ...ynna den gemensamma regionen. **Örkelljunga** är en kommun med stabil ekono... | x | x | 11 | 11 | [Örkelljunga, **municipality**](http://data.jobtechdev.se/taxonomy/concept/nBTS_Nge_dVH) |
| 6 | ...junga är en kommun med stabil **ekonomi** och har idag 10 000 invånare.... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 7 | ...och har idag 10 000 invånare. **Örkelljunga** kännetecknas av en närhet där... |  | x |  | 11 | [Örkelljunga, **municipality**](http://data.jobtechdev.se/taxonomy/concept/nBTS_Nge_dVH) |
| 8 | ...mhetens utveckling i stort. I **Örkelljunga** kommun arbetar vi med ett pro... | x | x | 11 | 11 | [Örkelljunga, **municipality**](http://data.jobtechdev.se/taxonomy/concept/nBTS_Nge_dVH) |
| 9 | ...e och handling. Som en del av **Örkelljunga** kommun har du möjlighet att a... | x | x | 11 | 11 | [Örkelljunga, **municipality**](http://data.jobtechdev.se/taxonomy/concept/nBTS_Nge_dVH) |
| 10 | ... att arbeta på mellanstadiet. **Lärarlegitimation**.  ÖVRIGT Som ett led i att b... | x | x | 17 | 17 | [Lärarlegitimation, **skill**](http://data.jobtechdev.se/taxonomy/concept/yKnk_W7X_Zvk) |
| | **Overall** | | | **77** | **101** | 77/101 = **76%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| x |  |  | [Grundlärare, 4-6, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Nzmh_iTb_z7h) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x | x | x | [Örkelljunga, **municipality**](http://data.jobtechdev.se/taxonomy/concept/nBTS_Nge_dVH) |
| x | x | x | [Helsingborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qj3q_oXH_MGR) |
| x | x | x | [Lärarlegitimation, **skill**](http://data.jobtechdev.se/taxonomy/concept/yKnk_W7X_Zvk) |
| | | **4** | 4/6 = **67%** |