# Results for '63cea388c08b4b55c210ed71ea13a18f6a50bb3c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [63cea388c08b4b55c210ed71ea13a18f6a50bb3c](README.md) | 1 | 1205 | 8 | 4 | 0/171 = **0%** | 0/10 = **0%** |

## Source text

Kanske fascineras du av produktdesign och funderar på varför produkter har en viss form? Inom CAD-konstruktion använder man sig av olika verktyg för att modellera, utveckla och tillverka produkter inom ett stort antal områden från hushållsprodukter till fordon. CAD-modeller används bland annat för att visualisera produkter innan fysisk tillverkning.    Som CAD-konstruktör arbetar du med att designa och utveckla produkter i 3D med inriktning mot industrin. Kompetensen efterfrågas i allt större utsträckning, då allt fler komplexa produkter, material och tillverkningsprocesser uppkommer. I yrkesrollen har man många olika branscher och områden att specialisera sig mot och innefattar allt från design av hushållsprodukter till fordon.       I utbildningen lär du dig alla de avancerade CAD-system som krävs och får kunskap inom de olika metoder och processer som branschen använder sig av. Du lär dig analysera detaljkonstruktion och får djupare förståelse för hur ritningsstandarder används. Du lär dig även att jobba med att skapa produktionsprojekt utifrån kravspecifikation.        Vill du veta mer? Läs mer om utbildningen och ansök på:   https://www.ecutbildning.se/utbildningar/cad-konstruktor/

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...dukter har en viss form? Inom **CAD-konstruktion** använder man sig av olika ver... |  | x |  | 16 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 2 | ... sig av olika verktyg för att **modellera**, utveckla och tillverka produ... | x |  |  | 9 | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| 3 | ...ka verktyg för att modellera, **utveckla** och tillverka produkter inom ... | x |  |  | 8 | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| 4 | ...lisera produkter innan fysisk **tillverkning**.    Som CAD-konstruktör arbet... |  | x |  | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 5 | ...n fysisk tillverkning.    Som **CAD-konstruktör** arbetar du med att designa oc... |  | x |  | 15 | [CAD-konstruktör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/C41S_vox_2gE) |
| 5 | ...n fysisk tillverkning.    Som **CAD-konstruktör** arbetar du med att designa oc... |  | x |  | 15 | [CAD-ritare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/KWNv_oWK_5E2) |
| 5 | ...n fysisk tillverkning.    Som **CAD-konstruktör** arbetar du med att designa oc... | x |  |  | 15 | [CAD-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/piwA_uGN_4s6) |
| 6 | ...onstruktör arbetar du med att **designa och utveckla produkter i 3D** med inriktning mot industrin.... | x |  |  | 35 | [3D-modellering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Uj8G_QVJ_TWj) |
| 7 | ...lär du dig alla de avancerade **CAD-system** som krävs och får kunskap ino... | x |  |  | 10 | [CAD-konstruktion/CAD-ritning, **skill**](http://data.jobtechdev.se/taxonomy/concept/oaiv_GsB_AFK) |
| 8 | ... sig av. Du lär dig analysera **detaljkonstruktion** och får djupare förståelse fö... | x |  |  | 18 | [Digital konstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/WHyi_vfU_5Bg) |
| 9 | ...år djupare förståelse för hur **ritningsstandarder** används. Du lär dig även att ... | x |  |  | 18 | [CAD-konstruktion/CAD-ritning, **skill**](http://data.jobtechdev.se/taxonomy/concept/oaiv_GsB_AFK) |
| | **Overall** | | | **0** | **171** | 0/171 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [CAD-konstruktör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/C41S_vox_2gE) |
|  | x |  | [CAD-ritare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/KWNv_oWK_5E2) |
|  | x |  | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| x |  |  | [3D-modellering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Uj8G_QVJ_TWj) |
| x |  |  | [Digital konstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/WHyi_vfU_5Bg) |
| x |  |  | [CAD-konstruktion/CAD-ritning, **skill**](http://data.jobtechdev.se/taxonomy/concept/oaiv_GsB_AFK) |
| x |  |  | [CAD-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/piwA_uGN_4s6) |
|  | x |  | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| x |  |  | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| x |  |  | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| | | **0** | 0/10 = **0%** |