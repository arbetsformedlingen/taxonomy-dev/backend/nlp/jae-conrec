# Results for 'c5f50697102b21ee2ef6bae298825cdebe23852e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c5f50697102b21ee2ef6bae298825cdebe23852e](README.md) | 1 | 327 | 5 | 4 | 19/57 = **33%** | 2/5 = **40%** |

## Source text

Pizzaförbredning och diskare  Hej ja söker personal för förbredning av pizza och diskare . Du ska komma i tid och vara lite stresstålig. Du ska vara också trevlig mot kunder  Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Pizzaförbredning** och diskare  Hej ja söker per... | x |  |  | 16 | [tillaga pizza, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6g2p_euz_vwn) |
| 2 | Pizzaförbredning och **diskare**  Hej ja söker personal för fö... | x | x | 7 | 7 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| 3 | ...r personal för förbredning av **pizza** och diskare . Du ska komma i ... | x | x | 5 | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 4 | ... för förbredning av pizza och **diskare** . Du ska komma i tid och vara... | x | x | 7 | 7 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| 5 | ...ska komma i tid och vara lite **stresstålig**. Du ska vara också trevlig mo... | x |  |  | 11 | [reagera lugnt i stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WX8v_Fni_hwE) |
| 5 | ...ska komma i tid och vara lite **stresstålig**. Du ska vara också trevlig mo... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | **Overall** | | | **19** | **57** | 19/57 = **33%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [tillaga pizza, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6g2p_euz_vwn) |
| x |  |  | [reagera lugnt i stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WX8v_Fni_hwE) |
| x | x | x | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| x | x | x | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **2** | 2/5 = **40%** |