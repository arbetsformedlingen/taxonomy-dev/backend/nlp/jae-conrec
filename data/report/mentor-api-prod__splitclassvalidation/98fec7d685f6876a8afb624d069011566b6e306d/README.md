# Results for '98fec7d685f6876a8afb624d069011566b6e306d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [98fec7d685f6876a8afb624d069011566b6e306d](README.md) | 1 | 3002 | 14 | 11 | 128/245 = **52%** | 5/12 = **42%** |

## Source text

Entreprenadingenjör Nu söker vi på Peab Skellefteå en Entreprenadingenjör som vill vara med och bidra till våra byggprojekt. Hos oss arbetar du tillsammans med engagerade och skickliga kollegor och har goda möjligheter att utvecklas i din yrkesroll. Välkommen till oss!  Vi erbjuder   Ett spännande och givande arbete i ett bra team. Peab är Nordens samhällsbyggare med det stora företagets resurser. Samtidigt sysslar vi med det vi kallar för ”närproducerat byggande”, som en väletablerad lokal byggentreprenör. Hos oss har du stora möjligheter att utvecklas i din karriär- och samtidigt ha roligt på vägen! Du har gott stöd av kompetenta kollegor när du själv stöttar våra olika byggprojekt, allt från mindre ombyggnationer till bostäder och industriprojekt.  Om tjänsten  Som entreprenadingenjör kommer du att arbeta med anbudsunderlag och förfrågningsunderlag fram till färdigt anbud. Det innebär att du har kontakt med många olika samarbetspartners, kunder och kollegor. Du kommer också att analysera, identifiera och hitta alternativa konkurrenskraftiga lösningar på våra kunders projektplaner.   I våra projekt kommer du sedan att arbeta med projektekonomi, kontraktsadministration och projektadministration.  Övriga arbetsuppgifter består i att:  - Arbeta med hela försäljningsprocessen från tidig förfrågan till färdigt anbud - Stödja och driva det ekonomiska arbetet i projektet. - Hantera fakturering mot kund och beställare. - Stödja projektets prognosarbete, betalplaner och tidsplanering.  - Ansvara för att arbete styrs utifrån kontrakt och avtal samt stödja produktionen i att hantera avvikelser. - Bistå produktionen vid ÄTA-arbeten genom framtagande av kalkylunderlag  Om dig  Vi söker dig som har en teknisk utbildning på högskolenivå eller motsvarande kunskaper som du tillgodogjort dig genom erfarenhet i byggbranschen. Det är viktigt att du har god dator- och systemvana samt en hög ekonomisk medvetenhet. Erfarenhet av kalkyl- och anbudsarbete inom byggbranschen, kunskap om kontraktsadministration och god inblick i byggprocessen är klart meriterande.   Du är en ansvarsfull person som utför ditt arbete på ett organiserat och välplanerat sätt, där du sätter kvalitet före kvantitet i det du gör. Du är engagerad och kan självständigt driva ditt arbete framåt, samtidigt som du har en god samarbetsförmåga med såväl externa aktörer som kollegor. Slutligen vill vi att du delar våra kärnvärden; jordnära, utvecklande, personlig och pålitlig. Dessa värden är en röd tråd i allt det vi gör och kännetecknar hur vi arbetar tillsammans.  Ansökan och övrig information  Klicka på "Sök tjänsten" nedan för att komma till vårt rekryteringssystem och söka jobbet som entreprenadingenjör hos oss. Vi kommer att arbeta med löpande urval från augusti, så skicka gärna in din ansökan så snart som möjligt.  Vid frågor om tjänsten är du välkommen att kontakta;  Emma Nilsson, HR-partner: 073-337 16 46  Jens Skoglind, Arbetschef: 073-337 23 88  Vi finns tillgängliga för frågor från 1 augusti

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Entreprenadingenjör** Nu söker vi på Peab Skellefte... | x | x | 19 | 19 | [Entreprenadingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/namT_cva_UiK) |
| 2 | ...dingenjör Nu söker vi på Peab **Skellefteå** en Entreprenadingenjör som vi... | x | x | 10 | 10 | [Skellefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kicB_LgH_2Dk) |
| 3 | ...öker vi på Peab Skellefteå en **Entreprenadingenjör** som vill vara med och bidra t... | x | x | 19 | 19 | [Entreprenadingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/namT_cva_UiK) |
| 4 | ...triprojekt.  Om tjänsten  Som **entreprenadingenjör** kommer du att arbeta med anbu... | x | x | 19 | 19 | [Entreprenadingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/namT_cva_UiK) |
| 5 | ...ommer du sedan att arbeta med **projektekonomi**, kontraktsadministration och ... |  | x |  | 14 | [Ekonomi, undervisning högskolenivå, **skill**](http://data.jobtechdev.se/taxonomy/concept/9Ra9_2yh_jH4) |
| 6 | ..., kontraktsadministration och **projektadministration**.  Övriga arbetsuppgifter best... | x | x | 21 | 21 | [Projektadministration, **skill**](http://data.jobtechdev.se/taxonomy/concept/NMZ1_bCs_WcR) |
| 7 | ... dig  Vi söker dig som har en **teknisk utbildning** på högskolenivå eller motsvar... |  | x |  | 18 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 8 | ... har en teknisk utbildning på **högskolenivå** eller motsvarande kunskaper s... | x |  |  | 12 | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| 9 | ...ogjort dig genom erfarenhet i **byggbranschen**. Det är viktigt att du har go... | x |  |  | 13 | [byggbranschen, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Dq3z_72P_8Zy) |
| 10 | ...igt att du har god dator- och **systemvana** samt en hög ekonomisk medvete... |  | x |  | 10 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 11 | ...kalkyl- och anbudsarbete inom **byggbranschen**, kunskap om kontraktsadminist... | x |  |  | 13 | [byggbranschen, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Dq3z_72P_8Zy) |
| 12 | ...klart meriterande.   Du är en **ansvarsfull** person som utför ditt arbete ... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 13 | ... gör. Du är engagerad och kan **självständigt** driva ditt arbete framåt, sam... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 14 | ..., samtidigt som du har en god **samarbetsförmåga** med såväl externa aktörer som... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 15 | ...med såväl externa aktörer som **kollegor**. Slutligen vill vi att du del... | x |  |  | 8 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 16 | ...ngssystem och söka jobbet som **entreprenadingenjör** hos oss. Vi kommer att arbeta... | x | x | 19 | 19 | [Entreprenadingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/namT_cva_UiK) |
| 17 | ... att kontakta;  Emma Nilsson, **HR-partner**: 073-337 16 46  Jens Skoglind... | x | x | 10 | 10 | [HR-generalist/HR-partner, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ds7X_mdp_bPc) |
| | **Overall** | | | **128** | **245** | 128/245 = **52%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Ekonomi, undervisning högskolenivå, **skill**](http://data.jobtechdev.se/taxonomy/concept/9Ra9_2yh_jH4) |
| x |  |  | [byggbranschen, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Dq3z_72P_8Zy) |
|  | x |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
|  | x |  | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| x | x | x | [Projektadministration, **skill**](http://data.jobtechdev.se/taxonomy/concept/NMZ1_bCs_WcR) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [HR-generalist/HR-partner, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ds7X_mdp_bPc) |
| x | x | x | [Skellefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kicB_LgH_2Dk) |
| x | x | x | [Entreprenadingenjör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/namT_cva_UiK) |
| x |  |  | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| | | **5** | 5/12 = **42%** |