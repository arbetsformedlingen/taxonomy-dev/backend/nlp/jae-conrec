# Results for 'ed27b5bf31a4e8b24f9a8e56ca74688826307953'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ed27b5bf31a4e8b24f9a8e56ca74688826307953](README.md) | 1 | 1009 | 8 | 6 | 66/74 = **89%** | 4/6 = **67%** |

## Source text

Köksbiträde till SEN Street kitchen - Värnamo SEN STREET KITCHEN är ett restaurangkoncept som återskapar de autentiska smakerna, dofterna och känslorna vid de livliga gatumarknaderna och precis som alla andra gatukök över hela östra Asien. Enkelt, snabbt, nyttigt och fräscht.  Vad kan vi erbjuda dig?  All utbildning sker på plats. Oavsett om du är erfaren som köksbiträde och vill utvecklas eller är mindre erfaren och vill lära dig från grunden, så är du välkommen att visa ditt intresse. Vi är ett familjärt team som jobbar tight ihop.  Vem är du?  Du är social och glad person som gillar att jobba med service. Du brinner för restaurangbranschen, är kvalitetsmedveten och kan jobba i ett team i högt tempo. Du är organiserad och gillar att ha rent och snyggt runt om dig.  Som köksbiträde ingår:  - Prep. - Montering av rätterna. - Städ och en del disk.   Schemat är varierande dag- och kvällstid, vardagar samt helger, därför bör du vara flexibel.  Välkommen med din ansökan så berättar jag mer för dig!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Köksbiträde** till SEN Street kitchen - Vär... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 2 | ...äde till SEN Street kitchen - **Värnamo** SEN STREET KITCHEN är ett res... | x | x | 7 | 7 | [Värnamo, **municipality**](http://data.jobtechdev.se/taxonomy/concept/6bS8_fzf_xpW) |
| 3 | ...rna och precis som alla andra **gatukök** över hela östra Asien. Enkelt... | x | x | 7 | 7 | [Gatukök, **skill**](http://data.jobtechdev.se/taxonomy/concept/dmi6_Jfs_pso) |
| 4 | ... Oavsett om du är erfaren som **köksbiträde** och vill utvecklas eller är m... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 5 | ...a med service. Du brinner för **restaurangbranschen**, är kvalitetsmedveten och kan... | x | x | 19 | 19 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 6 | ... och snyggt runt om dig.  Som **köksbiträde** ingår:  - Prep. - Montering a... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 7 | ...p. - Montering av rätterna. - **Städ** och en del disk.   Schemat är... | x |  |  | 4 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 8 | ...v rätterna. - Städ och en del **disk**.   Schemat är varierande dag-... | x |  |  | 4 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| | **Overall** | | | **66** | **74** | 66/74 = **89%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Värnamo, **municipality**](http://data.jobtechdev.se/taxonomy/concept/6bS8_fzf_xpW) |
| x | x | x | [Gatukök, **skill**](http://data.jobtechdev.se/taxonomy/concept/dmi6_Jfs_pso) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| x |  |  | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| | | **4** | 4/6 = **67%** |