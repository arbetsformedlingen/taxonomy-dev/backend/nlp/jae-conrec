# Results for '7cd013ce7856742a92b587d94645d2db69d4a321'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [7cd013ce7856742a92b587d94645d2db69d4a321](README.md) | 1 | 4379 | 27 | 25 | 127/403 = **32%** | 10/20 = **50%** |

## Source text

Sjuksköterska Flygkoordineringscentralen, Ambulanssjukvården Västerbotten Region Västerbotten arbetar för att god hälsa och hållbar utveckling ska stärka varandra. Vi tar ansvar för en jämlik välfärd och för att forskning och innovation ger resultat.  Vill du arbeta i en arbetsgrupp med stöttande och engagerade chefer och kollegor plus få möjligheten att vara delaktig i och utveckla en ny verksamhet? Då kan rollen som sjuksköterska vid vår medicinska beställningsmottagning vara något för dig.   Kommunalförbundet Svenskt Ambulansflyg (KSA) har ansvaret för att samordna och utföra ambulansflygtransporter från tre beredskapsbaser i Sverige, dygnets alla timmar, året runt. KSA:s flygkoordineringscentral (FKC) finns placerad i Umeå. Det är en ny verksamhet som varit i drift i nya moderna lokaler vid Norrlands Universitetssjukhus sedan november 2021. FKC ansvarar för den medicinska prioriteringen, färdplaneringen och koordineringen av ambulansflyguppdrag för KSA. Detta arbete på FKC utförs gemensamt av sjuksköterskor och flygkoordinatorer.  Den Medicinska beställningsmottagningen vid FKC bemannas med sjuksköterskor dygnets alla timmar och är organiserad som en egen avdelning i Region Västerbotten tillhörande Ambulanssjukvården Västerbotten.   Vad kan vi då erbjuda dig?   Vid anställning erbjuds du en lång inskolning där du kommer erhålla erfarenhet och kunskap inom såväl flygmedicin som aktuella system samt auskultering inom både ambulanssjukvård och ambulansflygverksamheten. Du kommer erbjudas kompetensutveckling inom samtalsmetodik samt ha möjligheten att önska ditt egna schema. Vidare avsätts en vecka per år till individuell utveckling inom yrkesområdet.  Låter detta intressant? Tveka inte att söka denna viktiga och utvecklande tjänst!  Urval och intervjuer sker löpande så välkommen med din ansökan så snart du kan.  ARBETSUPPGIFTER Som sjuksköterska hos oss bidrar du med kunskap och engagemang som får människor att känna sig trygga.  Arbetsuppgifterna vid den medicinska beställningsmottagningen är varierande och innefattar bland annat beställningsmottagning av flyguppdrag, medicinsk prioritering och koordinering av ambulansflyguppdrag, Arbetet består till stor del av samverkan både skriftligt och muntligt, där du dagligen kommunicerar med bland annat personal vid SOS alarm, vårdavdelningar samt personal på ambulansflyg och ambulans. Vidare ingår dokumentation av medicinsk information och uppdragshantering samt utveckling av tjänsten med fokus på patientsäkerhet, logistik och goda patienttransporter.  Inledningsvis tjänstgör du vid vår medicinska beställningsmottagning på heltid. Efter genomförd introduktion tjänstgör du på heltid, eller 50% vid den medicinska beställningsmottagningen och 50% vid en klinisk verksamhet enligt dygnet runt schema, årets alla dagar, som planeras mellan dig och ansvariga chefer.   KVALIFIKATIONER Vi söker dig som är legitimerad sjuksköterska med några års yrkeserfarenhet och som vill utvecklas i din yrkesroll.   Du är en person som trivs med logistik, planering, att ha många bollar i luften och att vara spindeln i nätet.  Rollen innebär samverkan med många olika aktörer vilket kräver att du har god samarbetsförmåga. Du är en vänlig och lyhörd person som har lätt för att prata med människor.  Du ska kunna tala och skriva på såväl svenska som engelska. Vidare ska du ha god datorvana och särskilt goda kunskaper i Office-paketet (Word och Excel).   Tjänsten är säkerhetsklassad varför du ska kunna visa registerutdrag ur belastningsregister utan anmärkning. Det är meriterande om du behärskar andra språk än svenska och engelska.  ÖVRIGT Läs mer om våra förmåner för dig som jobbar med oss på webbsidan https://www.regionvasterbotten.se/nar-du-jobbar-hos-oss/att-vara-medarbetare  Vi strävar efter en jämn könsfördelning och ser mångfald som en styrka och välkomnar därför sökande med olika bakgrund.  Inför rekryteringsarbetet har vi tagit ställning till rekryteringskanaler och marknadsföring. Vi undanbeder oss därför bestämt kontakt med mediesäljare, rekryteringssajter och liknande.  Vi använder e-rekrytering för att effektivisera och kvalitetssäkra rekryteringsarbetet. Alla dina uppgifter sparas på ditt CV-konto och du kan enkelt logga in och uppdatera dina uppgifter när du vill. Fyll därför i dina uppgifter så omsorgsfullt som möjligt för att ge en rättvisande bild av dig själv.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sjuksköterska** Flygkoordineringscentralen, A... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 1 | **Sjuksköterska** Flygkoordineringscentralen, A... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 2 | ...a Flygkoordineringscentralen, **Ambulanssjukvården** Västerbotten Region Västerbot... | x |  |  | 18 | [Ambulanssjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/eiAk_5vQ_2NY) |
| 3 | ...centralen, Ambulanssjukvården **Västerbotten** Region Västerbotten arbetar f... | x | x | 12 | 12 | [Västerbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/g5Tt_CAV_zBd) |
| 4 | ...bulanssjukvården Västerbotten **Region **Västerbotten arbetar för att g... | x |  |  | 7 | [Västerbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/g5Tt_CAV_zBd) |
| 5 | ...jukvården Västerbotten Region **Västerbotten** arbetar för att god hälsa och... | x | x | 12 | 12 | [Västerbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/g5Tt_CAV_zBd) |
| 6 | ...terbotten arbetar för att god **hälsa** och hållbar utveckling ska st... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 7 | ...arbetar för att god hälsa och **hållbar utveckling** ska stärka varandra. Vi tar a... | x |  |  | 18 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 8 | ...verksamhet? Då kan rollen som **sjuksköterska** vid vår medicinska beställnin... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 8 | ...verksamhet? Då kan rollen som **sjuksköterska** vid vår medicinska beställnin... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 9 | ...er från tre beredskapsbaser i **Sverige**, dygnets alla timmar, året ru... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 10 | ...entral (FKC) finns placerad i **Umeå**. Det är en ny verksamhet som ... |  | x |  | 4 | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| 11 | ...te på FKC utförs gemensamt av **sjuksköterskor** och flygkoordinatorer.  Den M... |  | x |  | 14 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 11 | ...te på FKC utförs gemensamt av **sjuksköterskor** och flygkoordinatorer.  Den M... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 12 | ...agningen vid FKC bemannas med **sjuksköterskor** dygnets alla timmar och är or... |  | x |  | 14 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 12 | ...agningen vid FKC bemannas med **sjuksköterskor** dygnets alla timmar och är or... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 13 | ...om en egen avdelning i Region **Västerbotten** tillhörande Ambulanssjukvårde... |  | x |  | 12 | [Västerbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/g5Tt_CAV_zBd) |
| 14 | ...m samt auskultering inom både **ambulanssjukvård** och ambulansflygverksamheten.... | x | x | 16 | 16 | [Ambulanssjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/eiAk_5vQ_2NY) |
| 15 | ...udas kompetensutveckling inom **samtalsmetodik** samt ha möjligheten att önska... | x | x | 14 | 14 | [Samtalsmetodik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Ggsm_7px_yvJ) |
| 16 | ...u kan.  ARBETSUPPGIFTER Som **sjuksköterska** hos oss bidrar du med kunskap... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 16 | ...u kan.  ARBETSUPPGIFTER Som **sjuksköterska** hos oss bidrar du med kunskap... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 17 | ... kommunicerar med bland annat **personal** vid SOS alarm, vårdavdelninga... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 18 | ...S alarm, vårdavdelningar samt **personal** på ambulansflyg och ambulans.... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 19 | ... personal på ambulansflyg och **ambulans**. Vidare ingår dokumentation a... | x |  |  | 8 | [Ambulans, **keyword**](http://data.jobtechdev.se/taxonomy/concept/VYLr_fEP_8Yh) |
| 20 | ...yg och ambulans. Vidare ingår **dokumentation** av medicinsk information och ... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 21 | ...med fokus på patientsäkerhet, **logistik** och goda patienttransporter. ... | x | x | 8 | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 22 | ...ska beställningsmottagning på **heltid**. Efter genomförd introduktion... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 23 | ... introduktion tjänstgör du på **heltid**, eller 50% vid den medicinska... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 24 | ... söker dig som är legitimerad **sjuksköterska** med några års yrkeserfarenhet... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 24 | ... söker dig som är legitimerad **sjuksköterska** med några års yrkeserfarenhet... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 25 | ...Du är en person som trivs med **logistik**, planering, att ha många boll... | x | x | 8 | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 26 | ...unna tala och skriva på såväl **svenska** som engelska. Vidare ska du h... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 27 | ...h skriva på såväl svenska som **engelska**. Vidare ska du ha god datorva... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 28 | ...och särskilt goda kunskaper i **Office-paketet** (Word och Excel).   Tjänsten ... |  | x |  | 14 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 29 | ...a kunskaper i Office-paketet (**Word** och Excel).   Tjänsten är säk... | x | x | 4 | 4 | [MS Word, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Ei3_fZV_YKM) |
| 30 | ...er i Office-paketet (Word och **Excel**).   Tjänsten är säkerhetsklas... | x | x | 5 | 5 | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| 31 | ...m du behärskar andra språk än **svenska** och engelska.  ÖVRIGT Läs me... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 32 | ...ar andra språk än svenska och **engelska**.  ÖVRIGT Läs mer om våra för... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| | **Overall** | | | **127** | **403** | 127/403 = **32%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| x | x | x | [MS Word, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Ei3_fZV_YKM) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x | x | x | [Samtalsmetodik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Ggsm_7px_yvJ) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| x |  |  | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| x |  |  | [Ambulans, **keyword**](http://data.jobtechdev.se/taxonomy/concept/VYLr_fEP_8Yh) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| x | x | x | [Ambulanssjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/eiAk_5vQ_2NY) |
| x | x | x | [Västerbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/g5Tt_CAV_zBd) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x |  |  | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| x |  |  | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
|  | x |  | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| x | x | x | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **10** | 10/20 = **50%** |