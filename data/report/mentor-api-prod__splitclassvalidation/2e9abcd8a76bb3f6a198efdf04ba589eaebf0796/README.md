# Results for '2e9abcd8a76bb3f6a198efdf04ba589eaebf0796'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2e9abcd8a76bb3f6a198efdf04ba589eaebf0796](README.md) | 1 | 470 | 10 | 6 | 37/143 = **26%** | 4/11 = **36%** |

## Source text

Erfaren smed sökes till expanderande smidesföretag i Stockholm SSKM AB söker ny kollega vars huvudsakliga arbetsuppgift kommer bestå av tillverkning och montage av diverse smidesprodukter. Du kommer få tillfälle att arbeta både självständigt i mindre projekt och tillsammans med kollegor vid större och mer komplexa arbeten. Minimum 10 års erfarenhet av byggnadssmide, underhållssmide och montering av stålkonstruktioner.  Meriterande är om du pratar Spanska.  B-körkort

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Erfaren **smed** sökes till expanderande smide... |  | x |  | 4 | [Smed, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FM1r_vQd_zS8) |
| 1 | Erfaren **smed** sökes till expanderande smide... | x |  |  | 4 | [Smeder, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/q8Ho_3Xw_sHq) |
| 2 | ... expanderande smidesföretag i **Stockholm** SSKM AB söker ny kollega vars... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 3 | ...arbetsuppgift kommer bestå av **tillverkning** och montage av diverse smides... | x | x | 12 | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 4 | ...r. Du kommer få tillfälle att **arbeta** både självständigt i mindre p... | x |  |  | 6 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 5 | ... få tillfälle att arbeta både **självständigt** i mindre projekt och tillsamm... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 6 | ...mer komplexa arbeten. Minimum **10 års erfarenhet** av byggnadssmide, underhållss... | x |  |  | 17 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 7 | ... Minimum 10 års erfarenhet av **byggnadssmide**, underhållssmide och monterin... | x |  |  | 13 | [Byggnadssmide, **skill**](http://data.jobtechdev.se/taxonomy/concept/UHKN_Nqw_6Z5) |
| 8 | ...adssmide, underhållssmide och **montering av stålkonstruktioner**.  Meriterande är om du pratar... | x |  |  | 31 | [Stålkonstruktionsmontage, **skill**](http://data.jobtechdev.se/taxonomy/concept/QyH1_VMW_rzh) |
| 9 | ...erhållssmide och montering av **stålkonstruktioner**.  Meriterande är om du pratar... |  | x |  | 18 | [Stålkonstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/11rR_H3J_EQQ) |
| 10 | ...  Meriterande är om du pratar **Spanska**.  B-körkort | x | x | 7 | 7 | [Spanska, **language**](http://data.jobtechdev.se/taxonomy/concept/i9Hd_bVm_Z3c) |
| 11 | ...nde är om du pratar Spanska.  **B-körkort** | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| | **Overall** | | | **37** | **143** | 37/143 = **26%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Stålkonstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/11rR_H3J_EQQ) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [Smed, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FM1r_vQd_zS8) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Stålkonstruktionsmontage, **skill**](http://data.jobtechdev.se/taxonomy/concept/QyH1_VMW_rzh) |
| x |  |  | [Byggnadssmide, **skill**](http://data.jobtechdev.se/taxonomy/concept/UHKN_Nqw_6Z5) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Spanska, **language**](http://data.jobtechdev.se/taxonomy/concept/i9Hd_bVm_Z3c) |
| x |  |  | [Smeder, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/q8Ho_3Xw_sHq) |
| x | x | x | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| | | **4** | 4/11 = **36%** |