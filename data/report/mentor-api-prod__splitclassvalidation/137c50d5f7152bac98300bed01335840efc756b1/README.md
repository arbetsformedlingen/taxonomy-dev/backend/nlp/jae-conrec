# Results for '137c50d5f7152bac98300bed01335840efc756b1'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [137c50d5f7152bac98300bed01335840efc756b1](README.md) | 1 | 5110 | 30 | 29 | 151/417 = **36%** | 7/20 = **35%** |

## Source text

Bibliotekarie till Uppsökande verksamheten, Bibliotek Uppsala  Uppsökande verksamheten, Bibliotek Uppsala och Norra enheten  Drivs du av biblioteksarbete med barn och unga som målgrupp? Vill du vara med och bidra till läsfrämjande för barn via Bibliotek Uppsalas biblioteksbussar? Då kan du vara den vi söker!   Bibliotek Uppsala har en väl utbyggd uppsökande verksamhet som når invånare i alla åldrar i hela Uppsala kommun. Verksamheten består av 2 biblioteksbussar som trafikerar stad och landsbygd, Boken kommer-service samt talboksservice till enskilda personer och vård- och omsorgsboenden. Verksamheten arbetar även med kulturaktiviteter för äldreomsorg och vuxna inom LSS/socialpsykiatri. Det är en relationsskapande verksamhet med 12 positiva och engagerande medarbetare, välutvecklade samarbeten och kreativa idéer och goda möjligheter till att bedriva utvecklingsarbete.   Nu förstärker vi verksamhetens arbete mot barn och unga, och söker därför en bibliotekarie med intresse och erfarenhet av att arbeta utåtriktat och läsfrämjande mot barn och unga. I vårt arbete inom Uppsala kommun gör vi skillnad och arbetar tillsammans med medborgarna i fokus.   Här kan du läsa mer om Biblioteksbussen (https://bibliotekuppsala.se/en/web/arena/biblioteksbussen) Här kan du läsa mer om Bibliotek Uppsala (https://bibliotekuppsala.se/)  Ditt uppdrag I tjänsten som bibliotekarie har du som uppgift att utveckla nya arbetssätt för att nå barn via biblioteksbussarna med särskild inriktning mot läsfrämjande för barn 6-12 år. Du inspirerar till läsning genom litteratur och aktiviteter och är med och håller i dessa. I ditt uppdrag samarbetar du nära kollegor samt samverkar med andra aktörer. Du omvärldsbevakar och fångar upp idéer som kan bidra till verksamhetsutveckling. Att du har erfarenhet av och tycker att det är roligt att arbeta med och möta barn och unga är en förutsättning för den här tjänsten. Du får också möjlighet att ingå i den nuvarande utvecklingsgruppen för barnverksamhet inom Bibliotek Uppsala.  Du deltar i den ordinarie bemanningen på biblioteksbussarna och möter barn i alla åldrar samt vuxna. Du är också delaktig i och bidrar till biblioteksbussverksamhetens kommunikation och närvaro i sociala kanaler. Du deltar även i gemensamma arbetsuppgifter i nära samarbete med övriga medarbetare. I tjänsten ingår informationstjänstgöring samt kvälls- och helgtjänstgöring enligt schema.  Din bakgrund Vi söker dig som har examen i biblioteks- och informationsvetenskap och erfarenhet av biblioteksarbete med barn och unga. Vi tycker att det är positivt om du har erfarenhet av uppsökande biblioteksverksamhet och tillgänglighetsfrågor. Vi ser gärna att du har vana att skapa och upprätthålla goda samarbeten såväl inom som utanför biblioteksverksamheten.   Tjänsten förutsätter att du har goda kunskaper i svenska och engelska, både i tal och skrift. Vidare krävs god IT-vana och gärna kunskaper i Book-IT. Har du kunskaper i fler språk än svenska och engelska är det en fördel.   Som person är du lyhörd, har lätt för att samarbeta och kan anpassa dig utifrån ändrade förutsättningar. Du ser positivt på förändring och trivs med att testa nya arbetssätt. Du deltar aktivt i verksamhetsutveckling samt bidrar med idéer och nya angreppssätt i verksamhetsfrågor. Du är en strukturerad, engagerad och driven person som sätter igång aktiviteter för att uppnå önskade resultat med fokus på verksamhetens bästa. Du är utåtriktad och kommunikativ, har lätt för att lyssna och anpassar ditt sätt att kommunicera till olika situationer.  Upplysningar Har du frågor om tjänsten eller rekryteringsprocessen, kontakta: Maria Kungsman, enhetschef Norra enheten, 018-727 25 01 Wilda Johansson, verksamhetsledare uppsökande verksamheten, 018-727 17 73.  Facklig företrädare: DIK, Marianne Skoglund 018-727 57 09  Har du frågor gällande registrering av din ansökan, kontakta enheten för kompetensförsörjning: 018-727 26 59.  I din ansökan ska du ange medborgarskap. Om du inte är medborgare i Sverige, EU, EES eller Schweiz måste du också kunna styrka att du har ett giltigt arbetstillstånd eller är undantagen från skyldigheten att ha arbetstillstånd. Anställning kan ske först efter kontroll av arbetstillstånd, vilket krävs enligt utlänningsförordningen.  Om vår organisation Välkommen till kulturförvaltningen. Förvaltningen ansvarar för den strategiska planeringen inom kulturnämndens ansvarsområde och merparten av kommunens kulturverksamhet samt verksamhet för barn och unga. Förvaltningen handlägger också stöd och stipendier till det fria kulturlivet, studieförbund och föreningslivet i Uppsala kommun. Förvaltningen består av fem avdelningar: Kulturförvaltningens stab, Bibliotek Uppsala, Fritid Uppsala, Uppsala konst och kulturarv samt Uppsala kulturskola.  Mer information om vår organisation hittar du på uppsala.se (https://www.uppsala.se/kommun-och-politik/organisation/forvaltningar/kulturforvaltningen/?selectedyear=)  Vi undanber oss erbjudanden om annonserings- och rekryteringshjälp.  Dina personuppgifter kommer att användas för att administrera din ansökan inom Uppsala kommun.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bibliotekarie** till Uppsökande verksamheten,... | x | x | 13 | 13 | [Bibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GujS_MtW_PRH) |
| 2 | ...kande verksamheten, Bibliotek **Uppsala**  Uppsökande verksamheten, Bib... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 3 | ...kande verksamheten, Bibliotek **Uppsala** och Norra enheten  Drivs du... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 4 | ...ara den vi söker!   Bibliotek **Uppsala** har en väl utbyggd uppsökande... |  | x |  | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 5 | ...invånare i alla åldrar i hela **Uppsala** kommun. Verksamheten består a... | x |  |  | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 6 | ...ven med kulturaktiviteter för **äldreomsorg** och vuxna inom LSS/socialpsyk... |  | x |  | 11 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 7 | ... möjligheter till att bedriva **utvecklingsarbete**.   Nu förstärker vi verksamhe... | x | x | 17 | 17 | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| 8 | ...och unga, och söker därför en **bibliotekarie** med intresse och erfarenhet a... |  | x |  | 13 | [Bibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GujS_MtW_PRH) |
| 8 | ...och unga, och söker därför en **bibliotekarie** med intresse och erfarenhet a... | x |  |  | 13 | [bibliotekarie, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/UzDm_fz2_Bnp) |
| 9 | ... och unga. I vårt arbete inom **Uppsala** kommun gör vi skillnad och ar... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 10 | ... kan du läsa mer om Bibliotek **Uppsala** (https://bibliotekuppsala.se/... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 11 | ...  Ditt uppdrag I tjänsten som **bibliotekarie** har du som uppgift att utveck... | x | x | 13 | 13 | [Bibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GujS_MtW_PRH) |
| 12 | ...inspirerar till läsning genom **litteratur** och aktiviteter och är med oc... | x |  |  | 10 | [Litteratur, **skill**](http://data.jobtechdev.se/taxonomy/concept/nGtv_qkF_AxF) |
| 13 | ...åller i dessa. I ditt uppdrag **samarbetar** du nära kollegor samt samverk... | x |  |  | 10 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 14 | ...oligt att arbeta med och möta **barn och unga** är en förutsättning för den h... | x |  |  | 13 | [Barn och ungdom, **skill**](http://data.jobtechdev.se/taxonomy/concept/qkyH_MC1_YKU) |
| 15 | ...arande utvecklingsgruppen för **barnverksamhet** inom Bibliotek Uppsala.  Du d... |  | x |  | 14 | [Ungdomsverksamhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/kb9z_LG1_CZE) |
| 16 | ...barnverksamhet inom Bibliotek **Uppsala**.  Du deltar i den ordinarie b... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 17 | ...Vi söker dig som har examen i **biblioteks- och informationsvetenskap** och erfarenhet av biblioteksa... | x | x | 37 | 37 | [Bibliotekarieutbildning samt biblioteks- och informationsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/FsKa_aFC_yPT) |
| 18 | ...enhet av biblioteksarbete med **barn och unga**. Vi tycker att det är positiv... | x |  |  | 13 | [Barn och ungdom, **skill**](http://data.jobtechdev.se/taxonomy/concept/qkyH_MC1_YKU) |
| 19 | ... har erfarenhet av uppsökande **biblioteksverksamhet** och tillgänglighetsfrågor. Vi... |  | x |  | 20 | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
| 20 | ...att du har vana att skapa och **upprätthålla goda samarbeten** såväl inom som utanför biblio... | x |  |  | 28 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 21 | ...r att du har goda kunskaper i **svenska** och engelska, både i tal och ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 22 | ... goda kunskaper i svenska och **engelska**, både i tal och skrift. Vidar... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 23 | ... och skrift. Vidare krävs god **IT-vana** och gärna kunskaper i Book-IT... | x | x | 7 | 7 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 24 | ...i Book-IT. Har du kunskaper i **fler språk** än svenska och engelska är de... | x |  |  | 10 | [Diverse språk, **language**](http://data.jobtechdev.se/taxonomy/concept/8GQ1_Avf_vdR) |
| 25 | ... du kunskaper i fler språk än **svenska** och engelska är det en fördel... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 26 | ...r i fler språk än svenska och **engelska** är det en fördel.   Som perso... |  | x |  | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 26 | ...r i fler språk än svenska och **engelska** är det en fördel.   Som perso... | x |  |  | 8 | [Engelska, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xiD7_snJ_D8x) |
| 27 | ...r du lyhörd, har lätt för att **samarbeta** och kan anpassa dig utifrån ä... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 28 | ...s bästa. Du är utåtriktad och **kommunikativ**, har lätt för att lyssna och ... | x |  |  | 12 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 29 | ..., har lätt för att lyssna och **anpassar ditt sätt att kommunicera** till olika situationer.  Uppl... | x |  |  | 34 | [anpassa kommunikationsstilen efter mottagaren, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BUjh_kV6_mhd) |
| 30 | ...p. Om du inte är medborgare i **Sverige**, EU, EES eller Schweiz måste ... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 31 | ...eförbund och föreningslivet i **Uppsala** kommun. Förvaltningen består ... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 32 | ...örvaltningens stab, Bibliotek **Uppsala**, Fritid Uppsala, Uppsala kons... |  | x |  | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 33 | ...sala, Fritid Uppsala, Uppsala **konst** och kulturarv samt Uppsala ku... |  | x |  | 5 | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| 34 | ...administrera din ansökan inom **Uppsala** kommun. |  | x |  | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| | **Overall** | | | **151** | **417** | 151/417 = **36%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Diverse språk, **language**](http://data.jobtechdev.se/taxonomy/concept/8GQ1_Avf_vdR) |
| x |  |  | [anpassa kommunikationsstilen efter mottagaren, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BUjh_kV6_mhd) |
| x | x | x | [Bibliotekarieutbildning samt biblioteks- och informationsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/FsKa_aFC_yPT) |
| x | x | x | [Bibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GujS_MtW_PRH) |
| x | x | x | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x | x | x | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [bibliotekarie, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/UzDm_fz2_Bnp) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
|  | x |  | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
|  | x |  | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Ungdomsverksamhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/kb9z_LG1_CZE) |
| x |  |  | [Litteratur, **skill**](http://data.jobtechdev.se/taxonomy/concept/nGtv_qkF_AxF) |
|  | x |  | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| x |  |  | [Barn och ungdom, **skill**](http://data.jobtechdev.se/taxonomy/concept/qkyH_MC1_YKU) |
| x |  |  | [Engelska, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xiD7_snJ_D8x) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/20 = **35%** |