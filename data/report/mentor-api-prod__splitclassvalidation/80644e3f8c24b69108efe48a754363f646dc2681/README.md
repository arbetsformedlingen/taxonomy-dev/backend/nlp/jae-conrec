# Results for '80644e3f8c24b69108efe48a754363f646dc2681'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [80644e3f8c24b69108efe48a754363f646dc2681](README.md) | 1 | 1291 | 6 | 9 | 35/87 = **40%** | 3/7 = **43%** |

## Source text

Som svetsare är du anställd inom verkstadsindustrin, vid bygg- och anläggningsföretag, vid entreprenadföretag samt som underhålls- och reparationspersonal vid olika industrier. Du kan arbeta exempelvis vid broanläggningar, husbyggen, vid installation av rörsystem inom processindustrin eller vid smidesarbete. Arbetet som svetsare kan innebära allt från rutinbetonad skötsel av en automatisk svetsmaskin till krävande specialsvetsning av rörledningar och tryckkärl. Det är en stor efterfrågan på svetsare då inte tillräckligt många har utbildats. Fortsatt efterfrågan på både kort och lång sikt.  Utbildningen är en påbyggnadsutbildning inom svetsmetoden TIG(gasvolframsvetsning) och är inriktad på käl- plåt-och rörsvetsning. Målet med utbildningen är IW diplom för uppnådd på minst en nivå. Utbildningen består av både praktiska och teoretiska moment. De teoretiska momenten innehåller exempelvis materiallära och under de praktiska momenten får du fördjupa dig i svetsmetoden TIG. Utbildningen sker i nära samarbete med branschen och en del av utbildningen gör du på ett företag för att få kännedom om hur det fungerar i branschen. Hela utbildningen är c:a 14 veckor lång. Men utifrån dina förutsättningar och företagens behov kan utbildningen bli kortare eller längre.      

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Som **svetsare** är du anställd inom verkstads... | x | x | 8 | 8 | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| 2 | ...om processindustrin eller vid **smidesarbete**. Arbetet som svetsare kan in... |  | x |  | 12 | [Byggnadssmide, **skill**](http://data.jobtechdev.se/taxonomy/concept/UHKN_Nqw_6Z5) |
| 3 | ...id smidesarbete. Arbetet som **svetsare** kan innebära allt från rutinb... | x | x | 8 | 8 | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| 4 | ...onad skötsel av en automatisk **svetsmaskin** till krävande specialsvetsnin... |  | x |  | 11 | [Automatsvetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/vt7F_XTv_7ED) |
| 5 | ...isk svetsmaskin till krävande **specialsvetsning** av rörledningar och tryckkärl... |  | x |  | 16 | [Tunnvägg (rörsvets, gods max 2 mm.), **skill**](http://data.jobtechdev.se/taxonomy/concept/bdHi_zUS_xqV) |
| 6 | ... krävande specialsvetsning av **rörledningar** och tryckkärl. Det är en sto... |  | x |  | 12 | [Tunnvägg (rörsvets, gods max 2 mm.), **skill**](http://data.jobtechdev.se/taxonomy/concept/bdHi_zUS_xqV) |
| 7 | ...svetsning av rörledningar och **tryckkärl**. Det är en stor efterfrågan ... | x | x | 9 | 9 | [Tryckkärl, **skill**](http://data.jobtechdev.se/taxonomy/concept/fBgY_UWc_49v) |
| 8 | ...Det är en stor efterfrågan på **svetsare** då inte tillräckligt många ha... | x | x | 8 | 8 | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| 9 | ...ng. Målet med utbildningen är **IW** diplom för uppnådd på minst e... | x | x | 2 | 2 | [IW (International Welder), certifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/aNcD_UfW_vYN) |
| 10 | ...nschen. Hela utbildningen är **c**:a 14 veckor lång. Men utifrån... | x |  |  | 1 | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| | **Overall** | | | **35** | **87** | 35/87 = **40%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Byggnadssmide, **skill**](http://data.jobtechdev.se/taxonomy/concept/UHKN_Nqw_6Z5) |
| x | x | x | [IW (International Welder), certifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/aNcD_UfW_vYN) |
|  | x |  | [Tunnvägg (rörsvets, gods max 2 mm.), **skill**](http://data.jobtechdev.se/taxonomy/concept/bdHi_zUS_xqV) |
| x | x | x | [Tryckkärl, **skill**](http://data.jobtechdev.se/taxonomy/concept/fBgY_UWc_49v) |
| x |  |  | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| x | x | x | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
|  | x |  | [Automatsvetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/vt7F_XTv_7ED) |
| | | **3** | 3/7 = **43%** |