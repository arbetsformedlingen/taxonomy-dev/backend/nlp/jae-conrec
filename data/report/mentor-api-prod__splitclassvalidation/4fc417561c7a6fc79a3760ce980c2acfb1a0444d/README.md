# Results for '4fc417561c7a6fc79a3760ce980c2acfb1a0444d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4fc417561c7a6fc79a3760ce980c2acfb1a0444d](README.md) | 1 | 886 | 6 | 10 | 35/71 = **49%** | 3/6 = **50%** |

## Source text

Medarbetare till Spisa pizza Hammarbu sjöstad, solna/ultiksdal ,sollentuna. Om jobbet Vi söker nya medarbetare som brinner för mat och service hos oss på Spisa Pizza! Ett nytt och unikt koncept inom pizza världen. Vi finns i ulriksdal, sollentuna c, och snart i Hammarby sjösrad.   Tjänsten innefattar: Tillagning av vår unika pizza som är vår kärna. Och även sallad. Förberedelser av deg och råvaror. Ta emot kunder och ge din bästa service med ett stort leende. Se till att allt är rent och snyggt inne i lokalen. Detta är huvuduppgifterna inom tjänsten.    Vi letar efter dig som vill vara kreativ i ditt arbete och inte är rädd att gå utanför ramarna. Du skall vara serviceinriktad, ha intresse för mat, effektiv, flexibel, stresstålig, bra på att jobba i team men även lika bra ensam. Det är bra om du behärskar det svenska och engelska språket då det är mycket kontakt med kunder.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Medarbetare till Spisa **pizza** Hammarbu sjöstad, solna/ultik... | x | x | 5 | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 2 | ...Spisa pizza Hammarbu sjöstad, **solna**/ultiksdal ,sollentuna. Om job... |  | x |  | 5 | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| 3 | ...rbu sjöstad, solna/ultiksdal ,**sollentuna**. Om jobbet Vi söker nya medar... |  | x |  | 10 | [Sollentuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Z5Cq_SgB_dsB) |
| 4 | ... och service hos oss på Spisa **Pizza**! Ett nytt och unikt koncept i... | x | x | 5 | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 5 | ...t nytt och unikt koncept inom **pizza** världen. Vi finns i ulriksdal... | x | x | 5 | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 6 | ...ärlden. Vi finns i ulriksdal, **sollentuna** c, och snart i Hammarby sjösr... |  | x |  | 10 | [Sollentuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Z5Cq_SgB_dsB) |
| 7 | ...ttar: Tillagning av vår unika **pizza** som är vår kärna. Och även sa... | x | x | 5 | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 8 | ... för mat, effektiv, flexibel, **stresstålig**, bra på att jobba i team men ... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 9 | ...et är bra om du behärskar det **svenska** och engelska språket då det ä... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 10 | ... du behärskar det svenska och **engelska** språket då det är mycket kont... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| | **Overall** | | | **35** | **71** | 35/71 = **49%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Sollentuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Z5Cq_SgB_dsB) |
| x | x | x | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
|  | x |  | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/6 = **50%** |