# Results for '9817b17766a08ad5155ef0b1ac6663ab8a5cfcfd'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9817b17766a08ad5155ef0b1ac6663ab8a5cfcfd](README.md) | 1 | 3420 | 16 | 14 | 0/194 = **0%** | 3/11 = **27%** |

## Source text

Kjell & Company Västerås söker säljare med servicekänsla och teknikintre... Hej!  Ur kunders ögon innehar vi förstaplatsen i branschen när det kommer till mest kunnig och mest flexibel personal. Vi är dessutom den affärskedja som bedöms ge snabbast service enligt kunders tycke - detta tror vi helt och hållet beror på våra säljares förmåga att lösa kundens problem på bästa vis! ✨  Vad erbjuder vi dig?  Inom retail och inte minst på Kjell & Company går utvecklingen i rask takt och vi utvecklar ständigt nya vägar för att möta våra kunders nya behov, alltid med Service i världsklass som given målsättning. Just nu är vi i full transformation från butiker till Service Points. I en Service Point möter du som säljare främst våra kunder i fysisk form, men kundmötena sker även digitalt genom chatt och 1-to-1 meeting eller livestreaming - alltid med kundens behov i fokus. Vi är stolta och säkra att du erbjuds en kultur där du kan vara dig själv, känna dig trygg och börja bygga många underbara minnen.  Detta innebär med andra ord att du i säljarrollen på Kjell & Company kommer att vara med om en fantastisk resa där du ges möjlighet att bredda din kompetens och utvecklas inom flera spännande områden framåt. Oavsett kan vi lova dig att du har en oerhört spännande resa framför dig!  Vill du veta mer om hur är det att jobba som säljare hos oss? Låt vår grymma säljare Emil berätta mer! 👇  Vad söker vi?  Vi söker dig som vill arbeta 50% i vår butik i Västerås samt Erikslund. En tjänst i denna storlek kan också vara ett steg mot en större tjänst och kanske även en karriär vidare i företaget.  Vad är viktigt för att passa och trivas i rollen?   Teknikintresserad! Du är nyfiken och intresserad av att lära dig mer inom teknikområdet. Mer än så behöver du inte - med fantastiska kollegor vid din sida och verktyg i form utav både introduktionsdagar och utbildningsportal med onlinekurser lär du dig mer och mer för var dag som går.   Relationsbyggande! Du är genuint intresserad av försäljning, service och förstår kraften av att bygga långsiktiga relationer. Du ser det som en spännande utmaning att överträffa varje kunds förväntningar och känner stolthet i både ditt personliga varumärke så väl som företagets.   Målinriktad! För oss är inte tidigare erfarenhet nödvändig - vi lägger stor vikt vid din utveckling och att du växer i rollen som säljare med dina personliga egenskaper som grund. I linje med vår strategi ”grow with Kjell”, med det menar vi oavsett om din ambition är att arbeta hos oss vid sidan om dina studier eller om du på sikt vill växa internt erbjuder vi på Kjell den möjligheten. Idag internrekryteras 70% utav alla våra ledare, vi erbjuder ett ledarskapsprogram som vi kallar Butikschef - trainee för dig som är på sugen på att utvecklas vidare och ta steget till en Butikschefs-roll. På Kjell vill vi att du ska kunna växa och ta personliga kliv framåt, både i den roll du har och i nya roller.  Allmän information   Arbetet är förlagt till både vardagar och helger.   Rekrytering sker löpande så tveka inte att skicka in din ansökan redan idag!   Tjänsten kommer att tillsättas omgående.  Utöver Handels kollektivavtal erbjuder vi även bonussystem, friskvårdsbidrag, attraktiv personalrabatt och vassa internutbildningar (exempelvis Butikschef - trainee).  Jag ser fram emot att läsa din ansökan!  Bästa hälsningar  Eleonora, Talent Acquisition & Development Partner, Kjell & Company 💙

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Kjell & Company **Västerås** söker säljare med servicekäns... |  | x |  | 8 | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| 2 | ...jell & Company Västerås söker **säljare** med servicekänsla och tekniki... | x |  |  | 7 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 3 | ...mest kunnig och mest flexibel **personal**. Vi är dessutom den affärsked... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 4 | ...helt och hållet beror på våra **säljares** förmåga att lösa kundens prob... | x |  |  | 8 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 5 | ...en Service Point möter du som **säljare** främst våra kunder i fysisk f... | x |  |  | 7 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 6 | ...nnebär med andra ord att du i **säljarrollen** på Kjell & Company kommer att... | x |  |  | 12 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 7 | ...r om hur är det att jobba som **säljare** hos oss? Låt vår grymma sälja... | x |  |  | 7 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 8 | ...ljare hos oss? Låt vår grymma **säljare** Emil berätta mer! 👇  Vad söke... | x |  |  | 7 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 9 | ...Vi söker dig som vill arbeta 5**0% **i vår butik i Västerås samt Er... | x |  |  | 3 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 10 | ...ig som vill arbeta 50% i vår b**utik **i Västerås samt Erikslund. En ... | x |  |  | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 11 | ...ill arbeta 50% i vår butik i V**ästerås **samt Erikslund. En tjänst i de... | x |  |  | 8 | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| 12 | ...?   Teknikintresserad! Du är n**yfiken **och intresserad av att lära di... | x |  |  | 7 | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
| 13 | ...om företagets.   Målinriktad! **F**ör oss är inte tidigare erfare... |  | x |  | 1 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 14 | ... Målinriktad! För oss är inte **tidigar**e erfarenhet nödvändig - vi lä... |  | x |  | 7 | [Trainee välfärd, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/dJxe_nVr_6Pq) |
| 14 | ... Målinriktad! För oss är inte **tidigar**e erfarenhet nödvändig - vi lä... |  | x |  | 7 | [Trainee brist, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/k7AF_NQa_TXm) |
| 15 | ...- vi lägger stor vikt vid din **ut**veckling och att du växer i ro... |  | x |  | 2 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 16 | ...lägger stor vikt vid din utvec**k**ling och att du växer i rollen... |  | x |  | 1 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 17 | ...gger stor vikt vid din utveckl**i**ng och att du växer i rollen s... |  | x |  | 1 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 18 | ...stor vikt vid din utveckling o**ch** att du växer i rollen som säl... |  | x |  | 2 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 19 | ...ch att du växer i rollen som s**äljare **med dina personliga egenskaper... | x |  |  | 7 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 20 | ...arskapsprogram som vi kallar B**utikschef **- trainee för dig som är på su... | x |  |  | 10 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 21 | ...vidare och ta steget till en B**utikschefs-**roll. På Kjell vill vi att du ... | x |  |  | 11 | [Butikschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WWVG_4sM_faC) |
| 22 | ... både vardagar och helger.   R**e**krytering sker löpande så tvek... |  | x |  | 1 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 23 | ...både vardagar och helger.   Re**krytering sker** löpande så tveka inte att ski... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 24 | ...e vardagar och helger.   Rekry**tering **sker löpande så tveka inte att... |  | x |  | 7 | [Trainee välfärd, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/dJxe_nVr_6Pq) |
| 24 | ...e vardagar och helger.   Rekry**tering **sker löpande så tveka inte att... |  | x |  | 7 | [Trainee brist, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/k7AF_NQa_TXm) |
| 25 | ... sker löpande så tveka inte at**t** skicka in din ansökan redan i... |  | x |  | 1 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 26 | ...ker löpande så tveka inte att **skic**ka in din ansökan redan idag! ... |  | x |  | 4 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 27 | ...as omgående.  Utöver Handels k**ollektivavtal **erbjuder vi även bonussystem, ... | x |  |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 28 | ...ternutbildningar (exempelvis B**utikschef **- trainee).  Jag ser fram emot... | x |  |  | 10 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| | **Overall** | | | **0** | **194** | 0/194 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x |  |  | [Butikschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WWVG_4sM_faC) |
| x | x | x | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
|  | x |  | [Trainee välfärd, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/dJxe_nVr_6Pq) |
|  | x |  | [Trainee brist, **unemployment-type**](http://data.jobtechdev.se/taxonomy/concept/k7AF_NQa_TXm) |
| x |  |  | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
| x |  |  | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| x |  |  | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **3** | 3/11 = **27%** |