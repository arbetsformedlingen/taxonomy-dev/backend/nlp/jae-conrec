# Results for '52434a08efa83d1e967ca399ab6dae27065c8cdf'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [52434a08efa83d1e967ca399ab6dae27065c8cdf](README.md) | 1 | 668 | 8 | 7 | 51/68 = **75%** | 5/7 = **71%** |

## Source text

Kallskänka 50% Vi är ett traditionellt konditori med eget bageri i centrala Uppsala. Du ska ha en positiv inställning, vara självgående och ta egna initiativ samt vara snabb och noggrann. Erfarenhet av liknande jobb/kök är ett plus, men stort intresse för kök och matlagning är ett krav. Du har hand om beställningar av varor, egenkontroll samt utveckling av produkter. Du bör vara tillgänglig för intervju omgående då vi önskar tillsätta tjänsten så snart som möjligt. Du jobbar förmiddagar kl 8-ca 12 i vår kallskänk med att göra smörgåsar, sallader, pajer, smörgåstårtor mm. Du är effektiv, noggrann, stark och kunnig inom kök/matlagning. Du måste vara minst 18 år.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kallskänka** 50% Vi är ett traditionellt k... | x | x | 10 | 10 | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| 2 | Kallskänka **50%** Vi är ett traditionellt kondi... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 3 | ...a 50% Vi är ett traditionellt **konditori** med eget bageri i centrala Up... | x | x | 9 | 9 | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| 4 | ...ditionellt konditori med eget **bageri** i centrala Uppsala. Du ska ha... |  | x |  | 6 | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| 5 | ...ri med eget bageri i centrala **Uppsala**. Du ska ha en positiv inställ... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 6 | ...initiativ samt vara snabb och **noggrann**. Erfarenhet av liknande jobb/... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 7 | ... i vår kallskänk med att göra **smörgåsar**, sallader, pajer, smörgåstårt... | x | x | 9 | 9 | [Smörgåsar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ib92_FER_3Dd) |
| 8 | ...skänk med att göra smörgåsar, **sallader**, pajer, smörgåstårtor mm. Du ... | x |  |  | 8 | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| 9 | ...gåstårtor mm. Du är effektiv, **noggrann**, stark och kunnig inom kök/ma... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| | **Overall** | | | **51** | **68** | 51/68 = **75%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Bageri och konditori, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4t2m_4Dj_ksJ) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| x | x | x | [Smörgåsar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ib92_FER_3Dd) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| x |  |  | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| | | **5** | 5/7 = **71%** |