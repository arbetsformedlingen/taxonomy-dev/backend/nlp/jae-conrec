# Results for '767de45ca2ca217e2b1767befc64fd4e494448f5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [767de45ca2ca217e2b1767befc64fd4e494448f5](README.md) | 1 | 371 | 4 | 3 | 22/41 = **54%** | 2/5 = **40%** |

## Source text

Restaurangbiträde Vi söker personal som ska jobba heltid vardagar och helger.   Skicka gärna Mail med cv.   På La Vieto serveras det pizza, pasta, sallad, dagens lunch. Uppgifterna blir att jobba i servering och kassa. Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Restaurangbiträde** Vi söker personal som ska job... | x | x | 17 | 17 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 2 | Restaurangbiträde Vi söker **personal** som ska jobba heltid vardagar... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 3 | ... söker personal som ska jobba **heltid** vardagar och helger.   Skicka... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 4 | ...v.   På La Vieto serveras det **pizza**, pasta, sallad, dagens lunch.... | x | x | 5 | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 5 | ...lir att jobba i servering och **kassa**. Öppen för alla Vi fokuserar ... |  | x |  | 5 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| | **Overall** | | | **22** | **41** | 22/41 = **54%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x | x | x | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| x | x | x | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| | | **2** | 2/5 = **40%** |