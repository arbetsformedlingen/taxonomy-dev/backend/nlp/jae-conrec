# Results for 'd0a6d172c4a3cc49c4b4d209440b72f7d360821e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d0a6d172c4a3cc49c4b4d209440b72f7d360821e](README.md) | 1 | 289 | 3 | 2 | 38/53 = **72%** | 1/2 = **50%** |

## Source text

Bilrekonditionerare Nu har ni en unik möjlighet att arbeta på Mekopartner som är partner till Mekonomen bilverkstad . Vi söker en Bilrekonditionerare som kan få fastanställning efter provanställning. Hör av er på mejl        norremark@mekopartner.se          eller   ring        0762881848

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bilrekonditionerare** Nu har ni en unik möjlighet a... | x | x | 19 | 19 | [Bilrekonditionerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/gP3r_Q2H_ht9) |
| 2 | ...men bilverkstad . Vi söker en **Bilrekonditionerare** som kan få fastanställning ef... | x | x | 19 | 19 | [Bilrekonditionerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/gP3r_Q2H_ht9) |
| 3 | ...ilrekonditionerare som kan få **fastanställning** efter provanställning. Hör av... | x |  |  | 15 | (not found in taxonomy) |
| | **Overall** | | | **38** | **53** | 38/53 = **72%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | (not found in taxonomy) |
| x | x | x | [Bilrekonditionerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/gP3r_Q2H_ht9) |
| | | **1** | 1/2 = **50%** |