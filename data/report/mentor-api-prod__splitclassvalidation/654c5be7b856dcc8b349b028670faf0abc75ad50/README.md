# Results for '654c5be7b856dcc8b349b028670faf0abc75ad50'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [654c5be7b856dcc8b349b028670faf0abc75ad50](README.md) | 1 | 3387 | 16 | 19 | 139/323 = **43%** | 6/20 = **30%** |

## Source text

Produktionsledare Vill du jobba i en ledarroll där du arbetar produktionsnära och samtidigt ansvarar för att stötta medarbetare i att nå sin fulla potential? Den möjligheten kan vara närmare än du tror. Då en av våra produktionsledare går vidare mot nytt uppdrag söker vi nu en ny medarbetare som vill fortsätta driva arbetet framåt på en tillverkningslina inom Walkie Montering.  Som Produktionsledare på Toyota kommer du bland annat att arbeta med:   * Att leda det dagliga arbetet  * Utveckla avdelningen i linje med Toyotas produktionssystem och verksamhetens övergripande mål  * Implementera och stötta förbättringsarbete  * Övergripande ansvar för personal, leverans, kvalitet, arbetsmiljö samt ekonomin på din avdelning  * Skapa goda förutsättningar för arbetsgruppen genom att bidra till att skapa en god samarbetskultur och en positiv arbetsmiljö för dina medarbetare  I tjänsten rapporterar du till produktionschefen och kommer att ha ett nära samarbete med övriga produktionsledare i ledningsgruppen. Din avdelning består av cirka 30 skiftgående medarbetare. Du har en lokal ledningsgrupp bestående av Team Leaders, assisterande Team Leaders och vid behov produktionstekniker. Tillsammans för ni avdelningen framåt.  Är du en person, som genom ett nära och personligt ledarskap, drivs av att utveckla människor samt trivs i en föränderlig miljö och vill utvecklas som person kan det vara just dig vi letar efter. Varmt välkommen att söka tjänsten hos oss.  Till tjänsten Produktionsledare söker vi dig som:   * Har en stark drivkraft att nå uppsatta mål  * Är kommunikativ och har en god förmåga att förmedla ditt budskap  * Kan engagera och motivera andra  * Gillar ett högt tempo och förändring  * Du har erfarenhet av att arbeta inom industrin  * Tidigare erfarenhet från ledande roll är meriterande  Eftersom vi arbetar för en jämn könsfördelning ser vi gärna kvinnliga sökande till tjänsten.  Vårt erbjudande Vilka är Toyota Material Handling? På vår site i Mjölby arbetar 3000 medarbetare med materialhantering från utvecklingskoncept till producerat fordon. Vår produktrange går från trotjänare som manuella handtruckar till autonoma självkörande fordon och innovativa energilösningar. Hos oss står medarbetaren i fokus och våra medarbetare beskriver oss som ett vänskapligt bolag med fokus på personlig utveckling, personligt ansvar och hälsa.  Hållbar arbetsgivare  På Toyota eftersträvar vi att vara en hållbar arbetsgivare genom vårt framstående miljöarbete och vår personalpolitik. Vi har i dagsläget bland annat en koldioxidneutral produktion och en KRAV-märkt personalrestaurang. Vidare erbjuder vi vår personal goda träningsmöjligheter genom gruppträning åtta gånger per vecka och ett nyrenoverat gym.    Pendling Vi har goda pendlingsmöjligheter med pendeltåg och bil från de flesta städerna i Östergötland, 30 minuter från Linköping och 20 minuter från Motala. Vi erbjuder gratis parkering inom 5 minuter från din arbetsplats med fri tillgång till motorvärmare.  Din ansökan  Låter det intressant? Ansök i så fall senast den 2022-08-14. Vi påbörjar urvalet efter sista ansökningsdagen.   För mer information om tjänsten:  Fredrik Lundell, Produktionschef, Walkie Montering, 0142-865 08  Linda Zaar, HR Business Partner, tel 0142-883 70  Instagram: ToyotaMHsweden  Linkedin: Toyota Material Handling Manufacturering Sweden AB  Youtube: Toyota Material Handling

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Produktionsledare** Vill du jobba i en ledarroll ... | x | x | 17 | 17 | [produktionsledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/c45A_Cvx_xmD) |
| 2 | ...are än du tror. Då en av våra **produktionsledare** går vidare mot nytt uppdrag s... | x | x | 17 | 17 | [produktionsledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/c45A_Cvx_xmD) |
| 3 | ...a inom Walkie Montering.  Som **Produktionsledare** på Toyota kommer du bland ann... | x | x | 17 | 17 | [produktionsledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/c45A_Cvx_xmD) |
| 4 | ...delningen i linje med Toyotas **produktionssystem** och verksamhetens övergripand... |  | x |  | 17 | [hantera produktionssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/8HGZ_c9R_e47) |
| 4 | ...delningen i linje med Toyotas **produktionssystem** och verksamhetens övergripand... |  | x |  | 17 | [utveckla produktionssystem för druvor som får torka på vinstockarna, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HaC3_HW5_Zkn) |
| 5 | ...te  * Övergripande ansvar för **personal**, leverans, kvalitet, arbetsmi... | x |  |  | 8 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| 6 | ...gripande ansvar för personal, **leverans**, kvalitet, arbetsmiljö samt e... | x |  |  | 8 | [ansvara för leveranser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UEdf_JTg_VXa) |
| 7 | ...personal, leverans, kvalitet, **arbetsmiljö** samt ekonomin på din avdelnin... | x |  |  | 11 | [ansvara för arbetsmiljöfrågor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fNhC_2ti_e4W) |
| 7 | ...personal, leverans, kvalitet, **arbetsmiljö** samt ekonomin på din avdelnin... |  | x |  | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 8 | ...s, kvalitet, arbetsmiljö samt **ekonomin** på din avdelning  * Skapa god... | x |  |  | 8 | [Ekonomiansvarig, **job-title**](http://data.jobtechdev.se/taxonomy/concept/eBQs_p5w_Jkj) |
| 9 | ...amarbetskultur och en positiv **arbetsmiljö** för dina medarbetare  I tjäns... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 10 | ...ett nära samarbete med övriga **produktionsledare** i ledningsgruppen. Din avdeln... | x | x | 17 | 17 | [produktionsledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/c45A_Cvx_xmD) |
| 11 | ...de Team Leaders och vid behov **produktionstekniker**. Tillsammans för ni avdelning... |  | x |  | 19 | [produktionstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/A9i8_BAH_H1M) |
| 12 | ...nsten hos oss.  Till tjänsten **Produktionsledare** söker vi dig som:   * Har en ... | x | x | 17 | 17 | [produktionsledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/c45A_Cvx_xmD) |
| 13 | ...erial Handling? På vår site i **Mjölby** arbetar 3000 medarbetare med ... | x | x | 6 | 6 | [Mjölby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/stqv_JGB_x8A) |
| 14 | ... arbetar 3000 medarbetare med **materialhantering** från utvecklingskoncept till ... |  | x |  | 17 | [underhålla utrustning för materialhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KRDD_CbQ_xA1) |
| 14 | ... arbetar 3000 medarbetare med **materialhantering** från utvecklingskoncept till ... |  | x |  | 17 | [använda utrustning för materialhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xpFq_J44_sHW) |
| 15 | ...änskapligt bolag med fokus på **personlig utveckling**, personligt ansvar och hälsa.... | x | x | 20 | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 16 | ...ckling, personligt ansvar och **hälsa**.  Hållbar arbetsgivare  På To... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 17 | ...urang. Vidare erbjuder vi vår **personal** goda träningsmöjligheter geno... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 18 | ...goda pendlingsmöjligheter med **pendeltåg** och bil från de flesta städer... |  | x |  | 9 | [Pendeltåg, förarutbildning/Motorvagn, förarutbildning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FCdQ_vPV_Yrq) |
| 19 | ...bil från de flesta städerna i **Östergötland**, 30 minuter från Linköping oc... | x | x | 12 | 12 | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
| 20 | ...Östergötland, 30 minuter från **Linköping** och 20 minuter från Motala. V... |  | x |  | 9 | [Linköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bm2x_1mr_Qhx) |
| 21 | ...Linköping och 20 minuter från **Motala**. Vi erbjuder gratis parkering... |  | x |  | 6 | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| 22 | ...ing, 0142-865 08  Linda Zaar, **HR Business Partner**, tel 0142-883 70  Instagram: ... | x |  |  | 19 | [HR business partner, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/23nR_PXa_9br) |
| | **Overall** | | | **139** | **323** | 139/323 = **43%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [HR business partner, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/23nR_PXa_9br) |
|  | x |  | [hantera produktionssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/8HGZ_c9R_e47) |
|  | x |  | [produktionstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/A9i8_BAH_H1M) |
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
|  | x |  | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
|  | x |  | [Pendeltåg, förarutbildning/Motorvagn, förarutbildning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FCdQ_vPV_Yrq) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [utveckla produktionssystem för druvor som får torka på vinstockarna, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HaC3_HW5_Zkn) |
|  | x |  | [underhålla utrustning för materialhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KRDD_CbQ_xA1) |
| x |  |  | [ansvara för leveranser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UEdf_JTg_VXa) |
| x |  |  | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
|  | x |  | [Linköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bm2x_1mr_Qhx) |
| x | x | x | [produktionsledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/c45A_Cvx_xmD) |
| x |  |  | [Ekonomiansvarig, **job-title**](http://data.jobtechdev.se/taxonomy/concept/eBQs_p5w_Jkj) |
| x |  |  | [ansvara för arbetsmiljöfrågor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fNhC_2ti_e4W) |
| x | x | x | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
| x | x | x | [Mjölby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/stqv_JGB_x8A) |
| x | x | x | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
|  | x |  | [använda utrustning för materialhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xpFq_J44_sHW) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **6** | 6/20 = **30%** |