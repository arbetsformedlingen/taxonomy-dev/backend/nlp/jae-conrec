# Results for 'd8d837519dff6e6cb14c0c06155fca21e13ea5b9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d8d837519dff6e6cb14c0c06155fca21e13ea5b9](README.md) | 1 | 3278 | 17 | 9 | 20/226 = **9%** | 2/19 = **11%** |

## Source text

Skyltmakare / foliemontör / montör till bildekor Om företaget Tryckcentrum är en kvalitetsleverantör inom print, skylt & dekor och arbetskläder. Vi finns i en nybyggd produktionsanläggning i Karlskrona där vi också har huvudkontor, butik och showroom. Teknikhallen rymmer allt från tryckpressar till brodyrmaskiner och storformatskrivare, helt enkelt för att kapa kostnader och ledtider samtidigt som det är en kreativ miljö där vi inspirerar varandra och våra kunder.  Vi har även en modern skyltfabrik i Sölvesborg och produktion i Backaryd. Det finns med andra ord stor möjlighet att göra en bra och lång karriär hos oss på Tryckcentrum.  Dina arbetsuppgifter För att söka detta jobb krävs det att du har erfarenhet av skyltmakeri och foliemontering på allt från fordon till skyltfönster. Du kommer att jobba i högt tempo och krav på stort eget ansvar är en självklarhet.  Din bakgrund/Dina egenskaper Då arbetet kan innehålla dagar med högt tempo gäller det att du kan hantera stress samtidigt som det krävs att jobbet blir gjort med hög kvalitet.  Du måste ha ett stort engagemang och verka för att vi tillsammans skall utvecklas och göra arbetsplatsen trivsam och spännande.  Viktigt är att du är kvalitetsmedveten, driven. Trivs du som bäst när det är ordning och reda runt omkring dig kommer detta passa utmärkt för dig. Vi ser även att du är positiv, initiativtagande och kan jobba i både grupp och enskilt.  Du brinner för att ge god service och arbetar alltid med viljan att överträffa kundens förväntningar. Du är effektiv i ditt arbete, tar egna initiativ och du har ett flexibelt arbetssätt där du kan arbeta mot flera mål samtidigt, även under tidspress.  Vi kommer i denna rekrytering att lägga stor vikt vid din personlighet.  Information och kontakt Anställning Detta är ett konsultuppdrag hos Wikan Personal där du arbetar mot vår kund. Tjänsten börjar med 6 månaders visstidsanställning men om allting fungerar som det ska övergår tjänsten till en tillsvidareanställning. Omfattningen är på dagtid och på heltid (100%)  När du är anställd hos oss på Wikan Personal kan du vara säker på att du hamnat rätt. Vi är ett auktoriserat bemanningsbolag som värnar och ser till det bästa för våra anställda och kunder. Du har en lokal konsultchef med hög närvaro som ett stöd om det skulle uppstå frågor eller dylikt.  Ansök redan idag. Dock senast den 7/8 2022 Vi kontaktar aktuella kandidater löpande och tjänsten kan tillsättas innan sista ansökningsdatum. (Notera att vi inte tar emot ansökningar via e-post eller telefon)  Har du frågor eller funderingar ring Kjelle Rutzell, 0455 - 61 27 01   Om Wikan Personal Wikan Personal är din personliga och lokala arbetsgivare i bemanningsbranschen. Vårt breda kontaktnät hjälper dig som har rätt inställning och kompetens, till arbete. Vi verkar i flera olika branscher, vilket öppnar många dörrar för dig i jakten på nästa jobb. Vi lägger fokus på vår personal, deras välmående och utveckling. Vi kan personal.  Wikan Personal är ett auktoriserat och etablerat bemannings- och rekryteringsföretag. Med lokal förankring vill vi hjälpa företag och organisationer med kompetent och välmotiverad personal. Wikan har kontor på 15 orter i Sverige, omsätter ca 450 Msek och har omkring 1000 anställda. Läs mer på www.wikan.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Skyltmakare** / foliemontör / montör till b... |  | x |  | 11 | [Skyltmakare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/TLbt_inQ_VT8) |
| 1 | **Skyltmakare** / foliemontör / montör till b... | x |  |  | 11 | [Skylttillverkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zvCK_hhg_Tt9) |
| 2 | Skyltmakare / **foliemontör** / montör till bildekor Om för... | x |  |  | 11 | [Folierare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EMSh_67p_cSp) |
| 3 | Skyltmakare / folie**montör** / montör till bildekor Om för... |  | x |  | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 4 | Skyltmakare / foliemontör / **montör** till bildekor Om företaget Tr... | x |  |  | 6 | [Övriga montörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/HXzD_qvt_HV6) |
| 5 | ...byggd produktionsanläggning i **Karlskrona** där vi också har huvudkontor,... | x | x | 10 | 10 | [Karlskrona, **municipality**](http://data.jobtechdev.se/taxonomy/concept/YSt4_bAa_ccs) |
| 6 | ...där vi också har huvudkontor, **butik** och showroom. Teknikhallen ry... |  | x |  | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 7 | ...Teknikhallen rymmer allt från **tryckpressar** till brodyrmaskiner och storf... |  | x |  | 12 | [använda digitala tryckpressar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1voi_okZ_Tcb) |
| 7 | ...Teknikhallen rymmer allt från **tryckpressar** till brodyrmaskiner och storf... |  | x |  | 12 | [använda tryckpressar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m8Ee_FsW_rNP) |
| 8 | ...r allt från tryckpressar till **brodyrmaskiner** och storformatskrivare, helt ... |  | x |  | 14 | [Brodyrmaskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/b4tF_Msc_GKp) |
| 9 | ... även en modern skyltfabrik i **Sölvesborg** och produktion i Backaryd. De... | x | x | 10 | 10 | [Sölvesborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EVPy_phD_8Vf) |
| 10 | ...erfarenhet av skyltmakeri och **foliemontering** på allt från fordon till skyl... | x |  |  | 14 | [Foliering, **skill**](http://data.jobtechdev.se/taxonomy/concept/jK9j_m1s_vxD) |
| 11 | ...obba i högt tempo och krav på **stort eget ansvar** är en självklarhet.  Din bakg... | x |  |  | 17 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 12 | ...t tempo gäller det att du kan **hantera stress** samtidigt som det krävs att j... | x |  |  | 14 | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
| 13 | ... ska övergår tjänsten till en **tillsvidareanställning**. Omfattningen är på dagtid oc... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 14 | ...attningen är på dagtid och på **heltid** (100%)  När du är anställd ho... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 15 | ... jobb. Vi lägger fokus på vår **personal**, deras välmående och utveckli... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 16 | ...mående och utveckling. Vi kan **personal**.  Wikan Personal är ett aukto... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 17 | ...ling. Vi kan personal.  Wikan **Personal** är ett auktoriserat och etabl... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 18 | ...ed kompetent och välmotiverad **personal**. Wikan har kontor på 15 orter... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 19 | ...motiverad personal. Wikan har **kontor** på 15 orter i Sverige, omsätt... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 20 | ...ikan har kontor på 15 orter i **Sverige**, omsätter ca 450 Msek och har... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **20** | **226** | 20/226 = **9%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [använda digitala tryckpressar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1voi_okZ_Tcb) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Folierare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EMSh_67p_cSp) |
| x | x | x | [Sölvesborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EVPy_phD_8Vf) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x |  |  | [Övriga montörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/HXzD_qvt_HV6) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
|  | x |  | [Skyltmakare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/TLbt_inQ_VT8) |
|  | x |  | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| x | x | x | [Karlskrona, **municipality**](http://data.jobtechdev.se/taxonomy/concept/YSt4_bAa_ccs) |
|  | x |  | [Brodyrmaskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/b4tF_Msc_GKp) |
| x |  |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Foliering, **skill**](http://data.jobtechdev.se/taxonomy/concept/jK9j_m1s_vxD) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [använda tryckpressar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/m8Ee_FsW_rNP) |
| x |  |  | [hantera stressande situationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qhLq_ZdJ_rqJ) |
|  | x |  | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| x |  |  | [Skylttillverkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zvCK_hhg_Tt9) |
| | | **2** | 2/19 = **11%** |