# Results for 'de640632c65d8d25ee86bd2e35ddc1b5b8783335'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [de640632c65d8d25ee86bd2e35ddc1b5b8783335](README.md) | 1 | 1803 | 11 | 6 | 48/149 = **32%** | 4/13 = **31%** |

## Source text

Ekonomibiträde till köket Blomstergården Allmän visstidsanställning på 50 % för perioden 220822 - 230822. Arbetstider 10:30-15:00  Måltidsservice är en enhet i Östersunds kommun. Vi tillagar frukost, lunch och mellanmål till förskolor och skolor. Samt lunch, mellanmål, middag och portionsmat till äldre som bor hemma, på servicehus och på särskilt boende.  Inom vår verksamhet har vi närmare 90 kök fördelat på sju områden. Våra kockar och biträden tillagar ca 11 000 måltider per dag.  Blomstergården med sina 264 inskrivna elever, är en tillfällig evakuerinsskola för Lugnviksskolan, under ombyggnadstiden. Det är ett mottagningskök, vilket betyder att huvudkomponenten skickas varje dag från ett centralt tillagningskök, men ris, pasta och potatis kokar man själva, samt bereder större delen av salladsbuffén. På Blomstergården jobbar du i ett team på 2 personer. Man serverar lunch, samt diskar och städar matsalen. Man hjälps även åt med råvarubeställning, mängdbeställning, matsvinnsmätning och statistik. Du utför de uppgifter som krävs i ett mottagningskök, och följer de lagar, policys och uppsatta mål som enheten har. Vårt område heter "Centrala, Odensala & Lugnvik."  - Biträdesutbildning eller erfarenhet från kök eller servering 1 år  - Goda kunskaper i livsmedelshygien. - Utbildning i HACCP är meriterande.  - Erfarenhet av liknande arbete är meriterande.  - Vi sätter stort värde på personliga egenskaper såsom gott bemötande, service, lyhördhet, engagemang och god samarbetsförmåga.  - Arbetet är fysiskt ansträngande med en del tunga lyft, så du bör ha god fysik.  Bifoga CV, personligt brev samt referenser i din ansökan. Betyg/intyg behöver inte skickas med. Uppvisande av detta blir aktuellt vid eventuell intervju. Intervjuer kan eventuellt ske löpande under ansökningstiden.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Ekonomibiträde** till köket Blomstergården All... | x | x | 14 | 14 | [Ekonomibiträde, storhushåll, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Kf29_nju_Xd4) |
| 2 | ...äde till köket Blomstergården **Allmän visstidsanställning** på 50 % för perioden 220822 -... | x |  |  | 26 | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| 3 | ...Allmän visstidsanställning på **50 %** för perioden 220822 - 230822.... | x |  |  | 4 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 4 | ... Måltidsservice är en enhet i **Östersunds kommun**. Vi tillagar frukost, lunch o... | x |  |  | 17 | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| 5 | ...ost, lunch och mellanmål till **förskolor** och skolor. Samt lunch, mella... | x |  |  | 9 | [Förskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/6o2S_cvR_GwA) |
| 6 | ... till äldre som bor hemma, på **servicehus** och på särskilt boende.  Inom... | x | x | 10 | 10 | [Servicehus/Äldreboende, **skill**](http://data.jobtechdev.se/taxonomy/concept/7gPY_qhU_mkB) |
| 7 | ...r hemma, på servicehus och på **särskilt boende**.  Inom vår verksamhet har vi ... | x | x | 15 | 15 | [Sjukhem/Särskilt boende, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZpXZ_PG5_B9s) |
| 8 | ...fördelat på sju områden. Våra **kockar** och biträden tillagar ca 11 0... | x |  |  | 6 | [Kock, **job-title**](http://data.jobtechdev.se/taxonomy/concept/RTQf_1Qm_YhD) |
| 8 | ...fördelat på sju områden. Våra **kockar** och biträden tillagar ca 11 0... |  | x |  | 6 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 9 | ... sju områden. Våra kockar och **biträden** tillagar ca 11 000 måltider p... |  | x |  | 8 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 10 | ...ällning, matsvinnsmätning och **statistik**. Du utför de uppgifter som kr... | x | x | 9 | 9 | [Statistik, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/VDZC_YeP_k1P) |
| 11 | ...ring 1 år  - Goda kunskaper i **livsmedelshygien**. - Utbildning i HACCP är meri... | x |  |  | 16 | [Livsmedelshygien, **skill**](http://data.jobtechdev.se/taxonomy/concept/kj4Q_nLh_u8Q) |
| 12 | ... del tunga lyft, så du bör ha **god fysik**.  Bifoga CV, personligt brev ... | x |  |  | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| | **Overall** | | | **48** | **149** | 48/149 = **32%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Förskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/6o2S_cvR_GwA) |
| x | x | x | [Servicehus/Äldreboende, **skill**](http://data.jobtechdev.se/taxonomy/concept/7gPY_qhU_mkB) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Ekonomibiträde, storhushåll, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Kf29_nju_Xd4) |
| x |  |  | [Kock, **job-title**](http://data.jobtechdev.se/taxonomy/concept/RTQf_1Qm_YhD) |
|  | x |  | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x | x | x | [Statistik, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/VDZC_YeP_k1P) |
| x |  |  | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| x | x | x | [Sjukhem/Särskilt boende, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZpXZ_PG5_B9s) |
| x |  |  | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| x |  |  | [Livsmedelshygien, **skill**](http://data.jobtechdev.se/taxonomy/concept/kj4Q_nLh_u8Q) |
| x |  |  | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
|  | x |  | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| | | **4** | 4/13 = **31%** |