# Results for '8ac4b3165f08fd6817e0045d4ff33c288ac22b32'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8ac4b3165f08fd6817e0045d4ff33c288ac22b32](README.md) | 1 | 455 | 4 | 3 | 45/69 = **65%** | 3/4 = **75%** |

## Source text

Serveringspersonal - Restaurangbiträde Vi söker nya medarbetare till vår restaurang! Här finns förutsättningar för den som har viljan att utvecklas och växa med oss, meriterande om du har minst 1-2 års erfarenhet. Välkommen till en bra arbetsgivare och en välmående arbetsgrupp med mycket arbetsglädje! Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Serveringspersonal** - Restaurangbiträde Vi söker ... | x | x | 18 | 18 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | Serveringspersonal - **Restaurangbiträde** Vi söker nya medarbetare till... | x | x | 17 | 17 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 3 | ...öker nya medarbetare till vår **restaurang**! Här finns förutsättningar fö... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 4 | ...ed oss, meriterande om du har **minst 1-2 års erfarenhet**. Välkommen till en bra arbets... | x |  |  | 24 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| | **Overall** | | | **45** | **69** | 45/69 = **65%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x | x | x | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| | | **3** | 3/4 = **75%** |