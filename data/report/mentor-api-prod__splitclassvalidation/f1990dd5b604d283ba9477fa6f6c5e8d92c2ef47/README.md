# Results for 'f1990dd5b604d283ba9477fa6f6c5e8d92c2ef47'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f1990dd5b604d283ba9477fa6f6c5e8d92c2ef47](README.md) | 1 | 4285 | 30 | 26 | 279/489 = **57%** | 12/23 = **52%** |

## Source text

Kriminalvårdare till kriminalvårdsutbildningen NTE Västerås, placering Sala Kriminalvården växer – vi bygger nytt, bygger om och bygger till och söker nu nya medarbetare. Vårt viktigaste mål är att minska återfall i brott – att bryta den onda cirkeln. Lyckas vi med det blir samhället tryggare och säkrare för alla. Vi är den myndighet som ansvarar för anstalter, frivård, häkten och klienttransporter.  ARBETSUPPGIFTER Den här annonsen är för dig som vill arbeta som kriminalvårdare men som inte har gått kriminalvårdsutbildning.  Som kriminalvårdare kan du bidra till ett tryggare och säkrare samhälle. Du jobbar i team tillsammans med andra yrkesgrupper och får möjlighet att hjälpa människor att bryta den onda cirkeln och motivera till att inte återfalla i brott. Det är viktigt att du delar Kriminalvårdens grundläggande värderingar om en human människosyn, respekt för individen och tron på att människan kan utvecklas och förändras.   Att arbeta vid transportenheten innebär att din huvudsakliga uppgift är att transportera frihetsberövade personer, för Kriminalvården och andra myndigheters räkning. Arbetet omfattar också bevakning under t.ex. rättegångar och vid sjukhusbesök. Det varierande uppdraget innebär att arbetstiden kan vara olika dag för dag.  Beroende på antalet utbildningsplatser och vårt behov av personal börjar du din anställning med antingen utbildning eller arbete. Om du börjar arbeta direkt är tanken att du så fort som möjligt ska börja din kriminalvårdsutbildning. Det är en obligatorisk utbildning på 23 veckor och du får full lön under hela utbildningstiden. Utbildningen varvar teori och praktik. Under provanställningen genomförs löpande bedömningar för din möjlighet till tillsvidareanställning.  KVALIFIKATIONER Dina personliga egenskaper är viktiga. Som kriminalvårdare behöver du vara trygg, stabil, ha god självinsikt, gott omdöme och integritet. Tillsammans med dina kollegor driver du verksamheten, så du behöver ha god samarbetsförmåga och vara tydlig i din kommunikation. Du ska kunna agera fysiskt när det krävs.  Vi söker dig som har:  •	Arbetslivserfarenhet, även i form av säsongsarbete, timanställning eller eget företagande. Praktik och volontärarbete är inte att jämställa med anställning. •	Fullständig gymnasieutbildning alternativt annan utbildning i kombination med arbetslivserfarenhet som KV bedömer relevant. •	Goda kunskaper i svenska språket i både tal och skrift •	God datorvana •	B-körkort för manuellt växlat fordon och körvana  Det är meriterande om du har:  •	Arbetslivserfarenhet från kriminalvård, säkerhetsarbete, social omsorg, psykiatri eller vård- och omsorgsarbete •	Kunskap inom relevanta områden, exempelvis beteendevetenskap, kriminologi psykiatri, juridik och konflikthantering •	Relevanta språkkunskaper  ÖVRIGT Vi ställer höga krav på våra medarbetares säkerhets- och sekretessmedvetande. Därför gör vi en säkerhetskontroll innan vi beslutar om du får anställning hos oss. Vissa tjänster är placerade i säkerhetsklass och då gör vi en säkerhetsprövning med registerkontroll innan vi fattar anställningsbeslut. För tjänster med högre säkerhetsklass behöver du vara svensk medborgare, men du får gärna ha utländsk bakgrund.  I anställningen ingår en obligatorisk kriminalvårdsutbildning. Den kan komma att genomföras på annan ort. Kriminalvårdens verksamhet pågår dygnet runt, året runt. Det innebär att du kommer att arbeta dagar, kvällar och helger. Vissa scheman innebär arbete på nätter.   Tjänsten tillsätts under förutsättning att erforderliga beslut fattas.  Läs mer om förutsättningar kring anställningsform här: https://www.kriminalvarden.se//jobba-hos-oss/yrkesroller/kriminalvardare/anstallningsform-kriminalvardsutbildning/     Kriminalvården strävar efter en jämn köns- och åldersfördelning och ökad mångfald.   För att skicka in din ansökan, klicka på ansökningslänken i annonsen. Frågor om hur du ansöker och lägger in ditt CV ställer du direkt till Visma Recruit 0771- 693 693. Frågor om specifika jobb besvaras av den kontaktperson som anges i annonsen. Vi tar inte emot ansökningar i pappersform eller per e-post.  Vi undanber oss vänligen men bestämt direktkontakt med bemannings- och rekryteringsföretag och försäljare av rekryteringsannonser. Kriminalvården har upphandlade avtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kriminalvårdare** till kriminalvårdsutbildninge... | x | x | 15 | 15 | [Kriminalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NoHX_doS_a8J) |
| 2 | ...kriminalvårdsutbildningen NTE **Västerås**, placering Sala Kriminalvårde... | x | x | 8 | 8 | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| 3 | ...ingen NTE Västerås, placering **Sala** Kriminalvården växer – vi byg... | x | x | 4 | 4 | [Sala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dAen_yTK_tqz) |
| 4 | ... NTE Västerås, placering Sala **Kriminalvården** växer – vi bygger nytt, bygge... | x | x | 14 | 14 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 5 | ...r för dig som vill arbeta som **kriminalvårdare** men som inte har gått krimina... | x | x | 15 | 15 | [Kriminalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NoHX_doS_a8J) |
| 6 | ...vårdare men som inte har gått **kriminalvårdsutbildning**.  Som kriminalvårdare kan du ... | x | x | 23 | 23 | [Kriminalvårdsutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WW47_z4A_uww) |
| 7 | ...kriminalvårdsutbildning.  Som **kriminalvårdare** kan du bidra till ett tryggar... | x | x | 15 | 15 | [Kriminalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NoHX_doS_a8J) |
| 8 | .... Det är viktigt att du delar **Kriminalvårdens** grundläggande värderingar om ... | x |  |  | 15 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 9 | ...frihetsberövade personer, för **Kriminalvården** och andra myndigheters räknin... | x | x | 14 | 14 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 10 | ...ingsplatser och vårt behov av **personal** börjar du din anställning med... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 11 | ...ort som möjligt ska börja din **kriminalvårdsutbildning**. Det är en obligatorisk utbil... | x | x | 23 | 23 | [Kriminalvårdsutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WW47_z4A_uww) |
| 12 | ...ningar för din möjlighet till **tillsvidareanställning**.  KVALIFIKATIONER Dina pers... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 13 | ...ga egenskaper är viktiga. Som **kriminalvårdare** behöver du vara trygg, stabil... | x | x | 15 | 15 | [Kriminalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NoHX_doS_a8J) |
| 14 | ...amheten, så du behöver ha god **samarbetsförmåga** och vara tydlig i din kommuni... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 15 | ...ivserfarenhet, även i form av **säsongsarbete**, timanställning eller eget fö... | x | x | 13 | 13 | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
| 16 | ...även i form av säsongsarbete, **timanställning** eller eget företagande. Prakt... | x |  |  | 14 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 17 | ...sarbete, timanställning eller **eget **företagande. Praktik och volon... |  | x |  | 5 | [Företagande, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fxGP_Unf_RJZ) |
| 18 | ...te, timanställning eller eget **företagande**. Praktik och volontärarbete ä... | x | x | 11 | 11 | [Företagande, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fxGP_Unf_RJZ) |
| 19 | ...ed anställning. •	Fullständig **gymnasieutbildning** alternativt annan utbildning ... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 20 | ... relevant. •	Goda kunskaper i **svenska** språket i både tal och skrift... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 21 | ... relevant. •	Goda kunskaper i **svenska språket** i både tal och skrift •	God d... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 22 | ... och skrift •	God datorvana •	**B-körkort** för manuellt växlat fordon oc... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 23 | ...  •	Arbetslivserfarenhet från **kriminalvård**, säkerhetsarbete, social omso... |  | x |  | 12 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 23 | ...  •	Arbetslivserfarenhet från **kriminalvård**, säkerhetsarbete, social omso... | x |  |  | 12 | [Kriminalvård, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/xVKK_eNQ_jrR) |
| 24 | ...riminalvård, säkerhetsarbete, **social omsorg**, psykiatri eller vård- och om... | x | x | 13 | 13 | [Social omsorg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sw4N_2Vt_PVD) |
| 25 | ...kerhetsarbete, social omsorg, **psykiatri** eller vård- och omsorgsarbete... | x | x | 9 | 9 | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| 26 | ...relevanta områden, exempelvis **beteendevetenskap**, kriminologi psykiatri, jurid... |  | x |  | 17 | [Beteendevetenskap, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AdeF_LAM_GQH) |
| 26 | ...relevanta områden, exempelvis **beteendevetenskap**, kriminologi psykiatri, jurid... | x |  |  | 17 | [beteendevetenskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Q4vC_gZx_HUa) |
| 27 | ...exempelvis beteendevetenskap, **kriminologi** psykiatri, juridik och konfli... | x | x | 11 | 11 | [Kriminologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/tc2X_KdJ_VEb) |
| 28 | ...eteendevetenskap, kriminologi **psykiatri**, juridik och konflikthanterin... | x | x | 9 | 9 | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| 29 | ...nskap, kriminologi psykiatri, **juridik** och konflikthantering •	Relev... | x | x | 7 | 7 | [Juridik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z9ZH_7XU_Kkb) |
| 30 | ...nologi psykiatri, juridik och **konflikthantering** •	Relevanta språkkunskaper  ... |  | x |  | 17 | [konflikthantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/N47j_j55_R2J) |
| 31 | ...lningen ingår en obligatorisk **kriminalvårdsutbildning**. Den kan komma att genomföras... | x | x | 23 | 23 | [Kriminalvårdsutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WW47_z4A_uww) |
| 32 | ... att genomföras på annan ort. **Kriminalvårdens** verksamhet pågår dygnet runt,... | x |  |  | 15 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 33 | ...-kriminalvardsutbildning/     **Kriminalvården** strävar efter en jämn köns- o... | x | x | 14 | 14 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 34 | ...jare av rekryteringsannonser. **Kriminalvården** har upphandlade avtal. | x | x | 14 | 14 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| | **Overall** | | | **279** | **489** | 279/489 = **57%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
|  | x |  | [Beteendevetenskap, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AdeF_LAM_GQH) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| x | x | x | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [konflikthantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/N47j_j55_R2J) |
| x | x | x | [Kriminalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NoHX_doS_a8J) |
| x |  |  | [beteendevetenskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Q4vC_gZx_HUa) |
| x | x | x | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| x | x | x | [Social omsorg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sw4N_2Vt_PVD) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Kriminalvårdsutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WW47_z4A_uww) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Juridik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z9ZH_7XU_Kkb) |
| x | x | x | [Sala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dAen_yTK_tqz) |
| x | x | x | [Företagande, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fxGP_Unf_RJZ) |
| x |  |  | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| x | x | x | [Kriminologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/tc2X_KdJ_VEb) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x |  |  | [Kriminalvård, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/xVKK_eNQ_jrR) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **12** | 12/23 = **52%** |