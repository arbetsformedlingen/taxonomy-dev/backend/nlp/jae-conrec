# Results for '533244c3cd1050bf72a4d37efe81e2a40fdd13f2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [533244c3cd1050bf72a4d37efe81e2a40fdd13f2](README.md) | 1 | 2374 | 3 | 9 | 9/88 = **10%** | 1/11 = **9%** |

## Source text

Liseberg söker Kaniner till hösten & vintern 2022! (säsongsanställning) Liseberg är en av Nordens ledande turistattraktioner med 3,4 miljoner gäster årligen. Verksamheten består av nöjespark, restauranger, teatrar och boende, och sysselsätter omkring 350 årsanställda och ca 2300 säsongsanställda. Liseberg omsätter årligen ca 1,2 miljard kronor och ingår i koncernen Stadshus AB som är helägt av Göteborgs Stad.     Vill du bli en Lisebergare och dessutom arbeta som Kanin?  På Liseberg får du arbeta med service när den är som bäst i en av Europas ledande nöjesparker.  Du får dessutom arbeta med likasinnade och du kommer få vänner för livet.  Du kan åka Valkyria och andra attraktioner varje dag och njuta av vår vackra park med allt vad det innebär såsom mysiga affärer, restauranger, glasskiosker, spel, hamburgerrestauranger, blommor, uppträdanden med mera. I rollen som Lisebergare är du med och skapar Liseberg, vilket påverkar våra gästers helhetsupplevelse. Liseberg är en plats där du gör skillnad både för våra gäster såväl som för dina kollegor. Hos oss kan du vara dig själv och får utlopp för din kreativa sida.  För att arbeta som aktör på Liseberg krävs inga förkunskaper, men vi tror att du är en naturlig glädjespridare som har lätt till skratt. Vi ser gärna sökanden med olika bakgrund och erfarenheter då vi strävar mot en bred representation i organisationen.  Hopp och lek som Lisebergskanin i Kaninlandet Är du ett energiknippe utöver det vanliga, som fullkomligt spritter av tanken på att hänga med stora gröna kaniner? Då är Kaninlandet absolut rätt arbetsplats för dig!  Du tycker om att arbeta med barn och vill vara en del av våra minsta besökares första möte med Liseberg. Du har lätt för att bjuda på dig själv och rejält med sväng i tassen. Vi lägger stort fokus på sammanhållning och gemenskap i arbetsgruppen med målsättningen att vi ska vara bäst på att leverera glädje, kärlek och minnen till alla gäster i Kaninlandet.  Att arbeta som kanin är fysiskt krävande och för att klara av att bära dräkten under din arbetsdag behöver du ha god fysik och inga skador. Vi ser gärna att du anger din längd i ansökan.  Rekrytering sker via audition.   Villkor Förläggning av arbetstid är knuten till parkens öppettider. Villkor enligt avtal.  Varmt välkommen att anmäla ditt intresse, vi rekryterar löpande så skicka in din ansökan redan idag!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... till hösten & vintern 2022! (**säsongsanställning**) Liseberg är en av Nordens le... | x |  |  | 18 | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
| 2 | ... spel, hamburgerrestauranger, **blommor**, uppträdanden med mera. I rol... |  | x |  | 7 | [Specialiserad butikshandel med blommor och andra växter, frön och gödselmedel, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/FzVF_Qvu_nUq) |
| 2 | ... spel, hamburgerrestauranger, **blommor**, uppträdanden med mera. I rol... |  | x |  | 7 | [sälja blommor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Ms9U_cd7_EmG) |
| 2 | ... spel, hamburgerrestauranger, **blommor**, uppträdanden med mera. I rol... |  | x |  | 7 | [lämna gödslingsrekommendationer för blommor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Q1WQ_m3a_MJo) |
| 2 | ... spel, hamburgerrestauranger, **blommor**, uppträdanden med mera. I rol... |  | x |  | 7 | [specialiserad säljare, blommor och trädgård, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/SqQs_nst_GCG) |
| 2 | ... spel, hamburgerrestauranger, **blommor**, uppträdanden med mera. I rol... |  | x |  | 7 | [Blommor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/VGTH_Jdx_J5v) |
| 2 | ... spel, hamburgerrestauranger, **blommor**, uppträdanden med mera. I rol... |  | x |  | 7 | [butikschef, blommor och trädgård, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/c7D5_J5p_HQW) |
| 2 | ... spel, hamburgerrestauranger, **blommor**, uppträdanden med mera. I rol... |  | x |  | 7 | [Specialiserad butikshandel med blommor och andra växter, frön och gödselmedel samt små sällskapsdjur, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/iJxX_z6D_5VR) |
| 2 | ... spel, hamburgerrestauranger, **blommor**, uppträdanden med mera. I rol... |  | x |  | 7 | [råda kunder om typ av blommor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/srD7_5kE_Ud2) |
| 3 | ...iva sida.  För att arbeta som **aktör** på Liseberg krävs inga förkun... | x |  |  | 5 | [Skådespelare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Xoz2_dQe_9KC) |
| 4 | ...r din arbetsdag behöver du ha **god fysik** och inga skador. Vi ser gärna... | x | x | 9 | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| | **Overall** | | | **9** | **88** | 9/88 = **10%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
|  | x |  | [Specialiserad butikshandel med blommor och andra växter, frön och gödselmedel, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/FzVF_Qvu_nUq) |
|  | x |  | [sälja blommor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Ms9U_cd7_EmG) |
|  | x |  | [lämna gödslingsrekommendationer för blommor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Q1WQ_m3a_MJo) |
|  | x |  | [specialiserad säljare, blommor och trädgård, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/SqQs_nst_GCG) |
|  | x |  | [Blommor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/VGTH_Jdx_J5v) |
| x |  |  | [Skådespelare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Xoz2_dQe_9KC) |
|  | x |  | [butikschef, blommor och trädgård, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/c7D5_J5p_HQW) |
|  | x |  | [Specialiserad butikshandel med blommor och andra växter, frön och gödselmedel samt små sällskapsdjur, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/iJxX_z6D_5VR) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
|  | x |  | [råda kunder om typ av blommor, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/srD7_5kE_Ud2) |
| | | **1** | 1/11 = **9%** |