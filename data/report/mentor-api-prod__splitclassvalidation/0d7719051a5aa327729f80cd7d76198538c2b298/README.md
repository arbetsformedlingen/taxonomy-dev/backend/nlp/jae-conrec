# Results for '0d7719051a5aa327729f80cd7d76198538c2b298'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0d7719051a5aa327729f80cd7d76198538c2b298](README.md) | 1 | 5192 | 33 | 32 | 291/562 = **52%** | 11/23 = **48%** |

## Source text

Specialistläkare i barn- och ungdomspsykiatri  & ADHD i Göteborg Vill du arbeta på en flexibel arbetsplats som tar hänsyn till individens behov – både medarbetarnas och patienternas? Drivs du av ett genuint intresse för neuropsykiatri och värdesätter erfarna kollegor med stort kvalitetsfokus? Uppskattar du tydliga mål, fria arbetstider och möjligheten att förlägga delar av arbetet i hemmet? Då kan Modigo vara arbetsplatsen för dig. Nu utökar vi Modigos Göteborgs-team med ytterligare en specialist i barn- och ungdomspsykiatri för arbete med neuropsykiatriska utredningar. Det finns även möjlighet att arbeta med behandling. Hos oss får du ett spännande och utvecklande arbete i ett bolag som vill utmana branschen till större individanpassning och hög kvalitet. Om Modigo Modigo är en väletablerad vårdgivare och arbetsplats för psykologer, läkare och administrativa medarbetare på fyra orter i Sverige. Vi är specialister inom neuropsykiatri och arbetar inom psykiatrisk öppenvård med utredningar, behandling, rådgivning och föräldrastöd. Företaget präglas av en hög servicenivå och stor flexibilitet. Våra sju mottagningar finns i Stockholm, Göteborg, Lund och Sundsvall. Vi har avtal och gott samarbete med ett flertal regioner, bland andra VGR. I vårt systerföretag ADHD Care by Modigo ger vi medicinsk behandling till patienter med ADHD-diagnos. ADHD Care är en digital klinik som möter patienter från hela Sverige via video- och telefonsamtal. Om tjänsten Nu söker vi ytterligare en specialistläkare som ska arbeta med neuropsykiatriska utredningar av barn och ungdomar. Det finns även möjlighet att arbeta med behandling av ADHD. Tjänsten är tillsvidare på del- eller heltid. Som del i vår flexibilitet kan tjänstgöringsgraden diskuteras. Du har även stor frihet att lägga upp dina arbetstider på ett sätt som passar just dig. Din placering är i vår fina lokal på Otterhällegatan i centrala Göteborg. Du har också möjlighet att arbeta hemifrån när du inte träffar patienter. Varför jobba på Modigo? På Modigo arbetar du i en verksamhet med god struktur och rutiner kring det kliniska arbetet och kollektivavtal, bra pensionslösningar och friskvårdsbidrag för dig som anställd. Här är några anledningar som våra anställda ofta framhåller med Modigo som arbetsgivare: Fokus på kvalitet. På Modigo är det patientnära arbetet i fokus. Cheferna är psykologer med klinisk bakgrund och deras uppgift är att skapa bra förutsättningar för dig som medarbetare. Vi håller nere antalet möten och möteslängden till förmån för det kliniska arbetet. De möten vi har är få och motiverade. Du är omgiven av kompetenta kollegor, bland andra vårt nätverk av specialistläkare och vår MLA. Vidare erbjuds du kontinuerligt kompetenshöjande insatser, utbildningar och deltagande i nationella och internationella kongresser. Flexibilitet för individuella behov. På Modigo utformar du ditt eget arbete och styr din tid. Det kan handla om att själv planera in när på dagen du har patientbesök – för att få livspusslet att gå ihop – och att arbeta hemifrån när du inte träffar patient. En annan viktig aspekt är att Modigo tar hänsyn till patienternas behov och önskemål, exempelvis genom möjligheten att genomföra utredningar under en kortare, koncentrerad period. Bra villkor. Vi erbjuder bland annat konkurrenskraftiga löner, bra pensionslösningar och generöst friskvårdsbidrag. Framåtanda och högt i tak. Modigo är en prestigelös organisation som hela tiden utvecklas för patienternas och medarbetarnas bästa. Har du idéer som kan göra verksamheten bättre – prata med din chef. Chansen är stor att de snart blir verklighet.  Om dig Vi söker dig som är legitimerad läkare och specialist i barn- och ungdomspsykiatri. Du har ett genuint intresse för neuropsykiatri, är flexibel, serviceinriktad och har lätt för att samarbeta. För dig är det självklart att sätta patienten i fokus och du har mycket god förmåga att kommunicera och bygga förtroende. Du är ödmjuk, ansvarstagande, nyfiken och noggrann. Vi vet att mångfald berikar oss och ser gärna sökande med olika bakgrunder och erfarenheter. För att trivas hos oss är det viktigt att du är: Självständig och strukturerad. Du lägger själv upp en plan för ditt arbete, ser till att få jobbet gjort samtidigt som du tar hand om dig själv, till exempel genom tillräckligt med pauser. Trygg i din yrkesroll och kompetens. Du står på egna ben i det kliniska arbetet, samtidigt som du drar nytta av den struktur och kompetens som finns till ditt förfogande på Modigo. Flexibel och nyfiken. Du vill arbeta i en agil verksamhet som ständigt utvecklas. Du är lösningsorien-terad och vill, utifrån den givna strukturen, hitta individuella anpassningar för att hjälpa varje patient så bra som möjligt.   Frågor Har du frågor kring tjänsten är du varmt välkommen att kontakta Medicinsk ledningsansvarig Ioannis Pantziaras via ioannis.pantziaras@modigo.se. Får du inget svar semesterperioden så var lugn, Ioannis återkommer så fort han har möjlighet. Så ansöker du Maila din ansökan med CV och personligt brev till jobb@modigo.se. Skriv ”Läkare Göteborg” i ämnesraden i mailet. Urval sker löpande så skicka in din ansökan så snart som möjligt, dock senast 14 augusti.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Specialistläkare** i barn- och ungdomspsykiatri ... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 2 | Specialistläkare i **barn- och ungdomspsykiatri**  & ADHD i Göteborg Vill du ar... | x | x | 26 | 26 | [Barnpsykiatri/Ungdomspsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/ckhF_ACN_GqU) |
| 3 | ...ch ungdomspsykiatri  & ADHD i **Göteborg** Vill du arbeta på en flexibel... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 4 | ...u av ett genuint intresse för **neuropsykiatri** och värdesätter erfarna kolle... |  | x |  | 14 | [neuropsykiatri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hVTC_kQL_qY1) |
| 5 | ...borgs-team med ytterligare en **specialist** i barn- och ungdomspsykiatri ... | x |  |  | 10 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 6 | ...d ytterligare en specialist i **barn- och ungdomspsykiatri** för arbete med neuropsykiatri... | x | x | 26 | 26 | [Barnpsykiatri/Ungdomspsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/ckhF_ACN_GqU) |
| 7 | ...gdomspsykiatri för arbete med **neuropsykiatriska utredningar**. Det finns även möjlighet att... | x | x | 29 | 29 | [neuropsykiatri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hVTC_kQL_qY1) |
| 8 | ...årdgivare och arbetsplats för **psykologer**, läkare och administrativa me... | x | x | 10 | 10 | [Psykolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xBo9_V9m_bfg) |
| 9 | ...h arbetsplats för psykologer, **läkare** och administrativa medarbetar... | x |  |  | 6 | [Läkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9K3i_XD4_Kyy) |
| 9 | ...h arbetsplats för psykologer, **läkare** och administrativa medarbetar... |  | x |  | 6 | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| 10 | ...ts för psykologer, läkare och **administrativa medarbetare** på fyra orter i Sverige. Vi ä... | x |  |  | 26 | [Administrativa assistenter, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/qQt2_8Qw_vMC) |
| 11 | ...a medarbetare på fyra orter i **Sverige**. Vi är specialister inom neur... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 12 | ...rige. Vi är specialister inom **neuropsykiatri** och arbetar inom psykiatrisk ... |  | x |  | 14 | [neuropsykiatri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hVTC_kQL_qY1) |
| 13 | ...Våra sju mottagningar finns i **Stockholm**, Göteborg, Lund och Sundsvall... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 14 | ...ttagningar finns i Stockholm, **Göteborg**, Lund och Sundsvall. Vi har a... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 15 | ... finns i Stockholm, Göteborg, **Lund** och Sundsvall. Vi har avtal o... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 16 | ...Stockholm, Göteborg, Lund och **Sundsvall**. Vi har avtal och gott samarb... | x | x | 9 | 9 | [Sundsvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dJbx_FWY_tK6) |
| 17 | ...som möter patienter från hela **Sverige** via video- och telefonsamtal.... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 18 | ...en Nu söker vi ytterligare en **specialistläkare** som ska arbeta med neuropsyki... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 19 | ...listläkare som ska arbeta med **neuropsykiatriska utredningar** av barn och ungdomar. Det fin... | x | x | 29 | 29 | [neuropsykiatri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hVTC_kQL_qY1) |
| 20 | ...handling av ADHD. Tjänsten är **tillsvidare** på del- eller heltid. Som del... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 21 | ...D. Tjänsten är tillsvidare på **del- eller heltid**. Som del i vår flexibilitet k... | x |  |  | 17 | (not found in taxonomy) |
| 22 | ...på Otterhällegatan i centrala **Göteborg**. Du har också möjlighet att a... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 23 | ...g. Du har också möjlighet att **arbeta hemifrån** när du inte träffar patienter... | x |  |  | 15 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 24 | ...ring det kliniska arbetet och **kollektivavtal**, bra pensionslösningar och fr... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 25 | ... arbetet i fokus. Cheferna är **psykologer** med klinisk bakgrund och dera... | x | x | 10 | 10 | [Psykolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xBo9_V9m_bfg) |
| 26 | ..., bland andra vårt nätverk av **specialistläkare** och vår MLA. Vidare erbjuds d... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 27 | ....  Om dig Vi söker dig som är **legitimerad läkare** och specialist i barn- och un... | x |  |  | 18 | [Läkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9K3i_XD4_Kyy) |
| 28 | ... söker dig som är legitimerad **läkare** och specialist i barn- och un... |  | x |  | 6 | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| 29 | ...som är legitimerad läkare och **specialist** i barn- och ungdomspsykiatri.... | x |  |  | 10 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 30 | ...merad läkare och specialist i **barn- och ungdomspsykiatri**. Du har ett genuint intresse ... | x | x | 26 | 26 | [Barnpsykiatri/Ungdomspsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/ckhF_ACN_GqU) |
| 31 | ... har ett genuint intresse för **neuropsykiatri**, är flexibel, serviceinriktad... | x | x | 14 | 14 | [neuropsykiatri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hVTC_kQL_qY1) |
| 32 | ...inriktad och har lätt för att **samarbeta**. För dig är det självklart at... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 33 | ...ten i fokus och du har mycket **god förmåga att kommunicera** och bygga förtroende. Du är ö... | x |  |  | 27 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 34 | ...gga förtroende. Du är ödmjuk, **ansvarstagande**, nyfiken och noggrann. Vi vet... | x |  |  | 14 | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
| 34 | ...gga förtroende. Du är ödmjuk, **ansvarstagande**, nyfiken och noggrann. Vi vet... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 35 | ..., ansvarstagande, nyfiken och **noggrann**. Vi vet att mångfald berikar ... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 36 | ...oss är det viktigt att du är: **Självständig** och strukturerad. Du lägger s... | x | x | 12 | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 37 | ...som ständigt utvecklas. Du är **lösningsorien-terad** och vill, utifrån den givna s... | x |  |  | 19 | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| 38 | ...v till jobb@modigo.se. Skriv ”**Läkare** Göteborg” i ämnesraden i mail... |  | x |  | 6 | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| 39 | ...jobb@modigo.se. Skriv ”Läkare **Göteborg**” i ämnesraden i mailet. Urval... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| | **Overall** | | | **291** | **562** | 291/562 = **52%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Läkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9K3i_XD4_Kyy) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x |  |  | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Barnpsykiatri/Ungdomspsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/ckhF_ACN_GqU) |
| x | x | x | [Sundsvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dJbx_FWY_tK6) |
| x |  |  | (not found in taxonomy) |
| x | x | x | [neuropsykiatri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hVTC_kQL_qY1) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| x | x | x | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| x |  |  | [Administrativa assistenter, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/qQt2_8Qw_vMC) |
|  | x |  | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| x |  |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| x | x | x | [Psykolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xBo9_V9m_bfg) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **11** | 11/23 = **48%** |