# Results for '784532fb953c09ba06b0aa25235479447e38598d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [784532fb953c09ba06b0aa25235479447e38598d](README.md) | 1 | 3133 | 27 | 22 | 156/388 = **40%** | 13/32 = **41%** |

## Source text

Embedded utvecklare till familjärt bolag i tillväxtresa Här har vi ett företag som är så bra på det de gör att deras varumärke har blivit synonymt med deras produkt, i hela branschen. Hur coolt är inte det!? De letar nu efter en ny programmerare som ska jobba med inbyggda system och du kan bli deras nya medarbetare. Här får du både skapa nytt och vidareutveckla kod som redan finns. Om det här låter som ditt nästa steg i karriären, läs mer om tjänsten här nedanför och sök redan idag!   OM TJÄNSTEN  Vi hjälper nu vår kund som är mitt i en stor uppskalningsresa och växer både i medarbetare och produkter att hitta sin nya programmerare. Företaget, som är svenskgrundat och idag är ungefär 20 medarbetare, har produktion, utveckling och försäljning av avancerade flödesvakter och flödesmätare. De är verksamma över hela världen och deras varumärke är så starkt att det har blivit synonymt med flödesövervakning.  #  Du erbjuds   * En långsiktig tjänst i ett familjärt företag där alla känner alla * Delaktighet i hela utvecklingsprocessen av företagets produkter * På sikt möjlighet att specialisera dig inom det du brinner för mest!   Som konsult för Academic Work erbjuder vi stora möjligheter för dig att växa professionellt, bygga ditt nätverk och skapa värdefulla kontakter för framtiden. Läs mer om vårt konsulterbjudande. Anställningen kommer börja som ett konsultuppdrag genom Academic Work men ambitionen är att det kommer övergå till en anställning direkt hos företaget efter en tid.  ARBETSUPPGIFTER  Du kommer arbeta i ett litet team i nära samarbete med en elektronikingenjör och en mekanikingenjör. Eftersom bolaget i Sverige har ca 20 anställda kommer du samarbeta mycket över team-gränsen och lära känna hela organisationen!  Inom teamet komemr du ansvara för utvecklingen av styrsystemen i våra flödesmätare vilket innebär att arbeta med ADC, DAC, olika sensorer samt mätteknik och data visualisering t.ex OLED/TFT display. Flödesmätare kan kopplas upp mot andra system via industriella kommunikationsprotokoll t.ex 4-20mA, HART, IO-link och modbus. Som utvecklare fokuserar du främst på nyutveckling, vidareutveckling och arbetar med allt ifrån drivrutiner och kommunikation till UI.  VI SÖKER DIG SOM  - Har en utbildning från universitet eller högskola inom relevant område, exempelvis datavetenskap, mekatronik eller elektronik - Har erfarenhet av mjukvaruutveckling i C, C# och C++ - Har goda kunskaper i Engelska och Svenska, i både tal och skrift.  Vidare ser vi det som mycket meriterande om du har erfarenhet av test, kunskap inom sensorer för tryck och flödesmätning samt om du har erfarenhet av industriella kommunikationsprotokoll och IoT.  Som person är du   * Ansvarstagande * Strukturerad * Samarbetsorienterad   Övrig information   * Start: Så snart som möjligt * Omfattning: Heltid, tillsvidare * Placering: Huddinge, Stockholm   Rekryteringsprocessen hanteras av Academic Work och kundens önskemål är att alla frågor rörande tjänsten hanteras av Academic Work.  Vi går igenom urvalet löpande och annonsen kan stängas ner innan tjänsten är tillsatt om vi gått över till urvals- och intervjufasen.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Embedded** utvecklare till familjärt bol... |  | x |  | 8 | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| 1 | **Embedded** utvecklare till familjärt bol... |  | x |  | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 2 | **Embedded utvecklare** till familjärt bolag i tillvä... | x |  |  | 19 | [Embeddedutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/sEFD_3Zs_WiA) |
| 3 | ...det!? De letar nu efter en ny **programmerare** som ska jobba med inbyggda sy... | x |  |  | 13 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 4 | ...ogrammerare som ska jobba med **inbyggda system** och du kan bli deras nya meda... | x | x | 15 | 15 | [Inbyggda system/Embedded system, **skill**](http://data.jobtechdev.se/taxonomy/concept/XThy_HCH_FFc) |
| 5 | ...h produkter att hitta sin nya **programmerare**. Företaget, som är svenskgrun... | x |  |  | 13 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 6 | ... team i nära samarbete med en **elektronikingenjör** och en mekanikingenjör. Efter... | x |  |  | 18 | [Ingenjörer och tekniker, elektronik, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Fq5X_uPr_YXt) |
| 7 | ... en elektronikingenjör och en **mekanik**ingenjör. Eftersom bolaget i S... | x |  |  | 7 | [Mekanik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/LCib_ueo_f6w) |
| 8 | ... en elektronikingenjör och en **mekanikingenjör**. Eftersom bolaget i Sverige h... |  | x |  | 15 | [Mekanikingenjör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2Tag_Vi7_ZYD) |
| 9 | ...ktronikingenjör och en mekanik**ingenjör**. Eftersom bolaget i Sverige h... | x |  |  | 8 | [Ingenjörer och tekniker, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DhEP_m8M_qoi) |
| 10 | ...kingenjör. Eftersom bolaget i **Sverige** har ca 20 anställda kommer du... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 11 | ...ADC, DAC, olika sensorer samt **mätteknik** och data visualisering t.ex O... | x | x | 9 | 9 | [Mätteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/wZpC_X9a_xzG) |
| 12 | ...a sensorer samt mätteknik och **data** visualisering t.ex OLED/TFT d... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 13 | ...andra system via industriella **kommunikationsprotokoll** t.ex 4-20mA, HART, IO-link oc... | x | x | 23 | 23 | [Kommunikationsprotokoll, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/QkYc_pTH_tDk) |
| 14 | ...ng och arbetar med allt ifrån **drivrutiner** och kommunikation till UI.  V... |  | x |  | 11 | [utveckla drivrutin till IT-enhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VAcE_Kej_sj4) |
| 15 | ....  VI SÖKER DIG SOM  - Har en **utbildning** från universitet eller högsko... | x |  |  | 10 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 16 | ...SOM  - Har en utbildning från **universitet eller högskola** inom relevant område, exempel... | x |  |  | 26 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 17 | ...m relevant område, exempelvis **datavetenskap**, mekatronik eller elektronik ... | x | x | 13 | 13 | [datavetenskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VxiM_uBZ_V4d) |
| 18 | ...de, exempelvis datavetenskap, **mekatronik** eller elektronik - Har erfare... | x | x | 10 | 10 | [mekatronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uKr9_bDT_KTc) |
| 19 | ...tavetenskap, mekatronik eller **elektronik** - Har erfarenhet av mjukvaruu... |  | x |  | 10 | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| 20 | ...lektronik - Har erfarenhet av **mjukvaruutveckling** i C, C# och C++ - Har goda ku... | x | x | 18 | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 21 | ...enhet av mjukvaruutveckling i **C**, C# och C++ - Har goda kunska... | x |  |  | 1 | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| 22 | ...et av mjukvaruutveckling i C, **C#** och C++ - Har goda kunskaper ... | x | x | 2 | 2 | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| 23 | ...jukvaruutveckling i C, C# och **C++** - Har goda kunskaper i Engels... | x | x | 3 | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 24 | ...ch C++ - Har goda kunskaper i **Engelska** och Svenska, i både tal och s... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 25 | ...goda kunskaper i Engelska och **Svenska**, i både tal och skrift.  Vida... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 26 | ...ar erfarenhet av industriella **kommunikationsprotokoll** och IoT.  Som person är du   ... | x | x | 23 | 23 | [Kommunikationsprotokoll, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/QkYc_pTH_tDk) |
| 27 | ...a kommunikationsprotokoll och **IoT**.  Som person är du   * Ansvar... | x | x | 3 | 3 | [Internet of things/IoT, **skill**](http://data.jobtechdev.se/taxonomy/concept/v1kW_h4M_Kej) |
| 28 | ...ch IoT.  Som person är du   * **Ansvarstagande** * Strukturerad * Samarbetsori... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 29 | ...svarstagande * Strukturerad * **Samarbetsorienterad**   Övrig information   * Start... |  | x |  | 19 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 30 | ...art som möjligt * Omfattning: **Heltid**, tillsvidare * Placering: Hud... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 31 | ...möjligt * Omfattning: Heltid, **tillsvidare** * Placering: Huddinge, Stockh... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 32 | ...tid, tillsvidare * Placering: **Huddinge**, Stockholm   Rekryteringsproc... | x | x | 8 | 8 | [Huddinge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g1Gc_aXK_EKu) |
| 33 | ...vidare * Placering: Huddinge, **Stockholm**   Rekryteringsprocessen hante... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 33 | ...vidare * Placering: Huddinge, **Stockholm**   Rekryteringsprocessen hante... | x |  |  | 9 | [Stockholms län, **region**](http://data.jobtechdev.se/taxonomy/concept/CifL_Rzy_Mku) |
| | **Overall** | | | **156** | **388** | 156/388 = **40%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
|  | x |  | [Mekanikingenjör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2Tag_Vi7_ZYD) |
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
|  | x |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Stockholms län, **region**](http://data.jobtechdev.se/taxonomy/concept/CifL_Rzy_Mku) |
| x |  |  | [Ingenjörer och tekniker, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DhEP_m8M_qoi) |
| x |  |  | [Ingenjörer och tekniker, elektronik, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Fq5X_uPr_YXt) |
| x |  |  | [Mekanik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/LCib_ueo_f6w) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Kommunikationsprotokoll, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/QkYc_pTH_tDk) |
|  | x |  | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
|  | x |  | [utveckla drivrutin till IT-enhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VAcE_Kej_sj4) |
| x | x | x | [datavetenskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VxiM_uBZ_V4d) |
|  | x |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Inbyggda system/Embedded system, **skill**](http://data.jobtechdev.se/taxonomy/concept/XThy_HCH_FFc) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| x | x | x | [Huddinge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g1Gc_aXK_EKu) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| x |  |  | [Embeddedutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/sEFD_3Zs_WiA) |
| x | x | x | [mekatronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uKr9_bDT_KTc) |
| x | x | x | [Internet of things/IoT, **skill**](http://data.jobtechdev.se/taxonomy/concept/v1kW_h4M_Kej) |
| x | x | x | [Mätteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/wZpC_X9a_xzG) |
|  | x |  | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| x | x | x | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **13** | 13/32 = **41%** |