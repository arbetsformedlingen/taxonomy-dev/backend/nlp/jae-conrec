# Results for 'b8a1ad018bec478d0ae0c39ce2c477b8f620981b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b8a1ad018bec478d0ae0c39ce2c477b8f620981b](README.md) | 1 | 1863 | 14 | 5 | 53/164 = **32%** | 4/10 = **40%** |

## Source text

UX Design - Front Endutveckling till Gröna Lund Våra webbar utvecklas fort och under parollen ”IT i gästens hand” blir webbar och appar ett stöd för våra gäster att få ut det mesta av sitt besök hos oss.  Gröna Lund söker nu en front endutvecklare med kompetens inom React och Javascript. Du har vana av att utveckla webbplatser, har öga för både design och UX, och vill vara med och påverka vår digitala gästupplevelse. Har du tänkt ”Varför kan jag inte göra det här online på Grönan? Jag skulle vilja göra det så här…” så finns det här en plats för dig att utveckla och utvecklas.  Du kan ta ett tänkt flöde via UX-skisser och feedbackrundor till att delta i bygget av den färdiga lösningen. Du har en stark känsla för våra gästers upplevelse av våra digitala kanaler och brinner för att skapa en härlig digital upplevelse.  Som utvecklare hos oss arbetar du för Parks and Resorts alla fyra parker; Gröna Lund, Kolmården, Furuviksparken och Skara Sommarland. Din arbetsplats blir på Gröna Lund på Djurgården. Webbarna är byggda med React med stöd av bl.a. Contentful och Gatsby.     Vi erbjuder dig  En arbetsplats där glädje och upplevelse står i centrum  Arbete med inspirerande webbar och roliga produkter  Samarbete både med utvecklarkollegor och många andra kompetenser i de fyra parkerna     Vem är du?  Kunnig inom frontend-utveckling med fokus på Javascript  En stark UX/UI-designer där gästens upplevelse är viktig  Social – Gillar samarbete och diskussion  Självgående  Engagerad och har mycket energi     Information  Tjänsten är en tillsvidareanställnig, vi tillämpar 6 månaders provanställning. Sista ansökningsdag är den 31 augusti,  urval sker löpande och tjänsten kan komma att tillsättas innan sista ansökningsdag, så skicka in din ansökan redan idag.  Vid frågor om tjänsten, kontakta vår CIO Jan Eriksson på  jan.eriksson@parksandresorts.com.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **UX Design** - Front Endutveckling till Gr... | x |  |  | 9 | [UX design, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PTYG_goE_TxP) |
| 2 | UX Design - **Front Endutveckling** till Gröna Lund Våra webbar u... | x |  |  | 19 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 3 | ...klas fort och under parollen ”**IT** i gästens hand” blir webbar o... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 4 | ... oss.  Gröna Lund söker nu en **front endutvecklare** med kompetens inom React och ... | x |  |  | 19 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 5 | ... med kompetens inom React och **Javascript**. Du har vana av att utveckla ... | x | x | 10 | 10 | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
| 6 | ..., har öga för både design och **UX**, och vill vara med och påverk... | x |  |  | 2 | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| 7 | ...a     Vem är du?  Kunnig inom **frontend-utveckling** med fokus på Javascript  En s... | x | x | 19 | 19 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 8 | ...ntend-utveckling med fokus på **Javascript**  En stark UX/UI-designer där ... | x | x | 10 | 10 | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
| 9 | ...fokus på Javascript  En stark **UX**/UI-designer där gästens upple... | x |  |  | 2 | [UX designer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/BARa_87y_Zpb) |
| 10 | ...us på Javascript  En stark UX/**UI-designer** där gästens upplevelse är vik... | x | x | 11 | 11 | [UI-designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3GKG_7xX_AcT) |
| 11 | ...lar samarbete och diskussion  **Självgående**  Engagerad och har mycket ene... | x |  |  | 11 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 12 | ...  Information  Tjänsten är en **tillsvidareanställnig**, vi tillämpar 6 månaders prov... | x |  |  | 21 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 13 | ...idareanställnig, vi tillämpar **6 månaders provanställning**. Sista ansökningsdag är den 3... | x |  |  | 26 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 14 | ...gor om tjänsten, kontakta vår **CIO** Jan Eriksson på  jan.eriksson... | x | x | 3 | 3 | [Chief Information Officer/CIO/IT-chef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cEY1_Bh8_URP) |
| | **Overall** | | | **53** | **164** | 53/164 = **32%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [UI-designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3GKG_7xX_AcT) |
| x |  |  | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| x |  |  | [UX designer, **job-title**](http://data.jobtechdev.se/taxonomy/concept/BARa_87y_Zpb) |
| x | x | x | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
| x | x | x | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [UX design, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PTYG_goE_TxP) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [Chief Information Officer/CIO/IT-chef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cEY1_Bh8_URP) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| | | **4** | 4/10 = **40%** |