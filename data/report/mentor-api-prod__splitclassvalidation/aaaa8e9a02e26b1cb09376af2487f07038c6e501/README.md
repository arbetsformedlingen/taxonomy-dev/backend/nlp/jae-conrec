# Results for 'aaaa8e9a02e26b1cb09376af2487f07038c6e501'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [aaaa8e9a02e26b1cb09376af2487f07038c6e501](README.md) | 1 | 2194 | 13 | 9 | 52/176 = **30%** | 4/15 = **27%** |

## Source text

Vill du jobba med att skapa fotorealistiska 3D-bilder av byggnader, inomhus- och utomhusmiljöer? Under den här tvååriga utbildningen lär du dig hantera olika program för visualisering, bildbehandling och animation. Efter examen kan du jobba som visualiserare på arkitektkontor eller byggföretag – i Sverige eller internationellt.    OM UTBILDNINGEN  Under utbildningen lär du dig bland annat att  • arbeta i marknadsledande visualiseringsprogram  • efterbearbeta i bildbehandlingsprogram  • hantera CAD-system, BIM-data och 3D-modeller för interaktiv visualisering  • skapa kreativ bildkommunikation, fotorealistiska bilder, animationer och interaktiva miljöer  • arbeta i skarpa projekt tillsammans med olika roller och kompetenser.    INNEHÅLLER EN STOR DEL LIA (PRAKTIK)  En stor del av utbildningen består av LIA (lärande i arbete), vilket är yrkeshögskolans version av praktik. Under LIA:n får du träning i yrkesrollen och kontakter i arbetslivet som mycket väl kan leda till jobb efter examen.    ATT ARBETA SOM VISUALISERARE  Som visualiserare inom arkitektur skapar du fotorealistiska tredimensionella bilder av byggnader och miljöer, vilket gör att arkitekter, ingenjörer och designer kan kommunicera idéer snabbare och lättare. Bilderna används vid skiss, planering och design och även i marknadsföring.  Visualisering gör att man kan se exakt hur en designidé kommer se ut i verkligheten och bilderna innehåller ofta detaljer såsom belysning, möblering, akustik och skuggor. De kan också vara interaktiva, vilket gör det möjligt att ”vandra runt” i en miljö och uppleva den från olika vinklar.    TÄNKBARA YRKESROLLER OCH ARBETSPLATSER  Efter avslutad utbildning kan du jobba som 3D-visualiserare, 3D-grafiker eller 3D-modellör. Din framtida arbetsplats kan vara på arkitektkontor, bygg- eller mäklarföretag. Du kan välja att jobba i Sverige eller internationellt.    KURSER  • Arkitektonisk visualisering 1, 85 p  • Arkitektonisk visualisering 2, 60 p  • Arkitektonisk visualisering 3, 35 p  • Arkitektur- och byggkunskap, 20 p  • CAD – Bygg, 20 p  • Examensarbete, 10 p  • Interaktiv grafik och animation, 40 p  • LIA, Lärande i arbete 1, 65 p  • LIA, Lärande i arbete 2, 65 p     

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...med att skapa fotorealistiska **3D-bilder** av byggnader, inomhus- och ut... | x |  |  | 9 | [3D, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hL9a_dxf_fBN) |
| 2 | ...utomhusmiljöer? Under den här **tvååriga utbildningen** lär du dig hantera olika prog... | x |  |  | 21 | (not found in taxonomy) |
| 3 | ...ka program för visualisering, **bildbehandling** och animation. Efter examen k... |  | x |  | 14 | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
| 4 | ...handling och animation. Efter **examen** kan du jobba som visualiserar... | x |  |  | 6 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 5 | ...tkontor eller byggföretag – i **Sverige** eller internationellt.    OM ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 6 | ...behandlingsprogram  • hantera **CAD-system**, BIM-data och 3D-modeller för... | x |  |  | 10 | [CAD-program, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/3SAB_gMy_c9b) |
| 7 | ...tera CAD-system, BIM-data och **3D-modeller** för interaktiv visualisering ... | x | x | 11 | 11 | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| 8 | ... väl kan leda till jobb efter **examen**.    ATT ARBETA SOM VISUALISER... | x |  |  | 6 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 9 | ...tt arkitekter, ingenjörer och **designer** kan kommunicera idéer snabbar... | x |  |  | 8 | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
| 10 | ...anering och design och även i **marknadsföring**.  Visualisering gör att man k... | x |  |  | 14 | [marknadsföring, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UXRx_guv_ZGc) |
| 11 | ...r såsom belysning, möblering, **akustik** och skuggor. De kan också var... |  | x |  | 7 | [Teknisk akustik, **skill**](http://data.jobtechdev.se/taxonomy/concept/9F3h_HNn_mtg) |
| 11 | ...r såsom belysning, möblering, **akustik** och skuggor. De kan också var... |  | x |  | 7 | [akustik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pzSz_jBn_Jo3) |
| 12 | ...d utbildning kan du jobba som **3D-visualiserare**, 3D-grafiker eller 3D-modellö... | x | x | 16 | 16 | [3d-visualiserare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/dwck_5Tf_yYy) |
| 13 | ...u jobba som 3D-visualiserare, **3D-grafiker** eller 3D-modellör. Din framti... |  | x |  | 11 | [samarbeta med ett filmredigeringsteam, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcVq_PHa_g7C) |
| 13 | ...u jobba som 3D-visualiserare, **3D-grafiker** eller 3D-modellör. Din framti... | x |  |  | 11 | [3D artist/3D-grafiker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eDCK_B3K_cqW) |
| 14 | ...ualiserare, 3D-grafiker eller **3D-modellör**. Din framtida arbetsplats kan... | x | x | 11 | 11 | [3D-modellör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/9oDx_7ex_ar2) |
| 15 | ...tag. Du kan välja att jobba i **Sverige** eller internationellt.    KUR... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **52** | **176** | 52/176 = **30%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [CAD-program, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/3SAB_gMy_c9b) |
| x |  |  | [Designer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/42SW_cUg_uq2) |
|  | x |  | [Teknisk akustik, **skill**](http://data.jobtechdev.se/taxonomy/concept/9F3h_HNn_mtg) |
| x | x | x | [3D-modellör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/9oDx_7ex_ar2) |
| x |  |  | (not found in taxonomy) |
| x |  |  | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| x |  |  | [marknadsföring, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UXRx_guv_ZGc) |
|  | x |  | [samarbeta med ett filmredigeringsteam, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcVq_PHa_g7C) |
| x | x | x | [3d-visualiserare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/dwck_5Tf_yYy) |
| x |  |  | [3D artist/3D-grafiker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eDCK_B3K_cqW) |
|  | x |  | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
| x |  |  | [3D, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hL9a_dxf_fBN) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [akustik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pzSz_jBn_Jo3) |
| x | x | x | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| | | **4** | 4/15 = **27%** |