# Results for '26a2128e1d6bd70c3a5e5800235cb0aec1c92b7e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [26a2128e1d6bd70c3a5e5800235cb0aec1c92b7e](README.md) | 1 | 3642 | 19 | 22 | 159/388 = **41%** | 11/21 = **52%** |

## Source text

Art director / Marknadskoordinator med en bredare roll Om jobbet MicroNät erbjuder dig en central och bred roll, på ett snabbväxande företag.  Den vi söker skall vara en vass AD/ Marknadskoordinator med känsla för grafisk formgivning.  Du skall vara sugen på att ta ett större ansvar och utveckla marknadsavdelningen.   Om MicroNät MicroNät är en branschledande leverantör av luftburet bredband och intresset för våra produkter och tjänster är stort runtom i landet. I dagsläget finns vi i Skellefteå i norr till Hedemora i söder. Just nu befinner vi oss i en spännande fas där vi expanderar kraftigt och för att kunna möta den stora efterfrågan söker vi nu en ny kollega.   Arbetsbeskrivning Din roll blir att ta fram allt löpande marknadsmaterial för både print och digital marknadsföring. Du har stora möjligheter att skapa och driva egna projekt. Vi har ett eget interntryckeri som vi har för avsikt att bygga ut. Vi vill bygga en studio för att spela in rörliga format och utveckla nya kommunikationsvägar.   Exempel på arbetsuppgifter - Skapa material för print. - Driva och skapa reklam för sociala medier. - Uppdatera webben och interna kanaler. - Bygga upp fotoarkiv. - Annonsproduktion web/print. - Skicka ut pressreleaser.  Kvalifikationer Vi söker dig som är en idéstark lagspelare som kan leverera allt från kommunikativa idéer till konkreta kampanjer. Vi letar efter en riktig doer som får saker att hända och inte är rädd för att utveckla nya idéer. Är du samtidigt en prestigelös problemlösare med hjärtat på rätt plats vill vi gärna att du ansöker.   Vi önskar att du har följande kompetenser och erfarenheter - Vana av att självständigt producera både print och digitalt format. - Dokumenterad och relevant högskoleutbildning eller motsvarande. inom reklam/marknadsföring. - Ett skarpt grafiskt öga och kreativt sinne i kombination med intresse av och kvalitet i det grafiska hantverket. - Genuint intresse för kommunikation, varumärkesstärkande likväl som säljdrivande. - Vana av att jobba i Adobe Suite. - Goda kunskaper i engelska. - Du kommunicerar obehindrat i tal och skrift på svenska.   Utöver ovan lägger vi stor vikt vid din personlighet.   Meriterande - Kunskaper inom webbdesign. - Kunskap inom prepress och rippar.     Vem är du? Vi lägger stor vikt vid dina personliga egenskaper och för att trivas hos oss är du en person som gillar innovation och bredd. Du behöver vara prestigelös, van att hugga i där det behövs och du drivs av att skapa/bygga upp nytt. Vi ser att du är idéstark och är van att driva ditt eget arbete samt är mån om att nå resultat. Vi tror även att du är hands on och du har en förmåga att verkställa idéer. Du behöver inte ha kunskap inom alla delar av marknadsföring men har en stark vilja och driv att lära dig!   Du erbjuds  En bred och central roll i ett snabbväxande företag med härlig kultur. Vi är mitt i en tillväxtresa med massor av spännande utmaningar framför oss.    Din ansökan Låter detta som rätt nästa steg för dig? Vi ser då fram emot din ansökan redan idag! Skicka in din ansökan på    Urval och intervjuer hanteras löpande, varför rollen kan komma att tillsättas före sista ansökningsdag. Vi har som krav för ansökan att man lämnar referenser/portfolio.    Örnsköldsviksbaserade MicroNät har sedan 2010 byggt trådlösa bredbandsnät och finns idag representerade från Skellefteå i norr till Hedemora i Dalarna i söder. Förutom sin produkt Radio-Fiber för privatkonsumenter, tillhandahåller MicroNät en teknik speciellt anpassad för företag och lägenhetshus i stadskärnor och tätorter, 6XG, som levererar hastighet upp till 1000 Mbit/s.  Krav:  Körkort: * B (Personbil)

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Art director** / Marknadskoordinator med en ... | x | x | 12 | 12 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 2 | Art director / **Marknadskoordinator** med en bredare roll Om jobbet... |  | x |  | 19 | [Marknadskoordinator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3abF_Jyu_WtS) |
| 2 | Art director / **Marknadskoordinator** med en bredare roll Om jobbet... |  | x |  | 19 | [Marknadssamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/8sEx_nbw_S8T) |
| 2 | Art director / **Marknadskoordinator** med en bredare roll Om jobbet... | x | x | 19 | 19 | [marknadskoordinator, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/fjxD_KNy_FrF) |
| 3 | ...n vi söker skall vara en vass **AD**/ Marknadskoordinator med käns... | x | x | 2 | 2 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 4 | ... söker skall vara en vass AD/ **Marknadskoordinator** med känsla för grafisk formgi... |  | x |  | 19 | [Marknadskoordinator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3abF_Jyu_WtS) |
| 4 | ... söker skall vara en vass AD/ **Marknadskoordinator** med känsla för grafisk formgi... |  | x |  | 19 | [Marknadssamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/8sEx_nbw_S8T) |
| 4 | ... söker skall vara en vass AD/ **Marknadskoordinator** med känsla för grafisk formgi... | x | x | 19 | 19 | [marknadskoordinator, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/fjxD_KNy_FrF) |
| 5 | ...adskoordinator med känsla för **grafisk formgivning**.  Du skall vara sugen på att ... | x | x | 19 | 19 | [Grafisk formgivare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/VpYo_k5f_N9R) |
| 6 | ...andet. I dagsläget finns vi i **Skellefteå** i norr till Hedemora i söder.... | x | x | 10 | 10 | [Skellefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kicB_LgH_2Dk) |
| 7 | ...s vi i Skellefteå i norr till **Hedemora** i söder. Just nu befinner vi ... | x | x | 8 | 8 | [Hedemora, **municipality**](http://data.jobtechdev.se/taxonomy/concept/DE9u_V4K_Z1S) |
| 8 | ...dsmaterial för både print och **digital marknadsföring**. Du har stora möjligheter att... | x | x | 22 | 22 | [Digital marknadsföring/Onlinemarknadsföring, **skill**](http://data.jobtechdev.se/taxonomy/concept/Lz84_K5d_CcL) |
| 9 | ...ler. - Bygga upp fotoarkiv. - **Annonsproduktion** web/print. - Skicka ut pressr... |  | x |  | 16 | [Annonsproduktion, design, **skill**](http://data.jobtechdev.se/taxonomy/concept/VBn4_Qqi_CHB) |
| 9 | ...ler. - Bygga upp fotoarkiv. - **Annonsproduktion** web/print. - Skicka ut pressr... | x | x | 16 | 16 | [Annonsproduktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/nvM6_J6v_qKQ) |
| 10 | ...ch erfarenheter - Vana av att **självständigt** producera både print och digi... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 11 | .... - Dokumenterad och relevant **högskoleutbildning** eller motsvarande. inom rekla... | x |  |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 11 | .... - Dokumenterad och relevant **högskoleutbildning** eller motsvarande. inom rekla... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 12 | ...ller motsvarande. inom reklam/**marknadsföring**. - Ett skarpt grafiskt öga oc... | x |  |  | 14 | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| 13 | ...erket. - Genuint intresse för **kommunikation**, varumärkesstärkande likväl s... | x |  |  | 13 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 14 | ...obe Suite. - Goda kunskaper i **engelska**. - Du kommunicerar obehindrat... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 15 | ...behindrat i tal och skrift på **svenska**.   Utöver ovan lägger vi stor... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 16 | ... Meriterande - Kunskaper inom **webbdesign**. - Kunskap inom prepress och ... | x | x | 10 | 10 | [Webbdesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/YNXq_yH4_fhK) |
| 17 | ...ha kunskap inom alla delar av **marknadsföring** men har en stark vilja och dr... | x |  |  | 14 | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| 18 | ...inns idag representerade från **Skellefteå** i norr till Hedemora i Dalarn... |  | x |  | 10 | [Skellefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kicB_LgH_2Dk) |
| 19 | ...e från Skellefteå i norr till **Hedemora** i Dalarna i söder. Förutom si... |  | x |  | 8 | [Hedemora, **municipality**](http://data.jobtechdev.se/taxonomy/concept/DE9u_V4K_Z1S) |
| 20 | ...lefteå i norr till Hedemora i **Dalarna** i söder. Förutom sin produkt ... |  | x |  | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 21 | ... i söder. Förutom sin produkt **Radio**-Fiber för privatkonsumenter, ... | x |  |  | 5 | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
| 22 | ...p till 1000 Mbit/s.  Krav:  **Körkort**: * B (Personbil) | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 23 | ...1000 Mbit/s.  Krav:  Körkort**: * B (Personbil)** | x |  |  | 17 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| | **Overall** | | | **159** | **388** | 159/388 = **41%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Marknadskoordinator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3abF_Jyu_WtS) |
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| x |  |  | [Radio, **skill**](http://data.jobtechdev.se/taxonomy/concept/5T5g_ij5_JUC) |
|  | x |  | [Marknadssamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/8sEx_nbw_S8T) |
| x | x | x | [Hedemora, **municipality**](http://data.jobtechdev.se/taxonomy/concept/DE9u_V4K_Z1S) |
| x | x | x | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| x | x | x | [Digital marknadsföring/Onlinemarknadsföring, **skill**](http://data.jobtechdev.se/taxonomy/concept/Lz84_K5d_CcL) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Annonsproduktion, design, **skill**](http://data.jobtechdev.se/taxonomy/concept/VBn4_Qqi_CHB) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Grafisk formgivare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/VpYo_k5f_N9R) |
| x | x | x | [Webbdesign, **skill**](http://data.jobtechdev.se/taxonomy/concept/YNXq_yH4_fhK) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x | x | x | [marknadskoordinator, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/fjxD_KNy_FrF) |
| x | x | x | [Skellefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kicB_LgH_2Dk) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x | x | x | [Annonsproduktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/nvM6_J6v_qKQ) |
|  | x |  | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x |  |  | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| | | **11** | 11/21 = **52%** |