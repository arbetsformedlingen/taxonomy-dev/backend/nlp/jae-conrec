# Results for '9491fcb5567f9ac058fc97b72dd249bbb2bd64d9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9491fcb5567f9ac058fc97b72dd249bbb2bd64d9](README.md) | 1 | 2485 | 21 | 8 | 49/265 = **18%** | 4/20 = **20%** |

## Source text

Purchasing Specialist to energy company We are looking for a Purchasing Specialist to a growing energy company with start as soon as possible. This is assignment will give you great experience from a fast growing tech company and the opportunity to work with a motivated team in Stockholm.  OM TJÄNSTEN  The electrification of cars, trucks and buses is one of the most extensive and disruptive industry transformations ever. Our client leads this change and aims to become the leading battery partner for the European automotive industry and set new benchmarks for sustainability and CO2 footprint.  As a Purchasing Specialist you have an exciting opportunity to work across all procurement for all entities. You enable our stakeholders with strategic and tactical choices while being cost-effective. This position is responsible for early and ongoing engagement with internal stakeholders and external suppliers from specifications to sourcing to delivery. You will get the chance to have a real impact on our success story in Sweden and see the immediate results of your work.  This is primarily a temporary position for 6 months starting immediately, but as the company grows, there are good opportunities for extension.  You are offered   * As a consultant at Academic Work you are offered a great opportunity to grow as a professional, extend your network and establish valuable contacts for the future. Read more about our offer   ARBETSUPPGIFTER  - Creation in Purchase Orders in MS Dynamics - Follow-up on Scheduling and Expediting of placed Purchase Orders - Negotiation of Delivery Terms of MRO Frame Agreements - Occasional Travel to Skelleftea Production Site (once a month)  VI SÖKER DIG SOM  - Have at least one year of working experience from purchasing - Great experience in working with Dynamics365 - Proficient in Microsoft Office with knowledge of Excel - Fluent in English, Swedish is a plus  It is highly preferable if you have:   * Bachelor's degree witihin related field * Knowledge within E-procurement   Other information   * Start: As soon as possible * Work extent: Full time * Location: Stockholm, Kungsholmen * Contact information: This recruitment process is conducted by Academic Work. It is a request from client that all questions regarding the position are handled by Academic Work.   Our selection process is continuous and the advert may close before the recruitment process is completed if we have moved forward to the screening or interview phase.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Purchasing Specialist to **energy** company We are looking for a ... | x |  |  | 6 | [Energifrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/hLqS_EHC_3GK) |
| 2 | ... company We are looking for a **Purchasing Specialist** to a growing energy company w... | x |  |  | 21 | [Inköpsspecialist, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yAKG_f8F_btt) |
| 3 | ...asing Specialist to a growing **energy** company with start as soon as... | x |  |  | 6 | [Energifrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/hLqS_EHC_3GK) |
| 4 | ...work with a motivated team in **Stockholm**.  OM TJÄNSTEN  The electrific... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 5 | ...NSTEN  The electrification of **cars**, trucks and buses is one of t... | x |  |  | 4 | [Bil, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xkUP_ZNx_aU4) |
| 6 | ... The electrification of cars, **trucks** and buses is one of the most ... | x |  |  | 6 | [Lastbil, **keyword**](http://data.jobtechdev.se/taxonomy/concept/SAb5_TAd_ctX) |
| 7 | ...rucks and buses is one of the **most** extensive and disruptive indu... |  | x |  | 4 | [MOST, kommunikationsprotokoll, **skill**](http://data.jobtechdev.se/taxonomy/concept/ob2k_Bcp_XHY) |
| 8 | ...try transformations ever. Our **client** leads this change and aims to... |  | x |  | 6 | [Client, programmering/Server, programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bw3F_U6s_ZeC) |
| 9 | ...ading battery partner for the **European** automotive industry and set n... | x |  |  | 8 | [Europa/EU/EES/EURES, **continent**](http://data.jobtechdev.se/taxonomy/concept/HstV_nCB_W2i) |
| 10 | ...tery partner for the European **automotive** industry and set new benchmar... | x | x | 10 | 10 | [Automotive/Fordonsindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/jnLy_6Ns_KpH) |
| 11 | ...lity and CO2 footprint.  As a **Purchasing Specialist** you have an exciting opportun... | x |  |  | 21 | [Inköpsspecialist, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yAKG_f8F_btt) |
| 12 | ...mpact on our success story in **Sweden** and see the immediate results... | x |  |  | 6 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 13 | ...ur work.  This is primarily a **temporary position for 6 months** starting immediately, but as ... | x |  |  | 31 | [Tidsbegränsad anställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/sTu5_NBQ_udq) |
| 14 | ...reation in Purchase Orders in **MS Dynamics** - Follow-up on Scheduling and... | x |  |  | 11 | [Microsoft Dynamics NAV, affärssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Ygao_Vsn_eJ2) |
| 15 | ...ements - Occasional Travel to **Skelleftea** Production Site (once a month... | x |  |  | 10 | [Skellefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kicB_LgH_2Dk) |
| 16 | ...ÖKER DIG SOM  - Have at least **one year of working experience** from purchasing - Great exper... | x |  |  | 30 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| 17 | ...h Dynamics365 - Proficient in **Microsoft Office** with knowledge of Excel - Flu... | x | x | 16 | 16 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 18 | ...soft Office with knowledge of **Excel** - Fluent in English, Swedish ... | x | x | 5 | 5 | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| 19 | ...nowledge of Excel - Fluent in **English**, Swedish is a plus  It is hig... | x |  |  | 7 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 20 | ...of Excel - Fluent in English, **Swedish** is a plus  It is highly prefe... | x |  |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 21 | ...y preferable if you have:   * **Bachelor's degree** witihin related field * Knowl... | x |  |  | 17 | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| 22 | ...on as possible * Work extent: **Full time** * Location: Stockholm, Kungsh... | x |  |  | 9 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 23 | ...extent: Full time * Location: **Stockholm**, Kungsholmen * Contact inform... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 24 | ...ic Work. It is a request from **client** that all questions regarding ... |  | x |  | 6 | [Client, programmering/Server, programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bw3F_U6s_ZeC) |
| | **Overall** | | | **49** | **265** | 49/265 = **18%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [Client, programmering/Server, programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bw3F_U6s_ZeC) |
| x |  |  | [Europa/EU/EES/EURES, **continent**](http://data.jobtechdev.se/taxonomy/concept/HstV_nCB_W2i) |
| x |  |  | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| x |  |  | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [Lastbil, **keyword**](http://data.jobtechdev.se/taxonomy/concept/SAb5_TAd_ctX) |
| x |  |  | [Microsoft Dynamics NAV, affärssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Ygao_Vsn_eJ2) |
| x |  |  | [Energifrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/hLqS_EHC_3GK) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Automotive/Fordonsindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/jnLy_6Ns_KpH) |
| x |  |  | [Skellefteå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kicB_LgH_2Dk) |
|  | x |  | [MOST, kommunikationsprotokoll, **skill**](http://data.jobtechdev.se/taxonomy/concept/ob2k_Bcp_XHY) |
| x |  |  | [Tidsbegränsad anställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/sTu5_NBQ_udq) |
| x |  |  | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| x | x | x | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| x |  |  | [Bil, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xkUP_ZNx_aU4) |
| x |  |  | [Inköpsspecialist, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yAKG_f8F_btt) |
| x |  |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/20 = **20%** |