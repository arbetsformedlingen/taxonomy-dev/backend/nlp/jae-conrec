# Results for 'd579e8b5b29ce29de680b49b906c0b3f284683b3'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d579e8b5b29ce29de680b49b906c0b3f284683b3](README.md) | 1 | 1470 | 10 | 7 | 46/76 = **61%** | 6/10 = **60%** |

## Source text

Städerska deltid Vår kollega slutar efter många år hos oss. Därför behöver vi nu rekrytera. Arbetsplatsen kommer att vara ute hos våra kunder runt om i Skaraborg. Du ska gärna bo i närområdet av Skövde eller Tidaholm. Vårt mål är att alla ska ha råd med hemstäd till rimlig kostnad. Vi är ett litet företag, och vi anpassar oss till varje kund. För oss är det viktigt att kunderna känner sig nöjda och trygga, därför kommer du ha fasta kunder som du tar hand om. Vi söker en driven och noggrann person som har servicekänsla. Du ska gärna ha erfarenhet, men ej nödvändigt då vi kommer lära upp dig. Det är viktigt att du är ordningsam, flexibel och har sociala kompetenser för möten med kunder. Ditt arbete kommer att bestå i att städa i privata hem och på mindre företag. Du kommer arbeta efter schema. Då du kommer träffa och tala med många av våra kunder, är det viktigt att du kan behärska svenska eller engelska väl för kundkontakter. Eftersom arbetet i huvudsak kommer att vara tisdagar - fredagar, men även vissa måndagar vid behov. Arbetsuppgifterna består i att städa i privata hem, på mindre företag eller flyttstäd. Du kommer arbeta enligt fast schema. Varannan fredag är det möte med de anställda där schema gås igenom och där eventuella ändringar görs. Arbetet gäller i första hand på tisdagar - fredagar. Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Städerska** deltid Vår kollega slutar eft... | x | x | 9 | 9 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 2 | Städerska **deltid** Vår kollega slutar efter mång... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 3 | ... ska gärna bo i närområdet av **Skövde** eller Tidaholm. Vårt mål är a... | x | x | 6 | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 4 | ... i närområdet av Skövde eller **Tidaholm**. Vårt mål är att alla ska ha ... | x | x | 8 | 8 | [Tidaholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Zsf5_vpP_Bs4) |
| 5 | ...ål är att alla ska ha råd med **hemstäd** till rimlig kostnad. Vi är et... | x |  |  | 7 | [Hemstädare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/TYBE_rFF_6cD) |
| 5 | ...ål är att alla ska ha råd med **hemstäd** till rimlig kostnad. Vi är et... |  | x |  | 7 | [Städtjänster, **skill**](http://data.jobtechdev.se/taxonomy/concept/fZdk_vbt_LhP) |
| 6 | ...nd om. Vi söker en driven och **noggrann** person som har servicekänsla.... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 7 | ...arbete kommer att bestå i att **städa** i privata hem och på mindre f... | x |  |  | 5 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 8 | ...t viktigt att du kan behärska **svenska** eller engelska väl för kundko... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 9 | ...du kan behärska svenska eller **engelska** väl för kundkontakter. Efters... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 10 | ...rbetsuppgifterna består i att **städa** i privata hem, på mindre före... | x |  |  | 5 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| | **Overall** | | | **46** | **76** | 46/76 = **61%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [Hemstädare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/TYBE_rFF_6cD) |
| x | x | x | [Tidaholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Zsf5_vpP_Bs4) |
| x | x | x | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
|  | x |  | [Städtjänster, **skill**](http://data.jobtechdev.se/taxonomy/concept/fZdk_vbt_LhP) |
| x | x | x | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/10 = **60%** |