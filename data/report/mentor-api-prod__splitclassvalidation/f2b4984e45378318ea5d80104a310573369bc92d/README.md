# Results for 'f2b4984e45378318ea5d80104a310573369bc92d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f2b4984e45378318ea5d80104a310573369bc92d](README.md) | 1 | 680 | 12 | 13 | 56/213 = **26%** | 3/15 = **20%** |

## Source text

Kylmontör/Kyltekniker Klimatteknik är idag ett mindre familjeföretag som växer och behöver bli fler!  Vi söker en kylmontör/kyltekniker. Vi jobbar med allt från mindre kylanläggningar till värmepumpar. Krav: B-körkort Flytande i Svenska i både tal och skrivit Någon typ av tidigare erfarenhet av värmepumpar, vvs eller kyla Praktiskt lagd Datavana, gärna i SDF  Meriternade: Kylcertifakt cat 1 Heta arbeten Lödprov Tidigare arbete med Mitsubishi Electric   Vi erbjuder dig ett väldigt fritt arbete med bra villkor och ett trevligt arbetsklimat.  Vi intervjuar personer löpande så vänta inte med att höra av dig! Kontakt person: Robin Gladh, 070 349 46 15  robin@klimattekniksyd.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kylmontör**/Kyltekniker Klimatteknik är i... |  | x |  | 9 | [Kylmontörer och kyltekniker, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/9BVv_nt4_8eT) |
| 1 | **Kylmontör**/Kyltekniker Klimatteknik är i... | x | x | 9 | 9 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 1 | **Kylmontör**/Kyltekniker Klimatteknik är i... |  | x |  | 9 | [Kyl- och värmepumpsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WPYe_qfi_s8P) |
| 2 | Kylmontör**/**Kyltekniker Klimatteknik är id... | x |  |  | 1 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 3 | Kylmontör/**Kyltekniker** Klimatteknik är idag ett mind... | x | x | 11 | 11 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 4 | Kylmontör/Kyltekniker **Klimatteknik** är idag ett mindre familjeför... |  | x |  | 12 | [Klimatteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/VvvT_DJ3_aPH) |
| 5 | ...ehöver bli fler!  Vi söker en **kylmontör**/kyltekniker. Vi jobbar med al... |  | x |  | 9 | [Kylmontörer och kyltekniker, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/9BVv_nt4_8eT) |
| 5 | ...ehöver bli fler!  Vi söker en **kylmontör**/kyltekniker. Vi jobbar med al... | x | x | 9 | 9 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 5 | ...ehöver bli fler!  Vi söker en **kylmontör**/kyltekniker. Vi jobbar med al... |  | x |  | 9 | [Kyl- och värmepumpsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WPYe_qfi_s8P) |
| 6 | ...i fler!  Vi söker en kylmontör**/**kyltekniker. Vi jobbar med all... | x |  |  | 1 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 7 | ... fler!  Vi söker en kylmontör/**kyltekniker**. Vi jobbar med allt från mind... | x | x | 11 | 11 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 8 | ...i jobbar med allt från mindre **kylanläggningar** till värmepumpar. Krav: B-kör... |  | x |  | 15 | [Kylanläggning, underhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/fjoN_ZHr_XG4) |
| 8 | ...i jobbar med allt från mindre **kylanläggningar** till värmepumpar. Krav: B-kör... |  | x |  | 15 | [Kylmaskinist, kyl- och frysanläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zx5U_1JE_XtB) |
| 9 | ...ingar till värmepumpar. Krav: **B-körkort** Flytande i Svenska i både tal... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 10 | ...r. Krav: B-körkort Flytande i **Svenska** i både tal och skrivit Någon ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 11 | ...skrivit Någon typ av tidigare **erfarenhet av värmepumpar**, vvs eller kyla Praktiskt lag... | x |  |  | 25 | [Installera värmepumpar, **skill**](http://data.jobtechdev.se/taxonomy/concept/uJoK_dM3_8AM) |
| 12 | ...re erfarenhet av värmepumpar, **vvs** eller kyla Praktiskt lagd Dat... | x |  |  | 3 | [VVS, pumpteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/q25B_p3f_8PD) |
| 13 | ...het av värmepumpar, vvs eller **kyla** Praktiskt lagd Datavana, gärn... | x |  |  | 4 | [Kylteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/11L4_Q2Z_8tM) |
| 14 | ...vvs eller kyla Praktiskt lagd **Datavana**, gärna i SDF  Meriternade: Ky... | x |  |  | 8 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 15 | ...na, gärna i SDF  Meriternade: **Kylcertifakt cat 1** Heta arbeten Lödprov Tidigare... | x |  |  | 18 | [Kylcertifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/VWmQ_9fd_GLL) |
| 16 | ...riternade: Kylcertifakt cat 1 **Heta arbeten** Lödprov Tidigare arbete med M... | x |  |  | 12 | [Certifikat, heta arbeten, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q7eC_B7g_QiU) |
| 17 | ...lcertifakt cat 1 Heta arbeten **Lödprov** Tidigare arbete med Mitsubish... | x |  |  | 7 | [Lödcertifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/x7ih_HZS_VDB) |
| | **Overall** | | | **56** | **213** | 56/213 = **26%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Kylteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/11L4_Q2Z_8tM) |
|  | x |  | [Kylmontörer och kyltekniker, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/9BVv_nt4_8eT) |
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x | x | x | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| x |  |  | [Certifikat, heta arbeten, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q7eC_B7g_QiU) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Kylcertifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/VWmQ_9fd_GLL) |
|  | x |  | [Klimatteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/VvvT_DJ3_aPH) |
|  | x |  | [Kyl- och värmepumpsmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WPYe_qfi_s8P) |
|  | x |  | [Kylanläggning, underhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/fjoN_ZHr_XG4) |
| x |  |  | [VVS, pumpteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/q25B_p3f_8PD) |
| x |  |  | [Installera värmepumpar, **skill**](http://data.jobtechdev.se/taxonomy/concept/uJoK_dM3_8AM) |
| x |  |  | [Lödcertifikat, **skill**](http://data.jobtechdev.se/taxonomy/concept/x7ih_HZS_VDB) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
|  | x |  | [Kylmaskinist, kyl- och frysanläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zx5U_1JE_XtB) |
| | | **3** | 3/15 = **20%** |