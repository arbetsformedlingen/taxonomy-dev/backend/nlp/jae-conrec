# Results for '1d8aebbad421ff1a9d48566724defe6aecc7a652'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1d8aebbad421ff1a9d48566724defe6aecc7a652](README.md) | 1 | 1455 | 10 | 18 | 58/275 = **21%** | 4/18 = **22%** |

## Source text

Maskinoperatör, plåtbearbetning, kvällskift Hos oss arbetar du i en fräsch och modern plåtverkstad? Till våra stansmaskiner i Plåtverkstaden söker vi nu en maskinoperatör (kvällsskift). Du arbetar i en fräsch och modern verkstad. Företaget har hög investeringsvilja och i vår plåtverkstad har vi senaste åren genomfört stora investeringar i nya maskiner.  Du kommer att ingå i ett team om 3 personer där ni övervakar och ansvarar för att båda maskinerna går smidigt och producerar den mängd plåt som ligger enligt plan. Arbetsuppgifterna innebär också till viss del underhållsarbete, felsökning, service och reparationer. Arbetstiderna är måndag till torsdag 14.55-22.40 samt söndagar 7 timmar (valfri tid när dessa utförs) Vi söker dig som har erfarenhet från produktion eller industriell tillverkning, gärna inom plåtbearbetning. Det är meriterande om du tidigare har arbetat som maskinoperatör. Du är nyfiken på att lära dig nya saker och tar initiativ till att rycka in där det behövs i processen. Vi ser gärna att du har ett tekniskt intresse. Det viktigaste är att du är rätt person för att passa in i vårt lag och är engagerad och ansvarsfull. Flytande svenska och i tal och skrift är ett krav. Har du frågor om tjänsten är du välkommen att kontakta Sophie Pantsar, HR-chef, 0472-360 92. Vi vill gärna ha din ansökan så snart som möjligt, urval sker löpande, dock senast 2022-07-24. Skicka din ansökan märkt med ”Plåtverkstad” till personal@wica.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Maskinoperatör**, plåtbearbetning, kvällskift ... | x |  |  | 14 | [Process- och maskinoperatörer, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/XnLr_FQp_c82) |
| 1 | **Maskinoperatör**, plåtbearbetning, kvällskift ... |  | x |  | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 2 | Maskinoperatör, **plåtbearbetning**, kvällskift Hos oss arbetar d... | x | x | 15 | 15 | [Plåtbearbetning, **skill**](http://data.jobtechdev.se/taxonomy/concept/SrzF_GXp_pFE) |
| 3 | ...odern plåtverkstad? Till våra **stansmaskiner** i Plåtverkstaden söker vi nu ... |  | x |  | 13 | [sköta stansmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/C46y_Cdx_rnv) |
| 3 | ...odern plåtverkstad? Till våra **stansmaskiner** i Plåtverkstaden söker vi nu ... |  | x |  | 13 | [använda stansmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MMuh_d9u_myu) |
| 3 | ...odern plåtverkstad? Till våra **stansmaskiner** i Plåtverkstaden söker vi nu ... |  | x |  | 13 | [typer av stansmaskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Yseh_MV1_gub) |
| 4 | ...Plåtverkstaden söker vi nu en **maskinoperatör** (kvällsskift). Du arbetar i e... | x |  |  | 14 | [Process- och maskinoperatörer, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/XnLr_FQp_c82) |
| 4 | ...Plåtverkstaden söker vi nu en **maskinoperatör** (kvällsskift). Du arbetar i e... |  | x |  | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 5 | ...a innebär också till viss del **underhållsarbete**, felsökning, service och repa... | x |  |  | 16 | [underhålla maskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZtfT_5jT_4r1) |
| 6 | ...ll viss del underhållsarbete, **felsökning**, service och reparationer. Ar... | x | x | 10 | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 7 | ...arenhet från produktion eller **industriell tillverkning**, gärna inom plåtbearbetning. ... | x |  |  | 24 | [Industriell tillverkning, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/wTEr_CBC_bqh) |
| 8 | ... produktion eller industriell **tillverkning**, gärna inom plåtbearbetning. ... |  | x |  | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 9 | ...iell tillverkning, gärna inom **plåtbearbetning**. Det är meriterande om du tid... | x | x | 15 | 15 | [Plåtbearbetning, **skill**](http://data.jobtechdev.se/taxonomy/concept/SrzF_GXp_pFE) |
| 10 | ...m du tidigare har arbetat som **maskinoperatör**. Du är nyfiken på att lära di... | x |  |  | 14 | [Process- och maskinoperatörer, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/XnLr_FQp_c82) |
| 10 | ...m du tidigare har arbetat som **maskinoperatör**. Du är nyfiken på att lära di... |  | x |  | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 11 | ...vårt lag och är engagerad och **ansvarsfull**. Flytande svenska och i tal o... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 12 | ...rad och ansvarsfull. Flytande **svenska** och i tal och skrift är ett k... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 13 | ... att kontakta Sophie Pantsar, **HR-chef**, 0472-360 92. Vi vill gärna h... |  | x |  | 7 | [Personal- och HR-chefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Agy9_ifj_dB6) |
| 13 | ... att kontakta Sophie Pantsar, **HR-chef**, 0472-360 92. Vi vill gärna h... |  | x |  | 7 | [Personal- och HR-chefer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/mNoS_AN5_drn) |
| 13 | ... att kontakta Sophie Pantsar, **HR-chef**, 0472-360 92. Vi vill gärna h... |  | x |  | 7 | [Personal- och HR-chefer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/poaM_jLY_jjY) |
| 13 | ... att kontakta Sophie Pantsar, **HR-chef**, 0472-360 92. Vi vill gärna h... |  | x |  | 7 | [personalchef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/szzd_zs8_gr1) |
| 13 | ... att kontakta Sophie Pantsar, **HR-chef**, 0472-360 92. Vi vill gärna h... |  | x |  | 7 | [Personalchef, **job-title**](http://data.jobtechdev.se/taxonomy/concept/wiXH_S4Z_LuE) |
| 13 | ... att kontakta Sophie Pantsar, **HR-chef**, 0472-360 92. Vi vill gärna h... |  | x |  | 7 | [HR-chef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/y22E_Nz6_5VU) |
| | **Overall** | | | **58** | **275** | 58/275 = **21%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Personal- och HR-chefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Agy9_ifj_dB6) |
|  | x |  | [sköta stansmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/C46y_Cdx_rnv) |
| x | x | x | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
|  | x |  | [använda stansmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MMuh_d9u_myu) |
| x | x | x | [Plåtbearbetning, **skill**](http://data.jobtechdev.se/taxonomy/concept/SrzF_GXp_pFE) |
| x |  |  | [Process- och maskinoperatörer, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/XnLr_FQp_c82) |
|  | x |  | [typer av stansmaskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Yseh_MV1_gub) |
| x |  |  | [underhålla maskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZtfT_5jT_4r1) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
|  | x |  | [Personal- och HR-chefer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/mNoS_AN5_drn) |
|  | x |  | [Personal- och HR-chefer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/poaM_jLY_jjY) |
|  | x |  | [personalchef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/szzd_zs8_gr1) |
|  | x |  | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| x |  |  | [Industriell tillverkning, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/wTEr_CBC_bqh) |
|  | x |  | [Personalchef, **job-title**](http://data.jobtechdev.se/taxonomy/concept/wiXH_S4Z_LuE) |
|  | x |  | [HR-chef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/y22E_Nz6_5VU) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/18 = **22%** |