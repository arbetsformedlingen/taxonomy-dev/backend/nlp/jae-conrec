# Results for 'dd86d4c87cd36f3f5f76e61ba4fd781f3a72a641'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [dd86d4c87cd36f3f5f76e61ba4fd781f3a72a641](README.md) | 1 | 308 | 4 | 6 | 12/79 = **15%** | 2/5 = **40%** |

## Source text

Bilmekanik / Mekaniker / Mekanik Vi söker dig som är ansvarstagande, duktig och erfaren bilmekaniker. Du ska ha ambition och hålla dig uppdaterad om senaste tekniken. Du ska ha ett brinnande intresse för bilar och bilmekanik.   Arbetsuppgiften innefattar felsökning, service, och reparationer av olika bilar.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bilmekanik** / Mekaniker / Mekanik Vi söke... |  | x |  | 10 | [Felsökning, bilmekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/qQms_DVy_WhS) |
| 2 | Bil**mekanik** / Mekaniker / Mekanik Vi söke... |  | x |  | 7 | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| 3 | Bilmekanik / **Mekaniker** / Mekanik Vi söker dig som är... | x |  |  | 9 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 4 | Bilmekanik / Mekaniker / **Mekanik** Vi söker dig som är ansvarsta... | x |  |  | 7 | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| 5 | .../ Mekanik Vi söker dig som är **ansvarstagande**, duktig och erfaren bilmekani... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 6 | ...rstagande, duktig och erfaren **bilmekaniker**. Du ska ha ambition och hålla... | x | x | 12 | 12 | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
| 7 | ...nnande intresse för bilar och **bilmekanik**.   Arbetsuppgiften innefattar... |  | x |  | 10 | [Felsökning, bilmekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/qQms_DVy_WhS) |
| 8 | ...   Arbetsuppgiften innefattar **felsökning**, service, och reparationer av... |  | x |  | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| | **Overall** | | | **12** | **79** | 12/79 = **15%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| x | x | x | [Fordonstekniker/Bilmekaniker/Personbilsmekaniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bLKd_4Wg_3rn) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Felsökning, bilmekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/qQms_DVy_WhS) |
| x | x | x | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| | | **2** | 2/5 = **40%** |