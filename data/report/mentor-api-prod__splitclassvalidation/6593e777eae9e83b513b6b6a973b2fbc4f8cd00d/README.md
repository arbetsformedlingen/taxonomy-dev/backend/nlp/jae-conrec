# Results for '6593e777eae9e83b513b6b6a973b2fbc4f8cd00d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [6593e777eae9e83b513b6b6a973b2fbc4f8cd00d](README.md) | 1 | 2283 | 11 | 15 | 31/402 = **8%** | 2/21 = **10%** |

## Source text

Vi söker fler säljare på deltid, där du får lönen utbetald varje vecka! Om jobbet! Söker du ett extrajobb som du kan kombinera med skola eller liknande? Då har du hittat rätt! Vi på Uniq Dialog söker nu nya stjärnskott till vårt sälj-team. Du väljer själv ditt schema och får dessutom lön varje fredag! För att söka till oss behöver du nödvändigtvis inte ha erfarenhet från försäljning sedan tidigare men givetvis är det meriterande! Det absolut viktigaste för oss är att du som söker är social, motiverad, trevlig och behärskar det svenska språket i både tal och skrift. Skulle det vara så att du tidigare jobbat med försäljning så är det viktigt att förstå att försäljning bygger på relationer, inte på att en säljare argumenterar ner en kund. Våra arbetspass är måndag-torsdag 16:45-21:00 samt söndag 12:30-16:45. Du bokar själv ditt schema med vilka kvällar och helger du vill jobba och du får jobba hur mycket du vill utifrån de pass som vi erbjuder men minst 2 pass i veckan. Har du drivet, hungern och snacket för att kunna bli en av våra nya toppsäljare? Hör då av dig genom att skicka en ansökan till oss via vår hemsida. Där kan du även hitta ytterligare info om Malmös bästa arbetsplats för gymnasieelever! ______________________________________________________________________ Arbetet består av att erbjuda prenumerationsprodukter till kunder som sedan tidigare har en koppling till produkten, men kan även omfatta försäljning av tjänster, marknadsundersökningar eller mötesbokning. Du representerar alltid väldigt stora varumärken inom respektive segment. Kunderna som kontaktas har som sagt i de flesta fall använt produkten tidigare vilket gör att försäljningen bygger på en befintlig relation med kunden. De första dagarna kombinerar vi teori med praktik och du kommer att få lära dig allt du behöver för att sälja på alla typer av kundunderlag. ALLA som jobbar här är måna om att du som börjar hos oss ska trivas och få den hjälp som just du behöver för att lyckas riktigt bra! Vi erbjuder alla verktyg som du behöver i form av utbildning, kontinuerlig coachning och säljtävlingar. Arbetstider: Måndag-torsdag 16:45-21:00 söndag 12:30-16:45. (Minst 2 pass/vecka) Krav: Behärska svenska språket i tal och skrift Lönetyp: Rörlig ackords- eller provisionslön (Veckolön)

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Vi söker fler **säljare** på deltid, där du får lönen u... | x |  |  | 7 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 2 | Vi söker fler säljare på **deltid**, där du får lönen utbetald va... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 3 | ... nu nya stjärnskott till vårt **sälj**-team. Du väljer själv ditt sc... | x |  |  | 4 | [Sälj, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QaXB_Zd5_gmh) |
| 4 | ...ad, trevlig och behärskar det **svenska** språket i både tal och skrift... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 5 | ...ad, trevlig och behärskar det **svenska språket** i både tal och skrift. Skulle... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 6 | ...på relationer, inte på att en **säljare** argumenterar ner en kund. Vår... | x |  |  | 7 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| 7 | ...ven hitta ytterligare info om **Malmös** bästa arbetsplats för gymnasi... | x |  |  | 6 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [Marknadsundersökningar, statistisk analys, **skill**](http://data.jobtechdev.se/taxonomy/concept/2KH5_1dw_QQF) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [utföra marknadsundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/3WQJ_czh_ovC) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [Marknads- och opinionsundersökning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/3kjc_WuU_NQZ) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [intervjuare, opinions- och marknadsundersökning, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/9Kic_bwR_6Tp) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [göra marknadsundersökningar inom skobranschen, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Q49g_uoz_CM2) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [Marknads- och opinionsundersökning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/QJ7Q_5CN_oFx) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [Reklam och marknadsundersökning, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/WwRj_Y6s_cW5) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [utföra marknadsundersökningar gällande smycken, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XeU7_fuE_9JY) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... | x | x | 22 | 22 | [Marknadsundersökningar/Resultatanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/u3bU_va9_8dx) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [dra slutsatser av resultat från marknadsundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/u5xM_Cz4_ac5) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [marknadsundersökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/vVmn_YoY_zDm) |
| 8 | ...atta försäljning av tjänster, **marknadsundersökningar** eller mötesbokning. Du repres... |  | x |  | 22 | [Marknads- och opinionsundersökning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/viSq_jPm_6dp) |
| 9 | ...riktigt bra! Vi erbjuder alla **verktyg** som du behöver i form av utbi... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 10 | ...behöver i form av utbildning, **kontinuerlig **coachning och säljtävlingar. A... |  | x |  | 13 | [Coaching/Coachning, **skill**](http://data.jobtechdev.se/taxonomy/concept/91Ef_58Y_jX4) |
| 11 | ...m av utbildning, kontinuerlig **coachning** och säljtävlingar. Arbetstide... | x | x | 9 | 9 | [Coaching/Coachning, **skill**](http://data.jobtechdev.se/taxonomy/concept/91Ef_58Y_jX4) |
| 12 | ... 2 pass/vecka) Krav: Behärska **svenska** språket i tal och skrift Löne... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 13 | ... 2 pass/vecka) Krav: Behärska **svenska språket** i tal och skrift Lönetyp: Rör... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 14 | ...ket i tal och skrift Lönetyp: **Rörlig ackords- eller provisionslön** (Veckolön) | x |  |  | 35 | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
| | **Overall** | | | **31** | **402** | 31/402 = **8%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Marknadsundersökningar, statistisk analys, **skill**](http://data.jobtechdev.se/taxonomy/concept/2KH5_1dw_QQF) |
|  | x |  | [utföra marknadsundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/3WQJ_czh_ovC) |
|  | x |  | [Marknads- och opinionsundersökning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/3kjc_WuU_NQZ) |
| x | x | x | [Coaching/Coachning, **skill**](http://data.jobtechdev.se/taxonomy/concept/91Ef_58Y_jX4) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
|  | x |  | [intervjuare, opinions- och marknadsundersökning, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/9Kic_bwR_6Tp) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| x |  |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
|  | x |  | [göra marknadsundersökningar inom skobranschen, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Q49g_uoz_CM2) |
|  | x |  | [Marknads- och opinionsundersökning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/QJ7Q_5CN_oFx) |
| x |  |  | [Sälj, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QaXB_Zd5_gmh) |
|  | x |  | [Reklam och marknadsundersökning, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/WwRj_Y6s_cW5) |
|  | x |  | [utföra marknadsundersökningar gällande smycken, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XeU7_fuE_9JY) |
| x |  |  | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x |  |  | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| x | x | x | [Marknadsundersökningar/Resultatanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/u3bU_va9_8dx) |
|  | x |  | [dra slutsatser av resultat från marknadsundersökningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/u5xM_Cz4_ac5) |
|  | x |  | [marknadsundersökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/vVmn_YoY_zDm) |
| x |  |  | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
|  | x |  | [Marknads- och opinionsundersökning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/viSq_jPm_6dp) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **2** | 2/21 = **10%** |