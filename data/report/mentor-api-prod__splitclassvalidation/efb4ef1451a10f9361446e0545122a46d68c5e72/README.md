# Results for 'efb4ef1451a10f9361446e0545122a46d68c5e72'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [efb4ef1451a10f9361446e0545122a46d68c5e72](README.md) | 1 | 4609 | 32 | 18 | 95/454 = **21%** | 7/27 = **26%** |

## Source text

Länsförsäkringar Jämtland söker Försäkringschef Länsförsäkringar Jämtland är ett lokalt bank- och försäkringsbolag. Vi är också kundägda. Det innebär att våra förpliktelser i första hand är mot alla fantastiska människor som bor i Jämtland och Härjedalen men också skogen, fjällen och staden. Vår livsmiljö. Som medarbetare arbetar du på en flexibel arbetsplats i ständig utveckling som tar hänsyn till livets förutsättningar. Tillsammans gör vi det som är bäst för våra kunder, våra medarbetare och hela länet   Vill du jobba i ett företag som sätter både kunden och medarbetaren i centrum? På Länsförsäkringar Jämtland ser vi sambandet mellan nöjda medarbetare och nöjda kunder. Som Försäkringschef hos oss på Länsförsäkringar Jämtland har du ansvar för hela Försäkringsorganisationen.    Vill du jobba i ett företag som sätter både kunden och medarbetaren i centrum? På Länsförsäkringar Jämtland ser vi sambandet mellan nöjda medarbetare och nöjda kunder. Hos oss jobbar vi i grupper där vi skapar trygghet för våra kunder, men också trivsel och utvecklingsmöjligheter för våra medarbetare. Vi vill överträffa kundernas förväntningar och det gör vi genom att vi har en bra atmosfär på arbetsplatsen och vi utgår ifrån kundens behov. Det är extra viktigt för oss eftersom det är våra kunder som äger oss.   Vi söker en Försäkringschef  Vår nuvarande försäkringschef Jan Persson kommer att gå i pension om ett år och vi söker en ersättare till honom. Som Försäkringschef hos oss på Länsförsäkringar Jämtland har du ansvar för hela Försäkringsorganisationen. Du har fyra direktrapporterande Gruppchefer. Försäkringsorganisationen består av ca 60 medarbetare fördelade på Försäkringscenter, Företag/Lantbruk, Försäkringsstöd och Risk. För att lyckas i rollen som Försäkringschef krävs stor förståelse för sälj, riskhantering, marknads- samt produkt och prissättningsfrågor. Du har även god förståelse för regelverkshantering och myndighetskrav. Rollen innebär också ansvar för att säkerställa hanteringen av bolagets största risker samt att återförsäkringsbehov och riskurvalsregler efterföljs.  Försäkringschefen har budgetansvar för hela affärsområdet och har tillsammans med företagsledningen ett delat ansvar för företagets strategiska affärsutveckling. En viktig del i tjänsten är att driva, leda och utveckla individer, grupper och verksamhet. Försäkringsbranschen är i ständig utveckling och det är viktigt med erfarenhet av att leda i förändring.  Som Försäkringschef deltar och verkar du i flera samverkansgrupper såsom kundledning, företagsledning och i bolagsgemensamma nationella sammanhang i LF-gruppen. Du rapporterar till VD.  Vem är du?  Vi söker dig som har en akademisk utbildning eller motsvarande och dokumenterad ledarerfarenhet. Som person är du nyfiken och engagerad och drivs av att coacha och få andra människor att växa. Vi ser gärna att du har erfarenhet från försäkringsbranschen med intresse av så väl privat- som lantbruk/företagssidan. Du har en gedigen erfarenhet av försäljning, är utåtriktad, social och aktiv. Du är dessutom affärsmässig, drivande och har ett analytiskt och strukturerat arbetssätt.  Du är en person med goda ledaregenskaper som kan utveckla vår försäkringsverksamhet tillsammans med våra engagerade ledare och medarbetare. Tydlighet i ledarskapet och god kommunikationsförmåga är viktiga egenskaper i chefsrollen. Ett krav är att du är, eller blir, bosatt i Jämtland, då vårt kontor finns i Östersund.  Vi verkar på en marknad som ständigt utvecklas och det kräver att du som person är anpassningsbar, självständig och har ett positivt förhållningssätt till förändringar. För oss är det viktigt med jämställdhet och mångfald och därför vill vi att du är med och bidrar till en inkluderande kultur. Du delar våra värderingar och agerar utifrån vår värdegrund, dvs med beaktande av hållbarhet, via ett engagemang och mod. För att uppnå en så fördomsfri rekrytering som möjligt använder vi oss av tester i processen.  Anställningsvillkor  Tillsvidare, heltid och tillträde enligt överenskommelse. Vi tillämpar individuell lönesättning.  Vill du veta mer?  Facklig representant Joakim Hilldén, joakim.hillden@lfz.se, 063-19 33 00 (växeln)  Jefferson Wells kommer att göra den här rekryteringen. Dela gärna vår annons på sociala medier. Kontaktperson för rekryteringsprocessen Rosmari Hagström på   rosmari.hagstrom@jeffersonwells.se eller via 073-028 70 40.   Du söker tjänsten genom att registrera dina kontaktuppgifter och laddar upp ditt svenska CV (innehållande personligt brev och meritförteckning) senast den 4 september 2022  Välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Länsförsäkringar **Jämtland** söker Försäkringschef Länsför... |  | x |  | 8 | [Jämtlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/65Ms_7r1_RTG) |
| 2 | ...nsförsäkringar Jämtland söker **Försäkringschef** Länsförsäkringar Jämtland är ... | x |  |  | 15 | [Bank- och försäkringschefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/B7nd_2Es_DBa) |
| 3 | ...säkringschef Länsförsäkringar **Jämtland** är ett lokalt bank- och försä... |  | x |  | 8 | [Jämtlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/65Ms_7r1_RTG) |
| 4 | ...ringar Jämtland är ett lokalt **bank**- och försäkringsbolag. Vi är ... | x |  |  | 4 | [Bank, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xqgq_CRg_5mv) |
| 5 | ...tland är ett lokalt bank- och **försäkringsbolag**. Vi är också kundägda. Det in... | x |  |  | 16 | [Försäkring, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/wjA9_2f4_Wy8) |
| 6 | ...ntastiska människor som bor i **Jämtland** och Härjedalen men också skog... |  | x |  | 8 | [Jämtlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/65Ms_7r1_RTG) |
| 7 | ... centrum? På Länsförsäkringar **Jämtland** ser vi sambandet mellan nöjda... |  | x |  | 8 | [Jämtlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/65Ms_7r1_RTG) |
| 8 | ...rbetare och nöjda kunder. Som **Försäkringschef** hos oss på Länsförsäkringar J... | x |  |  | 15 | [Bank- och försäkringschefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/B7nd_2Es_DBa) |
| 9 | ...f hos oss på Länsförsäkringar **Jämtland** har du ansvar för hela Försäk... |  | x |  | 8 | [Jämtlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/65Ms_7r1_RTG) |
| 10 | ... centrum? På Länsförsäkringar **Jämtland** ser vi sambandet mellan nöjda... |  | x |  | 8 | [Jämtlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/65Ms_7r1_RTG) |
| 11 | ...r som äger oss.   Vi söker en **Försäkringschef**  Vår nuvarande försäkringsche... | x |  |  | 15 | [Bank- och försäkringschefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/B7nd_2Es_DBa) |
| 12 | ...örsäkringschef  Vår nuvarande **försäkringschef** Jan Persson kommer att gå i p... | x |  |  | 15 | [Bank- och försäkringschefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/B7nd_2Es_DBa) |
| 13 | ...ion om ett år och vi söker en **ersättare** till honom. Som Försäkringsch... | x |  |  | 9 | [Ersättare, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ZXUJ_8nn_MS8) |
| 14 | ... en ersättare till honom. Som **Försäkringschef** hos oss på Länsförsäkringar J... | x |  |  | 15 | [Bank- och försäkringschefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/B7nd_2Es_DBa) |
| 15 | ...f hos oss på Länsförsäkringar **Jämtland** har du ansvar för hela Försäk... |  | x |  | 8 | [Jämtlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/65Ms_7r1_RTG) |
| 16 | ... har fyra direktrapporterande **Gruppchefer**. Försäkringsorganisationen be... | x |  |  | 11 | [Ekonomiansvarig gruppchef, **job-title**](http://data.jobtechdev.se/taxonomy/concept/R7CP_R6m_G73) |
| 17 | ...rdelade på Försäkringscenter, **Företag**/Lantbruk, Försäkringsstöd och... | x |  |  | 7 | [Företagande, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fxGP_Unf_RJZ) |
| 18 | ...på Försäkringscenter, Företag/**Lantbruk**, Försäkringsstöd och Risk. Fö... | x |  |  | 8 | [Lantbruk, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/RXMT_9Ui_TJJ) |
| 19 | ...ingscenter, Företag/Lantbruk, **Försäkrings**stöd och Risk. För att lyckas ... | x |  |  | 11 | [Försäkring, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/wjA9_2f4_Wy8) |
| 20 | .... För att lyckas i rollen som **Försäkringschef** krävs stor förståelse för säl... | x |  |  | 15 | [Bank- och försäkringschefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/B7nd_2Es_DBa) |
| 21 | ...hef krävs stor förståelse för **sälj**, riskhantering, marknads- sam... | x |  |  | 4 | [Sälj, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QaXB_Zd5_gmh) |
| 22 | ...ävs stor förståelse för sälj, **riskhantering**, marknads- samt produkt och p... |  | x |  | 13 | [Risk management, **skill**](http://data.jobtechdev.se/taxonomy/concept/bHQT_Bhn_w3S) |
| 22 | ...ävs stor förståelse för sälj, **riskhantering**, marknads- samt produkt och p... | x | x | 13 | 13 | [riskhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nqH4_69a_CHG) |
| 23 | ...riskurvalsregler efterföljs.  **Försäkringschefen** har budgetansvar för hela aff... | x |  |  | 17 | [Bank- och försäkringschefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/B7nd_2Es_DBa) |
| 24 | ...följs.  Försäkringschefen har **budgetansvar** för hela affärsområdet och ha... | x | x | 12 | 12 | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| 25 | ...ar för företagets strategiska **affärsutveckling**. En viktig del i tjänsten är ... | x | x | 16 | 16 | [Affärsutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TzHP_38y_mhm) |
| 26 | ...änsten är att driva, leda och **utveckla** individer, grupper och verksa... | x |  |  | 8 | [Personlig utveckling, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/finP_hST_DPd) |
| 27 | ...v att leda i förändring.  Som **Försäkringschef** deltar och verkar du i flera ... | x |  |  | 15 | [Bank- och försäkringschefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/B7nd_2Es_DBa) |
| 28 | ...-gruppen. Du rapporterar till **VD**.  Vem är du?  Vi söker dig so... | x |  |  | 2 | (not found in taxonomy) |
| 29 | ...ch engagerad och drivs av att **coacha** och få andra människor att vä... | x |  |  | 6 | [Coaching/Coachning, **skill**](http://data.jobtechdev.se/taxonomy/concept/91Ef_58Y_jX4) |
| 30 | ...ntresse av så väl privat- som **lantbruk**/företagssidan. Du har en gedi... | x |  |  | 8 | [Lantbruk, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/RXMT_9Ui_TJJ) |
| 31 | ...enskaper som kan utveckla vår **försäkringsverksamhet** tillsammans med våra engagera... |  | x |  | 21 | [Stödtjänster till försäkrings- och pensionsfondsverksamhet, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/8FyX_MnR_bUv) |
| 31 | ...enskaper som kan utveckla vår **försäkringsverksamhet** tillsammans med våra engagera... | x | x | 21 | 21 | [Finans- och försäkringsverksamhet, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/WzRS_b9h_VV7) |
| 32 | ...t du är, eller blir, bosatt i **Jämtland**, då vårt kontor finns i Öster... |  | x |  | 8 | [Jämtlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/65Ms_7r1_RTG) |
| 33 | ...r, bosatt i Jämtland, då vårt **kontor** finns i Östersund.  Vi verkar... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 34 | ...tland, då vårt kontor finns i **Östersund**.  Vi verkar på en marknad som... | x | x | 9 | 9 | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| 35 | ...som person är anpassningsbar, **självständig** och har ett positivt förhålln... | x | x | 12 | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 36 | ...r. För oss är det viktigt med **jämställdhet** och mångfald och därför vill ... | x | x | 12 | 12 | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| 37 | ... viktigt med jämställdhet och **mångfald** och därför vill vi att du är ... | x |  |  | 8 | [stödja kulturell mångfald, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ARp9_7Lk_uhS) |
| 38 | ...cessen.  Anställningsvillkor  **Tillsvidare**, heltid och tillträde enligt ... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 39 | ...ällningsvillkor  Tillsvidare, **heltid** och tillträde enligt överensk... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 40 | ...uppgifter och laddar upp ditt **svenska** CV (innehållande personligt b... | x |  |  | 7 | [Svenska, **keyword**](http://data.jobtechdev.se/taxonomy/concept/gfhX_bqH_U4K) |
| 40 | ...uppgifter och laddar upp ditt **svenska** CV (innehållande personligt b... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **95** | **454** | 95/454 = **21%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Jämtlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/65Ms_7r1_RTG) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Stödtjänster till försäkrings- och pensionsfondsverksamhet, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/8FyX_MnR_bUv) |
| x |  |  | [Coaching/Coachning, **skill**](http://data.jobtechdev.se/taxonomy/concept/91Ef_58Y_jX4) |
| x |  |  | [stödja kulturell mångfald, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ARp9_7Lk_uhS) |
| x |  |  | [Bank- och försäkringschefer, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/B7nd_2Es_DBa) |
| x | x | x | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Sälj, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QaXB_Zd5_gmh) |
| x |  |  | [Ekonomiansvarig gruppchef, **job-title**](http://data.jobtechdev.se/taxonomy/concept/R7CP_R6m_G73) |
| x |  |  | [Lantbruk, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/RXMT_9Ui_TJJ) |
| x | x | x | [Affärsutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TzHP_38y_mhm) |
| x | x | x | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| x | x | x | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| x | x | x | [Finans- och försäkringsverksamhet, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/WzRS_b9h_VV7) |
| x |  |  | [Ersättare, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ZXUJ_8nn_MS8) |
| x |  |  | (not found in taxonomy) |
|  | x |  | [Risk management, **skill**](http://data.jobtechdev.se/taxonomy/concept/bHQT_Bhn_w3S) |
| x |  |  | [Personlig utveckling, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/finP_hST_DPd) |
| x |  |  | [Företagande, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fxGP_Unf_RJZ) |
| x |  |  | [Svenska, **keyword**](http://data.jobtechdev.se/taxonomy/concept/gfhX_bqH_U4K) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [riskhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nqH4_69a_CHG) |
| x |  |  | [Försäkring, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/wjA9_2f4_Wy8) |
| x |  |  | [Bank, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xqgq_CRg_5mv) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/27 = **26%** |