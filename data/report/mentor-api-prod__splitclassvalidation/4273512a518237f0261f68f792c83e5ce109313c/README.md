# Results for '4273512a518237f0261f68f792c83e5ce109313c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4273512a518237f0261f68f792c83e5ce109313c](README.md) | 1 | 795 | 13 | 16 | 88/244 = **36%** | 2/7 = **29%** |

## Source text

Lär dig att skapa kommunikation som både når ut och berör – från idé till färdigt koncept. Art Director & Copywriter är en intensiv tvåårig utbildning för dig som vill bli kreatör.    Art Director & Copywriter är en tvåårig kreatörsutbildning med två inriktningar. Du väljer inriktning, Art Director eller Copywriter, när du ansöker, men ni jobbar tätt ihop under utbildningen – precis som ni kommer att göra i arbetslivet.    Som art director är du ansvarig för det visuella uttrycket, medan du som copywriter ansvarar för textinnehållet. Ert fokus är att tillsammans skapa utmanande idébaserad kommunikation utifrån ett tydligt syfte och mål.    Efter avslutad utbildning kan du till exempel jobba på reklam- eller kommunikationsbyrå eller inhouse på företag. Många väljer också att frilansa. 

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Lär dig att skapa **kommunikation** som både når ut och berör – f... | x |  |  | 13 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 2 | ...rån idé till färdigt koncept. **Art Director** & Copywriter är en intensiv t... | x | x | 12 | 12 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 3 | ...rdigt koncept. Art Director & **Copywriter** är en intensiv tvåårig utbild... |  | x |  | 10 | [copywriter, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5kS5_54h_Qd1) |
| 3 | ...rdigt koncept. Art Director & **Copywriter** är en intensiv tvåårig utbild... | x | x | 10 | 10 | [Copywriter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7mFy_ghh_iL6) |
| 3 | ...rdigt koncept. Art Director & **Copywriter** är en intensiv tvåårig utbild... |  | x |  | 10 | [Utbildning till copywriter, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Rnqh_CKD_YrV) |
| 4 | ...r & Copywriter är en intensiv **tvåårig utbildning** för dig som vill bli kreatör.... | x |  |  | 18 | [Högskoleutbildning, yrkesinriktad, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/zdKB_pTL_HMS) |
| 5 | ... dig som vill bli kreatör.    **Art Director** & Copywriter är en tvåårig kr... | x | x | 12 | 12 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 6 | ...li kreatör.    Art Director & **Copywriter** är en tvåårig kreatörsutbildn... |  | x |  | 10 | [copywriter, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5kS5_54h_Qd1) |
| 6 | ...li kreatör.    Art Director & **Copywriter** är en tvåårig kreatörsutbildn... | x | x | 10 | 10 | [Copywriter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7mFy_ghh_iL6) |
| 6 | ...li kreatör.    Art Director & **Copywriter** är en tvåårig kreatörsutbildn... |  | x |  | 10 | [Utbildning till copywriter, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Rnqh_CKD_YrV) |
| 7 | ...t Director & Copywriter är en **tvåårig kreatörsutbildning** med två inriktningar. Du välj... | x |  |  | 26 | [Högskoleutbildning, yrkesinriktad, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/zdKB_pTL_HMS) |
| 8 | ...ningar. Du väljer inriktning, **Art Director** eller Copywriter, när du ansö... | x | x | 12 | 12 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 9 | ...nriktning, Art Director eller **Copywriter**, när du ansöker, men ni jobba... |  | x |  | 10 | [copywriter, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5kS5_54h_Qd1) |
| 9 | ...nriktning, Art Director eller **Copywriter**, när du ansöker, men ni jobba... | x | x | 10 | 10 | [Copywriter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7mFy_ghh_iL6) |
| 9 | ...nriktning, Art Director eller **Copywriter**, när du ansöker, men ni jobba... |  | x |  | 10 | [Utbildning till copywriter, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Rnqh_CKD_YrV) |
| 10 | ...tt göra i arbetslivet.    Som **art director** är du ansvarig för det visuel... | x | x | 12 | 12 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 11 | ...uella uttrycket, medan du som **copywriter** ansvarar för textinnehållet. ... |  | x |  | 10 | [copywriter, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5kS5_54h_Qd1) |
| 11 | ...uella uttrycket, medan du som **copywriter** ansvarar för textinnehållet. ... | x | x | 10 | 10 | [Copywriter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7mFy_ghh_iL6) |
| 11 | ...uella uttrycket, medan du som **copywriter** ansvarar för textinnehållet. ... |  | x |  | 10 | [Utbildning till copywriter, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Rnqh_CKD_YrV) |
| 12 | ...ns skapa utmanande idébaserad **kommunikation** utifrån ett tydligt syfte och... | x |  |  | 13 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 13 | ... kan du till exempel jobba på **reklam**- eller kommunikationsbyrå ell... | x |  |  | 6 | [Reklambyråverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/tMUW_CCQ_kq1) |
| | **Overall** | | | **88** | **244** | 88/244 = **36%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [copywriter, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5kS5_54h_Qd1) |
| x | x | x | [Copywriter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7mFy_ghh_iL6) |
| x | x | x | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
|  | x |  | [Utbildning till copywriter, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Rnqh_CKD_YrV) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x |  |  | [Reklambyråverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/tMUW_CCQ_kq1) |
| x |  |  | [Högskoleutbildning, yrkesinriktad, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/zdKB_pTL_HMS) |
| | | **2** | 2/7 = **29%** |