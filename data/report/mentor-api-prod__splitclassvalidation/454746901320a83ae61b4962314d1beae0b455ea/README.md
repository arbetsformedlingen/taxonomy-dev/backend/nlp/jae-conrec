# Results for '454746901320a83ae61b4962314d1beae0b455ea'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [454746901320a83ae61b4962314d1beae0b455ea](README.md) | 1 | 827 | 6 | 3 | 18/73 = **25%** | 2/7 = **29%** |

## Source text

Serveringspersonal #jobbjustnu Vi söker dig som gillar service och vill vara med i vårt härliga team. Du vill ge bästa service till våra gäster och tycker om att vara social. Du är inte rädd att hjälpa till med det mesta i restaurangen och kan arbeta dels luncher dels några kvällar samt vid stora bokningar. Du har gott ordningssinne och tycker om när det är snyggt och städat omkring dig. Du bör vara stresstålig och kunna ta initiativ för att lösa problem som ofta uppstår under arbetstid i denna bransch. Man bör också vara utåtriktad, trevlig, pratglad och tillmötesgående. Du bör ha fyllt 18 år för att vi har alkoholtillstånd. Du ska kunna tala och förstå svenska. Tjänsten är på minst 75% med möjlighet till 100% Är du den vi letar efter? Gillar du när tempot är högt? Är du den som alltid ger bäst service? #jobbjustnu

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Serveringspersonal** #jobbjustnu Vi söker dig som ... | x |  |  | 18 | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| 1 | **Serveringspersonal** #jobbjustnu Vi söker dig som ... |  | x |  | 18 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | ...t hjälpa till med det mesta i **restaurangen** och kan arbeta dels luncher d... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 3 | ...ädat omkring dig. Du bör vara **stresstålig** och kunna ta initiativ för at... | x | x | 11 | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 4 | ... Du ska kunna tala och förstå **svenska**. Tjänsten är på minst 75% med... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 5 | ...svenska. Tjänsten är på minst **75%** med möjlighet till 100% Är du... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 6 | ... minst 75% med möjlighet till **100%** Är du den vi letar efter? Gil... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **18** | **73** | 18/73 = **25%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x |  |  | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| x |  |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
|  | x |  | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x | x | x | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **2** | 2/7 = **29%** |