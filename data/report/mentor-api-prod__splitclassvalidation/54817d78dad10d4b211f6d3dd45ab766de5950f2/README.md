# Results for '54817d78dad10d4b211f6d3dd45ab766de5950f2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [54817d78dad10d4b211f6d3dd45ab766de5950f2](README.md) | 1 | 2869 | 10 | 7 | 44/152 = **29%** | 6/11 = **55%** |

## Source text

Gekås Ullared söker restaurangmedarbetare till höstsäsongen 2022 Trivs du med att arbeta i en fartfylld och varierad miljö där lagandan står i fokus? Vill du vara med och överträffa våra besökares förväntningar? Då har du stor chans att bli en del av vårt team!  Vi kan erbjuda en stark och väletablerad företagskultur där vi tillsammans arbetar efter våra ledstjärnor – Kundfokus & Ärlighet, Laganda & Engagemang samt Sunt förnuft & Enkelhet.  Nyckeln till Gekås Ullareds framgång är engagerade medarbetare. Vi strävar hela tiden efter att vara en bra arbetsgivare genom att erbjuda en fartfylld, kreativ, rolig och dynamisk arbetsplats där alla ska känna sig välkomna och inkluderade i gemenskapen.  Arbetsuppgifter  De olika arbetsuppgifterna kan bestå av att förbereda och portionera upp mat, bemanna kassan samt finnas tillhands och vara behjälplig för våra ätande gäster. Alla i arbetslaget ansvarar för att hålla rent och snyggt omkring sig i sin arbetsmiljö, vilket innebär att vi gemensamt ansvarar för att torka bord, rengöra golv och övriga ytor samt tar hand om diskvagnar och diskstationer.  Vi söker dig som:   • Har fyllt 16 år • Är serviceinriktad och har en positiv inställning • Är prestigelös och kan rycka in där det behövs • Trivs med att arbeta i team och har en god samarbetsförmåga  Har du tidigare erfarenhet av restaurangbranschen är det meriterande, men inget krav.  Omfattning  Vi är i behov av dig som kan arbeta deltid (50–80%), samt dig som enbart är intresserad av att arbeta helger. Arbetstiderna är varierande och kan förläggas under dagtid, kvällar och helger.  Tillträde  Tycker du att beskrivningen låter spännande? Ta chansen att söka redan idag! Vi kommer att tillsätta tjänster fr.o.m. augusti och löpande för arbete över höstsäsongen med goda möjligheter att komma tillbaka till våren och sommaren 2023.  Visste du att Gekås Ullareds restauranger erbjuder högre Ob-ersättning för sina medarbetare?   Läs mer om det här >>  Förmåner  Som anställd hos oss på Gekås Ullared har du bl.a. personalrabatt i varuhuset, personalrestaurang med förmånliga priser samt tillgång till gratis gym och träningspass i aktivitetscentret. Läs mer om våra förmåner här >>  Om Gekås Restauranger  För att skapa bästa möjliga shoppingupplevelse för våra besökare erbjuder vi ett varierat utbud av restauranger i varuhuset och i anslutning till våra boendeanläggningar. Under högsäsong serverar vi allt ifrån frukost, fika, lunchrätter, finger-food och husmanskost. Gekås restauranger består av Kund- och personalrestaurangen, Kaffebaren, Sportbaren samt nyöppnade Macka & Jos. Vi har även två restauranger belägna utanför varuhuset; Hotellrestaurangerna i vår hotellbyggnad samt Gekåsbyns Bistro på campingen. Här erbjuder vi våra gäster frukostbuffé, kvällsbuffé med både varma och kalla rätter, pizza, a la carte-rätter samt kaffe med dessertbord.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Gekås Ullared söker **restaurang**medarbetare till höstsäsongen ... | x |  |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 2 | Gekås Ullared söker **restaurangmedarbetare** till höstsäsongen 2022 Trivs ... |  | x |  | 21 | [Restaurangpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZYjR_db3_jN5) |
| 3 | ...ereda och portionera upp mat, **bemanna kassan** samt finnas tillhands och var... | x |  |  | 14 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 4 | ... och snyggt omkring sig i sin **arbetsmiljö**, vilket innebär att vi gemens... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 5 | ...Har du tidigare erfarenhet av **restaurangbranschen** är det meriterande, men inget... | x |  |  | 19 | [Restaurangverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/CK7M_gYV_9ea) |
| 5 | ...Har du tidigare erfarenhet av **restaurangbranschen** är det meriterande, men inget... |  | x |  | 19 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 6 | ...i behov av dig som kan arbeta **deltid (50–80%)**, samt dig som enbart är intre... | x |  |  | 15 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 7 | ... lunchrätter, finger-food och **husmanskost**. Gekås restauranger består av... | x | x | 11 | 11 | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| 8 | .... Här erbjuder vi våra gäster **frukostbuffé**, kvällsbuffé med både varma o... | x | x | 12 | 12 | [Frukostbuffé, **skill**](http://data.jobtechdev.se/taxonomy/concept/sqd6_4Kk_b4w) |
| 9 | ... både varma och kalla rätter, **pizza**, a la carte-rätter samt kaffe... | x | x | 5 | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 10 | ...arma och kalla rätter, pizza, **a la carte**-rätter samt kaffe med dessert... | x |  |  | 10 | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| 11 | ...pizza, a la carte-rätter samt **kaffe** med dessertbord. | x | x | 5 | 5 | [Kaffe, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wqfj_Ka3_zu3) |
| | **Overall** | | | **44** | **152** | 44/152 = **29%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x |  |  | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| x |  |  | [Restaurangverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/CK7M_gYV_9ea) |
| x | x | x | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| x |  |  | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x | x | x | [Kaffe, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wqfj_Ka3_zu3) |
|  | x |  | [Restaurangpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZYjR_db3_jN5) |
| x | x | x | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Frukostbuffé, **skill**](http://data.jobtechdev.se/taxonomy/concept/sqd6_4Kk_b4w) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **6** | 6/11 = **55%** |