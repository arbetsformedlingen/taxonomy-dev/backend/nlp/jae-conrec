# Results for '37e0f74e97eb7114f85fe980165f7f16c53ddbf6'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [37e0f74e97eb7114f85fe980165f7f16c53ddbf6](README.md) | 1 | 637 | 6 | 2 | 7/122 = **6%** | 1/7 = **14%** |

## Source text

OM UTBILDNINGEN  Kod är grunden till alla webbsidor, appar och gränssnitt. Ju mer digitaliserade vi blir och ju fler plattformar som används, desto större blir också behovet efter programmerare som kan skriva och utveckla den koden.    Under denna utbildning specialiserar du dig på webbutveckling med utgångspunkt i Microsofts .NET-plattform och programmeringsspråket #C. Tillsammans ligger dessa till grund för de flesta av dagens kommersiella applikationer och är starkt etablerade både i Sverige och resten av världen.    Mer information om utbildningen hittar du på:  https://www.ecutbildning.se/utbildningar/webbutvecklare-inom-net

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ill alla webbsidor, appar och **gränssnitt**. Ju mer digitaliserade vi bli... |  | x |  | 10 | [Gränssnittsdesign, undervisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/kQVR_Hbt_KgR) |
| 2 | ...örre blir också behovet efter **programmerare** som kan skriva och utveckla d... | x |  |  | 13 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 3 | ...t efter programmerare som kan **skriva och utveckla den koden**.    Under denna utbildning sp... | x |  |  | 29 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 4 | ...dning specialiserar du dig på **webbutveckling** med utgångspunkt i Microsofts... | x |  |  | 14 | [Webbutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/AfJ4_Pvv_YBa) |
| 5 | ...utveckling med utgångspunkt i **Microsofts .NET-plattform** och programmeringsspråket #C.... | x |  |  | 25 | [ramverk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/JD8j_tGG_cJT) |
| 6 | ...Microsofts .NET-plattform och **programmeringsspråket #C**. Tillsammans ligger dessa til... | x |  |  | 24 | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| 7 | ...h är starkt etablerade både i **Sverige** och resten av världen.    Mer... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **7** | **122** | 7/122 = **6%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Webbutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/AfJ4_Pvv_YBa) |
| x |  |  | [ramverk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/JD8j_tGG_cJT) |
| x |  |  | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x |  |  | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
|  | x |  | [Gränssnittsdesign, undervisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/kQVR_Hbt_KgR) |
| | | **1** | 1/7 = **14%** |