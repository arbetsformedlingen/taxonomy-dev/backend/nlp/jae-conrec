# Results for 'ba19648b9fd79536fb5c61eb29cb38c189d176d5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ba19648b9fd79536fb5c61eb29cb38c189d176d5](README.md) | 1 | 2269 | 18 | 7 | 44/269 = **16%** | 3/15 = **20%** |

## Source text

Interim rektor- Carlsund Vill du bli vår nya kollega? I Motala, Östergötlands sjöstad, är vi öppna, stolta och nyskapande. Vår kommun är rätt plats för att bidra, utvecklas och trivas. Hos oss får du arbeta med engagerade kollegor, utveckla dig inom ditt område och jobba med det viktigaste som finns.  Vi gillar dig – och vi hjälper dig att utvecklas. Vår mission är att barn och vuxna tillsammans ska utvecklas till kreativa, positiva individer som väljer livsglädje. Därför erbjuder vi en bred och djup verksamhet där vi ger dig möjligheterna, oavsett vilka mål du har. Motala ska vara en attraktiv och innovativ del av östgötaregionen där det är enkelt att bo och verka. Det är också vår drivkraft inom bildningsförvaltningen.   Carlsund Utbildningscentrum är en av Motala kommuns två gymnasieskolor. Skolan leds av två rektorer. På skolan finns sju nationella yrkesprogram samt Introduktionsprogrammet IM.     ARBETSUPPGIFTER Vi rekryterar just nu en ny rektor till Carlsunds Utbildningscentrum. Ditt uppdrag som interim rektor blir att under rekryteringstiden leda den dagliga driften vid skolenheten och programmen Bygg-och anläggningsprogrammet, Fordon och transportprogrammet, Industritekniska programmet, Hotell och turismprogrammet samt IM. Du har ansvar för ekonomi, personal och verksamhet på enheten och rapporterar till verksamhetschef. Arbetet utför du i nära samarbete med ytterligare en rektor som idag arbetar på Carlsunds Utbildningscentrum.   KVALIFIKATIONER Vi söker dig med högskoleutbildning inom relevant område, gärna inom skolområdet. Du har tidigare erfarenhet av chefs- och ledaruppdrag och har arbetat som rektor inom utbildningssektorn.  Vi ser det som meriterande om du har påbörjad eller genomförd rektorsutbildning. Vi söker dig som är trygg och stabil i ditt ledarskap. Du har god kommunikativ förmåga och uttrycker dig väl i tal och skrift.   ÖVRIGT Tillsättning sker under förutsättning av vederbörliga beslut.   Rekrytering sker fortlöpande - så skicka in din ansökan redan idag!  Har du önskemål om annan tjänstgöringsgrad än heltid så kan det vara möjligt   Notera vikten av att du söker tjänsten digitalt via www.offentligajobb.se   Vi undanber oss all extern rekryterings- och annonseringshjälp i samband med denna annons.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Interim **rektor**- Carlsund Vill du bli vår nya... | x |  |  | 6 | [Rektor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/hr5f_dci_NcD) |
| 2 | ...ill du bli vår nya kollega? I **Motala**, Östergötlands sjöstad, är vi... | x | x | 6 | 6 | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| 3 | ...na, oavsett vilka mål du har. **Motala** ska vara en attraktiv och inn... | x | x | 6 | 6 | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| 4 | ...ttraktiv och innovativ del av **östgötaregionen** där det är enkelt att bo och ... | x |  |  | 15 | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
| 5 | ...d Utbildningscentrum är en av **Motala** kommuns två gymnasieskolor. S... | x | x | 6 | 6 | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| 6 | ...ldningscentrum är en av Motala** kommuns** två gymnasieskolor. Skolan le... | x |  |  | 8 | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| 7 | ...sieskolor. Skolan leds av två **rektorer**. På skolan finns sju nationel... | x |  |  | 8 | [Rektor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/hr5f_dci_NcD) |
| 8 | ...id skolenheten och programmen **Bygg-och anläggningsprogrammet**, Fordon och transportprogramm... | x |  |  | 30 | [Gymnasieskolans bygg- och anläggningsprogram, inriktning husbyggnad, **keyword**](http://data.jobtechdev.se/taxonomy/concept/cH6v_inP_pBN) |
| 9 | ...gg-och anläggningsprogrammet, **Fordon och **transportprogrammet, Industrit... | x |  |  | 11 | [Gymnasieskolans fordons- och transportprogram, inriktning transport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/2cVb_SQk_Ygo) |
| 10 | ...ggningsprogrammet, Fordon och **transportprogrammet**, Industritekniska programmet,... | x | x | 19 | 19 | [Gymnasieskolans fordons- och transportprogram, inriktning transport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/2cVb_SQk_Ygo) |
| 11 | ...rdon och transportprogrammet, **Industritekniska programmet**, Hotell och turismprogrammet ... | x |  |  | 27 | [Gymnasieskolans industritekniska program, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UxgM_KuE_pwv) |
| 12 | ... Industritekniska programmet, **Hotell** och turismprogrammet samt IM.... |  | x |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 13 | ... Industritekniska programmet, **Hotell och turismprogrammet** samt IM. Du har ansvar för ek... | x |  |  | 27 | [Gymnasieskolans hotell- och turismprogram, inriktning turism och resor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/FbhR_shS_md2) |
| 14 | ...et samt IM. Du har ansvar för **ekonomi**, personal och verksamhet på e... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 15 | ...M. Du har ansvar för ekonomi, **personal** och verksamhet på enheten och... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 16 | ...var för ekonomi, personal och **verksamhet** på enheten och rapporterar ti... | x |  |  | 10 | [Verksamhetsledning, gymnasieskolan, **skill**](http://data.jobtechdev.se/taxonomy/concept/Ju2P_fHA_n5B) |
| 17 | ... enheten och rapporterar till **verksamhetschef**. Arbetet utför du i nära sama... | x |  |  | 15 | [Chefer inom grund- och gymnasieskola samt vuxenutbildning, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/z8di_AqX_GBr) |
| 18 | ... samarbete med ytterligare en **rektor** som idag arbetar på Carlsunds... | x |  |  | 6 | [Rektor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/hr5f_dci_NcD) |
| 19 | ...LIFIKATIONER Vi söker dig med **högskoleutbildning** inom relevant område, gärna i... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 19 | ...LIFIKATIONER Vi söker dig med **högskoleutbildning** inom relevant område, gärna i... | x |  |  | 18 | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| 20 | ...aruppdrag och har arbetat som **rektor** inom utbildningssektorn.  Vi ... | x |  |  | 6 | [Rektor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/hr5f_dci_NcD) |
| 21 | ...om annan tjänstgöringsgrad än **heltid** så kan det vara möjligt   Not... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **44** | **269** | 44/269 = **16%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Gymnasieskolans fordons- och transportprogram, inriktning transport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/2cVb_SQk_Ygo) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Motala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/E1MC_1uG_phm) |
| x |  |  | [Gymnasieskolans hotell- och turismprogram, inriktning turism och resor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/FbhR_shS_md2) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x |  |  | [Verksamhetsledning, gymnasieskolan, **skill**](http://data.jobtechdev.se/taxonomy/concept/Ju2P_fHA_n5B) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [Gymnasieskolans industritekniska program, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UxgM_KuE_pwv) |
| x |  |  | [Gymnasieskolans bygg- och anläggningsprogram, inriktning husbyggnad, **keyword**](http://data.jobtechdev.se/taxonomy/concept/cH6v_inP_pBN) |
| x |  |  | [Rektor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/hr5f_dci_NcD) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x |  |  | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
| x |  |  | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| x |  |  | [Chefer inom grund- och gymnasieskola samt vuxenutbildning, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/z8di_AqX_GBr) |
| | | **3** | 3/15 = **20%** |