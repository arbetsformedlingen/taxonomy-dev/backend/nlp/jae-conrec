# Results for 'bea728043c7ba79c5a94ac3360a12b9b4fb21d97'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [bea728043c7ba79c5a94ac3360a12b9b4fb21d97](README.md) | 1 | 4572 | 28 | 16 | 163/362 = **45%** | 14/22 = **64%** |

## Source text

Senior fuktspecialist Som ett av Sveriges ledande byggföretag har vi ett stort ansvar, men också möjligheter att skapa förändringar i hur vi ska bygga våra hus mer klimatsmarta för en mer hållbar framtid. För att nå våra hållbarhetsmål behöver vi förstärka enheten Hållbarhet med ytterligare en senior fuktspecialist.  I enheten Hållbarhet är vi totalt i Sverige är vi drygt 40 hållbarhetsspecialister, varav ungefär hälften i Solna, som arbetar med frågor som rör miljö, klimat, energi, byggnadsfysik, social hållbarhet och landskapsarkitektur.  Vår verksamhet fokuserar på stöd i tidigt skede, projektering, produktionsfrågor och frågor i byggnadernas driftsfas. Vi har en bra gemenskap med en stark lärandekultur och detta är bidragande orsaker till att vi har många nöjda och stolta medarbetare.  Rollen som senior fuktspecialist Din uppgift blir att driva egna uppdrag och samarbeta med andra för att uppnå rätt resultat i projekten. Utöver att arbeta operativt i våra projekt kommer du fungera som expertstöd till våra affärschefer i sakfrågor samt delta i kunddialoger. Du kommer på ett pedagogiskt sätt förklara hållbarhetsarbetet i projektets olika faser samt hur arbetet stärker affären.  Primärt kommer du att fokusera på arbete som fuktsakkunnig och som fuktstöd till projektering, oftast för miljöcertifierade byggnader, men även som stöd under produktion.  Du kommer även att ha möjlighet att arbeta med central och extern utveckling kopplat till hållbarhets- och byggnadsfysikfrågor.  Du kommer att jobba i ett entreprenörföretag där närheten till våra beställare och till produktionen är i det närmaste unik i branschen. Tillsammans med våra kollegor får vi följa projekten vi arbetar med vilket ger oss en spännande arbetsmiljö och stora utvecklingsmöjligheter.  Din profil Du har en akademisk utbildning som är relevant för rollen  års erfarenhet av arbete med byggnadsfysik med inriktning mot fuktområdet och med erfarenhet att arbeta som fuktsakkunnig. Vi ser positivt på om du har tidigare erfarenhet som konsult.   Det är också meriterande ifall du har erfarenhet av fuktberäkningar och har arbetat med miljöcertifieringssystem såsom BREEAM och Miljöbyggnad. Har du dessutom erfarenhet av andra certifieringssystem, exempelvis Svanen, eller specialistkunskaper inom klimatfrågan är det extra meriterande.  Du har god förmåga att planera, driva och administrera dina uppdrag och du kan leda juniora kollegor. Att hålla i utbildningar och kunskapshöjande insatser är något som du trivs med. För att lyckas i rollen är du självgående, utåtriktad och skicklig på att kommunicera.  Du har mycket goda kunskaper i svenska och engelska i såväl tal som skrift.  Ytterligare information Tjänsten är en tillsvidareanställning med placering på NCC:s kontor i Solna. Resor i tjänsten förekommer, främst i Stockholm/Mälardalen. B-körkort krävs för tjänsten.  Kontakt och ansökan Har du frågor om tjänsten kontakta gärna Gruppchef Fredrik Gränne, 070-227 46 76 efter den 15/8 eller HR-specialist Rekrytering Karin Österman de Wall, 079-078 72 10 efter den 17/8. Skicka in ditt CV och brev senast den 2022-08-31. Urval och intervjuer sker tidigast vecka 35 efter ansökningstidens utgång.  Välkommen med din ansökan!  Om oss  Varje dag tar våra över 13 000 medarbetare beslut som förbättrar människors vardag, både idag och i morgon. Här arbetar du i en stark gemenskap tillsammans med engagerade och professionella kollegor som drivs av att lära nytt, nå uppsatta mål, dela erfarenheter och göra verklig skillnad tillsammans. Vi utmanar oss själva för att driva utvecklingen och skapar hållbara lösningar som för samhället framåt med ny kunskap.    Som ett av Nordens ledande bygg- och fastighetsutvecklingsföretag utvecklar vi kommersiella fastigheter, bygger skolor, sjukhus, bostäder, vägar, broar och annan infrastruktur som formar vårt sätt att leva, arbeta och resa i samhället. Genom vår industriverksamhet erbjuder vi produkter och tjänster med inriktning på stenmaterial och asfaltsproduktion, beläggningsuppdrag.   Vi värnar ett hållbart arbetsliv med starkt fokus på säkerhet, personlig utveckling och balans mellan jobb och fritid.    NCC ska spegla våra kunder såväl som samhället i stort och är beroende av medarbetare med olika kompetenser. Vi strävar efter att anställa människor med olika bakgrund och värdesätter den kunskap och erfarenhet det medför.   I enlighet med NCC:s säkerhetskultur genomför vi bakgrundskontroll på våra slutkandidater.    Vi undanber oss vänligen kontakt med rekryterare samt säljare av annons- eller bemanningslösningar.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ior fuktspecialist Som ett av **Sveriges** ledande byggföretag har vi et... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 2 | ...s mer klimatsmarta för en mer **hållbar framtid**. För att nå våra hållbarhetsm... | x |  |  | 15 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 3 | ...lbar framtid. För att nå våra **hållbarhetsmål** behöver vi förstärka enheten ... | x |  |  | 14 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 4 | ... behöver vi förstärka enheten **Hållbarhet** med ytterligare en senior fuk... | x |  |  | 10 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 5 | ...or fuktspecialist.  I enheten **Hållbarhet** är vi totalt i Sverige är vi ... | x |  |  | 10 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 6 | ...ten Hållbarhet är vi totalt i **Sverige** är vi drygt 40 hållbarhetsspe... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 7 | ...talt i Sverige är vi drygt 40 **hållbarhetsspecialister**, varav ungefär hälften i Soln... |  | x |  | 23 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 8 | ...ster, varav ungefär hälften i **Solna**, som arbetar med frågor som r... | x | x | 5 | 5 | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| 9 | ...limat, energi, byggnadsfysik, **social hållbarhet** och landskapsarkitektur.  Vår... | x | x | 17 | 17 | [Social hållbarhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/9MoM_m8y_Em5) |
| 10 | ...sfysik, social hållbarhet och **landskapsarkitektur**.  Vår verksamhet fokuserar på... | x | x | 19 | 19 | [landskapsarkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VryZ_dNm_o4a) |
| 11 | ...serar på stöd i tidigt skede, **projektering**, produktionsfrågor och frågor... | x | x | 12 | 12 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 12 | ...kkunnig och som fuktstöd till **projektering**, oftast för miljöcertifierade... | x | x | 12 | 12 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 13 | ...d vilket ger oss en spännande **arbetsmiljö** och stora utvecklingsmöjlighe... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 14 | ...För att lyckas i rollen är du **självgående**, utåtriktad och skicklig på a... | x |  |  | 11 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ...u har mycket goda kunskaper i **svenska** och engelska i såväl tal som ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 16 | ... goda kunskaper i svenska och **engelska** i såväl tal som skrift.  Ytte... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 17 | ...re information Tjänsten är en **tillsvidareanställning** med placering på NCC:s kontor... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 18 | ...llning med placering på NCC:s **kontor** i Solna. Resor i tjänsten för... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 19 | ...d placering på NCC:s kontor i **Solna**. Resor i tjänsten förekommer,... | x | x | 5 | 5 | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| 20 | ...ring på NCC:s kontor i Solna. **Resor i tjänsten förekommer**, främst i Stockholm/Mälardale... | x |  |  | 27 | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| 21 | ...tjänsten förekommer, främst i **Stockholm**/Mälardalen. B-körkort krävs f... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 22 | ...rämst i Stockholm/Mälardalen. **B-körkort** krävs för tjänsten.  Kontakt ... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 23 | ...27 46 76 efter den 15/8 eller **HR-specialist** Rekrytering Karin Österman de... | x | x | 13 | 13 | [HR-specialist/HR-advisor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s28Q_sya_S2b) |
| 24 | ...nriktning på stenmaterial och **asfaltsproduktion**, beläggningsuppdrag.   Vi vär... | x |  |  | 17 | [Asfalt, **keyword**](http://data.jobtechdev.se/taxonomy/concept/k9fW_Kv6_kf8) |
| 25 | ...terial och asfaltsproduktion, **beläggningsuppdrag**.   Vi värnar ett hållbart arb... | x |  |  | 18 | [Beläggningsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/E7ax_HKq_oPy) |
| 26 | ...rag.   Vi värnar ett hållbart **arbetsliv** med starkt fokus på säkerhet,... | x | x | 9 | 9 | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| 27 | ...med starkt fokus på säkerhet, **personlig utveckling** och balans mellan jobb och fr... | x | x | 20 | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 28 | ...nber oss vänligen kontakt med **rekryterare** samt säljare av annons- eller... | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| 29 | ... kontakt med rekryterare samt **säljare** av annons- eller bemanningslö... | x |  |  | 7 | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| | **Overall** | | | **163** | **362** | 163/362 = **45%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Social hållbarhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/9MoM_m8y_Em5) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Beläggningsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/E7ax_HKq_oPy) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| x | x | x | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| x | x | x | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [landskapsarkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VryZ_dNm_o4a) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Asfalt, **keyword**](http://data.jobtechdev.se/taxonomy/concept/k9fW_Kv6_kf8) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| x |  |  | [Säljare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/rDcj_Tao_TJJ) |
| x | x | x | [HR-specialist/HR-advisor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s28Q_sya_S2b) |
| x | x | x | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| x | x | x | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| x | x | x | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **14** | 14/22 = **64%** |