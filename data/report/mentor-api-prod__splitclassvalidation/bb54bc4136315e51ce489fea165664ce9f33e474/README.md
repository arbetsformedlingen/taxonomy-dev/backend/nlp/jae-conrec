# Results for 'bb54bc4136315e51ce489fea165664ce9f33e474'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [bb54bc4136315e51ce489fea165664ce9f33e474](README.md) | 1 | 3187 | 9 | 18 | 80/296 = **27%** | 6/21 = **29%** |

## Source text

Operationskoordinator till öron-, näsa och hals mottagningen Verksamhetsområde öron-, näsa och halssjukdomar   Vi är Akademiska sjukhuset – ett av Sveriges ledande universitetssjukhus. Förutom rollen som länssjukhus är vi leverantör av högspecialiserad vård och betjänar två miljoner människor i Mellansverige. Hos oss pågår det något hela tiden – ett nytt liv ser dagens ljus, en mormor opereras, en kollega hyllas som livets hjälte. Vi tror att ärlighet varar längst och det är ingen hemlighet att vi behöver bli fler. Som kollega till oss får du vara med om mycket. Hårda prövningar men också fantastiska händelser, stark gemenskap och stöttning från kollegor och medmänniskor. För att se delar ur vår verklighet, besök vår fotoutställning på akademiskafotoutstallning.se.  Välkommen med din ansökan och bli en del av oss.  Vår verksamhet Inom vår verksamhet bedrivs akut, planerad specialistvård och högspecialiserad vård. De akuta besöken utgörs främst av patienter med svåra infektioner inom öron-, näs- och halsområdet, övre luftvägs- och sväljningsproblem, blödningar och yrsel. Den högspecialiserade vården, som till stor del är riks - och regionvård, innefattar patienter med tumörsjukdomar, luftvägssjukdomar och bihålesjukdomar, otokirurgiska sjukdomar och hörselsjukdomar.  Ditt uppdrag Som operationskoordinator ansvarar du för planering och samordning av patienter som ska genomgå operation och behandling. Du kommer vara personen som ansvarar för helheten kring planeringen. Det innebär kontakt med patienter, andra regioner, kontaktsjuksköterskor, operationsavdelning, vårdavdelning, röntgen o.s.v. Du har möjlighet att själv lägga upp och planera ditt arbete. Du förväntas ha ett mycket gott och nära samarbete med övriga två koordinatorer på avdelningen.  Dina kvalifikationer  Sjukvårdsutbildad, helst sjuksköterska men inget krav Några års erfarenhet inom vården är ett krav God datorvana, gärna erfarenhet av Cosmic och/eller Orbit Gärna erfarenhet från en administrativ och koordinerande roll Du ska vara kommunikativ och serviceinriktad Noggrann, strukturerad och lösningsorienterad Du behöver ha en professionell inställning med hög servicenivå och gilla att vara spindeln i nätet Du är en engagerad och glad kollega som bidrar till en bra arbetsmiljö.  Vi kommer lägga stor vikt vid personliga egenskaper  Vi erbjuder En tillsvidareanställning. Helst heltid men det kan eventuellt diskuteras. Tillträde enligt överenskommelse. Arbetstiden är dagtid måndag till fredag.  Hos oss får du förmåner som gör skillnad, läs om förmånerna här (https://regionuppsala.se/jobba-hos-oss/bli-var-nya-kollega/formaner/).  Vill du veta mer? Avdelningschef Helen Orest 018-611 52 70  Fackliga kontaktperson: Mariana Nordensvärd (Vårdförbundet) 018-6110000  Vill du jobba med oss? Välkommen med din ansökan via länken nedan.  Region Uppsala värdesätter de kvaliteter som jämn könsfördelning och mångfald tillför verksamheten. Vi ser därför gärna sökande av alla kön och med olika födelsebakgrund, funktionalitet och livserfarenhet.  Denna rekrytering sker helt genom Region Uppsalas försorg. Vi undanber oss därför telefonsamtal från rekryteringsföretag och annonsförsäljare.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ottagningen Verksamhetsområde **öron-, näsa och **halssjukdomar   Vi är Akadem... | x |  |  | 16 | [öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xh91_71f_LT4) |
| 2 | ...samhetsområde öron-, näsa och **halssjukdomar**   Vi är Akademiska sjukhuse... |  | x |  | 13 | [behandling av öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7kkE_kp7_oCm) |
| 2 | ...samhetsområde öron-, näsa och **halssjukdomar**   Vi är Akademiska sjukhuse... |  | x |  | 13 | [tolka diagnostiska tester rörande öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/CgKa_XQQ_joT) |
| 2 | ...samhetsområde öron-, näsa och **halssjukdomar**   Vi är Akademiska sjukhuse... |  | x |  | 13 | [Öron-, näs- och halssjukdomar, **skill**](http://data.jobtechdev.se/taxonomy/concept/WBhS_w1X_B3f) |
| 2 | ...samhetsområde öron-, näsa och **halssjukdomar**   Vi är Akademiska sjukhuse... |  | x |  | 13 | [använda specialiserade instrument för öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cKCz_NxU_2rt) |
| 2 | ...samhetsområde öron-, näsa och **halssjukdomar**   Vi är Akademiska sjukhuse... |  | x |  | 13 | [Läkarutbildning, öron- näs- och halssjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/syj1_KrL_qf2) |
| 2 | ...samhetsområde öron-, näsa och **halssjukdomar**   Vi är Akademiska sjukhuse... | x | x | 13 | 13 | [öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xh91_71f_LT4) |
| 3 | ... öron-, näsa och halssjukdomar** **  Vi är Akademiska sjukhuset ... | x |  |  | 2 | [öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xh91_71f_LT4) |
| 4 | ...samhet bedrivs akut, planerad **specialistvård** och högspecialiserad vård. De... |  | x |  | 14 | [specialistvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uXRf_eEH_Qwz) |
| 5 | ...ård, innefattar patienter med **tumörsjukdomar**, luftvägssjukdomar och bihåle... | x | x | 14 | 14 | [tumörsjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KnNJ_Z5E_H94) |
| 5 | ...ård, innefattar patienter med **tumörsjukdomar**, luftvägssjukdomar och bihåle... |  | x |  | 14 | [Läkarutbildning, tumörsjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/XfCk_9ZC_uvS) |
| 5 | ...ård, innefattar patienter med **tumörsjukdomar**, luftvägssjukdomar och bihåle... |  | x |  | 14 | [Läkarutbildning, gynekologiska tumörsjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h6TM_CiK_2fm) |
| 6 | ...patienter med tumörsjukdomar, **luftvägssjukdomar** och bihålesjukdomar, otokirur... |  | x |  | 17 | [diagnostisera luftvägssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kq2R_WYL_3Qh) |
| 7 | ...ed patienter, andra regioner, **kontaktsjuksköterskor**, operationsavdelning, vårdavd... | x | x | 21 | 21 | [Kontaktsjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/P255_NB8_Aa7) |
| 8 | ...terskor, operationsavdelning, **vårdavdelning**, röntgen o.s.v. Du har möjlig... | x | x | 13 | 13 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 9 | ...nära samarbete med övriga två **koordinatorer** på avdelningen.  Dina kvalifi... |  | x |  | 13 | [Koordinator, **job-title**](http://data.jobtechdev.se/taxonomy/concept/FfDD_gxA_Bsr) |
| 10 | ...ner  Sjukvårdsutbildad, helst **sjuksköterska** men inget krav Några års erfa... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 10 | ...ner  Sjukvårdsutbildad, helst **sjuksköterska** men inget krav Några års erfa... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 11 | ...munikativ och serviceinriktad **Noggrann**, strukturerad och lösningsori... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 12 | ...ollega som bidrar till en bra **arbetsmiljö**.  Vi kommer lägga stor vikt v... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 13 | ...ga egenskaper  Vi erbjuder En **tillsvidareanställning**. Helst heltid men det kan eve... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 14 | ...tillsvidareanställning. Helst **heltid** men det kan eventuellt diskut... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 15 | ...kan via länken nedan.  Region **Uppsala** värdesätter de kvaliteter som... |  | x |  | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| | **Overall** | | | **80** | **296** | 80/296 = **27%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [behandling av öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7kkE_kp7_oCm) |
|  | x |  | [tolka diagnostiska tester rörande öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/CgKa_XQQ_joT) |
|  | x |  | [Koordinator, **job-title**](http://data.jobtechdev.se/taxonomy/concept/FfDD_gxA_Bsr) |
| x | x | x | [tumörsjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KnNJ_Z5E_H94) |
| x | x | x | [Kontaktsjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/P255_NB8_Aa7) |
| x | x | x | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
|  | x |  | [Öron-, näs- och halssjukdomar, **skill**](http://data.jobtechdev.se/taxonomy/concept/WBhS_w1X_B3f) |
|  | x |  | [Läkarutbildning, tumörsjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/XfCk_9ZC_uvS) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
|  | x |  | [använda specialiserade instrument för öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cKCz_NxU_2rt) |
|  | x |  | [Läkarutbildning, gynekologiska tumörsjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h6TM_CiK_2fm) |
| x |  |  | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [diagnostisera luftvägssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kq2R_WYL_3Qh) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
|  | x |  | [Läkarutbildning, öron- näs- och halssjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/syj1_KrL_qf2) |
|  | x |  | [specialistvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uXRf_eEH_Qwz) |
| x | x | x | [öron-, näs- och halssjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xh91_71f_LT4) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **6** | 6/21 = **29%** |