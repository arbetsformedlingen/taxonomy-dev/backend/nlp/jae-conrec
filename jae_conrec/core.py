dev = False

## Load code
from jae_conrec.config import get_config
from pathlib import Path
from conrec_utils.env import default_env
from conrec_utils import jae_synonyms
from conrec_utils import jae_api
from conrec_utils import job
from conrec_utils import taxonomy
from conrec_utils import json_file_map
from conrec_utils import json_utils
from conrec_utils import common
from conrec_utils.common import ObjectWalker
from conrec_utils import annotated_document_dataset
from conrec_utils.csv_utils import string_column, int_column, CsvConfig
from conrec_utils import spec
from conrec_utils import time_utils
from jae_conrec import string_aligner
import conrec_utils.annotated_document_evaluation as ade
import os

def make_settings(cfg):
    return {"enriched_concept_labels_dir": os.path.join(cfg["dataroot"], "enriched_concept_labels"),
            "enriched_dataset_dir": os.path.join(cfg["dataroot"], "enriched_dataset"),
            "reconstructed_enrichment_dir": os.path.join(cfg["dataroot"], "reconstructed_dataset_enrichment"),
            "synonym2concept_frequency_filename": os.path.join(cfg["dataroot"], "synonym2concept_frequencies.json"),
            "result_dir": os.path.join(cfg["dataroot"], "results"),
            "report_dir": os.path.join(cfg["dataroot"], "report"),
            "step": 100}

def all_labels(concept):
    return [label
            for label_group in [concept["preferred_label"],
                                concept.get("alternative_labels", [])]
            for label in label_group]

def enriched_concept_labels_filename(settings):
    return os.path.join(settings["enriched_concept_labels_dir"], "enriched_concept_labels.json")

def concepts_per_synonym_table_filename(settings):
    return os.path.join(settings["enriched_concept_labels_dir"], "concepts_per_synonym_table.csv")


synonym_type2singular = {"competencies": "competence",
                         "occupations": "occupation",
                         "traits": "trait",
                         "geos": "geo"}

def make_synonym_key(synonym_type, synonym_label):
    return "{:s}/{:s}".format(synonym_type, synonym_label)

def with_synonym_key(m):
    return {**m, "synonym_key": make_synonym_key(m["synonym_type"], m["synonym_label"])}

def enrich_and_save_concept_labels(tax, settings):
    def labels_of_interest(c):
        return [c["preferred_label"]]
    
    flat_data_to_enrich = [{**c, "concept_preferred_label": label, "label_key": "{:s}/{:d}".format(c["id"], i)}
                           for c in tax.concept_list
                           for (i, label) in enumerate(labels_of_interest(c))]

    label_key2src = {x["label_key"]:x for x in flat_data_to_enrich}
    
    n = len(flat_data_to_enrich)
    jobdir = os.path.join(settings["enriched_concept_labels_dir"], "jobs")
    
    jobs = []
    step = settings["step"]
    lower = 0

    result_map = json_file_map.JsonFileMap(jobdir)

    jae_enricher = jae_api.JobadEnricher()
    
    def enrich_slice(l, u):
        print("Enrich slice {:d}, {:d}".format(l, u))
        assert(l < u)
        subdata = flat_data_to_enrich[l:u]
        docs = []
        for x in subdata:
            doc = jae_api.preclean_jae_document_input(
                jae_api.jae_document_input(x["label_key"], "", x["concept_preferred_label"]))
            docs.append(doc)
            
        assert(0 < len(docs))
        return jae_enricher.enrich_documents(docs)
    
    while lower < n:
        upper = min(n, lower + step)
        jobs.append(job.FunctionJob(
            "enrich{:d}_{:d}".format(lower, upper),
            lambda bds: enrich_slice(bds[0], bds[1])).with_data((lower, upper)))
        lower = upper

    job.JobRunner(result_map).run_jobs(jobs)

    flat_result = [with_synonym_key({"concept_id": src["id"],
                                     "concept_preferred_label": src["concept_preferred_label"],
                                     "synonym_type": synonym_type2singular[synonym_type],
                                     "synonym_label": candidate["concept_label"],
                                     "term": candidate["term"]})
                   for batch in result_map.values()
                   for result in batch
                   for synonym_type in result["enriched_candidates"]
                   for candidate in result["enriched_candidates"][synonym_type]
                   for src in [label_key2src[result["doc_id"]]]]

    filename = enriched_concept_labels_filename(settings)
    print("Save to " + filename)
    
    json_utils.write_json(filename, flat_result)
    return flat_result

concept_match_spec = spec.KeysSpec({"concept_id": spec.str_spec,
                                    "concept_preferred_label": spec.str_spec,
                                    "concept_type": spec.str_spec,
                                    "match_type": spec.str_spec})
concept_matches_spec = spec.CollSpec(concept_match_spec)
concept_ids_spec = spec.CollSpec(spec.str_spec)

def get_best_concept_ids_from_frequencies(freqs):
    max_freq = 0
    for (k, v) in freqs.items():
        max_freq = max(max_freq, v)
    return [k for (k, v) in freqs.items() if ((v == max_freq) and (k != ""))]


def annotations_from_matched_candidate(cand):
    return [{"concept-id": m["concept_id"],
             "type": m["concept_type"],
             "start-position": start_position,
             "end-position": end_position}
            for m in cand["concept_matches"]
            for (start_position, end_position) in common.segments_from_inds([i for i in get_src_inds(cand) if i != None])]

class Processor(ade.AbstractAlgorithm):
    def __init__(self, tax, concepts_per_synonym, concept_per_synonym_frequencies, settings):
        concepts_per_synonym_spec.check(concepts_per_synonym)
        self.concept_per_synonym_frequencies = concept_per_synonym_frequencies
        self.tax = tax
        self.concepts_per_synonym = concepts_per_synonym
        self.enricher = jae_api.JobadEnricher()
        self.settings = settings
        self.synonym2concepts = {(x["synonym_type"], x["synonym_label"]):x["candidates"]
                                 for x in self.concepts_per_synonym}
        self.src_enrichment_reconstructor = SourceEnrichmentReconstructor()

        self.table_match_counter = 0
        self.freq_match_counter = 0


    def get_concept_matches(self, candidate):
        synonym_key = candidate["synonym_key"]
        freqs = self.concept_per_synonym_frequencies.get(synonym_key, {})


        if len(freqs) == 0:
            s2c_key = (candidate["synonym_type"], candidate["concept_label"])
            cands =  self.synonym2concepts.get(s2c_key, [])
            
            self.table_match_counter += 1

            return concept_matches_spec.check([{**c, "match_type": "table"}
                                               for c in cands])
        else:
            best_concept_ids = get_best_concept_ids_from_frequencies(freqs)

            self.freq_match_counter += 1
            
            return concept_matches_spec.check(
                [{"concept_id": cid,
                  "concept_type": concept["type"],
                  "concept_preferred_label": concept["preferred_label"],
                  "match_type": "freq"}
                    for cid in best_concept_ids
                    for concept in [self.tax.concept_map.get(cid, None)]
                    if concept != None])
        
    def match_candidate(self, candidate):
        concept_matches = self.get_concept_matches(candidate)
        concept_matches_spec.check(concept_matches)
        return {**candidate, "concept_matches": concept_matches}

    def render_matched_candidates_csv(self, dst_filename, matched_candidates):
        cols = [int_column("sentence-index"),
                string_column("sentence"),
                string_column("term"),
                string_column("synonym-label"),
                string_column("synonym-type"),
                string_column("concept-type"),
                string_column("concept-label"),
                string_column("match-type")]

        def csv_dict(c, concept_match):
            concept_match_spec.check(concept_match)
            
            concept = self.tax.concept_map[concept_match["concept_id"]]
            return {"sentence-index": c["sentence_index"],
                    "sentence": c["sentence"],
                    "term": c["term"],
                    "synonym-label": c["concept_label"],
                    "synonym-type": c["synonym_type"],
                    "concept-type": concept["type"],
                    "concept-label": concept_match["concept_preferred_label"],
                    "match-type": concept_match["match_type"]}
        
        CsvConfig(cols).write_dicts(
            dst_filename,
            [csv_dict(c, match)
             for c in matched_candidates
             for match in c["concept_matches"]])

    def annotations_from_text_and_jae_result(self, text, jae_result):
        reconstruction = self.src_enrichment_reconstructor.reconstruct(text, jae_result)
        
        sentences = jae_result["doc_sentences"]
        flat_candidates = jae_api.flat_candidates(reconstruction)
        matched_candidates = [mc
                              for c in flat_candidates
                              for mc in [self.match_candidate(c)]
                              if 0 < len(mc["concept_matches"])]
        annotations = [a
                       for c in matched_candidates
                       for a in annotations_from_matched_candidate(c)]
        return {"annotations": annotations}
        
    def process_dataset_item_result(self, dataset_item, jae_result):
        text = dataset_item.data["text"]
        return annotations_from_text_and_jae_result(text, jae_result)

    def process_text(self, text):
        jae_enricher = jae_api.JobadEnricher()
        jae_result = jae_enricher.enrich_documents([jae_api.preclean_jae_document_input(jae_api.jae_document_input("id", "", text))])[0]
        return self.annotations_from_text_and_jae_result(text, jae_result)

    def preprocess_for_dataset(self, evaluation_context, dataset):
        jae_input_docs = [jae_api.jae_document_input(item.id, "", item.data["text"])
                          for item in dataset]
        path = evaluation_context.result_repo.root_path.joinpath("enriched_tmp")
        enriched_map = json_file_map.JsonFileMap(str(path))
        batch_enrich(self.enricher, enriched_map, jae_input_docs)
        return enriched_map

    def process_dataset_item(self, evaluation_context, enriched_map, item):
        k = item.id
        return self.process_dataset_item_result(item, enriched_map[k])
        
synonym_concept_type_cost_map = {("competence", "skill"): 0,
                                 ("competence", "esco-skill"): 1,
                                 ("competence", "skill-headline"): 2,
                                 ("competence", "keyword"): 3,
                                 
                                 ("occupation", "occupation-name"): 0,
                                 ("occupation", "esco-occupation"): 1,
                                 ("occupation", "occupation-field"): 2,
                                 ("occupation", "occupation-collection"): 2,
                                 ("occupation", "keyword"): 3}
        
def synonym_concept_type_cost(synonym_type, concept_type):
    return synonym_concept_type_cost_map.get((synonym_type, concept_type), 1000)

def process_concept_candidates(tax, candidates):
    synonym_type = candidates[0]["synonym_type"]
    concept_id_map = common.group_by("concept_id", candidates)
    return list(sorted([{"concept_id": concept_id,
                         "concept_preferred_label": concept["preferred_label"],
                         "concept_type": concept["type"],
                         "label_count": len(v),
                         "type_cost": synonym_concept_type_cost(synonym_type, concept["type"]),
                         "candidate_annotation": ""}
            for (concept_id, v) in concept_id_map.items() if concept_id in tax.concept_map
            for concept in [tax.concept_map[concept_id]]], key=lambda c: (c["type_cost"], len(c["concept_preferred_label"]))))

concepts_per_synonym_spec = spec.CollSpec(spec.KeysSpec({
    "synonym_key": spec.str_spec,
    "synonym_type": spec.str_spec,
    "synonym_label": spec.str_spec,
    "candidates": spec.CollSpec(spec.KeysSpec({
        "concept_id": spec.str_spec,
        "concept_preferred_label": spec.str_spec,
        "concept_type": spec.str_spec,
        "label_count": spec.int_spec,
        "type_cost": spec.int_spec,
        "candidate_annotation": spec.str_spec}))}))

synonym_keys = ["synonym_key", "synonym_label", "synonym_type"]

def concepts_per_synonym(tax, enriched_concept_labels):
    result = list(sorted(
        [{"synonym_key": k,
          "synonym_type": v[0]["synonym_type"],
          "synonym_label": v[0]["synonym_label"],
          "candidates": process_concept_candidates(tax, v)}
              for (k, v) in common.group_by("synonym_key", enriched_concept_labels).items()],
        key=lambda x: (x["candidates"][0]["type_cost"], -len(x["candidates"]), x["synonym_key"])))
    concepts_per_synonym_spec.check(result)
    return result

def flatten_concepts_per_synonym(cps):
    return [{**common.remove_keys(element, ["candidates"]), **c}
            for element in cps
            for (i, c) in enumerate(element["candidates"])]

def unflatten_concepts_per_synonym(cps):
    grouped = {k:{**common.select_keys(v[0], synonym_keys),
                  "candidates": [common.remove_keys(x, synonym_keys) for x in v]}
               for (k, v) in common.group_by("synonym_key", cps).items()}

    visited = set()
    dst = []
    for x in cps:
        synonym_key = x["synonym_key"]
        if not(synonym_key in visited):
            visited.add(synonym_key)
            dst.append(grouped[synonym_key])
    assert(len(dst) == len(grouped))
    concepts_per_synonym_spec.check(dst)
    return dst

concepts_per_synonym_csv_config = CsvConfig([
    string_column("synonym_key"),
    string_column("synonym_type"),
    string_column("synonym_label"),
    string_column("concept_id"),
    string_column("concept_preferred_label"),
    string_column("concept_type"),
    int_column("label_count"),
    int_column("type_cost"),
    string_column("candidate_annotation")
])

def write_flat_concepts_per_synonym(filename, cps):
    concepts_per_synonym_csv_config.write_dicts(filename, cps)

def read_flat_concepts_per_synonym(filename):
    return concepts_per_synonym_csv_config.read_dicts(filename)
    
def batch_enrich(enricher, result_map, document_inputs, step=10):
    
    n = len(document_inputs)
    lower = 0
    slices = []
    while lower < n:
        upper = min(n, lower + step)
        slices.append(document_inputs[lower:upper])
        lower = upper

    print("Enrich {:d} documents in {:d} slices".format(n, len(slices)))
    prog = time_utils.ProgressReporter(len(slices))
    for docs in slices:
        results = enricher.enrich_documents(docs)
        for result in results:
            result_map[result["doc_id"]] = result
        prog.end_of_iteration_message("Batch enrich into " + str(result_map))

def enriched_dataset_map(settings):
    return json_file_map.JsonFileMap(settings["enriched_dataset_dir"])

def reconstructed_enrichment_map(settings):
    return json_file_map.JsonFileMap(settings["reconstructed_enrichment_dir"])

# Analyze an ad
def enrich_dataset(result_map, dataset, step):
    #spec.str_spec.check(output_dir)
    assert(isinstance(dataset, annotated_document_dataset.Dataset))
    spec.int_spec.check(step)
    documents = [jae_api.preclean_jae_document_input(jae_api.jae_document_input(data["sha1"], "", data["text"]))
                 for item in dataset
                 for data in [item.data]]
    batch_enrich(jae_api.JobadEnricher(), result_map, documents, step)



def compress_inds_to_range(inds):
    n = len(inds)
    if n == 0:
        return inds
    
    lower = inds[0]

    inds_map = {"src_inds": inds}
    
    if lower == None:
        return inds_map

    for i in range(n):
        if inds[i] != lower + i:
            inds_map
            
    return {"src_range": [lower, lower + n]}

def get_src_inds(candidate):
    rng = candidate.get("src_range")
    if rng != None:
        (l, u) = rng
        return list(range(l, u))

    return candidate["src_inds"]
        
class SourceEnrichmentReconstructor:
    def __init__(self):
        self.lower_sentence_index = 1
        self.string_mapping_settings = string_aligner.StringMappingSettings()
        
    def reconstruct(self, source_text, enrichment_data):
        doc_sentences = enrichment_data["doc_sentences"]
        sentences = [sentence if self.lower_sentence_index <= i else "" for (i, sentence) in enumerate(doc_sentences)]
        sentence_mapping = string_aligner.jae_map_sentences(source_text, sentences, self.string_mapping_settings)

        def reconstruct_candidate(c):
            sindex = c["sentence_index"]
            if c["sentence"] != doc_sentences[sindex]:
                print("Inconsistent sentence data at item {:s}, sentence index {:d}".format(enrichment_data["doc_id"], sindex))
                return common.select_keys(c, ["sentence_index"])
            
            sentence = c["sentence"].lower()

            m = string_aligner.jae_map_string(sentence, c["term"], self.string_mapping_settings)
            sinds = sentence_mapping.sentence_inds[sindex]
            source_inds = [None if i == None else sinds[i] for i in m.inds]

            return {**c, **compress_inds_to_range(source_inds)}
        
        new_enriched_candidates = {synonym_type:[reconstruct_candidate(candidate) for candidate in candidates]
                                   for (synonym_type, candidates) in enrichment_data["enriched_candidates"].items()}

        return {"doc_id": enrichment_data["doc_id"], "source_text": source_text, "enriched_candidates": new_enriched_candidates}
            
                

def synonym_to_taxonomy_concept_frequency(dataset, reconstructed_source_enrichments):
    dst = {}
    ow = ObjectWalker(dst)
    for item in dataset:
        concept_annotations = [x for x in item.data["annotations"] if x["concept-id"] != None]
        achars = ade.annotated_characters_from_annotations(concept_annotations)
        annot_map = common.group_by(lambda c: c.character_index, achars)
        rec = reconstructed_source_enrichments[item.id]
        candidates = jae_api.flat_candidates(rec)
        for candidate in candidates:
            inds = get_src_inds(candidate)
            cand_concept_ids = {char.concept_id
                                for i in inds
                                for char in annot_map.get(i, [])}
            owc = ow.new_or_existing(candidate["synonym_key"], {})

            if len(cand_concept_ids) == 0:
                owc.update("", common.inc, 0)
            else:
                for cid in cand_concept_ids:
                    owc.update(cid, common.inc, 0)
    return dst

class JaeConrecApi:
    def __init__(self):
        self.config = get_config()
        self.env = default_env()
        self.settings = make_settings(self.config)

    def enrich_concept_labels(self):
        tax = taxonomy.load_taxonomy(self.env)
        enrich_and_save_concept_labels(tax, self.settings)

    def build_synonym_to_concept_frequencies(self):
        full_dataset = self.get_full_dataset()
        trainsub = full_dataset.train_subset()

        # Enrich the dataset
        enrich_dataset(enriched_dataset_map(self.settings), trainsub, 10)

        # Reconstruct sources
        rec = SourceEnrichmentReconstructor()
        src = enriched_dataset_map(self.settings)
        rem = reconstructed_enrichment_map(self.settings)
        prog = time_utils.ProgressReporter(len(trainsub))
        for x in trainsub:
            item_id = x.id
            rem[item_id] = rec.reconstruct(x.data["text"], src[item_id])
            prog.end_of_iteration_message("Reconstructing source enrichment")

        # Build frequeny table
        freq = synonym_to_taxonomy_concept_frequency(trainsub, rem)
        json_utils.write_json(self.settings["synonym2concept_frequency_filename"], freq)
        
    def build_concepts_per_synonym_table(self):
        tax = taxonomy.load_taxonomy(self.env)
        enriched_concept_labels = json_utils.read_json(enriched_concept_labels_filename(self.settings))
        write_flat_concepts_per_synonym(
            concepts_per_synonym_table_filename(self.settings),
            flatten_concepts_per_synonym(concepts_per_synonym(tax, enriched_concept_labels)))

    def get_full_dataset(self):
        repo = annotated_document_dataset.load_known_repository(self.env, "mentor-api-prod")
        return repo.full_dataset()

    def result_repo(self):
        return ade.ResultRepository(Path(self.settings["result_dir"]))

    def make_processor(self):
        tax = taxonomy.load_taxonomy(self.env)
        flat_cps = read_flat_concepts_per_synonym(concepts_per_synonym_table_filename(self.settings))
        cps = unflatten_concepts_per_synonym(flat_cps)
        cps_freq = json_utils.read_json(self.settings["synonym2concept_frequency_filename"])
        proc = Processor(tax, cps, cps_freq, self.settings)
        return proc
    
    def process_dataset(self):
        ds = self.get_full_dataset().validation_subset()
        ade.evaluate_algorithm(self.result_repo(), [ds], self.make_processor())

    def start_server(self):
        ade.MentorApiHttpServer(self.make_processor()).run_dev()
        
    def make_report(self):
        writer = ade.MarkdownReportWriter(Path(self.settings["report_dir"]))
        tax = taxonomy.load_taxonomy(self.env)
        context = ade.ReportingContext(self.env, tax).with_writer(writer)
        context.render_report(self.result_repo())


## Process the datasaet
if dev:
    
    print("table match counter: {:d}".format(proc.table_match_counter))
    print("freq match counter: {:d}".format(proc.freq_match_counter))
    
    print("Processed")

## 0. Load the synonyms

if dev:
    config = get_config()
    env = default_env()
    settings = make_settings(config)


## Load stuff
if dev:
    tax = taxonomy.load_taxonomy(env)
    enriched_concept_labels = json_utils.read_json(enriched_concept_labels_filename(settings))

    

## Load the data

if dev:
    repo = annotated_document_dataset.load_known_repository(env, "mentor-api-prod")
    full_dataset = repo.full_dataset()
    print("Loaded")

## Activate dev

dev = True
