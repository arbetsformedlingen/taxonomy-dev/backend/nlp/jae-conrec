# Results for '3b210407fa80990e19aeca74b5146e9da2fa1930'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [3b210407fa80990e19aeca74b5146e9da2fa1930](README.md) | 1 | 133 | 3 | 2 | 22/25 = **88%** | 1/2 = **50%** |

## Source text

Pizzabagare sökes Vi söker erfaren pizzabagare till Jacks Corner. Vid intresse, kontakta Yacoub Hedo på Mail: yacoub.hedo65@gmail.com

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Pizzabagare** sökes Vi söker erfaren pizzab... | x | x | 11 | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 2 | ...bagare sökes Vi söker erfaren **pizzabagare** till Jacks Corner. Vid intres... | x | x | 11 | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 3 | ... på Mail: yacoub.hedo65@gmail.**com** | x |  |  | 3 | [Object Component Model/COM, **skill**](http://data.jobtechdev.se/taxonomy/concept/Np6W_aQg_Rkn) |
| | **Overall** | | | **22** | **25** | 22/25 = **88%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Object Component Model/COM, **skill**](http://data.jobtechdev.se/taxonomy/concept/Np6W_aQg_Rkn) |
| x | x | x | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| | | **1** | 1/2 = **50%** |