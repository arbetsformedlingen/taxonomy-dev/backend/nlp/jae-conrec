# Results for 'cd1cf92ab4fb9b56c9d7c9908e6df6f3ffd5af06'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [cd1cf92ab4fb9b56c9d7c9908e6df6f3ffd5af06](README.md) | 1 | 4751 | 35 | 37 | 268/664 = **40%** | 17/33 = **52%** |

## Source text

Arbetsterapeut inom reumatologi Skaraborgs Sjukhus (SkaS) har verksamhet på flera orter i Skaraborg bl.a. i Falköping, Lidköping, Mariestad och Skövde. Vi ger specialistsjukvård inom ett trettiotal specialiteter och arbetar tillsammans för patientens hälsa genom livet, där vi erbjuder en sammanhållen och trygg vård. Varje dag och varje timma gör våra medarbetare insatser som påverkar andra människors liv. Vi bryr oss om dig som gör skillnad för andra och tillsammans skapar vi framtidens arbetsplats.    Beskrivning Arbetsterapi/Fysioterapi på Skaraborgs Sjukhus är en sjukhusövergripande funktion som tillhandahåller specialiserad arbetsterapi och fysioterapi till sjukhusets verksamheter.  Vi är ca 100 medarbetare med både specialisering och bredd i yrket. Vi finns på två sjukhusorter - Skövde och Lidköping. Vår verksamhet är under ständig utveckling och var dag möts vi av nya utmaningar med möjlighet att påverka vårt nuvarande och framtida arbete. För att klara av detta ges medarbetare goda möjligheter till interna och externa utvecklings-/ utbildningsinsatser. Drivs du av att göra skillnad för dina medmänniskor, att ständigt utvecklas i din profession och att bidra med just din goda kompetens i en arbetsgrupp med bred kompetens i olika åldrar där vi är måna om varandra? Vill du vara en del i vår framtid så tycker vi att du ska söka dig till oss!    Arbetsuppgifter Vi söker nu en leg arbetsterapeut inom området reumatologi. I denna grupp arbetar ytterligare tre arbetsterapeuter. I det dagliga arbetet har du som ansvarig arbetsterapeut en central roll i teamet. Du möter äldre och yngre med reumatiska sjukdomar som är i behov av specialiserade insatser inom dag- och öppenvård. Patienten är i centrum och du arbetar i nära samarbete med läkare, fysioterapeut, sjuksköterska och undersköterska. Du arbetar självständigt med bedömning och behandling av dessa patienter. I ditt arbete kommer du att ha en viktig roll i samverkan både i den egna förvaltningen men också till andra vårdaktörer så som kommun, primärvård och Försäkringskassa. Verksamheten har samarbete med högskolor runt om i landet och i dina arbetsuppgifter ingår det därför även att handleda arbetsterapeutstudenter som är under utbildning. Tjänsten innebär också att vid behov vara flexibel och kunna ingå helt eller delvis inom den slutenvård som finns på sjukhuset.    Kvalifikationer Vi söker dig som är legitimerad arbetsterapeut med lägst kandidatexamen, gärna med erfarenhet av arbetsområdet. Goda kunskaper i skrift och tal i det svenska språket är ett krav. Du är en person som trivs med nya utmaningar som inte är rädd för att bredda din kompetens. Vidare ser vi att du har god förmåga att prioritera i ditt arbete. Flexibilitet i både arbetssätt, arbetsuppgifter och att du är van att arbeta självständigt under eget ansvar är viktigt. Vi kommer att lägga stor vikt vid personlig lämplighet.    Villkor Vi har en mycket god arbetsmiljö vilket vi arbetar aktivt med och har en mycket bra sammanhållning. Du har nära till många kollegor med mycket god kunskap och kompetens inom flera områden, vilket är en stor styrka hos oss. Möjlighet till fortbildning är stor hos oss. Vi har, inom enheten, ett mentorskapsprogram där du kan ges möjlighet att delta som mentor eller adept. Rekrytering kan ske under ansökningstiden.  Om Västra Götalandsregionen Västra Götalandsregionen finns till för människorna i Västra Götaland. Vi ser till att det finns god hälso- och sjukvård för alla. Vi arbetar för en hållbar utveckling och tillväxt, bra miljö, förbättrad folkhälsa, ett rikt kulturliv och goda kommunikationer i hela Västra Götaland. Västra Götalandsregionen arbetar aktivt för att digitalisera fler arbetssätt. För att vara en del av den digitala omvandlingen har du med dig grundläggande färdigheter och kan använda digitala verktyg och tjänster. Du kan söka information, kommunicera, interagera digitalt, är riskmedveten och har motivation att delta i utvecklingen för att lära nytt.   Vill du veta mer om Västra Götalandsregionen kan du besöka vår introduktion till nya medarbetare på länken https://www.vgregion.se/introduktion   Ansökan Västra Götalandsregionen ser helst att du registrerar din ansökan via rekryteringssystemet. Om du som sökande har frågor om den utannonserade tjänsten eller av särskilda och speciella skäl inte kan registrera dina uppgifter i ett offentligt system - kontakta kontaktperson för respektive annons.   Till bemannings-, förmedlings- och rekryteringsföretag och till dig som är försäljare: Vi undanber oss vänligen men bestämt direktkontakt med bemannings-, förmedlings- och rekryteringsföretag samt andra externa aktörer och försäljare av ytterligare jobbannonser. Västra Götalandsregionen har upphandlade avtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Arbetsterapeut** inom reumatologi Skaraborgs S... | x | x | 14 | 14 | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| 2 | Arbetsterapeut inom **reumatologi** Skaraborgs Sjukhus (SkaS) har... | x | x | 11 | 11 | [reumatologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iuMT_v6w_ESG) |
| 3 | ...era orter i Skaraborg bl.a. i **Falköping**, Lidköping, Mariestad och Skö... | x | x | 9 | 9 | [Falköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ZySF_gif_zE4) |
| 4 | ... Skaraborg bl.a. i Falköping, **Lidköping**, Mariestad och Skövde. Vi ger... | x | x | 9 | 9 | [Lidköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/FN1Y_asc_D8y) |
| 5 | ...bl.a. i Falköping, Lidköping, **Mariestad** och Skövde. Vi ger specialist... | x | x | 9 | 9 | [Mariestad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Lzpu_thX_Wpa) |
| 6 | ...ing, Lidköping, Mariestad och **Skövde**. Vi ger specialistsjukvård in... | x | x | 6 | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 7 | ...ar tillsammans för patientens **hälsa** genom livet, där vi erbjuder ... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 8 | ...   Beskrivning Arbetsterapi/**Fysioterapi** på Skaraborgs Sjukhus är en s... |  | x |  | 11 | [Fysioterapi (generell examen), **keyword**](http://data.jobtechdev.se/taxonomy/concept/r2Fr_sgm_z6u) |
| 9 | ...i finns på två sjukhusorter - **Skövde** och Lidköping. Vår verksamhet... | x | x | 6 | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 10 | ...två sjukhusorter - Skövde och **Lidköping**. Vår verksamhet är under stän... | x | x | 9 | 9 | [Lidköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/FN1Y_asc_D8y) |
| 11 | ...suppgifter Vi söker nu en leg **arbetsterapeut** inom området reumatologi. I d... | x | x | 14 | 14 | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| 12 | ...g arbetsterapeut inom området **reumatologi**. I denna grupp arbetar ytterl... | x | x | 11 | 11 | [reumatologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iuMT_v6w_ESG) |
| 13 | ...grupp arbetar ytterligare tre **arbetsterapeuter**. I det dagliga arbetet har du... | x |  |  | 16 | [Arbetsterapeuter, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/TPH4_2AM_isT) |
| 13 | ...grupp arbetar ytterligare tre **arbetsterapeuter**. I det dagliga arbetet har du... |  | x |  | 16 | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| 14 | ...a arbetet har du som ansvarig **arbetsterapeut** en central roll i teamet. Du ... | x | x | 14 | 14 | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| 15 | ... Du möter äldre och yngre med **reumatiska sjukdomar** som är i behov av specialiser... |  | x |  | 20 | [Läkarutbildning, reumatiska sjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/FDe7_DGz_Bn6) |
| 15 | ... Du möter äldre och yngre med **reumatiska sjukdomar** som är i behov av specialiser... |  | x |  | 20 | [diagnostisera reumatiska sjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dQGM_BjW_P6a) |
| 16 | ... arbetar i nära samarbete med **läkare**, fysioterapeut, sjuksköterska... | x |  |  | 6 | [Läkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9K3i_XD4_Kyy) |
| 16 | ... arbetar i nära samarbete med **läkare**, fysioterapeut, sjuksköterska... |  | x |  | 6 | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| 17 | ... i nära samarbete med läkare, **fysioterapeut**, sjuksköterska och undersköte... | x | x | 13 | 13 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 17 | ... i nära samarbete med läkare, **fysioterapeut**, sjuksköterska och undersköte... |  | x |  | 13 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 18 | ...te med läkare, fysioterapeut, **sjuksköterska** och undersköterska. Du arbeta... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 18 | ...te med läkare, fysioterapeut, **sjuksköterska** och undersköterska. Du arbeta... | x |  |  | 13 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 19 | ...ioterapeut, sjuksköterska och **undersköterska**. Du arbetar självständigt med... | x | x | 14 | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 20 | ...terska och undersköterska. Du **arbetar självständigt** med bedömning och behandling ... | x |  |  | 21 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 21 | ...ra vårdaktörer så som kommun, **primärvård** och Försäkringskassa. Verksam... | x | x | 10 | 10 | [primärvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WvGv_6YE_mun) |
| 22 | ...så som kommun, primärvård och **Försäkringskassa**. Verksamheten har samarbete m... |  | x |  | 16 | [Försäkringshandläggare, försäkringskassa, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2AGe_heZ_E94) |
| 22 | ...så som kommun, primärvård och **Försäkringskassa**. Verksamheten har samarbete m... |  | x |  | 16 | [Platschef, försäkringskassa/Områdeschef, försäkringskassa, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2Eib_vX9_79R) |
| 22 | ...så som kommun, primärvård och **Försäkringskassa**. Verksamheten har samarbete m... |  | x |  | 16 | [Försäkringsutredare, försäkringskassa, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YkcC_EmP_nA5) |
| 22 | ...så som kommun, primärvård och **Försäkringskassa**. Verksamheten har samarbete m... |  | x |  | 16 | [försäkringsutredare, försäkringskassa, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/sbzk_iTR_bRb) |
| 23 | ... söker dig som är legitimerad **arbetsterapeut** med lägst kandidatexamen, gär... | x | x | 14 | 14 | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| 24 | ...erad arbetsterapeut med lägst **kandidatexamen**, gärna med erfarenhet av arbe... |  | x |  | 14 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 24 | ...erad arbetsterapeut med lägst **kandidatexamen**, gärna med erfarenhet av arbe... | x | x | 14 | 14 | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| 25 | ...skaper i skrift och tal i det **svenska** språket är ett krav. Du är en... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 26 | ...i skrift och tal i det svenska** språket** är ett krav. Du är en person ... | x |  |  | 8 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 27 | ...pgifter och att du är van att **arbeta självständigt** under eget ansvar är viktigt.... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 28 | ... Villkor Vi har en mycket god **arbetsmiljö** vilket vi arbetar aktivt med ... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 29 | ...ke under ansökningstiden.  Om **Västra Götalandsregionen** Västra Götalandsregionen finn... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 30 | ...  Om Västra Götalandsregionen **Västra Götaland**sregionen finns till för männi... | x | x | 15 | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 31 | ...alandsregionen Västra Götaland**sregionen** finns till för människorna i ... | x |  |  | 9 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 32 | ... finns till för människorna i **Västra Götaland**. Vi ser till att det finns go... | x |  |  | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 33 | ...Vi ser till att det finns god **hälso- oc**h sjukvård för alla. Vi arbeta... | x | x | 9 | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 34 | ...ll att det finns god hälso- oc**h sjukvård** för alla. Vi arbetar för en h... | x |  |  | 10 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 35 | ...d för alla. Vi arbetar för en **hållbar utveckling** och tillväxt, bra miljö, förb... | x |  |  | 18 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 36 | ...llväxt, bra miljö, förbättrad **folkhälsa**, ett rikt kulturliv och goda ... | x | x | 9 | 9 | [folkhälsa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jEdA_1Fe_Jji) |
| 37 | ...h goda kommunikationer i hela **Västra Götaland**. Västra Götalandsregionen arb... | x | x | 15 | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 38 | ...ioner i hela Västra Götaland. **Västra Götalandsregionen** arbetar aktivt för att digita... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 39 | ...eter och kan använda digitala **verktyg** och tjänster. Du kan söka inf... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 40 | ...a nytt.   Vill du veta mer om **Västra Götalandsregionen** kan du besöka vår introduktio... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 41 | ...ion.se/introduktion   Ansökan **Västra Götalandsregionen** ser helst att du registrerar ... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| | **Overall** | | | **268** | **664** | 268/664 = **40%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Försäkringshandläggare, försäkringskassa, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2AGe_heZ_E94) |
|  | x |  | [Platschef, försäkringskassa/Områdeschef, försäkringskassa, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2Eib_vX9_79R) |
| x | x | x | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [Läkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9K3i_XD4_Kyy) |
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
|  | x |  | [Läkarutbildning, reumatiska sjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/FDe7_DGz_Bn6) |
| x | x | x | [Lidköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/FN1Y_asc_D8y) |
| x |  |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| x | x | x | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| x | x | x | [Mariestad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Lzpu_thX_Wpa) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| x |  |  | [Arbetsterapeuter, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/TPH4_2AM_isT) |
| x | x | x | [primärvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WvGv_6YE_mun) |
|  | x |  | [Försäkringsutredare, försäkringskassa, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YkcC_EmP_nA5) |
| x | x | x | [Falköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ZySF_gif_zE4) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
|  | x |  | [diagnostisera reumatiska sjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dQGM_BjW_P6a) |
| x | x | x | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| x | x | x | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| x | x | x | [reumatologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/iuMT_v6w_ESG) |
| x | x | x | [folkhälsa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jEdA_1Fe_Jji) |
| x |  |  | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
|  | x |  | [Fysioterapi (generell examen), **keyword**](http://data.jobtechdev.se/taxonomy/concept/r2Fr_sgm_z6u) |
| x | x | x | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
|  | x |  | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
|  | x |  | [försäkringsutredare, försäkringskassa, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/sbzk_iTR_bRb) |
|  | x |  | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| x | x | x | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| x | x | x | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| | | **17** | 17/33 = **52%** |