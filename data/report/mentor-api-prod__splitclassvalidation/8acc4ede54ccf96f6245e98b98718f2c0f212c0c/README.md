# Results for '8acc4ede54ccf96f6245e98b98718f2c0f212c0c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8acc4ede54ccf96f6245e98b98718f2c0f212c0c](README.md) | 1 | 1132 | 8 | 7 | 79/83 = **95%** | 3/4 = **75%** |

## Source text

Nagelterapeut nagelteknolog sökes Hejsan nu söker vi en ny nagelteknolog/nagelteraput som kan axla rollen på vår salong My Corner på väster i örebro.  vi har legat här i fryra år nu i nyrenoverade lokaler. Här välkomnar vi alla unga som gamla kunder… vi samarbetar som ett team och familj, då vi hjälps ått med allt för att trivas tillsammans  vi söker dig som kan hyra in dig hos oss då vår nuvarande nageltjej ska börja studera o ändra inriktning. Hon har en stor kundkrets som består  av nöjda kunder som kräver noggrannhet och skicklighet . essys naglar vi önskar att du jobbar med gelé.  Idagsläget jobbar hon med light elegance som är något många kunder är supernöjda med .  vår huvudsak och specialitet är att vi tar hand om våra kunder och är lyhörda vad de önskar o har funderingar över. Service,ärlighet,noggrannhet och omhändertagande är ett måste.  gå gärna in på vår hemsida och läs mer om oss .kontakta gärna mig  sofia rylen  070-5551739           info@mycorner.nu Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Nagelterapeut** nagelteknolog sökes Hejsan nu... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 2 | Nagelterapeut **nagelteknolog** sökes Hejsan nu söker vi en n... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 3 | ...ökes Hejsan nu söker vi en ny **nagelteknolog**/nagelteraput som kan axla rol... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 4 | ... söker vi en ny nagelteknolog/**nagelteraput** som kan axla rollen på vår sa... | x | x | 12 | 12 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 5 | ... salong My Corner på väster i **örebro**.  vi har legat här i fryra år... | x | x | 6 | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 6 | ...r  av nöjda kunder som kräver **noggrannhet** och skicklighet . essys nagla... | x | x | 11 | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 7 | ...r vi önskar att du jobbar med **gelé**.  Idagsläget jobbar hon med l... | x |  |  | 4 | [Nagelförlängning, **skill**](http://data.jobtechdev.se/taxonomy/concept/VxS2_ZHa_Kag) |
| 8 | ...ringar över. Service,ärlighet,**noggrannhet** och omhändertagande är ett må... | x | x | 11 | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| | **Overall** | | | **79** | **83** | 79/83 = **95%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| x |  |  | [Nagelförlängning, **skill**](http://data.jobtechdev.se/taxonomy/concept/VxS2_ZHa_Kag) |
| x | x | x | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| | | **3** | 3/4 = **75%** |