# Results for 'b8d648396abce24a5ed76efa272ca5f01bc46c03'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b8d648396abce24a5ed76efa272ca5f01bc46c03](README.md) | 1 | 2048 | 15 | 14 | 105/151 = **70%** | 8/11 = **73%** |

## Source text

Skådespelare med sångkunskaper till Halloween på Liseberg 2022 Upplevelse- och åkattraktionerna är för många gäster anledningen till ett Lisebergsbesök.  Med bemötande och säkerhet i fokus ansluter Gästverksamhetsområde Attraktion starkt till ansvaret för gästens helhetsupplevelse och förstärker Lisebergsupplevelsen ytterligare.  Beskrivning  Gästverksamhetsområde Attraktion på Liseberg söker skådespelare med sångkunskaper till Skräckenheten Cirkus Bisarr. Dagen kommer att blandas med sångframträdanden och talade monologer. Som skådespelare uppträder du med manusbaserade nummer. Sångvana är ett krav.  Arbetsuppgifter  En arbetsdag innefattar förutom schemalagda framträdanden, även uppvärmning, förberedelser och iordningställande. Tjänsterna vi erbjuder är säsongstjänster och arbetstiden följer Lisebergsparkens öppettider och är förlagd till såväl vardagar som helger, dagtid som kvällstid.  Kvalifikationer  De framträdanden som spelas är till stor del manusbundna och regisserade, men innehåller även improviserade delar, vilket gör att vi lägger stor vikt vid din förmåga att improvisera.  Vi söker dig som är en stark sångare och har röstläge sopran eller alt. Har du erfarenheter eller utbildning inom sång samt fysisk teater är detta meriterande.  För att arbeta som skådespelare på Liseberg ser vi att du har utbildning och/eller professionell erfarenhet inom området teater/sång/dans. Samt goda kunskaper inom improvisation.  Vi vill att du förutom din ansökan och CV att du spelar in en stycke film där du berättar om vem du är, varför vill du arbeta på Halloween och presentera ditt nummer? 60 sek.     Vill du så får du gärna länka din showreal.     Vill du veta mer om våra karaktärer eller övrig information om tjänsten är du välkommen att kontakta vår konceptkoordinator/arbetsledare Elin Ljungberg, 031-766 70 48, områdeschef Emil Aronsson, 031-766 73 43 alt via mail till skrack@liseberg.se.  Observera att sista ansökningsdag är söndagen den 14 augusti och audition kommer att hållas på Liseberg måndag den 29 augusti.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Skådespelare** med sångkunskaper till Hallow... | x | x | 12 | 12 | [Skådespelare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/iBUL_s38_izZ) |
| 2 | Skådespelare med **sångkunskaper** till Halloween på Liseberg 20... | x |  |  | 13 | [Sångkunnig, **skill**](http://data.jobtechdev.se/taxonomy/concept/tJxe_t4t_C3k) |
| 3 | ... Attraktion på Liseberg söker **skådespelare** med sångkunskaper till Skräck... | x | x | 12 | 12 | [Skådespelare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/iBUL_s38_izZ) |
| 4 | ...seberg söker skådespelare med **sångkunskaper** till Skräckenheten Cirkus Bis... | x |  |  | 13 | [Sångkunnig, **skill**](http://data.jobtechdev.se/taxonomy/concept/tJxe_t4t_C3k) |
| 5 | ...gkunskaper till Skräckenheten **Cirkus** Bisarr. Dagen kommer att blan... |  | x |  | 6 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 6 | ...den och talade monologer. Som **skådespelare** uppträder du med manusbaserad... | x | x | 12 | 12 | [Skådespelare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/iBUL_s38_izZ) |
| 7 | ... du med manusbaserade nummer. **Sångvana** är ett krav.  Arbetsuppgifter... | x |  |  | 8 | [Sång, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tKeW_sAm_D72) |
| 8 | ... Vi söker dig som är en stark **sångare** och har röstläge sopran eller... | x | x | 7 | 7 | [Sångare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/k3ZW_16R_riY) |
| 9 | ...tark sångare och har röstläge **sopran** eller alt. Har du erfarenhete... |  | x |  | 6 | [Sopran, **job-title**](http://data.jobtechdev.se/taxonomy/concept/MZ7q_edu_6Gm) |
| 9 | ...tark sångare och har röstläge **sopran** eller alt. Har du erfarenhete... | x | x | 6 | 6 | [Sopran, **skill**](http://data.jobtechdev.se/taxonomy/concept/S4jc_j2F_CmR) |
| 10 | ...enheter eller utbildning inom **sång** samt fysisk teater är detta m... | x | x | 4 | 4 | [Sång, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tKeW_sAm_D72) |
| 11 | ...ler utbildning inom sång samt **fysisk teater** är detta meriterande.  För at... | x | x | 13 | 13 | [Fysisk teater, **skill**](http://data.jobtechdev.se/taxonomy/concept/RG5k_D9y_iW5) |
| 12 | ...iterande.  För att arbeta som **skådespelare** på Liseberg ser vi att du har... | x | x | 12 | 12 | [Skådespelare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/iBUL_s38_izZ) |
| 13 | ...onell erfarenhet inom området **teater**/sång/dans. Samt goda kunskape... | x | x | 6 | 6 | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| 14 | ...rfarenhet inom området teater/**sång**/dans. Samt goda kunskaper ino... | x | x | 4 | 4 | [Sång, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tKeW_sAm_D72) |
| 15 | ...nhet inom området teater/sång/**dans**. Samt goda kunskaper inom imp... | x | x | 4 | 4 | [Dans, **skill**](http://data.jobtechdev.se/taxonomy/concept/kusY_oZZ_Wbv) |
| 16 | ...ans. Samt goda kunskaper inom **improvisation**.  Vi vill att du förutom din ... | x | x | 13 | 13 | [Improvisation, **skill**](http://data.jobtechdev.se/taxonomy/concept/qwHa_w4A_tTc) |
| | **Overall** | | | **105** | **151** | 105/151 = **70%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Sopran, **job-title**](http://data.jobtechdev.se/taxonomy/concept/MZ7q_edu_6Gm) |
| x | x | x | [Fysisk teater, **skill**](http://data.jobtechdev.se/taxonomy/concept/RG5k_D9y_iW5) |
| x | x | x | [Sopran, **skill**](http://data.jobtechdev.se/taxonomy/concept/S4jc_j2F_CmR) |
|  | x |  | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| x | x | x | [Skådespelare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/iBUL_s38_izZ) |
| x | x | x | [Sångare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/k3ZW_16R_riY) |
| x | x | x | [Dans, **skill**](http://data.jobtechdev.se/taxonomy/concept/kusY_oZZ_Wbv) |
| x | x | x | [Improvisation, **skill**](http://data.jobtechdev.se/taxonomy/concept/qwHa_w4A_tTc) |
| x |  |  | [Sångkunnig, **skill**](http://data.jobtechdev.se/taxonomy/concept/tJxe_t4t_C3k) |
| x | x | x | [Sång, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tKeW_sAm_D72) |
| x | x | x | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| | | **8** | 8/11 = **73%** |