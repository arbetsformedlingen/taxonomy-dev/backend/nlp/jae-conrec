# Results for '155626e681195efef0f495406fa22f49cc1bce99'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [155626e681195efef0f495406fa22f49cc1bce99](README.md) | 1 | 2063 | 6 | 3 | 39/63 = **62%** | 3/4 = **75%** |

## Source text

Max söker medarbetare MAX är Sveriges äldsta burgarkedja och den enda rikstäckande burgarkedjan som bara använder svenskt kött, kyckling och bacon. Vi vill bli världens bästa burgarkedja och vägen dit är att göra världen lite godare, både genom att servera de godaste burgarna och genom att vara en god kraft i samhället. Vi har över 150 restauranger och sysselsätter cirka 5500 medarbetare.     Är du sugen på en karriär inom snabbmatsbranschen eller ett extra jobb vid sidan av studierna? Oavsett vilket mål du har för framtiden så tror vi du kan gilla att jobba hos oss!    Max utgångspunkt är alla människors lika värde.  För oss betyder det lika möjligheter, rättigheter och skyldigheter oavsett vem du är. Det är dina egna ambitioner och resultat som avgör hur långt du kan gå. Vi hjälper dig på vägen med en tydlig plan, interna utbildningar och goda utvecklingsmöjligheter på Sveriges godaste burgarkedja.     Som restaurangbiträde serverar du gäster, jobbar i köket, kassan och håller rent i restaurangen. Tillsammans med dina kollegor ser du till att vi alltid serverar gästerna Sveriges godaste burgare. Arbetet innebär varierande arbetstider på dagar, kvällar, nätter och helger. Under handledning av restaurangchefen eller en erfaren kollega lär du dig jobbet steg för steg på de olika stationerna samt genom våra digitala utbildningar. Nästan alla våra chefer har börjat precis här.     Vem är du?  För att trivas hos oss behöver du vara serviceinriktad och gilla ett högt arbetstempo. Du tycker om att jobba i team och presterar bäst som en del av en grupp. Kvalitet är det allra viktigaste för oss och därför tror vi att du är noggrann men samtidigt effektiv.     Vad erbjuder vi?  Ett fartfyllt jobb i ett sammansvetsat team med väldigt goda möjligheter att växa och utvecklas. Lön och villkor enligt kollektivavtal samt friskvårdsbidrag.     Vi tittar på ansökningarna löpande och jobbet kan tillsättas innan sista ansökningsdag. Välkommen med din ansökan!  Vill du läsa om hur vi hanterar dina personuppgifter? Gå in på www.max.se/dataskydd

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Max söker medarbetare MAX är **Sveriges** äldsta burgarkedja och den en... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 2 | ...oda utvecklingsmöjligheter på **Sveriges** godaste burgarkedja.     Som ... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 3 | ... godaste burgarkedja.     Som **restaurangbiträde** serverar du gäster, jobbar i ... | x | x | 17 | 17 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 4 | ...t vi alltid serverar gästerna **Sveriges** godaste burgare. Arbetet inne... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 5 | ... och därför tror vi att du är **noggrann** men samtidigt effektiv.     V... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 6 | ...cklas. Lön och villkor enligt **kollektivavtal** samt friskvårdsbidrag.     Vi... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **39** | **63** | 39/63 = **62%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **3** | 3/4 = **75%** |