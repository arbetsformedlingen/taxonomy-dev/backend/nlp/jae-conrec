# Results for 'd2e9ceb01e91fe69fecc09c29bfacd48c1baab19'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d2e9ceb01e91fe69fecc09c29bfacd48c1baab19](README.md) | 1 | 850 | 3 | 5 | 31/59 = **53%** | 3/4 = **75%** |

## Source text

Fotvårdsspecialist  Hej är du vår nya kollega att hyra in dig på vår salong my Corner på väster i örebro. Ringgatan 20 vi söker dig som är noggran o känner stor glädje inför ditt jobb. Vi välkomnar alla kunder, unga som gamla, med glädje.  här jobbar vi som ett team o en familj där alla hjälps åt för att göra en så härlig miljö som möjlugt på salongen.  vi har 5 behandlingsrum som hyrs ut till egenföretagare som vill jobba i team men styra sig  själva. vi som jobbar är utbildade i vår kategori och jobbar med noggranhet och god kundvård.  gå in på vår hemsida för att läsa mer om oss och hör av dog till mig så pratar vi mer o bestämmer vidare träff. sofia  rylen 070-5551739 info@mycorner.nu Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Fotvårdsspecialist**  Hej är du vår nya kollega at... |  | x |  | 18 | [fotvårdsspecialist, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/YTdC_idh_i5M) |
| 1 | **Fotvårdsspecialist**  Hej är du vår nya kollega at... | x | x | 18 | 18 | [Fotterapeut/Fotvårdsspecialist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ekn1_xkW_zPM) |
| 2 | ... salong my Corner på väster i **örebro**. Ringgatan 20 vi söker dig so... | x | x | 6 | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 3 | ...ggatan 20 vi söker dig som är **noggran** o känner stor glädje inför di... | x | x | 7 | 7 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 4 | ...i vår kategori och jobbar med **noggranhet** och god kundvård.  gå in på v... |  | x |  | 10 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| | **Overall** | | | **31** | **59** | 31/59 = **53%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [fotvårdsspecialist, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/YTdC_idh_i5M) |
| x | x | x | [Fotterapeut/Fotvårdsspecialist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ekn1_xkW_zPM) |
| x | x | x | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| | | **3** | 3/4 = **75%** |