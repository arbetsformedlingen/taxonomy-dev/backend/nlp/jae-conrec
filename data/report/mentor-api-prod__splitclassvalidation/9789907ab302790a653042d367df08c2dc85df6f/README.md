# Results for '9789907ab302790a653042d367df08c2dc85df6f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9789907ab302790a653042d367df08c2dc85df6f](README.md) | 1 | 835 | 7 | 10 | 56/76 = **74%** | 4/5 = **80%** |

## Source text

Extrapersonal frukost och restaurang På vårt hotell arbetar vi med arbetsrotation och i tjänsten ingår arbete i frukostservering, bar & restaurang, städning. Tjänsten innebär varierande arbetstider och tjänsten kan även innebära att tillfälligt hjälpa till med städning av hotellrum. Du kommer att få jobba både dag, kväll, helger. Tjänsten innebär även att det finns möjlighet att ibland arbeta på vårt andra hotell Motel L i Lund. Vi söker dig som är mycket flexibel och ansvarstagande med ett brinnande intresse för servicebranschen. Du delar vår vision om att bli Sveriges trevligaste hotell och deltar aktivt i vårt arbete att sätta gästen i fokus. Vi är ett tajt team på Good Morning Lund och det är därför av hög vikt att du är en naturlig lagspelare.   För mer information om hotellet och hotellkedja besök gärna: www.ligula.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Extrapersonal frukost och **restaurang** På vårt hotell arbetar vi med... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 2 | ...rukost och restaurang På vårt **hotell** arbetar vi med arbetsrotation... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 3 | ...ete i frukostservering, bar & **restaurang**, städning. Tjänsten innebär v... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 4 | ...tservering, bar & restaurang, **städning**. Tjänsten innebär varierande ... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 5 | ...t tillfälligt hjälpa till med **städning** av hotellrum. Du kommer att f... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 6 | ...t ibland arbeta på vårt andra **hotell** Motel L i Lund. Vi söker dig ... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 7 | ...å vårt andra hotell Motel L i **Lund**. Vi söker dig som är mycket f... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 8 | ...ig som är mycket flexibel och **ansvarstagande** med ett brinnande intresse fö... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 9 | ... att bli Sveriges trevligaste **hotell** och deltar aktivt i vårt arbe... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 10 | ...ett tajt team på Good Morning **Lund** och det är därför av hög vikt... |  | x |  | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| | **Overall** | | | **56** | **76** | 56/76 = **74%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| | | **4** | 4/5 = **80%** |