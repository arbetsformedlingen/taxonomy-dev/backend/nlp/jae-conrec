# Results for '5dafc91db69f2097ed767b5511705c81eeca6925'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [5dafc91db69f2097ed767b5511705c81eeca6925](README.md) | 1 | 3503 | 33 | 23 | 152/390 = **39%** | 14/31 = **45%** |

## Source text

Systemutvecklare till Tietoevry i Umeå Brinner du för avancerad problemlösning, mjukvaruutveckling, systemarkitektur eller är du bara en extremt duktig nörd? Då är du rätt person för oss!  Tietoevry, i samarbete med Poolia, söker nu dig som redan har och dig som vill få ett passionerat intresse för system- och mjukvaruutveckling. Vi söker juniora och seniora utvecklare som vill vara med och leda och utveckla 5G inom Enhanced Mobile Broadband, Ultra Reliable Low Latency Communications och Massive Machine Type Communications och även för utvecklings- och forskningssamarbeten inom framtidens 6G telekomsystem.   Urval sker löpande så ansök redan idag!  Tjänsten är på heltid med start enligt överenskommelse.  Om tjänsten Hos oss kommer du att arbeta i något av våra kundprojekt, hos de stora mobilsystemsföretagen, hos de främsta mobiltillverkarna och ibland även hos offensiva start-ups som är i behov av avancerade/innovativa muskler.   Stämningen i de olika teamen präglas av en stark lagkänsla där man möter framgångar och löser utmaningar tillsammans.  Du kommer att arbeta i ett agilt krossfunktionellt utvecklingsteam där arbetet bland annat består av:  -  Avancerad systemerin - Modellering/simulering - Hands-on mjukvaruutveckling i skarpa målsystem  Du kommer att arbeta mot inbyggda multi-core system där realtidsprestanda och aspekter som cykel- och minnesoptimering är centrala.   Mjukvaruutvecklingen bedrivs i ett flertal språk och scriptdialekter som:  - Realtime -C och C++ är centrala  - Java,  Golang, Perl och Python är också viktiga   Verktyg som används är:  - GIT, Eclipse, Jira, Gerrit och Jenkins är en naturlig del av utvecklingsmiljön.  - För simulering och modellering används även Matlab och Octave.  Vem är du? Vi söker både juniora och seniora utvecklare.   Du som vi söker tjänsten:  - Håller på eller har avsluta en civilingenjörsutbildning inom teknisk datavetenskap, elektro, teknisk fysik eller motsvarande.  - Du har kunskap inom Java och C/C++.  - Om du har erfarenhet från mjukvaruutveckling och är van vid att jobba i projektform/agilt är det meriterande men inget krav.   För att trivas i rollen bör du vara en ansvarstagande och noggrann lagspelare som inte är rädd för att komma med nya idéer. Du är även analytisk och kan se förbättringsmöjligheter. Du är självklart uppdaterad i den senaste tekniken och har förmåga att ta egna initiativ.  Om verksamheten Tietoevrys mål är att vara kundernas förstahandsval för affärsförnyelse och det ledande mjukvaru- och tjänsteföretaget i Norden.   I en snabbt föränderlig värld kan varje bit av information användas för att skapa nytt värde. Tieto vill omsätta möjligheterna i den datadrivna världen till värde för människor, företag och samhället i stort. Vi har idag en framträdande roll i det digitala ekosystemet och använder vår mjukvara och våra tjänster för att skapa verktyg och lösningar som förenklar vardagen för miljontals människor, hjälper kunder att förnya sin affär genom att fånga möjligheterna med modernisering, digitalisering och innovation och som skapar nya möjligheter baserat på öppenhet, innovationssamarbeten och ekosystem.  Tieto utgår från ett starkt nordiskt arv och kombinerar global förmåga med lokal närvaro. Huvudkontoret ligger i Esbo, Finland, och bolaget sysselsätter omkring 14 000 experter i cirka 20 länder. Tietos omsättning är cirka 1,5 miljarder euro.     Varmt välkommen med din ansökan. Urval sker löpande, ansök därför redan idag genom att klicka på ansök på poolia.se.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Systemutvecklare** till Tietoevry i Umeå Brinner... | x |  |  | 16 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 2 | ...emutvecklare till Tietoevry i **Umeå** Brinner du för avancerad prob... | x | x | 4 | 4 | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| 3 | ...för avancerad problemlösning, **mjukvaruutveckling**, systemarkitektur eller är du... | x | x | 18 | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 4 | ... ett passionerat intresse för **system- och mjuk**varuutveckling. Vi söker junio... |  | x |  | 16 | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| 5 | ... ett passionerat intresse för **system- och mjukvaruutveckling**. Vi söker juniora och seniora... | x |  |  | 30 | [Mjukvaru- och systemutvecklare m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/DJh5_yyF_hEM) |
| 6 | ...erat intresse för system- och **mjukvaruutveckling**. Vi söker juniora och seniora... |  | x |  | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 7 | ...e Communications och även för **utvecklings- och f**orskningssamarbeten inom framt... |  | x |  | 18 | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| 8 | ...k redan idag!  Tjänsten är på **heltid** med start enligt överenskomme... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 9 | ....  Du kommer att arbeta i ett **agilt** krossfunktionellt utvecklings... | x |  |  | 5 | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| 10 | ...i ett agilt krossfunktionellt **utvecklingsteam** där arbetet bland annat bestå... | x |  |  | 15 | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| 11 | ...llering/simulering - Hands-on **mjukvaruutveckling** i skarpa målsystem  Du kommer... | x | x | 18 | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 12 | ...ekter som:  - Realtime -C och **C++** är centrala  - Java,  Golang,... | x |  |  | 3 | [C++ Builder, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/wbj8_LR2_aZ8) |
| 12 | ...ekter som:  - Realtime -C och **C++** är centrala  - Java,  Golang,... |  | x |  | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 13 | ...ime -C och C++ är centrala  - **Java**,  Golang, Perl och Python är ... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 14 | ...är centrala  - Java,  Golang, **Perl** och Python är också viktiga  ... | x | x | 4 | 4 | [Perl, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/uLhB_Tto_EJ2) |
| 15 | ...la  - Java,  Golang, Perl och **Python** är också viktiga   Verktyg so... | x | x | 6 | 6 | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| 16 | ...och Python är också viktiga   **Verktyg** som används är:  - GIT, Eclip... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 17 | ...   Verktyg som används är:  - **GIT**, Eclipse, Jira, Gerrit och Je... | x | x | 3 | 3 | [Git, versionshanteringssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/2REh_Drn_nsh) |
| 18 | ...rktyg som används är:  - GIT, **Eclipse**, Jira, Gerrit och Jenkins är ... | x | x | 7 | 7 | [Eclipse, integrerad utvecklingsmiljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/BpH9_T22_HCv) |
| 19 | ... används är:  - GIT, Eclipse, **Jira**, Gerrit och Jenkins är en nat... | x | x | 4 | 4 | [JIRA, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQoJ_GY7_n5Z) |
| 20 | ...IT, Eclipse, Jira, Gerrit och **Jenkins** är en naturlig del av utveckl... | x | x | 7 | 7 | [Jenkins, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/tmyt_n6R_1Gc) |
| 21 | ... och modellering används även **Matlab** och Octave.  Vem är du? Vi sö... | x | x | 6 | 6 | [Tekniska beräkningar/MATLAB, **skill**](http://data.jobtechdev.se/taxonomy/concept/uvcb_DuX_NW8) |
| 22 | ...åller på eller har avsluta en **civilingenjörsutbildning** inom teknisk datavetenskap, e... | x | x | 24 | 24 | [Civilingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/idCW_nKR_Xnm) |
| 23 | ...civilingenjörsutbildning inom **teknisk datavetenskap**, elektro, teknisk fysik eller... | x |  |  | 21 | [Civilingenjörsutbildning, datateknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/XG6m_ecV_2di) |
| 24 | ...enjörsutbildning inom teknisk **datavetenskap**, elektro, teknisk fysik eller... |  | x |  | 13 | [datavetenskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VxiM_uBZ_V4d) |
| 25 | ...g inom teknisk datavetenskap, **elektro**, teknisk fysik eller motsvara... | x |  |  | 7 | [Civilingenjörsutbildning, elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Vzg5_p73_x7u) |
| 26 | ...knisk datavetenskap, elektro, **teknisk fysik** eller motsvarande.  - Du har ... | x |  |  | 13 | [Civilingenjörsutbildning, teknisk fysik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/JxwV_TPy_fXB) |
| 26 | ...knisk datavetenskap, elektro, **teknisk fysik** eller motsvarande.  - Du har ... |  | x |  | 13 | [Teknisk fysik, **skill**](http://data.jobtechdev.se/taxonomy/concept/meMN_wmY_RPf) |
| 27 | ...rande.  - Du har kunskap inom **Java** och C/C++.  - Om du har erfar... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 28 | ... Du har kunskap inom Java och **C**/C++.  - Om du har erfarenhet ... | x |  |  | 1 | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| 29 | ...u har kunskap inom Java och C/**C++**.  - Om du har erfarenhet från... | x | x | 3 | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 30 | ...  - Om du har erfarenhet från **mjukvaruutveckling** och är van vid att jobba i pr... | x | x | 18 | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 31 | ...utveckling och är van vid att **jobba** i projektform/agilt är det me... | x |  |  | 5 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 32 | ...n vid att jobba i projektform/**agilt** är det meriterande men inget ... | x |  |  | 5 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 33 | ...rivas i rollen bör du vara en **ansvarstagande** och noggrann lagspelare som i... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 34 | ...du vara en ansvarstagande och **noggrann** lagspelare som inte är rädd f... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 35 | ...mma med nya idéer. Du är även **analytisk** och kan se förbättringsmöjlig... | x |  |  | 9 | [tänka analytiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/osu5_TsT_qGo) |
| 36 | ...h våra tjänster för att skapa **verktyg** och lösningar som förenklar v... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 37 | ... Huvudkontoret ligger i Esbo, **Finland**, och bolaget sysselsätter omk... | x |  |  | 7 | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
| | **Overall** | | | **152** | **390** | 152/390 = **39%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Git, versionshanteringssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/2REh_Drn_nsh) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| x | x | x | [Eclipse, integrerad utvecklingsmiljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/BpH9_T22_HCv) |
| x |  |  | [Mjukvaru- och systemutvecklare m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/DJh5_yyF_hEM) |
| x |  |  | [Civilingenjörsutbildning, teknisk fysik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/JxwV_TPy_fXB) |
| x |  |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
|  | x |  | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| x | x | x | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| x |  |  | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
|  | x |  | [datavetenskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VxiM_uBZ_V4d) |
| x |  |  | [Civilingenjörsutbildning, elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Vzg5_p73_x7u) |
| x |  |  | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
| x |  |  | [Civilingenjörsutbildning, datateknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/XG6m_ecV_2di) |
| x |  |  | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| x | x | x | [JIRA, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQoJ_GY7_n5Z) |
| x |  |  | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| x | x | x | [Civilingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/idCW_nKR_Xnm) |
| x | x | x | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
|  | x |  | [Teknisk fysik, **skill**](http://data.jobtechdev.se/taxonomy/concept/meMN_wmY_RPf) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| x |  |  | [tänka analytiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/osu5_TsT_qGo) |
| x | x | x | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| x | x | x | [Jenkins, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/tmyt_n6R_1Gc) |
| x | x | x | [Perl, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/uLhB_Tto_EJ2) |
| x | x | x | [Tekniska beräkningar/MATLAB, **skill**](http://data.jobtechdev.se/taxonomy/concept/uvcb_DuX_NW8) |
| x |  |  | [C++ Builder, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/wbj8_LR2_aZ8) |
| x | x | x | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| | | **14** | 14/31 = **45%** |