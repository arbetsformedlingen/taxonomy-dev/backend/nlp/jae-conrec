# Results for '05bc5a14e773f54283dae1c2aafdd8e63030006d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [05bc5a14e773f54283dae1c2aafdd8e63030006d](README.md) | 1 | 1454 | 13 | 8 | 24/212 = **11%** | 3/15 = **20%** |

## Source text

skogsarbetare Vi söker arbetare till plantering och skogsvård. Jobb med planteringsborr, planteringsrör och röjsåg. Jobb from snarast i minst 2 månader. Till bra och duktiga arbetare vi kan erbjuda jobbet på längre tid, inklusive tillsvidareanställning.   Behov: ·        Mycket bra hälsa, pga arbete kräver fysisk ansträngning.  ·        Efter några dagars arbete bör en viss standard utföras norm (ställ in standard beroende på ytan på vilken arbetet utförs). ·        Förmåga att arbeta i ett lag (2- 5 personer). ·        Ansvar och mycket god kvalitet på arbetet. ·        Lojalitet mot företaget. ·        Respekt för kollegor. ·        Stor fördel är erfarenhet av att arbeta i skogen och körkortet B.  ·        Kunskaper i engelska och svenska är inte nödvändiga. ·        Gärna, personen interessade av långsiktigt samarbete.   Det mesta av plantering är ackordjobb. Pris per planta är från 0,72 sek per styck tom 1,237 sek per styck beroende på vilken typ av plantor (priset ingår semesterersättning och arbetstidskonto). Pris brutto. Vid arbetet för timmen lägsta pris är 143,10 sek per timme brutto med semesterersättning och arbetstidskonto. Skatter, försäkringar och pensionsavgifter betalas i Sverige. Vi erbjuder heltidsjobb. Säsonganställning. Till bästa och duktiga arbetare vi kan erbjuda jobbet på längre tid, inklusive tillsvidareanställning. Vi tillhandahåller arbetskläder, arbetsredskap och transport till arbetet med företagsbil.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **skogsarbetare** Vi söker arbetare till plante... | x |  |  | 13 | [Skogsarbetare, plantering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7x9Q_FTM_6kQ) |
| 1 | **skogsarbetare** Vi söker arbetare till plante... |  | x |  | 13 | [Skogsarbetare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/qouP_wtb_93q) |
| 2 | ...betare Vi söker arbetare till **plantering** och skogsvård. Jobb med plant... |  | x |  | 10 | [plantera gröna växter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Bz5Z_Zc2_YkF) |
| 2 | ...betare Vi söker arbetare till **plantering** och skogsvård. Jobb med plant... | x |  |  | 10 | [Skogsplantering, **skill**](http://data.jobtechdev.se/taxonomy/concept/jnhn_HSM_2eC) |
| 3 | ... arbetare till plantering och **skogsvård**. Jobb med planteringsborr, pl... | x | x | 9 | 9 | [Skogsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/zve2_ooW_xhy) |
| 4 | ...bbet på längre tid, inklusive **tillsvidareanställning**.   Behov: ·        Mycket bra... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 5 | ...   Behov: ·        Mycket bra **hälsa**, pga arbete kräver fysisk ans... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 6 | ...utförs). ·        Förmåga att **arbeta i ett lag** (2- 5 personer). ·        Ans... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 7 | ...et av att arbeta i skogen och **körkortet B.**  ·        Kunskaper i engelsk... | x |  |  | 12 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 8 | ...rtet B.  ·        Kunskaper i **engelska** och svenska är inte nödvändig... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 9 | ...     Kunskaper i engelska och **svenska** är inte nödvändiga. ·        ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 10 | ...igt samarbete.   Det mesta av **plantering** är ackordjobb. Pris per plant... |  | x |  | 10 | [plantera gröna växter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Bz5Z_Zc2_YkF) |
| 10 | ...igt samarbete.   Det mesta av **plantering** är ackordjobb. Pris per plant... | x |  |  | 10 | [Skogsplantering, **skill**](http://data.jobtechdev.se/taxonomy/concept/jnhn_HSM_2eC) |
| 11 | ...   Det mesta av plantering är **ackordjobb**. Pris per planta är från 0,72... | x |  |  | 10 | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
| 12 | ...ch pensionsavgifter betalas i **Sverige**. Vi erbjuder heltidsjobb. Säs... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 13 | ...etalas i Sverige. Vi erbjuder **heltidsjobb**. Säsonganställning. Till bäst... | x |  |  | 11 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 14 | ...ige. Vi erbjuder heltidsjobb. **Säsonganställning**. Till bästa och duktiga arbet... | x |  |  | 17 | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
| 15 | ...bbet på längre tid, inklusive **tillsvidareanställning**. Vi tillhandahåller arbetsklä... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| | **Overall** | | | **24** | **212** | 24/212 = **11%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Skogsarbetare, plantering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7x9Q_FTM_6kQ) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
|  | x |  | [plantera gröna växter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Bz5Z_Zc2_YkF) |
| x |  |  | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Skogsplantering, **skill**](http://data.jobtechdev.se/taxonomy/concept/jnhn_HSM_2eC) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Skogsarbetare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/qouP_wtb_93q) |
| x |  |  | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [Skogsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/zve2_ooW_xhy) |
| | | **3** | 3/15 = **20%** |