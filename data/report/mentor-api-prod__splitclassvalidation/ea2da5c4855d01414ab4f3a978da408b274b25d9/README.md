# Results for 'ea2da5c4855d01414ab4f3a978da408b274b25d9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ea2da5c4855d01414ab4f3a978da408b274b25d9](README.md) | 1 | 2271 | 23 | 17 | 93/270 = **34%** | 6/18 = **33%** |

## Source text

Lokalvårdare till Ara Städ i Osby I Osby hittar ni Ara städ. Vi har mer än 30 års erfarenhet av hemstädning, flyttstädning, storstädning och även trädgård och utemiljö för att nämna några av våra områden inom privatsektor. Vi har även företagskunder med fokus på tex kontorsstädning, butiksstädning, golvvård mm Vi sätter alltid kunden i fokus och erbjuder hög kvalité  Nu behöver vi förstärka vår personalstyrka med ytterligare lokalvårdare. Vi behöver 2 person på 50-75%. Brinner du för ordning och reda och vet med dig att du har ett starkt intresse för rena och fina miljöer?  Läs då vidare Dina arbetsuppgifter Du kommer främst att arbeta för våra kunder med hem städning , det kan ibland förekomma företags städ. Vi erbjuder även våra kunder städning gällande trädgård och utemiljö. Fönsterputsning är också en av våra arbetsuppgifter. Din profil För att bli framgångsrik i rollen som lokalvårdare hos oss måste du trivas med att arbeta för att ge våra kunder en bra service. Vi lämnar alltid ifrån oss ett väl och noggrant utfört arbete. Du behöver vara strukturerad och serviceminded. Ditt arbete är varierande vilket innebär att du behöver gilla tempo och ha en hög flexibilitet. Du är noggrann och levererar resultat med god precision även under tidspressade förhållanden. Du har lätt för att lära dig nya saker du har öga för detaljer. För att vara aktuell för tjänsterna har du mycket goda språkkunskaper i svenska, i tal och skrift körkort är ett krav, och egen bil krävs du ska också kunna uppvisa ett utdrag ur belastningsregistret vid en intervju  Villkor  Arbetstiderna är varierande under vardagar (inga helger). Arbetet utförs i Osby och orter runt om kring Osby. Lön enligt kommunal.  Vi går igenom ansökningarna löpande och tjänsterna kan komma att tillsättas före ansökningstidens slut. Ansök senast den 30 Augusti via ansokan@arastad.se Vi vill inte bli kontaktade av rekryteringsföretag då vi valt att göra denna rekrytering själva. Tack! Genom att mejla din ansökan godkänner du att vi lagrar dina personuppgifter så länge det är nödvändigt ur rekryteringssyfte. Om du vill att dina personuppgifter korrigeras, uppdateras eller tas bort så vänligen kontakta oss på ovan epost alt besök vår hemsida för ytterligare kontaktuppgifter  www.arastad.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Lokalvårdare** till Ara Städ i Osby I Osby h... | x | x | 12 | 12 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 2 | Lokalvårdare till Ara Städ i **Osby** I Osby hittar ni Ara städ. Vi... | x | x | 4 | 4 | [Osby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/najS_Lvy_mDD) |
| 3 | ...årdare till Ara Städ i Osby I **Osby** hittar ni Ara städ. Vi har me... | x | x | 4 | 4 | [Osby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/najS_Lvy_mDD) |
| 4 | ...r mer än 30 års erfarenhet av **hemstädning**, flyttstädning, storstädning ... | x |  |  | 11 | [Hemstädare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/TYBE_rFF_6cD) |
| 4 | ...r mer än 30 års erfarenhet av **hemstädning**, flyttstädning, storstädning ... |  | x |  | 11 | [Städtjänster, **skill**](http://data.jobtechdev.se/taxonomy/concept/fZdk_vbt_LhP) |
| 5 | ...v hemstädning, flyttstädning, **storstädning** och även trädgård och utemilj... | x |  |  | 12 | [Storstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/SHkX_Qc7_Xx3) |
| 6 | ...ädning, storstädning och även **trädgård** och utemiljö för att nämna nå... | x |  |  | 8 | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| 6 | ...ädning, storstädning och även **trädgård** och utemiljö för att nämna nå... |  | x |  | 8 | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| 7 | ...tädning och även trädgård och **utemiljö** för att nämna några av våra o... | x |  |  | 8 | [Skötsel och underhåll av utemiljöer, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y6VX_E1a_pNH) |
| 8 | ...retagskunder med fokus på tex **kontorsstädning**, butiksstädning, golvvård mm ... | x | x | 15 | 15 | [Kontorsstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/beHF_mum_ANM) |
| 9 | ...fokus på tex kontorsstädning, **butiksstädning**, golvvård mm Vi sätter alltid... | x |  |  | 14 | [Butiksstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/7NM4_ArB_Zy2) |
| 10 | ...torsstädning, butiksstädning, **golvvård** mm Vi sätter alltid kunden i ... | x |  |  | 8 | [Golvvårdare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/4nuk_Bd9_DDd) |
| 11 | ...ersonalstyrka med ytterligare **lokalvårdare**. Vi behöver 2 person på 50-75... | x | x | 12 | 12 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 12 | ...rdare. Vi behöver 2 person på **50-75%**. Brinner du för ordning och r... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 13 | ...tt arbeta för våra kunder med **hem städning** , det kan ibland förekomma fö... | x |  |  | 12 | [Hemstädare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/TYBE_rFF_6cD) |
| 14 | ...rbeta för våra kunder med hem **städning** , det kan ibland förekomma fö... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 15 | ... Vi erbjuder även våra kunder **städning** gällande trädgård och utemilj... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 15 | ... Vi erbjuder även våra kunder **städning** gällande trädgård och utemilj... | x |  |  | 8 | [Skötsel och underhåll av utemiljöer, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y6VX_E1a_pNH) |
| 16 | ...våra kunder städning gällande **trädgård** och utemiljö. Fönsterputsning... |  | x |  | 8 | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| 17 | ...våra kunder städning gällande **trädgård och utemiljö**. Fönsterputsning är också en ... | x |  |  | 21 | [Skötsel och underhåll av utemiljöer, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y6VX_E1a_pNH) |
| 18 | ...llande trädgård och utemiljö. **Fönsterputsning** är också en av våra arbetsupp... | x | x | 15 | 15 | [Fönsterputsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/BQPp_SQs_dtc) |
| 19 | ...bli framgångsrik i rollen som **lokalvårdare** hos oss måste du trivas med a... | x | x | 12 | 12 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 20 | ... alltid ifrån oss ett väl och **noggrant** utfört arbete. Du behöver var... | x |  |  | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 21 | ...ha en hög flexibilitet. Du är **noggrann** och levererar resultat med go... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 22 | ... mycket goda språkkunskaper i **svenska**, i tal och skrift körkort är ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 23 | ...r i svenska, i tal och skrift **körkort** är ett krav, och egen bil krä... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 23 | ...r i svenska, i tal och skrift **körkort** är ett krav, och egen bil krä... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 24 | ...nga helger). Arbetet utförs i **Osby** och orter runt om kring Osby.... | x | x | 4 | 4 | [Osby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/najS_Lvy_mDD) |
| 25 | ... Osby och orter runt om kring **Osby**. Lön enligt kommunal.  Vi går... | x |  |  | 4 | [Osby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/najS_Lvy_mDD) |
| | **Overall** | | | **93** | **270** | 93/270 = **34%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x |  |  | [Golvvårdare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/4nuk_Bd9_DDd) |
| x |  |  | [Butiksstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/7NM4_ArB_Zy2) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x |  |  | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| x | x | x | [Fönsterputsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/BQPp_SQs_dtc) |
| x |  |  | [Storstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/SHkX_Qc7_Xx3) |
| x |  |  | [Hemstädare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/TYBE_rFF_6cD) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| x |  |  | [Skötsel och underhåll av utemiljöer, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y6VX_E1a_pNH) |
| x | x | x | [Kontorsstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/beHF_mum_ANM) |
| x | x | x | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
|  | x |  | [Städtjänster, **skill**](http://data.jobtechdev.se/taxonomy/concept/fZdk_vbt_LhP) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Osby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/najS_Lvy_mDD) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/18 = **33%** |