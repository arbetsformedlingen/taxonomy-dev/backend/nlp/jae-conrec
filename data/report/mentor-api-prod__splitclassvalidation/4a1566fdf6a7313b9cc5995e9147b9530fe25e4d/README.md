# Results for '4a1566fdf6a7313b9cc5995e9147b9530fe25e4d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4a1566fdf6a7313b9cc5995e9147b9530fe25e4d](README.md) | 1 | 3609 | 19 | 20 | 115/272 = **42%** | 7/19 = **37%** |

## Source text

Assistent till extemporetillverkningen i Uppsala Har du erfarenhet av att jobba på en tillverkningsenhet? Tycker du det är spännande med omväxlande och stimulerande arbetsuppgifter? Gillar du att arbeta i ett större team? Då har vi rätt jobb för dig på Apoteket AB:s Nationella enheten för Tillverkning i Uppsala!  Låt oss själa dig en stund Apotekets vårdaffär står inför spännande utmaningar och möjligheter. Vi har tagit ett samlat grepp kring vård och dosaffären och satsar för att vara vårdens mest attraktiva partner när det gäller Läkemedelsförsörjning. I Uppsala har vi en nationell tillverkningsenhet där vi tillverkar sterila extemporeberedningar till både receptkunder och avtalskunder och vi behöver förstärka enheten med en tillverkningsassistent. Hos oss kommer du att få arbeta i helt nya lokaler i Uppsala då vi i början på nästa år flyttar vår verksamhet.  Tjänsten är en tillsvidareanställning på heltid med tillträde enligt överenskommelse.     Vissa jobb är mer själklara än andra Som tillverkningsassistent på Nationella enheten Tillverkning Uppsala har du en viktig roll i många delar av våra processer. Du jobbar i team med farmaceuter och andra assistenter för att säkra de dagliga leveranserna. Assistenternas arbetsuppgifter är varierande och omväxlande och du kan jobba med packning och skickning av färdiga beredningar, administrativa arbetsuppgifter, varuhantering eller med olika arbetsuppgifter i rena rum. Beroende på din kompetens, erfarenhet och utvecklingsintresse finns även andra ansvarsområde man kan lära sig. Du deltar aktivt för att utveckla och effektivisera vår verksamhet och för att enheten ska nå uppsatta mål.     Själklara meriter Vi söker dig som har gymnasiekompetens och erfarenhet av att arbeta med administrativa uppgifter. Du är van att arbeta med datorer men har också god fysik då arbetet ibland är fysiskt rörlig med tyngre lyft. Har du erfarenhet av att arbeta i rena rum och enligt GMP är det meriterande.  Du tycker om att ha ett praktiskt, omväxlande och strukturerat arbete med patienten i fokus. Du har god samarbetsförmåga och lyhördhet då du bygger förtroendefulla relationer med kollegor och våra kunder. Du trivs med att arbeta i team men har även förmåga att arbeta självständigt och förmåga att prioritera i ditt arbete. Du har lätt att lära dig nya arbetsuppgifter.  Välkommen med din ansökan senast den 28 augusti 2022. Intervjuer sker löpande och tjänsten kan komma att tillsättas innan sista dagen för ansökan, så tveka inte att söka tjänsten redan nu!     Många själ att trivas Kamratlig stämning, kunskap, utveckling och hälsa är viktiga ingredienser i Apotekets verksamhet. Vi vet att välmående och engagemang gör dig bättre på det du gör. Därför erbjuder vi branschens mest generösa friskvårdsbidrag och ett brett utbildningsprogram för att bädda för både karriär och meningsfull tillvaro.      Apoteket AB, med huvudkontor i Solna, är Sveriges största apotekskedja med drygt 3300 anställda och närmare 400 apotek över hela landet. Apoteket erbjuder läkemedel, hälsoprodukter och tjänster till privatpersoner, vården, företag och organisationer. Vi bryter ständigt ny mark inom läkemedelsrådgivning, patientsäkerhet, it och miljö.   Här präglas kulturen av omtanke och kollektiv kunskap för innovativ utveckling och leverans av produkter och tjänster som hjälper kunder till ett liv i hälsa. Här delar du övertygelse med människor som av olika skäl har valt Apoteket, och som medarbetare är del av vår själ: En stark tro på den goda kraften för att varje dag göra allt vi kan för att göra det enklare att må bra. För andra. För oss.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ill extemporetillverkningen i **Uppsala** Har du erfarenhet av att jobb... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 2 | ...t AB:s Nationella enheten för **Tillverkning** i Uppsala!  Låt oss själa dig... | x | x | 12 | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 3 | ...la enheten för Tillverkning i **Uppsala**!  Låt oss själa dig en stund ... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 4 | ...ller Läkemedelsförsörjning. I **Uppsala** har vi en nationell tillverkn... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 5 | ...å arbeta i helt nya lokaler i **Uppsala** då vi i början på nästa år fl... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 6 | ...r verksamhet.  Tjänsten är en **tillsvidareanställning** på heltid med tillträde enlig... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 7 | ... en tillsvidareanställning på **heltid** med tillträde enligt överensk... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 8 | ...är mer själklara än andra Som **tillverkning**sassistent på Nationella enhet... |  | x |  | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 9 | ...sistent på Nationella enheten **Tillverkning** Uppsala har du en viktig roll... | x |  |  | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 10 | ...tionella enheten Tillverkning **Uppsala** har du en viktig roll i många... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 11 | ...ocesser. Du jobbar i team med **farmaceuter** och andra assistenter för att... |  | x |  | 11 | [Farmaceuter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/5fat_b9d_Gmi) |
| 11 | ...ocesser. Du jobbar i team med **farmaceuter** och andra assistenter för att... |  | x |  | 11 | [Apotekare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/TMsM_oNw_j6z) |
| 11 | ...ocesser. Du jobbar i team med **farmaceuter** och andra assistenter för att... | x |  |  | 11 | [farmaceut, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/y3gj_k8b_oMw) |
| 12 | ...ministrativa arbetsuppgifter, **varuhantering** eller med olika arbetsuppgift... |  | x |  | 13 | [Varumottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/rYxp_wbM_YSG) |
| 13 | ... meriter Vi söker dig som har **gymnasiekompetens** och erfarenhet av att arbeta ... |  | x |  | 17 | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| 13 | ... meriter Vi söker dig som har **gymnasiekompetens** och erfarenhet av att arbeta ... | x |  |  | 17 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 14 | ...eta med datorer men har också **god fysik** då arbetet ibland är fysiskt ... | x | x | 9 | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 15 | ... ibland är fysiskt rörlig med **tyngre lyft**. Har du erfarenhet av att arb... |  | x |  | 11 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 16 | ... arbeta i rena rum och enligt **GMP** är det meriterande.  Du tycke... |  | x |  | 3 | [GXP (t.ex. GMP, GCP, GLP), **skill**](http://data.jobtechdev.se/taxonomy/concept/Q6DB_h8V_mc7) |
| 17 | ...team men har även förmåga att **arbeta självständigt** och förmåga att prioritera i ... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 18 | ...ning, kunskap, utveckling och **hälsa** är viktiga ingredienser i Apo... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 19 | ...poteket AB, med huvudkontor i **Solna**, är Sveriges största apoteksk... |  | x |  | 5 | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| 20 | ...ela landet. Apoteket erbjuder **läkemedel**, hälsoprodukter och tjänster ... | x | x | 9 | 9 | [Läkemedel/Sjukvårdsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/vVXb_pYT_E1z) |
| 21 | ...tioner. Vi bryter ständigt ny **mark** inom läkemedelsrådgivning, pa... | x |  |  | 4 | [Mark/infrastruktur-Bentley InRoads Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/tCER_yDZ_XnW) |
| 22 | ... bryter ständigt ny mark inom **läkemedelsrådgivning**, patientsäkerhet, it och milj... | x | x | 20 | 20 | [Läkemedelsrådgivning, **skill**](http://data.jobtechdev.se/taxonomy/concept/26DW_RVn_42Q) |
| 23 | ...srådgivning, patientsäkerhet, **it** och miljö.   Här präglas kult... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 24 | ...hjälper kunder till ett liv i **hälsa**. Här delar du övertygelse med... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| | **Overall** | | | **115** | **272** | 115/272 = **42%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Läkemedelsrådgivning, **skill**](http://data.jobtechdev.se/taxonomy/concept/26DW_RVn_42Q) |
|  | x |  | [Farmaceuter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/5fat_b9d_Gmi) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [GXP (t.ex. GMP, GCP, GLP), **skill**](http://data.jobtechdev.se/taxonomy/concept/Q6DB_h8V_mc7) |
|  | x |  | [Apotekare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/TMsM_oNw_j6z) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
|  | x |  | [Varumottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/rYxp_wbM_YSG) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| x |  |  | [Mark/infrastruktur-Bentley InRoads Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/tCER_yDZ_XnW) |
| x | x | x | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| x | x | x | [Läkemedel/Sjukvårdsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/vVXb_pYT_E1z) |
| x |  |  | [farmaceut, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/y3gj_k8b_oMw) |
|  | x |  | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| | | **7** | 7/19 = **37%** |