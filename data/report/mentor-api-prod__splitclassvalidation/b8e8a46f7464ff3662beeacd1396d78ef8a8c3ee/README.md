# Results for 'b8e8a46f7464ff3662beeacd1396d78ef8a8c3ee'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b8e8a46f7464ff3662beeacd1396d78ef8a8c3ee](README.md) | 1 | 821 | 12 | 10 | 67/119 = **56%** | 6/10 = **60%** |

## Source text

Kock sökes till populär lunchrestaurang. Hej! Restaurang Havet är en lunchrestaurang i Saluhallen Briggen som serverar klassisk svensk husmanskost. Vi söker nu en erfaren kock som vill bli en viktig del i vårt team. Arbetstiden är 08:00-16:00 måndag till fredag. Du är en positiv och självgående person som har goda kunskaper från det svenska köket. Som person skall du vara stresstålig och samarbetsvillig. Du bör ha en stor dos kärlek för det svenska köket, vara noggrann och punktlig.  I dina arbetsuppgifter ingår bland annat: • Menyplanering • Matlagning • Portionering   Kvalifikationer / Krav • Utbildad kock med yrkesvana   Tjänsten är en tillsvidaretjänst på 100% Lön enligt överenskommelse. Vänta inte med din ansökan! Alla ansökningar kommer att behandlas löpande så skicka in din ansökan så snart som möjligt.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kock** sökes till populär lunchresta... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 2 | ...populär lunchrestaurang. Hej! **Restaurang** Havet är en lunchrestaurang i... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 3 | ...Briggen som serverar klassisk **svensk **husmanskost. Vi söker nu en er... |  | x |  | 7 | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| 4 | ... som serverar klassisk svensk **husmanskost**. Vi söker nu en erfaren kock ... | x | x | 11 | 11 | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| 5 | ...skost. Vi söker nu en erfaren **kock** som vill bli en viktig del i ... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 6 | ...m har goda kunskaper från det **svenska** köket. Som person skall du va... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 7 | ...ket. Som person skall du vara **stresstålig** och samarbetsvillig. Du bör h... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 8 | ...ha en stor dos kärlek för det **svenska** köket, vara noggrann och punk... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 9 | ...k för det svenska köket, vara **noggrann** och punktlig.  I dina arbetsu... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 10 | ...ppgifter ingår bland annat: • **Menyplanering** • Matlagning • Portionering  ... | x |  |  | 13 | [komponera menyer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XyXa_Cd6_6AU) |
| 11 | ... Menyplanering • Matlagning • **Portionering**   Kvalifikationer / Krav • Ut... | x | x | 12 | 12 | [Portionering, **skill**](http://data.jobtechdev.se/taxonomy/concept/XCMK_vWW_njd) |
| 12 | ...ifikationer / Krav • Utbildad **kock** med yrkesvana   Tjänsten är e... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 13 | ...ed yrkesvana   Tjänsten är en **tillsvidaretjänst** på 100% Lön enligt överenskom... | x |  |  | 17 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 14 | ...en är en tillsvidaretjänst på **100%** Lön enligt överenskommelse. V... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **67** | **119** | 67/119 = **56%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| x | x | x | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x | x | x | [Portionering, **skill**](http://data.jobtechdev.se/taxonomy/concept/XCMK_vWW_njd) |
| x |  |  | [komponera menyer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XyXa_Cd6_6AU) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/10 = **60%** |