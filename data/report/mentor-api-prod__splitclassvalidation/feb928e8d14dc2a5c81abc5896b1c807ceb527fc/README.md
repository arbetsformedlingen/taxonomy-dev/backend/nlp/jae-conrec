# Results for 'feb928e8d14dc2a5c81abc5896b1c807ceb527fc'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [feb928e8d14dc2a5c81abc5896b1c807ceb527fc](README.md) | 1 | 3470 | 33 | 14 | 42/1141 = **4%** | 3/27 = **11%** |

## Source text

Teknisk förvaltare med erfarenhet av entreprenörsuppföljning, Stockholm Om konsultuppdraget  - Ort: Stockholm - Uppdragslängd: Ca. 4 månader - Sista ansökningsdagen: ansök snarast - Omfattning: 100% - OBS! Det är viktigt att du uppfyller skall-kraven för att vi överhuvudtaget ska kunna offerera dig, annars förkastar beställaren vårt anbud omgående där både din tid resp. vår tid går till spillo.   Uppdragsbeskrivning  Den tekniska förvaltaren har ett övergripande ansvar för funktion, underhåll och utveckling av bolagets fastigheteter. Funktionen säkerställer att såväl fastighetens byggnadskonstruktion som tekniska system underhålls och utvecklas. Funktionen säkerställer även att fastighetens kondition och kommande behov dokumenteras och budgeteras. Funktionen är övergripande ansvarig för fastighetens drift, driftkostnader och energiprestanda.  Arbetsuppgifter:  - Hantering av skadeärende - Genomförande och uppföljning av tekniskt underhåll - Ansvar för att myndighetskrav uppfylls - Uppföljning samt deltagande i förbättrings och utvecklingsarbete - Samordna och säkerställa fastigheternas funktioner - Ansvara för fastigheternas fastställda driftbudget och planerat underhållsbudget - Ansvara för underhållsplanering, flerårig underhållsplan och underhållsåtgärder - Utredningar och förstudier inför underhållsprojekt och hyresgästanpassningar - Beställa och överlämna underhållsprojekt till byggprojekt - Följa byggprojektens gång och mottaga färdigställda projekt - Delta i möten med hyresgäster och verksamheter vid behov   Obligatoriska kompetenser och erfarenhet (skallkrav):  -  På eftergymnasial nivå inom fastighetsförvaltning, fastighetsteknik eller motsvarande, alternativt motsvarande kunskaper genom erfarenhet av fastighetsteknik eller fastighetsförvaltning - KY utbildning. Teknisk utbildning, högskola, ingenjörsutbildning   - 5 års erfarenhet som teknisk förvaltare - God datorvana och lätt för att sätta dig in i och hantera olika systemverktyg - God kunskap om bygg -och installationsteknik - Erfarenhet av verksamhetsuppföljning och ekonomi - Erfarenhet av entreprenörsuppföljning - Erfarenhet av arbete inom ramen för LOU -  B-körkort     ______________________  Hur du kommer vidare  - Sök uppdraget genom denna annons - Lägg in ett CV i word-format - Vi återkopplar om något behöver kompletteras eller förtydligas. - Återkoppling sker vanligtvis från Kunden till oss inom 10 arbetsdagar från det att ansökningstiden utgått. Vi försöker återkoppla omgående till dig som kandidat snarast vi har ny information avseende din ansökan eller uppdraget. Skulle återkoppling dröja, vänligen kontakta oss genom att svara på bekräftande mailutskicket i samband med din ansökan.   På uppmaning från beställare/slutkunder vill de inte att vi lämnar ut information om dem. En annan anledning är att Shaya Solutions lägger stor mängd tid som man på förhand inte får betalt för såvida inte uppdraget tillsätts, och därav kan vara återhållsamma med informationsdelningen av naturliga skäl.  Inför en eventuell intervju meddelas du om vilken Kunden är i god tid.  Om Shaya Solutions  Konsult- och kompetenspartner inom IT, Management och Teknik.  Vi lägger ett stort fokus på kund-/konsultpartnernöjdhet och kvalité i våra leveranser och verkar idag på 13 orter i Sverige med utgångspunkt i Stockholm. Teamets motto är Ödmjukhet, Ihärdighet samt Flexibilitet.  Varmt välkommen att höra av dig vid frågor eller funderingar.  Annonsförsäljare undanbedes.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Teknisk förvaltare** med erfarenhet av entreprenör... | x |  |  | 18 | [Teknisk förvaltning, fastigheter, **skill**](http://data.jobtechdev.se/taxonomy/concept/zYxB_rdv_AyB) |
| 2 | ...t av entreprenörsuppföljning, **Stockholm** Om konsultuppdraget  - Ort: S... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 3 | ...m Om konsultuppdraget  - Ort: **Stockholm** - Uppdragslängd: Ca. 4 månade... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 4 | ...pillo.   Uppdragsbeskrivning  **Den tekniska förvaltaren** har ett övergripande ansvar f... | x |  |  | 24 | [Fastighetsförvaltare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EPe8_EQu_YCL) |
| 5 | ...r ett övergripande ansvar för **funktion, underhåll och utveckling av bolagets fastigheteter**. Funktionen säkerställer att ... | x |  |  | 60 | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| 6 | ...tionen säkerställer att såväl **fastighetens byggnadskonstruktion som tekniska system underhålls och utvecklas**. Funktionen säkerställer även... | x |  |  | 78 | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| 7 | ...ktionen säkerställer även att **fastighetens kondition och kommande behov dokumenteras och budgeteras**. Funktionen är övergripande a... | x |  |  | 69 | [Fastighetsekonomisk förvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/jmqj_shU_3VD) |
| 8 | ...s. Funktionen är övergripande **ansvarig för fastighetens drift, driftkostnader och energiprestanda**.  Arbetsuppgifter:  - Hanteri... | x |  |  | 67 | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| 9 | ... - Hantering av skadeärende - **Genomförande och uppföljning av tekniskt underhåll** - Ansvar för att myndighetskr... | x |  |  | 50 | [Fastighetsunderhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/fG9j_hFZ_Vm5) |
| 10 | ...att myndighetskrav uppfylls - **Uppföljning samt deltagande i förbättrings och **utvecklingsarbete - Samordna o... | x |  |  | 47 | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| 11 | ...deltagande i förbättrings och **utvecklingsarbete** - Samordna och säkerställa fa... | x | x | 17 | 17 | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| 12 | ...rings och utvecklingsarbete - **Samordna och säkerställa fastigheternas funktioner** - Ansvara för fastigheternas ... | x |  |  | 50 | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| 13 | ...a fastigheternas funktioner - **Ansvara för fastigheternas fastställda driftbudget och planerat underhållsbudget** - Ansvara för underhållsplane... | x |  |  | 80 | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| 14 | ...h planerat underhållsbudget - **Ansvara för underhållsplanering, flerårig underhållsplan och underhållsåtgärder** - Utredningar och förstudier ... | x |  |  | 79 | [Underhållsplanering, fastigheter, **skill**](http://data.jobtechdev.se/taxonomy/concept/ySUN_PZC_HQW) |
| 15 | ...plan och underhållsåtgärder - **Utredningar och förstudier inför underhållsprojekt och hyresgästanpassningar** - Beställa och överlämna unde... | x |  |  | 76 | [Utredningsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/QAaJ_QeY_Q6P) |
| 16 | ...t och hyresgästanpassningar - **Beställa och överlämna underhållsprojekt till byggprojekt** - Följa byggprojektens gång o... | x |  |  | 57 | [se över byggprojekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NdCR_GnJ_KwV) |
| 17 | ...llsprojekt till byggprojekt - **Följa byggprojektens gång och mottaga färdigställda projekt** - Delta i möten med hyresgäst... | x |  |  | 59 | [se över byggprojekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NdCR_GnJ_KwV) |
| 18 | ...  På eftergymnasial nivå inom **fastighetsförvaltning**, fastighetsteknik eller motsv... | x |  |  | 21 | [Fastighetsförvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/4XyZ_6t8_TWq) |
| 19 | ...å inom fastighetsförvaltning, **fastighetsteknik** eller motsvarande, alternativ... |  | x |  | 16 | [Fastighetsverksamhet, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/9uyq_gBA_8UJ) |
| 19 | ...å inom fastighetsförvaltning, **fastighetsteknik** eller motsvarande, alternativ... | x | x | 16 | 16 | [Fastighetsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/crzp_WfW_wWC) |
| 19 | ...å inom fastighetsförvaltning, **fastighetsteknik** eller motsvarande, alternativ... |  | x |  | 16 | [Fastighetsförvaltning på uppdrag, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/vepN_y8A_oA6) |
| 20 | ...kunskaper genom erfarenhet av **fastighetsteknik** eller fastighetsförvaltning -... | x |  |  | 16 | [Fastighetsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/crzp_WfW_wWC) |
| 21 | ...het av fastighetsteknik eller **fastighetsförvaltning** - KY utbildning. Teknisk utbi... | x |  |  | 21 | [Fastighetsförvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/4XyZ_6t8_TWq) |
| 22 | ...eller fastighetsförvaltning - **KY utbildning**. Teknisk utbildning, högskola... | x |  |  | 13 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 23 | ...sförvaltning - KY utbildning. **Teknisk utbildning**, högskola, ingenjörsutbildnin... | x |  |  | 18 | (not found in taxonomy) |
| 24 | ...bildning. Teknisk utbildning, **högskola**, ingenjörsutbildning   - 5 år... | x |  |  | 8 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 25 | ...Teknisk utbildning, högskola, **ingenjörsutbildning**   - 5 års erfarenhet som tekn... | x |  |  | 19 | [Ingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/SNvJ_i6V_DZH) |
| 26 | ...ning   - 5 års erfarenhet som **teknisk förvaltare** - God datorvana och lätt för ... | x |  |  | 18 | [Teknisk förvaltning, fastigheter, **skill**](http://data.jobtechdev.se/taxonomy/concept/zYxB_rdv_AyB) |
| 27 | ...nhet som teknisk förvaltare - **God datorvana** och lätt för att sätta dig in... | x |  |  | 13 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 28 | ...ta dig in i och hantera olika **systemverktyg** - God kunskap om bygg -och in... |  | x |  | 13 | [systemverktyg för nätverkshantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FZZM_P8m_KPu) |
| 29 | ...yg - God kunskap om bygg -och **installationsteknik** - Erfarenhet av verksamhetsup... | x |  |  | 19 | [Installationsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/1VVe_BBY_rpQ) |
| 30 | ...av verksamhetsuppföljning och **ekonomi** - Erfarenhet av entreprenörsu... | x |  |  | 7 | [Ekonomi, undervisning högskolenivå, **skill**](http://data.jobtechdev.se/taxonomy/concept/9Ra9_2yh_jH4) |
| 30 | ...av verksamhetsuppföljning och **ekonomi** - Erfarenhet av entreprenörsu... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 31 | ...g och ekonomi - Erfarenhet av **entreprenörsuppföljning** - Erfarenhet av arbete inom r... | x |  |  | 23 | [övervaka entreprenörers arbete, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rhqJ_Dih_Tzh) |
| 32 | ...nhet av arbete inom ramen för **LOU** -  B-körkort     ____________... | x |  |  | 3 | [Upphandlingsbestämmelser, **skill**](http://data.jobtechdev.se/taxonomy/concept/SmZd_Jaa_BeY) |
| 33 | ... arbete inom ramen för LOU -  **B-körkort**     ______________________  H... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 34 | ...och verkar idag på 13 orter i **Sverige** med utgångspunkt i Stockholm.... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 35 | ... i Sverige med utgångspunkt i **Stockholm**. Teamets motto är Ödmjukhet, ... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| | **Overall** | | | **42** | **1141** | 42/1141 = **4%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Installationsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/1VVe_BBY_rpQ) |
| x |  |  | [Fastighetsförvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/4XyZ_6t8_TWq) |
| x |  |  | [Ekonomi, undervisning högskolenivå, **skill**](http://data.jobtechdev.se/taxonomy/concept/9Ra9_2yh_jH4) |
|  | x |  | [Fastighetsverksamhet, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/9uyq_gBA_8UJ) |
|  | x |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Fastighetsförvaltare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EPe8_EQu_YCL) |
|  | x |  | [systemverktyg för nätverkshantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FZZM_P8m_KPu) |
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x |  |  | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| x |  |  | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| x | x | x | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| x |  |  | [se över byggprojekt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NdCR_GnJ_KwV) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [Utredningsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/QAaJ_QeY_Q6P) |
| x |  |  | [Ingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/SNvJ_i6V_DZH) |
| x |  |  | [Upphandlingsbestämmelser, **skill**](http://data.jobtechdev.se/taxonomy/concept/SmZd_Jaa_BeY) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| x | x | x | [Fastighetsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/crzp_WfW_wWC) |
| x |  |  | [Fastighetsunderhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/fG9j_hFZ_Vm5) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Fastighetsekonomisk förvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/jmqj_shU_3VD) |
| x |  |  | (not found in taxonomy) |
| x |  |  | [övervaka entreprenörers arbete, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rhqJ_Dih_Tzh) |
|  | x |  | [Fastighetsförvaltning på uppdrag, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/vepN_y8A_oA6) |
| x |  |  | [Underhållsplanering, fastigheter, **skill**](http://data.jobtechdev.se/taxonomy/concept/ySUN_PZC_HQW) |
| x |  |  | [Teknisk förvaltning, fastigheter, **skill**](http://data.jobtechdev.se/taxonomy/concept/zYxB_rdv_AyB) |
| | | **3** | 3/27 = **11%** |