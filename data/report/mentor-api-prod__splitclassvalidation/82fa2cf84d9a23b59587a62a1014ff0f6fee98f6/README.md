# Results for '82fa2cf84d9a23b59587a62a1014ff0f6fee98f6'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [82fa2cf84d9a23b59587a62a1014ff0f6fee98f6](README.md) | 1 | 1638 | 6 | 2 | 21/73 = **29%** | 2/5 = **40%** |

## Source text

Personlig assistent till sportintresserad 14-årig kille i Sundbyberg Hej!    Vi söker en personlig assistent, mellan 20 och 30 år som vill göra vardagen enklare för en sportintresserad 14-årig kille med humor som för närvarande är rullstolsburen och har stor begränsning av sina armar efter ett hastigt insjuknande förra sommaren. Han har inga kognitiva svårigheter, är mycket intelligent, men har en talsvårighet pga av motoriska skäl orsakade av sjukdomen.  Detta yttrar sig i att han talar mycket tyst och ibland otydligt.    Tjänsten innebär bla handräckning, förflyttning och hjälp med personlig hygien mm. Då han är i behov av viss hjälp med kommunikationen måste du vara flytande i svenska i tal och skrift. Jobbet är 100% med arbetstiderna förlagda på vardagar i skolan och hemma. Han har skolskjuts från dörr till dörr som du som assistent följer med på både morgon och eftermiddag.    Låter detta intressant är du varmt välkommen med din ansökan. Tjänsten skall tillsättas i början av augusti så vi har löpande intervjuer.    Vi kommer begära ett utdrag från belastningsregistret för arbete med barn med funktionsnedsättning och du får gärna begära ut ett sådant i samband med ansökan för att underlätta anställningsprocessen. Länken finns här nedan:    https://polisen.se/tjanster-tillstand/belastningsregistret/   Kavon Care är ett litet familjeägt assistansbolag. Våra kunder och assistenter uppskattar oss bland annat för att vi alltid är tillgängliga och snabba i beslut som behöver tas. Vi har kollektivavtal, friskvårdsbidrag och andra sedvanliga tillägg och förmåner. För mer info om oss gå gärna in på www.kavoncare.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Personlig assistent** till sportintresserad 14-årig... | x |  |  | 19 | [personlig assistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qnSi_h1g_ENs) |
| 2 | ...rtintresserad 14-årig kille i **Sundbyberg** Hej!    Vi söker en personli... | x |  |  | 10 | [Sundbyberg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UTJZ_zHH_mJm) |
| 3 | ...ndbyberg Hej!    Vi söker en **personlig assistent**, mellan 20 och 30 år som vill... | x |  |  | 19 | [personlig assistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qnSi_h1g_ENs) |
| 4 | ...onen måste du vara flytande i **svenska** i tal och skrift. Jobbet är 1... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 5 | ...a i tal och skrift. Jobbet är **100%** med arbetstiderna förlagda på... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 6 | ...eslut som behöver tas. Vi har **kollektivavtal**, friskvårdsbidrag och andra s... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **21** | **73** | 21/73 = **29%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Sundbyberg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UTJZ_zHH_mJm) |
| x |  |  | [personlig assistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qnSi_h1g_ENs) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **2** | 2/5 = **40%** |