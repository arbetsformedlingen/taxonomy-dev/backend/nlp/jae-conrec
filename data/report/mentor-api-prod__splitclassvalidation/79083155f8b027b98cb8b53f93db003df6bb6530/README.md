# Results for '79083155f8b027b98cb8b53f93db003df6bb6530'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [79083155f8b027b98cb8b53f93db003df6bb6530](README.md) | 1 | 1025 | 11 | 8 | 55/120 = **46%** | 3/7 = **43%** |

## Source text

Frisörer till Studio West i Stenungsund Studio West söker frisörer  Vi söker dig som älskar att ge service till våra kunder!  Du har jobbat ett par år som frisör, gärna med gesällbrev och kan börja snarast.  Är du duktig på bruduppsättningar är det ett stort bonus.  Vi har massor att göra och hinner inte riktigt med så nu behöver vi din hjälp. Vi har många kunder som står på kö för att få hjälp från dig.  Hos oss får vi kunden att känna sig hemma genom att bjuda på en god kopp kaffe medan man väntar, pratar med och lyssnar under behandlingen.  Studio West ligger i en vacker och stilfull salong där den proffsiga personalen gör allt för att kunderna ska trivas.  Vi klipper, gör färgningar/alla hårvårdsbehandlingar, ger massage, hudvård och gör Botox/fillersbehandlingar.  Salongen ligger en trappa upp intill Systembolaget på Stenungsunds torg i Stenungsund.  Du som söker pratar och skriver svenska obehindrat, har jobbat ett par år som frisör. Gesällbrev är en merit men inte ett krav.  Välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Frisörer** till Studio West i Stenungsun... | x | x | 8 | 8 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 2 | Frisörer till Studio West i **Stenungsund** Studio West söker frisörer  V... | x |  |  | 11 | [Stenungsund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/wHrG_FBH_hoD) |
| 3 | ...Stenungsund Studio West söker **frisörer**  Vi söker dig som älskar att ... | x | x | 8 | 8 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 4 | ... Du har jobbat ett par år som **frisör**, gärna med gesällbrev och kan... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 5 | ... par år som frisör, gärna med **gesällbrev** och kan börja snarast.  Är ... | x | x | 10 | 10 | [Gesällbrev, frisör, **skill**](http://data.jobtechdev.se/taxonomy/concept/YcrR_5QB_3XL) |
| 6 | ...a snarast.  Är du duktig på **bruduppsättningar** är det ett stort bonus.  Vi h... | x |  |  | 17 | [Håruppsättningar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Uqth_qM3_Fyn) |
| 7 | ...enom att bjuda på en god kopp **kaffe** medan man väntar, pratar med ... |  | x |  | 5 | [Kaffe, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wqfj_Ka3_zu3) |
| 8 | ...olaget på Stenungsunds torg i **Stenungsund**.  Du som söker pratar och skr... | x |  |  | 11 | [Stenungsund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/wHrG_FBH_hoD) |
| 9 | ... som söker pratar och skriver **svenska** obehindrat, har jobbat ett pa... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 10 | ...h skriver svenska obehindrat, **har jobbat ett par år** som frisör. Gesällbrev är en ... | x |  |  | 21 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| 11 | ...at, har jobbat ett par år som **frisör**. Gesällbrev är en merit men i... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 12 | ...jobbat ett par år som frisör. **Gesällbrev** är en merit men inte ett krav... | x | x | 10 | 10 | [Gesällbrev, frisör, **skill**](http://data.jobtechdev.se/taxonomy/concept/YcrR_5QB_3XL) |
| | **Overall** | | | **55** | **120** | 55/120 = **46%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| x |  |  | [Håruppsättningar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Uqth_qM3_Fyn) |
|  | x |  | [Kaffe, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wqfj_Ka3_zu3) |
| x | x | x | [Gesällbrev, frisör, **skill**](http://data.jobtechdev.se/taxonomy/concept/YcrR_5QB_3XL) |
| x | x | x | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| x |  |  | [Stenungsund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/wHrG_FBH_hoD) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/7 = **43%** |