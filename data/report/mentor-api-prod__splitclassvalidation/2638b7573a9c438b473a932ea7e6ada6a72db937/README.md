# Results for '2638b7573a9c438b473a932ea7e6ada6a72db937'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2638b7573a9c438b473a932ea7e6ada6a72db937](README.md) | 1 | 2395 | 10 | 4 | 34/117 = **29%** | 4/8 = **50%** |

## Source text

Produktionsmedarbetare till KG-list i Norrhult med start efter sommaren Trivs du med varierande uppgifter och vill ha ett rörligt jobb? Är du ihärdig, flexibel och noggrann som person? Då kan det här vara rätt jobb för dig! Nu söker KG List i Norrhult produktionsmedarbetare med start efter sommaren.   KG List är Sveriges största limfogtillverkare i lövträ. Fabriken är belägen i Norrhult, mitt i Småland och levererar produkter till kunder i branscher som möbel, kök, bad, snickeri, dörrar, fönster och trappor.   Vad kommer du att göra? Du roterar på de olika stationerna inom produktionen. Packning, påfyllning, övervakning av maskiner, samt truckkörning med gods på pall kan förekomma. Det är en fördel om du är tekniskt intresserad och har industrierfarenhet sedan tidigare.   Det handlar om dig För att passa i rollen som produktionspersonal känner du som person ett stort ansvar och engagemang för ditt arbete och har god samarbetsförmåga. Du ska vara flexibel, ha en positiv inställning och van att hålla ett högt tempo.   Passar du in i profilen? * Vi söker dig som trivs med varierade arbetsuppgifter. * Gärna erfarenhet av tillverkning/produktion * Har tidigare erfarenhet av truckkörning på pall. * Bil & Körkort är ett krav * Flytande i svenska både i tal och skrift  Vad vi erbjuder dig  Första månaden får du en introduktion och den är förlagd på dagtid. Därefter ständig kväll , måndag-torsdag. Vi på Lernia erbjuder inledningsvis en heltidsanställning med visstidskontrakt.  Anställningsform / omfattning / tjänstgöringsort Visstidsanställning med möjlighet att bli anställd av kund efter 6 mån / Heltid / Norrhult  Tillträdesdag Start efter sommaren  Information Vi tillämpar löpande rekrytering och tjänstens kan tillsättas innan sista ansökningsdagen.  Välkommen med din ansökan redan idag!  Ansökan sker via www.lernia.se/jobb/  Forma framtiden med oss! Lernia är ett av Sveriges ledande företag inom kompetensutveckling och kompetensförsörjning. Vi finns över hela landet och har tjänster inom vuxenutbildning, bemanning, rekrytering och omställning. Varje år får 9 000 bemanningskonsulter jobb på Lernia. Som konsult kan du prova på olika roller, dra nynna nytta av din befintliga kompetens eller kanske ta nästa steg i karriären, samtidigt som du har samma trygghet som hos andra företag. Välkommen till Lernia – tillsammans formar vi din framtid!  Läs mer på lernia.se.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...? Är du ihärdig, flexibel och **noggrann** som person? Då kan det här va... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 2 | ...övervakning av maskiner, samt **truckkörning** med gods på pall kan förekomm... | x |  |  | 12 | [Truckförare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/MrcX_RDn_N53) |
| 3 | ...gifter. * Gärna erfarenhet av **tillverkning**/produktion * Har tidigare erf... | x | x | 12 | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 4 | ... * Har tidigare erfarenhet av **truckkörning** på pall. * Bil & Körkort är e... | x |  |  | 12 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 5 | ...truckkörning på pall. * Bil & **Körkort** är ett krav * Flytande i sven... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 6 | ...kort är ett krav * Flytande i **svenska** både i tal och skrift  Vad vi... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 7 | ...nia erbjuder inledningsvis en **heltidsanställning** med visstidskontrakt.  Anstäl... | x |  |  | 18 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 8 | ...vis en heltidsanställning med **visstidskontrakt**.  Anställningsform / omfattni... | x |  |  | 16 | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| 9 | ...omfattning / tjänstgöringsort **Visstidsanställning** med möjlighet att bli anställ... | x |  |  | 19 | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| 10 | ...nställd av kund efter 6 mån / **Heltid** / Norrhult  Tillträdesdag Sta... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **34** | **117** | 34/117 = **29%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Truckförare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/MrcX_RDn_N53) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x |  |  | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/8 = **50%** |