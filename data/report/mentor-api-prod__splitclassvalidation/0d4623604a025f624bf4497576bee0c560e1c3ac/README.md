# Results for '0d4623604a025f624bf4497576bee0c560e1c3ac'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0d4623604a025f624bf4497576bee0c560e1c3ac](README.md) | 1 | 4898 | 20 | 30 | 9/461 = **2%** | 12/26 = **46%** |

## Source text

Projektdirektör Large Civil projects NCC Infrastructure "Det bästa med min roll är den stora variationen av arbetsuppgifter och alla människor man möter, både kollegor, kunder och andra intressenter. Att leda ett stort komplext anläggningsprojekt betyder att man måste gilla att vara problemlösare, bollplank, ekonom och ibland psykolog, allt beroende på var projektet befinner sig. Det passar mig perfekt!” (Sofia Edoff, Projektdirektör)  Large Civil Projects utvecklar och leder komplexa infrastrukturprojekt i miljardklassen i Sverige. Inom divisionen samlas erfarna projektchefer, specialister och andra nyckelkompetenser för att möta de krav som ställs på den här typen av projekt – från anbud till färdigt projekt.  Rollen som Projektdirektör Som Projektdirektör är du ytterst ansvarig för projektet. Du leder och genomför erhållet projekt inom bestämda tids-, kvalitets- och kostnadsramar samt säkerställer rätt kvalitet och goda relationer med beställare och övriga intressenter.  Rollen innebär följande:   • Huvudansvar för ekonomi, styrning och kontroll av projektet samt att säkerställa att arbetet under anbudsfasen förs över till projektet på bästa sätt • Säkerställa att projektet följer bestämda tids-, kvalitets- och kostnadsramar, samt att arbetet sker i enlighet med processerna i verksamhetssystemet och dokumenteras på erforderligt sätt • Ekonomi/Resultatansvar • Ansvar för kvalitet och leverans enligt kontrakt • Arbetsmiljö- och miljöansvar • Personalansvar för tilldelad personal och huvudansvar för den övergripande resursplaneringen (inkl externa resurser) • Ansvar för arbetet i projektets ledningsgrupp.  Du rapporterar till en projektstyrelse och ansvarar för att skapa en god kultur och teamkänsla på projektet samt en god arbetsmiljö och effektiva arbetsprocesser. Det är ditt ansvar att driva ledningsgruppens arbete samt att information- och kommunikationsvägar fungerar på projektet.  Din profil Vi söker dig som har erfarenhet av att leda andra chefer och större organisationer med flera nivåer. Du har mångårig erfarenhet av produktion och produktionsledning, företrädesvis i stora och komplexa projekt över en halv miljard. Utöver det även erfarenhet av utförande- och totalentreprenader.  Rollen kräver en strategisk kompetens och förståelse för anläggningsprojekt samt vilka parametrar som påverkar ett lönsamt projekt. Du behöver mycket god förståelse för projektledning med förmåga att förstå samband mellan kalkyl, planering, ekonomistyrning, inköp, projektering och produktion.  Som person är du relationsskapande med god förmåga att förstå och kommunicera framgångsrikt med olika intressenter. Utöver det har du en mycket god förmåga att styra, leda och engagera medarbetare mot uppsatta mål.   Din förmåga att uttrycka sig på svenska och engelska i tal och skrift är god.  Ytterligare information Division Large Civil Projects är specialiserad på stora och komplexa infrastrukturprojekt i Sverige med huvudfokus på Stockholm, Göteborg och Malmö. Din placering blir på något av våra stora projekt i Stockholm eller som en del i ett team som arbetar i tidiga skeden.   Kontakt och ansökan Vid frågor är du välkommen att höra av dig till Thomas Widehag, Avdelningschef Large Civil Projects, 070 315 79 86 eller Anna Nyberg, HR specialist Rekrytering, 076 521 58 59. Urval och tillsättning startar tidigast vecka 33. Ansök senast den 2022-08-17.  Varmt välkommen med din ansökan!  Om oss På NCC blir du en del i en organisation med goda värderingar, hög miljömedvetenhet och en stark vilja att lyckas tillsammans.  Varje dag tar våra över 13 000 medarbetare beslut som förbättrar människors vardag, både idag och i morgon. Här arbetar du i en stark gemenskap tillsammans med engagerade och professionella kollegor som drivs av att lära nytt, nå uppsatta mål, dela erfarenheter och göra verklig skillnad tillsammans. Vi utmanar oss själva för att driva utvecklingen och skapar hållbara lösningar som för samhället framåt med ny kunskap.   Som ett av Nordens ledande bygg- och fastighetsutvecklingsföretag utvecklar vi kommersiella fastigheter, bygger skolor, sjukhus, bostäder, vägar, broar och annan infrastruktur som formar vårt sätt att leva, arbeta och resa i samhället. Genom vår industriverksamhet erbjuder vi produkter och tjänster med inriktning på stenmaterial och asfaltsproduktion, beläggningsuppdrag.  Vi värnar ett hållbart arbetsliv med starkt fokus på säkerhet, personlig utveckling och balans mellan jobb och fritid.  NCC ska spegla våra kunder såväl som samhället i stort och är beroende av medarbetare med olika kompetenser. Vi strävar efter att anställa människor med olika bakgrund och värdesätter den kunskap och erfarenhet det medför.  2021 omsatte NCC ca 53 miljarder SEK och NCC:s aktier är noterade på Nasdaq Stockholm.  Vi undanber oss vänligen men bestämt kontakt med rekryterare samt säljare av annons- eller bemanningslösningar.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ara problemlösare, bollplank, **ekonom** och ibland psykolog, allt ber... | x |  |  | 6 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 1 | ...ara problemlösare, bollplank, **ekonom** och ibland psykolog, allt ber... |  | x |  | 6 | [Övriga ekonomer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Y6yY_SuR_hVh) |
| 2 | ... bollplank, ekonom och ibland **psykolog**, allt beroende på var projekt... |  | x |  | 8 | [Psykolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xBo9_V9m_bfg) |
| 2 | ... bollplank, ekonom och ibland **psykolog**, allt beroende på var projekt... | x |  |  | 8 | [psykolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/yLc4_ZtB_oiN) |
| 3 | ...turprojekt i miljardklassen i **Sverige**. Inom divisionen samlas erfar... | x |  |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ...rdklassen i Sverige. Inom divi**sionen **samlas erfarna projektchefer, ... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 5 | ...följande:   • Huvudansvar för **ekonomi**, styrning och kontroll av pro... | x |  |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 6 | ... följer bestämda tids-, kvalit**ets- oc**h kostnadsramar, samt att arbe... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 7 | ...nteras på erforderligt sätt • **Ekonomi**/Resultatansvar • Ansvar för k... | x |  |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 8 | ...var för kvalitet och leverans **enligt **kontrakt • Arbetsmiljö- och mi... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 9 | ...ch leverans enligt kontrakt • **Arbetsmiljö**- och miljöansvar • Personalan... | x |  |  | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 10 | ...rbetsmiljö- och miljöansvar • **Personalansvar** för tilldelad personal och hu... | x |  |  | 14 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| 11 | ...jö- och miljöansvar • Personal**ansvar** för tilldelad personal och hu... |  | x |  | 6 | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
| 12 | ...miljöansvar • Personalansvar f**ö**r tilldelad personal och huvud... |  | x |  | 1 | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
| 13 | ...öansvar • Personalansvar för t**il**ldelad personal och huvudansva... |  | x |  | 2 | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
| 14 | ... Personalansvar för tilldelad **personal** och huvudansvar för den överg... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 15 | ...urser) • Ansvar för arbetet i **projektets led**ningsgrupp.  Du rapporterar ti... |  | x |  | 14 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| 16 | ...nsla på projektet samt en god **arbetsmiljö** och effektiva arbetsprocesser... | x |  |  | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 17 | ...ar att driva ledningsgruppens **arbete samt** att information- och kommunik... |  | x |  | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 18 | ...t av utförande- och totalentre**prenader.  Rollen **kräver en strategisk kompetens... |  | x |  | 18 | [Produktionsledning, gruva, **skill**](http://data.jobtechdev.se/taxonomy/concept/9QUy_Akh_FN5) |
| 18 | ...t av utförande- och totalentre**prenader.  Rollen **kräver en strategisk kompetens... |  | x |  | 18 | [Produktionsledning, husbyggnadsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/MUh1_LJt_EBw) |
| 18 | ...t av utförande- och totalentre**prenader.  Rollen **kräver en strategisk kompetens... |  | x |  | 18 | [Produktionsledning, anläggningsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/g2yR_Gdi_LUN) |
| 18 | ...t av utförande- och totalentre**prenader.  Rollen **kräver en strategisk kompetens... |  | x |  | 18 | [Produktionsledning, olja och gas, **skill**](http://data.jobtechdev.se/taxonomy/concept/h4NH_dzu_vu3) |
| 18 | ...t av utförande- och totalentre**prenader.  Rollen **kräver en strategisk kompetens... |  | x |  | 18 | [Produktionsledning, reklam, **skill**](http://data.jobtechdev.se/taxonomy/concept/kWj3_ZLo_MqK) |
| 19 | ...ver mycket god förståelse för **projektledning** med förmåga att förstå samban... | x |  |  | 14 | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| 20 | ...åga att förstå samband mellan **kalkyl**, planering, ekonomistyrning, ... | x |  |  | 6 | [Kalkylering, **skill**](http://data.jobtechdev.se/taxonomy/concept/hb3b_fH2_bvK) |
| 21 | ...and mellan kalkyl, planering, **ekonomistyrning**, inköp, projektering och prod... | x |  |  | 15 | [Ekonomistyrning, **skill**](http://data.jobtechdev.se/taxonomy/concept/awjU_C3D_XEJ) |
| 22 | ...ring, ekonomistyrning, inköp, **projektering** och produktion.  Som person ä... | x |  |  | 12 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 23 | ...ektering och produktion.  Som **person är du** relationsskapande med god för... |  | x |  | 12 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 24 | ...ektering och produktion.  Som **person är du r**elationsskapande med god förmå... |  | x |  | 14 | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| 25 | ...örmåga att förstå och kommunic**era framgångsri**kt med olika intressenter. Utö... |  | x |  | 15 | [Ekonomistyrning, **skill**](http://data.jobtechdev.se/taxonomy/concept/awjU_C3D_XEJ) |
| 26 | ...n förmåga att uttrycka sig på **svenska** och engelska i tal och skrift... | x |  |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 27 | ...t uttrycka sig på svenska och **engelska** i tal och skrift är god.  Ytt... | x |  |  | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 28 | ...l och skrift är god.  Ytterlig**a**re information Division Large ... |  | x |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 29 | ...och skrift är god.  Ytterligar**e** information Division Large Ci... |  | x |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 30 | ...och skrift är god.  Ytterligar**e inform**ation Division Large Civil Pro... |  | x |  | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 31 | ... skrift är god.  Ytterligare i**n**formation Division Large Civil... |  | x |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 32 | ...mplexa infrastrukturprojekt i **Sverige** med huvudfokus på Stockholm, ... | x |  |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 33 | ...t i Sverige med huvudfokus på **Stockholm**, Göteborg och Malmö. Din plac... | x |  |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 34 | ... med huvudfokus på Stockholm, **Göteborg** och Malmö. Din placering blir... | x |  |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 35 | ...us på Stockholm, Göteborg och **Malmö**. Din placering blir på något ... | x |  |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 36 | ...teborg och Malmö. Din placerin**g blir p**å något av våra stora projekt ... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 37 | ...acering blir på något av våra **stora p**rojekt i Stockholm eller som e... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 38 | ...något av våra stora projekt i **Stockholm** eller som en del i ett team s... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 39 | ... våra stora projekt i Stockhol**m ell**er som en del i ett team som a... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 40 | ...idiga skeden.   Kontakt och an**sökan Vid** frågor är du välkommen att hö... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 41 | ...ering, 076 521 58 59. Urval oc**h tillsättnin**g startar tidigast vecka 33. A... |  | x |  | 13 | [HR-specialist/HR-advisor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s28Q_sya_S2b) |
| 42 | ...i stort och är beroende av med**arbetare **med olika kompetenser. Vi strä... |  | x |  | 9 | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| 43 | ...e av medarbetare med olika kom**petenser. Vi strävar** efter att anställa människor ... |  | x |  | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| | **Overall** | | | **9** | **461** | 9/461 = **2%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Produktionsledning, gruva, **skill**](http://data.jobtechdev.se/taxonomy/concept/9QUy_Akh_FN5) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [Produktionsledning, husbyggnadsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/MUh1_LJt_EBw) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
|  | x |  | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
|  | x |  | [Övriga ekonomer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Y6yY_SuR_hVh) |
| x | x | x | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| x | x | x | [Ekonomistyrning, **skill**](http://data.jobtechdev.se/taxonomy/concept/awjU_C3D_XEJ) |
|  | x |  | [Produktionsledning, anläggningsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/g2yR_Gdi_LUN) |
|  | x |  | [Produktionsledning, olja och gas, **skill**](http://data.jobtechdev.se/taxonomy/concept/h4NH_dzu_vu3) |
| x |  |  | [Kalkylering, **skill**](http://data.jobtechdev.se/taxonomy/concept/hb3b_fH2_bvK) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Produktionsledning, reklam, **skill**](http://data.jobtechdev.se/taxonomy/concept/kWj3_ZLo_MqK) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
|  | x |  | [HR-specialist/HR-advisor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s28Q_sya_S2b) |
|  | x |  | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
|  | x |  | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
|  | x |  | [Psykolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xBo9_V9m_bfg) |
| x |  |  | [psykolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/yLc4_ZtB_oiN) |
| x | x | x | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **12** | 12/26 = **46%** |