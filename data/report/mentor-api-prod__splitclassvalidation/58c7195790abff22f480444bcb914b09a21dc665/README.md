# Results for '58c7195790abff22f480444bcb914b09a21dc665'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [58c7195790abff22f480444bcb914b09a21dc665](README.md) | 1 | 337 | 6 | 4 | 14/105 = **13%** | 2/6 = **33%** |

## Source text

Medicinska sekreterare spelar en viktig roll för kvaliteten och säkerheten på det viktiga dokumentationsarbete som utförs inom sjukvården. Yrket har utvecklats mycket under de senaste tjugo åren och idag är efterfrågan stor på medicinska sekreterare med hög administrativ kompetens och kunskaper inom bland annat IT, ekonomi och juridik.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Medicinska sekreterare** spelar en viktig roll för kva... | x |  |  | 22 | [medicinsk sekreterare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/QGm6_5mD_bry) |
| 2 | Medicinska **sekreterare** spelar en viktig roll för kva... |  | x |  | 11 | [Medicinska sekreterare, vårdadministratörer m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/2kLc_pto_DpV) |
| 3 | ...h idag är efterfrågan stor på **medicinska sekreterare** med hög administrativ kompete... | x |  |  | 22 | [medicinsk sekreterare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/QGm6_5mD_bry) |
| 4 | ...fterfrågan stor på medicinska **sekreterare** med hög administrativ kompete... |  | x |  | 11 | [Medicinska sekreterare, vårdadministratörer m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/2kLc_pto_DpV) |
| 5 | ...edicinska sekreterare med hög **administrativ kompetens** och kunskaper inom bland anna... | x |  |  | 23 | [sköta administration, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NEWh_NMX_RCZ) |
| 6 | ...ch kunskaper inom bland annat **IT**, ekonomi och juridik. | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 7 | ...unskaper inom bland annat IT, **ekonomi** och juridik. | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 8 | ...m bland annat IT, ekonomi och **juridik**. | x | x | 7 | 7 | [Juridik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z9ZH_7XU_Kkb) |
| | **Overall** | | | **14** | **105** | 14/105 = **13%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Medicinska sekreterare, vårdadministratörer m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/2kLc_pto_DpV) |
| x |  |  | [sköta administration, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NEWh_NMX_RCZ) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [medicinsk sekreterare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/QGm6_5mD_bry) |
| x | x | x | [Juridik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z9ZH_7XU_Kkb) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| | | **2** | 2/6 = **33%** |