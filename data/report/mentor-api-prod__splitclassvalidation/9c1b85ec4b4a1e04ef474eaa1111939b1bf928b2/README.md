# Results for '9c1b85ec4b4a1e04ef474eaa1111939b1bf928b2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9c1b85ec4b4a1e04ef474eaa1111939b1bf928b2](README.md) | 1 | 4434 | 21 | 17 | 64/341 = **19%** | 4/21 = **19%** |

## Source text

Projektingenjör NCC Civil Västmanland och Södermanland Arbetar du som projektingenjör inom mark och anläggning idag och är nyfiken på vad NCC kan erbjuda dig?  Inom NCC Civil Västmanland och Södermanland arbetar du med såväl medelstora som större projekt inom infrastruktur i regionen.   Rollen som Projektingenjör Som projektingenjör arbetar du som spindel i nätet ute i produktionen och stöttar platschefer i frågor som rör KMA, ekonomi, planering, inköp och övrig adminstration. I samverkan med projektchef och platschef upprättar du bland annat produktionskalkyl, planeringsunderlag, sköter löpande ekonomiavstämningar, prognoser, hanterar UR och ÄTOR. Du förväntas vara insatt i och följa upp pågående kontraktshandlingar och styrande dokument och har i samverkan med platschefen en kontinuerlig kontakt med både kunder och leverantörer.  Som projektingenjör stöttar du ett eller flera projekt samtidigt och medverkar till att dessa genomförs med högsta möjliga produktivitet och lönsamhet på ett arbetsmiljösäkert sätt i enlighet med NCCs gemensamma arbetssätt och värderingar.   Din profil För att trivas som projektingenjör hos oss är du en person som trivs med att ha ordning och reda och att ha en koordinerande roll med många pågående saker parallellt som du driver framåt. Du är van att arbeta med uppsatta tidplaner och kan ställa krav i jobbet lika väl som du är stöttande och ödmjuk. Vi är mycket intresserade av dina personliga egenskaper och tror att en av de viktigaste är förmågan att skapa och utveckla relationer, både med kunder och våra interna team bestående av chefer, specialister och övriga medarbetare.  Vi ser att du har en byggteknisk utbildning från gymnasium eller högskola alternativt har du arbetat några år i branschen och känner att du skulle vilja arbeta i en nära roll till platschefen. Du bör ha god förståelse för hur ett mark-, väg- och/eller anläggningsprojekt fungerar när det gäller ekonomi, planering, KMA, inköp och tekniska lösningar. Som person är du duktig på att planera, administrera och koordinera och van att arbeta i olika system, där erfarenhet från MAP, PlanCon, MS Project och/eller CAD program är meriterande. Du har ett affärssinne, är duktig på att göra risk- och möjlighetsbedömningar och att skapa resultat.  Ytterligare information Tjänsten är en tillsvidareanställning. Placering Västmanland och Södermanland (projekt inom hela geografin kan förekomma) och start enligt överenskommelse. Arbetstider är produktionstider. Den här tjänsten bakgrundskontrolleras i enlighet med NCC:s säkerhetskultur. B-körkort är ett krav  Kontakt och ansökan Registrera din ansökan och ditt CV nedan, dock ej senare än 2022-08-14. Urval och intervjuer sker tidigast vecka 33. Frågor kring rollen besvaras av Thomas Tengbert Produktionschef 070 344 45 78. Frågor kring rekryteringsprocessen besvaras av Anna Nyberg HR specialist Rekrytering 076 521 58 59.     Varmt välkommen med din ansökan!   Sökord: Projektingenjör   Om oss På NCC blir du en del i en organisation med goda värderingar, hög miljömedvetenhet och en stark vilja att lyckas tillsammans. Varje dag tar våra över 13 000 medarbetare beslut som förbättrar människors vardag, både idag och i morgon. Här arbetar du i en stark gemenskap tillsammans med engagerade och professionella kollegor som drivs av att lära nytt, nå uppsatta mål, dela erfarenheter och göra verklig skillnad tillsammans. Vi utmanar oss själva för att driva utvecklingen och skapar hållbara lösningar som för samhället framåt med ny kunskap. Som ett av Nordens ledande bygg- och fastighetsutvecklingsföretag utvecklar vi kommersiella fastigheter, bygger skolor, sjukhus, bostäder, vägar, broar och annan infrastruktur som formar vårt sätt att leva, arbeta och resa i samhället. Genom vår industriverksamhet erbjuder vi produkter och tjänster med inriktning på stenmaterial och asfaltsproduktion, beläggningsuppdrag. Vi värnar ett hållbart arbetsliv med starkt fokus på säkerhet, personlig utveckling och balans mellan jobb och fritid. NCC ska spegla våra kunder såväl som samhället i stort och är beroende av medarbetare med olika kompetenser. Vi strävar efter att anställa människor med olika bakgrund och värdesätter den kunskap och erfarenhet det medför. 2021 omsatte NCC ca 53 miljarder SEK och NCC:s aktier är noterade på Nasdaq Stockholm.  Vi undanber oss vänligen men bestämt kontakt med rekryterare samt säljare av annons- eller bemanningslösningar.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...r du som projektingenjör inom **mark och anläggning** idag och är nyfiken på vad NC... |  | x |  | 19 | [Teknisk upphandling, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xr4s_dWi_W7L) |
| 1 | ...r du som projektingenjör inom **mark och anläggning** idag och är nyfiken på vad NC... | x |  |  | 19 | [Markanläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/us91_jB5_a3A) |
| 2 | ...te i produktionen och stöttar **platschefer** i frågor som rör KMA, ekonomi... |  | x |  | 11 | [platschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/7ffE_ZTJ_Mix) |
| 2 | ...te i produktionen och stöttar **platschefer** i frågor som rör KMA, ekonomi... |  | x |  | 11 | [Platschef, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fCCr_Mp8_6oE) |
| 3 | ...schefer i frågor som rör KMA, **ekonomi**, planering, inköp och övrig a... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 4 | ...i, planering, inköp och övrig **adminstration**. I samverkan med projektchef ... | x |  |  | 13 | [Administration av infrastrukturprogram, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/GZ6W_Jjf_66K) |
| 5 | ...samverkan med projektchef och **platschef** upprättar du bland annat prod... |  | x |  | 9 | [platschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/7ffE_ZTJ_Mix) |
| 5 | ...samverkan med projektchef och **platschef** upprättar du bland annat prod... | x | x | 9 | 9 | [Platschef, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fCCr_Mp8_6oE) |
| 6 | ...ument och har i samverkan med **platschefen** en kontinuerlig kontakt med b... |  | x |  | 11 | [platschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/7ffE_ZTJ_Mix) |
| 6 | ...ument och har i samverkan med **platschefen** en kontinuerlig kontakt med b... | x | x | 11 | 11 | [Platschef, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fCCr_Mp8_6oE) |
| 7 | ...betare.  Vi ser att du har en **byggteknisk utbildning** från gymnasium eller högskola... | x |  |  | 22 | [Teknisk linje, byggteknik  (ingenjörsutbildning), **keyword**](http://data.jobtechdev.se/taxonomy/concept/UanS_xtr_fee) |
| 8 | ...n byggteknisk utbildning från **gymnasium** eller högskola alternativt ha... |  | x |  | 9 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 9 | ...bildning från gymnasium eller **högskola** alternativt har du arbetat nå... | x |  |  | 8 | [Högskoleingenjörsutbildning, byggteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/sP52_Lyv_71S) |
| 10 | ...ja arbeta i en nära roll till **platschefen**. Du bör ha god förståelse för... |  | x |  | 11 | [platschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/7ffE_ZTJ_Mix) |
| 10 | ...ja arbeta i en nära roll till **platschefen**. Du bör ha god förståelse för... | x | x | 11 | 11 | [Platschef, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fCCr_Mp8_6oE) |
| 11 | ...ha god förståelse för hur ett **mark**-, väg- och/eller anläggningsp... | x |  |  | 4 | [Markanläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/us91_jB5_a3A) |
| 12 | ...förståelse för hur ett mark-, **väg**- och/eller anläggningsprojekt... | x |  |  | 3 | [Anläggning av vägar och motorvägar, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/MXFf_MAe_yya) |
| 13 | ...hur ett mark-, väg- och/eller **anläggningsprojekt** fungerar när det gäller ekono... | x |  |  | 18 | [Markanläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/us91_jB5_a3A) |
| 14 | ...ojekt fungerar när det gäller **ekonomi**, planering, KMA, inköp och te... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 15 | ... är du duktig på att planera, **administrera** och koordinera och van att ar... | x |  |  | 12 | [Administration av infrastrukturprogram, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/GZ6W_Jjf_66K) |
| 16 | ...a system, där erfarenhet från **MAP**, PlanCon, MS Project och/elle... | x |  |  | 3 | [Mark/infrastruktur-AutoCAD Map 3D, **skill**](http://data.jobtechdev.se/taxonomy/concept/zSbb_3rY_Z7H) |
| 17 | ...erfarenhet från MAP, PlanCon, **MS Project** och/eller CAD program är meri... | x | x | 10 | 10 | [MS Project, **skill**](http://data.jobtechdev.se/taxonomy/concept/DJgq_zBj_bKt) |
| 18 | ...PlanCon, MS Project och/eller **CAD** program är meriterande. Du ha... | x |  |  | 3 | [CAD - Datorstödd konstruktion, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/mTLu_uo2_gq2) |
| 19 | ...ssinne, är duktig på att göra **risk**- och möjlighetsbedömningar oc... | x |  |  | 4 | [Risk management, **skill**](http://data.jobtechdev.se/taxonomy/concept/bHQT_Bhn_w3S) |
| 20 | ...re information Tjänsten är en **tillsvidareanställning**. Placering Västmanland och Sö... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 21 | ...svidareanställning. Placering **Västmanland** och Södermanland (projekt ino... | x |  |  | 11 | [Västmanlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/G6DV_fKE_Viz) |
| 22 | ...ng. Placering Västmanland och **Södermanland** (projekt inom hela geografin ... | x |  |  | 12 | [Södermanlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/s93u_BEb_sx2) |
| 23 | ...et med NCC:s säkerhetskultur. **B-körkort** är ett krav  Kontakt och ansö... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 24 | ...essen besvaras av Anna Nyberg **HR specialist** Rekrytering 076 521 58 59.   ... |  | x |  | 13 | [HR-specialist/HR-advisor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s28Q_sya_S2b) |
| 25 | ...pdrag. Vi värnar ett hållbart **arbetsliv** med starkt fokus på säkerhet,... |  | x |  | 9 | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| 26 | ...med starkt fokus på säkerhet, **personlig utveckling** och balans mellan jobb och fr... |  | x |  | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| | **Overall** | | | **64** | **341** | 64/341 = **19%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [platschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/7ffE_ZTJ_Mix) |
| x | x | x | [MS Project, **skill**](http://data.jobtechdev.se/taxonomy/concept/DJgq_zBj_bKt) |
| x |  |  | [Västmanlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/G6DV_fKE_Viz) |
| x |  |  | [Administration av infrastrukturprogram, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/GZ6W_Jjf_66K) |
| x |  |  | [Anläggning av vägar och motorvägar, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/MXFf_MAe_yya) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [Teknisk linje, byggteknik  (ingenjörsutbildning), **keyword**](http://data.jobtechdev.se/taxonomy/concept/UanS_xtr_fee) |
|  | x |  | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [Teknisk upphandling, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xr4s_dWi_W7L) |
| x |  |  | [Risk management, **skill**](http://data.jobtechdev.se/taxonomy/concept/bHQT_Bhn_w3S) |
| x | x | x | [Platschef, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fCCr_Mp8_6oE) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [CAD - Datorstödd konstruktion, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/mTLu_uo2_gq2) |
|  | x |  | [HR-specialist/HR-advisor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s28Q_sya_S2b) |
| x |  |  | [Södermanlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/s93u_BEb_sx2) |
| x |  |  | [Högskoleingenjörsutbildning, byggteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/sP52_Lyv_71S) |
| x |  |  | [Markanläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/us91_jB5_a3A) |
|  | x |  | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x |  |  | [Mark/infrastruktur-AutoCAD Map 3D, **skill**](http://data.jobtechdev.se/taxonomy/concept/zSbb_3rY_Z7H) |
| | | **4** | 4/21 = **19%** |