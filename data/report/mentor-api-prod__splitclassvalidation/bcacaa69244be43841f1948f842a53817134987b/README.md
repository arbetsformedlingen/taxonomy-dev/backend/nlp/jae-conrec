# Results for 'bcacaa69244be43841f1948f842a53817134987b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [bcacaa69244be43841f1948f842a53817134987b](README.md) | 1 | 2589 | 24 | 17 | 118/328 = **36%** | 8/22 = **36%** |

## Source text

Maskinoperatörer sökes till kund i Katrineholm Om kunden Vi söker maskinoperatörer, gärna med erfarenhet av att arbeta i industrimiljö åt en kund i Katrineholm.  Om oss A-Staffing är auktoriserade inom bemanning samt medlemmar i Almega kompetensföretagen, Byggföretagen och Installatörsföretagen (IN) så du kan vara säker på vår kompetens samt att du alltid har rätt anställningsvillkor. Inom A-Staffing verkar A-Staffing Sweden AB, A-Staffing Construction AB och A-Staffing Nordic AB.  Vår prioritet är öppenhet och tillgänglighet samt din trivsel, hälsa och säkerhet. Vi är även kvalitets- och miljöcertifierade enligt ISO 9001 och 14001.  Vi levererar tjänster inom bygg-, anläggnings- och elteknikbranschen samt tillverkningsindustrin. Det tidigare namnet har varit VMP Group AB, namnändringen gjordes under 2021.  Arbetsbeskrivning Programmering och körning av CNC/NC-maskin. Jobbet innefattar ritningsläsning och kontrollmätning med olika mätdon. Övriga arbetsuppgifter är klassificering och återrapportering av materialet, tillsyn och upprätthållande av maskiner. Det ingår även ansvar för planering och förberedelser utifrån arbetsorder så att arbetet flyter på utan onödiga avbrott. Till arbetet hör att utföra eget planerat underhåll av maskinen samt hjälpa underhållspersonalen vid planerade stopp. Du kommer jobba i team och vara en viktig del i det ständiga förbättringsarbetet.  Utbildning Minst gymnasieutbildning med praktisk inriktning, helst inom industri. Truck- och traverskort är meriterande. Heta arbeten är önskvärt. CNC utbildning är meriterande men praktisk erfarenhet väger tyngst.  Erfarenhet Erfarenhet från industri eller annan praktisk bakgrund är ett krav. Vi ser helst att du har minst ett par års erfarenhet av liknande arbetsuppgifter samt erfarenhet av ritningsläsning och att du kan använda olika mätdon. Är du erfaren CNC-operatör och har jobbat i Okumas styrsystem så är det väldigt meriterande.  Personliga kvalifikationer Har du ett bra omdöme med högt säkerhetstänk så passar denna tjänst perfekt för dig. Flera arbetsmoment är tunga och kräver därför en god fysik. Du ska vara händig och van vid att arbeta med verktyg och gärna i en industrimiljö. Du ska även ha möjlighet att jobba skift. För samtliga tjänster krävs god svenska i tal och skrift då arbetsplatsen har säkerhet som högsta prioritet. Företaget värdesätter god samarbetsförmåga, lojalitet, engagemang och ansvarstagande.  Ansökan Intervjuer, urval och tillsättning kommer att ske löpande under annonseringstiden. Vänta därför inte med att skicka in din ansökan om du är intresserad!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Maskinoperatörer** sökes till kund i Katrineholm... | x |  |  | 16 | [CNC-operatör/FMS-operatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EvmU_s9w_euP) |
| 1 | **Maskinoperatörer** sökes till kund i Katrineholm... |  | x |  | 16 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 2 | ...noperatörer sökes till kund i **Katrineholm** Om kunden Vi söker maskinoper... | x | x | 11 | 11 | [Katrineholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/snx9_qVD_Dr1) |
| 3 | ...atrineholm Om kunden Vi söker **maskinoperatörer**, gärna med erfarenhet av att ... | x |  |  | 16 | [CNC-operatör/FMS-operatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EvmU_s9w_euP) |
| 3 | ...atrineholm Om kunden Vi söker **maskinoperatörer**, gärna med erfarenhet av att ... |  | x |  | 16 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 4 | ... i industrimiljö åt en kund i **Katrineholm**.  Om oss A-Staffing är auktor... | x | x | 11 | 11 | [Katrineholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/snx9_qVD_Dr1) |
| 5 | ...lgänglighet samt din trivsel, **hälsa** och säkerhet. Vi är även kval... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 6 | ...nder 2021.  Arbetsbeskrivning **Programmering** och körning av CNC/NC-maskin.... | x | x | 13 | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 7 | ... Programmering och körning av **CNC**/NC-maskin. Jobbet innefattar ... |  | x |  | 3 | [CNC, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zXvT_Gvb_mPj) |
| 8 | .../NC-maskin. Jobbet innefattar **ritningsläsning** och kontrollmätning med olika... | x | x | 15 | 15 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 9 | ...ngsarbetet.  Utbildning Minst **gymnasieutbildning** med praktisk inriktning, hels... | x |  |  | 18 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 9 | ...ngsarbetet.  Utbildning Minst **gymnasieutbildning** med praktisk inriktning, hels... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 10 | ...iktning, helst inom industri. **Truck**- och traverskort är meriteran... | x |  |  | 5 | [Truckvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/pGts_PKK_c8W) |
| 11 | ...iktning, helst inom industri. **Truck- oc**h traverskort är meriterande. ... |  | x |  | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 12 | ...lst inom industri. Truck- och **traverskort** är meriterande. Heta arbeten ... | x | x | 11 | 11 | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
| 13 | ...h traverskort är meriterande. **Heta arbeten** är önskvärt. CNC utbildning ä... | x |  |  | 12 | [Certifikat, heta arbeten, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q7eC_B7g_QiU) |
| 14 | ...de. Heta arbeten är önskvärt. **CNC** utbildning är meriterande men... |  | x |  | 3 | [CNC, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zXvT_Gvb_mPj) |
| 15 | ...Vi ser helst att du har minst **ett par års erfarenhet** av liknande arbetsuppgifter s... | x |  |  | 22 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 16 | ...suppgifter samt erfarenhet av **ritningsläsning** och att du kan använda olika ... | x | x | 15 | 15 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 17 | ...a olika mätdon. Är du erfaren **CNC-operatör** och har jobbat i Okumas styrs... | x | x | 12 | 12 | [CNC-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ezxx_3Jf_Li6) |
| 18 | ...CNC-operatör och har jobbat i **Okumas styrsystem** så är det väldigt meriterande... | x |  |  | 17 | [CNC-programmering, Okuma, **skill**](http://data.jobtechdev.se/taxonomy/concept/yYvR_uTu_o6c) |
| 19 | ...är tunga och kräver därför en **god fysik**. Du ska vara händig och van v... | x | x | 9 | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 20 | ...ig och van vid att arbeta med **verktyg** och gärna i en industrimiljö.... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 21 | ... Du ska även ha möjlighet att **jobba skift**. För samtliga tjänster krävs ... | x |  |  | 11 | [Skiftarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/YnG9_nVP_CsZ) |
| 22 | ...r samtliga tjänster krävs god **svenska** i tal och skrift då arbetspla... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 23 | ...et. Företaget värdesätter god **samarbetsförmåga**, lojalitet, engagemang och an... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 24 | ...ga, lojalitet, engagemang och **ansvarstagande**.  Ansökan Intervjuer, urval o... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| | **Overall** | | | **118** | **328** | 118/328 = **36%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [CNC-operatör/FMS-operatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EvmU_s9w_euP) |
| x |  |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| x | x | x | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
| x |  |  | [Certifikat, heta arbeten, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q7eC_B7g_QiU) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
|  | x |  | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Skiftarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/YnG9_nVP_CsZ) |
| x | x | x | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [CNC-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ezxx_3Jf_Li6) |
|  | x |  | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| x | x | x | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x |  |  | [Truckvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/pGts_PKK_c8W) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| x | x | x | [Katrineholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/snx9_qVD_Dr1) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x |  |  | [CNC-programmering, Okuma, **skill**](http://data.jobtechdev.se/taxonomy/concept/yYvR_uTu_o6c) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
|  | x |  | [CNC, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zXvT_Gvb_mPj) |
| | | **8** | 8/22 = **36%** |