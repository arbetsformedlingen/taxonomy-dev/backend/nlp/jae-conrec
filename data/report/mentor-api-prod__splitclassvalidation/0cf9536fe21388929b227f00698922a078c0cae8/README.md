# Results for '0cf9536fe21388929b227f00698922a078c0cae8'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0cf9536fe21388929b227f00698922a078c0cae8](README.md) | 1 | 4338 | 28 | 26 | 107/402 = **27%** | 5/20 = **25%** |

## Source text

Inköpare som vill utvecklas i Nacka kommun! Vill du bidra till bättre affärer i Nacka? Nu har du chansen att bli en del av en kompetent, utvecklingsorienterad och effektiv inköpsavdelning!   Ditt uppdrag  Nu har du möjligheten att få utvecklas inom inköpsområdet tillsammans med ett härligt gäng i Nacka Kommun.  Nacka arbetar kategoristyrt och har flera stora, spännande upphandlingar på gång. Nu söker vi dig som vill vara med på vår utvecklingsresa för att skapa framtidens Nacka. Läs gärna mer om enheten och hur vi arbetar här: https://www.nacka.se/medarbetare/enheter/inkopsenheten/om-enheten/  I rollen som inköpare arbetar du kategoribaserat och stödjs vid behov av en kategoriansvarig. Du kommer arbeta med att genomföra inköpsprojekt med olika komplexitet inom flera områden - alltid i nära samverkan med verksamheten.  Du får en viktig roll för att driva inköpsprocessen samt de kommersiella aspekterna av de affärer kommunen gör i enlighet med LOU. Du jobbar projektorienterat tillsammans med ansvariga chefer, projektledare och andra medarbetare inom kommunen. I ditt uppdrag ingår;   - Kravspecifikation, marknadsanalys och att driva upphandlingarna.  - Att delta i leverans- och leverantörsuppföljningsprocesserna.  - Utveckla affärsmässigheten och de kommersiella aspekterna ute i verksamheterna.  Målet är att Nacka göra goda affärer i enlighet med LOU som bidrar till Nackas utveckling.  Som inköpare blir du en del av inköpsenheten, som är ett härligt, positivt team på 18 personer med höga ambitioner. Din närmsta chef blir inköpschef Sebastian Nordgren, som har gedigen erfarenhet av inköpsverksamhet både i privat och offentlig sektor.   Är det här du?   Vi söker dig som har erfarenhet av att arbeta med offentliga inköp gärna i en politiskt styrd eller större organisation och som stimuleras av mångfalden i både frågeställningar och metoder för att kunna bidra till kommunens affärer och att utveckla samhället. Du har relevant akademisk utbildning och med största sannolikhet har du arbetat med inköp i minst 2-3 år i privat och/eller offentlig sektor. Du bör ha erfarenhet av att genomföra upphandlingar i enlighet med LOU. Nu har du chansen att få vara en del i att bygga och skapa framtidens Nacka.  Dina personliga egenskaper För att lyckas ser vi gärna att du har ett genuint intresse för inköparens roll som affärspartner. Vi fäster stor vikt vid dina personliga egenskaper och söker dig som;   - Är målinriktad och har hög egen drivkraft.  - Är kommunikativ och har fallenhet för att bygga relationer.  - Har god samarbetsförmåga.  - Är serviceinriktad och delar med dig av din kunskap.     Vi erbjuder  Vi erbjuder dig att få vara med på en spännande resa, där du får möjlighet att arbeta i en bred självständig roll. I Nacka får du arbeta i en kommun som vill vara bäst i Sverige på att vara kommun, och som har öppenhet och mångfald som sin vision och en grundläggande värdering att lita på människors kunskap, vilja och förmåga.  Blir du vår nya kollega så erbjuder vi dig: gym i huset, friskvårdsersättning, förmånscykelserbjudande samt möjlighet att växla semesterersättning mot fler semesterdagar. Arbetsplatsen ligger i Nacka stadshus i öppna, ljusa lokaler, granne med Nacka forum och 15 min med buss från Slussen och 20 minuter från Gustavsberg.   Intresserad?   Tjänsten är en tillsvidareanställning på heltid med tillträde enligt överenskommelse. Provanställning kan komma att tillämpas.  Om du vill bli en del av Nacka kommun, tveka inte att skicka in din ansökan! Vill du veta mer om tjänsten är du välkommen att kontakta mig på:  sebastian.nordgren@nacka.se, 070 431 91 60 https://www.linkedin.com/in/sebastian-nordgren-189264/  Urvalet kommer att ske löpande och denna tjänst kan komma att tillsättas före ansökningstidens utgång. Sista ansökningsdag är 2022-08-21.      Vi ser fram emot att få din ansökan!     Nacka är en av Sveriges mest attraktiva och välrenommerade kommuner. Med fokus på mål, resultat, hållbart medarbetarengagemang och ständig utveckling strävar Nacka efter att bli bäst på att vara kommun. Nacka kommun är arbetsgivaren för dig som med stor frihet förnyar och gör skillnad.    Inför rekryteringsarbetet har Nacka kommun tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför kontakt med mediasäljare, rekryteringssajter och liknande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Inköpare** som vill utvecklas i Nacka ko... | x | x | 8 | 8 | [Inköpare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KVVN_sqH_Wpz) |
| 2 | Inköpare som vill utvecklas i **Nacka** kommun! Vill du bidra till bä... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 3 | ...u bidra till bättre affärer i **Nacka**? Nu har du chansen att bli en... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 4 | ...ammans med ett härligt gäng i **Nacka** Kommun.  Nacka arbetar kate... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 5 | ...rligt gäng i Nacka Kommun.  **Nacka** arbetar kategoristyrt och har... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 6 | ...resa för att skapa framtidens **Nacka**. Läs gärna mer om enheten och... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 7 | ...ten/om-enheten/  I rollen som **inköpare** arbetar du kategoribaserat oc... | x | x | 8 | 8 | [Inköpare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KVVN_sqH_Wpz) |
| 8 | ...at och stödjs vid behov av en **kategoriansvarig**. Du kommer arbeta med att gen... | x | x | 16 | 16 | [Kategorichef/Kategoriansvarig, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xQP2_w9J_jbZ) |
| 9 | ...sammans med ansvariga chefer, **projektledare** och andra medarbetare inom ko... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 10 | ...ingår;   - Kravspecifikation, **marknadsanalys** och att driva upphandlingarna... |  | x |  | 14 | [marknadsanalys, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bNvJ_y92_J7b) |
| 11 | ...verksamheterna.  Målet är att **Nacka** göra goda affärer i enlighet ... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 12 | ...ighet med LOU som bidrar till **Nackas** utveckling.  Som inköpare bli... | x |  |  | 6 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 13 | ... till Nackas utveckling.  Som **inköpare** blir du en del av inköpsenhet... | x | x | 8 | 8 | [Inköpare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KVVN_sqH_Wpz) |
| 14 | ...tioner. Din närmsta chef blir **inköpschef** Sebastian Nordgren, som har g... | x | x | 10 | 10 | [Inköpschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EbSk_Hv8_qog) |
| 14 | ...tioner. Din närmsta chef blir **inköpschef** Sebastian Nordgren, som har g... |  | x |  | 10 | [inköpschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/YoG7_kES_zzQ) |
| 14 | ...tioner. Din närmsta chef blir **inköpschef** Sebastian Nordgren, som har g... |  | x |  | 10 | [inköpschef, läderråvara, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/sV29_bn2_8qW) |
| 15 | ...är du?   Vi söker dig som har **erfarenhet av att arbeta med offentliga inköp** gärna i en politiskt styrd el... | x |  |  | 45 | [Inköpsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/eKuz_edm_kmi) |
| 16 | ...la samhället. Du har relevant **akademisk utbildning** och med största sannolikhet h... | x |  |  | 20 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 17 | ...et har du arbetat med inköp i **minst 2-3 år** i privat och/eller offentlig ... | x |  |  | 12 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 18 | ...r offentlig sektor. Du bör ha **erfarenhet av att genomföra upphandlingar** i enlighet med LOU. Nu har du... | x |  |  | 41 | [utföra upphandlingsprocesser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DFY6_CRj_i2N) |
| 19 | ...tt bygga och skapa framtidens **Nacka**.  Dina personliga egenskaper ... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 20 | ...har hög egen drivkraft.  - Är **kommunikativ** och har fallenhet för att byg... | x |  |  | 12 | [Kommunikationsförmåga - Inköpare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/ZTEB_W6w_oWP) |
| 21 | ...tiv och har fallenhet för att **bygga relationer**.  - Har god samarbetsförmåga.... | x |  |  | 16 | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| 22 | ... bygga relationer.  - Har god **samarbetsförmåga**.  - Är serviceinriktad och de... | x |  |  | 16 | [Samarbetsförmåga - Inköpare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/n4qM_KrG_9dq) |
| 23 | ...r god samarbetsförmåga.  - Är **serviceinriktad** och delar med dig av din kuns... | x |  |  | 15 | [Serviceinriktad - Inköpsassistent, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/bbwa_gDs_oc7) |
| 24 | ...öjlighet att arbeta i en bred **självständig** roll. I Nacka får du arbeta i... |  | x |  | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 25 | ... en bred självständig roll. I **Nacka** får du arbeta i en kommun som... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 26 | ...n kommun som vill vara bäst i **Sverige** på att vara kommun, och som h... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 27 | ...dagar. Arbetsplatsen ligger i **Nacka** stadshus i öppna, ljusa lokal... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 28 | ...na, ljusa lokaler, granne med **Nacka** forum och 15 min med buss frå... | x |  |  | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 29 | ...Intresserad?   Tjänsten är en **tillsvidareanställning** på heltid med tillträde enlig... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 30 | ... en tillsvidareanställning på **heltid** med tillträde enligt överensk... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 31 | ...as.  Om du vill bli en del av **Nacka** kommun, tveka inte att skicka... | x | x | 5 | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 32 | ... emot att få din ansökan!     **Nacka** är en av Sveriges mest attrak... |  | x |  | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 33 | ...ch ständig utveckling strävar **Nacka** efter att bli bäst på att var... |  | x |  | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 34 | ... bli bäst på att vara kommun. **Nacka** kommun är arbetsgivaren för d... |  | x |  | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| 35 | ...Inför rekryteringsarbetet har **Nacka** kommun tagit ställning till r... |  | x |  | 5 | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
| | **Overall** | | | **107** | **402** | 107/402 = **27%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [utföra upphandlingsprocesser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DFY6_CRj_i2N) |
| x | x | x | [Inköpschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EbSk_Hv8_qog) |
| x |  |  | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| x | x | x | [Inköpare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KVVN_sqH_Wpz) |
|  | x |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
|  | x |  | [inköpschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/YoG7_kES_zzQ) |
| x |  |  | [Kommunikationsförmåga - Inköpare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/ZTEB_W6w_oWP) |
| x | x | x | [Nacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aYA7_PpG_BqP) |
|  | x |  | [marknadsanalys, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bNvJ_y92_J7b) |
| x |  |  | [Serviceinriktad - Inköpsassistent, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/bbwa_gDs_oc7) |
| x |  |  | [Inköpsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/eKuz_edm_kmi) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x |  |  | [Samarbetsförmåga - Inköpare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/n4qM_KrG_9dq) |
|  | x |  | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
|  | x |  | [inköpschef, läderråvara, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/sV29_bn2_8qW) |
| x | x | x | [Kategorichef/Kategoriansvarig, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xQP2_w9J_jbZ) |
| | | **5** | 5/20 = **25%** |