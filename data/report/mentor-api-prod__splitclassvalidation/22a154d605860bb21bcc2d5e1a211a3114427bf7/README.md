# Results for '22a154d605860bb21bcc2d5e1a211a3114427bf7'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [22a154d605860bb21bcc2d5e1a211a3114427bf7](README.md) | 1 | 3053 | 15 | 13 | 36/176 = **20%** | 7/16 = **44%** |

## Source text

This is Deploy söker Frontend Developer Consultant You will join a highly motivated team and will be working on a modern solution. We are looking for technology experts who want to make an impact on new business by applying their best practices. With your teammates, you’ll be solving complex code related challenges and you’ll be continuously making an impact!  Tasks: - Have a strong voice in choosing languages, frameworks and application architecture for new projects. Currently used technologies in selection include React, Angular, Vue, TypeScript, GraphQL, ReactNative. - Be involved in infrastructure work such as cloud server orchestration, with tools like Docker, Kubernetes. - Be exposed to the whole product development process, in an environment where your ideas count. - Participate in code review, to make sure we ship the best possible code. - Work closely with the business team, fellow developers and UX experts to plan and execute product releases with an agile team.     Are you the one we are looking for? - Have a minimum five years actively working with JavaScript. - Have a good understanding of the current state of JavaScript. - Have production experience in at least one of the main JavaScript Frameworks, e.g., React, Vue, or Angular. - Have experience in setting up and configuring webpack - Have experience in TypeScript and GraphQL - Have experience in working with Node/Express. - Have a good knowledge of HTML and CSS - Have a good eye for details   About the company To be on the forefront of digitalisation and IT you have to rely on people. That is why Deploy as an organization is built on trust and responsibility, with a comprehensive personal development program. All consultants work on-site with companys clients, but while its on-site all of Deploys combined knowledge is available. A culture where you are never afraid of turning to each other for input - because they cherish diverse experiences, knowledge sharing and personal values at Deploy. Since this is Deploy started in 2017 they have grown fast and employed 10+ people and have assisted clients such as Klarna, BoardClic, Care to translate, and Jobylon to mention a few. Deploy produce work that they can be proud of, and it is done through a tight-knit team built on trust and responsibility.    As employee you will receive: - A unique opportunity of being a part of a really dynamic scaleup-team! - Possibility to continuously grow and gain valuable experience - An experienced and creative team to learn from - A strong, care-oriented community, eager to help eachother - Swedish benefits such as pension, insurances, health care allowance, phone and computer   Submit your application today! Does this sound like something for you? You are welcome with your application! Access is by agreement. We read applications and call for an interview on an ongoing basis. We look forward to hear from you!  Work place: Stockholm  Start: Immediately taking into account the notice period  Salary: Basic salary  Extent: Full-time (consultant work on-site)

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | This is Deploy söker **Frontend Developer** Consultant You will join a hi... |  | x |  | 18 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 2 | ...s in selection include React, **Angular**, Vue, TypeScript, GraphQL, Re... |  | x |  | 7 | [ramverk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/JD8j_tGG_cJT) |
| 3 | ... include React, Angular, Vue, **TypeScript**, GraphQL, ReactNative. - Be i... | x | x | 10 | 10 | [TypeScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/USj9_RR6_Q1z) |
| 4 | ...rchestration, with tools like **Docker**, Kubernetes. - Be exposed to ... | x | x | 6 | 6 | [Docker, container-plattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZipR_ckJ_YQC) |
| 5 | ...s team, fellow developers and **UX** experts to plan and execute p... | x |  |  | 2 | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| 6 | ...cute product releases with an **agile** team.     Are you the one we ... |  | x |  | 5 | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
| 7 | ... we are looking for? - Have a **minimum five years** actively working with JavaScr... | x |  |  | 18 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 8 | ...e years actively working with **JavaScript**. - Have a good understanding ... | x | x | 10 | 10 | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
| 9 | ...nding of the current state of **JavaScript**. - Have production experience... | x | x | 10 | 10 | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
| 10 | ...e in at least one of the main **JavaScript** Frameworks, e.g., React, Vue,... | x |  |  | 10 | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
| 11 | ... webpack - Have experience in **TypeScript** and GraphQL - Have experience... | x |  |  | 10 | [TypeScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/USj9_RR6_Q1z) |
| 12 | ... Have experience in working wi**th Node/Ex**press. - Have a good knowledge... |  | x |  | 10 | [TypeScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/USj9_RR6_Q1z) |
| 13 | ...ve experience in working with **Node**/Express. - Have a good knowle... | x |  |  | 4 | [Node, exekveringsmiljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/McKJ_SG4_hCM) |
| 14 | ... Node/Express. - Have a good k**nowl**edge of HTML and CSS - Have a ... |  | x |  | 4 | [Node, exekveringsmiljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/McKJ_SG4_hCM) |
| 15 | ...s. - Have a good knowledge of **HTML** and CSS - Have a good eye for... | x |  |  | 4 | [HTML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/K8eq_sEz_eXV) |
| 16 | ... a good knowledge of HTML and **CSS** - Have a good eye for details... | x |  |  | 3 | [CSS, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/g5MZ_uTz_89a) |
| 17 | ...d knowledge of HTML and CSS - **Have** a good eye for details   Abou... |  | x |  | 4 | [HTML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/K8eq_sEz_eXV) |
| 18 | ...S - Have a good eye for detail**s**   About the company To be on ... |  | x |  | 1 | [CSS, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/g5MZ_uTz_89a) |
| 19 | ...refront of digitalisation and **IT** you have to rely on people. T... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 20 | ...ibility, with a comprehensive **personal** development program. All cons... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 21 | ... of turning to each other for **inp**ut - because they cherish dive... |  | x |  | 3 | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| 22 | ...o hear from you!  Work place: **Stockholm**  Start: Immediately taking in... | x |  |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 23 | ... you!  Work place: Stockholm  **Start: Im**mediately taking into account ... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 24 | ...Salary: Basic salary  Extent: **Full-time** (consultant work on-site) | x |  |  | 9 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **36** | **176** | 36/176 = **20%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| x |  |  | [User experience, **skill**](http://data.jobtechdev.se/taxonomy/concept/AdgQ_txR_S3M) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
|  | x |  | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [ramverk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/JD8j_tGG_cJT) |
| x | x | x | [HTML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/K8eq_sEz_eXV) |
| x | x | x | [Node, exekveringsmiljö, **skill**](http://data.jobtechdev.se/taxonomy/concept/McKJ_SG4_hCM) |
| x | x | x | [TypeScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/USj9_RR6_Q1z) |
| x | x | x | [Docker, container-plattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZipR_ckJ_YQC) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
|  | x |  | [agil utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/azwQ_pDo_Dur) |
|  | x |  | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| x | x | x | [CSS, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/g5MZ_uTz_89a) |
| | | **7** | 7/16 = **44%** |