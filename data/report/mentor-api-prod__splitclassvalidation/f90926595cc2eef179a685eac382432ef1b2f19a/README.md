# Results for 'f90926595cc2eef179a685eac382432ef1b2f19a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f90926595cc2eef179a685eac382432ef1b2f19a](README.md) | 1 | 3136 | 22 | 18 | 117/297 = **39%** | 8/15 = **53%** |

## Source text

Scrum master med erfarenhet av testautomation, Stockholm, Västerås Om konsultuppdraget  - Ort: Stockholm, Västerås - Uppdragslängd: Ca. 6 månader - Sista ansökningsdagen: ansök snarast - Omfattning: 100% - OBS! Det är viktigt att du uppfyller skall-kraven för att vi överhuvudtaget ska kunna offerera dig, annars förkastar beställaren vårt anbud omgående där både din tid resp. vår tid går till spillo.   Uppdragsbeskrivning  Kunden söker nu en Scrum Master till sitt team. Teamet ansvarar för ny-, vidareutveckling och förvaltning. Arbetssättet är agilt med SAFE som ramverk. Detta innebär bland annat; självorganisering kring arbetsuppgifter, en testdriven approach och möjligheten att kunna göra omprioriteringar efter förutsättningar.  Vilket även innebär att teammedlemmar tar ett ansvar för många olika aktiviteter och uppgifter under utvecklingens gång, så som; test, scrum, ci/cd medmera.  Arbetsuppgifter:  - Säkra att teamet följer agila principer, processer och metoder. - Facilitera de agila momenten. - Undanröja hinder. - Representera teamet i Scrum of scrums. - Utveckla, såväl front-end som back-end. - Säkerställa att realisationen av kraven fungerar väl med kundens lösningar i så väl befintliga som framtida miljöer. - Aktiv bidra till att teamet som helhet ökar sin förmåga genom at -säkerställa att hen breddar sin egen kompetens genom kompetensöverföring från övriga teammedlemmarnas på samma sätt som hen ser till att överföra av den egna kompetensen till andra teammedlemmar -föreslå och aktiv delta i teamutvecklande aktiviteter.   Obligatoriska kompetenser och erfarenhet (skallkrav):  - 6 års erfarenhet av scrum master-rollen - 6 års erfarenhet av test   Meriterande kompetenser och erfarenhet (börkrav):  - Jira eller liknande planeringsstöd - SAFe - Testautomatisering med tillhörande verktyg - Testledning   ______________________  Hur du kommer vidare  - Sök uppdraget genom denna annons - Lägg in ett CV i word-format - Vi återkopplar om något behöver kompletteras eller förtydligas. - Återkoppling sker vanligtvis från Kunden till oss inom 10 arbetsdagar från det att ansökningstiden utgått. Vi försöker återkoppla omgående till dig som kandidat snarast vi har ny information avseende din ansökan eller uppdraget. Skulle återkoppling dröja, vänligen kontakta oss genom att svara på bekräftande mailutskicket i samband med din ansökan.   På uppmaning från beställare/slutkunder vill de inte att vi lämnar ut information om dem. En annan anledning är att Shaya Solutions lägger stor mängd tid som man på förhand inte får betalt för såvida inte uppdraget tillsätts, och därav kan vara återhållsamma med informationsdelningen av naturliga skäl.  Inför en eventuell intervju meddelas du om vilken Kunden är i god tid.  Om Shaya Solutions  Konsult- och kompetenspartner inom IT, Management och Teknik.  Vi lägger ett stort fokus på kund-/konsultpartnernöjdhet och kvalité i våra leveranser och verkar idag på 13 orter i Sverige med utgångspunkt i Stockholm. Teamets motto är Ödmjukhet, Ihärdighet samt Flexibilitet.  Varmt välkommen att höra av dig vid frågor eller funderingar.  Annonsförsäljare undanbedes.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Scrum master** med erfarenhet av testautomat... | x | x | 12 | 12 | [Scrum master, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1UTZ_bS9_kUD) |
| 2 | ...crum master med erfarenhet av **testautomation**, Stockholm, Västerås Om konsu... | x | x | 14 | 14 | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| 3 | ...erfarenhet av testautomation, **Stockholm**, Västerås Om konsultuppdraget... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 4 | ...av testautomation, Stockholm, **Västerås** Om konsultuppdraget  - Ort: S... | x | x | 8 | 8 | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| 5 | ...s Om konsultuppdraget  - Ort: **Stockholm**, Västerås - Uppdragslängd: Ca... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 6 | ...tuppdraget  - Ort: Stockholm, **Västerås** - Uppdragslängd: Ca. 6 månade... | x | x | 8 | 8 | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| 7 | ...: ansök snarast - Omfattning: **100%** - OBS! Det är viktigt att du ... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 8 | ...skrivning  Kunden söker nu en **Scrum Master** till sitt team. Teamet ansvar... | x | x | 12 | 12 | [Scrum master, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1UTZ_bS9_kUD) |
| 9 | ...reutveckling och förvaltning. **Arbetssättet är agilt** med SAFE som ramverk. Detta i... | x |  |  | 21 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 10 | ...ng. Arbetssättet är agilt med **SAFE** som ramverk. Detta innebär bl... | x |  |  | 4 | [Scaled Agile Framework, SAFe, **skill**](http://data.jobtechdev.se/taxonomy/concept/eTW8_AY5_tpe) |
| 11 | ...cklingens gång, så som; test, **scrum**, ci/cd medmera.  Arbetsuppgif... | x | x | 5 | 5 | [Scrum, systemutvecklingsmetod, **skill**](http://data.jobtechdev.se/taxonomy/concept/Cnqu_REg_6jo) |
| 12 | ...ns gång, så som; test, scrum, **ci**/cd medmera.  Arbetsuppgifter:... | x | x | 2 | 2 | [Continuous integration/CI, **skill**](http://data.jobtechdev.se/taxonomy/concept/xT8A_n4a_Zji) |
| 13 | ... gång, så som; test, scrum, ci**/cd** medmera.  Arbetsuppgifter:  -... |  | x |  | 3 | [Continuous integration/CI, **skill**](http://data.jobtechdev.se/taxonomy/concept/xT8A_n4a_Zji) |
| 14 | ...r:  - Säkra att teamet följer **agila principer, processer och metoder**. - Facilitera de agila moment... | x |  |  | 38 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 15 | ... och metoder. - Facilitera de **agila momenten**. - Undanröja hinder. - Repres... | x |  |  | 14 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 16 | ...nder. - Representera teamet i **Scrum** of scrums. - Utveckla, såväl ... | x | x | 5 | 5 | [Scrum, systemutvecklingsmetod, **skill**](http://data.jobtechdev.se/taxonomy/concept/Cnqu_REg_6jo) |
| 17 | ... - Representera teamet i Scrum** of scrums**. - Utveckla, såväl front-end ... | x |  |  | 10 | [Scrum, systemutvecklingsmetod, **skill**](http://data.jobtechdev.se/taxonomy/concept/Cnqu_REg_6jo) |
| 18 | ... of scrums. - Utveckla, såväl **front-end** som back-end. - Säkerställa a... |  | x |  | 9 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 19 | ...Utveckla, såväl front-end som **back-end**. - Säkerställa att realisatio... |  | x |  | 8 | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| 20 | ...ch erfarenhet (skallkrav):  - **6 års erfarenhet** av scrum master-rollen - 6 år... | x |  |  | 16 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 21 | ...krav):  - 6 års erfarenhet av **scrum** master-rollen - 6 års erfaren... |  | x |  | 5 | [Scrum, systemutvecklingsmetod, **skill**](http://data.jobtechdev.se/taxonomy/concept/Cnqu_REg_6jo) |
| 22 | ...krav):  - 6 års erfarenhet av **scrum master**-rollen - 6 års erfarenhet av ... | x |  |  | 12 | [Scrum master, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1UTZ_bS9_kUD) |
| 23 | ...nhet av scrum master-rollen - **6 års erfarenhet** av test   Meriterande kompete... | x |  |  | 16 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 24 | ... och erfarenhet (börkrav):  - **Jira** eller liknande planeringsstöd... | x | x | 4 | 4 | [JIRA, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQoJ_GY7_n5Z) |
| 25 | ...ler liknande planeringsstöd - **SAFe** - Testautomatisering med till... | x |  |  | 4 | [Scaled Agile Framework, SAFe, **skill**](http://data.jobtechdev.se/taxonomy/concept/eTW8_AY5_tpe) |
| 26 | ...nande planeringsstöd - SAFe - **Testautomatisering** med tillhörande verktyg - Tes... | x | x | 18 | 18 | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| 27 | ...ing med tillhörande verktyg - **Testledning**   ______________________  Hur... | x | x | 11 | 11 | [Testledning, datasystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/dexT_Znd_Yrs) |
| 28 | ...och verkar idag på 13 orter i **Sverige** med utgångspunkt i Stockholm.... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 29 | ... i Sverige med utgångspunkt i **Stockholm**. Teamets motto är Ödmjukhet, ... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| | **Overall** | | | **117** | **297** | 117/297 = **39%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Scrum master, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1UTZ_bS9_kUD) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| x | x | x | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Scrum, systemutvecklingsmetod, **skill**](http://data.jobtechdev.se/taxonomy/concept/Cnqu_REg_6jo) |
|  | x |  | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| x |  |  | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| x | x | x | [JIRA, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQoJ_GY7_n5Z) |
| x | x | x | [Testledning, datasystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/dexT_Znd_Yrs) |
| x |  |  | [Scaled Agile Framework, SAFe, **skill**](http://data.jobtechdev.se/taxonomy/concept/eTW8_AY5_tpe) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Testautomatisering/Test automation, **skill**](http://data.jobtechdev.se/taxonomy/concept/muKv_rHW_7ES) |
| x | x | x | [Continuous integration/CI, **skill**](http://data.jobtechdev.se/taxonomy/concept/xT8A_n4a_Zji) |
| | | **8** | 8/15 = **53%** |