# Results for '5144dc879171e61bb80ff81d15a3f36838ed886e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [5144dc879171e61bb80ff81d15a3f36838ed886e](README.md) | 1 | 681 | 12 | 12 | 90/158 = **57%** | 8/12 = **67%** |

## Source text

Servitör / servitrice Rydbergs ligger centralt i Stockholm city. Rydbergs är en anrik restaurang som serverar lunch med husmankost fram till kl. 15,00 och därefter serverar vi svenskt/italienskt a la carte meny. Vi söker en servitör eller servitris som har tidigare erfarenhet på bordsservering, och är serviceinriktad och stresstålig. Du som söker ska kunna behärska svenska och engelska språket. Dina arbetsuppgifter är att ta emot gäster vid dörren, visa dem till bord, ta beställning på bord, presentera drycker, servera drycker, servera mat, ta hand om kassa, mm Vi söker dig som kan lite mer om mer försäljning och kan presentera menyn till dina gäster på ett proffsigt sätt.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Servitör** / servitrice Rydbergs ligger ... | x | x | 8 | 8 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | Servitör** / servitrice** Rydbergs ligger centralt i St... | x |  |  | 13 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 3 | ...ce Rydbergs ligger centralt i **Stockholm** city. Rydbergs är en anrik re... | x |  |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 4 | ...lm city. Rydbergs är en anrik **restaurang** som serverar lunch med husman... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 5 | ...aurang som serverar lunch med **husmankost** fram till kl. 15,00 och däref... |  | x |  | 10 | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| 6 | ... därefter serverar vi svenskt/**italienskt** a la carte meny. Vi söker en ... | x |  |  | 10 | [Italienskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/T38R_QfY_piB) |
| 7 | ...erverar vi svenskt/italienskt **a la carte** meny. Vi söker en servitör el... | x | x | 10 | 10 | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| 8 | ... a la carte meny. Vi söker en **servitör** eller servitris som har tidig... | x | x | 8 | 8 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 9 | ...y. Vi söker en servitör eller **servitris** som har tidigare erfarenhet p... | x | x | 9 | 9 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 10 | ...om har tidigare erfarenhet på **bordsservering**, och är serviceinriktad och s... | x | x | 14 | 14 | [Bordsservering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5fAq_Squ_o7p) |
| 11 | ...g, och är serviceinriktad och **stresstålig**. Du som söker ska kunna behär... | x | x | 11 | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 12 | ... som söker ska kunna behärska **svenska** och engelska språket. Dina ar... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 13 | ...ka kunna behärska svenska och **engelska** språket. Dina arbetsuppgifter... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 14 | ... på bord, presentera drycker, **servera drycker**, servera mat, ta hand om kass... |  | x |  | 15 | [servera drycker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/F4yK_4xp_zJ5) |
| 15 | ...servera drycker, servera mat, **ta hand om **kassa, mm Vi söker dig som kan... | x |  |  | 11 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 16 | ...cker, servera mat, ta hand om **kassa**, mm Vi söker dig som kan lite... | x | x | 5 | 5 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| | **Overall** | | | **90** | **158** | 90/158 = **57%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Bordsservering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5fAq_Squ_o7p) |
| x |  |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
|  | x |  | [servera drycker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/F4yK_4xp_zJ5) |
|  | x |  | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [Italienskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/T38R_QfY_piB) |
| x | x | x | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x | x | x | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/12 = **67%** |