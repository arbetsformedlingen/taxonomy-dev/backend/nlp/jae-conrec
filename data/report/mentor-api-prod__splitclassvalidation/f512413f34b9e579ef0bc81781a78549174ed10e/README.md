# Results for 'f512413f34b9e579ef0bc81781a78549174ed10e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f512413f34b9e579ef0bc81781a78549174ed10e](README.md) | 1 | 3980 | 17 | 20 | 127/307 = **41%** | 7/19 = **37%** |

## Source text

IT-tekniker Applikationsdrift  Kriminalvården växer – vi bygger nytt, bygger om och bygger till och söker nu nya medarbetare. Vårt viktigaste mål är att minska återfall i brott – att bryta den onda cirkeln. Lyckas vi med det blir samhället tryggare och säkrare för alla. Vi är den myndighet som ansvarar för anstalter, frivård, häkten och klienttransporter.  Vill du vara med och bidra till ett tryggare samhälle? Vår IT-avdelning genomför en stor satsning med fokus på en digital Kriminalvård. Tillsammans jobbar vi för att skapa förutsättningar och lösningar som gör skillnad i vår verksamhet. Under 2022 växer avdelningen med 60-talet nya medarbetare inom flertalet roller inom systemutveckling; IT-infrastruktur, supportfunktioner samt projektledning och IT-säkerhet. Hos oss får du goda möjligheter att växa och kompetensutvecklas efter dina förutsättningar och intresseområden. Vi söker dig som har ett stort IT-intresse, kunskaper som kan berika vår digitaliseringsresa och som vill vara med och göra skillnad i samhället inom just din profession. Läs mer om oss på Krimtech.se  ARBETSUPPGIFTER Som IT Tekniker inom applikationsdrift ingår du i en driftgrupp som ansvarar för att verksamhetens kärn- och stödsystem fungerar för Kriminalvårdens över 13.000 anställda. Du kommer jobba med drift och förvaltning av flera av Kriminalvårdens applikationer.  KVALIFIKATIONER Vi söker dig som arbetar bra med andra men som själv kan strukturera ditt arbetssätt för att driva dina processer vidare. Du arbetar bra med komplexa frågor och löser komplicerade problem. Du är noggrann och lägger stor vikt vid kvalité. I ditt arbete tar du initiativ, sätter igång aktiviteter och uppnår resultat. Du har en god förståelse för hur människan tar till sig kunskap, och anpassar ditt sätt att förmedla ditt budskap utifrån mottagaren. Vidare är du en kunskapsresurs för dina kollegor.   För oss är det viktigt att du delar Kriminalvårdens grundläggande värderingar om en human människosyn, respekt för individen samt tron på människans vilja och förmåga att utvecklas. Du är även medveten om och har förståelse för hur bakgrund, kultur och grupptillhörighet påverkar dig själv och andra. Vidare förväntas du med ditt förhållningssätt bidra till att alla blir likvärdigt bemötta. Vi lägger stor vikt vid personlig lämplighet  Vi söker dig som har:  •	Fullständig gymnasiekompetens eller annan utbildning/erfarenhet som vi bedömer relevant. •	Flerårig erfarenhet av daglig hantering och förvaltning av IT infrastruktur och system i Microsoft-miljöer, inklusive hantering av systemuppgraderingar, patchning etc. •	Förmåga att uttrycka dig väl i tal och skrift på svenska.  Det är meriterande om du har:  •	Certifieringar inom Microsoft •	Erfarenhet av att planera och dokumentera införandet av applikationsplattformar •	Vana av att hantera kundkontakter inom IT-frågor mellan Driftsorganisation och systemanvändare, leverantörer och applikationsägare •	Erfarenhet av automation •	Drift och underhåll av webbapplikationer •	Databaskompetens i form av drift och underhåll av MSSQL. •	Förståelse för nätverk, säkerhet och brandväggar.  ÖVRIGT Vi ställer höga krav på våra medarbetares säkerhets- och sekretessmedvetande. Befattningen är placerad i säkerhetsklass och en säkerhetsprövning med registerkontroll kommer att genomföras innan beslut om anställning fattas. Du behöver vara svensk medborgare, men får gärna ha utländsk bakgrund.  Kriminalvården strävar efter en jämn könsfördelning och ökad mångfald.   För att skicka in din ansökan, klicka på ansökningslänken i annonsen. Frågor om hur du ansöker och lägger in ditt CV ställer du direkt till Visma Recruit 0771- 693 693. Frågor om specifika jobb besvaras av den kontaktperson som anges i annonsen. Vi tar inte emot ansökningar i pappersform eller per e-post.   Vi undanber oss vänligen men bestämt direktkontakt med bemannings- och rekryteringsföretag samt försäljare av rekryteringsannonser. Kriminalvården har upphandlade avtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **IT-tekniker** Applikationsdrift  Kriminalvå... |  | x |  | 11 | [it-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ybqU_Mu2_Eti) |
| 2 | ...T-tekniker Applikationsdrift  **Kriminalvården** växer – vi bygger nytt, bygge... | x | x | 14 | 14 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 3 | ...ll ett tryggare samhälle? Vår **IT**-avdelning genomför en stor sa... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 4 | ...sning med fokus på en digital **Kriminalvård**. Tillsammans jobbar vi för at... |  | x |  | 12 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 4 | ...sning med fokus på en digital **Kriminalvård**. Tillsammans jobbar vi för at... | x |  |  | 12 | [Kriminalvård, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/xVKK_eNQ_jrR) |
| 5 | ...re inom flertalet roller inom **systemutveckling**; IT-infrastruktur, supportfun... | x | x | 16 | 16 | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| 6 | ...roller inom systemutveckling; **IT**-infrastruktur, supportfunktio... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 7 | ...uktur, supportfunktioner samt **projektledning** och IT-säkerhet. Hos oss får ... |  | x |  | 14 | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| 8 | ...ioner samt projektledning och **IT-säkerhet**. Hos oss får du goda möjlighe... | x | x | 11 | 11 | [IT-säkerhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KF7V_H1U_vHv) |
| 9 | ...i söker dig som har ett stort **IT**-intresse, kunskaper som kan b... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 10 | ...ech.se  ARBETSUPPGIFTER Som **IT** Tekniker inom applikationsdri... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 11 | ...ech.se  ARBETSUPPGIFTER Som **IT Tekniker** inom applikationsdrift ingår ... |  | x |  | 11 | [it-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ybqU_Mu2_Eti) |
| 12 | ...r komplicerade problem. Du är **noggrann** och lägger stor vikt vid kval... |  | x |  | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 13 | ...r dig som har:  •	Fullständig **gymnasiekompetens** eller annan utbildning/erfare... |  | x |  | 17 | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| 14 | ... hantering och förvaltning av **IT** infrastruktur och system i Mi... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 15 | ...a dig väl i tal och skrift på **svenska**.  Det är meriterande om du ha... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 16 | ...och dokumentera införandet av **applikationsplattformar** •	Vana av att hantera kundkon... | x | x | 23 | 23 | [Applikationsplattformar, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/TdNz_dnv_jzb) |
| 17 | ...tt hantera kundkontakter inom **IT**-frågor mellan Driftsorganisat... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 18 | ...emanvändare, leverantörer och **applikationsägare** •	Erfarenhet av automation •	... | x | x | 17 | 17 | [Applikationsägare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1bKH_ERy_fJi) |
| 19 | ...ikationsägare •	Erfarenhet av **automation** •	Drift och underhåll av webb... | x |  |  | 10 | [Automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/tDxT_7oT_NyU) |
| 20 | ...tion •	Drift och underhåll av **webbapplikationer** •	Databaskompetens i form av ... |  | x |  | 17 | [Systemutveckling och programmering, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/3Jc3_Ppa_YeJ) |
| 20 | ...tion •	Drift och underhåll av **webbapplikationer** •	Databaskompetens i form av ... |  | x |  | 17 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 20 | ...tion •	Drift och underhåll av **webbapplikationer** •	Databaskompetens i form av ... |  | x |  | 17 | [Webbutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/AfJ4_Pvv_YBa) |
| 20 | ...tion •	Drift och underhåll av **webbapplikationer** •	Databaskompetens i form av ... |  | x |  | 17 | [Webbapplikationsutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ovLk_gz7_FAe) |
| 21 | ...orm av drift och underhåll av **MSSQL**. •	Förståelse för nätverk, sä... |  | x |  | 5 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 22 | ...lse för nätverk, säkerhet och **brandväggar**.  ÖVRIGT Vi ställer höga kra... | x | x | 11 | 11 | [Brandväggar, **skill**](http://data.jobtechdev.se/taxonomy/concept/iXzD_6DF_sL4) |
| 23 | ... gärna ha utländsk bakgrund.  **Kriminalvården** strävar efter en jämn könsför... | x | x | 14 | 14 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 24 | ...jare av rekryteringsannonser. **Kriminalvården** har upphandlade avtal. | x | x | 14 | 14 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| | **Overall** | | | **127** | **307** | 127/307 = **41%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Applikationsägare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1bKH_ERy_fJi) |
|  | x |  | [Systemutveckling och programmering, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/3Jc3_Ppa_YeJ) |
|  | x |  | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
|  | x |  | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
|  | x |  | [Webbutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/AfJ4_Pvv_YBa) |
| x | x | x | [IT-säkerhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KF7V_H1U_vHv) |
|  | x |  | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| x | x | x | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| x | x | x | [Applikationsplattformar, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/TdNz_dnv_jzb) |
|  | x |  | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [systemutveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cm71_uwW_5zU) |
| x | x | x | [Brandväggar, **skill**](http://data.jobtechdev.se/taxonomy/concept/iXzD_6DF_sL4) |
|  | x |  | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [Webbapplikationsutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ovLk_gz7_FAe) |
| x |  |  | [Automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/tDxT_7oT_NyU) |
| x |  |  | [Kriminalvård, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/xVKK_eNQ_jrR) |
|  | x |  | [it-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ybqU_Mu2_Eti) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/19 = **37%** |