# Results for '10df515f5ad33c4b628acbd9e74989fe88677be8'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [10df515f5ad33c4b628acbd9e74989fe88677be8](README.md) | 1 | 3980 | 19 | 46 | 74/334 = **22%** | 5/14 = **36%** |

## Source text

Bussförare sökes till förskolebuss Ref: 20222332   Arbetsuppgifter Som bussförare arbetar du för att skapa en trygg, rolig och lärorik upplevelse med barnen i fokus. Det är en självklarhet att du framför bussen med säkerhet och precision. I lika hög grad är du även vårt ansikte utåt, med ett positivt, lojalt uppträdande - alltid med ambitionen att ge våra barn som alla har olika önskemål med sin resa bästa service och en trevlig och säker färdupplevelse. Vi söker dig som har lätt för att samarbeta med andra kollegor, ansvarstagande, flexibel och brinner för service. Säkerheten är viktig och alltid barnens bästa I fokus.                                         • Ansvara för att framföra fordonet på ett ansvarsfullt och säkert vis för att utföra persontransporter.                 • Ansvara för att aktuella säkerhetsföreskrifter följs och efterlevs i samband med persontransporter.                                                                                                                                               • Medverka i aktiviteter med barnen i dialog med arbetslag som deltar på utflykterna                     • Ansvara för hämtning, lämning och överlämning av fordon i garage enligt gällande riktlinjer.      • Samverka och upprätthålla en professionell relation till barn, medarbetare och andra samarbetspartners                                                                                                                                      • Arbeta självständigt och fatta beslut inom gällande riktlinjer.  Kvalifikationer Vi söker dig som är utbildad bussförare. Har körkort för buss och YKB bevis, samt därmed grundläggande kunskap om lagar och kollektivavtal samt gällande kör- och vilotidsregler.                                                                                                           Positivt är att du även har en barnskötare utbildning eller annan pedagogisk utbildning men inget krav.  Varmt välkommen med din ansökan!  Om arbetsplatsen Vi söker en bussförare som kan framföra en av våra förskolebussar under våran ordinaries föräldraledighet.  Vi har 4 förskole bussar varje chaufförer kör för ett förskole områdena A-D. Syftet med bussen är att den ska bidra till att öka likvärdigheten och den utepedagogiska kvaliteten inom utbildningsområdet. Bussen fungerar som inspiration och drivkraft i det utepedagogiska arbetet vilket företrädesvis är kopplat till t.ex. de projekt som pågår i verksamheten. Bussen fördelas inom området på ett sätt så att den särskilt kompenserar förskolor som har brister i den egna utemiljön, eller där barnen har begränsad tillgång till natur och andra miljöer till följd av socioekonomiska faktorer. Barn i Malmö Stad ska få möjlighet att vistas i annorlunda lärmiljöer. Bokträdgården i Torup fungerar som en utepedagogisk bas för verksamheten. Varje utbildningsområde besöker Bokträdgården en dag i veckan under perioden januari till juni och augusti till december.  Malmö stads verksamheter är organiserade i 14 förvaltningar. Vårt gemensamma uppdrag är att skapa välfärd och hållbar samhällsutveckling. Tillsammans med Malmöborna utvecklar vi en ännu bättre stad att bo, verka och vara i; olikheter, livserfarenheter och talanger gör Malmö till en smart stad i hjärtat av Europa. Världens kunskap finns här.   Läs om Malmö stad som arbetsgivare här malmo.se/jobb   Information om tjänsten Anställningsform: Tidsbegränsad anställning Omfattning: Heltid Antal tjänster: 1 Tillträde: 221003  Övrigt Observera att vi i Malmö stad tagit bort det personliga brevet från ansökningsprocessen och ersatt det med urvalsfrågor, detta för en mer inkluderande rekryteringsprocess och en effektiv kandidatupplevelse för dig som söker jobb hos oss. Dina svar blir en viktig del i vårt urvalsarbete.   Malmö stad strävar efter att medarbetarna ska representera den mångfald som finns i vår stad. Vi välkomnar därför sökande som kan bidra till att vi som arbetsgivare kan uppfylla Malmöbornas behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bussförare** sökes till förskolebuss Ref: ... | x | x | 10 | 10 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 2 | ...0222332   Arbetsuppgifter Som **bussförare** arbetar du för att skapa en t... | x | x | 10 | 10 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 3 | ...samarbeta med andra kollegor, **ansvarstagande**, flexibel och brinner för ser... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 4 | ...och säkert vis för att utföra **persontransporter**.                 • Ansvara fö... |  | x |  | 17 | [Personbilsförare, persontransporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2dp3_GhB_yQS) |
| 4 | ...och säkert vis för att utföra **persontransporter**.                 • Ansvara fö... | x | x | 17 | 17 | [Persontransporter, **skill**](http://data.jobtechdev.se/taxonomy/concept/zBWb_UEF_vHK) |
| 5 | ...s och efterlevs i samband med **persontransporter**.                             ... |  | x |  | 17 | [Personbilsförare, persontransporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2dp3_GhB_yQS) |
| 5 | ...s och efterlevs i samband med **persontransporter**.                             ... | x | x | 17 | 17 | [Persontransporter, **skill**](http://data.jobtechdev.se/taxonomy/concept/zBWb_UEF_vHK) |
| 6 | ...ng av fordon i garage enligt g**äl**lande riktlinjer.      • Samve... |  | x |  | 2 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 7 | ...v fordon i garage enligt gälla**nd**e riktlinjer.      • Samverka ... |  | x |  | 2 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 8 | ...don i garage enligt gällande r**i**ktlinjer.      • Samverka och ... |  | x |  | 1 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 9 | ...nell relation till barn, medar**betare och** andra samarbetspartners      ... |  | x |  | 10 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 10 | ...ll relation till barn, medarbe**t**are och andra samarbetspartner... |  | x |  | 1 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 11 | ...till barn, medarbetare och and**r**a samarbetspartners           ... |  | x |  | 1 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 12 | ...ill barn, medarbetare och andr**a s**amarbetspartners              ... |  | x |  | 3 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 13 | ...rn, medarbetare och andra sama**rbets**partners                      ... |  | x |  | 5 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 14 | ...                            • **Arbeta självständigt** och fatta beslut inom gälland... | x |  |  | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ...                        • Arbe**ta** självständigt och fatta beslu... |  | x |  | 2 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 16 | ...                  • Arbeta sjä**l**vständigt och fatta beslut ino... |  | x |  | 1 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 17 | ...         • Arbeta självständig**t** och fatta beslut inom gälland... |  | x |  | 1 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 18 | ...       • Arbeta självständigt **o**ch fatta beslut inom gällande ... |  | x |  | 1 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 19 | ...lvständigt och fatta beslut in**o**m gällande riktlinjer.  Kvalif... |  | x |  | 1 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 20 | ...ndigt och fatta beslut inom gä**ll**ande riktlinjer.  Kvalifikatio... |  | x |  | 2 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 21 | ... och fatta beslut inom gälland**e** riktlinjer.  Kvalifikationer ... |  | x |  | 1 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 22 | ... fatta beslut inom gällande ri**kt**linjer.  Kvalifikationer Vi sö... |  | x |  | 2 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 23 | ...tta beslut inom gällande riktl**i**njer.  Kvalifikationer Vi söke... |  | x |  | 1 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 24 | ...t inom gällande riktlinjer.  K**va**lifikationer Vi söker dig som ... |  | x |  | 2 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 25 | ...gällande riktlinjer.  Kvalifik**at**ioner Vi söker dig som är utbi... |  | x |  | 2 | [Barnskötare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rM7G_ge7_XhP) |
| 26 | ...ande riktlinjer.  Kvalifikatio**n**er Vi söker dig som är utbilda... |  | x |  | 1 | [Barnskötare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rM7G_ge7_XhP) |
| 27 | ...de riktlinjer.  Kvalifikatione**r** Vi söker dig som är utbildad ... |  | x |  | 1 | [Barnskötare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rM7G_ge7_XhP) |
| 28 | ...ktlinjer.  Kvalifikationer Vi **söke**r dig som är utbildad bussföra... |  | x |  | 4 | [Barnskötare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rM7G_ge7_XhP) |
| 29 | ... Vi söker dig som är utbildad **bussförare**. Har körkort för buss och YKB... | x |  |  | 10 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 30 | ...m är utbildad bussförare. Har **körkort för buss** och YKB bevis, samt därmed gr... | x |  |  | 16 | [D, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/hK1a_wsQ_4UG) |
| 31 | ...are. Har körkort för buss och **YKB** bevis, samt därmed grundlägga... | x |  |  | 3 | [Yrkeskompetensbevis YKB, buss, **skill**](http://data.jobtechdev.se/taxonomy/concept/L31X_f88_viY) |
| 32 | ...s, samt därmed grundläggande k**u**nskap om lagar och kollektivav... |  | x |  | 1 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 33 | ... samt därmed grundläggande kun**s**kap om lagar och kollektivavta... |  | x |  | 1 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 34 | ...läggande kunskap om lagar och **kollektivavtal** samt gällande kör- och viloti... | x |  |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 35 | ...r och kollektivavtal samt gäll**a**nde kör- och vilotidsregler.  ... |  | x |  | 1 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 36 | ...ch kollektivavtal samt gälland**e** kör- och vilotidsregler.     ... |  | x |  | 1 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 37 | ...kollektivavtal samt gällande k**ör**- och vilotidsregler.         ... |  | x |  | 2 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 38 | ...ositivt är att du även har en **barnskötare** utbildning eller annan pedago... | x |  |  | 11 | [barnskötare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xn6g_aCe_G1o) |
| 39 | ... Om arbetsplatsen Vi söker en **bussförare** som kan framföra en av våra f... | x |  |  | 10 | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
| 40 | ...olor som har brister i den egn**a** utemiljön, eller där barnen h... |  | x |  | 1 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 41 | ...som har brister i den egna ute**m**iljön, eller där barnen har be... |  | x |  | 1 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 42 | ...m har brister i den egna utemi**l**jön, eller där barnen har begr... |  | x |  | 1 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 43 | ...har brister i den egna utemilj**ö**n, eller där barnen har begrän... |  | x |  | 1 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 44 | ...ioekonomiska faktorer. Barn i **Malmö Stad** ska få möjlighet att vistas i... | x |  |  | 10 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 45 | ...jlighet att vistas i annorlund**a** lärmiljöer. Bokträdgården i T... |  | x |  | 1 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 46 | ...ighet att vistas i annorlunda **l**ärmiljöer. Bokträdgården i Tor... |  | x |  | 1 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 47 | ...et att vistas i annorlunda lär**m**iljöer. Bokträdgården i Torup ... |  | x |  | 1 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 48 | ...tt vistas i annorlunda lärmilj**ö**er. Bokträdgården i Torup fung... |  | x |  | 1 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 49 | ...i och augusti till december.  **Malmö** stads verksamheter är organis... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 50 | ... augusti till december.  Malmö** stads** verksamheter är organiserade ... | x |  |  | 6 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 51 | ...erfarenheter och talanger gör **Malmö** till en smart stad i hjärtat ... | x |  |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 52 | ...s kunskap finns här.   Läs om **Malmö** stad som arbetsgivare här mal... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 53 | ...skap finns här.   Läs om Malmö** stad** som arbetsgivare här malmo.se... | x |  |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 54 | ...om tjänsten Anställningsform: **Tidsbegränsad anställning** Omfattning: Heltid Antal tjän... | x |  |  | 25 | [Tidsbegränsad anställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/sTu5_NBQ_udq) |
| 55 | ...änsad anställning Omfattning: **Heltid** Antal tjänster: 1 Tillträde: ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 56 | ...03  Övrigt Observera att vi i **Malmö** stad tagit bort det personlig... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 57 | ...vrigt Observera att vi i Malmö** stad** tagit bort det personliga bre... | x |  |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 58 | ...ig del i vårt urvalsarbete.   **Malmö** stad strävar efter att medarb... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 59 | ...l i vårt urvalsarbete.   Malmö** stad** strävar efter att medarbetarn... | x |  |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| | **Overall** | | | **74** | **334** | 74/334 = **22%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Personbilsförare, persontransporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2dp3_GhB_yQS) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Yrkeskompetensbevis YKB, buss, **skill**](http://data.jobtechdev.se/taxonomy/concept/L31X_f88_viY) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [D, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/hK1a_wsQ_4UG) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x | x | x | [Bussförare/Busschaufför, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/qSXj_aXc_EGp) |
|  | x |  | [Barnskötare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rM7G_ge7_XhP) |
| x |  |  | [Tidsbegränsad anställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/sTu5_NBQ_udq) |
| x |  |  | [barnskötare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xn6g_aCe_G1o) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Persontransporter, **skill**](http://data.jobtechdev.se/taxonomy/concept/zBWb_UEF_vHK) |
| | | **5** | 5/14 = **36%** |