# Results for '93d36bcc8319fe152dca10adffcbc97b68aeb52b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [93d36bcc8319fe152dca10adffcbc97b68aeb52b](README.md) | 1 | 420 | 4 | 2 | 21/37 = **57%** | 1/3 = **33%** |

## Source text

Hästkötare Vallentuna Ridskola har ca 50 hästar och fyra anställda som jobbar i stall. Vi söker en hästskötare på heltid. I tjänsten ingår Mockning av stall. In och ut släpp av hästar, Visitering av häst , Iordningställa häst för ridning. Ridning ingår inte i tjänsten. Rökning är inte tillåtet på arbetsplatsen. Boende kan ej ordnas på anläggningen Arbetstider 7-16 Månad-Fredag Ingen helgtjänst Lunch rast mellan 12-13

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Hästkötare** Vallentuna Ridskola har ca 50... | x | x | 10 | 10 | [Djurskötare, häst, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JMd5_2p6_zQt) |
| 2 | Hästkötare **Vallentuna** Ridskola har ca 50 hästar och... | x |  |  | 10 | [Vallentuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/K4az_Bm6_hRV) |
| 3 | ...m jobbar i stall. Vi söker en **hästskötare** på heltid. I tjänsten ingår M... | x | x | 11 | 11 | [Djurskötare, häst, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JMd5_2p6_zQt) |
| 4 | ...l. Vi söker en hästskötare på **heltid**. I tjänsten ingår Mockning av... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **21** | **37** | 21/37 = **57%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Djurskötare, häst, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JMd5_2p6_zQt) |
| x |  |  | [Vallentuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/K4az_Bm6_hRV) |
| | | **1** | 1/3 = **33%** |