# Results for 'fcc3dd33f46ef884e0a1dfe4f2af36b55d9ab9b6'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [fcc3dd33f46ef884e0a1dfe4f2af36b55d9ab9b6](README.md) | 1 | 502 | 6 | 6 | 27/69 = **39%** | 4/7 = **57%** |

## Source text

Herrfrisör/Barberare Vi söker efter dig som verkligen brinner för ditt hantverk. Vi har byggt upp en trogen kundkrets och välkomnar varje dag nya som gamla kunder. Din service känsla är viktig för oss men viktigast av allt att du kan ta till dig vår anda och leverera klippningar och skäggvård med högklass! Våra kunder är både svenska och internationella så du behöver behärska svenska eller engelska.  Om du tror att du är den vi söker så är du välkommen att boka in ett möte, skicka ett CV till oss.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Herrfrisör**/Barberare Vi söker efter dig ... | x | x | 10 | 10 | [Herrfrisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/84wd_wvd_zH8) |
| 2 | Herrfrisör/**Barberare** Vi söker efter dig som verkli... | x | x | 9 | 9 | [Barberare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/hDzC_BF5_Wz2) |
| 3 | ...om verkligen brinner för ditt **hantverk**. Vi har byggt upp en trogen k... |  | x |  | 8 | [Hantverk, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/PaxQ_o1G_wWH) |
| 4 | ...ill dig vår anda och leverera **klippningar** och skäggvård med högklass! V... | x |  |  | 11 | [hårklippningstekniker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ctoB_kAr_kEo) |
| 5 | ... och leverera klippningar och **skäggvård** med högklass! Våra kunder är ... | x |  |  | 9 | [behandla skägg och mustascher, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/TD6B_28Q_wDn) |
| 6 | ...högklass! Våra kunder är både **svenska** och internationella så du beh... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 7 | ...onella så du behöver behärska **svenska** eller engelska.  Om du tror a... | x |  |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 8 | ...ehöver behärska svenska eller **engelska**.  Om du tror att du är den vi... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| | **Overall** | | | **27** | **69** | 27/69 = **39%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Herrfrisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/84wd_wvd_zH8) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Hantverk, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/PaxQ_o1G_wWH) |
| x |  |  | [behandla skägg och mustascher, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/TD6B_28Q_wDn) |
| x |  |  | [hårklippningstekniker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ctoB_kAr_kEo) |
| x | x | x | [Barberare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/hDzC_BF5_Wz2) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/7 = **57%** |