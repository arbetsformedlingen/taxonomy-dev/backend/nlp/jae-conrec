# Results for '677128d0054f17538edc14fa74fe8db7b13a58ff'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [677128d0054f17538edc14fa74fe8db7b13a58ff](README.md) | 1 | 1870 | 4 | 1 | 7/71 = **10%** | 1/4 = **25%** |

## Source text

75% dagtjänst hos 26årig tjej i Åkersberga Hej!   Jag är en tjej som bor med min mamma och pappa i en villa i Åkersberga, fint beläget nära vattnet. Har även en katt som jag älskar väldigt mycket.   Nu söker jag dig som är lugn, lyhörd och har en förmåga att på sikt kunna känna av mina signaler då jag inte kommunicerar i tal.   Jag behöver dig som kan arbeta 75-100%  under dagtid måndag-fredag. Kan tillkomma kvällar och helger vid behov.   2 ggr/vecka åker jag på dagligverksamhet, du deltar då som min assistent. På sikt ska jag även börja på ridning så ett krav är att du inte är rädd för hästar och ej har pälsallergi.  Det här jobbet är långsiktigt från min sida och jag vill att du ska känna samma sak då jag behöver en stabil assistent som jag kan lita på. Om du har erfarenhet av yrken med ungdomar med speciella behov är det meriterande men inget krav. Viktigast är att du har rätt inställning till att vi ska förstå och fungera med varandara.    Du får veta mer om mig vid en personlig intervju.  Jag ser fram emot att lära känna dig!!    Vi kontaktar de personer som kan vara aktuella för en personlig intervju och intervjuer kommer att ske löpande med anställning omgående. God assistans erbjuder generöst friskvårdsbidrag och är kollektivt anslutna till KFS.   Vi önskar ett utdrag från belastningsregistret vad anställning, beställ detta via länken nedan: Belastningsregistret - begär utdrag \| Polismyndigheten (polisen.se)   God Assistans bedriver personlig assistans för kunder över hela Sverige. Vi är ett företag med stort hjärta och hög kompetens som brinner för det vi gör och har lång erfarenhet inom assistans. Att värna om våra medarbetare och förenkla tillvaron för våra kunder är kärnan i vår verksamhet som kännetecknas av personligt bemötande, hög tillgänglighet och snabb service. Läs gärna mer om oss på vår hemsida: www.godassistans.nu

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **75%** dagtjänst hos 26årig tjej i Å... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 2 | ... sak då jag behöver en stabil **assistent** som jag kan lita på. Om du ha... | x |  |  | 9 | [Personlig assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eU1q_zvL_9Rf) |
| 3 | ...om jag kan lita på. Om du har **erfarenhet av yrken med ungdomar med speciella behov** är det meriterande men inget ... | x |  |  | 52 | [Barn med särskilda behov, **skill**](http://data.jobtechdev.se/taxonomy/concept/CGBF_fgA_qnj) |
| 4 | ...ssistans för kunder över hela **Sverige**. Vi är ett företag med stort ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **7** | **71** | 7/71 = **10%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x |  |  | [Barn med särskilda behov, **skill**](http://data.jobtechdev.se/taxonomy/concept/CGBF_fgA_qnj) |
| x |  |  | [Personlig assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eU1q_zvL_9Rf) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | | **1** | 1/4 = **25%** |