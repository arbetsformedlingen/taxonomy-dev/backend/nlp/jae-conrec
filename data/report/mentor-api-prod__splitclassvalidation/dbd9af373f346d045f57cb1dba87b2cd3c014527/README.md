# Results for 'dbd9af373f346d045f57cb1dba87b2cd3c014527'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [dbd9af373f346d045f57cb1dba87b2cd3c014527](README.md) | 1 | 244 | 4 | 4 | 28/32 = **88%** | 2/2 = **100%** |

## Source text

Kock Du ska vara utbildad kock eller ha dokumenterad erfarenhet av arbete som kock i minst 1 år. Du ska klara av hög arbetsbelastning då det stundtals är högt tempo. Du ska ha förmåga att arbeta självständigt utifrån den meny vi har tagit fram.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kock** Du ska vara utbildad kock ell... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 2 | Kock Du ska vara utbildad **kock** eller ha dokumenterad erfaren... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 3 | ...erad erfarenhet av arbete som **kock** i minst 1 år. Du ska klara av... | x |  |  | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 4 | ... tempo. Du ska ha förmåga att **arbeta självständigt** utifrån den meny vi har tagit... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| | **Overall** | | | **28** | **32** | 28/32 = **88%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| | | **2** | 2/2 = **100%** |