# Results for '417dc0e78bda0039bb1b323e44f71f62830130d4'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [417dc0e78bda0039bb1b323e44f71f62830130d4](README.md) | 1 | 2147 | 13 | 15 | 68/174 = **39%** | 7/18 = **39%** |

## Source text

Vi söker Dig som vill arbeta extra inom industri och lager! Är du en glad pensionär, student eller egenföretagare som känner att det vore roligt att komma ut och extrajobba när det passar dig?  Som timanställd på Uniflex får du möjlighet till uppdrag hos olika kunder med varierande typer av arbetsuppgifter som t e x maskinövervakning, plock/pack och sortering/avsyning och truckkörning.  Våra kunder finns främst i Växjö och Alvesta men även i närliggande kommuner.  Arbetstider och timlön varierar mellan uppdragen.  Vi söker dig som är öppen för olika arbetstider som passar din vardag.  Vi ser gärna att du har körkort och tillgång till bil för att enklare ta dig till uppdragen.   Vem är du?  Som person är du  - Flexibel - Lyhörd  - Noggrann - Glad - En go kollega  Ta chansen att tjäna en extra kosing samtidigt som du får socialisera med våra trevliga konsulter och kunder! För att söka tjänsten behöver du ha någon annan form utan sysselsättning (pension/jobb/studier) på minst 51%. Är du student och söker, vänligen bifoga aktuellt studieintyg i din ansökan.      Du skickar din ansökan via vår hemsida www.uniflex.se  Se till att eventuella referenser som finns namngivna i dina ansökningshandlingar har kännedom om att de finns med där och att de eventuellt kommer att bli kontaktade vid en eventuell rekryteringsprocess.  Uniflex är ett auktoriserat bemanningsföretag som arbetar med uthyrning och rekrytering. Uniflex finns i Sverige, Norge och Finland. I Sverige är vi drygt 3 000 anställda och finns på ett 40 tal orter. Vi är medlemmar i Almega Kompetensföretagen samt Byggföretagen och har kollektivavtal med LO, Byggnads, Unionen och Akademikerförbunden. Uniflex är kvalitets-, miljö- och arbetsmiljöcertifierade enligt ISO 9001, ISO 14001 samt ISO 45001.  På Uniflex ser vi våra medarbetare som vår viktigaste tillgång och därför erbjuder vi följande:  • Kollektivavtal • Tjänstepension • Friskvårdsbidrag och andra personalförmåner • Försäkringar • Marknadsmässig lön • Karriär och utveckling  Vi söker ständigt efter nya kollegor som arbetar i enighet med våra värderingar affärsfokus, engagemang, glädje och ansvarstagande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...rbeta extra inom industri och **lager**! Är du en glad pensionär, stu... | x | x | 5 | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 2 | ...obba när det passar dig?  Som **timanställd** på Uniflex får du möjlighet t... | x |  |  | 11 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 3 | ... av arbetsuppgifter som t e x **maskinövervakning**, plock/pack och sortering/avs... |  | x |  | 17 | [övervaka maskinaktivitet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/57eS_U77_gX8) |
| 4 | ... som t e x maskinövervakning, **plock**/pack och sortering/avsyning o... | x |  |  | 5 | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
| 5 | ... e x maskinövervakning, plock/**pack** och sortering/avsyning och tr... | x |  |  | 4 | [packare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/RH1P_4KB_1WT) |
| 6 | ...inövervakning, plock/pack och **sortering**/avsyning och truckkörning.  V... |  | x |  | 9 | [sortering av fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/CKTg_SGv_vEB) |
| 6 | ...inövervakning, plock/pack och **sortering**/avsyning och truckkörning.  V... |  | x |  | 9 | [traditioner för sortering av djurdelar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DrvX_LVa_Qzf) |
| 6 | ...inövervakning, plock/pack och **sortering**/avsyning och truckkörning.  V... |  | x |  | 9 | [sköta sortering och uppläggning av virke, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/djQw_ZkM_xEH) |
| 7 | ...ing, plock/pack och sortering/**avsyning** och truckkörning.  Våra kunde... | x | x | 8 | 8 | [Avsyning, **skill**](http://data.jobtechdev.se/taxonomy/concept/X4EU_ZC7_QmK) |
| 8 | ...ck och sortering/avsyning och **truck**körning.  Våra kunder finns fr... | x |  |  | 5 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 9 | ....  Våra kunder finns främst i **Växjö** och Alvesta men även i närlig... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 10 | ...nder finns främst i Växjö och **Alvesta** men även i närliggande kommun... | x | x | 7 | 7 | [Alvesta, **municipality**](http://data.jobtechdev.se/taxonomy/concept/MMph_wmN_esc) |
| 11 | ...dag.  Vi ser gärna att du har **körkort** och tillgång till bil för att... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 12 | ...är du  - Flexibel - Lyhörd  - **Noggrann** - Glad - En go kollega  Ta ch... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 13 | ... rekrytering. Uniflex finns i **Sverige**, Norge och Finland. I Sverige... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 14 | ...Sverige, Norge och Finland. I **Sverige** är vi drygt 3 000 anställda o... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 15 | ...en samt Byggföretagen och har **kollektivavtal** med LO, Byggnads, Unionen och... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 16 | ...certifierade enligt ISO 9001, **ISO 14001** samt ISO 45001.  På Uniflex s... | x |  |  | 9 | [Miljöledning (ISO 14000-serien), **skill**](http://data.jobtechdev.se/taxonomy/concept/3Sj1_uuL_y1A) |
| 17 | ...rför erbjuder vi följande:  • **Kollektivavtal** • Tjänstepension • Friskvårds... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 18 | ...fokus, engagemang, glädje och **ansvarstagande**. |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| | **Overall** | | | **68** | **174** | 68/174 = **39%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Miljöledning (ISO 14000-serien), **skill**](http://data.jobtechdev.se/taxonomy/concept/3Sj1_uuL_y1A) |
|  | x |  | [övervaka maskinaktivitet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/57eS_U77_gX8) |
|  | x |  | [sortering av fisk, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/CKTg_SGv_vEB) |
|  | x |  | [traditioner för sortering av djurdelar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DrvX_LVa_Qzf) |
| x | x | x | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x | x | x | [Alvesta, **municipality**](http://data.jobtechdev.se/taxonomy/concept/MMph_wmN_esc) |
| x |  |  | [packare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/RH1P_4KB_1WT) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x | x | x | [Avsyning, **skill**](http://data.jobtechdev.se/taxonomy/concept/X4EU_ZC7_QmK) |
| x |  |  | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [sköta sortering och uppläggning av virke, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/djQw_ZkM_xEH) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| x | x | x | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **7** | 7/18 = **39%** |