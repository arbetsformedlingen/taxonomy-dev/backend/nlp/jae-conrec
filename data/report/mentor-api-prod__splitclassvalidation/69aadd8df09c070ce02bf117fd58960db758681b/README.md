# Results for '69aadd8df09c070ce02bf117fd58960db758681b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [69aadd8df09c070ce02bf117fd58960db758681b](README.md) | 1 | 3502 | 25 | 18 | 85/298 = **29%** | 10/26 = **38%** |

## Source text

Trafik- och gatudesigner Jobbeskrivning Ramboll har lanserat en ny strategi för 2022-2025 - “The Partner for Sustainable Change” - och söker nu dig som vill vara med på vår resa och arbeta för en mer hållbar framtid. Vi erbjuder dig möjligheten att använda tekniska kunskap i kombination med ditt engagemang för innovation och hållbarhet för att arbeta med den trafikteknisk utformning för att kunna skapa en mer hållbar och jämställd infrastruktur. För att trivas och lyckas i den här rollen tror vi att du har arbetat i både stora och små uppdrag inom trafikdesign och stadsutveckling där du brinner för att skapa affärsmässiga, innovativa och hållbara lösningar. Du kommer att tillhöra affärsområdet Smart Mobility och någon av enheterna antingen i Stockholm, Göteborg, Malmö, Linköping, Norrköping eller Luleå beroende på var du bor. Som vår nya Trafik- och gatudesigner kommer du att vara en del av vår förändringsresa för att tillgodose våra kunders efterfrågan på mer hållbara och innovativa lösningar för att kunna bygga framtidens samhälle. Dina huvudsakliga arbetsuppgifter: Ansvara för utformningsfrågor och trafikdesign i uppdragen samt bidra till att leda utvecklingen inom Smart Mobility Utredare eller uppdragsledare för trafikutredningar och teknikansvarig för trafik- och gatudesign i större uppdrag Delta i marknadsbearbetningen, både genom proaktiv kontakt med våra kunder och genom anbudsarbete Samarbeta med kollegor, nationellt och internationellt  Utvecklas tillsammans med oss När du blir en del av Ramboll kommer vi att göra vårt bästa för att stötta dig i din personliga och professionella utveckling. För att lyckas i den här rollen tror vi att: Du har gedigen erfarenhet av att arbeta med trafiktekniska utredningar, utformningsfrågor och trafikdesign. Du kan också ha lång erfarenhet av gatuprojektering, men vill utvecklas mer i tidiga utredningsskeden   Du är duktig på VGU, teknisk handbok och livsrumsmodellen Du har ett stort intresse för hur mobilitet och stadsutveckling utvecklas tillsammans och ett stort engagemang för hållbarhet och en hållbar samhällsutveckling Du har mycket goda kunskaper i CAD och gärna kunskaper i Sketchup/Revit, Space syntax eller liknande program Du har en relevant ingenjörsutbildning Du har erfarenhet av att arbeta i stora komplexa projekt där du gärna haft en ansvarsroll Du har förmåga och intresse av samarbete över både tekniska och geografiska gränser Du har god kommunikativ förmåga i tal och skrift på svenska och engelska  Du är en person vars främsta drivkraft är att uppnå resultat i projekt som adderar reellt värde. Tack vare din strategiska infallsvinkel och samarbetsförmåga lyckas du möta våra kunders krav och leverera lösningar anpassade till morgondagens samhälle. Du är flexibel och trivs när du utmanas i intressanta projekt som inbegriper såväl lokalt som internationellt samarbete. För att du ska växa och utvecklas erbjuder vi ett ledarskap som stöder, coachar och inkluderar dig i strategiska beslut. Ramboll i Sverige Ramboll är ett av Sveriges största teknik-, design- och konsultföretag med fler än 1800 experter på 30 kontor över hela landet. Hos oss kan du bidra med innovativa och hållbara lösningar till nytta för människa, natur och samhälle. Låt oss veta mer om dig Vi tar tacksam emot ditt CV eller skriv några rader om dig själv som ett första steg och ser därefter fram emot att få veta mer om dig om du har den profilen vi söker. Vi ser fram emot att höra från dig! Sista ansökningsdag: 2022-08-15

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...någon av enheterna antingen i **Stockholm**, Göteborg, Malmö, Linköping, ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 2 | ...heterna antingen i Stockholm, **Göteborg**, Malmö, Linköping, Norrköping... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 3 | ...tingen i Stockholm, Göteborg, **Malmö**, Linköping, Norrköping eller ... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 4 | ...i Stockholm, Göteborg, Malmö, **Linköping**, Norrköping eller Luleå beroe... | x | x | 9 | 9 | [Linköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bm2x_1mr_Qhx) |
| 5 | ..., Göteborg, Malmö, Linköping, **Norrköping** eller Luleå beroende på var d... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 6 | ..., Linköping, Norrköping eller **Luleå** beroende på var du bor. Som v... | x | x | 5 | 5 | [Luleå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CXbY_gui_14v) |
| 7 | ...ecklingen inom Smart Mobility **Utredare** eller uppdragsledare för traf... |  | x |  | 8 | [Handläggare, offentlig förvaltning/Utredare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YG1s_tUg_jWJ) |
| 8 | ...kunder och genom anbudsarbete **Samarbeta med kollegor**, nationellt och internationel... | x |  |  | 22 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 9 | ... erfarenhet av att arbeta med **trafiktekniska utredningar**, utformningsfrågor och trafik... | x |  |  | 26 | [Trafikteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/7sFk_ixH_faW) |
| 10 | ...n också ha lång erfarenhet av **gatuprojektering**, men vill utvecklas mer i tid... |  | x |  | 16 | [Gatu- och vägprojektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/6XcQ_ugX_h5E) |
| 10 | ...n också ha lång erfarenhet av **gatuprojektering**, men vill utvecklas mer i tid... | x |  |  | 16 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 11 | ...skeden   Du är duktig på VGU, **teknisk handbok** och livsrumsmodellen Du har e... | x |  |  | 15 | [Teknisk dokumentation, **skill**](http://data.jobtechdev.se/taxonomy/concept/AEgv_4gP_ndv) |
| 12 | ...u har mycket goda kunskaper i **CAD** och gärna kunskaper i Sketchu... | x |  |  | 3 | [CAD-konstruktion/CAD-ritning, **skill**](http://data.jobtechdev.se/taxonomy/concept/oaiv_GsB_AFK) |
| 13 | ...r i CAD och gärna kunskaper i **Sketchup**/Revit, Space syntax eller lik... | x |  |  | 8 | [Arkitekt-SketchUp 3D, **skill**](http://data.jobtechdev.se/taxonomy/concept/g7bY_vJ5_XC8) |
| 13 | ...r i CAD och gärna kunskaper i **Sketchup**/Revit, Space syntax eller lik... |  | x |  | 8 | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| 14 | ...ch gärna kunskaper i Sketchup/**Revit**, Space syntax eller liknande ... |  | x |  | 5 | [Arkitekt-AutoCAD Revit Architecture Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6MJf_3Va_KQD) |
| 14 | ...ch gärna kunskaper i Sketchup/**Revit**, Space syntax eller liknande ... |  | x |  | 5 | [VVS, el, tele-AutoCAD Revit MEP Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6ZKB_QVh_xPs) |
| 14 | ...ch gärna kunskaper i Sketchup/**Revit**, Space syntax eller liknande ... | x | x | 5 | 5 | [Byggkonstruktion-Autodesk Revit Structure, **skill**](http://data.jobtechdev.se/taxonomy/concept/MsNS_1ea_u4p) |
| 14 | ...ch gärna kunskaper i Sketchup/**Revit**, Space syntax eller liknande ... |  | x |  | 5 | [Byggkonstruktion-AutoCAD Revit Structure Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/bgjg_cQD_QgM) |
| 14 | ...ch gärna kunskaper i Sketchup/**Revit**, Space syntax eller liknande ... |  | x |  | 5 | [Arkitekt-Autodesk Revit Architecture, **skill**](http://data.jobtechdev.se/taxonomy/concept/wZg2_hFD_TPv) |
| 15 | ...de program Du har en relevant **ingenjörsutbildning** Du har erfarenhet av att arbe... | x | x | 19 | 19 | [Ingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/SNvJ_i6V_DZH) |
| 16 | ...ning Du har erfarenhet av att **arbeta i stora komplexa projekt** där du gärna haft en ansvarsr... | x |  |  | 31 | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| 17 | ...u har förmåga och intresse av **samarbete** över både tekniska och geogra... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 18 | ...ch geografiska gränser Du har **god kommunikativ förmåga** i tal och skrift på svenska o... | x |  |  | 24 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 19 | ...v förmåga i tal och skrift på **svenska** och engelska  Du är en person... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 20 | ...tal och skrift på svenska och **engelska**  Du är en person vars främsta... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 21 | ...strategiska beslut. Ramboll i **Sverige** Ramboll är ett av Sveriges st... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **85** | **298** | 85/298 = **29%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
|  | x |  | [Arkitekt-AutoCAD Revit Architecture Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6MJf_3Va_KQD) |
|  | x |  | [Gatu- och vägprojektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/6XcQ_ugX_h5E) |
|  | x |  | [VVS, el, tele-AutoCAD Revit MEP Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6ZKB_QVh_xPs) |
| x |  |  | [Trafikteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/7sFk_ixH_faW) |
| x |  |  | [Teknisk dokumentation, **skill**](http://data.jobtechdev.se/taxonomy/concept/AEgv_4gP_ndv) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Luleå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CXbY_gui_14v) |
| x | x | x | [Byggkonstruktion-Autodesk Revit Structure, **skill**](http://data.jobtechdev.se/taxonomy/concept/MsNS_1ea_u4p) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Ingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/SNvJ_i6V_DZH) |
| x | x | x | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
|  | x |  | [Handläggare, offentlig förvaltning/Utredare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YG1s_tUg_jWJ) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
|  | x |  | [Byggkonstruktion-AutoCAD Revit Structure Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/bgjg_cQD_QgM) |
| x | x | x | [Linköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bm2x_1mr_Qhx) |
| x |  |  | [Arkitekt-SketchUp 3D, **skill**](http://data.jobtechdev.se/taxonomy/concept/g7bY_vJ5_XC8) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x |  |  | [CAD-konstruktion/CAD-ritning, **skill**](http://data.jobtechdev.se/taxonomy/concept/oaiv_GsB_AFK) |
|  | x |  | [Arkitekt-Autodesk Revit Architecture, **skill**](http://data.jobtechdev.se/taxonomy/concept/wZg2_hFD_TPv) |
|  | x |  | [3D-modellering, **skill**](http://data.jobtechdev.se/taxonomy/concept/yQcc_fTn_JL5) |
| x |  |  | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **10** | 10/26 = **38%** |