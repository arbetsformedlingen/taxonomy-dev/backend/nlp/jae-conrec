# Results for '08379d9f4e803cb26eb3563b7374867aa41db88c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [08379d9f4e803cb26eb3563b7374867aa41db88c](README.md) | 1 | 3522 | 31 | 43 | 158/650 = **24%** | 14/37 = **38%** |

## Source text

IT-specialister/Systemtekniker till Enheten Teknikförvaltning Vårdsystem Verksamhetsområde Medicinsk teknik (MT) ger stöd i inköp, förvaltning, drift och underhåll av medicinteknisk utrustning och vårdsystem inom hälso- och sjukvården.  Vi arbetar i en komplex IT-miljö för att stödja tekniskt avancerade verksamheter och levererar IT-lösningar som skapar nytta och god vård för regionens invånare.    Om du har ett genuint intresse av IT och är nyfiken på eller har erfarenhet av teknik för sjukvården har vi spännande och utmanande tjänster.  Arbetsbeskrivning Inom Teknikförvaltning Vårdsystem arbetar cirka 25 stycken IT-specialister/systemtekniker och supporttekniker. Tillsammans har vi det tekniska ansvaret för de system som nyttjas inom vården.  Antalet system och komplexitet ökar hela tiden och därför behöver vi nu bli fler inom följande spännande områden:   - Laboratoriemedicin  - Operation/IVA  - Fysiologi  I samarbete med vårdverksamheten, kollegor inom Medicinsk Teknik och leverantörer, utför och ansvarar vi för installationer, uppgraderingar och konfigurationer av medicintekniska IT-system som är integrerade i regionens infrastruktur.  Vi är delaktiga i ett systems hela livscykel, från behovsinsamling till avveckling/ersättning.  Vanligt förekommande arbetsuppgifter är:   - Felavhjälpning och support  - Installation och konfigurering  - Uppgraderingar  - Integrationer  - Delta i projekt- och upphandlingar av Medicintekniska IT-system  - Kravställning mot intern infrastruktur samt externa leverantörer  - Livscykelhantering  - Tekniska beskrivningar  Vi är en flexibel arbetsplats med ambitionen att det ska finnas en bra balans mellan arbete och privatliv. Tillsammans jobbar vi med ständiga förbättringar och hos oss finns goda möjligheter att utvecklas inom organisationen.   Erfarenhet och utbildning Vi söker dig som har relevant högskoleutbildning inom IT och/eller längre arbetserfarenhet som vi bedömer som relevant.  Du har gärna en bakgrund som IT-tekniker, systemtekniker, teknisk systemförvaltare eller liknande.  Du har intresse för eller tidigare erfarenhet av något av följande:   - Applikationsdrift  - Databashantering (MS SQL)  - Windows Server-plattform  - Medicinska IT-system  - Support och förvaltning av IT-system  - Ramverk som Pm3 och ITIL  Goda kunskaper i svenska och engelska såväl i tal som skrift är en förutsättning. B-körkort är ett krav.  Dina personliga egenskaper Stor vikt kommer att läggas vid dina personliga egenskaper. För att trivas hos oss är du en person som är drivande, självgående, engagerad, initiativtagande och serviceinriktad. Du är bra på att samarbeta, kommunicera och tycker det är spännande med problemlösning. Självklart är du ansvarstagande, har ordningssinne samt inser vikten av god planering och dokumentation. Du innehar god social kompetens och har förmågan att arbeta självständigt och i grupp. Du klarar att prioritera mellan och samtidigt hålla igång flera pågående aktiviteter. Du hittar förbättringar i vardagen för att utveckla ditt eget och enhetens arbete.  Varmt välkommen in med din ansökan! Om du behöver hjälp med att registrera i systemet, kontakta kundtjänst Stöd och service, 010-103 60 00, under kontorstid.  Till bemannings-, förmedlings- och rekryteringsföretag och till dig som är försäljare: Vi undanber oss vänligen men bestämt direktkontakt med bemannings-, förmedlings- och rekryteringsföretag samt andra externa aktörer och försäljare av ytterligare jobbannonser. Region Östergötland har upphandlade avtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **IT**-specialister/Systemtekniker t... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 2 | IT**-specialister**/Systemtekniker till Enheten T... |  | x |  | 13 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 3 | IT-specialister/**Systemtekniker** till Enheten Teknikförvaltnin... |  | x |  | 14 | [Nätverkstekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Nkbd_wa9_TZF) |
| 3 | IT-specialister/**Systemtekniker** till Enheten Teknikförvaltnin... | x | x | 14 | 14 | [Nätverks- och systemtekniker m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/VCpu_5EN_bBt) |
| 4 | ...ill Enheten Teknikförvaltning **Vårdsystem** Verksamhetsområde Medicinsk t... |  | x |  | 10 | [hälso- och sjukvårdssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/429C_bXW_42j) |
| 5 | ...tning, drift och underhåll av **medicinteknisk utrustning** och vårdsystem inom hälso- oc... |  | x |  | 25 | [Maskinunderhåll, medicinteknisk utrustning, **skill**](http://data.jobtechdev.se/taxonomy/concept/8Lrt_DBh_iZ6) |
| 5 | ...tning, drift och underhåll av **medicinteknisk utrustning** och vårdsystem inom hälso- oc... |  | x |  | 25 | [Tekniker, bilddiagnostik och medicinteknisk utrustning, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/GC7L_Yw7_Mfo) |
| 5 | ...tning, drift och underhåll av **medicinteknisk utrustning** och vårdsystem inom hälso- oc... |  | x |  | 25 | [Tekniker, bilddiagnostik och medicinteknisk utrustning, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/kLth_soy_Qaj) |
| 6 | ...medicinteknisk utrustning och **vårdsystem** inom hälso- och sjukvården. ... |  | x |  | 10 | [hälso- och sjukvårdssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/429C_bXW_42j) |
| 7 | ...trustning och vårdsystem inom **hälso- och **sjukvården.  Vi arbetar i en... |  | x |  | 11 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 8 | ...trustning och vårdsystem inom **hälso- och sjukvården**.  Vi arbetar i en komplex I... | x |  |  | 21 | [Hälso- och sjukvård, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/NYW6_mP6_vwf) |
| 9 | ...n.  Vi arbetar i en komplex **IT**-miljö för att stödja tekniskt... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 10 | ...  Vi arbetar i en komplex IT**-miljö** för att stödja tekniskt avanc... |  | x |  | 6 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 11 | ...de verksamheter och levererar **IT**-lösningar som skapar nytta oc... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 12 | ... verksamheter och levererar IT**-lösningar** som skapar nytta och god vård... |  | x |  | 10 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 13 | ...u har ett genuint intresse av **IT** och är nyfiken på eller har e... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 14 | ...ivning Inom Teknikförvaltning **Vårdsystem** arbetar cirka 25 stycken IT-s... |  | x |  | 10 | [hälso- och sjukvårdssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/429C_bXW_42j) |
| 15 | ...stem arbetar cirka 25 stycken **IT-specialister**/systemtekniker och supporttek... | x |  |  | 15 | [Övriga IT-specialister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/UxT1_tPF_Kbg) |
| 15 | ...stem arbetar cirka 25 stycken **IT-specialister**/systemtekniker och supporttek... |  | x |  | 15 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 16 | ...ka 25 stycken IT-specialister/**systemtekniker** och supporttekniker. Tillsamm... |  | x |  | 14 | [Nätverkstekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Nkbd_wa9_TZF) |
| 16 | ...ka 25 stycken IT-specialister/**systemtekniker** och supporttekniker. Tillsamm... |  | x |  | 14 | [Nätverks- och systemtekniker m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/VCpu_5EN_bBt) |
| 16 | ...ka 25 stycken IT-specialister/**systemtekniker** och supporttekniker. Tillsamm... | x |  |  | 14 | [IT-systemtekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/h9KW_298_QRF) |
| 17 | ...ecialister/systemtekniker och **supporttekniker**. Tillsammans har vi det tekni... | x | x | 15 | 15 | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| 18 | ...iemedicin  - Operation/IVA  - **Fysiologi**  I samarbete med vårdverksamh... | x | x | 9 | 9 | [Fysiologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/7rqs_dJb_gjc) |
| 18 | ...iemedicin  - Operation/IVA  - **Fysiologi**  I samarbete med vårdverksamh... |  | x |  | 9 | [Fysiologi, medicintekniskt område ingenjör, **skill**](http://data.jobtechdev.se/taxonomy/concept/ApUn_CB8_UWH) |
| 18 | ...iemedicin  - Operation/IVA  - **Fysiologi**  I samarbete med vårdverksamh... |  | x |  | 9 | [hörselns fysiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Mchb_zNL_Ub1) |
| 18 | ...iemedicin  - Operation/IVA  - **Fysiologi**  I samarbete med vårdverksamh... |  | x |  | 9 | [balansens fysiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/YfAC_XSd_qwU) |
| 18 | ...iemedicin  - Operation/IVA  - **Fysiologi**  I samarbete med vårdverksamh... |  | x |  | 9 | [djurs fysiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pedK_CVT_549) |
| 19 | ...gurationer av medicintekniska **IT**-system som är integrerade i r... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 20 | ...rationer av medicintekniska IT**-system** som är integrerade i regionen... |  | x |  | 7 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 21 | ...om är integrerade i regionens **infrastruktur**.  Vi är delaktiga i ett syste... | x |  |  | 13 | [IT-infrastruktur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/62is_raU_tqs) |
| 22 | ...handlingar av Medicintekniska **IT**-system  - Kravställning mot i... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 23 | ...ndlingar av Medicintekniska IT**-system**  - Kravställning mot intern i... |  | x |  | 7 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 24 | ...iv. Tillsammans jobbar vi med **ständiga förbättringar** och hos oss finns goda möjlig... | x | x | 22 | 22 | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| 25 | ...Vi söker dig som har relevant **högskoleutbildning** inom IT och/eller längre arbe... | x |  |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 25 | ...Vi söker dig som har relevant **högskoleutbildning** inom IT och/eller längre arbe... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 26 | ...evant högskoleutbildning inom **IT** och/eller längre arbetserfare... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 27 | ... Du har gärna en bakgrund som **IT**-tekniker, systemtekniker, tek... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 28 | ... Du har gärna en bakgrund som **IT-tekniker**, systemtekniker, teknisk syst... |  | x |  | 11 | [it-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ybqU_Mu2_Eti) |
| 29 | ... en bakgrund som IT-tekniker, **systemtekniker**, teknisk systemförvaltare ell... |  | x |  | 14 | [Nätverkstekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Nkbd_wa9_TZF) |
| 29 | ... en bakgrund som IT-tekniker, **systemtekniker**, teknisk systemförvaltare ell... |  | x |  | 14 | [Nätverks- och systemtekniker m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/VCpu_5EN_bBt) |
| 29 | ... en bakgrund som IT-tekniker, **systemtekniker**, teknisk systemförvaltare ell... | x |  |  | 14 | [IT-systemtekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/h9KW_298_QRF) |
| 30 | ...iker, systemtekniker, teknisk **systemförvaltare** eller liknande.  Du har intre... | x |  |  | 16 | [Systemförvaltare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/rno7_MFk_voB) |
| 31 | ...nde:   - Applikationsdrift  - **Databashantering** (MS SQL)  - Windows Server-pl... |  | x |  | 16 | [databashanteringssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BZwG_81P_TyN) |
| 32 | ...onsdrift  - Databashantering (**MS **SQL)  - Windows Server-plattfo... |  | x |  | 3 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 33 | ...drift  - Databashantering (MS **SQL**)  - Windows Server-plattform ... | x | x | 3 | 3 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 34 | ... Databashantering (MS SQL)  - **Windows** Server-plattform  - Medicinsk... | x | x | 7 | 7 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 35 | ...erver-plattform  - Medicinska **IT**-system  - Support och förvalt... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 36 | ...ver-plattform  - Medicinska IT**-system**  - Support och förvaltning av... |  | x |  | 7 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 37 | ... - Support och förvaltning av **IT**-system  - Ramverk som Pm3 och... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 38 | ... Support och förvaltning av IT**-system**  - Ramverk som Pm3 och ITIL  ... |  | x |  | 7 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 39 | ...h förvaltning av IT-system  - **Ramverk** som Pm3 och ITIL  Goda kunska... | x |  |  | 7 | [Ramverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/srsj_GQb_CE1) |
| 40 | ...g av IT-system  - Ramverk som **Pm3** och ITIL  Goda kunskaper i sv... | x | x | 3 | 3 | [pm3, förvaltningsmodell, **skill**](http://data.jobtechdev.se/taxonomy/concept/abgV_e4C_gwj) |
| 41 | ...system  - Ramverk som Pm3 och **ITIL**  Goda kunskaper i svenska och... |  | x |  | 4 | [arbeta i en ITIL-baserad miljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZJS4_Han_D8x) |
| 42 | ...m3 och ITIL  Goda kunskaper i **svenska** och engelska såväl i tal som ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 43 | ... Goda kunskaper i svenska och **engelska** såväl i tal som skrift är en ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 44 | ...m skrift är en förutsättning. **B-körkort** är ett krav.  Dina personliga... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 45 | ...blemlösning. Självklart är du **ansvarstagande**, har ordningssinne samt inser... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 46 | ...r vikten av god planering och **dokumentation**. Du innehar god social kompet... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 47 | ...ompetens och har förmågan att **arbeta självständigt** och i grupp. Du klarar att pr... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 48 | ...gistrera i systemet, kontakta **kundtjänst** Stöd och service, 010-103 60 ... |  | x |  | 10 | [Kundtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/aMyW_hB7_TpU) |
| 49 | ...erligare jobbannonser. Region **Östergötland** har upphandlade avtal. |  | x |  | 12 | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
| | **Overall** | | | **158** | **650** | 158/650 = **24%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [hälso- och sjukvårdssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/429C_bXW_42j) |
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| x |  |  | [IT-infrastruktur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/62is_raU_tqs) |
| x | x | x | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| x | x | x | [Fysiologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/7rqs_dJb_gjc) |
|  | x |  | [Maskinunderhåll, medicinteknisk utrustning, **skill**](http://data.jobtechdev.se/taxonomy/concept/8Lrt_DBh_iZ6) |
|  | x |  | [Fysiologi, medicintekniskt område ingenjör, **skill**](http://data.jobtechdev.se/taxonomy/concept/ApUn_CB8_UWH) |
|  | x |  | [databashanteringssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BZwG_81P_TyN) |
| x | x | x | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
|  | x |  | [Tekniker, bilddiagnostik och medicinteknisk utrustning, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/GC7L_Yw7_Mfo) |
| x | x | x | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
|  | x |  | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
|  | x |  | [hörselns fysiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Mchb_zNL_Ub1) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [Hälso- och sjukvård, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/NYW6_mP6_vwf) |
|  | x |  | [Nätverkstekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Nkbd_wa9_TZF) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Övriga IT-specialister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/UxT1_tPF_Kbg) |
| x | x | x | [Nätverks- och systemtekniker m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/VCpu_5EN_bBt) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
|  | x |  | [balansens fysiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/YfAC_XSd_qwU) |
|  | x |  | [arbeta i en ITIL-baserad miljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZJS4_Han_D8x) |
|  | x |  | [Kundtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/aMyW_hB7_TpU) |
| x | x | x | [pm3, förvaltningsmodell, **skill**](http://data.jobtechdev.se/taxonomy/concept/abgV_e4C_gwj) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [IT-systemtekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/h9KW_298_QRF) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
|  | x |  | [Tekniker, bilddiagnostik och medicinteknisk utrustning, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/kLth_soy_Qaj) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
|  | x |  | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
|  | x |  | [djurs fysiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/pedK_CVT_549) |
| x |  |  | [Systemförvaltare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/rno7_MFk_voB) |
| x |  |  | [Ramverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/srsj_GQb_CE1) |
|  | x |  | [it-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ybqU_Mu2_Eti) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **14** | 14/37 = **38%** |