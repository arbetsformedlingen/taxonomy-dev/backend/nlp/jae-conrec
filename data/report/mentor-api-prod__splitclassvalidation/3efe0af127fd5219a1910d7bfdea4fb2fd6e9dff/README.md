# Results for '3efe0af127fd5219a1910d7bfdea4fb2fd6e9dff'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [3efe0af127fd5219a1910d7bfdea4fb2fd6e9dff](README.md) | 1 | 2552 | 13 | 27 | 57/266 = **21%** | 3/19 = **16%** |

## Source text

Badvakt till Karlskrona Simsällskap Om tjänsten Hos oss får du möjlighet att arbeta i ett glatt arbetslag där vi jobbar med att erbjuda våra besökare en god personlig service. Vi erbjuder våra gäster ett säkert och tryggt besök i simhallen. Tjänsten innebär att du arbetar i arbetslag med cirkulationstjänstgöring. Tjänstgöringen är schemalagd med arbete morgon, dag, kvällar samt helger.   Arbetsbeskrivning I arbetsuppgifterna ingår bemanning av anläggning för att se till att trivsel - och säkerhetsregler efterföljs. Anställningen innebär att tjänstgöringen är på badhus/utebad. Deltidsanställning 80 %.   Kvalifikationer/Krav Klara kunskapskrav i simning och livräddning motsvarande guldbojen SLS. Simtest görs innan anställning. Vara 18 år fyllda Erfarenhet av serviceyrke Vana av samarbete i grupp/arbetslag God kunskap i svenska språket   Meriterande  Erfarenhet från arbete vid badanläggning – poollivräddare, badvakt   Övrigt  Utdrag ut belastningsregistret ska lämnas in. Vi lägger stor vikt vid personliga egenskaper.   Karlskrona simsällskaps är en ideell förening som bedriver idrottsverksamhet, med simkonsten som grund inom simsport och triathlon. Härutöver håller Simsällskapet simskola och badvaktsverksamhet. Verksamheten genomförs på årlig basis och kan i stort delas in i en vår-, en sommar- och en hösttermin.  Verksamhetsplatserna är Karlskrona Simhall, inom- och utomhus bassängerna i Rödeby, kommunens badplatser samt i kommunens idrottshallar. Det sistnämnd avsett för träningsgruppernas landträning.   Merparten av Simsällskapets ledare är ideella eller arvoderade. Ett mindre antal är del- eller heltidsanställda. All verksamhet leds av en klubbchef.   Simsällskapet bildades 25 augusti 1900 och har en välrenommerad och lyckosam historia innefattande betydande simidrottsliga framgångar vid olympiska spel, världs-, europiska-, nordiska och svenska mästerskap. Insatserna som sim utbildande föreningen är också framgångsrika. Simsällskapet är sedan decennier kommunens största simskoleleverantör. Sedan invigningen av Karlskrona Simhall 1995 har sällskapet ansvaret att hålla badvaktsverksamheten i Karlskrona simhall och Rödeby inom- och utomhus bassänger. Badvaktsverksamhet sker såväl på dagtid som kvällstid samt vardag som helgdag.   För att kunna ge sällskapets medlemmar och simidrottsutövare service och stöd finns en kanslifunktion, vid Nya Skeppsbrogatan 7-9   Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Badvakt** till Karlskrona Simsällskap O... |  | x |  | 7 | [Badmästare/Badbevakare/Badvakt/Simhallsvakt/Strandvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BbNn_Gtf_rKp) |
| 1 | **Badvakt** till Karlskrona Simsällskap O... |  | x |  | 7 | [Simhallsvakt, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Cvxu_Ees_nCc) |
| 1 | **Badvakt** till Karlskrona Simsällskap O... |  | x |  | 7 | [Godkänd badvakt, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/FABn_RTJ_1xT) |
| 1 | **Badvakt** till Karlskrona Simsällskap O... |  | x |  | 7 | [Utbildning till badmästare, **keyword**](http://data.jobtechdev.se/taxonomy/concept/eRxR_Rqt_QjF) |
| 1 | **Badvakt** till Karlskrona Simsällskap O... |  | x |  | 7 | [badmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qTnH_gbA_1fm) |
| 1 | **Badvakt** till Karlskrona Simsällskap O... | x | x | 7 | 7 | [badvakt, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tps1_qiD_eL3) |
| 2 | Badvakt till **Karlskrona** Simsällskap Om tjänsten Hos o... | x | x | 10 | 10 | [Karlskrona, **municipality**](http://data.jobtechdev.se/taxonomy/concept/YSt4_bAa_ccs) |
| 3 | ...göringen är på badhus/utebad. **Deltidsanställning 80 %**.   Kvalifikationer/Krav Klara... | x |  |  | 23 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 4 | ...äddning motsvarande guldbojen **SLS**. Simtest görs innan anställni... |  | x |  | 3 | [Godkänd badvakt, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/FABn_RTJ_1xT) |
| 4 | ...äddning motsvarande guldbojen **SLS**. Simtest görs innan anställni... | x | x | 3 | 3 | [Godkänd livräddare, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/GJho_f1V_YbF) |
| 4 | ...äddning motsvarande guldbojen **SLS**. Simtest görs innan anställni... |  | x |  | 3 | [Godkänd simlärarassistent, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/JJLA_ymq_vYf) |
| 4 | ...äddning motsvarande guldbojen **SLS**. Simtest görs innan anställni... |  | x |  | 3 | [Godkänd simlärare, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/sxer_JSL_7Bv) |
| 5 | ...grupp/arbetslag God kunskap i **svenska** språket   Meriterande  Erfare... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 6 | ...grupp/arbetslag God kunskap i **svenska språket**   Meriterande  Erfarenhet frå... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 7 | ...danläggning – poollivräddare, **badvakt**   Övrigt  Utdrag ut belastnin... |  | x |  | 7 | [Badmästare/Badbevakare/Badvakt/Simhallsvakt/Strandvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BbNn_Gtf_rKp) |
| 7 | ...danläggning – poollivräddare, **badvakt**   Övrigt  Utdrag ut belastnin... |  | x |  | 7 | [Simhallsvakt, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Cvxu_Ees_nCc) |
| 7 | ...danläggning – poollivräddare, **badvakt**   Övrigt  Utdrag ut belastnin... |  | x |  | 7 | [Godkänd badvakt, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/FABn_RTJ_1xT) |
| 7 | ...danläggning – poollivräddare, **badvakt**   Övrigt  Utdrag ut belastnin... |  | x |  | 7 | [Utbildning till badmästare, **keyword**](http://data.jobtechdev.se/taxonomy/concept/eRxR_Rqt_QjF) |
| 7 | ...danläggning – poollivräddare, **badvakt**   Övrigt  Utdrag ut belastnin... |  | x |  | 7 | [badmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qTnH_gbA_1fm) |
| 7 | ...danläggning – poollivräddare, **badvakt**   Övrigt  Utdrag ut belastnin... | x | x | 7 | 7 | [badvakt, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tps1_qiD_eL3) |
| 8 | ... vid personliga egenskaper.   **Karlskrona** simsällskaps är en ideell för... | x | x | 10 | 10 | [Karlskrona, **municipality**](http://data.jobtechdev.se/taxonomy/concept/YSt4_bAa_ccs) |
| 9 | ... ideell förening som bedriver **idrottsverksamhet**, med simkonsten som grund ino... |  | x |  | 17 | [stödja utveckling av idrottsverksamhet i utbildning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zGgd_iMg_EWp) |
| 10 | ...med simkonsten som grund inom **simsport** och triathlon. Härutöver håll... |  | x |  | 8 | [Simsport, instruktör/Simsport, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/fTok_VjS_cH2) |
| 10 | ...med simkonsten som grund inom **simsport** och triathlon. Härutöver håll... |  | x |  | 8 | [Simsport, professionell utövning, **skill**](http://data.jobtechdev.se/taxonomy/concept/sVqA_fEm_aCY) |
| 11 | ...n som grund inom simsport och **triathlon**. Härutöver håller Simsällskap... |  | x |  | 9 | [Triathlon, instruktör/Triathlon, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/LznR_wFc_MLQ) |
| 12 | ...min.  Verksamhetsplatserna är **Karlskrona** Simhall, inom- och utomhus ba... | x | x | 10 | 10 | [Karlskrona, **municipality**](http://data.jobtechdev.se/taxonomy/concept/YSt4_bAa_ccs) |
| 13 | ...voderade. Ett mindre antal är **del**- eller heltidsanställda. All ... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 14 | ...tt mindre antal är del- eller **heltidsanställda**. All verksamhet leds av en kl... | x |  |  | 16 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 15 | ...ds-, europiska-, nordiska och **svenska** mästerskap. Insatserna som si... | x |  |  | 7 | [Svenska, **keyword**](http://data.jobtechdev.se/taxonomy/concept/gfhX_bqH_U4K) |
| 15 | ...ds-, europiska-, nordiska och **svenska** mästerskap. Insatserna som si... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 16 | ...erantör. Sedan invigningen av **Karlskrona** Simhall 1995 har sällskapet a... | x | x | 10 | 10 | [Karlskrona, **municipality**](http://data.jobtechdev.se/taxonomy/concept/YSt4_bAa_ccs) |
| 17 | ... hålla badvaktsverksamheten i **Karlskrona** simhall och Rödeby inom- och ... | x |  |  | 10 | [Karlskrona, **municipality**](http://data.jobtechdev.se/taxonomy/concept/YSt4_bAa_ccs) |
| | **Overall** | | | **57** | **266** | 57/266 = **21%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
|  | x |  | [Badmästare/Badbevakare/Badvakt/Simhallsvakt/Strandvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BbNn_Gtf_rKp) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
|  | x |  | [Simhallsvakt, **job-title**](http://data.jobtechdev.se/taxonomy/concept/Cvxu_Ees_nCc) |
|  | x |  | [Godkänd badvakt, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/FABn_RTJ_1xT) |
| x | x | x | [Godkänd livräddare, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/GJho_f1V_YbF) |
|  | x |  | [Godkänd simlärarassistent, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/JJLA_ymq_vYf) |
|  | x |  | [Triathlon, instruktör/Triathlon, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/LznR_wFc_MLQ) |
| x | x | x | [Karlskrona, **municipality**](http://data.jobtechdev.se/taxonomy/concept/YSt4_bAa_ccs) |
|  | x |  | [Utbildning till badmästare, **keyword**](http://data.jobtechdev.se/taxonomy/concept/eRxR_Rqt_QjF) |
|  | x |  | [Simsport, instruktör/Simsport, tränare, **skill**](http://data.jobtechdev.se/taxonomy/concept/fTok_VjS_cH2) |
| x |  |  | [Svenska, **keyword**](http://data.jobtechdev.se/taxonomy/concept/gfhX_bqH_U4K) |
|  | x |  | [badmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qTnH_gbA_1fm) |
|  | x |  | [Simsport, professionell utövning, **skill**](http://data.jobtechdev.se/taxonomy/concept/sVqA_fEm_aCY) |
|  | x |  | [Godkänd simlärare, SLS, **skill**](http://data.jobtechdev.se/taxonomy/concept/sxer_JSL_7Bv) |
| x | x | x | [badvakt, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tps1_qiD_eL3) |
|  | x |  | [stödja utveckling av idrottsverksamhet i utbildning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zGgd_iMg_EWp) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/19 = **16%** |