# Results for 'ec337ec7712903f9fb1c047ee2d26d19c9a98a43'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ec337ec7712903f9fb1c047ee2d26d19c9a98a43](README.md) | 1 | 3720 | 20 | 10 | 116/269 = **43%** | 7/17 = **41%** |

## Source text

Junior backendutvecklare till Eniac! Vill du hjälpa Eniac att utveckla affärssystem med omtanke? Eniac söker en engagerad utvecklare som vill växa i programmerarrollen, och som drivs av att göra slutanvändaren nöjd. Som programutvecklare på Eniac erbjuds du att arbeta med helheten, och har stora möjligheter att vara med och påverka slutresultatet.  Låter det intressant? Välkommen med din ansökan redan idag!   OM TJÄNSTEN  Vi söker dig som kan utöka Eniacs team inom backendutveckling och hjälpa dem att leverera nytta och värde till sina kunder. Du kommer att ingå i ett korsfunktionellt team och du får jobba från ax till limpa med produktutveckling i samarbete med produktägare och andra utvecklare. Du kommer arbeta med kundanpassade installationer, utveckling av ny funktionalitet samt vidareutveckling av existerande system. De har idag ett Windows-system med hundratals användare över hela landet, och är inne på en resa för att göra denna produkt helt webbaserad. Utmaningarna är många, och du kommer att få många tillfällen att bidra och utvecklas inom olika områden.  ARBETSUPPGIFTER  Hos Eniac får du stor möjlighet att växa som utvecklare. De har flera programmerare med lång erfarenhet, och de hjälper alltid och stöttar varandra för att det de gör ska bli så bra som möjligt! De jobbar tight ihop och sätter alltid kunden i fokus. För att du ska få rätt förutsättningar och stöttning kommer du tilldelas en mentor som introducerar dig i system och arbetsuppgifter.  På Eniac försöker de alltid jobba långsiktigt, lyhört, flexibelt och prestigelöst. Under deras dryga 40 år som växande bolag har de märkt att allt hänger ihop. Framgångsrika affärer börjar med ett bra arbetsklimat, såväl internt som gentemot kund. På så vis lyfter de varandra och skapar system i världsklass, människor och teknik emellan. De satsar på sina medarbetare och erbjuder kollektivavtal, generöst friskvårdsbidrag och friskvård på arbetstid. Dessutom har de regelbundet stora och små personalaktiviteter – så som personalkonferenser, afterwork eller en fralla till kaffet. Missa inte chansen att arbeta tillsammans med kunniga, hjälpsamma och jordnära kollegor i ett kundnära bolag med öppen företagskultur!  VI SÖKER DIG SOM  · Har utbildningsnivå som motsvarar programmeringskunskap på högskola eller universitet  · Är driven och vill utvecklas som programmerare  · Gillar att göra användare nöjda  · Trivs med att arbeta tillsammans med andra  Det är väldigt meriterande om du har arbetslivserfarenhet från en liknande roll inom programutveckling eller har egna hobbyprojekt som påvisar dina kunskaper och intresse för utveckling.  De personliga egenskaper vi kommer lägga fokus på vid rekryteringen är:   * Problemlösande * Samarbetsförmåga * Ansvarstagande * Självgående   Övrig information   * Start: Enligt överenskommelse * Omfattning: Heltid, Tillsvidare * Placering: Krokslätts Torg 5, Mölndal * Rekryteringsprocessen hanteras av Academic Work och Eniac's önskemål är att alla frågor rörande tjänsten hanteras av Academic Work.   Vi går igenom urvalet löpande och annonsen kan stängas ner innan tjänsten är tillsatt om vi gått över till urvals- och intervjufasen.  INFORMATION OM FÖRETAGET  Eniac är ett företag med 34 anställda och kontor i Mölndal. Företaget kännetecknas av en platt organisationsstruktur med stort eget ansvar. De levererar kvalificerade, administrativa system och IT-tjänster till sina kunder, så att de kan fokusera helhjärtat på sin verksamhet. De är marknadsledande leverantör till enheter inom Svenska kyrkan. För dem är det viktigt att affärssystemen som de utvecklar inte bara håller en hög standard utan att de också är anpassade efter kundens behov. Det kallar de för system med omtanke!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Junior **backendutvecklare** till Eniac! Vill du hjälpa En... | x | x | 17 | 17 | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| 2 | ...om kan utöka Eniacs team inom **backendutveckling** och hjälpa dem att leverera n... | x | x | 17 | 17 | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| 3 | ... jobba från ax till limpa med **produktutveckling** i samarbete med produktägare ... | x | x | 17 | 17 | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| 4 | ...rande system. De har idag ett **Windows-system** med hundratals användare över... | x |  |  | 14 | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| 5 | ... som utvecklare. De har flera **programmerare** med lång erfarenhet, och de h... | x |  |  | 13 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 6 | ...sina medarbetare och erbjuder **kollektivavtal**, generöst friskvårdsbidrag oc... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 7 | ...uder kollektivavtal, generöst **friskvårdsbidrag** och friskvård på arbetstid. D... | x |  |  | 16 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 8 | ...generöst friskvårdsbidrag och **friskvård** på arbetstid. Dessutom har de... | x |  |  | 9 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 9 | ...tur!  VI SÖKER DIG SOM  · Har **utbildningsnivå** som motsvarar programmeringsk... | x |  |  | 15 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 10 | ...utbildningsnivå som motsvarar **programmeringskunskap** på högskola eller universitet... | x | x | 21 | 21 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 11 | ...driven och vill utvecklas som **programmerare**  · Gillar att göra användare ... | x |  |  | 13 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 12 | ...ändare nöjda  · Trivs med att **arbeta tillsammans med andra**  Det är väldigt meriterande o... | x |  |  | 28 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 13 | ...mlösande * Samarbetsförmåga * **Ansvarstagande** * Självgående   Övrig informa... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 14 | ...överenskommelse * Omfattning: **Heltid**, Tillsvidare * Placering: Kro... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 15 | ...ommelse * Omfattning: Heltid, **Tillsvidare** * Placering: Krokslätts Torg ... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 16 | ...Placering: Krokslätts Torg 5, **Mölndal** * Rekryteringsprocessen hante... | x | x | 7 | 7 | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
| 17 | ... företag med 34 anställda och **kontor** i Mölndal. Företaget kännetec... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 18 | ...med 34 anställda och kontor i **Mölndal**. Företaget kännetecknas av en... | x | x | 7 | 7 | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
| 19 | ...de, administrativa system och **IT**-tjänster till sina kunder, så... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 20 | ..., administrativa system och IT**-tjänster** till sina kunder, så att de k... |  | x |  | 9 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 21 | ... leverantör till enheter inom **Svenska** kyrkan. För dem är det viktig... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 22 | ...tör till enheter inom Svenska **kyrkan**. För dem är det viktigt att a... | x |  |  | 6 | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
| | **Overall** | | | **116** | **269** | 116/269 = **43%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| x |  |  | [Windows, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/G4sF_xod_zoq) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| x |  |  | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
| x | x | x | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
| x | x | x | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| x |  |  | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/17 = **41%** |