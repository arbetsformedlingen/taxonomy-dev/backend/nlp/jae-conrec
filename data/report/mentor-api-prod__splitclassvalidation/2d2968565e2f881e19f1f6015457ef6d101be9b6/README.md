# Results for '2d2968565e2f881e19f1f6015457ef6d101be9b6'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2d2968565e2f881e19f1f6015457ef6d101be9b6](README.md) | 1 | 928 | 5 | 6 | 24/108 = **22%** | 2/9 = **22%** |

## Source text

Hotellstädare med erfarenhet Vi söker en serviceinriktad medarbetare som också är glädjespridare på stället. Dina huvudsakliga arbetsuppgifter är att hålla rent och snyggt på hotellrummen, hålla ordning i förråd, hantera kvarglömda saker och hjälpa med andra sysslor när tillfälle ges. Du är ansvarig för all städning av hotellrum och allmänna ytor samt tvätt av sängkläder och diska. Vi ser dig som en glad och trevlig person till både kollegor och gäster, gillar att hålla ordning och rent och har ett öga för det lilla extra. Om du har jobbat som vaktmästare är det meriterande till vissa uppgifter. Noggrannhet och prestigelöshet är en av dina drivkrafter samtidigt som gästen är i fokus och din känsla för service är klart avgörande egenskaper. Det handlar om provanställning med möjlighet till förlängning. Anställningen  är fr.o.m. 15 augusti t.o.m. 15 september 2022. Cirka 40 % tjänst, dagtid mån-fre. Lön enligt avtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Hotellstädare** med erfarenhet Vi söker en se... | x | x | 13 | 13 | [Hotellstädare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PiBg_iBj_qEw) |
| 1 | **Hotellstädare** med erfarenhet Vi söker en se... |  | x |  | 13 | [Hotell- och kontorsstädare m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/f6at_pP2_3eJ) |
| 2 | ...slor när tillfälle ges. Du är **ansvarig för all städning** av hotellrum och allmänna yto... | x |  |  | 25 | [Städansvarig, **job-title**](http://data.jobtechdev.se/taxonomy/concept/iSxM_oBw_xa4) |
| 3 | ...e ges. Du är ansvarig för all **städning** av hotellrum och allmänna yto... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 4 | ... samt tvätt av sängkläder och **diska**. Vi ser dig som en glad och t... |  | x |  | 5 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 5 | ...a extra. Om du har jobbat som **vaktmästare** är det meriterande till vissa... |  | x |  | 11 | [vaktmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/88vj_SBF_e3N) |
| 5 | ...a extra. Om du har jobbat som **vaktmästare** är det meriterande till vissa... | x |  |  | 11 | [Vaktmästare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/rXQH_ojd_Pe1) |
| 6 | ...terande till vissa uppgifter. **Noggrannhet** och prestigelöshet är en av d... | x | x | 11 | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 7 | ...o.m. 15 september 2022. Cirka **40 % tjänst**, dagtid mån-fre. Lön enligt a... | x |  |  | 11 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| | **Overall** | | | **24** | **108** | 24/108 = **22%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
|  | x |  | [vaktmästare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/88vj_SBF_e3N) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
|  | x |  | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x | x | x | [Hotellstädare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PiBg_iBj_qEw) |
|  | x |  | [Hotell- och kontorsstädare m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/f6at_pP2_3eJ) |
| x |  |  | [Städansvarig, **job-title**](http://data.jobtechdev.se/taxonomy/concept/iSxM_oBw_xa4) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [Vaktmästare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/rXQH_ojd_Pe1) |
| | | **2** | 2/9 = **22%** |