# Results for '8b6fe377d1a67a10f333e00799d9782cab86deac'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8b6fe377d1a67a10f333e00799d9782cab86deac](README.md) | 1 | 1953 | 20 | 14 | 144/268 = **54%** | 11/18 = **61%** |

## Source text

Attendo hemtjänst Kungsholmen söker engagerade medarbetare! Välkommen till en värderingsstyrd organisation  I Attendo äldreomsorg är vi över 9000 medarbetare som arbetar inom hemtjänst och äldreboende i Skandinavien, vi är en del av Attendo, Nordens ledande privata vård och omsorgsföretag. Hos oss kommer du att arbeta med vår vision: Att stärka individen och alltid utgå från den enskildes behov och förutsättningar.  Attendo hemtjänst på Kungsholmen söker engagerade och positiva medarbetare! Vi kan erbjuda dig både dag- och kvällsschema. Vi söker tillsvidareanställda och timanställda.  Beskrivning av tjänsten  Stor vikt läggs vid bemötande, kommunikation, kontaktmanskap och dokumentation i journalsystem.  - Goda kunskaper i svenska språket (tal och skrift) - God datorkunskap/datorvana - Kan cykla - Körkort B - Meriterande om du har undersköterske- eller vårdbiträdesutbildning - Meriterande om du tidigare arbetat inom äldreomsorgen och hemtjänst  Du trivs med att ge god service, skapa fina relationer och vill bidra till höjd livskvalité hos våra kunder. Du är en självständig, ödmjuk och ansvarsfull person som brinner för vård av äldre och kan se individens behov.  Stämmer beskrivningen in på dig? Sök redan idag!  Om Attendo  Attendo är ett av Sveriges ledande vård- och omsorgsföretag med 24 000 medarbetare. Mångfald värderas högt, vårt arbete ska stärka individen och vi arbetar för att ge utmärkt service både till våra kunder och kollegor. Attendo verkar i en platt organisation och är en trygg arbetsgivare, med kollektivavtal, avtalsförsäkringar och det finns stora möjligheter att göra karriär.  Vi går igenom ansökningarna löpande.  Före erbjudande om anställning ska utdrag ur polisens belastningsregister uppvisas tillsammans med giltig fotolegitimation.  Vi har även krav på vaccination mot covid-19 och kommer att kontrollera vaccinationsbevis innan anställning.  Låt oss tillsammans utveckla framtidens viktigaste bransch!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Attendo **hemtjänst** Kungsholmen söker engagerade ... | x |  |  | 9 | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
| 2 | ...styrd organisation  I Attendo **äldreomsorg** är vi över 9000 medarbetare s... | x | x | 11 | 11 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 3 | ... medarbetare som arbetar inom **hemtjänst** och äldreboende i Skandinavie... | x |  |  | 9 | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
| 4 | ...om arbetar inom hemtjänst och **äldreboende** i Skandinavien, vi är en del ... | x | x | 11 | 11 | [Äldreboende, **skill**](http://data.jobtechdev.se/taxonomy/concept/tykJ_BZQ_Wer) |
| 5 | ...och förutsättningar.  Attendo **hemtjänst** på Kungsholmen söker engagera... | x |  |  | 9 | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
| 6 | ...g- och kvällsschema. Vi söker **tillsvidareanställda** och timanställda.  Beskrivnin... | x |  |  | 20 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 7 | ...öker tillsvidareanställda och **timanställda**.  Beskrivning av tjänsten  St... | x |  |  | 12 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 8 | ...vid bemötande, kommunikation, **kontaktmanskap** och dokumentation i journalsy... |  | x |  | 14 | [Kontaktmannaskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/xLxq_sQ7_RzR) |
| 9 | ...unikation, kontaktmanskap och **dokumentation** i journalsystem.  - Goda kuns... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 10 | ...alsystem.  - Goda kunskaper i **svenska** språket (tal och skrift) - Go... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 11 | ....  - Goda kunskaper i svenska **språket** (tal och skrift) - God datork... | x |  |  | 7 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 12 | ...pråket (tal och skrift) - God **datorkunskap**/datorvana - Kan cykla - Körko... |  | x |  | 12 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 13 | ...nskap/datorvana - Kan cykla - **Körkort** B - Meriterande om du har und... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 14 | ...atorvana - Kan cykla - Körkort** B** - Meriterande om du har under... | x |  |  | 2 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 15 | ...ort B - Meriterande om du har **undersköterske**- eller vårdbiträdesutbildning... | x | x | 14 | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 16 | ...rande om du har undersköterske**- eller vå**rdbiträdesutbildning - Meriter... |  | x |  | 10 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 17 | ... du har undersköterske- eller **vårdbiträde**sutbildning - Meriterande om d... | x |  |  | 11 | [Vårdbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4zLr_jP5_peZ) |
| 18 | ...e om du tidigare arbetat inom **äldreomsorgen** och hemtjänst  Du trivs med a... | x | x | 13 | 13 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 19 | ...rbetat inom äldreomsorgen och **hemtjänst**  Du trivs med att ge god serv... | x |  |  | 9 | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
| 20 | ...ité hos våra kunder. Du är en **självständig**, ödmjuk och ansvarsfull perso... | x | x | 12 | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 21 | ...r en självständig, ödmjuk och **ansvarsfull** person som brinner för vård a... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 22 | ...rsfull person som brinner för **vård av äldre** och kan se individens behov. ... | x | x | 13 | 13 | [Vård av äldre, **skill**](http://data.jobtechdev.se/taxonomy/concept/yL7p_NgY_yKc) |
| 23 | ...är en trygg arbetsgivare, med **kollektivavtal**, avtalsförsäkringar och det f... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 24 | ...tsgivare, med kollektivavtal, **avtalsförsäkringar** och det finns stora möjlighet... | x | x | 18 | 18 | [Avtalsförsäkringar, **skill**](http://data.jobtechdev.se/taxonomy/concept/AzJi_3Dp_Sm9) |
| | **Overall** | | | **144** | **268** | 144/268 = **54%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [Vårdbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4zLr_jP5_peZ) |
| x | x | x | [Avtalsförsäkringar, **skill**](http://data.jobtechdev.se/taxonomy/concept/AzJi_3Dp_Sm9) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
|  | x |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x |  |  | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Äldreboende, **skill**](http://data.jobtechdev.se/taxonomy/concept/tykJ_BZQ_Wer) |
|  | x |  | [Kontaktmannaskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/xLxq_sQ7_RzR) |
| x |  |  | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
| x | x | x | [Vård av äldre, **skill**](http://data.jobtechdev.se/taxonomy/concept/yL7p_NgY_yKc) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **11** | 11/18 = **61%** |