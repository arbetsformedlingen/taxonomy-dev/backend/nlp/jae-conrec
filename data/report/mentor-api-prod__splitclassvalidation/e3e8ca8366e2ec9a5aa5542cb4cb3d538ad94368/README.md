# Results for 'e3e8ca8366e2ec9a5aa5542cb4cb3d538ad94368'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e3e8ca8366e2ec9a5aa5542cb4cb3d538ad94368](README.md) | 1 | 4111 | 33 | 25 | 169/310 = **55%** | 13/29 = **45%** |

## Source text

Svetsare/Plåtslagare Svetsare/Plåtslagare Muskö  På Saab blickar vi ständigt framåt och utmanar gränserna för vad som är tekniskt möjligt. Vi samarbetar med kollegor runt om i världen som alla delar vår utmaning - att göra världen till en säkrare plats. På Saab, Muskö blir du en del av ett sammansvetsat gäng som gör nästan allt för att Marinens fartyg skall vara tillgängliga och körklara.  Vad du blir en del av Saab är en helhetsleverantör av marin högteknologi i världsklass - på ytan och under. Vi designar, bygger och underhåller marina ytfartyg och ubåtar som innehåller världens mest avancerade smygteknik. Andra framgångsrika produkter är det luftoberoende Stirling (AIP) systemet och minröjningssystem. På Saab är produktionen hjärtat i varvsverksamheten. Vi ansvarar för nybyggnation, ombyggnad samt underhåll av ytfartyg och u-båtar. Att jobba hos oss innebär att du får jobba med morgondagens teknologi i en miljö som andas både nytänkande och världsarv. För att Saab ska kunna tillhöra världseliten, både på ytan och under, krävs framsynthet, fantasi och kreativitet. Kanske är just Du den vi söker.  Din framtida utmaning Som del i plåt/svets verkstaden på Muskö kommer dina arbetsuppgifter vara allt från nytillverkning av enstycksdetaljer, byte av inredning detaljer, samt många kontroller som ska göras tex ankarkätting, Livflotte arrangemang m.m. Även stor som små renovering och ombyggnationer av marinens ytfartyg,. varför det ställs krav på din förmåga att kunna ta egna initiativ och komma med egna kreativa lösningar. Du är en problemlösare som älskar att arbeta med teknik. På Muskö finns möjlighet att utveckla dina tekniska kunskaper då du kommer arbeta med kollegor som har hög kompetens. Arbetet är varierande eftersom det handlar om olika fartyg med olika komplexitet och ålder. Vill du utveckla dig själv och arbeta i en spännande miljö?   Resor både inrikes/utrikes kan förekomma.  Den du är idag Glad och positiv Kille/Tjej, som är ordningsam till avdelning stål/svets. Du är utbildad plåtslagare eller svetsare med några år i yrket och erfarenhet inom framförallt aluminium och rostfritt men även stål. Du bör ha erfarenhet och kunskap i metoderna TIG, MIG och MAG. Licenser är meriterande men inget krav. Din noggrannhet ska märkas i dina jobb, kvalitet är mycket viktigt och genom ett strukturerat arbetssätt, god planering och rätt prioritering kan du själv utvärdera och lämna ifrån dig dina jobb.  För att lyckas hos oss ser vi gärna att du har:  Du kan uttrycka dig i tal och skrift på svenska och på engelska samt innehar minst B-körkort.Samarbete är viktigt men också förmågan att kunna jobba självständigt. Du måste vara ansvarstagande, ordningsam och strukturerad, innehar minst B-körkort och god grundläggande datorvana.   Meriterande:  Om du har erfarenheter ifrån arbete med travers-, truck och mobil plattforms-arbete, tunglyft arbeten tex taljor. Bearbetat material med bland annat borrning, svarvning, fräsning samt gängning    Befattningen kräver att du genomgår och godkänns enligt vid var tid gällande bestämmelser för säkerhetsskydd. För befattningar där Saab har krav på säkerhetsklassinplacering kan, i förekommande fall, medföra på visst medborgarskap. Vänligen observera att vi arbetar med löpande urval och tjänsten kan komma att tillsättas innan sista ansökningsdatum har gått ut.  I denna rekrytering samarbetar Saab med Skill. Kontakta ansvarig rekryterare Nina Torebrink, 0455-10405 nina.torebrink@skill.se, Mikaela Samuelsson, 0455-365365, mikaela.samuelsson@skill.se  Om Saab  Saab är ett globalt försvars- och säkerhetsföretag verksamt inom flyg-, land- och marinförsvar, civil säkerhet och kommersiell flygteknik. Vi är 15 500 medarbetare och har verksamhet på alla kontinenter. Tekniskt är vi ledande inom många områden och en femtedel av våra intäkter går till forskning och utveckling. Saab är också ett möjligheternas företag. Ett företag där vi ser mångfald som en tillgång och där du som medarbetare får stort ansvar och goda utvecklingsmöjligheter. Men också ett företag som respekterar varje människas behov av ett liv utanför arbetet.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Svetsare**/Plåtslagare Svetsare/Plåtslag... | x | x | 8 | 8 | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| 2 | Svetsare/**Plåtslagare** Svetsare/Plåtslagare Muskö  P... | x | x | 11 | 11 | [Plåtslagare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/JezX_oUD_TZr) |
| 3 | Svetsare/Plåtslagare **Svetsare**/Plåtslagare Muskö  På Saab bl... | x | x | 8 | 8 | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| 4 | Svetsare/Plåtslagare Svetsare/**Plåtslagare** Muskö  På Saab blickar vi stä... | x | x | 11 | 11 | [Plåtslagare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/JezX_oUD_TZr) |
| 5 | ... nästan allt för att Marinens **fartyg** skall vara tillgängliga och k... | x | x | 6 | 6 | [Fartyg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dRFw_Rcu_dX7) |
| 6 | ...ller världens mest avancerade **smygteknik**. Andra framgångsrika produkte... |  | x |  | 10 | [smygteknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VHJn_ELx_H1v) |
| 7 | ...du får jobba med morgondagens **teknologi** i en miljö som andas både nyt... | x |  |  | 9 | [Teknologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KQjv_TUZ_Hy9) |
| 8 | ...mtida utmaning Som del i plåt/**svets** verkstaden på Muskö kommer di... | x | x | 5 | 5 | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| 9 | ... av enstycksdetaljer, byte av **inredning** detaljer, samt många kontroll... | x | x | 9 | 9 | [Inredning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/uAtM_95o_kaj) |
| 10 | ...eftersom det handlar om olika **fartyg** med olika komplexitet och åld... | x | x | 6 | 6 | [Fartyg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dRFw_Rcu_dX7) |
| 11 | ...rbeta i en spännande miljö?   **Resor** både inrikes/utrikes kan före... | x |  |  | 5 | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| 12 | ...   Resor både inrikes/utrikes **kan förekomma**.  Den du är idag Glad och pos... | x |  |  | 13 | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| 13 | ... är ordningsam till avdelning **stål**/svets. Du är utbildad plåtsla... |  | x |  | 4 | [Stålprodukter/Verkstadsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/dhQh_Xhd_NSF) |
| 14 | ...rdningsam till avdelning stål/**svets**. Du är utbildad plåtslagare e... | x | x | 5 | 5 | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| 15 | ...ng stål/svets. Du är utbildad **plåtslagare** eller svetsare med några år i... | x | x | 11 | 11 | [Plåtslagare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/JezX_oUD_TZr) |
| 16 | ...är utbildad plåtslagare eller **svetsare** med några år i yrket och erfa... | x | x | 8 | 8 | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| 17 | ...minium och rostfritt men även **stål**. Du bör ha erfarenhet och kun... |  | x |  | 4 | [Stålprodukter/Verkstadsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/dhQh_Xhd_NSF) |
| 18 | ...enhet och kunskap i metoderna **TIG**, MIG och MAG. Licenser är mer... | x |  |  | 3 | [TIG-svetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/WBnc_JXj_MvY) |
| 19 | ... och kunskap i metoderna TIG, **MIG** och MAG. Licenser är meritera... | x |  |  | 3 | [MIG-svetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q1D1_Pdc_rnT) |
| 20 | ...skap i metoderna TIG, MIG och **MAG**. Licenser är meriterande men ... | x |  |  | 3 | [MAG-svetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/NH9n_Snj_CNM) |
| 21 | ...i metoderna TIG, MIG och MAG. **Licenser** är meriterande men inget krav... | x |  |  | 8 | [Certifikat/licenser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/s48H_Sft_DTn) |
| 22 | ...riterande men inget krav. Din **noggrannhet** ska märkas i dina jobb, kvali... | x | x | 11 | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 23 | ...rycka dig i tal och skrift på **svenska** och på engelska samt innehar ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 24 | ... och skrift på svenska och på **engelska** samt innehar minst B-körkort.... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 25 | ...å engelska samt innehar minst **B-körkort**.Samarbete är viktigt men ocks... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 26 | ... men också förmågan att kunna **jobba självständigt**. Du måste vara ansvarstagande... | x |  |  | 19 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 27 | ... självständigt. Du måste vara **ansvarstagande**, ordningsam och strukturerad,... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 28 | ...h strukturerad, innehar minst **B-körkort** och god grundläggande datorva... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 29 | ...erfarenheter ifrån arbete med **travers**-, truck och mobil plattforms-... | x |  |  | 7 | [Travers/Lyftanordningar, **skill**](http://data.jobtechdev.se/taxonomy/concept/R6ZK_FGC_ovb) |
| 30 | ...er ifrån arbete med travers-, **truck** och mobil plattforms-arbete, ... | x | x | 5 | 5 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 31 | ...etat material med bland annat **borrning**, svarvning, fräsning samt gän... | x | x | 8 | 8 | [Borrning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZuMC_5GQ_9jw) |
| 32 | ...ial med bland annat borrning, **svarvning**, fräsning samt gängning    Be... | x |  |  | 9 | [Svarvning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/EGCS_78o_x3F) |
| 32 | ...ial med bland annat borrning, **svarvning**, fräsning samt gängning    Be... |  | x |  | 9 | [Svarvning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/d2yX_cZM_fSy) |
| 33 | ...nd annat borrning, svarvning, **fräsning** samt gängning    Befattningen... | x |  |  | 8 | [Fräsning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZqZp_rib_2PF) |
| 33 | ...nd annat borrning, svarvning, **fräsning** samt gängning    Befattningen... |  | x |  | 8 | [Fräsning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/wpVJ_UXf_uSb) |
| 34 | ...ing, svarvning, fräsning samt **gängning**    Befattningen kräver att du... | x |  |  | 8 | [sköta gängningsmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/oRGL_Kzb_G83) |
| 35 | ... med Skill. Kontakta ansvarig **rekryterare** Nina Torebrink, 0455-10405 ni... | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| 36 | ...ivil säkerhet och kommersiell **flygteknik**. Vi är 15 500 medarbetare och... | x | x | 10 | 10 | [Flygteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/23tt_fXS_N7o) |
| | **Overall** | | | **169** | **310** | 169/310 = **55%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Flygteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/23tt_fXS_N7o) |
| x |  |  | [Svarvning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/EGCS_78o_x3F) |
| x | x | x | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| x | x | x | [Plåtslagare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/JezX_oUD_TZr) |
| x |  |  | [Teknologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KQjv_TUZ_Hy9) |
| x |  |  | [MAG-svetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/NH9n_Snj_CNM) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [MIG-svetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q1D1_Pdc_rnT) |
| x |  |  | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| x |  |  | [Travers/Lyftanordningar, **skill**](http://data.jobtechdev.se/taxonomy/concept/R6ZK_FGC_ovb) |
|  | x |  | [smygteknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VHJn_ELx_H1v) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [TIG-svetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/WBnc_JXj_MvY) |
| x | x | x | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x |  |  | [Fräsning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZqZp_rib_2PF) |
| x | x | x | [Borrning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZuMC_5GQ_9jw) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Svarvning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/d2yX_cZM_fSy) |
| x | x | x | [Fartyg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dRFw_Rcu_dX7) |
|  | x |  | [Stålprodukter/Verkstadsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/dhQh_Xhd_NSF) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [sköta gängningsmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/oRGL_Kzb_G83) |
| x |  |  | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| x | x | x | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| x |  |  | [Certifikat/licenser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/s48H_Sft_DTn) |
| x | x | x | [Inredning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/uAtM_95o_kaj) |
|  | x |  | [Fräsning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/wpVJ_UXf_uSb) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **13** | 13/29 = **45%** |