# Results for '4d54c6089fece6364e969a6d95943d2336d94bbd'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4d54c6089fece6364e969a6d95943d2336d94bbd](README.md) | 1 | 933 | 15 | 11 | 55/134 = **41%** | 7/13 = **54%** |

## Source text

Cafébiträde/Barista sökes  till Café Nova i Gamla Stan Mysigt och charmigt café på Järntorget i Gamla Stan som är känd för sin vackra och mysiga interiör tillsammans med ett fantastiskt utbud av hembakat samt varma rätter. Detta tillsammans med ett riktigt välsmakande espressokaffe, en kall öl eller ett glas vin. Balkong- och torgserveringen tillsammans med läget sprider en medelhavskänsla. Vi söker dig som är serviceinriktad och glad person som håller humöret uppe vid de stressigaste tillfällena. Du ska ha minst ett års erfarenhet från café-/restaurangbranschen och verkligen vara intresserad av branschen. Erfarenhet inom kallskänk är ett plus. Stresstålig, Positiv, Snabb , Engagerad och Social. I övrigt gäller sedvanliga arbetsuppgifter inom café. Hantera kund, kassa samt servering och ge en bra service och klara av ett högt tempo. Hantera espressomaskin, servera öl och vin. För rätt person finns möjlighet till heltid.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Café**biträde/Barista sökes  till Ca... |  | x |  | 4 | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| 2 | **Cafébiträde**/Barista sökes  till Café Nova... | x | x | 11 | 11 | [Cafébiträde, **job-title**](http://data.jobtechdev.se/taxonomy/concept/qZG4_pVE_ZYZ) |
| 3 | Cafébiträde/**Barista** sökes  till Café Nova i Gamla... | x | x | 7 | 7 | [Barista/Kaffebartender, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QREG_zsK_FLo) |
| 4 | ...fébiträde/Barista sökes  till **Café** Nova i Gamla Stan Mysigt och ... | x |  |  | 4 | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| 5 | ...amla Stan Mysigt och charmigt **café** på Järntorget i Gamla Stan so... | x | x | 4 | 4 | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| 6 | ...fe, en kall öl eller ett glas **vin**. Balkong- och torgserveringen... | x | x | 3 | 3 | [Sprit/Vin, **skill**](http://data.jobtechdev.se/taxonomy/concept/S4vL_AVu_s6q) |
| 7 | ... tillfällena. Du ska ha minst **ett års erfarenhet** från café-/restaurangbransche... | x |  |  | 18 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| 8 | ...minst ett års erfarenhet från **café**-/restaurangbranschen och verk... | x |  |  | 4 | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| 9 | ...ett års erfarenhet från café-/**restaurangbranschen** och verkligen vara intressera... | x | x | 19 | 19 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 10 | ...av branschen. Erfarenhet inom **kallskänk** är ett plus. Stresstålig, Pos... | x |  |  | 9 | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| 11 | ...t inom kallskänk är ett plus. **Stresstålig**, Positiv, Snabb , Engagerad o... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 12 | ...dvanliga arbetsuppgifter inom **café**. Hantera kund, kassa samt ser... | x | x | 4 | 4 | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| 13 | ...fter inom café. Hantera kund, **kassa** samt servering och ge en bra ... | x | x | 5 | 5 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 14 | ...afé. Hantera kund, kassa samt **servering** och ge en bra service och kla... | x |  |  | 9 | [Restaurang, servering, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GqXD_ArB_Mmg) |
| 15 | ...empo. Hantera espressomaskin, **servera **öl och vin. För rätt person fi... |  | x |  | 8 | [servera öl, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fp3h_n1j_Pgs) |
| 16 | ...ntera espressomaskin, servera **öl** och vin. För rätt person finn... | x | x | 2 | 2 | [servera öl, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fp3h_n1j_Pgs) |
| 17 | ...spressomaskin, servera öl och **vin**. För rätt person finns möjlig... |  | x |  | 3 | [Sprit/Vin, **skill**](http://data.jobtechdev.se/taxonomy/concept/S4vL_AVu_s6q) |
| 17 | ...spressomaskin, servera öl och **vin**. För rätt person finns möjlig... | x |  |  | 3 | [servera viner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r55f_US8_Kt9) |
| 18 | ...t person finns möjlighet till **heltid**. | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **55** | **134** | 55/134 = **41%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Restaurang, servering, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GqXD_ArB_Mmg) |
| x |  |  | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| x |  |  | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| x | x | x | [Barista/Kaffebartender, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QREG_zsK_FLo) |
| x | x | x | [Sprit/Vin, **skill**](http://data.jobtechdev.se/taxonomy/concept/S4vL_AVu_s6q) |
| x | x | x | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x | x | x | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| x | x | x | [servera öl, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fp3h_n1j_Pgs) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Cafébiträde, **job-title**](http://data.jobtechdev.se/taxonomy/concept/qZG4_pVE_ZYZ) |
| x |  |  | [servera viner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r55f_US8_Kt9) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **7** | 7/13 = **54%** |