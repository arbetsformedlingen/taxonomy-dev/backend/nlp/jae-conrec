# Results for 'aad3bce95218a0df94c5707b99edfe9ed5c4a8ef'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [aad3bce95218a0df94c5707b99edfe9ed5c4a8ef](README.md) | 1 | 4482 | 33 | 33 | 149/578 = **26%** | 9/32 = **28%** |

## Source text

Projektledare till samhällsbyggnadskontoret Partille kommun har idag nära 40 000 invånare och är en förstad i utveckling. Det är upp till oss 2 900 anställda att leverera kvalitet och service som gör det gott att växa upp, arbeta och leva i Partille. Detta med grund i våra värdeord: allas lika värde, professionalitet och framåtanda.  Partille, med sin närhet till Göteborg, fortsätter att avancera från förort till förstad. Vi arbetar beslutsamt mot att bli en ekonomiskt, ekologiskt och socialt hållbar kommun. Vi har korta beslutsvägar, här uppmuntras samarbete över gränserna och du som medarbete ges utrymme att växa. Vårt arbete är varierat, utmanande och komplext. Kort sagt, hos oss blir det aldrig långtråkigt.  Samhällsbyggnadskontoret (SBK) består av tekniska avdelningen, planeringsenheten och mark- och exploateringsenheten. Tekniska avdelningen består i sin tur av fyra enheter: Gata och park, VA, Trafik samt Projekt & Avfall. Hos oss finner du en mängd spännande och intressanta arbetsuppgifter, där du utvecklar och förvaltar infrastruktur och bidrar till att skapa en tillgänglig kommun. Just nu planeras det för en ny stadsdel med 500 bostäder och ett nytt centrum i Öjersjö. Det planeras även för utvidgning av Partille centrum, söder om E20, som en del i arbetet att ta Partille från förort till förstad. Parallellt med dessa projekt pågår även ett antal upprustningar av kommunens befintliga infrastruktur.   ARBETSUPPGIFTER Vill du vara delaktig i att förverkliga uppdrag inom samhällsbyggnadsområdet i Partille kommun? Nu söker vi en projektledare till projekt- och avfallsenheten inom samhällsbyggnadskontoret. Partille är en expansiv kommun och det pågår flera spännande projekt som du kommer att vara direkt involverad i. Hos oss får du möjlighet att vara med och påverka samhällsutvecklingen i Partille kommun.  Som projektledare hos oss ansvarar du för att leda och engagera projektgrupper bestående av interna och externa resurser mot gemensamma mål. Projekten är av varierande storlek och komplexitet men består oftast i ny- eller ombyggnad av vägar, broar, VA-anläggningar, parker med mera. På enheten pågår ett spännande utvecklingsarbete kopplat till vår målsättning att bli en framgångsrik projektorganisation inom samhällsbyggnad.   I ditt uppdrag ingår bland annat att:  - Ta fram projektplaner och föreslå resurssättning  - Uppföljning av projekten kopplat till ekonomi, budget och prognoser  - Ansvara för att projektet når mål om tid, kostnad och innehåll  - Samordna arbetsmiljöfrågor och riskhantering samt för löpande uppföljning av kravefterlevnad  - Granska handlingar och genomföra besiktningar  - Planera och genomföra upphandlingar enligt LOU - Bevaka Partille kommuns intressen vid val av tekniska lösningar   Var med och bygg Partille tillsammans med oss!   KVALIFIKATIONER Till denna tjänst söker vi dig som har:  - Civilingenjörsexamen inom anläggning/samhällsbyggnad eller annan utbildning som arbetsgivaren bedömer som likvärdig.   - Flerårig erfarenhet som projektledare, byggledare, uppdragsledare eller platschef där du har ansvarat för bland annat budget, prognoser och ekonomisk uppföljning.  - God kontrakts- och förhandlingsvana - Erfarenhet av infrastrukturprojekt såsom gata/väg, VA, parker, broar/tunnlar, torgytor men även andra typer av markarbeten såsom markstabilisering eller sanering och/eller bygg- och anläggningsprojekt.  - God förmåga att uttrycka dig i tal och skrift - Körkort B  Vidare ser vi att du har goda kunskaper inom:  - Entreprenadjuridik (AB04) och konsultjuridik (ABK09) - AMA Anläggning 20 och MER Anläggning 20 - AutoCAD  Som person besitter du goda ledaregenskaper samt har god förmåga att bygga relationer och samarbeta med andra. I rollen som projektledare kommer du att ha många kontakter, både internt och externt, varför vi letar efter dig som är en god kommunikatör. Stor vikt kommer att läggas vid personlig lämplighet.  Välkommen med din ansökan!   ÖVRIGT Urval och intervjuer sker löpande under ansökningstiden.  Vi eftersträvar mångfald bland personalen avseende kön, ålder och kulturell/etnisk bakgrund.  För att kvalitetssäkra rekryteringsprocessen samt möjliggöra god kommunikation med våra sökande ber vi dig ansöka online via länk i annonsen och inte via e-post eller i pappersformat.  Inför rekryteringsarbetet har vi tagit ställning till rekryteringskanaler och marknadsföring. Vi undanbeder oss därför bestämt kontakt med mediesäljare, rekryteringssajter och liknande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Projektledare** till samhällsbyggnadskontoret... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 1 | **Projektledare** till samhällsbyggnadskontoret... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 2 | ...till samhällsbyggnadskontoret **Partille** kommun har idag nära 40 000 i... | x | x | 8 | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 3 | ...t växa upp, arbeta och leva i **Partille**. Detta med grund i våra värde... | x | x | 8 | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 4 | ...essionalitet och framåtanda.  **Partille**, med sin närhet till Göteborg... | x | x | 8 | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 5 | ...Partille, med sin närhet till **Göteborg**, fortsätter att avancera från... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 6 | ...aneras även för utvidgning av **Partille** centrum, söder om E20, som en... | x | x | 8 | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 7 | ..., som en del i arbetet att ta **Partille** från förort till förstad. Par... | x |  |  | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 8 | ...nom samhällsbyggnadsområdet i **Partille** kommun? Nu söker vi en projek... | x | x | 8 | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 9 | ...rtille kommun? Nu söker vi en **projektledare** till projekt- och avfallsenhe... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 10 | ...nom samhällsbyggnadskontoret. **Partille** är en expansiv kommun och det... |  | x |  | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 11 | ...åverka samhällsutvecklingen i **Partille** kommun.  Som projektledare ho... | x | x | 8 | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 12 | ...ingen i Partille kommun.  Som **projektledare** hos oss ansvarar du för att l... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 12 | ...ingen i Partille kommun.  Som **projektledare** hos oss ansvarar du för att l... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 13 | ...å enheten pågår ett spännande **utvecklingsarbete** kopplat till vår målsättning ... |  | x |  | 17 | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| 14 | ...tning att bli en framgångsrik **projektorganisation** inom samhällsbyggnad.   I dit... |  | x |  | 19 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ...srik projektorganisation inom **samhällsbyggnad**.   I ditt uppdrag ingår bland... |  | x |  | 15 | [Samhällsbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/KEPE_tsb_fB8) |
| 16 | ...ing av projekten kopplat till **ekonomi**, budget och prognoser  - Ansv... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 17 | ...amordna arbetsmiljöfrågor och **riskhantering** samt för löpande uppföljning ... |  | x |  | 13 | [Risk management, **skill**](http://data.jobtechdev.se/taxonomy/concept/bHQT_Bhn_w3S) |
| 17 | ...amordna arbetsmiljöfrågor och **riskhantering** samt för löpande uppföljning ... |  | x |  | 13 | [riskhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nqH4_69a_CHG) |
| 18 | ...andlingar enligt LOU - Bevaka **Partille** kommuns intressen vid val av ... |  | x |  | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 19 | ... lösningar   Var med och bygg **Partille** tillsammans med oss!   KVAL... |  | x |  | 8 | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| 20 | ...änst söker vi dig som har:  - **Civilingenjörsexamen** inom anläggning/samhällsbyggn... | x | x | 20 | 20 | [Civilingenjörsexamen, **skill**](http://data.jobtechdev.se/taxonomy/concept/ND2K_ivB_6RA) |
| 21 | ...  - Civilingenjörsexamen inom **anläggning**/samhällsbyggnad eller annan u... | x |  |  | 10 | [Anläggningsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/oPP6_byU_yyn) |
| 22 | ...genjörsexamen inom anläggning/**samhällsbyggnad** eller annan utbildning som ar... | x | x | 15 | 15 | [Samhällsbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/KEPE_tsb_fB8) |
| 23 | ...en bedömer som likvärdig.   - **Flerårig erfarenhet** som projektledare, byggledare... | x |  |  | 19 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 24 | ....   - Flerårig erfarenhet som **projektledare**, byggledare, uppdragsledare e... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 24 | ....   - Flerårig erfarenhet som **projektledare**, byggledare, uppdragsledare e... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 25 | ...erfarenhet som projektledare, **byggledare**, uppdragsledare eller platsch... | x | x | 10 | 10 | [Byggledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Ys2U_uQv_V4n) |
| 26 | ...gledare, uppdragsledare eller **platschef** där du har ansvarat för bland... |  | x |  | 9 | [platschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/7ffE_ZTJ_Mix) |
| 26 | ...gledare, uppdragsledare eller **platschef** där du har ansvarat för bland... | x |  |  | 9 | [Platschef, bygg, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NZcr_uB1_6rX) |
| 26 | ...gledare, uppdragsledare eller **platschef** där du har ansvarat för bland... |  | x |  | 9 | [Platschef, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fCCr_Mp8_6oE) |
| 27 | ... har ansvarat för bland annat **budget**, prognoser och ekonomisk uppf... | x |  |  | 6 | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| 28 | ...varat för bland annat budget, **prognoser** och ekonomisk uppföljning.  -... | x |  |  | 9 | [Ekonomiska prognoser, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y4Fw_JZQ_AcA) |
| 29 | ...d annat budget, prognoser och **ekonomisk uppföljning**.  - God kontrakts- och förhan... | x |  |  | 21 | [Ekonomisk rapportering, **skill**](http://data.jobtechdev.se/taxonomy/concept/WNnT_qrt_M77) |
| 30 | ...ekonomisk uppföljning.  - God **kontrakts**- och förhandlingsvana - Erfar... | x |  |  | 9 | [Kontraktsskrivning, **skill**](http://data.jobtechdev.se/taxonomy/concept/qhUT_4Gs_bY7) |
| 31 | ...ljning.  - God kontrakts- och **förhandlingsvana** - Erfarenhet av infrastruktur... | x | x | 16 | 16 | [Förhandlingsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/q7Mx_gjU_F9q) |
| 32 | ...gytor men även andra typer av **markarbeten** såsom markstabilisering eller... |  | x |  | 11 | [Anläggningsarbeten, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/pSzC_2YX_dkE) |
| 33 | ...gsprojekt.  - God förmåga att **uttrycka dig i tal och skrift** - Körkort B  Vidare ser vi at... | x |  |  | 29 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 34 | ...trycka dig i tal och skrift - **Körkort** B  Vidare ser vi att du har g... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 35 | ...dig i tal och skrift - Körkort** B**  Vidare ser vi att du har god... | x |  |  | 2 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 36 | ...u har goda kunskaper inom:  - **Entreprenadjuridik** (AB04) och konsultjuridik (AB... | x | x | 18 | 18 | [Entreprenadjuridik, **skill**](http://data.jobtechdev.se/taxonomy/concept/nF6J_MNc_ZxS) |
| 37 | ...Entreprenadjuridik (AB04) och **konsultjuridik** (ABK09) - AMA Anläggning 20 o... | x |  |  | 14 | [juridik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DtTv_E8V_YhP) |
| 38 | ...ng 20 och MER Anläggning 20 - **AutoCAD**  Som person besitter du goda ... | x | x | 7 | 7 | [Allmänkonstruktion-AutoCAD, **skill**](http://data.jobtechdev.se/taxonomy/concept/K5JR_yfk_8oj) |
| 39 | ...  Som person besitter du goda **ledaregenskaper** samt har god förmåga att bygg... | x |  |  | 15 | [utöva ledarskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DP6Z_cdB_jKo) |
| 40 | ...aper samt har god förmåga att **bygga relationer** och samarbeta med andra. I ro... | x |  |  | 16 | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| 41 | ...måga att bygga relationer och **samarbeta** med andra. I rollen som proje... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 42 | ...rbeta med andra. I rollen som **projektledare** kommer du att ha många kontak... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 43 | ...letar efter dig som är en god **kommunikatör**. Stor vikt kommer att läggas ... |  | x |  | 12 | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| | **Overall** | | | **149** | **578** | 149/578 = **26%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [platschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/7ffE_ZTJ_Mix) |
| x | x | x | [Partille, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCiR_sXa_BVW) |
| x |  |  | [utöva ledarskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DP6Z_cdB_jKo) |
| x |  |  | [juridik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DtTv_E8V_YhP) |
| x |  |  | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| x |  |  | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| x | x | x | [Allmänkonstruktion-AutoCAD, **skill**](http://data.jobtechdev.se/taxonomy/concept/K5JR_yfk_8oj) |
| x | x | x | [Samhällsbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/KEPE_tsb_fB8) |
|  | x |  | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| x | x | x | [Civilingenjörsexamen, **skill**](http://data.jobtechdev.se/taxonomy/concept/ND2K_ivB_6RA) |
| x |  |  | [Platschef, bygg, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NZcr_uB1_6rX) |
|  | x |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Ekonomisk rapportering, **skill**](http://data.jobtechdev.se/taxonomy/concept/WNnT_qrt_M77) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Ekonomiska prognoser, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y4Fw_JZQ_AcA) |
| x | x | x | [Byggledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Ys2U_uQv_V4n) |
|  | x |  | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
|  | x |  | [Risk management, **skill**](http://data.jobtechdev.se/taxonomy/concept/bHQT_Bhn_w3S) |
|  | x |  | [Platschef, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fCCr_Mp8_6oE) |
| x | x | x | [Entreprenadjuridik, **skill**](http://data.jobtechdev.se/taxonomy/concept/nF6J_MNc_ZxS) |
|  | x |  | [riskhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nqH4_69a_CHG) |
|  | x |  | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| x |  |  | [Anläggningsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/oPP6_byU_yyn) |
|  | x |  | [Anläggningsarbeten, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/pSzC_2YX_dkE) |
| x |  |  | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| x | x | x | [Förhandlingsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/q7Mx_gjU_F9q) |
| x |  |  | [Kontraktsskrivning, **skill**](http://data.jobtechdev.se/taxonomy/concept/qhUT_4Gs_bY7) |
| | | **9** | 9/32 = **28%** |