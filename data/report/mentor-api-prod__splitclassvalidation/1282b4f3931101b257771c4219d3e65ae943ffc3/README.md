# Results for '1282b4f3931101b257771c4219d3e65ae943ffc3'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1282b4f3931101b257771c4219d3e65ae943ffc3](README.md) | 1 | 732 | 6 | 6 | 71/93 = **76%** | 4/6 = **67%** |

## Source text

Specialistläkare, Bemanning Jämför och chatta med bemanningsbolagen enkelt via Vårdföretag App (vardforetag.se) Skapa din profil på mindre än 1 minut och lägg upp dina önskemål. Få erbjudanden på uppdrag som kan passa dina önskemål och chatta med flera av de ledande bemanningsbolagen inom vården. Enkelt och kostnadsfritt! Just nu finns ett ledigt uppdrag som specialistläkare på vårdcentral i Göteborg, Västra Götalands län. Uppdraget finns att söka bland flera bemanningsbolag i Vårdföretag.se   Fördelar: -Hög ersättning -Inga onödiga Mail Eller telefonsamtal -Enkelt och kostnadsfritt -Inget bindande   Vill du starta eget aktiebolag? Vi på vardforetag.se hjälper dig att starta aktiebolag och redovisning till fasta kostnader.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Specialistläkare**, Bemanning Jämför och chatta ... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 2 | ... finns ett ledigt uppdrag som **specialistläkare** på vårdcentral i Göteborg, Vä... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 3 | ...pdrag som specialistläkare på **vårdcentral** i Göteborg, Västra Götalands ... | x | x | 11 | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 4 | ...ialistläkare på vårdcentral i **Göteborg**, Västra Götalands län. Uppdra... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 5 | ...re på vårdcentral i Göteborg, **Västra Götalands län**. Uppdraget finns att söka bla... | x | x | 20 | 20 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 6 | ...dig att starta aktiebolag och **redovisning** till fasta kostnader. |  | x |  | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| 6 | ...dig att starta aktiebolag och **redovisning** till fasta kostnader. | x |  |  | 11 | [redovisning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aAbn_jKY_u2g) |
| | **Overall** | | | **71** | **93** | 71/93 = **76%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
|  | x |  | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x |  |  | [redovisning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aAbn_jKY_u2g) |
| x | x | x | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| x | x | x | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| | | **4** | 4/6 = **67%** |