# Results for '849cd89027907917be5f16c8c34491badc7af0e9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [849cd89027907917be5f16c8c34491badc7af0e9](README.md) | 1 | 4608 | 30 | 18 | 108/329 = **33%** | 7/20 = **35%** |

## Source text

Vi söker vikarier till kök och städ! Vill du vara med och skapa framtidens Karlstad? Hos oss i koncernen Karlstads kommun kan du göra skillnad på riktigt. Här är varje medarbetare en viktig del i helheten och tillsammans hjälps vi åt att förverkliga vår vision om "Ett bättre liv i Solstaden". Kom och väx med oss!   Barn- och ungdomsförvaltningen ansvarar för förskola och skola för cirka 13 200 barn och elever och är kommunens största förvaltning med drygt 2 400 medarbetare. Vi har förskolor, familjedaghem, öppna förskolor, förskoleklasser, grundskolor, grundsärskolor, fritidshem och kulturskola och arbetar även med barn- och elevhälsofrågor. Vi rustar barnen för framtiden.    Beskrivning Har du ett intresse för att laga mat, gillar ordning och reda, samt tycker om att ge god service? Vad bra då är det kanske dig vi letar efter! Vi söker nu vikarier till vår vikariepool inom kök och städ i förskola och skola, där du får ett meningsfullt arbete med ett stort kontaktnät.  Som vikarie är du viktig för våra yngsta medborgare för att de ska får en trivsam vistelse i våra skolor och förskolor.  I vikariepoolen ersätter du ordinarie personal vid behov, och är därför en viktig pusselbit för att våra verksamheter ska fungera bra. Ibland ersätter du vid enstaka arbetspass med kort varsel och andra gånger under en längre tid.     Arbetsuppgifter I vikariepoolen blir du anställd som kock, köksbiträde eller ekonomibiträde (städ).   - Som kock ansvarar du för matlagning, salladsberedning, packning, diskning/städning och egenkontroller i skolköken. För att arbeta som kock behöver du ha en relevant utbildning och dokumenterad erfarenhet från arbete inom kök.  - Som köksbiträde hjälper du till vid tillagning av frukost, lunch och mellanmål i skolköken. Du ansvarar även över disk, städning i kök och matsal, servering av måltiderna och är delaktig i egenkontroller. Inom äldreomsorgen ingår även kassahantering. För att arbeta som köksbiträde behöver du ha relevant utbildning och/eller dokumenterad erfarenhet från arbete i kök.  - Som ekonomibiträde (städ) har du en kombinationstjänst mellan städning och enklare hantering av mat. Du arbetar huvudsakligen med att hålla rent på våra förskolor men även i ett mottagningskök där du förbereder frukost, lunch och mellanmål för servering. Utöver detta ansvarar du även för disk, beställning av varor samt är delaktig i egenkontroller. Det är meriterande om du har en städutbildning och/eller har dokumenterad erfarenhet från liknande arbete.     Kvalifikationer Vi söker dig som har fyllt 18 år och behärskar det svenska språket i både skrift och tal.  För att lyckas i rollen ser vi att du är serviceinriktad, flexibel i ditt förhållningssätt då du ofta träffar nya personal- och barngrupper, självgående, ordningsam och strukturerad och tar ansvar för dina arbetsuppgifter. Vi lägger stor vikt vid personliga egenskaper och intresse för tjänsten.   Som vikarie behöver du kunna arbeta minst två dagar i veckan vilket även kan innebära arbete på helgerna och dessutom kunna arbeta inom hela Karlstads kommun inklusive våra ytterområden (Molkom, Vålberg, Edsvalla, Väse och Skattkärr).  Innan vi kan erbjuda dig en anställning begär vi att få se ett utdrag från belastningsregistret. Vi rekommenderar att du beställer utdraget i samband att du skickar in din ansökan. Du beställer utdraget här: https://polisen.se/tjanster-tillstand/belastningsregistret/skola-eller-forskola/    Vad kan vi erbjuda dig? Som anställd i barn- och ungdomsförvaltningen får du möjlighet att göra våra barn och elevers vardag bättre. Att arbeta i vikariepoolen innebär att du styr själv över när du är tillgänglig för arbete, vilket möjliggör en balans mellan fritid och arbete. Du får anställning hos en trygg arbetsgivare där alla anställda omfattas av kollektivavtal. Om du jobbar i vikariepoolen en längre period finns goda möjligheter till längre vikariat och fasta tjänster.   Utöver detta erbjuds du som arbetar i vikariepoolen förmånen att under 2022 träna och simma helt gratis på kommunens eget friskvårdscenter Haga motion och på Sundstabadet. Bra va?!   För att kvalitetssäkra rekryteringsprocessen och förenkla kommunikationen med våra sökande ber vi dig skicka in din ansökan digitalt och inte via e-post eller pappersformat. Vi undanber oss alla erbjudanden om annonserings- och rekryteringshjälp i samband med denna annons.   Har du skyddade personuppgifter ska du inte registrera dina ansökningshandlingar i vårt rekryteringssystem och inte heller skicka in dem via e-post. Vänd dig istället till kommunens växel 054-540 00 00 för vidare hantering.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Vi söker **vikarier** till kök och städ! Vill du va... | x |  |  | 8 | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| 2 | ...i söker vikarier till kök och **städ**! Vill du vara med och skapa f... | x |  |  | 4 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 3 | ...vara med och skapa framtidens **Karlstad**? Hos oss i koncernen Karlstad... | x | x | 8 | 8 | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| 4 | ...Karlstad? Hos oss i koncernen **Karlstads kommun** kan du göra skillnad på rikti... | x |  |  | 16 | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| 5 | ...omsförvaltningen ansvarar för **förskola** och skola för cirka 13 200 ba... | x | x | 8 | 8 | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| 6 | ... grundskolor, grundsärskolor, **fritidshem** och kulturskola och arbetar ä... | x | x | 10 | 10 | [Fritidshem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Aq3e_Bdv_v45) |
| 7 | ...kariepool inom kök och städ i **förskola** och skola, där du får ett men... | x | x | 8 | 8 | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| 8 | ...epoolen ersätter du ordinarie **personal** vid behov, och är därför en v... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 9 | ...iepoolen blir du anställd som **kock**, köksbiträde eller ekonomibit... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 10 | ...en blir du anställd som kock, **köksbiträde** eller ekonomibiträde (städ). ... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 11 | ...d som kock, köksbiträde eller **ekonomibiträde** (städ).   - Som kock ansvarar... | x | x | 14 | 14 | [Ekonomibiträde, storhushåll, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Kf29_nju_Xd4) |
| 12 | ...biträde eller ekonomibiträde (**städ**).   - Som kock ansvarar du fö... | x |  |  | 4 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 13 | ...konomibiträde (städ).   - Som **kock** ansvarar du för matlagning, s... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 14 | ...k ansvarar du för matlagning, **salladsberedning**, packning, diskning/städning ... | x | x | 16 | 16 | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| 15 | ..., salladsberedning, packning, **diskning**/städning och egenkontroller i... | x |  |  | 8 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 15 | ..., salladsberedning, packning, **diskning**/städning och egenkontroller i... |  | x |  | 8 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| 16 | ...beredning, packning, diskning/**städning** och egenkontroller i skolköke... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 16 | ...beredning, packning, diskning/**städning** och egenkontroller i skolköke... | x |  |  | 8 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 17 | ...skolköken. För att arbeta som **kock** behöver du ha en relevant utb... | x |  |  | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 18 | ... från arbete inom kök.  - Som **köksbiträde** hjälper du till vid tillagnin... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 19 | ...lköken. Du ansvarar även över **disk**, städning i kök och matsal, s... | x |  |  | 4 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 20 | .... Du ansvarar även över disk, **städning** i kök och matsal, servering a... | x |  |  | 8 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 21 | ...laktig i egenkontroller. Inom **äldreomsorgen** ingår även kassahantering. Fö... | x |  |  | 13 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 22 | ...Inom äldreomsorgen ingår även **kassahantering**. För att arbeta som köksbiträ... | x |  |  | 14 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 23 | ...hantering. För att arbeta som **köksbiträde** behöver du ha relevant utbild... | x |  |  | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 24 | ...het från arbete i kök.  - Som **ekonomibiträde** (städ) har du en kombinations... | x | x | 14 | 14 | [Ekonomibiträde, storhushåll, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Kf29_nju_Xd4) |
| 25 | ...i kök.  - Som ekonomibiträde (**städ**) har du en kombinationstjänst... | x |  |  | 4 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 26 | ... en kombinationstjänst mellan **städning** och enklare hantering av mat.... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 26 | ... en kombinationstjänst mellan **städning** och enklare hantering av mat.... | x |  |  | 8 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 27 | ...er detta ansvarar du även för **disk**, beställning av varor samt är... | x |  |  | 4 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 28 | ...fyllt 18 år och behärskar det **svenska** språket i både skrift och tal... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 29 | ...fyllt 18 år och behärskar det **svenska språket** i både skrift och tal.  För a... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 30 | ...gssätt då du ofta träffar nya **personal**- och barngrupper, självgående... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 31 | ...ssutom kunna arbeta inom hela **Karlstads kommun** inklusive våra ytterområden (... | x |  |  | 16 | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| 32 | ...är alla anställda omfattas av **kollektivavtal**. Om du jobbar i vikariepoolen... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 33 | ...ånen att under 2022 träna och **simma** helt gratis på kommunens eget... |  | x |  | 5 | [simma, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eqz3_Ffb_Ybf) |
| 34 | ...ns eget friskvårdscenter Haga **motion** och på Sundstabadet. Bra va?!... |  | x |  | 6 | [agera med motion capture-utrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r3Jg_Qvy_5vd) |
| | **Overall** | | | **108** | **329** | 108/329 = **33%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x | x | x | [Fritidshem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Aq3e_Bdv_v45) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x |  |  | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x | x | x | [Ekonomibiträde, storhushåll, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Kf29_nju_Xd4) |
| x | x | x | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x |  |  | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x |  |  | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
|  | x |  | [simma, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eqz3_Ffb_Ybf) |
| x |  |  | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| x | x | x | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| x | x | x | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| x | x | x | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
|  | x |  | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| x | x | x | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
|  | x |  | [agera med motion capture-utrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r3Jg_Qvy_5vd) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/20 = **35%** |