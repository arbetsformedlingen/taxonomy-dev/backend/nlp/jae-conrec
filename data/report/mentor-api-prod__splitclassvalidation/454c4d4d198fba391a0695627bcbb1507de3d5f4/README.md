# Results for '454c4d4d198fba391a0695627bcbb1507de3d5f4'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [454c4d4d198fba391a0695627bcbb1507de3d5f4](README.md) | 1 | 2991 | 23 | 21 | 93/561 = **17%** | 5/23 = **22%** |

## Source text

Byggnadsantikvarie Länsstyrelsen i Dalarnas län är en statlig myndighet som finns nära människorna i vårt län. Vi är en viktig länk mellan länet och regeringen, riksdagen och andra myndigheter. Vår uppgift är att säkerställa att nationella mål och uppdrag genomförs regionalt och med hänsyn till förutsättningarna i just vårt län. Vårt arbete bygger på en samsyn kring mål och prioriteringar och utgår från de fyra ledorden rättssäkerhet, kunskap, samarbete och bra bemötande.  Länsstyrelsen söker ny medarbetare till enheten för kulturmiljö och samhällsplanering med god erfarenhet av arbete med byggnadsvård och kulturmiljöer.   ARBETSUPPGIFTER Du kommer att i nära samarbete med våra övriga kulturmiljövårdare arbeta med Dalarnas kulturhistoriska byggnader, bebyggelsemiljöer och kulturlandskap utifrån gällande lagstiftning och nationella och regionala mål och strategier. Du kommer att arbeta tvärsektoriellt i olika samhällsfrågor med andra sakområden på myndigheten.  Som byggnadsantikvarie kommer du att: •handlägga frågor om kyrkor, kyrkliga miljöer, kulturhistoriskt värdefull bebyggelse, kulturlandskap samt bidrag •företräda kulturmiljöfrågor i samråd om samhällsplanerings- och i utvecklingsfrågor •ta fram kunskapsunderlag kopplade till kulturhistorisk bebyggelse och kulturmiljöer.  KVALIFIKATIONER Krav •Du har högskoleutbildning med bebyggelseantikvarisk inriktning.  •Du har arbetslivserfarenhet av byggnadsantikvariskt arbete. •Du har goda kunskaper om kulturmiljölagen och aktuella delar av miljöbalken och plan- och bygglagen. •Du har B-körkort  Meriterande •Du har arbetserfarenhet av ärendehandläggning av kulturhistorisk bebyggelse och kulturlandskap, byggnadsvård och samhällsplanering. •Du har erfarenhet av att utarbeta kunskapsunderlag och/eller kulturmiljöprogram för den fysiska samhällsplaneringen. •Du har annan relevant högskoleutbildning, som kulturgeografi, kulturmiljövetenskap, kulturminnesvård, samhällsplanering, kyrkohistoria, m.m.  Personliga egenskaper •Du ska ha lätt för att uttrycka dig i tal och skrift på svenska.  •Du kommer att ingå i en arbetsgrupp och bidra till gruppens samlade arbete.  •Du behöver ha ett gott omdöme, vara strukturerad och serviceinriktad, ha god samarbetsförmåga och bidra till ett gott och öppet arbetsklimat i enlighet med vår värdegrund.  Vi lägger stor vikt vid dina personliga egenskaper och referenser.  ÖVRIGT Länsstyrelsen arbetar för att erbjuda en attraktiv arbetsplats som är fri från diskriminering och för att ge alla lika möjligheter. Vi tar gärna emot samtal från dig som är intresserad av jobbet, men vi tackar nej till dig som säljer annonser och rekryteringstjänster.  Länsstyrelsen har en viktig samordnande roll vid samhällsstörningar och därmed ingår samtliga anställda i kris- och krigsorganisationen.  Vissa av våra tjänster är säkerhetsklassade vilket innebär att säkerhetsprövning då kommer att genomföras innan anställning.   Vi kan erbjuda medflyttarservice via rekryteringslots Dalarna.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Byggnadsantikvarie** Länsstyrelsen i Dalarnas län ... | x |  |  | 18 | [Byggnadsantikvarie, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gv3f_7Fh_X3e) |
| 1 | **Byggnadsantikvarie** Länsstyrelsen i Dalarnas län ... |  | x |  | 18 | [Bebyggelseantikvarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ijoc_xYk_hmR) |
| 2 | ...adsantikvarie Länsstyrelsen i **Dalarnas län** är en statlig myndighet som f... | x | x | 12 | 12 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 3 | ...l enheten för kulturmiljö och **samhällsplanering** med god erfarenhet av arbete ... | x | x | 17 | 17 | [Samhällsplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5FMU_Dt4_2hF) |
| 4 | ... god erfarenhet av arbete med **byggnadsvård** och kulturmiljöer.   ARBETS... |  | x |  | 12 | [Byggnadsvård, antikvarie, **skill**](http://data.jobtechdev.se/taxonomy/concept/EaE2_UK8_eSV) |
| 4 | ... god erfarenhet av arbete med **byggnadsvård** och kulturmiljöer.   ARBETS... | x |  |  | 12 | [byggnadsvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Tk44_CrF_B33) |
| 5 | ...ära samarbete med våra övriga **kulturmiljövårdare** arbeta med Dalarnas kulturhis... | x |  |  | 18 | [Kulturmiljövård, **skill**](http://data.jobtechdev.se/taxonomy/concept/XRfU_bRB_DwH) |
| 6 | ...jövårdare arbeta med Dalarnas **kulturhistoriska** byggnader, bebyggelsemiljöer ... | x |  |  | 16 | [kulturhistoria, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FMUN_KzQ_KrA) |
| 7 | ...kområden på myndigheten.  Som **byggnadsantikvarie** kommer du att: •handlägga frå... | x |  |  | 18 | [Byggnadsantikvarie, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gv3f_7Fh_X3e) |
| 7 | ...kområden på myndigheten.  Som **byggnadsantikvarie** kommer du att: •handlägga frå... |  | x |  | 18 | [Bebyggelseantikvarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ijoc_xYk_hmR) |
| 8 | ...adsantikvarie kommer du att: •**handlägga** frågor om kyrkor, kyrkliga mi... | x |  |  | 9 | [Handläggare, offentlig förvaltning/Utredare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YG1s_tUg_jWJ) |
| 9 | ... du att: •handlägga frågor om **kyrkor**, kyrkliga miljöer, kulturhist... | x |  |  | 6 | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
| 10 | ... •handlägga frågor om kyrkor, **kyrkliga** miljöer, kulturhistoriskt vär... | x |  |  | 8 | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
| 11 | ... om kyrkor, kyrkliga miljöer, **kulturhistoriskt** värdefull bebyggelse, kulturl... | x |  |  | 16 | [kulturhistoria, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FMUN_KzQ_KrA) |
| 12 | ...kulturmiljöfrågor i samråd om **samhällsplanerings**- och i utvecklingsfrågor •ta ... | x |  |  | 18 | [Samhällsplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5FMU_Dt4_2hF) |
| 13 | ...unskapsunderlag kopplade till **kulturhistorisk** bebyggelse och kulturmiljöer.... | x |  |  | 15 | [kulturhistoria, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FMUN_KzQ_KrA) |
| 14 | ... KVALIFIKATIONER Krav •Du har **högskoleutbildning** med bebyggelseantikvarisk inr... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 15 | ... KVALIFIKATIONER Krav •Du har **högskoleutbildning med bebyggelseantikvarisk inriktning**.  •Du har arbetslivserfarenhe... | x |  |  | 55 | [Bebyggelseantikvarieutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ccEo_ska_zuZ) |
| 16 | ...u har arbetslivserfarenhet av **byggnadsantikvariskt** arbete. •Du har goda kunskape... | x |  |  | 20 | [Byggnadsantikvarie, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gv3f_7Fh_X3e) |
| 17 | ...ella delar av miljöbalken och **plan- och** bygglagen. •Du har B-körkort ... |  | x |  | 9 | [Plan- och bygglagen, PBL, **skill**](http://data.jobtechdev.se/taxonomy/concept/otrX_HCb_Vqx) |
| 18 | ... plan- och bygglagen. •Du har **B-körkort**  Meriterande •Du har arbetser... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 19 | ...nhet av ärendehandläggning av **kulturhistorisk** bebyggelse och kulturlandskap... | x |  |  | 15 | [kulturhistoria, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FMUN_KzQ_KrA) |
| 20 | ...ebyggelse och kulturlandskap, **byggnadsvård** och samhällsplanering. •Du ha... |  | x |  | 12 | [Byggnadsvård, antikvarie, **skill**](http://data.jobtechdev.se/taxonomy/concept/EaE2_UK8_eSV) |
| 20 | ...ebyggelse och kulturlandskap, **byggnadsvård** och samhällsplanering. •Du ha... | x |  |  | 12 | [byggnadsvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Tk44_CrF_B33) |
| 21 | ...turlandskap, byggnadsvård och **samhällsplanering**. •Du har erfarenhet av att ut... | x | x | 17 | 17 | [Samhällsplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5FMU_Dt4_2hF) |
| 22 | ...ingen. •Du har annan relevant **högskoleutbildning**, som kulturgeografi, kulturmi... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 23 | ...evant högskoleutbildning, som **kulturgeografi**, kulturmiljövetenskap, kultur... |  | x |  | 14 | [Kulturgeografi, **skill**](http://data.jobtechdev.se/taxonomy/concept/44Lr_qs6_RTG) |
| 23 | ...evant högskoleutbildning, som **kulturgeografi**, kulturmiljövetenskap, kultur... |  | x |  | 14 | [Annan utbildning i sociologi, etnologi och kulturgeografi, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AnNr_vva_5BW) |
| 23 | ...evant högskoleutbildning, som **kulturgeografi**, kulturmiljövetenskap, kultur... |  | x |  | 14 | [Kulturgeografi och ekonomisk geografi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Cx9n_gxy_H9m) |
| 23 | ...evant högskoleutbildning, som **kulturgeografi**, kulturmiljövetenskap, kultur... | x | x | 14 | 14 | [Kulturgeografi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Eayw_i4b_qgS) |
| 23 | ...evant högskoleutbildning, som **kulturgeografi**, kulturmiljövetenskap, kultur... |  | x |  | 14 | [Kultur- och samhällsgeografi, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/W14Q_t2q_3pt) |
| 23 | ...evant högskoleutbildning, som **kulturgeografi**, kulturmiljövetenskap, kultur... |  | x |  | 14 | [Forskare, kulturgeografi, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dhfK_iio_46c) |
| 23 | ...evant högskoleutbildning, som **kulturgeografi**, kulturmiljövetenskap, kultur... |  | x |  | 14 | [Sociologi, etnologi och kulturgeografi, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/zRKE_r3q_U1s) |
| 24 | ...övetenskap, kulturminnesvård, **samhällsplanering**, kyrkohistoria, m.m.  Personl... | x | x | 17 | 17 | [Samhällsplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5FMU_Dt4_2hF) |
| 25 | ...rycka dig i tal och skrift på **svenska**.  •Du kommer att ingå i en ar... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 26 | ...d och serviceinriktad, ha god **samarbetsförmåga** och bidra till ett gott och ö... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 27 | ...rservice via rekryteringslots **Dalarna**. |  | x |  | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| | **Overall** | | | **93** | **561** | 93/561 = **17%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Kulturgeografi, **skill**](http://data.jobtechdev.se/taxonomy/concept/44Lr_qs6_RTG) |
| x | x | x | [Samhällsplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/5FMU_Dt4_2hF) |
|  | x |  | [Annan utbildning i sociologi, etnologi och kulturgeografi, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AnNr_vva_5BW) |
|  | x |  | [Kulturgeografi och ekonomisk geografi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Cx9n_gxy_H9m) |
|  | x |  | [Byggnadsvård, antikvarie, **skill**](http://data.jobtechdev.se/taxonomy/concept/EaE2_UK8_eSV) |
| x | x | x | [Kulturgeografi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Eayw_i4b_qgS) |
| x |  |  | [kulturhistoria, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FMUN_KzQ_KrA) |
| x |  |  | [byggnadsvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Tk44_CrF_B33) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [Kultur- och samhällsgeografi, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/W14Q_t2q_3pt) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Kulturmiljövård, **skill**](http://data.jobtechdev.se/taxonomy/concept/XRfU_bRB_DwH) |
| x |  |  | [Handläggare, offentlig förvaltning/Utredare, offentlig förvaltning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YG1s_tUg_jWJ) |
| x |  |  | [Bebyggelseantikvarieutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ccEo_ska_zuZ) |
|  | x |  | [Forskare, kulturgeografi, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dhfK_iio_46c) |
| x |  |  | [Byggnadsantikvarie, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gv3f_7Fh_X3e) |
| x |  |  | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
|  | x |  | [Bebyggelseantikvarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ijoc_xYk_hmR) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x | x | x | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
|  | x |  | [Plan- och bygglagen, PBL, **skill**](http://data.jobtechdev.se/taxonomy/concept/otrX_HCb_Vqx) |
|  | x |  | [Sociologi, etnologi och kulturgeografi, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/zRKE_r3q_U1s) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/23 = **22%** |