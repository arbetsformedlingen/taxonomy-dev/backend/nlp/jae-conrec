# Results for '8dc47e8ceedb5d83cb5d18176d5a6334f0677d2d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8dc47e8ceedb5d83cb5d18176d5a6334f0677d2d](README.md) | 1 | 2397 | 13 | 14 | 46/155 = **30%** | 3/9 = **33%** |

## Source text

Servispersonal 100% -  Cuckoo's Nest Restaurang & Bar Cuckoo’s Nest är en restaurang & bar och en mötesplats för visionärer och drömmare, kreatörer och tvärtomtänkare, innovatörer och möjlighetsmänniskor. Beläget i Radisson Blu Riverside Hotel på Lindholmen, Göteborg. Att Cuckoo’s Nest är en plats där man befinner sig i gränslandet mellan genialitet och galenskap avspeglar sig i allt från inredning till servicen till våra gäster. Restaurangen är tvåfaldig vinnare av priset Europas Bästa Bar.  Eftersom gränsen mellan genialitet och galenskap är hårfin, döpte vi restaurangen till Cuckoo’s Nest. Vi tror att du kommer att känna dig som hemma.  Radisson Blu är ett internationellt varumärke med hotell världen över och vad är det inte som säger att detta kan vara en dörr ut i världen.  Nu söker vi dig som är passionerad, driven och älskar människor, du är inte rädd att bjuda på dig själv för att skapa en oförglömlig gästupplevelse. Du gillar att jobba i ett tight team, samt självständigt på din station som du tar dig an med stort ansvar Du kan jobba flexibelt både kvällar och helger. Du kommer att ingå i ett härligt team där vi tillsammans skapar en fantastisk arbetsplats och mötesplats för våra gäster. Du bjuder på dig själv, är engagerad i kollegor och gäster på hotellet och har alltid nära till skratt.  För att söka detta jobb bör du ha ett par års erfarenhet inom restaurangyrket och a la cartéservering, ordningsam, ansvarsfull tillsammans med en fantastisk personlighet. Naturligtvis älskar du mat & dryck i kombination och att alltid ge det extra till våra gäster.  Anställningsform: 100% start mitten av Juli Intervjuer kommer att ske löpande under perioden. Lön enligt överenskommelse  Tycker du att det här låter spännande så passa på och sök. Hos oss har du massa kul förmåner! Bara inte förmånen att arbeta med grymma kollegor utan även ett av världens största hotellvarumärke. Du har fina personalpriser världen över och även chansen att göra karriär ute i världen på något av våra hotell. Du har även fina förmåner på Winn hotell groups hotell, friskvårdsbidrag och massa fina utbildningar.  Hotellet ägs och drivs av  Winn Hotel Group AB (https://www.winn.se/) som äger, driver och utvecklar hotell med personlighet, engagemang och glädje.  Winn Hotel Group driver även sina egna affärsakademi kallad  Winn Business School (https://www.winn.se/winn-business-school/).

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Servispersonal** 100% -  Cuckoo's Nest Restaur... | x |  |  | 14 | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| 1 | **Servispersonal** 100% -  Cuckoo's Nest Restaur... |  | x |  | 14 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | Servispersonal **100%** -  Cuckoo's Nest Restaurang &... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 3 | ...ersonal 100% -  Cuckoo's Nest **Restaurang** & Bar Cuckoo’s Nest är en res... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 4 | ...ang & Bar Cuckoo’s Nest är en **restaurang** & bar och en mötesplats för v... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 5 | ...äget i Radisson Blu Riverside **Hotel** på Lindholmen, Göteborg. Att ... |  | x |  | 5 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 6 | ...iverside Hotel på Lindholmen, **Göteborg**. Att Cuckoo’s Nest är en plat... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 7 | ...kap avspeglar sig i allt från **inredning** till servicen till våra gäste... |  | x |  | 9 | [Inredning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/uAtM_95o_kaj) |
| 8 | ...ll servicen till våra gäster. **Restaurangen** är tvåfaldig vinnare av prise... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 9 | ...internationellt varumärke med **hotell** världen över och vad är det i... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 10 | ... jobba i ett tight team, samt **självständigt** på din station som du tar dig... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 11 | ...a cartéservering, ordningsam, **ansvarsfull** tillsammans med en fantastisk... |  | x |  | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 12 | ...ra gäster.  Anställningsform: **100%** start mitten av Juli Intervju... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 13 | ...te i världen på något av våra **hotell**. Du har även fina förmåner på... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 14 | ...ar även fina förmåner på Winn **hotell** groups hotell, friskvårdsbidr... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 15 | ...örmåner på Winn hotell groups **hotell**, friskvårdsbidrag och massa f... | x |  |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 16 | ...och massa fina utbildningar.  **Hotell**et ägs och drivs av  Winn Hote... |  | x |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 17 | ...om äger, driver och utvecklar **hotell** med personlighet, engagemang ... | x |  |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 18 | ... engagemang och glädje.  Winn **Hotel** Group driver även sina egna a... |  | x |  | 5 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| | **Overall** | | | **46** | **155** | 46/155 = **30%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x |  |  | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
|  | x |  | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
|  | x |  | [Inredning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/uAtM_95o_kaj) |
| | | **3** | 3/9 = **33%** |