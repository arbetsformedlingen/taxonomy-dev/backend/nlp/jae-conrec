# Results for '9a5754f7b6d141ed48bb82de698db42b2d0828b4'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9a5754f7b6d141ed48bb82de698db42b2d0828b4](README.md) | 1 | 3504 | 4 | 8 | 36/86 = **42%** | 2/7 = **29%** |

## Source text

Vill du arbeta på ett företag som andas hållbarhet & automation? Toyota Material Handling söker hårdvarunära utvecklare!  I rollen som utvecklare hos Toyota Material Handling får du arbeta med teknik i framkant. Toyota har i dagsläget flera spännande projekt på gång där du erbjuds möjligheten att få vara med på en utvecklande resa för nästa generations autonoma truckar. Du kommer att, tillsammans med ditt närmsta team fokusera på nyutveckling av förarlösa truckar i syfte att stärka Toyotas position som en världsledande aktör på marknaden. Här arbetar du nära slutprodukten med stort inflytande och möjlighet att utvecklas både i rollen och på Toyota!  Ditt anställningserbjudande   • Ett internationellt företag med korta beslutsvägar och en vänskaplig kultur där du kan utvecklas, påverka arbetet samt ta stort ansvar  • Att vara del inom förarlösa logistiklösningar, ett område som växer snabbt, både hos Toyota och i omvärlden  • Tillfälle att arbeta nära slutprodukten med ny teknik och fokus på nyutveckling  • En koldioxidneutral anläggning som värderar hållbarhet högt, både för omvärlden och medarbetaren  Dina arbetsuppgifter  Som utvecklare hos Toyota Material Handling kommer du att arbeta tätt tillsammans med ditt närmsta team och övriga funktioner inom Auto Solutions. Förutom nya modeller arbetar du även med vidareutveckling av befintliga truckmodeller, utvecklar kundspecifika lösningar samt verifierar din kod på truck i deras moderna lab. Arbetet sker nära slutprodukten och utvecklingen sker i huvudsak i C++ och strukturerad text mot Toyotas egna hårdvaruplattformar.  I rollen kommer du:   • Arbeta med hårdvarunära nyutveckling  • Innovera nya lösningar i nära samarbete med ditt team och övriga funktioner inom Auto Solutions  Värt att veta  Du kommer ingå i Toyota Material Handlings Auto Solutions-organisation som består av omkring 60 personer där 30 arbetar med mjukvaruutveckling. Mjukvaruavdelningen är uppdelad på tre olika team där alla har ett nära samarbete, både inom sitt närmsta team och mellan teamen. Du kommer att ingå i ett projektorienterat team som består av 12 personer där arbetet sker agilt. Auto Solutions är en självständig R&D-organisation inom Toyota Material Handling. Huvudarbetsplats är Toyota Material Handlings produktionsanläggning i Mjölby. På anläggningen arbetar idag omkring 2500 personer.  Våra förväntningar  Vi söker dig som har ett brett tekniskt intresse och har jobbat ett antal år med inbäddad mjukvaruutveckling. Du har kanske jobbat med C, C++ och/eller python. Det är viktigt att du har en förmåga för att analysera och lösa problem, samt se helhetslösningar. Samarbetet med andra kommer naturligt för dig och du delar gärna med dig av din kunskap och erfarenhet. Du trivs i en roll där du får ta ansvar för dina arbetsuppgifter och strävar ständigt efter att leverera en god slutprodukt.  Intresserad? I den här rekryteringen har vi valt att samarbeta med Ada Digital som strävar efter en transparent, inkluderande och snabbrörlig rekryteringsupplevelse. Vi vill veta mer om dig och din potential! Du söker tjänsten enkelt och behöver inte bifoga några dokument. Det räcker med en motivering till varför du söker tjänsten och om du saknar CV kan du bifoga din LinkedIn-profil. Följ sedan din ansökan live via vår hemsida. Urvalet sker löpande och tjänsten kan bli tillsatt före sista ansökningsdatum. Är du nyfiken och vill veta mer innan du söker? Hör av dig till ansvarig rekryterare. Vi ser fram emot att få kontakt med dig!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...öretag som andas hållbarhet & **automation**? Toyota Material Handling sök... | x |  |  | 10 | [Automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/tDxT_7oT_NyU) |
| 2 | ...idareutveckling av befintliga **truck**modeller, utvecklar kundspecif... |  | x |  | 5 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 3 | ...ar samt verifierar din kod på **truck** i deras moderna lab. Arbetet ... | x |  |  | 5 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 4 | ...tvecklingen sker i huvudsak i **C++** och strukturerad text mot Toy... |  | x |  | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 5 | ...0 personer där 30 arbetar med **mjukvaruutveckling**. Mjukvaruavdelningen är uppde... | x | x | 18 | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 6 | ...r agilt. Auto Solutions är en **självständig** R&D-organisation inom Toyota ... |  | x |  | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 7 | ...lings produktionsanläggning i **Mjölby**. På anläggningen arbetar idag... |  | x |  | 6 | [Mjölby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/stqv_JGB_x8A) |
| 8 | ...bat ett antal år med inbäddad **mjukvaruutveckling**. Du har kanske jobbat med C, ... | x | x | 18 | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 9 | .... Du har kanske jobbat med C, **C++** och/eller python. Det är vikt... |  | x |  | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 10 | ...e jobbat med C, C++ och/eller **python**. Det är viktigt att du har en... |  | x |  | 6 | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| | **Overall** | | | **36** | **86** | 36/86 = **42%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
|  | x |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
|  | x |  | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
|  | x |  | [Mjölby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/stqv_JGB_x8A) |
| x |  |  | [Automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/tDxT_7oT_NyU) |
|  | x |  | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| | | **2** | 2/7 = **29%** |