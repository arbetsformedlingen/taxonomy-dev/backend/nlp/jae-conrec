# Results for 'c12902ffc81ca0b9e795e401452b6d8b1d1cb99c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c12902ffc81ca0b9e795e401452b6d8b1d1cb99c](README.md) | 1 | 1445 | 8 | 7 | 34/69 = **49%** | 4/6 = **67%** |

## Source text

En del av vårt konferensteam The Collector's Hotels är ett familjeägt företag i Gamla Stan, Stockholm. Vi består av Lord Nelson Hotel, Lady Hamilton Hotel, Victory Hotel och Victory Konferens. I koncernen ingår även Restaurangerna Djuret, La Ragazza, Tweed, The Burgundy, Vingården och Flickan. Läs gärna mer om oss på www.collectorshotels.se samt på www.djuret.se   www.thecollectorshotels.se Vi är en liten familjeägd hotellkedja i Gamla Stan med fokus på det personliga och kontakten med gästen. Vi ser därför att du som medarbetare hos oss, oavsett vilken avdelning du jobbar på, har en hög känsla för service, tycker det är roligt och har lätt för att kommunicera inte bara med dina kollegor utan framför allt med våra gäster. Gästerna är din första prioritet i alla lägen. Du är flexibel, trivs i ett högt tempo och är lösningsorienterad. Just nu jobbar vi med att förfina och lyfta vårt helhetskoncept på vår konferensavdelning. Vi söker då dig som är sugen på att vara med på den resan. Du har gärna erfarenhet av hotell, restaurang eller konferens, Du känner dig bekväm med att bereda smörgåsar och förbereda fika, vara gästerna behjälplig med tekniken, att finnas till hands för dem under deras dagar hos oss. Du ansvarar för att deras upplevelse blir kvalitativ i alla led. Om du tycker att detta låter som en intressant tjänst så är du varmt välkommen med din ansökan till oss! Tjänsten är initialt en deltid förlagd måndag - fredag.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | En del av vårt **konferens**team The Collector's Hotels är... | x |  |  | 9 | [Konferensservice, **skill**](http://data.jobtechdev.se/taxonomy/concept/H73b_dCy_CTT) |
| 2 | ...iljeägt företag i Gamla Stan, **Stockholm**. Vi består av Lord Nelson Hot... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 3 | ...olm. Vi består av Lord Nelson **Hotel**, Lady Hamilton Hotel, Victory... |  | x |  | 5 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 4 | ....se Vi är en liten familjeägd **hotell**kedja i Gamla Stan med fokus p... | x |  |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 5 | ...n. Du har gärna erfarenhet av **hotell**, restaurang eller konferens, ... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 6 | ...r gärna erfarenhet av hotell, **restaurang** eller konferens, Du känner di... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 7 | ...t av hotell, restaurang eller **konferens**, Du känner dig bekväm med att... | x |  |  | 9 | [Konferensservice, **skill**](http://data.jobtechdev.se/taxonomy/concept/H73b_dCy_CTT) |
| 8 | ...ner dig bekväm med att bereda **smörgåsar** och förbereda fika, vara gäst... | x | x | 9 | 9 | [Smörgåsar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ib92_FER_3Dd) |
| 9 | ... oss! Tjänsten är initialt en **deltid** förlagd måndag - fredag. | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| | **Overall** | | | **34** | **69** | 34/69 = **49%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Konferensservice, **skill**](http://data.jobtechdev.se/taxonomy/concept/H73b_dCy_CTT) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x | x | x | [Smörgåsar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ib92_FER_3Dd) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| | | **4** | 4/6 = **67%** |