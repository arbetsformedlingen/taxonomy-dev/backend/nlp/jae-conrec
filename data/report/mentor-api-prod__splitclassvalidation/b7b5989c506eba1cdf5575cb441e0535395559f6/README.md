# Results for 'b7b5989c506eba1cdf5575cb441e0535395559f6'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b7b5989c506eba1cdf5575cb441e0535395559f6](README.md) | 1 | 158 | 5 | 4 | 37/45 = **82%** | 3/4 = **75%** |

## Source text

Frisör Vi söker en frisör till oss på Salongtorget 10. Du ska kunna herrfrisering och gärna damfrisering. Det är meriterande om du har erfarenhet av färgning.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Frisör** Vi söker en frisör till oss p... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 2 | Frisör Vi söker en **frisör** till oss på Salongtorget 10. ... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 3 | ...Salongtorget 10. Du ska kunna **herrfrisering** och gärna damfrisering. Det ä... | x | x | 13 | 13 | [Herrfrisering, **skill**](http://data.jobtechdev.se/taxonomy/concept/NrJn_nRJ_TeL) |
| 4 | ...kunna herrfrisering och gärna **damfrisering**. Det är meriterande om du har... | x | x | 12 | 12 | [Damfrisering, **skill**](http://data.jobtechdev.se/taxonomy/concept/koMr_VoX_RA3) |
| 5 | ...rande om du har erfarenhet av **färgning**. | x |  |  | 8 | [hårfärgning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4ket_STk_esu) |
| | **Overall** | | | **37** | **45** | 37/45 = **82%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [hårfärgning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4ket_STk_esu) |
| x | x | x | [Herrfrisering, **skill**](http://data.jobtechdev.se/taxonomy/concept/NrJn_nRJ_TeL) |
| x | x | x | [Damfrisering, **skill**](http://data.jobtechdev.se/taxonomy/concept/koMr_VoX_RA3) |
| x | x | x | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| | | **3** | 3/4 = **75%** |