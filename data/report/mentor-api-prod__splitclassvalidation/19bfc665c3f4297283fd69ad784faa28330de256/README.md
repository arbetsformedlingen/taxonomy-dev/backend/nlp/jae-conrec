# Results for '19bfc665c3f4297283fd69ad784faa28330de256'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [19bfc665c3f4297283fd69ad784faa28330de256](README.md) | 1 | 557 | 11 | 6 | 21/125 = **17%** | 2/9 = **22%** |

## Source text

Fullstack är en av marknadens mest eftertraktade roller just nu. En flexibel roll med starkt fokus på lösningen. Att vara fullstack betyder att du kan koda överallt och tillsammans med specialister inom alla utvecklingsområden. Du känner dig hemma i databaser, servermiljö, frontend och backend. Du har också kompetenser som gör dig lämpad att leda utvecklingen i teamet som; användbarhet, arkitektur och projektplanering. Under utbildningen finns det också möjlighet att nischa sig så du kommer ut med ett starkare fokus på antingen backend eller frontend.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Fullstack** är en av marknadens mest efte... | x |  |  | 9 | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
| 2 | ... fokus på lösningen. Att vara **fullstack** betyder att du kan koda övera... | x |  |  | 9 | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
| 3 | ... fullstack betyder att du kan **koda** överallt och tillsammans med ... | x |  |  | 4 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 4 | ...mråden. Du känner dig hemma i **databaser**, servermiljö, frontend och ba... | x | x | 9 | 9 | [Databaser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/Uj5W_dft_Ssg) |
| 5 | ...mma i databaser, servermiljö, **frontend** och backend. Du har också kom... |  | x |  | 8 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 6 | ...er, servermiljö, frontend och **backend**. Du har också kompetenser som... |  | x |  | 7 | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| 7 | ...tenser som gör dig lämpad att **leda utvecklingen i teamet** som; användbarhet, arkitektur... | x |  |  | 26 | [Processutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuP9_SAc_iMs) |
| 8 | ...da utvecklingen i teamet som; **användbarhet**, arkitektur och projektplaner... | x | x | 12 | 12 | [Användbarhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/wYxH_RXX_ZYx) |
| 9 | ...n i teamet som; användbarhet, **arkitektur** och projektplanering. Under u... | x |  |  | 10 | [hantera IT-arkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/K1ft_EPa_SCk) |
| 10 | ... användbarhet, arkitektur och **projektplanering**. Under utbildningen finns det... | x |  |  | 16 | [genomföra projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Js6n_4RN_q7R) |
| 11 | ...tt starkare fokus på antingen **backend** eller frontend. |  | x |  | 7 | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
| 12 | ...kus på antingen backend eller **frontend**. |  | x |  | 8 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| | **Overall** | | | **21** | **125** | 21/125 = **17%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
|  | x |  | [Backend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/7wdX_4rv_33z) |
|  | x |  | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| x |  |  | [genomföra projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Js6n_4RN_q7R) |
| x |  |  | [hantera IT-arkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/K1ft_EPa_SCk) |
| x |  |  | [Processutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuP9_SAc_iMs) |
| x | x | x | [Databaser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/Uj5W_dft_Ssg) |
| x |  |  | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x | x | x | [Användbarhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/wYxH_RXX_ZYx) |
| | | **2** | 2/9 = **22%** |