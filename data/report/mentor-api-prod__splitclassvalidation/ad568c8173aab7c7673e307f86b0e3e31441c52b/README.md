# Results for 'ad568c8173aab7c7673e307f86b0e3e31441c52b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ad568c8173aab7c7673e307f86b0e3e31441c52b](README.md) | 1 | 1642 | 17 | 13 | 82/185 = **44%** | 7/14 = **50%** |

## Source text

Lokalvårdare deltid - Gnesta Serviceföretaget Pima AB är ett företag som specialiserat sig på lokalvård för företag med säkerställd renhet men vi har även en stor variation av kunder och hjälper till med allt från golvvård, fönsterputs till regelmässig lokalvård.    Vi har idag ca 150 medarbetare som levererar service till organisationer och privatpersoner i hela södra Sverige, från Uppsala till Skåne.    Beskrivning Vi behöver nu dig som vill jobba som lokalvårdare på skolor och förskolor i Gnesta.  Vi städar kvällstid på flera objekt.   Arbetsuppgifter Du kommer städa i olika typer av skolmiljöer så som i skolsalar, korridorer, matsal, toaletter, trapphus, idrottssal, omklädningsrum, förskolelokaler och andra typer av lokaler.   Kvalifikationer Vi söker dig som är: - Ansvarstagande - Är social - Gillar att arbetat med barn runt omkring - Klarar av att arbeta ensam men även i grupp - Tycker om att följa instruktioner  Det är meriterande om du har erfarenhet av städning i skolor sedan tidigare.  För att förstå instruktioner och kunna kommunicera med kunden är det viktigt att du kan svenska i tal och skrift.  Stämmer detta in på dig? Sök direkt, då intervjuer och rekrytering sker löpande ser vi fram emot din ansökan redan idag!    Villkor Tjänsterna vi söker är på deltid, vi tillämpar visstidsanställning. Start på respektive objekt sker från v 35. Arbetstiderna är mån-fre mellan ca 17.00 - 20.30 efter verksamheten har stängt.  Arbetsplatserna ligger på olika adresser i Gnesta.  Utdrag ur belastningsregistret måste visas då arbetet är i skolmiljö.  Intervjuer sker löpande, vi ser fram emot din ansökan redan idag.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Lokalvårdare** deltid - Gnesta Serviceföreta... | x | x | 12 | 12 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 2 | Lokalvårdare **deltid** - Gnesta Serviceföretaget Pim... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 3 | Lokalvårdare deltid - **Gnesta** Serviceföretaget Pima AB är e... | x | x | 6 | 6 | [Gnesta, **municipality**](http://data.jobtechdev.se/taxonomy/concept/os8Y_RUo_U3u) |
| 4 | ...etag som specialiserat sig på **lokalvård** för företag med säkerställd r... |  | x |  | 9 | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| 5 | ...h privatpersoner i hela södra **Sverige**, från Uppsala till Skåne.   ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 6 | ...er i hela södra Sverige, från **Uppsala** till Skåne.    Beskrivning ... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 7 | ...ra Sverige, från Uppsala till **Skåne**.    Beskrivning Vi behöver ... | x | x | 5 | 5 | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| 8 | ...ver nu dig som vill jobba som **lokalvårdare** på skolor och förskolor i Gne... | x | x | 12 | 12 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 9 | ...are på skolor och förskolor i **Gnesta**.  Vi städar kvällstid på fl... | x | x | 6 | 6 | [Gnesta, **municipality**](http://data.jobtechdev.se/taxonomy/concept/os8Y_RUo_U3u) |
| 10 | ....   Arbetsuppgifter Du kommer **städa i olika typer av skolmiljöer** så som i skolsalar, korridore... | x |  |  | 34 | [Städning av förskola/Städning av skola, **skill**](http://data.jobtechdev.se/taxonomy/concept/y4sg_zUA_mPz) |
| 11 | ...tioner Vi söker dig som är: - **Ansvarstagande** - Är social - Gillar att arbe... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 12 | ...cial - Gillar att arbetat med **barn** runt omkring - Klarar av att ... | x |  |  | 4 | [Barn och ungdom, **skill**](http://data.jobtechdev.se/taxonomy/concept/qkyH_MC1_YKU) |
| 13 | ... runt omkring - Klarar av att **arbeta ensam** men även i grupp - Tycker om ... | x |  |  | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 14 | ... av att arbeta ensam men även **i grupp** - Tycker om att följa instruk... | x |  |  | 7 | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| 15 | ...rande om du har erfarenhet av **städning** i skolor sedan tidigare.  För... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 16 | ...rande om du har erfarenhet av **städning i skolor** sedan tidigare.  För att förs... | x |  |  | 17 | [Städning av förskola/Städning av skola, **skill**](http://data.jobtechdev.se/taxonomy/concept/y4sg_zUA_mPz) |
| 17 | ...den är det viktigt att du kan **svenska** i tal och skrift.  Stämmer de... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 18 | ...kor Tjänsterna vi söker är på **deltid**, vi tillämpar visstidsanställ... | x |  |  | 6 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 19 | ...na ligger på olika adresser i **Gnesta**.  Utdrag ur belastningsregist... | x | x | 6 | 6 | [Gnesta, **municipality**](http://data.jobtechdev.se/taxonomy/concept/os8Y_RUo_U3u) |
| | **Overall** | | | **82** | **185** | 82/185 = **44%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
|  | x |  | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Gnesta, **municipality**](http://data.jobtechdev.se/taxonomy/concept/os8Y_RUo_U3u) |
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| x |  |  | [Barn och ungdom, **skill**](http://data.jobtechdev.se/taxonomy/concept/qkyH_MC1_YKU) |
| x |  |  | [Städning av förskola/Städning av skola, **skill**](http://data.jobtechdev.se/taxonomy/concept/y4sg_zUA_mPz) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/14 = **50%** |