# Results for 'dac97137f9547ca609c8167c1eea916cd3726f5a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [dac97137f9547ca609c8167c1eea916cd3726f5a](README.md) | 1 | 3494 | 16 | 14 | 93/248 = **38%** | 7/19 = **37%** |

## Source text

Köksbiträde till Familjehuset i Lund VoB (Vård och Behandling) är ett kommunägt bolag som drivs på samhällsnyttig grund utan vinstsyfte. VoB erbjuder goda utvecklings- och karriärmöjligheter för dig som vill satsa i ett kreativt sammanhang med korta beslutsvägar. Arbetsklimatet i VoB präglas av engagemang, öppenhet och prestigelöshet. Som medarbetare ges du stort utrymme att ta eget ansvar och du har stor frihet att lösa ditt uppdrag på det sätt som du tycker är bäst. Du erbjuds kontinuerlig kompetensutveckling och subventionerad friskvård.   Familjehuset är ett akut, utrednings- och behandlingshem för barn och familjer, i verksamheten finns två familjeavdelningar.  Den ena avdelningen tar emot familjer med barn i åldern 0-3 år och äldre syskon kan finnas med i placeringen. Den andra avdelningen tar emot familjer med spädbarn och fokus ligger på barnets tidiga anknytning. Avdelningen tar även emot gravida kvinnor några veckor före beräknad förlossning.  Vi söker nu en serviceinriktad medarbetare med ansvar för olika delar inom kök och matlagning och som trivs att arbeta nära barn och vara en av barnens förebilder. Verksamheten som i nuläget ligger i Hörby kommer under den senare delen av hösten att flytta till nya lokaler i Lund. Stora möjligheter att påverka utformningen av tjänsten finns i samband med flytten till nya lokaler.  ARBETSUPPGIFTER I tjänsten ingår att ta emot mat som levereras färdig för lunch och vara behjälplig med dukning och vid servering. Du kommer också att förbereda kvällsmat för dagen samt vid veckoslut även kvällsmål för helgen.   Viss tvätt och städning av kök och matsal ingår liksom att planera matsedel.  Varje vecka görs matbeställningar och du tar då även hand om dessa inköp med tex uppackning av varor mm.  I arbetsuppgifterna ingår även att handleda familjerna i köket samt att vara rollmodell vid de möjligheter till socialt samspel som arbete i köket kan ge. Till exempel genom enklare bakning och planering av firande vid lov och högtidsdagar.   KVALIFIKATIONER Du som söker tjänsten är serviceinriktad, engagerad, stresstålig och prestigelös och tycker att arbetsuppgifterna och verksamhetens uppdrag motsvarar dina förväntningar. För dig är det självklart att förena struktur och flexibilitet i det dagliga arbete.   Vidare är du  självständig och självgående, tar ansvar för dina uppgifter vilket bl a innebär att du planerar, organiserar och prioriterar arbetet på ett effektivt sätt.  Du har någon form av köksutbildning som te x kallskänka, köksbiträde eller annan utbildning samt praktisk erfarenhet av matlagning och övrigt köksarbete.  Det är en merit om du arbetat på arbetsplatser inom det sociala området och med människor i behov av extra stöd Du är kvalitetsmedveten och det är positivt om du därmed har erfarenhet av egenkontroll och livsmedelshygien. Befattningen motsvarar en halvtidstjänst. Utöver detta finns möjlighet till utökning av tjänstgöringsgrad och ytterligare arbetsuppgifter beroende på din erfarenhet, personliga förmågor, intresse och utbildning.  Tjänsten kan med fördel kompletteras med till exempel ansvar för lokalvård av gemensamma utrymmen eller pedagogiskt arbete med barnen på boendet.  För utökning av tjänstgöringsgrad med pedagogiskt arbete med barnen krävs även barnskötarutbildning eller motsvarande liksom erfarenhet av arbete med barn.  ÖVRIGT En förutsättning för anställning är att du avstår från att röka under arbetstid. VoB tillämpar vård omsorg - HÖK/AB  Urval sker löpande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Köksbiträde** till Familjehuset i Lund VoB ... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 2 | ...ksbiträde till Familjehuset i **Lund** VoB (Vård och Behandling) är ... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 3 | ...utveckling och subventionerad **friskvård**.   Familjehuset är ett akut, ... | x |  |  | 9 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 4 | ... är ett akut, utrednings- och **behandlingshem** för barn och familjer, i verk... | x |  |  | 14 | [Behandlingshem, **skill**](http://data.jobtechdev.se/taxonomy/concept/oQm8_Ucv_eSp) |
| 5 | ...mheten som i nuläget ligger i **Hörby** kommer under den senare delen... |  | x |  | 5 | [Hörby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/autr_KMa_cfp) |
| 6 | ...att flytta till nya lokaler i **Lund**. Stora möjligheter att påverk... |  | x |  | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 7 | ...ehjälplig med dukning och vid **servering**. Du kommer också att förbered... | x |  |  | 9 | [Serveringsbiträde, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gZVV_nVJ_LkV) |
| 8 | ... kvällsmål för helgen.   Viss **tvätt** och städning av kök och matsa... | x |  |  | 5 | [Tvätteri, **skill**](http://data.jobtechdev.se/taxonomy/concept/JLX2_ZVk_veF) |
| 9 | ... för helgen.   Viss tvätt och **städning** av kök och matsal ingår likso... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 9 | ... för helgen.   Viss tvätt och **städning** av kök och matsal ingår likso... | x |  |  | 8 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 10 | ...a matsedel.  Varje vecka görs **matbeställningar** och du tar då även hand om de... |  | x |  | 16 | [ta hand om mat- och dryckesbeställningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7WfK_CiU_Y61) |
| 10 | ...a matsedel.  Varje vecka görs **matbeställningar** och du tar då även hand om de... |  | x |  | 16 | [ta emot mat- och dryckesbeställningar från kunder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xHq2_765_ZC8) |
| 11 | ...r serviceinriktad, engagerad, **stresstålig** och prestigelös och tycker at... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 12 | ...gliga arbete.   Vidare är du  **självständig** och självgående, tar ansvar f... | x | x | 12 | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 13 | ...rm av köksutbildning som te x **kallskänka**, köksbiträde eller annan utbi... | x | x | 10 | 10 | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| 14 | ...bildning som te x kallskänka, **köksbiträde** eller annan utbildning samt p... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 15 | ...rfarenhet av egenkontroll och **livsmedelshygien**. Befattningen motsvarar en ha... | x | x | 16 | 16 | [Livsmedelshygien, **skill**](http://data.jobtechdev.se/taxonomy/concept/kj4Q_nLh_u8Q) |
| 16 | ...en. Befattningen motsvarar en **halvtidstjänst**. Utöver detta finns möjlighet... | x |  |  | 14 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 17 | ...s med till exempel ansvar för **lokalvård** av gemensamma utrymmen eller ... | x | x | 9 | 9 | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| 18 | ... av gemensamma utrymmen eller **pedagogiskt arbete** med barnen på boendet.  För u... | x |  |  | 18 | [Pedagogiskt arbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/NXqi_A8a_X1b) |
| 19 | ...ning av tjänstgöringsgrad med **pedagogiskt arbete** med barnen krävs även barnskö... | x |  |  | 18 | [Pedagogiskt arbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/NXqi_A8a_X1b) |
| 20 | ... arbete med barnen krävs även **barnskötarutbildning** eller motsvarande liksom erfa... | x | x | 20 | 20 | [Barnskötarutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zQSz_T95_Pmu) |
| | **Overall** | | | **93** | **248** | 93/248 = **38%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
|  | x |  | [ta hand om mat- och dryckesbeställningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7WfK_CiU_Y61) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| x |  |  | [Tvätteri, **skill**](http://data.jobtechdev.se/taxonomy/concept/JLX2_ZVk_veF) |
| x |  |  | [Pedagogiskt arbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/NXqi_A8a_X1b) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Hörby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/autr_KMa_cfp) |
| x | x | x | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| x |  |  | [Serveringsbiträde, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gZVV_nVJ_LkV) |
| x | x | x | [Livsmedelshygien, **skill**](http://data.jobtechdev.se/taxonomy/concept/kj4Q_nLh_u8Q) |
| x | x | x | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| x |  |  | [Behandlingshem, **skill**](http://data.jobtechdev.se/taxonomy/concept/oQm8_Ucv_eSp) |
| x | x | x | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
|  | x |  | [ta emot mat- och dryckesbeställningar från kunder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xHq2_765_ZC8) |
| x |  |  | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Barnskötarutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zQSz_T95_Pmu) |
| | | **7** | 7/19 = **37%** |