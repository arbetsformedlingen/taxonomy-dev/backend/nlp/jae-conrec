# Results for '2e62ae66246fc5e1bd3e89c95479063c276a27c2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2e62ae66246fc5e1bd3e89c95479063c276a27c2](README.md) | 1 | 2325 | 18 | 21 | 152/349 = **44%** | 11/25 = **44%** |

## Source text

Elmontörer inom apparatskåp till DeltaNordic - Norra Stockholm! DeltaNordic är en totalleverantör av avancerade elektriska och elektroniska system. De tillverkar apparatskåp, kablage och elektronik. Det är ett välmående företag och en attraktiv arbetsgivare. De är måna om medarbetarnas trivsel och de tillämpar flextid och kort arbetsdag på fredagar.  Nu söker vi två elmontörer till DeltaNordic!  Om tjänsten Arbetet innebär montering av kablage och komponenter i bl.a. apparatskåp. Till din hjälp har du noggrant framtagna instruktioner, men även montering utifrån elschema förekommer. Anställningen inleds med en god introduktion med stöd från mentor och kollegor. Du kommer få stora möjligheter att utbildas i nya komponenter och produkter inom el.   Företaget har en imponerande kundbas och högteknologiska produkter som används bl.a. inom gruvindustrin och transport.  DeltaNordic sitter i fina lokaler i Kungsängen. De tillämpar flextid och kort arbetsdag på fredagar. Tjänsten är ett uthyrningsuppdrag och behovet är på lång sikt! Eventuellt kan det finnas möjlighet att bli anställd hos kund efter en period som inhyrd.  Kvalifikationer Vi söker dig som har arbetat som apparatskåpsmontör i minst ett år, alternativt har du elutbildning och vill lära dig ett nytt yrke. Elutbildning är generellt ett stort plus, men inget krav om du har annan erfarenhet från branschen. Har du arbetat som provare, testare, elskåpsmontör, automationstekniker eller elektriker är det starkt meriterande.  Vi fäster stor vikt vid de personliga egenskaperna. Noggrannhet, bra ordningssinne och förmåga att arbeta självständigt är viktigt för att lyckas i tjänsten. Flytande svenska i tal och skrift krävs också då samarbete med kollegor är en viktig del av tjänsten.   Ansökan Låter det här som ett arbete för dig? Skicka din ansökan direkt då urvalet sker löpande!   DeltaNordic har anlitat MTX Rekrytering & Bemanning för den här rekryteringen och det är DeltaNordics önskemål att alla frågor kring tjänsten hanteras av MTX Rekrytering & Bemanning.  Övrigt Placering: DeltaNordics lokaler i Kungsängen (Brunna industriområde) Start: Omgående, med hänsyn till ev uppsägningstid Omfattning: Heltid Lön: Genomsnittligt förtjänstläge enligt kollektivavtal   Kontaktpersoner: Julia Edoff, ansvarig rekryteringskonsult Tel: 076 880 03 68

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Elmontörer** inom apparatskåp till DeltaNo... | x |  |  | 10 | [Elmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/AJDU_fpb_ADF) |
| 2 | ...skåp till DeltaNordic - Norra **Stockholm**! DeltaNordic är en totallever... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 3 | ...m. De tillverkar apparatskåp, **kablage** och elektronik. Det är ett vä... | x |  |  | 7 | [montör, kablage, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FYfj_bpz_zK9) |
| 4 | ...rkar apparatskåp, kablage och **elektronik**. Det är ett välmående företag... | x | x | 10 | 10 | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| 5 | ... fredagar.  Nu söker vi två **elmontörer** till DeltaNordic!  Om tjänste... | x |  |  | 10 | [Elmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/AJDU_fpb_ADF) |
| 6 | ...söker dig som har arbetat som **apparatskåpsmontör** i minst ett år, alternativt h... | x | x | 18 | 18 | [Apparatskåpsmontör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/5ETM_zu9_YMf) |
| 7 | ...st ett år, alternativt har du **elutbildning** och vill lära dig ett nytt yr... | x | x | 12 | 12 | [Godkänd elutbildning, **skill**](http://data.jobtechdev.se/taxonomy/concept/bG9S_mns_7q9) |
| 8 | ... vill lära dig ett nytt yrke. **Elutbildning** är generellt ett stort plus, ... | x | x | 12 | 12 | [Godkänd elutbildning, **skill**](http://data.jobtechdev.se/taxonomy/concept/bG9S_mns_7q9) |
| 9 | ...branschen. Har du arbetat som **provare**, testare, elskåpsmontör, auto... |  | x |  | 7 | [provare, fordonsmotorer, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/762o_aUs_TBZ) |
| 9 | ...branschen. Har du arbetat som **provare**, testare, elskåpsmontör, auto... |  | x |  | 7 | [provare, fartygsmotorer, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Bh58_er8_b1G) |
| 9 | ...branschen. Har du arbetat som **provare**, testare, elskåpsmontör, auto... | x | x | 7 | 7 | [provare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/VUu2_Cxm_BbE) |
| 9 | ...branschen. Har du arbetat som **provare**, testare, elskåpsmontör, auto... |  | x |  | 7 | [provare, motorer, rullande materiel, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/dUL5_Dwg_fx7) |
| 9 | ...branschen. Har du arbetat som **provare**, testare, elskåpsmontör, auto... |  | x |  | 7 | [kemisk provare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/zoH6_tiP_fqX) |
| 10 | ...arbetat som provare, testare, **elskåpsmontör**, automationstekniker eller el... | x | x | 13 | 13 | [Elskåpsmontör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/tNS4_dBr_Ysg) |
| 11 | ...vare, testare, elskåpsmontör, **automationstekniker** eller elektriker är det stark... |  | x |  | 19 | [Automationstekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CC4H_YCh_77T) |
| 11 | ...vare, testare, elskåpsmontör, **automationstekniker** eller elektriker är det stark... |  | x |  | 19 | [Automationstekniker, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/LrrC_U7A_Xus) |
| 11 | ...vare, testare, elskåpsmontör, **automationstekniker** eller elektriker är det stark... |  | x |  | 19 | [Automationstekniker, maskin, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/UiDW_ncp_spU) |
| 11 | ...vare, testare, elskåpsmontör, **automationstekniker** eller elektriker är det stark... |  | x |  | 19 | [automationstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/beLd_VF7_rzM) |
| 11 | ...vare, testare, elskåpsmontör, **automationstekniker** eller elektriker är det stark... | x | x | 19 | 19 | [Automationstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/mTT5_xgE_sRx) |
| 11 | ...vare, testare, elskåpsmontör, **automationstekniker** eller elektriker är det stark... |  | x |  | 19 | [Automationstekniker, elektronik, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oC9M_Bqu_WZH) |
| 12 | ...d de personliga egenskaperna. **Noggrannhet**, bra ordningssinne och förmåg... | x | x | 11 | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 13 | ...ordningssinne och förmåga att **arbeta självständigt** är viktigt för att lyckas i t... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 14 | ...t lyckas i tjänsten. Flytande **svenska** i tal och skrift krävs också ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 15 | ...tal och skrift krävs också då **samarbete med kollegor** är en viktig del av tjänsten.... | x |  |  | 22 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 16 | ...ev uppsägningstid Omfattning: **Heltid** Lön: Genomsnittligt förtjänst... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 17 | ...nittligt förtjänstläge enligt **kollektivavtal**   Kontaktpersoner: Julia Edof... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 18 | ...rsoner: Julia Edoff, ansvarig **rekryteringskonsult** Tel: 076 880 03 68 | x |  |  | 19 | [rekryteringskonsult, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tGyt_via_HNb) |
| | **Overall** | | | **152** | **349** | 152/349 = **44%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Apparatskåpsmontör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/5ETM_zu9_YMf) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [provare, fordonsmotorer, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/762o_aUs_TBZ) |
| x |  |  | [Elmontör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/AJDU_fpb_ADF) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [provare, fartygsmotorer, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Bh58_er8_b1G) |
|  | x |  | [Automationstekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CC4H_YCh_77T) |
| x |  |  | [montör, kablage, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FYfj_bpz_zK9) |
|  | x |  | [Automationstekniker, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/LrrC_U7A_Xus) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
|  | x |  | [Automationstekniker, maskin, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/UiDW_ncp_spU) |
| x | x | x | [provare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/VUu2_Cxm_BbE) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Godkänd elutbildning, **skill**](http://data.jobtechdev.se/taxonomy/concept/bG9S_mns_7q9) |
|  | x |  | [automationstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/beLd_VF7_rzM) |
|  | x |  | [provare, motorer, rullande materiel, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/dUL5_Dwg_fx7) |
| x | x | x | [Automationstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/mTT5_xgE_sRx) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [Automationstekniker, elektronik, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oC9M_Bqu_WZH) |
| x |  |  | [rekryteringskonsult, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tGyt_via_HNb) |
| x | x | x | [Elskåpsmontör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/tNS4_dBr_Ysg) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
|  | x |  | [kemisk provare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/zoH6_tiP_fqX) |
| | | **11** | 11/25 = **44%** |