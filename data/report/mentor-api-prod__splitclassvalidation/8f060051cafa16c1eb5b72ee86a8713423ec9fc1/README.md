# Results for '8f060051cafa16c1eb5b72ee86a8713423ec9fc1'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8f060051cafa16c1eb5b72ee86a8713423ec9fc1](README.md) | 1 | 573 | 4 | 4 | 25/39 = **64%** | 3/5 = **60%** |

## Source text

Servitör Har du viljan och förmågan att leverera en minnesvärd upplevelse för våra gäster? Är du intresserad av och kunnig inom mat och dryck? Då kanske du vill arbeta i just detta matsalsteam. Vi är ett sammansvetsat team i olika åldrar som tillsammans delar vår kärlek till service och gästbemötande. Huvudsakliga arbetsuppgifter servering av a la carte och gruppmiddagar under lunch och middag. Men även bröllop och andra event. Barkunskap är meriterande. Krav: körkort och svenska i tal och skrift. Känner du att detta stämmer in på dig är du välkommen med din ansökan.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Servitör** Har du viljan och förmågan at... | x | x | 8 | 8 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | ... arbetsuppgifter servering av **a la carte** och gruppmiddagar under lunch... | x | x | 10 | 10 | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| 3 | ...kunskap är meriterande. Krav: **körkort** och svenska i tal och skrift.... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 3 | ...kunskap är meriterande. Krav: **körkort** och svenska i tal och skrift.... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 4 | ...eriterande. Krav: körkort och **svenska** i tal och skrift. Känner du a... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **25** | **39** | 25/39 = **64%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/5 = **60%** |