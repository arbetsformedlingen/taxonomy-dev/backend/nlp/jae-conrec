# Results for 'dce1eaa2a6542be401b9be5fdcb76b2b53419379'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [dce1eaa2a6542be401b9be5fdcb76b2b53419379](README.md) | 1 | 5128 | 16 | 23 | 121/417 = **29%** | 6/20 = **30%** |

## Source text

Skolbibliotekarie till Teleborg centrum skola Nu söker vi dig, som tillsammans med oss vill göra nytta för dem vi är till för! Växjö kommun är Kronobergs största arbetsgivare som varje dag, året runt, arbetar för våra invånare, företag och besökare. Tillsammans skapar vi en hållbar morgondag och en attraktiv kommun att bo, leva och verka i – idag och i framtiden. Det innebär ett arbete där vi utmanar oss att tänka nytt, lära och utvecklas med varandra. Vill du vara en del i ett stort sammanhang där du får vara med och göra skillnad på riktigt? Välkommen till oss på Växjö kommun!  ARBETSUPPGIFTER Välkommen till utbildningsförvaltningen!  Hos oss har du möjlighet att göra skillnad för våra barn och elever. Vi ansvarar för cirka  18 000 barn och elever och jobbar varje dag för att de ska utvecklas och nå en ökad måluppfyllelse. Vi vill att barn kommer in i vår verksamhet som nyfikna kreativa ettåringar och lämnar oss som engagerade, kunniga och självständiga 1920-åringar.   Teleborg centrum skola rymmer flera olika verksamheter vilket ger en mångfald och dynamik. Här finns grundskola för åk 7-9, grundsärskola för 7-9 och Växjö International Baccalaureate grundskola. Läsåret 2022/2023 har vi cirka 430 elever uppdelade på 17 klasser och ca 60 anställda. På Tc lär eleverna för framtiden, de utmanas och ges det stöd de behöver. Ett prioriterat mål för oss är att eleverna känner att de har inflytande och rätt stöd för sitt lärande. På skolan finns många gemensamma traditioner som bidrar till ett gott klimat på skolan både hos elever och personal.   Som arbetstagare hos oss är du viktig! Oavsett vilken roll du har i vår förvaltning kommer du, tillsammans med oss, arbeta för att bidra till en positiv utveckling för våra barn och elever.  Vi söker en vikarie som skolbibliotekarie där du har en viktig roll att bidra till skolans måluppfyllelse då vår nuvarande skolbibliotekarie ska prova annat arbete i tre månader. Du ska arbeta för att elevers lust att läsa ökar och att elevernas resultat förbättras i alla ämnen.  Du ansvarar för att tillsammans med pedagogerna utveckla skolan/skolbiblioteket till en plats för läsglädje, kultur och källkritiskt tänkande. Du arbetar aktivt med läsfrämjande insatser i alla årskurser. Du är en inspiratör och resurs i skolornas arbete och en viktig pedagog när det gäller informationssökning, källkritik och nätetik utifrån skolans läroplan.  Du är delaktig i inköp av medier och är behjälplig för elever i behov av kompensatoriska lösningar. Du ingår i ett ämneslag(svenska) men är själv i din roll vilket betyder att det är viktigt att du har en förmåga att ta egna initiativ och driva arbetet framåt för att kunna uppnå målen för Teleborg centrums skolbibliotek. Vårt skolbibliotek är integrerat i ett folkbibliotek och du kommer att samarbeta bibliotekspersonal som arbetar där.   Är du en utåtriktad person som har en god pedagogisk förmåga? Vill du arbeta för att öka läslusten hos våra elever och utveckla nya sätt att lära. Då kan detta vara en tjänst som är intressant för dig!   Viktiga mål för skolbibliotekarietjän  KVALIFIKATIONER Vi söker dig som har utbildning i bibliotek- och informationsvetenskap eller en pedagogisk bakgrund som till exempel lärare. Du kan också söka om du har stor erfarenhet av arbetsområdet.  Som person är du social och utåtriktad. Du har god förmåga att bemöta alla typer av människor, speciellt barn/ungdomar. Du har en pedagogisk förmåga och du är flexibel och kan lätt se vilket behov som finns. Du är en person som gillar att ta initiativ och du vågar prova nya arbetsmetoder för att förbättra, förändra och utveckla. Samarbete är viktigt, både med övrig personal på skolan, bibliotekspersonal och våra elever. Du har en positiv attityd med förmåga att engagera och entusiasmera de du arbetar med.   Vid lägger stor vikt vid personlig lämplighet.   ÖVRIGT Växjö kommuns anställda ska spegla den mångfald som finns i kommunen. Vi välkomnar sökande oberoende av till exempel ålder, kön och etnicitet.  Det kan bli aktuellt med löpande intervjuer så skicka gärna din ansökan så snart som möjligt.  Ansökan sker enbart via Offentliga jobbs rekryteringsverktyg (www.offentligajobb.se).  Medtag utbildningsbevis och eventuell yrkeslegitimation vid intervju. För att bli aktuell för anställning behöver du visa två utdrag från belastningsregistret innan du anställs. Beställning av utdrag ur belastningsregistret kan göras på https://polisen.se/tjanster-tillstand/belastningsregistret/ och du ska använda dels det som heter Arbete inom skola eller förskola (442.5) samt Kontroll av egna uppgifter i belastningsregistret (442.3).  För interna kandidater kan det bli aktuellt med annat tillträdesdatum. För mer information se Beslut gällande tidpunkt för byte av enhet vid rekrytering av intern kandidat inom utbildningsförvaltningen på intranätet.  Läs mer om vad som händer i våra skolor/förskolor på pedagog.vaxjo.se.  Till bemannings- och rekryteringsföretag eller till dig som är försäljare: Vi undanber oss vänligen men bestämt direktkontakt med bemannings- och rekryteringsföretag samt försäljare av ytterligare annonser.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Skolbibliotekarie** till Teleborg centrum skola N... | x | x | 17 | 17 | [Skolbibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n9RX_nnz_ZYF) |
| 2 | ...nytta för dem vi är till för! **Växjö** kommun är Kronobergs största ... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 3 | ... för dem vi är till för! Växjö** kommun** är Kronobergs största arbetsg... | x |  |  | 7 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 4 | ... är till för! Växjö kommun är **Kronobergs** största arbetsgivare som varj... | x |  |  | 10 | [Kronobergs län, **region**](http://data.jobtechdev.se/taxonomy/concept/tF3y_MF9_h5G) |
| 5 | ...iktigt? Välkommen till oss på **Växjö** kommun!  ARBETSUPPGIFTER Vä... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 6 | ...t? Välkommen till oss på Växjö** kommun**!  ARBETSUPPGIFTER Välkommen... | x |  |  | 7 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 7 | ...-9, grundsärskola för 7-9 och **Växjö** International Baccalaureate g... |  | x |  | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 8 | ...på skolan både hos elever och **personal**.   Som arbetstagare hos oss ä... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 9 | ...barn och elever.  Vi söker en **vikarie** som skolbibliotekarie där du ... | x |  |  | 7 | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| 10 | ...ver.  Vi söker en vikarie som **skolbibliotekarie** där du har en viktig roll att... | x | x | 17 | 17 | [Skolbibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n9RX_nnz_ZYF) |
| 11 | ...luppfyllelse då vår nuvarande **skolbibliotekarie** ska prova annat arbete i tre ... | x |  |  | 17 | [Skolbibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n9RX_nnz_ZYF) |
| 12 | ...d pedagogerna utveckla skolan/**skolbiblioteket** till en plats för läsglädje, ... | x |  |  | 15 | [Skolbibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n9RX_nnz_ZYF) |
| 13 | ...ngar. Du ingår i ett ämneslag(**svenska**) men är själv i din roll vilk... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 14 | ...k och du kommer att samarbeta **bibliotekspersonal** som arbetar där.   Är du en u... | x | x | 18 | 18 | [Bibliotekspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/X67w_bmm_QFR) |
| 15 | ...KATIONER Vi söker dig som har **utbildning i bibliotek- och **informationsvetenskap eller en... | x |  |  | 28 | [Bibliotekarieutbildning samt biblioteks- och informationsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/FsKa_aFC_yPT) |
| 16 | ...r utbildning i bibliotek- och **informationsvetenskap** eller en pedagogisk bakgrund ... | x | x | 21 | 21 | [Bibliotekarieutbildning samt biblioteks- och informationsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/FsKa_aFC_yPT) |
| 17 | ...te är viktigt, både med övrig **personal** på skolan, bibliotekspersonal... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 18 | ...med övrig personal på skolan, **bibliotekspersonal** och våra elever. Du har en po... | x | x | 18 | 18 | [Bibliotekspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/X67w_bmm_QFR) |
| 19 | ...rsonlig lämplighet.   ÖVRIGT **Växjö** kommuns anställda ska spegla ... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 20 | ...ig lämplighet.   ÖVRIGT Växjö** kommuns** anställda ska spegla den mång... | x |  |  | 8 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis, industriisolerare, **skill**](http://data.jobtechdev.se/taxonomy/concept/33Lf_vKJ_3U3) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Arbetsplattform, utbildningsbevis, **skill**](http://data.jobtechdev.se/taxonomy/concept/8Xuo_Ss8_8cE) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis, VVS-montör, **skill**](http://data.jobtechdev.se/taxonomy/concept/DaGA_bsf_rVQ) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis, industrirörmontör, **skill**](http://data.jobtechdev.se/taxonomy/concept/FFLE_Mys_a7P) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis, mobila arbetsplattformar, **skill**](http://data.jobtechdev.se/taxonomy/concept/KfEn_V5y_npV) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis, fallskydd, **skill**](http://data.jobtechdev.se/taxonomy/concept/WLAi_BDr_5fW) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis, ställningsbyggnad 2 - 9 meter, **skill**](http://data.jobtechdev.se/taxonomy/concept/nkhA_jPz_pcE) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis, industriplåtslagare, **skill**](http://data.jobtechdev.se/taxonomy/concept/p6PZ_4XW_VgH) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis, VVS-isolerare, **skill**](http://data.jobtechdev.se/taxonomy/concept/qCUQ_2ZH_rKU) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis för storkran, **skill**](http://data.jobtechdev.se/taxonomy/concept/ws2R_MBQ_svX) |
| 21 | ...w.offentligajobb.se).  Medtag **utbildningsbevis** och eventuell yrkeslegitimati... |  | x |  | 16 | [Utbildningsbevis, säkra lyft, **skill**](http://data.jobtechdev.se/taxonomy/concept/ynWd_oCo_o61) |
| 22 | ...heter Arbete inom skola eller **förskola** (442.5) samt Kontroll av egna... | x | x | 8 | 8 | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| | **Overall** | | | **121** | **417** | 121/417 = **29%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Utbildningsbevis, industriisolerare, **skill**](http://data.jobtechdev.se/taxonomy/concept/33Lf_vKJ_3U3) |
|  | x |  | [Arbetsplattform, utbildningsbevis, **skill**](http://data.jobtechdev.se/taxonomy/concept/8Xuo_Ss8_8cE) |
|  | x |  | [Utbildningsbevis, VVS-montör, **skill**](http://data.jobtechdev.se/taxonomy/concept/DaGA_bsf_rVQ) |
|  | x |  | [Utbildningsbevis, industrirörmontör, **skill**](http://data.jobtechdev.se/taxonomy/concept/FFLE_Mys_a7P) |
| x | x | x | [Bibliotekarieutbildning samt biblioteks- och informationsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/FsKa_aFC_yPT) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [Utbildningsbevis, mobila arbetsplattformar, **skill**](http://data.jobtechdev.se/taxonomy/concept/KfEn_V5y_npV) |
|  | x |  | [Utbildningsbevis, fallskydd, **skill**](http://data.jobtechdev.se/taxonomy/concept/WLAi_BDr_5fW) |
| x | x | x | [Bibliotekspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/X67w_bmm_QFR) |
| x |  |  | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| x | x | x | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| x | x | x | [Skolbibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n9RX_nnz_ZYF) |
|  | x |  | [Utbildningsbevis, ställningsbyggnad 2 - 9 meter, **skill**](http://data.jobtechdev.se/taxonomy/concept/nkhA_jPz_pcE) |
| x | x | x | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
|  | x |  | [Utbildningsbevis, industriplåtslagare, **skill**](http://data.jobtechdev.se/taxonomy/concept/p6PZ_4XW_VgH) |
|  | x |  | [Utbildningsbevis, VVS-isolerare, **skill**](http://data.jobtechdev.se/taxonomy/concept/qCUQ_2ZH_rKU) |
| x |  |  | [Kronobergs län, **region**](http://data.jobtechdev.se/taxonomy/concept/tF3y_MF9_h5G) |
|  | x |  | [Utbildningsbevis för storkran, **skill**](http://data.jobtechdev.se/taxonomy/concept/ws2R_MBQ_svX) |
|  | x |  | [Utbildningsbevis, säkra lyft, **skill**](http://data.jobtechdev.se/taxonomy/concept/ynWd_oCo_o61) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/20 = **30%** |