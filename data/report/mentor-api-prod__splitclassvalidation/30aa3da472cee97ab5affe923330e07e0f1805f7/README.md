# Results for '30aa3da472cee97ab5affe923330e07e0f1805f7'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [30aa3da472cee97ab5affe923330e07e0f1805f7](README.md) | 1 | 1857 | 14 | 7 | 61/174 = **35%** | 4/13 = **31%** |

## Source text

Jobba med ommärkning av gods hos attraktivt företag i Älmhult Är du noggrann och har bra energi, sök jobbet! Du kommer att jobba med ommärkning av gods hos vår kund i Älmhult, start direkt efter sommaren.  Arbetsuppgifter Vi söker industriarbetare för uppdrag som startar direkt efter sommaren och sträcker sig en till två månader framåt. Du kommer att arbeta med att märka om gods hos vår kund. Du kan även få packa, plocka samt jobba med övrig materialhantering. Som person trivs du med att arbeta i ett högt tempo och har en god samarbetsförmåga.  Din bakgrund Vi ser gärna att du har tidigare industrivana och gärna erfarenhet av pack, plock, märkning eller motsvarande. Egentligen behövs ingen speciell erfarenhet, det viktiga är att du har rätt inställning till arbete och att du är energisk och nyfiken.  Personliga egenskaper Som person är du lättlärd, noggrann och ansvarstagande i ditt arbetssätt. Du tycker om att arbeta i högt tempo och är stresstålig. Vi ser även att du är samarbetsvillig och du gärna arbetar i team. Du ska vara driven och kunna arbeta självständigt med ett målinriktat tänk för att uppfylla kundens mål.  Anställningsform/omfattning/tjänstgöringsort  Visstidsanställning / Heltid, dagtid eller 2-skift/ Älmhult  Tillträdesdag  Start direkt efter sommaren.  Välkommen med din ansökan!  Forma framtiden med oss! Lernia är ett av Sveriges ledande företag inom kompetensutveckling och kompetensförsörjning. Vi finns över hela landet och har tjänster inom vuxenutbildning, bemanning, rekrytering och omställning. Varje år får 10 000 bemanningskonsulter jobb på Lernia. Som konsult kan du prova på olika roller, dra nytta av din befintliga kompetens eller kanske ta nästa steg i karriären, samtidigt som du har samma trygghet som hos andra företag. Välkommen till Lernia – tillsammans formar vi din framtid!  Läs mer på lernia.se.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...gods hos attraktivt företag i **Älmhult** Är du noggrann och har bra en... | x |  |  | 7 | [Älmhult, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EK6X_wZq_CQ8) |
| 2 | ...ktivt företag i Älmhult Är du **noggrann** och har bra energi, sök jobbe... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 3 | ...rkning av gods hos vår kund i **Älmhult**, start direkt efter sommaren.... | x |  |  | 7 | [Älmhult, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EK6X_wZq_CQ8) |
| 4 | ...en.  Arbetsuppgifter Vi söker **industriarbetare** för uppdrag som startar direk... | x |  |  | 16 | [Industriarbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fpA8_XFc_122) |
| 5 | ..., plocka samt jobba med övrig **materialhantering**. Som person trivs du med att ... |  | x |  | 17 | [underhålla utrustning för materialhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KRDD_CbQ_xA1) |
| 5 | ..., plocka samt jobba med övrig **materialhantering**. Som person trivs du med att ... |  | x |  | 17 | [använda utrustning för materialhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xpFq_J44_sHW) |
| 6 | ...ivana och gärna erfarenhet av **pack**, plock, märkning eller motsva... | x |  |  | 4 | [packare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/RH1P_4KB_1WT) |
| 7 | ...och gärna erfarenhet av pack, **plock**, märkning eller motsvarande. ... | x |  |  | 5 | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
| 8 | ...er Som person är du lättlärd, **noggrann** och ansvarstagande i ditt arb... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 9 | ... är du lättlärd, noggrann och **ansvarstagande** i ditt arbetssätt. Du tycker ... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 10 | ...tt arbeta i högt tempo och är **stresstålig**. Vi ser även att du är samarb... | x | x | 11 | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 11 | ... Du ska vara driven och kunna **arbeta självständigt** med ett målinriktat tänk för ... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 12 | .../omfattning/tjänstgöringsort  **Visstidsanställning** / Heltid, dagtid eller 2-skif... | x |  |  | 19 | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| 13 | ...ngsort  Visstidsanställning / **Heltid**, dagtid eller 2-skift/ Älmhul... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 14 | ...Heltid, dagtid eller 2-skift/ **Älmhult**  Tillträdesdag  Start direkt ... | x |  |  | 7 | [Älmhult, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EK6X_wZq_CQ8) |
| 15 | ...den med oss! Lernia är ett av **Sveriges** ledande företag inom kompeten... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **61** | **174** | 61/174 = **35%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Älmhult, **municipality**](http://data.jobtechdev.se/taxonomy/concept/EK6X_wZq_CQ8) |
|  | x |  | [underhålla utrustning för materialhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KRDD_CbQ_xA1) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [packare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/RH1P_4KB_1WT) |
| x |  |  | [plocka order för leverans, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZUh3_Nof_FF9) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Industriarbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fpA8_XFc_122) |
| x |  |  | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [använda utrustning för materialhantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xpFq_J44_sHW) |
| x | x | x | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **4** | 4/13 = **31%** |