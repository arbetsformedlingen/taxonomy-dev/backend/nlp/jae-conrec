# Results for '04a403227b39a2d6c99b607b5c4eeb07074f41e4'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [04a403227b39a2d6c99b607b5c4eeb07074f41e4](README.md) | 1 | 2018 | 4 | 5 | 12/78 = **15%** | 1/6 = **17%** |

## Source text

Nu söker vi dig som är svetsare!  Vad vi erbjuder Som uthyrd konsult via oss på Veldi Bemanning får du en marknadsmässig lön, friskvårdsbidrag, pensionsavsättning och anslutning till vårt kollektivavtal. Vi kommer ställa höga krav på dig som konsult och förväntar oss att du också ställer höga krav på oss som arbetsgivare. Det är en självklarhet för oss att du känner dig trygg och att du ska trivas i din anställning. Oavsett var du är i din karriär så ska konsultrollen hos oss på Veldi Bemanning ge dig erfarenhet och kunskap som du kan ha med dig till din nästa utmaning.  Dina arbetsuppgifter Våra kunder finns i hela Östergötland och många av företagen efterfrågar nu dig som är utbildad svetsare!   Eftersom arbetet sker ute hos våra kunder är det viktigt att man har en positiv inställning, lätt för att samarbeta i grupp och en vilja att lära sig nya saker. Meriterande är om du som söker även har kunskap om robotsvets då detta kan efterfrågas på vissa tjänster.      Om oss Veldi Bemanning är ett auktoriserat rekryterings- och bemanningsföretag.  Vårt team förstärks av lång erfarenhet från branschen och vet hur viktigt det är med en stark lokal förankring.  Med vår utbildning Veldi Kompetens i ryggen står vi starka i vår helhet som ett komplett kompetensföretag. Vi ställer höga krav på oss som en partner i er kompetensförsörjning och grunderna hos är att ni ska känna att våra samarbeten byggs av förtroende, långsiktighet och engagemang! -Det ska kännas enkelt och tryggt att jobba med oss!  Om du vill veta mer? Du får gärna kontakta mig om du har frågor om tjänsten eller har frågor om hur processen ser ut i sin helhet.  Jag som arbetar med detta uppdrag är konsultchef Kevin och vid frågor om tjänsten når ni mig enklast på mail (se nedan)  Vi går igenom ansökningarna löpande och tjänsten kan komma att tillsättas innan sista ansökningsdatum, vänta därför inte med att skicka in din ansökan! Obs: vi tar endast emot ansökningar via hemsidan, inte per mail!  Vi ser fram emot att höra från dig!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Nu söker vi dig som är **svetsare**!  Vad vi erbjuder Som uthyrd ... |  | x |  | 8 | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| 1 | Nu söker vi dig som är **svetsare**!  Vad vi erbjuder Som uthyrd ... | x |  |  | 8 | [Svetsare, manuell, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/wf1E_nWA_EJp) |
| 2 | ...ning och anslutning till vårt **kollektivavtal**. Vi kommer ställa höga krav p... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 3 | ...fter Våra kunder finns i hela **Östergötland** och många av företagen efterf... | x | x | 12 | 12 | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
| 4 | ...frågar nu dig som är utbildad **svetsare**!   Eftersom arbetet sker ute ... |  | x |  | 8 | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| 4 | ...frågar nu dig som är utbildad **svetsare**!   Eftersom arbetet sker ute ... | x |  |  | 8 | [Svetsare, manuell, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/wf1E_nWA_EJp) |
| 5 | ...som söker även har kunskap om **robotsvets** då detta kan efterfrågas på v... | x |  |  | 10 | [EN 1418, Robotsvetsning/ISO 14732, Mekaniserad svetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/1ym6_zNX_hkG) |
| 5 | ...som söker även har kunskap om **robotsvets** då detta kan efterfrågas på v... |  | x |  | 10 | [Robotsvetsare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fcGC_Px4_Ygm) |
| | **Overall** | | | **12** | **78** | 12/78 = **15%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [EN 1418, Robotsvetsning/ISO 14732, Mekaniserad svetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/1ym6_zNX_hkG) |
|  | x |  | [Robotsvetsare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fcGC_Px4_Ygm) |
| x | x | x | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
|  | x |  | [Svetsare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ouXc_F9J_bAC) |
| x |  |  | [Svetsare, manuell, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/wf1E_nWA_EJp) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **1** | 1/6 = **17%** |