# Results for '1e392120cffc47c1ace75e4d77b0e253ca6ad473'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1e392120cffc47c1ace75e4d77b0e253ca6ad473](README.md) | 1 | 3284 | 28 | 17 | 127/334 = **38%** | 10/25 = **40%** |

## Source text

Teknisk dokumentatör till Volvo CE - DISTANS Vi söker en erfaren och engagerad Teknisk Dokumentatör till vår kund utanför Växjö men du kommer sitta remote.  Har du en utbildning och erfarenhet av teknisk dokumentation med mycket bildbehandling? Är du teknikintresserad och är en noggrann och driven lagspelare? Då kan tjänsten som Teknisk dokumentatör vara rätt för dig!  Ansök idag!  Det här är ett konsultuppdrag på heltid med start så snart som möjligt. För rätt person finns goda möjligheter till ett långt uppdrag eller övergång till kund.  Din roll  Som Teknisk dokumentatör så kommer du arbeta tillsammans med konstruktörer och andra nyckelpersoner för att skapa illustrativa dokument kring företagets produkter. Tjänsten kommer främst att supportera en avdelning i Skottland.  Du kommer ansvara för både utformning och innehåll i text och bild.  Om dig  Vi söker dig som:  - har utbildning och erfarenhet av att arbeta med dokumentation inom industri  - mycket god datorvana, system och hantering av information.  - meriterande är om du har en teknisk utbildning  - har goda kunskaper i både svenska och engelska, både muntligt och skriftligt  - har erfarenhet av ett eller flera följande program/system:   * Alladin/Uptime  * Adobe Photoshop  * Adobe Illustrator  * PTC Creo  * Composer  Du är ansvarstagande, kommunikativ och har lätt för att samarbeta.  Som person tror vi att du är driven och effektiv. Du uppskattar att arbeta i ett högt tempo och är inte rädd för att ta för dig. Samtidigt ser vi att du har en god samarbetsförmåga där du trivs med att jobba sammansvetsat med andra. Då du i rollen också hanterar en hel del information och data är det viktigt att du har en god analytisk förmåga samt har ett sinne för ordning och struktur. Vi ser även att du är lösningsinriktad och är nyfiken och hungrig på att lära dig nya saker.  Vad kan Jefferson Wells erbjuda dig?  Jefferson Wells är specialister på kompetensförsörjning av chefer och specialister inom Ekonomi, HR, Executive, Inköp, Logistik, Marknad, Försäljning och Ingenjörsområdet. Vi är ett av få bolag som samarbetar med företag, myndigheter och organisationer inom privat och offentlig sektor i hela Sverige. Våra konsultchefer har gedigen kunskap inom just ditt område. Som konsult hos oss är din karriär och utveckling alltid i fokus. Vår verksamhet bygger på att våra anställda trivs och utvecklas. Vi erbjuder en trygg anställning med allt vad det innebär: månadslön, kollektivavtal, försäkringar m.m. Läs mer om hur det är att vara konsult på Jefferson Wells: https://www.jeffersonwells.se/sv/karriar/konsult-pa-jefferson-wells   Ansökan  Varmt välkommen med din ansökan genom att registrera ditt CV, urval sker löpande.  Sök tjänsten i dag!  Har du frågor är du välkommen att kontakta Daniella Bergström på telefonnummer 0455 - 30 27 88, alternativt per mail till daniella.bergstrom@manpower.se.  Från vecka 29 kontaktar du Cecilia Lindgren på telefonnummer 035-161 446, alternativt per mail till cecilia.a.lindgren@manpower.se  Observera att vi inte tar emot ansökningar via e-post, utan du söker tjänsten med en registrerad profil på vår hemsida genom att följa ansökningslänken i annonsen. Vi kan komma att tillsätta uppdrag redan innan ansökningstiden passerat, så skicka in din ansökan i dag!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... dokumentatör till Volvo CE - **DISTANS** Vi söker en erfaren och engag... | x |  |  | 7 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 2 | ...entatör till vår kund utanför **Växjö** men du kommer sitta remote.  ... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 3 | ...för Växjö men du kommer sitta **remote**.  Har du en utbildning och er... | x |  |  | 6 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 4 | ... utbildning och erfarenhet av **teknisk dokumentation** med mycket bildbehandling? Är... | x | x | 21 | 21 | [Teknisk dokumentation, **skill**](http://data.jobtechdev.se/taxonomy/concept/AEgv_4gP_ndv) |
| 5 | ...nisk dokumentation med mycket **bildbehandling**? Är du teknikintresserad och ... | x | x | 14 | 14 | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
| 6 | ...u teknikintresserad och är en **noggrann** och driven lagspelare? Då kan... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 7 | ... här är ett konsultuppdrag på **heltid** med start så snart som möjlig... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 8 | ... erfarenhet av att arbeta med **dokumentation** inom industri  - mycket god d... |  | x |  | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 9 | ... erfarenhet av att arbeta med **dokumentation inom industri**  - mycket god datorvana, syst... | x |  |  | 27 | [Dokumentation, processindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/AiwS_Mhv_ttk) |
| 10 | ...ation inom industri  - mycket **god datorvana**, system och hantering av info... | x |  |  | 13 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 11 | ...- meriterande är om du har en **teknisk utbildning**  - har goda kunskaper i både ... | x | x | 18 | 18 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 12 | ...  - har goda kunskaper i både **svenska** och engelska, både muntligt o... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 13 | ... kunskaper i både svenska och **engelska**, både muntligt och skriftligt... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 14 | ...system:   * Alladin/Uptime  * **Adobe Photoshop**  * Adobe Illustrator  * PTC C... | x | x | 15 | 15 | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
| 15 | .../Uptime  * Adobe Photoshop  * **Adobe Illustrator**  * PTC Creo  * Composer  Du ä... | x | x | 17 | 17 | [2D-grafik-Adobe Illustrator, **skill**](http://data.jobtechdev.se/taxonomy/concept/FKub_QFH_Btk) |
| 15 | .../Uptime  * Adobe Photoshop  * **Adobe Illustrator**  * PTC Creo  * Composer  Du ä... |  | x |  | 17 | [Adobe Illustrator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bGA6_xqh_Cvv) |
| 16 | ...be Illustrator  * PTC Creo  * **Composer**  Du är ansvarstagande, kommun... |  | x |  | 8 | [Film/redigering-Avid Media Composer, **skill**](http://data.jobtechdev.se/taxonomy/concept/jYGN_rDK_tM2) |
| 17 | ...* PTC Creo  * Composer  Du är **ansvarstagande**, kommunikativ och har lätt fö... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 18 | ...mposer  Du är ansvarstagande, **kommunikativ** och har lätt för att samarbet... | x |  |  | 12 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 19 | ...unikativ och har lätt för att **samarbeta**.  Som person tror vi att du ä... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 20 | ...digt ser vi att du har en god **samarbetsförmåga** där du trivs med att jobba sa... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 21 | ... är det viktigt att du har en **god analytisk förmåga** samt har ett sinne för ordnin... | x |  |  | 21 | [arbeta analytiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wLyJ_Zpf_tgC) |
| 22 | ...ruktur. Vi ser även att du är **lösningsinriktad** och är nyfiken och hungrig på... | x |  |  | 16 | [Lösningsfokuserat arbetssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/94uF_bkH_QF5) |
| 23 | ... chefer och specialister inom **Ekonomi**, HR, Executive, Inköp, Logist... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 24 | ...konomi, HR, Executive, Inköp, **Logistik**, Marknad, Försäljning och Ing... |  | x |  | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 25 | ...t och offentlig sektor i hela **Sverige**. Våra konsultchefer har gedig... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 26 | ...t vad det innebär: månadslön, **kollektivavtal**, försäkringar m.m. Läs mer om... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **127** | **334** | 127/334 = **38%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Lösningsfokuserat arbetssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/94uF_bkH_QF5) |
| x | x | x | [Teknisk dokumentation, **skill**](http://data.jobtechdev.se/taxonomy/concept/AEgv_4gP_ndv) |
| x |  |  | [Dokumentation, processindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/AiwS_Mhv_ttk) |
| x | x | x | [2D-grafik-Adobe Illustrator, **skill**](http://data.jobtechdev.se/taxonomy/concept/FKub_QFH_Btk) |
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x | x | x | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
|  | x |  | [Adobe Illustrator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bGA6_xqh_Cvv) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Film/redigering-Avid Media Composer, **skill**](http://data.jobtechdev.se/taxonomy/concept/jYGN_rDK_tM2) |
|  | x |  | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x | x | x | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| x |  |  | [arbeta analytiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wLyJ_Zpf_tgC) |
| x | x | x | [2D-grafik-Adobe Photoshop, **skill**](http://data.jobtechdev.se/taxonomy/concept/xNyv_PTi_fSa) |
|  | x |  | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **10** | 10/25 = **40%** |