# Results for '0d54cc46e277f0841bcf620c72e2d08579556496'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0d54cc46e277f0841bcf620c72e2d08579556496](README.md) | 1 | 2683 | 16 | 13 | 138/179 = **77%** | 8/11 = **73%** |

## Source text

IT-supporttekniker Nu söker vi dig, som tillsammans med oss vill göra nytta för dem vi är till för! Växjö kommun är Kronobergs största arbetsgivare som varje dag, året runt, arbetar för våra invånare, företag och besökare. Tillsammans skapar vi en hållbar morgondag och en attraktiv kommun att bo, leva och verka i – idag och i framtiden. Det innebär ett arbete där vi utmanar oss att tänka nytt, lära och utvecklas med varandra. Vill du vara en del i ett stort sammanhang där du får vara med och göra skillnad på riktigt? Välkommen till oss på Växjö kommun!  ARBETSUPPGIFTER Vi söker nu en IT-supporttekniker till vårt servicedeskgäng. Du kommer att arbeta med service och support i vår första linje. Tillsammans med kollegor kommer du att vara IT-avdelningens ansikte utåt och ingå i en IT-organisation med höga ambitioner och en strävan att alltid bli lite bättre.   Som IT-supporttekniker hos oss kommer du tillsammans med kollegor att: • med stort intresse och engagemang ge bästa möjliga service och support till våra användare och elever • ta emot och prioritera inkommande ärenden via telefon och vår ärendeportal • felsöka och åtgärda ärenden på ett lärande och instruerande sätt • samla in information och eskalera ärenden vidare i de fall så behövs • bidra till ännu bättre service och support genom att föreslå förbättringar när behov finns  KVALIFIKATIONER Vi söker dig som har lägst gymnasieexamen, gärna kompletterad med en eftergymnasial utbildning inom IT eller motsvarande arbetslivserfarenhet. Du har arbetslivserfarenhet inom kundservice eller supportyrket med god telefonvana.    För att trivas och lyckas i rollen har du förmåga att kommunicera med användare men även med tekniker inom avdelningen. Du har hög social kompetens och ett stort teknikintresse. Du har ett brinnande intresse för service och bemötande och, kanske det allra viktigaste, du har ett stort engagemang för kunden samt vilja och nyfikenhet att lära dig mer. Du har god förmåga att uttrycka dig på svenska i tal och skrift samt god förståelse i engelska.   Det är meriterande om du även har:  • kunskaper i Microsofts klientapplikationer och klientoperativsystem • känner till ITIL och LEAN då vi arbetar utifrån dessa metoder • kunskap i hantering/administrering av Ipad, Iphone och Androidenheter   Välkommen med din ansökan!  ÖVRIGT Rekrytering sker löpande.  Växjö kommun erbjuder en attraktiv arbetsplats som är fri från diskriminering och ger lika möjligheter för alla.    Till bemannings- och rekryteringsföretag eller till dig som är försäljare: Vi undanber oss vänligen men bestämt direktkontakt med bemannings- och rekryteringsföretag samt försäljare av ytterligare annonser.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **IT-supporttekniker** Nu söker vi dig, som tillsamm... | x | x | 18 | 18 | [IT-supporttekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZBUk_h8D_KZA) |
| 2 | ...nytta för dem vi är till för! **Växjö** kommun är Kronobergs största ... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 3 | ... för dem vi är till för! Växjö** kommun** är Kronobergs största arbetsg... | x |  |  | 7 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 4 | ... är till för! Växjö kommun är **Kronobergs** största arbetsgivare som varj... | x |  |  | 10 | [Kronobergs län, **region**](http://data.jobtechdev.se/taxonomy/concept/tF3y_MF9_h5G) |
| 5 | ...iktigt? Välkommen till oss på **Växjö** kommun!  ARBETSUPPGIFTER Vi... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 6 | ...t? Välkommen till oss på Växjö** kommun**!  ARBETSUPPGIFTER Vi söker ... | x |  |  | 7 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 7 | ...RBETSUPPGIFTER Vi söker nu en **IT-supporttekniker** till vårt servicedeskgäng. Du... | x | x | 18 | 18 | [IT-supporttekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZBUk_h8D_KZA) |
| 8 | ...d kollegor kommer du att vara **IT**-avdelningens ansikte utåt och... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 9 | ...ns ansikte utåt och ingå i en **IT**-organisation med höga ambitio... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 10 | ...alltid bli lite bättre.   Som **IT-supporttekniker** hos oss kommer du tillsammans... | x | x | 18 | 18 | [IT-supporttekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZBUk_h8D_KZA) |
| 11 | ...ER Vi söker dig som har lägst **gymnasieexamen**, gärna kompletterad med en ef... | x | x | 14 | 14 | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| 12 | ...en, gärna kompletterad med en **eftergymnasial utbildning** inom IT eller motsvarande arb... | x | x | 25 | 25 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 13 | ...ftergymnasial utbildning inom **IT** eller motsvarande arbetslivse... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 14 | ...har arbetslivserfarenhet inom **kundservice** eller supportyrket med god te... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 15 | ...d förmåga att uttrycka dig på **svenska** i tal och skrift samt god för... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 16 | ... skrift samt god förståelse i **engelska**.   Det är meriterande om du ä... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 17 | ...toperativsystem • känner till **ITIL** och LEAN då vi arbetar utifrå... |  | x |  | 4 | [arbeta i en ITIL-baserad miljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZJS4_Han_D8x) |
| 18 | ...system • känner till ITIL och **LEAN** då vi arbetar utifrån dessa m... | x | x | 4 | 4 | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| 19 | ...GT Rekrytering sker löpande.  **Växjö** kommun erbjuder en attraktiv ... | x | x | 5 | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 20 | ...krytering sker löpande.  Växjö** kommun** erbjuder en attraktiv arbetsp... | x |  |  | 7 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| | **Overall** | | | **138** | **179** | 138/179 = **77%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Ständiga förbättringar/Lean, **skill**](http://data.jobtechdev.se/taxonomy/concept/74Mw_JUN_VKx) |
| x | x | x | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| x | x | x | [IT-supporttekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZBUk_h8D_KZA) |
|  | x |  | [arbeta i en ITIL-baserad miljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZJS4_Han_D8x) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| x | x | x | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| x |  |  | [Kronobergs län, **region**](http://data.jobtechdev.se/taxonomy/concept/tF3y_MF9_h5G) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/11 = **73%** |