# Results for '8028cec398a35e305c86a908bc2d85ea7905c939'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8028cec398a35e305c86a908bc2d85ea7905c939](README.md) | 1 | 1769 | 28 | 18 | 141/334 = **42%** | 10/17 = **59%** |

## Source text

Fönstermontör Är du vår nya fönstermontör? RJ Byggmontage AB är ett företag som inriktat sig på fönstermontage. Vi är i dag 7 anställda varav 6 som monterar fönster och dörrar på heltid. Vi arbetar för att alla skall utvecklas. Vi vidareutbildar oss inom olika områden för att våra montage skall vara i mästarklass. Vi utför fönster och dörrbyten åt Mockfjärds som är ledande inom fönster och dörrbyten.  Du kommer ingå i ett monteringsteam men kan även arbeta självständigt. Vi lägger stor vikt vid personlighet och yrkeskunnande hos våra montörer. För att lyckas som montör hos oss tror vi att du är en framåtriktad, social och samarbetsvillig person som gillar utmaningar och har ett öga för att göra det lilla extra. Du trivs med att arbeta i grupp och du tar eget ansvar.   Arbetet innehåller bland annat: -Komplett montering av fönster och dörrar. -Byte av takfönster. -Snickeriarbete med lister, smygar invändig och utvändigt. -Plåtarbete i samband med fönster och dörrbyten. Andra arbetsmoment inom snickeri kan förekomma.   Som montör hos oss skall du vara självgående och noggrann. Se möjligheter att kunna leverera det bästa fönstermontaget varje dag. Planera arbetsdagen efter arbetsorder för att säkerställa att montagen utförs korrekt. Du behärskar svenska språket såväl i tal som i skrift då vi arbetar utifrån arbetsorder. Bra fysik eftersom det förekommer tunga moment. Vi utgår från Kävlinge men arbetar i hela Skåne. Arbete i andra delar av landen kan förekomma. Erfarenhet från fönstermontering/snickeri är ett krav. Körkort är ett krav. BE är ett plus. RJ Byggmontage AB är ett företag som inriktat sig på fönstermontage men där även andra arbete kan förekomma.   Välkomna med ansökningar till richard@rjbyggmontage.se Richard Jacobsson 0704-404804

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Fönstermontör** Är du vår nya fönstermontör? ... | x | x | 13 | 13 | [fönstermontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xx76_sML_rbh) |
| 2 | Fönstermontör Är du vår nya **fönstermontör**? RJ Byggmontage AB är ett för... | x | x | 13 | 13 | [fönstermontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xx76_sML_rbh) |
| 3 | ...t företag som inriktat sig på **fönstermontage**. Vi är i dag 7 anställda vara... | x | x | 14 | 14 | [Fönstermontage/Fönstermontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zZpU_nc8_tDf) |
| 4 | ...i dag 7 anställda varav 6 som **monterar fönster** och dörrar på heltid. Vi arbe... | x |  |  | 16 | [Fönstermontage/Fönstermontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zZpU_nc8_tDf) |
| 5 | ...onterar fönster och dörrar på **heltid**. Vi arbetar för att alla skal... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 6 | ...om olika områden för att våra **montage** skall vara i mästarklass. Vi ... | x |  |  | 7 | [Fönstermontage/Fönstermontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zZpU_nc8_tDf) |
| 7 | ...t monteringsteam men kan även **arbeta självständigt**. Vi lägger stor vikt vid pers... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 8 | ...et och yrkeskunnande hos våra **montörer**. För att lyckas som montör ho... | x | x | 8 | 8 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 9 | ... montörer. För att lyckas som **montör** hos oss tror vi att du är en ... |  | x |  | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 9 | ... montörer. För att lyckas som **montör** hos oss tror vi att du är en ... | x |  |  | 6 | [fönstermontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xx76_sML_rbh) |
| 10 | ...håller bland annat: -Komplett **montering av fönster** och dörrar. -Byte av takfönst... | x |  |  | 20 | [Fönstermontage/Fönstermontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zZpU_nc8_tDf) |
| 11 | ...lett montering av fönster och **dörrar**. -Byte av takfönster. -Snicke... | x |  |  | 6 | [montera dörrar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cSdA_5gH_348) |
| 12 | ... fönster och dörrar. -Byte av **takfönster**. -Snickeriarbete med lister, ... |  | x |  | 10 | [installera takfönster, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ceig_ViL_iun) |
| 13 | ...dörrar. -Byte av takfönster. -**Snickeri**arbete med lister, smygar invä... | x | x | 8 | 8 | [Snickeriarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jB4B_uED_vQh) |
| 14 | ...-Byte av takfönster. -Snickeri**arbete** med lister, smygar invändig o... |  | x |  | 6 | [Snickeriarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jB4B_uED_vQh) |
| 15 | ...ygar invändig och utvändigt. -**Plåtarbete** i samband med fönster och dör... | x | x | 10 | 10 | [Plåtbearbetning, **skill**](http://data.jobtechdev.se/taxonomy/concept/SrzF_GXp_pFE) |
| 16 | ...yten. Andra arbetsmoment inom **snickeri** kan förekomma.   Som montör h... | x |  |  | 8 | [Snickeriarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jB4B_uED_vQh) |
| 17 | ...snickeri kan förekomma.   Som **montör** hos oss skall du vara självgå... |  | x |  | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 17 | ...snickeri kan förekomma.   Som **montör** hos oss skall du vara självgå... | x |  |  | 6 | [fönstermontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xx76_sML_rbh) |
| 18 | ... montör hos oss skall du vara **självgående** och noggrann. Se möjligheter ... | x |  |  | 11 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 19 | ...skall du vara självgående och **noggrann**. Se möjligheter att kunna lev... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 20 | ... att kunna leverera det bästa **fönstermontaget** varje dag. Planera arbetsdage... | x |  |  | 15 | [Fönstermontage/Fönstermontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zZpU_nc8_tDf) |
| 21 | ...order för att säkerställa att **montagen** utförs korrekt. Du behärskar ... | x |  |  | 8 | [Fönstermontage/Fönstermontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zZpU_nc8_tDf) |
| 22 | ... utförs korrekt. Du behärskar **svenska** språket såväl i tal som i skr... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 23 | ... utförs korrekt. Du behärskar **svenska språket** såväl i tal som i skrift då v... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 24 | ... arbetar utifrån arbetsorder. **Bra **fysik eftersom det förekommer ... | x |  |  | 4 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 25 | ...etar utifrån arbetsorder. Bra **fysik** eftersom det förekommer tunga... | x | x | 5 | 5 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 26 | ...fysik eftersom det förekommer **tunga moment**. Vi utgår från Kävlinge men a... | x |  |  | 12 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 27 | ...r tunga moment. Vi utgår från **Kävlinge** men arbetar i hela Skåne. Arb... |  | x |  | 8 | [Kävlinge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/5ohg_WJU_Ktn) |
| 28 | ...n Kävlinge men arbetar i hela **Skåne**. Arbete i andra delar av land... | x | x | 5 | 5 | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| 29 | ...an förekomma. Erfarenhet från **fönstermontering**/snickeri är ett krav. Körkort... | x | x | 16 | 16 | [Fönstermontage/Fönstermontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zZpU_nc8_tDf) |
| 30 | ...arenhet från fönstermontering/**snickeri** är ett krav. Körkort är ett k... | x |  |  | 8 | [Snickeriarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jB4B_uED_vQh) |
| 31 | ...ntering/snickeri är ett krav. **Körkort** är ett krav. BE är ett plus. ... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 32 | ...tt krav. Körkort är ett krav. **BE** är ett plus. RJ Byggmontage A... | x |  |  | 2 | [BE, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/bcFd_Vkt_KXL) |
| 33 | ...t företag som inriktat sig på **fönstermontage** men där även andra arbete kan... | x | x | 14 | 14 | [Fönstermontage/Fönstermontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zZpU_nc8_tDf) |
| | **Overall** | | | **141** | **334** | 141/334 = **42%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Kävlinge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/5ohg_WJU_Ktn) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| x | x | x | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Plåtbearbetning, **skill**](http://data.jobtechdev.se/taxonomy/concept/SrzF_GXp_pFE) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| x |  |  | [BE, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/bcFd_Vkt_KXL) |
| x |  |  | [montera dörrar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cSdA_5gH_348) |
|  | x |  | [installera takfönster, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ceig_ViL_iun) |
| x | x | x | [Snickeriarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jB4B_uED_vQh) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| x | x | x | [fönstermontör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xx76_sML_rbh) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [Fönstermontage/Fönstermontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zZpU_nc8_tDf) |
| | | **10** | 10/17 = **59%** |