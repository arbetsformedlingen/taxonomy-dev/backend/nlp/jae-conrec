# Results for 'b7c7df959e69e118343b8fbb46e3e1c299f6fe9d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b7c7df959e69e118343b8fbb46e3e1c299f6fe9d](README.md) | 1 | 738 | 1 | 19 | 16/281 = **6%** | 1/14 = **7%** |

## Source text

Modulens beskrivning Modul B enligt Svetskommssionens kursplan   Modulens mål "Detta är en teoretisk modul, deltagaren ska efter avslutad modul Kunna:  - grunderna om stål för svetsning - grunden för svetsförband och deras terminologi - inverkan av svetsvärmen på stål - inverkan av svetsning vid krympning, restspänningar och formförändringar, veta hur man minimerar formförändringar före, under och efter svetsning - diskontinuiteter och formavvikelser i svetsar - ha en överblick över de mest användbara smältsvetsmetoderna - veta hur man svetsar säkert på byggarbetsplatser - principerna för de grundläggande metoderna för oförstörande provning som används vid svetsning - ha kunskap om hur kvalitetssäkring fungerar vid svetsning "  

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... modul Kunna:  - grunderna om **stål** för svetsning - grunden för s... |  | x |  | 4 | [Stålprodukter/Verkstadsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/dhQh_Xhd_NSF) |
| 2 | ...nna:  - grunderna om stål för **svetsning** - grunden för svetsförband oc... |  | x |  | 9 | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| 3 | ...en för svetsförband och deras **terminologi** - inverkan av svetsvärmen på ... |  | x |  | 11 | [terminologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jddE_6Q2_8L7) |
| 4 | ... - inverkan av svetsvärmen på **stål** - inverkan av svetsning vid k... |  | x |  | 4 | [Stålprodukter/Verkstadsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/dhQh_Xhd_NSF) |
| 5 | ...svärmen på stål - inverkan av **svetsning** vid krympning, restspänningar... |  | x |  | 9 | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [specialist på oförstörande provning, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/6Lv8_rKM_opZ) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [Oförstörande provning: Virvelstömsprovning (ET), **skill**](http://data.jobtechdev.se/taxonomy/concept/9YMK_uqf_9Fa) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [Oförstörande provning: Visuell kontroll (VT), **skill**](http://data.jobtechdev.se/taxonomy/concept/JKSD_AC8_Z2S) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [Oförstörande provning: Materialidentifiering PMI, **skill**](http://data.jobtechdev.se/taxonomy/concept/NYdq_ND3_JrB) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [Oförstörande provning: Röntgenprovning (RT), **skill**](http://data.jobtechdev.se/taxonomy/concept/Te3y_NpL_tkQ) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [Oförstörande provning: Täthetsprovning (LT), **skill**](http://data.jobtechdev.se/taxonomy/concept/ThXs_ruQ_cKp) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [Oförstörande provning: Penetrantprovning (PT), **skill**](http://data.jobtechdev.se/taxonomy/concept/Y6zg_1oi_cDZ) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [oförstörande provning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cj8V_fh9_CP1) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [Oförstörande provning: Ultraljudsprovning (UT), **skill**](http://data.jobtechdev.se/taxonomy/concept/n6GW_QHy_LBm) |
| 6 | ...e grundläggande metoderna för **oförstörande provning** som används vid svetsning - h... |  | x |  | 21 | [Oförstörande provning: Magnetpulverprovning (MT), **skill**](http://data.jobtechdev.se/taxonomy/concept/qnK9_CBi_ZKw) |
| 7 | ...ande provning som används vid **svetsning** - ha kunskap om hur kvalitets... |  | x |  | 9 | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| 8 | ...svetsning - ha kunskap om hur **kvalitetssäkring** fungerar vid svetsning "   | x | x | 16 | 16 | [Kvalitetssäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/a1Sf_mey_gVk) |
| 9 | ...kvalitetssäkring fungerar vid **svetsning** "   |  | x |  | 9 | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| | **Overall** | | | **16** | **281** | 16/281 = **6%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [specialist på oförstörande provning, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/6Lv8_rKM_opZ) |
|  | x |  | [Oförstörande provning: Virvelstömsprovning (ET), **skill**](http://data.jobtechdev.se/taxonomy/concept/9YMK_uqf_9Fa) |
|  | x |  | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
|  | x |  | [Oförstörande provning: Visuell kontroll (VT), **skill**](http://data.jobtechdev.se/taxonomy/concept/JKSD_AC8_Z2S) |
|  | x |  | [Oförstörande provning: Materialidentifiering PMI, **skill**](http://data.jobtechdev.se/taxonomy/concept/NYdq_ND3_JrB) |
|  | x |  | [Oförstörande provning: Röntgenprovning (RT), **skill**](http://data.jobtechdev.se/taxonomy/concept/Te3y_NpL_tkQ) |
|  | x |  | [Oförstörande provning: Täthetsprovning (LT), **skill**](http://data.jobtechdev.se/taxonomy/concept/ThXs_ruQ_cKp) |
|  | x |  | [Oförstörande provning: Penetrantprovning (PT), **skill**](http://data.jobtechdev.se/taxonomy/concept/Y6zg_1oi_cDZ) |
| x | x | x | [Kvalitetssäkring, **skill**](http://data.jobtechdev.se/taxonomy/concept/a1Sf_mey_gVk) |
|  | x |  | [oförstörande provning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cj8V_fh9_CP1) |
|  | x |  | [Stålprodukter/Verkstadsprodukter, **skill**](http://data.jobtechdev.se/taxonomy/concept/dhQh_Xhd_NSF) |
|  | x |  | [terminologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jddE_6Q2_8L7) |
|  | x |  | [Oförstörande provning: Ultraljudsprovning (UT), **skill**](http://data.jobtechdev.se/taxonomy/concept/n6GW_QHy_LBm) |
|  | x |  | [Oförstörande provning: Magnetpulverprovning (MT), **skill**](http://data.jobtechdev.se/taxonomy/concept/qnK9_CBi_ZKw) |
| | | **1** | 1/14 = **7%** |