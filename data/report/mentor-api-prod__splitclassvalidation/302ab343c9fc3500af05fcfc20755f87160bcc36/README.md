# Results for '302ab343c9fc3500af05fcfc20755f87160bcc36'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [302ab343c9fc3500af05fcfc20755f87160bcc36](README.md) | 1 | 1086 | 8 | 13 | 89/120 = **74%** | 3/5 = **60%** |

## Source text

Servitris/servitör/runners Servitris/servitör/runner  Restaurang Quê söker en servitris/servitör som har god känsla för service och vill arbeta i en modern vietnamesisk restaurang. Vi tar väl hand om våra gäster och pratar gärna om mat och dryck.  Vi serverar endast avsmakningsmenyer under kvällstid vardag och helger, personen vi söker bör tycka om att jobba inom fine dining och ha en stolthet för sitt yrke och service.   Vi ser gärna att du är vill utvecklas inom yrket och är villig att börja från ingångsnivå. Du behöver vara bekväm med att hantera servis och klara ett högt tempo. Det finns en viss flexibilitet i arbetstiderna men det är viktigt att ställa upp vid behov.  Vi ser gärna att ni har följande för tjänsten: •	Någon yrkeserfarenhet •	Du har god samarbetsförmåga. •	Stresstålig  Du kommer främst att jobba med en servitör/servitris som är ansvarig för servicen och hjälpa denna med olika arbetsuppgifter. t.ex. *att hjälpa till och servera mat *duka av och duka om borden *diska tallrikar, bestick m.m *du ska även kunna meny samt kunna presentera maten för gästerna

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Servitris**/servitör/runners Servitris/se... | x | x | 9 | 9 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | Servitris**/**servitör/runners Servitris/ser... | x |  |  | 1 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 3 | Servitris/**servitör**/runners Servitris/servitör/ru... | x | x | 8 | 8 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 4 | Servitris/servitör/runners **Servitris**/servitör/runner  Restaurang Q... | x | x | 9 | 9 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 5 | ...ris/servitör/runners Servitris**/**servitör/runner  Restaurang Qu... | x |  |  | 1 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 6 | ...is/servitör/runners Servitris/**servitör**/runner  Restaurang Quê söker ... | x | x | 8 | 8 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 7 | ...rs Servitris/servitör/runner  **Restaurang** Quê söker en servitris/servit... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 8 | ...nner  Restaurang Quê söker en **servitris**/servitör som har god känsla f... | x | x | 9 | 9 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 9 | ...taurang Quê söker en servitris**/**servitör som har god känsla fö... | x |  |  | 1 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 10 | ...aurang Quê söker en servitris/**servitör** som har god känsla för servic... | x | x | 8 | 8 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 11 | ...beta i en modern vietnamesisk **restaurang**. Vi tar väl hand om våra gäst... | x |  |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 12 | ...r vara bekväm med att hantera **servis** och klara ett högt tempo. Det... | x |  |  | 6 | [Servis, **job-title**](http://data.jobtechdev.se/taxonomy/concept/7coY_HT4_rJc) |
| 12 | ...r vara bekväm med att hantera **servis** och klara ett högt tempo. Det... |  | x |  | 6 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 13 | ...u har god samarbetsförmåga. •	**Stresstålig**  Du kommer främst att jobba m... | x | x | 11 | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 14 | ...ommer främst att jobba med en **servitör**/servitris som är ansvarig för... | x | x | 8 | 8 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 15 | ...ämst att jobba med en servitör**/**servitris som är ansvarig för ... | x |  |  | 1 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 16 | ...mst att jobba med en servitör/**servitris** som är ansvarig för servicen ... | x | x | 9 | 9 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 17 | ... *duka av och duka om borden ***diska** tallrikar, bestick m.m *du sk... |  | x |  | 5 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| | **Overall** | | | **89** | **120** | 89/120 = **74%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Servis, **job-title**](http://data.jobtechdev.se/taxonomy/concept/7coY_HT4_rJc) |
|  | x |  | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x | x | x | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **3** | 3/5 = **60%** |