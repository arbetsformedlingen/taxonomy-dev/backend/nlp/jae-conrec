# Results for 'bf81c0bf00046ee828ca5584324cf813bc044255'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [bf81c0bf00046ee828ca5584324cf813bc044255](README.md) | 1 | 2555 | 13 | 21 | 61/504 = **12%** | 3/23 = **13%** |

## Source text

Som 3D-tekniker får du arbeta i en kraftigt växande bransch. Utvecklingen inom additiv teknik går fort, både på process- och materialsidan. 3D-printing går snabbt: från idé till färdig produkt på bara några timmar!    Utveckla med nya tekniker  Som 3D-tekniker använder du den senaste 3D-tekniken för utveckling och framtagning av nya produkter. Området 3D-printing, additiv tillverkning, är inte helt nytt, men det görs intensiva framsteg som möjliggör användning i större skala och mer affärsmässigt. Det pågår mycket experimenterande och utveckling av programvaror, skrivare och material. Nya former blir plötsligt möjliga, former som med vanliga metoder inte går att tillverka.    Allt fler använder 3D-teknik  Branschen har ett stort behov av 3D-tekniker med stor och bred kompetens. Allt fler industriella företag får upp ögonen för 3D-printtekniken och den additiva tillverkningen. 3D-teknik ger utrymme för nya tankebanor, samtidigt som kravet på kunskap ökar för att kunna utnyttja alla möjligheter som finns. Branscher som idag använder tekniken är konstruktörer och produktutvecklare, arkitekter, industridesigners, verktygstillverkare, gummi-, plast-, fordons-, verkstads- och medicinindustrin.    3D Lab med modern teknik  Xenter har investerat i två olika 3D Lab där de senaste industriella och ickeindustriella 3D-skrivarna inom 3D-print finns att tillgå. De studerande får tillgång till datorer som klarar av de avancerade CAD-programmen som krävs för 3D-tekniken. Xenter deltar på många externa mässor för att påverka och visa upp 3D-teknikens möjligheter.    Är du orädd och nyfiken  YH-utbildningen är framtagen tillsammans med branschen och är skräddarsydd efter behoven som arbetslivet efterfrågar. Är du en driven person som har ett intresse för framtidens produktionsmetoder och samtidigt orädd och nyfiken? Vågar du misslyckas för att ta dig framåt? Är du en problemlösare som gillar att arbeta kreativt och nyskapande? Ja, då passar 3D-teknikerutbildningen dig.    "I en allt mer konkurrensutsatt svensk tillverkningsindustri upplever vi som företag att behov av välutbildad personal är ett krav för fortsatt verksamhet. Behov av 3D-tekniker kommer växa i takt med den utveckling som idag sker, fler och fler varianter och uppdateringar på produkter och utrustning krävs för att möta marknadens designkrav och där har sådan kompetens en viktig funktion att fylla. Vi som företag har stora behov av denna typ av yrkeskompetens både på kort och lång sikt i de utvecklingsplaner vi har för verksamheten.”  Henrik Lundell, vd Prototal

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Som **3D-tekniker** får du arbeta i en kraftigt v... | x | x | 11 | 11 | [3D-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tMpg_7vt_J8G) |
| 2 | ...å process- och materialsidan. **3D-printing** går snabbt: från idé till fär... |  | x |  | 11 | [3D-printning, **skill**](http://data.jobtechdev.se/taxonomy/concept/XCdg_UJK_w2q) |
| 3 | ...tveckla med nya tekniker  Som **3D-tekniker** använder du den senaste 3D-te... | x | x | 11 | 11 | [3D-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tMpg_7vt_J8G) |
| 4 | ...u den senaste 3D-tekniken för **utveckling och framtagning av nya produkter**. Området 3D-printing, additiv... | x |  |  | 43 | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| 5 | ...ing av nya produkter. Området **3D-printing**, additiv tillverkning, är int... |  | x |  | 11 | [3D-printning, **skill**](http://data.jobtechdev.se/taxonomy/concept/XCdg_UJK_w2q) |
| 6 | ... Området 3D-printing, additiv **tillverkning**, är inte helt nytt, men det g... |  | x |  | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 7 | ...nschen har ett stort behov av **3D-tekniker** med stor och bred kompetens. ... | x | x | 11 | 11 | [3D-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tMpg_7vt_J8G) |
| 8 | ...som idag använder tekniken är **konstruktörer** och produktutvecklare, arkite... | x |  |  | 13 | [Konstruktör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/44U4_7Fz_GCy) |
| 9 | ...tekniken är konstruktörer och **produktutvecklare**, arkitekter, industridesigner... | x | x | 17 | 17 | [Produktutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/32bC_YdL_JtU) |
| 9 | ...tekniken är konstruktörer och **produktutvecklare**, arkitekter, industridesigner... |  | x |  | 17 | [produktutvecklare, skoindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/6TrA_GB4_tq6) |
| 9 | ...tekniken är konstruktörer och **produktutvecklare**, arkitekter, industridesigner... |  | x |  | 17 | [produktutvecklare, textil, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/MK8E_a1X_E6k) |
| 9 | ...tekniken är konstruktörer och **produktutvecklare**, arkitekter, industridesigner... |  | x |  | 17 | [produktutvecklare, lädervaror, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/URLF_N37_aAh) |
| 9 | ...tekniken är konstruktörer och **produktutvecklare**, arkitekter, industridesigner... |  | x |  | 17 | [Produktutvecklare, konfektion, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tn2F_2Aw_NWq) |
| 10 | ...ktörer och produktutvecklare, **arkitekter**, industridesigners, verktygst... | x |  |  | 10 | [Arkitekt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XMWR_V22_WS1) |
| 11 | ...roduktutvecklare, arkitekter, **industridesigners**, verktygstillverkare, gummi-,... | x |  |  | 17 | [Industridesigner, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XpZG_8pA_V2c) |
| 12 | ...rkitekter, industridesigners, **verktygstillverkare**, gummi-, plast-, fordons-, ve... | x |  |  | 19 | [Verktygsmakare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tDS5_hG2_ML5) |
| 13 | ...r som klarar av de avancerade **CAD-programmen** som krävs för 3D-tekniken. Xe... | x |  |  | 14 | [CAD-konstruktion/CAD-ritning, **skill**](http://data.jobtechdev.se/taxonomy/concept/oaiv_GsB_AFK) |
| 14 | ...r att ta dig framåt? Är du en **problemlösare** som gillar att arbeta kreativ... | x |  |  | 13 | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| 15 | ...och nyskapande? Ja, då passar **3D-teknikerutbildningen** dig.    "I en allt mer konkur... | x |  |  | 23 | [3D-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tMpg_7vt_J8G) |
| 16 | ...t mer konkurrensutsatt svensk **tillverkningsindustri** upplever vi som företag att b... |  | x |  | 21 | [Konstruktör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/44U4_7Fz_GCy) |
| 16 | ...t mer konkurrensutsatt svensk **tillverkningsindustri** upplever vi som företag att b... |  | x |  | 21 | [Processtekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4eYC_a1M_xtL) |
| 16 | ...t mer konkurrensutsatt svensk **tillverkningsindustri** upplever vi som företag att b... |  | x |  | 21 | [Civilingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/84z6_pWk_XJQ) |
| 16 | ...t mer konkurrensutsatt svensk **tillverkningsindustri** upplever vi som företag att b... |  | x |  | 21 | [Utvecklingsingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8r8B_8bP_usp) |
| 16 | ...t mer konkurrensutsatt svensk **tillverkningsindustri** upplever vi som företag att b... |  | x |  | 21 | [Automationstekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CC4H_YCh_77T) |
| 16 | ...t mer konkurrensutsatt svensk **tillverkningsindustri** upplever vi som företag att b... |  | x |  | 21 | [Produktionsledare, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NaMW_ie6_VVV) |
| 16 | ...t mer konkurrensutsatt svensk **tillverkningsindustri** upplever vi som företag att b... |  | x |  | 21 | [Kalibreringsingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/RU3Y_Gqt_h3c) |
| 16 | ...t mer konkurrensutsatt svensk **tillverkningsindustri** upplever vi som företag att b... |  | x |  | 21 | [Kundserviceingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cPyP_GEv_NJ5) |
| 16 | ...t mer konkurrensutsatt svensk **tillverkningsindustri** upplever vi som företag att b... |  | x |  | 21 | [Produktionsplanerare, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zmTy_qPb_e9e) |
| 17 | ...fortsatt verksamhet. Behov av **3D-tekniker** kommer växa i takt med den ut... | x | x | 11 | 11 | [3D-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tMpg_7vt_J8G) |
| | **Overall** | | | **61** | **504** | 61/504 = **12%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Produktutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/32bC_YdL_JtU) |
| x | x | x | [Konstruktör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/44U4_7Fz_GCy) |
|  | x |  | [Processtekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4eYC_a1M_xtL) |
|  | x |  | [produktutvecklare, skoindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/6TrA_GB4_tq6) |
|  | x |  | [Civilingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/84z6_pWk_XJQ) |
|  | x |  | [Utvecklingsingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8r8B_8bP_usp) |
|  | x |  | [Automationstekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CC4H_YCh_77T) |
|  | x |  | [produktutvecklare, textil, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/MK8E_a1X_E6k) |
|  | x |  | [Produktionsledare, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NaMW_ie6_VVV) |
|  | x |  | [Kalibreringsingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/RU3Y_Gqt_h3c) |
|  | x |  | [produktutvecklare, lädervaror, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/URLF_N37_aAh) |
| x |  |  | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
|  | x |  | [3D-printning, **skill**](http://data.jobtechdev.se/taxonomy/concept/XCdg_UJK_w2q) |
| x |  |  | [Arkitekt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XMWR_V22_WS1) |
| x |  |  | [Industridesigner, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XpZG_8pA_V2c) |
|  | x |  | [Kundserviceingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cPyP_GEv_NJ5) |
| x |  |  | [CAD-konstruktion/CAD-ritning, **skill**](http://data.jobtechdev.se/taxonomy/concept/oaiv_GsB_AFK) |
| x |  |  | [Verktygsmakare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tDS5_hG2_ML5) |
| x | x | x | [3D-tekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tMpg_7vt_J8G) |
|  | x |  | [Produktutvecklare, konfektion, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tn2F_2Aw_NWq) |
|  | x |  | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| x |  |  | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
|  | x |  | [Produktionsplanerare, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zmTy_qPb_e9e) |
| | | **3** | 3/23 = **13%** |