# Results for '213e2ebe2f6bd70711e2fb0f560984785aaefe5c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [213e2ebe2f6bd70711e2fb0f560984785aaefe5c](README.md) | 1 | 3977 | 15 | 24 | 122/401 = **30%** | 8/18 = **44%** |

## Source text

Instruktör Trafik sökes av Göteborgs Spårvägar Göteborgs Spårvägar har satt Göteborg i rörelse sedan 1879. Med 141 miljoner spårvagnsresor per år är vi idag den största operatören av kollektivtrafik i Göteborgsregionen. Vi arbetar varje dag, året om för att ge våra resenärer en trygg och säker resa till jobb, skola och fritid. Vill du följa med på vår kvalitetsutvecklingsresa till en spårvagnsresa i världsklass?  ARBETSUPPGIFTER Nu söker vi dig som vill bli trafikinstruktör på vår utbildningsenhet inom avdelningen HR och kultur!  Enheten består av cirka 30 medarbetare och vårt uppdrag är att säkerställa att både nya och befintliga medarbetare har rätt och relevant kompetens för att kunna utföra sitt arbete säkert och med god kvalitet. Det innebär att vi genomför utbildning för hela bolaget men främst spårvagnsförare och teknisk personal. Vi utbildar mycket i vår egen regi men upphandlar också utbildningstjänster när det är mer ändamålsenligt.  Vår utbildningsenhet kännetecknas av gemenskap, samarbete och intresse av att arbeta med människor. Som instruktör kommer du att skapa många kontakter och samarbeta med andra avdelningar inom Göteborgs Spårvägar. Du är dessutom en viktig kontakt för alla kursdeltagare och din uppgift är att undervisa, stödja och handleda dem genom utbildningen.  Huvudsakliga arbetsuppgifter  • Du utbildar spårvagnsförare (alla behörigheter) samt övriga personalkategorier. • Du deltar i planering, utformning samt utveckling av nya kurser och tar fram utbildningsmaterial. • Du bidrar aktivt till generella förbättringar inom enheten tillsammans med dina kollegor.  För att kunna utföra ditt arbete kommer du få en instruktörsutbildning i början av din anställning. Under utbildningen får du en introduktion till instruktörsrollen och handleds av dina kollegor.  KVALIFIKATIONER • Gymnasieutbildning eller annan utbildning som arbetsgivaren bedömer som motsvarande • Svenskt körkort med lägst behörighet B och avslutad prövotid • Godkänd Spårvagnsförarutbildning med lägst behörighet B • God förmåga att uttrycka dig på svenska såväl muntligt som skriftligt  • Hälsogodkännande för säkerhetstjänst enligt TSFS 2019:113 samt godkänt drogtest  Meriterade • Pedagogisk utbildning • God digital kompetens med goda kunskaper i Office 365 • Erfarenhet av att hålla utbildning • Erfarenhet av att arbeta som spårvagnsförare   Vi söker dig som är serviceinriktad med ett brinnande intresse för att lära ut till andra. Du är pedagogisk med god förmåga att förklara problem och information för olika mottagare och ur olika perspektiv. Du är dessutom kreativ och bidrar med nya idéer och perspektiv i arbetsrelaterade frågor samt i situationer som behöver lösas. Tjänsten innefattar många sociala kontaktytor varför du måste ha god samarbetsförmåga med förståelse för det gemensamma uppdraget och vilja att bidra till helheten. Du är också strukturerad och arbetar systematiskt, organiserat och metodiskt. Då vi allt mer arbetar med digitalt lärande ser vi gärna att du har ett intresse för digitalisering. Hos oss får du vara med och utveckla verksamheten till nya nivåer!  För tjänsten krävs hälsogodkännande för säkerhetstjänst enligt TSFS 2019:113 samt godkänt drogtest.  Vi kan komma att använda oss av arbetspsykologiska urvalstester i rekryteringsprocessen. Var extra uppmärksam på din e-post då det oftast är så vi kontaktar dig. Ibland hamnar e-post från vårt system i din skräppost så titta gärna där också.  Intervjuer och urval kan komma att ske löpande.  Välkommen att söka till en utvecklande verksamhet!  Provanställning sex månader tillämpas.    ÖVRIGT Vi tar enbart emot digitala ansökningar via vårt rekryteringsverktyg www.offentligajobb.se  Till annonsförsäljare och bemannings- och rekryteringsföretag: Vi undanber oss vänligen men bestämt direktkontakt från bemannings- och rekryteringsföretag samt försäljare av ytterligare jobbannonser. Göteborgs Spårvägar har upphandlade avtal.  http://www.goteborgssparvagar.se/

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... sökes av Göteborgs Spårvägar **Göteborg**s Spårvägar har satt Göteborg ... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 2 | ... Göteborgs Spårvägar har satt **Göteborg** i rörelse sedan 1879. Med 141... | x |  |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 3 | ...dag den största operatören av **kollektivtrafik** i Göteborgsregionen. Vi arbet... | x | x | 15 | 15 | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| 4 | ...eratören av kollektivtrafik i **Göteborgsregionen**. Vi arbetar varje dag, året o... | x |  |  | 17 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 5 | ...ldningsenhet inom avdelningen **HR** och kultur!  Enheten består a... | x |  |  | 2 | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
| 6 | ...g för hela bolaget men främst **spårvagnsförare** och teknisk personal. Vi utbi... |  | x |  | 15 | [Buss- och spårvagnsförare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/FiDm_2S8_gx7) |
| 6 | ...g för hela bolaget men främst **spårvagnsförare** och teknisk personal. Vi utbi... |  | x |  | 15 | [Spårvagnsförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GkMH_KEV_QKX) |
| 6 | ...g för hela bolaget men främst **spårvagnsförare** och teknisk personal. Vi utbi... |  | x |  | 15 | [Buss- och spårvagnsförare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/PLXw_QzY_dvg) |
| 6 | ...g för hela bolaget men främst **spårvagnsförare** och teknisk personal. Vi utbi... |  | x |  | 15 | [Buss- och spårvagnsförare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/TgyQ_cnb_2EY) |
| 6 | ...g för hela bolaget men främst **spårvagnsförare** och teknisk personal. Vi utbi... | x | x | 15 | 15 | [spårvagnsförare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bnFa_huz_Cpc) |
| 7 | ...t spårvagnsförare och teknisk **personal**. Vi utbildar mycket i vår ege... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 8 | ...Vi utbildar mycket i vår egen **regi** men upphandlar också utbildni... | x |  |  | 4 | [Regi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wqo3_zSi_eNj) |
| 9 | ...gen regi men upphandlar också **utbildningstjänster** när det är mer ändamålsenligt... |  | x |  | 19 | [Utbildningstjänster, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/ETsW_C5r_RPu) |
| 10 | ...rbetsuppgifter  • Du utbildar **spårvagnsförare** (alla behörigheter) samt övri... |  | x |  | 15 | [Buss- och spårvagnsförare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/FiDm_2S8_gx7) |
| 10 | ...rbetsuppgifter  • Du utbildar **spårvagnsförare** (alla behörigheter) samt övri... |  | x |  | 15 | [Spårvagnsförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GkMH_KEV_QKX) |
| 10 | ...rbetsuppgifter  • Du utbildar **spårvagnsförare** (alla behörigheter) samt övri... |  | x |  | 15 | [Buss- och spårvagnsförare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/PLXw_QzY_dvg) |
| 10 | ...rbetsuppgifter  • Du utbildar **spårvagnsförare** (alla behörigheter) samt övri... |  | x |  | 15 | [Buss- och spårvagnsförare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/TgyQ_cnb_2EY) |
| 10 | ...rbetsuppgifter  • Du utbildar **spårvagnsförare** (alla behörigheter) samt övri... | x | x | 15 | 15 | [spårvagnsförare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bnFa_huz_Cpc) |
| 11 | ...a ditt arbete kommer du få en **instruktörsutbildning** i början av din anställning. ... | x | x | 21 | 21 | [Instruktörsutbildning, **skill**](http://data.jobtechdev.se/taxonomy/concept/B4Yb_Ypo_CbL) |
| 12 | ...ollegor.  KVALIFIKATIONER • **Gymnasieutbildning** eller annan utbildning som ar... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 13 | ...mer som motsvarande • Svenskt **körkort** med lägst behörighet B och av... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 13 | ...mer som motsvarande • Svenskt **körkort** med lägst behörighet B och av... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 14 | ... körkort med lägst behörighet **B** och avslutad prövotid • Godkä... | x |  |  | 1 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 15 | ...h avslutad prövotid • Godkänd **Spårvagnsförarutbildning** med lägst behörighet B • God ... | x | x | 24 | 24 | [Spårvagnsförarutbildning, **skill**](http://data.jobtechdev.se/taxonomy/concept/okxy_qpM_Xjo) |
| 16 | ...d förmåga att uttrycka dig på **svenska** såväl muntligt som skriftligt... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 17 | ...ompetens med goda kunskaper i **Office 365** • Erfarenhet av att hålla utb... | x | x | 10 | 10 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 18 | ... Erfarenhet av att arbeta som **spårvagnsförare**   Vi söker dig som är service... |  | x |  | 15 | [Buss- och spårvagnsförare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/FiDm_2S8_gx7) |
| 18 | ... Erfarenhet av att arbeta som **spårvagnsförare**   Vi söker dig som är service... |  | x |  | 15 | [Spårvagnsförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GkMH_KEV_QKX) |
| 18 | ... Erfarenhet av att arbeta som **spårvagnsförare**   Vi söker dig som är service... |  | x |  | 15 | [Buss- och spårvagnsförare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/PLXw_QzY_dvg) |
| 18 | ... Erfarenhet av att arbeta som **spårvagnsförare**   Vi söker dig som är service... |  | x |  | 15 | [Buss- och spårvagnsförare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/TgyQ_cnb_2EY) |
| 18 | ... Erfarenhet av att arbeta som **spårvagnsförare**   Vi söker dig som är service... | x | x | 15 | 15 | [spårvagnsförare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bnFa_huz_Cpc) |
| | **Overall** | | | **122** | **401** | 122/401 = **30%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Instruktörsutbildning, **skill**](http://data.jobtechdev.se/taxonomy/concept/B4Yb_Ypo_CbL) |
|  | x |  | [Utbildningstjänster, produktkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/ETsW_C5r_RPu) |
|  | x |  | [Buss- och spårvagnsförare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/FiDm_2S8_gx7) |
|  | x |  | [Spårvagnsförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GkMH_KEV_QKX) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [Buss- och spårvagnsförare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/PLXw_QzY_dvg) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
|  | x |  | [Buss- och spårvagnsförare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/TgyQ_cnb_2EY) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Regi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wqo3_zSi_eNj) |
| x | x | x | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| x |  |  | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
| x | x | x | [spårvagnsförare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bnFa_huz_Cpc) |
| x | x | x | [Spårvagnsförarutbildning, **skill**](http://data.jobtechdev.se/taxonomy/concept/okxy_qpM_Xjo) |
| x | x | x | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/18 = **44%** |