# Results for '60b09ded6a74e827b226ed533cc7a79e36a4a82d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [60b09ded6a74e827b226ed533cc7a79e36a4a82d](README.md) | 1 | 3903 | 24 | 16 | 138/351 = **39%** | 11/24 = **46%** |

## Source text

produktionspersonal för tekniskt arbete Arbetsbeskrivning Har du precis tagit gymnasieexamen, är tekniskt lagd och gillar utmaningar? Då kan vi på randstad i Malmö ha ett uppdrag just för dig! Vi letar just nu efter nyexaminerade tekniker eller naturvetare som vill arbeta med både kroppen och knoppen.  Arbetet kräver en god teknisk baskunskap, ett logiskt tänkande och god analysförmåga.   Ute hos kunden ingår man i ett team där det finns andra randstad-konsulter. Detta är en heltidstjänst där du är uthyrd av randstad i 4-6 månader med chans till anställning hos vår kund.   Som konsult hos Randstad är du anställd hos oss och jobbar ute hos någon av våra kunder. Du har samma fördelar hos Randstad som hos andra arbetsgivare med kollektivavtal och förmåner som friskvårdsbidrag, företagshälsovård, försäkringar och rabatt på träningskort. Tjänsten kan innebära många kontakter och då är det är viktigt att du har en bra samarbetsförmåga. På Randstad strävar vi efter att erbjuda dig en mängd karriärmöjligheter, så att du kan utveckla din kompetens och få ett välfyllt CV. Söker du en arbetsgivare som erbjuder varierande uppdrag och nya kontaktnät kommer du att trivas hos oss.   Ansvarsområden Vi är i behov av teknisk kunniga industriarbetare för att stötta upp serietillverkning av kompositverktyg.  Montage av spolar är manuell, och vid justeringen av värmefördelningen arbetar du både med manuell tilläggsmontage och programvara för att inspektera värmebilder. Du dokumenterar även arbetet i Microsoft Office.      Arbetsuppgifterna är av varierande art och kräver god teknisk baskunskap samt hög grad av ansvar noggrannhet, och analysförmåga. Du förväntas kunna lösa komplexa och tidskrävande uppgifter på utsatt tid med kvalité och noggrannhet i fokus.    Vår kund har utvecklat en banbrytande teknologi för snabb värmecykling av formverktyg, tekniken används bl.a.. vid tillverkning av lätta och styva kompositdetaljer vilket är en nyckel för elektrifiering inom industrin. Tekniken som utvecklats under flertalet år står inför ett fasskifte där överlämning från R&D till produktion ska ske, vår kund är därför i behov av en duktig och motiverad produktionspersonal, som är intresserad av att vara med på en spännande utvecklingsresa.   Kvalifikationer Du har en avslutad  gymnasieutbildning med en teknisk eller naturvetenskaplig inriktning. Och/eller  el och energi, produktion, produktutveckling, gymnasieingenjör.  Det är viktigt att du är driven, tekniskt intresserad och trivs med att arbeta mot uppsatta mål och i högt tempo.    Krav:      God teknisk baskunskap       Flytande svenska i tal och skrift      Mycket god engelska i tal och skrift    Meriterande:    Erfarenhet från arbete eller egna projekt med inslag av händighet som exempelvis meka med bilar, sy kläder etc.    Eftergymnasial teknisk eller naturvetenskaps-utbildning    För oss är det viktigt att all kompetens på arbetsmarknaden tillvaratas. Vi välkomnar alla sökande och eftersträvar mångfald.   Ansökan  2022-07-05 urval och intervjuer kommer att ske löpande. Tjänsten kan komma att bli tillsatt innan sista ansökningsdag, ansök därför så snart som möjligt.   För information: Slavica Vasilevska, Konsultchef  Slavica.vasilevska@randstad.se  Om företaget Med över 600 000 anställda i omkring 40 länder är Randstad världsledande inom HR-tjänster och erbjuder bemannings-, konsult- och rekryteringslösningar inom alla kompetensområden. Vi erbjuder även interim management, executive search och omställningstjänster. Vi har ett stort nätverk av bolag och kandidater vilket innebär att vi förmedlar hundratals jobb inom olika branscher, från Kiruna i norr till Malmö i söder. Vår ambition är att vara den bästa arbetsgivaren på marknaden. Genom att kombinera vår passion för människor med kraften i dagens teknologi hjälper vi människor och organisationer att nå deras sanna potential. Vi kallar det Human Forward.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | produktionspersonal för **tekniskt arbete** Arbetsbeskrivning Har du prec... | x |  |  | 15 | [El-tekniskt arbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/c4hC_ASk_6eh) |
| 2 | ...skrivning Har du precis tagit **gymnasieexamen**, är tekniskt lagd och gillar ... | x | x | 14 | 14 | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| 3 | ...ngar? Då kan vi på randstad i **Malmö** ha ett uppdrag just för dig! ... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 4 | ... nyexaminerade tekniker eller **naturvetare** som vill arbeta med både krop... | x |  |  | 11 | [Naturvetenskap, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/kJeN_wmw_9wX) |
| 5 | ...n god teknisk baskunskap, ett **logiskt tänkande** och god analysförmåga.   Ute ... |  | x |  | 16 | [tillämpa logisk programmering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XgJ7_LyX_Xw4) |
| 5 | ...n god teknisk baskunskap, ett **logiskt tänkande** och god analysförmåga.   Ute ... |  | x |  | 16 | [använda logiskt tänkande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rdhB_SJE_x3U) |
| 6 | ...ndstad-konsulter. Detta är en **heltidstjänst** där du är uthyrd av randstad ... | x |  |  | 13 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 7 | ...om hos andra arbetsgivare med **kollektivavtal** och förmåner som friskvårdsbi... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 8 | ...örmåner som friskvårdsbidrag, **företagshälsovård**, försäkringar och rabatt på t... | x | x | 17 | 17 | [Företagshälsovård, **skill**](http://data.jobtechdev.se/taxonomy/concept/ULfW_rVS_Vpr) |
| 9 | ... är viktigt att du har en bra **samarbetsförmåga**. På Randstad strävar vi efter... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 10 | ...u dokumenterar även arbetet i **Microsoft Office**.      Arbetsuppgifterna är av... | x | x | 16 | 16 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 11 | ...nskap samt hög grad av ansvar **noggrannhet**, och analysförmåga. Du förvän... | x | x | 11 | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 12 | ...på utsatt tid med kvalité och **noggrannhet** i fokus.    Vår kund har utve... | x | x | 11 | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 13 | ... har utvecklat en banbrytande **teknologi** för snabb värmecykling av for... | x |  |  | 9 | [Teknologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KQjv_TUZ_Hy9) |
| 14 | ..., tekniken används bl.a.. vid **tillverkning** av lätta och styva kompositde... | x | x | 12 | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 15 | ...ikationer Du har en avslutad  **gymnasieutbildning** med en teknisk eller naturvet... | x |  |  | 18 | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| 15 | ...ikationer Du har en avslutad  **gymnasieutbildning** med en teknisk eller naturvet... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 16 | ...ad  gymnasieutbildning med en **teknisk** eller naturvetenskaplig inrik... | x |  |  | 7 | [Gymnasieskolans teknikprogram, inriktning teknikvetenskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hMYG_YRx_hPo) |
| 17 | ...bildning med en teknisk eller **naturvetenskaplig** inriktning. Och/eller  el och... | x |  |  | 17 | [Gymnasieskolans naturvetenskapliga program, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BaGV_FQz_U5R) |
| 18 | ...kaplig inriktning. Och/eller  **el och energi**, produktion, produktutvecklin... | x |  |  | 13 | [Gymnasieskolans el- och energiprogram, inriktning elteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/pVwG_R4v_WPR) |
| 19 | ...r  el och energi, produktion, **produktutveckling**, gymnasieingenjör.  Det är vi... | x | x | 17 | 17 | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| 20 | ...roduktion, produktutveckling, **gymnasieingenjör**.  Det är viktigt att du är dr... | x |  |  | 16 | [Gymnasieingenjörsutbildning, inriktning produktionsteknik (T4), **keyword**](http://data.jobtechdev.se/taxonomy/concept/4Zza_Ceq_LKH) |
| 21 | ...isk baskunskap       Flytande **svenska** i tal och skrift      Mycket ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 22 | ...al och skrift      Mycket god **engelska** i tal och skrift    Meriteran... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 23 | ...exempelvis meka med bilar, sy **kläder** etc.    Eftergymnasial teknis... | x | x | 6 | 6 | [Kläder/Mode, **skill**](http://data.jobtechdev.se/taxonomy/concept/agGK_Zte_X2y) |
| 24 | ... med bilar, sy kläder etc.    **Eftergymnasial** teknisk eller naturvetenskaps... | x |  |  | 14 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 25 | ...cher, från Kiruna i norr till **Malmö** i söder. Vår ambition är att ... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 26 | ...änniskor med kraften i dagens **teknologi** hjälper vi människor och orga... | x |  |  | 9 | [Teknologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KQjv_TUZ_Hy9) |
| | **Overall** | | | **138** | **351** | 138/351 = **39%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Gymnasieingenjörsutbildning, inriktning produktionsteknik (T4), **keyword**](http://data.jobtechdev.se/taxonomy/concept/4Zza_Ceq_LKH) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| x |  |  | [Gymnasieskolans naturvetenskapliga program, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BaGV_FQz_U5R) |
| x |  |  | [Teknologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KQjv_TUZ_Hy9) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Företagshälsovård, **skill**](http://data.jobtechdev.se/taxonomy/concept/ULfW_rVS_Vpr) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
|  | x |  | [tillämpa logisk programmering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/XgJ7_LyX_Xw4) |
| x | x | x | [Kläder/Mode, **skill**](http://data.jobtechdev.se/taxonomy/concept/agGK_Zte_X2y) |
| x |  |  | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| x |  |  | [El-tekniskt arbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/c4hC_ASk_6eh) |
| x |  |  | [Gymnasieskolans teknikprogram, inriktning teknikvetenskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hMYG_YRx_hPo) |
| x |  |  | [Naturvetenskap, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/kJeN_wmw_9wX) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x |  |  | [Gymnasieskolans el- och energiprogram, inriktning elteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/pVwG_R4v_WPR) |
|  | x |  | [använda logiskt tänkande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rdhB_SJE_x3U) |
| x | x | x | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| x | x | x | [Produktutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/uwev_L2n_fpz) |
| x | x | x | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **11** | 11/24 = **46%** |