# Results for 'ccfe458b40f814bbe4c327f08cddd7fd910be35b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ccfe458b40f814bbe4c327f08cddd7fd910be35b](README.md) | 1 | 2515 | 18 | 35 | 78/500 = **16%** | 6/27 = **22%** |

## Source text

Lundbergs Möbler söker nytänkande ytbehandlare Är du intresserad av att arbeta som ytbehandlare? Nu har du möjlighet att bli en del av det familjära teamet på Lundbergs Möbler i Tibro, en av Skandinaviens ledande tillverkare av bord i trä! Tjänsten innebär en direktanställning på Lundbergs Möbler.   Dina arbetsuppgifter: Som ytbehandlare hos oss kommer du att arbeta med manuell- och datoriserad applicering av färg på möbler. Programmering av sprutautomat, handsprutning, oljning och betsning kommer att ingå i ditt arbete. Du kommer också ha en central roll i utvecklingen av vår ytbehandling mot målet att bli nordens ledande tillverkare av bord. Även andra arbetsmoment inom möbelproduktion kan förekomma. Tjänsten är förlagd på dagtid och innebär en anställning direkt på Lundbergs Möbler.    Kunskaper och erfarenheter som efterfrågas:   Gymnasieutbildning   Tidigare erfarenhet av ytbehandling  Erfarenhet inom träindustri  Krav och meriter Bra förståelse för svenska i tal- och skrift   Grundläggande PC-kunskaper   Om dig:  Vi söker dig som är driven, nyfiken och gärna tänker stort och nytt. Vi ser gärna att du är tekniskt intresserad, kan lägga upp ditt arbete på ett strukturerat sätt, är flexibel och har en känsla för detaljer och kvalité. Det är ett självständigt arbete men du kommer också arbeta nära dina kollegor, därför är det viktigt att du har lätt för att samarbeta i team likväl som att arbeta självständigt.  Ansökan I den här rekryteringen samarbetar vi med OnePartnerGroup. Vid frågor eller mer information vänligen kontakta ansvarig rekryterare Julia Hillerström på julia.hillerstrom@onepartnergroup.se eller Evelyn Wiseby på evelyn.wiseby@onepartnergroup.se. Urval sker löpande med start vecka 32. Då vi inte tar emot ansökan via mail hänvisar vi dig till att söka via annonsen.   Om OnePartnerGroup OnePartnerGroup är en svenskägd koncern med ett unikt helhetskoncept för företag. Vi är en trygg partner när du behöver hjälp med rekrytering, bemanning, utbildning och smarta produktionslösningar. Hos oss har du tillgång till den bästa lokala kompetensen inom både yrkesarbetar- och tjänstemannasektorn. Du får också flexibla lösningar som möjliggör långsiktig tillväxt och lönsamhet. Vi finns nära dig. OnePartnerGroup är verksam på 50 orter, med över 2500 engagerade medarbetare. Genom vår kännedom om, och närvaro på, så många platser är vi Sveriges lokalaste samarbetspartner. Vi är ett auktoriserat rekryterings- och bemanningsföretag.  Läs mer om oss på www.onepartnergroup.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Lundbergs **Möbler** söker nytänkande ytbehandlare... |  | x |  | 6 | [Möbler, **skill**](http://data.jobtechdev.se/taxonomy/concept/J91r_GLJ_qoa) |
| 2 | ...bergs Möbler söker nytänkande **ytbehandlare** Är du intresserad av att arbe... |  | x |  | 12 | [Ytbehandlare, trä, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Famt_Urq_gTi) |
| 2 | ...bergs Möbler söker nytänkande **ytbehandlare** Är du intresserad av att arbe... | x | x | 12 | 12 | [Ytbehandlare manuell, trä/Sprutlackerare, manuell, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KTVC_1PC_cJG) |
| 2 | ...bergs Möbler söker nytänkande **ytbehandlare** Är du intresserad av att arbe... |  | x |  | 12 | [Ytbehandlare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/UAqf_aw1_f4q) |
| 2 | ...bergs Möbler söker nytänkande **ytbehandlare** Är du intresserad av att arbe... |  | x |  | 12 | [ytbehandlare, manuell, trä, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/UQPc_hFt_AS2) |
| 2 | ...bergs Möbler söker nytänkande **ytbehandlare** Är du intresserad av att arbe... |  | x |  | 12 | [Manuella ytbehandlare, trä, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/v7W3_cLQ_q2S) |
| 2 | ...bergs Möbler söker nytänkande **ytbehandlare** Är du intresserad av att arbe... |  | x |  | 12 | [Ytbehandlare, trä och möbelsnickare m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/zLCo_bDm_gkM) |
| 3 | ...intresserad av att arbeta som **ytbehandlare**? Nu har du möjlighet att bli ... |  | x |  | 12 | [Ytbehandlare, trä, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Famt_Urq_gTi) |
| 3 | ...intresserad av att arbeta som **ytbehandlare**? Nu har du möjlighet att bli ... | x | x | 12 | 12 | [Ytbehandlare manuell, trä/Sprutlackerare, manuell, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KTVC_1PC_cJG) |
| 3 | ...intresserad av att arbeta som **ytbehandlare**? Nu har du möjlighet att bli ... |  | x |  | 12 | [Ytbehandlare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/UAqf_aw1_f4q) |
| 3 | ...intresserad av att arbeta som **ytbehandlare**? Nu har du möjlighet att bli ... |  | x |  | 12 | [ytbehandlare, manuell, trä, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/UQPc_hFt_AS2) |
| 3 | ...intresserad av att arbeta som **ytbehandlare**? Nu har du möjlighet att bli ... |  | x |  | 12 | [Manuella ytbehandlare, trä, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/v7W3_cLQ_q2S) |
| 3 | ...intresserad av att arbeta som **ytbehandlare**? Nu har du möjlighet att bli ... |  | x |  | 12 | [Ytbehandlare, trä och möbelsnickare m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/zLCo_bDm_gkM) |
| 4 | ...familjära teamet på Lundbergs **Möbler** i Tibro, en av Skandinaviens ... |  | x |  | 6 | [Möbler, **skill**](http://data.jobtechdev.se/taxonomy/concept/J91r_GLJ_qoa) |
| 5 | ... teamet på Lundbergs Möbler i **Tibro**, en av Skandinaviens ledande ... | x | x | 5 | 5 | [Tibro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aLFZ_NHw_atB) |
| 6 | ...irektanställning på Lundbergs **Möbler**.   Dina arbetsuppgifter: Som ... |  | x |  | 6 | [Möbler, **skill**](http://data.jobtechdev.se/taxonomy/concept/J91r_GLJ_qoa) |
| 7 | ....   Dina arbetsuppgifter: Som **ytbehandlare** hos oss kommer du att arbeta ... |  | x |  | 12 | [Ytbehandlare, trä, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Famt_Urq_gTi) |
| 7 | ....   Dina arbetsuppgifter: Som **ytbehandlare** hos oss kommer du att arbeta ... | x | x | 12 | 12 | [Ytbehandlare manuell, trä/Sprutlackerare, manuell, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KTVC_1PC_cJG) |
| 7 | ....   Dina arbetsuppgifter: Som **ytbehandlare** hos oss kommer du att arbeta ... |  | x |  | 12 | [Ytbehandlare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/UAqf_aw1_f4q) |
| 7 | ....   Dina arbetsuppgifter: Som **ytbehandlare** hos oss kommer du att arbeta ... |  | x |  | 12 | [ytbehandlare, manuell, trä, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/UQPc_hFt_AS2) |
| 7 | ....   Dina arbetsuppgifter: Som **ytbehandlare** hos oss kommer du att arbeta ... |  | x |  | 12 | [Manuella ytbehandlare, trä, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/v7W3_cLQ_q2S) |
| 7 | ....   Dina arbetsuppgifter: Som **ytbehandlare** hos oss kommer du att arbeta ... |  | x |  | 12 | [Ytbehandlare, trä och möbelsnickare m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/zLCo_bDm_gkM) |
| 8 | ... med manuell- och datoriserad **applicering** av färg på möbler. Programmer... | x |  |  | 11 | [applicera färglager, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kvBK_97E_JGz) |
| 9 | ...ch datoriserad applicering av **färg** på möbler. Programmering av s... | x | x | 4 | 4 | [Färg/Kemikalieprodukter/Tapeter, **skill**](http://data.jobtechdev.se/taxonomy/concept/fyJi_1k3_CdV) |
| 10 | ...iserad applicering av färg på **möbler**. Programmering av sprutautoma... | x | x | 6 | 6 | [Möbler, **skill**](http://data.jobtechdev.se/taxonomy/concept/J91r_GLJ_qoa) |
| 11 | ...pplicering av färg på möbler. **Programmering** av sprutautomat, handsprutnin... |  | x |  | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 12 | ...g på möbler. Programmering av **sprutautomat**, handsprutning, oljning och b... | x |  |  | 12 | [Sprutmålning, **skill**](http://data.jobtechdev.se/taxonomy/concept/qdJB_5Qn_i5h) |
| 13 | ...rogrammering av sprutautomat, **handsprutning**, oljning och betsning kommer ... | x |  |  | 13 | [Sprutmålning, **skill**](http://data.jobtechdev.se/taxonomy/concept/qdJB_5Qn_i5h) |
| 14 | ...t, handsprutning, oljning och **betsning** kommer att ingå i ditt arbete... | x |  |  | 8 | [betsa trä, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bssv_GPM_kBj) |
| 15 | ...ställning direkt på Lundbergs **Möbler**.    Kunskaper och erfarenhete... |  | x |  | 6 | [Möbler, **skill**](http://data.jobtechdev.se/taxonomy/concept/J91r_GLJ_qoa) |
| 16 | ...farenheter som efterfrågas:   **Gymnasieutbildning**   Tidigare erfarenhet av ytbe... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 17 | ...farenheter som efterfrågas:   **Gymnasieutbildning **  Tidigare erfarenhet av ytbeh... | x |  |  | 19 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 18 | ...ning   Tidigare erfarenhet av **ytbehandling**  Erfarenhet inom träindustri ... | x |  |  | 12 | [Ytbehandling, **skill**](http://data.jobtechdev.se/taxonomy/concept/ohUU_nf8_B1U) |
| 19 | ...ytbehandling  Erfarenhet inom **träindustri**  Krav och meriter Bra förståe... |  | x |  | 11 | [Maskinskötsel, trä- och pappersindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/BGie_HPa_fNA) |
| 19 | ...ytbehandling  Erfarenhet inom **träindustri**  Krav och meriter Bra förståe... |  | x |  | 11 | [arbetsledare, träindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/CdBW_CWg_FKY) |
| 19 | ...ytbehandling  Erfarenhet inom **träindustri**  Krav och meriter Bra förståe... |  | x |  | 11 | [Processoperatörer, trä- och pappersindustri, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MjXx_5mw_RNf) |
| 19 | ...ytbehandling  Erfarenhet inom **träindustri**  Krav och meriter Bra förståe... |  | x |  | 11 | [Processoperatörer, trä- och pappersindustri, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Mu2E_RpA_86z) |
| 19 | ...ytbehandling  Erfarenhet inom **träindustri**  Krav och meriter Bra förståe... |  | x |  | 11 | [Maskinsnickare och maskinoperatörer, träindustri, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/VKKg_e4t_t46) |
| 19 | ...ytbehandling  Erfarenhet inom **träindustri**  Krav och meriter Bra förståe... | x |  |  | 11 | [Trävaruarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/ndTs_hs5_z5f) |
| 19 | ...ytbehandling  Erfarenhet inom **träindustri**  Krav och meriter Bra förståe... |  | x |  | 11 | [chef, träindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/wC4Z_Uqd_mbh) |
| 20 | ...ch meriter Bra förståelse för **svenska** i tal- och skrift   Grundlägg... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 21 | ...r svenska i tal- och skrift   **Grundläggande PC-kunskaper**   Om dig:  Vi söker dig som ä... | x |  |  | 26 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 22 | ...ktigt att du har lätt för att **samarbeta** i team likväl som att arbeta ... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 23 | ...marbeta i team likväl som att **arbeta självständigt**.  Ansökan I den här rekryteri... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| | **Overall** | | | **78** | **500** | 78/500 = **16%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Maskinskötsel, trä- och pappersindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/BGie_HPa_fNA) |
|  | x |  | [arbetsledare, träindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/CdBW_CWg_FKY) |
|  | x |  | [Ytbehandlare, trä, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/Famt_Urq_gTi) |
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x | x | x | [Möbler, **skill**](http://data.jobtechdev.se/taxonomy/concept/J91r_GLJ_qoa) |
| x | x | x | [Ytbehandlare manuell, trä/Sprutlackerare, manuell, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KTVC_1PC_cJG) |
|  | x |  | [Processoperatörer, trä- och pappersindustri, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MjXx_5mw_RNf) |
|  | x |  | [Processoperatörer, trä- och pappersindustri, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Mu2E_RpA_86z) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Ytbehandlare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/UAqf_aw1_f4q) |
|  | x |  | [ytbehandlare, manuell, trä, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/UQPc_hFt_AS2) |
|  | x |  | [Maskinsnickare och maskinoperatörer, träindustri, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/VKKg_e4t_t46) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Tibro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/aLFZ_NHw_atB) |
| x |  |  | [betsa trä, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bssv_GPM_kBj) |
| x | x | x | [Färg/Kemikalieprodukter/Tapeter, **skill**](http://data.jobtechdev.se/taxonomy/concept/fyJi_1k3_CdV) |
|  | x |  | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x |  |  | [applicera färglager, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kvBK_97E_JGz) |
| x |  |  | [Trävaruarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/ndTs_hs5_z5f) |
| x |  |  | [Ytbehandling, **skill**](http://data.jobtechdev.se/taxonomy/concept/ohUU_nf8_B1U) |
| x |  |  | [Sprutmålning, **skill**](http://data.jobtechdev.se/taxonomy/concept/qdJB_5Qn_i5h) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
|  | x |  | [Manuella ytbehandlare, trä, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/v7W3_cLQ_q2S) |
|  | x |  | [chef, träindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/wC4Z_Uqd_mbh) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
|  | x |  | [Ytbehandlare, trä och möbelsnickare m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/zLCo_bDm_gkM) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/27 = **22%** |