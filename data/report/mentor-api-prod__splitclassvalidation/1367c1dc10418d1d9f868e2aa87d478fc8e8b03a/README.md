# Results for '1367c1dc10418d1d9f868e2aa87d478fc8e8b03a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1367c1dc10418d1d9f868e2aa87d478fc8e8b03a](README.md) | 1 | 2770 | 15 | 16 | 71/175 = **41%** | 7/16 = **44%** |

## Source text

Kock till Centrumrestaurangen i Luleå Kock till Centrumrestaurangen på LTU i Luleå  Tillsvidare, heltid 100%   Varför ska du välja oss?  • Vi rekryterar schysst! Vi tror att mångfald, olika erfarenheter och kompetenser gör den bästa stjärnhimlen  • Vi älskar service och vi vill leverera service i världsklass genom att alltid ge det lilla extra  • Vi tror på hållbarhet! Både när det gäller vår planet och våra kollegor  • Vi levererar kvalitet i allt vi gör!   Vem söker vi?  Nu söker vi en passionerad kock som vill arbeta i ett team där serviceanda och lagarbete står i centrum. Med nyfikenhet och entusiasm skapar du smakfulla och vällagade rätter med fokus på hållbarhet.  I tjänsten ingår bland annat dagligt arbete med egenkontroll, att ha god kännedom om allergener och kunna redogöra för alla ingredienser, beställning och mottagning av varor samt svinnhantering. Du kommer även vara behjälplig i menyplanering och inventering.  Vi är en del av Compass Group som är en världsledande leverantör inom mat, dryck och supportservice. Över hela världen har vi över 600 000 kollegor! I Sverige har vi cirka 450 verksamheter runt om i landet, vilket gör att det finns fantastiska utvecklingsmöjligheter hos oss. Vi har förmåner genom kollektivavtal som årlig löneökning, försäkringar och tjänstepension.  Compass Group i Sverige omsätter ca 3 miljarder kronor och drivs under varumärken som Food & Co, Marketplace, Medirest, Amica, Chartwells, Levy Restaurants, UNISON, Eurest och fler. Compass Group Sverige är en del i koncernen Compass Group PLC och förvärvade Fazer Food Services den 31 januari 2020. Koncernens omsättning uppgår globalt till £25 miljarder (2019). Våra kunder är företag, arenor, universitet, skolor, vård och omsorg med flera. Vi tillhandahåller service såsom mat och dryck, konferens, reception, lokalvård, vaktmästeri med mera.  www.compass-group.se  Vem är du?  • Du är över 18 år  • Du har utbildning inom hotell och restaurang med kockinriktning eller motsvarande  • Du har erfarenhet av restaurangarbete sedan tidigare  • Du har en förmåga att planera och strukturera arbetet effektivt  • Du tar genom din passion ansvar för att hålla en hög service- och kvalitetsnivå på det du levererar  • Du ser din betydelse i nöjda gäster, är positiv, stresstålig och har lätt för att samarbeta och kommunicera  • Du kan ta egna initiativ och är flexibel i din yrkesroll  • Du talar och skriver svenska  • Erfarenhet av menyplaneringssystem är ett plus   Rekrytering sker löpande så varmt välkommen med din ansökan!   Vi tar enbart emot ansökningar via webben, ansökningar som kommer in på annat sätt behandlas inte.  Compass Group AB behandlar dina personuppgifter, för mer information vänligen besök: https://www.compass-group.se/integritetspolicy/

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kock** till Centrumrestaurangen i Lu... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 2 | ...ck till Centrumrestaurangen i **Luleå** Kock till Centrumrestaurangen... | x | x | 5 | 5 | [Luleå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CXbY_gui_14v) |
| 3 | ...l Centrumrestaurangen i Luleå **Kock** till Centrumrestaurangen på L... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 4 | ... Centrumrestaurangen på LTU i **Luleå**  Tillsvidare, heltid 100%   V... | x | x | 5 | 5 | [Luleå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CXbY_gui_14v) |
| 5 | ...mrestaurangen på LTU i Luleå  **Tillsvidare**, heltid 100%   Varför ska du ... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 6 | ... på LTU i Luleå  Tillsvidare, **heltid 100%**   Varför ska du välja oss?  •... | x |  |  | 11 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 7 | ...?  Nu söker vi en passionerad **kock** som vill arbeta i ett team dä... | x | x | 4 | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 8 | ...lagarbete står i centrum. Med **nyfikenhet** och entusiasm skapar du smakf... | x |  |  | 10 | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
| 9 | ...ingredienser, beställning och **mottagning** av varor samt svinnhantering.... | x | x | 10 | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 10 | ...r vi över 600 000 kollegor! I **Sverige** har vi cirka 450 verksamheter... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 11 | ...os oss. Vi har förmåner genom **kollektivavtal** som årlig löneökning, försäkr... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 12 | ...nstepension.  Compass Group i **Sverige** omsätter ca 3 miljarder krono... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 13 | ...urest och fler. Compass Group **Sverige** är en del i koncernen Compass... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 14 | ...del i koncernen Compass Group **PLC** och förvärvade Fazer Food Ser... | x |  |  | 3 | [Programmerbart styrsystem/PLC, **skill**](http://data.jobtechdev.se/taxonomy/concept/Uytu_hWi_5nh) |
| 15 | ...som mat och dryck, konferens, **reception**, lokalvård, vaktmästeri med m... | x | x | 9 | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 16 | ... dryck, konferens, reception, **lokalvård**, vaktmästeri med mera.  www.c... | x | x | 9 | 9 | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| 17 | ... år  • Du har utbildning inom **hotell** och restaurang med kockinrikt... |  | x |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 18 | ... år  • Du har utbildning inom **hotell och restaurang** med kockinriktning eller mots... | x |  |  | 21 | [Hotell och restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/F5kB_F4h_yc5) |
| 19 | ...ar utbildning inom hotell och **restaurang** med kockinriktning eller mots... |  | x |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 20 | ...e i nöjda gäster, är positiv, **stresstålig** och har lätt för att samarbet... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 21 | ...sroll  • Du talar och skriver **svenska**  • Erfarenhet av menyplanerin... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **71** | **175** | 71/175 = **41%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Luleå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CXbY_gui_14v) |
| x |  |  | [Hotell och restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/F5kB_F4h_yc5) |
|  | x |  | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x | x | x | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x | x | x | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| x |  |  | [Programmerbart styrsystem/PLC, **skill**](http://data.jobtechdev.se/taxonomy/concept/Uytu_hWi_5nh) |
| x | x | x | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
|  | x |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/16 = **44%** |