# Results for 'b35871cfc42cf53a9e67db142cd79a7e341f0de6'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b35871cfc42cf53a9e67db142cd79a7e341f0de6](README.md) | 1 | 525 | 7 | 7 | 61/75 = **81%** | 5/7 = **71%** |

## Source text

Organist Vi söker dig som har organistutbildning och är väl förtrogen med Svenska kyrkans tro och liv. I tjänsten som organist kommer du att spela på gudstjänster, andakter och kyrkliga handlingar runt om i vår församling – både i Kalix kyrka och i byar (bönhus och kapell). Vi ser gärna att du vill jobba med en vuxenkör som kan medverka på gudstjänster och konserter. Vi lägger stor vikt vid personlig lämplighet och samarbetsförmåga. Vi arbetar aktivt för jämställdhet och vi välkomnar både kvinnliga och manliga sökanden.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Organist** Vi söker dig som har organist... | x | x | 8 | 8 | [Organist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DNDp_1sY_ZrR) |
| 2 | Organist Vi söker dig som har **organistutbildning** och är väl förtrogen med Sven... | x | x | 18 | 18 | [Organistutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/eESF_8Qa_pBs) |
| 3 | ...ning och är väl förtrogen med **Svenska** kyrkans tro och liv. I tjänst... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 4 | ... är väl förtrogen med Svenska **kyrkans** tro och liv. I tjänsten som o... | x |  |  | 7 | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
| 5 | ...s tro och liv. I tjänsten som **organist** kommer du att spela på gudstj... | x | x | 8 | 8 | [Organist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DNDp_1sY_ZrR) |
| 6 | ...liga handlingar runt om i vår **församling** – både i Kalix kyrka och i by... | x | x | 10 | 10 | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
| 7 | ... om i vår församling – både i **Kalix** kyrka och i byar (bönhus och ... | x | x | 5 | 5 | [Kalix, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cUyN_C9V_HLU) |
| 8 | ...örmåga. Vi arbetar aktivt för **jämställdhet** och vi välkomnar både kvinnli... | x | x | 12 | 12 | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| | **Overall** | | | **61** | **75** | 61/75 = **81%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Organist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/DNDp_1sY_ZrR) |
| x | x | x | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| x | x | x | [Kalix, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cUyN_C9V_HLU) |
| x | x | x | [Organistutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/eESF_8Qa_pBs) |
| x |  |  | [Kyrkan, **keyword**](http://data.jobtechdev.se/taxonomy/concept/h4LF_GE3_FGu) |
| x | x | x | [Församling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rfGv_wrD_n9x) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/7 = **71%** |