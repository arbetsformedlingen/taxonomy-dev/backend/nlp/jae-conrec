# Results for 'f60b4e489c9f27f1ce601202d614b80a6c0d35e2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f60b4e489c9f27f1ce601202d614b80a6c0d35e2](README.md) | 1 | 2595 | 11 | 12 | 40/166 = **24%** | 4/15 = **27%** |

## Source text

Heltidstjänst som Personlig assistent i centrala Västerås För dig som vill jobba heltid och dygn. Detta är jobbet för dig som är redo att släppa kontrollen och utmana ditt sinne på flera sätt. Du som är lugn, tålmodig och kan ta instruktioner eftersom arbetet utförs efter muntliga instruktioner. Det är bra om du har känsla för integritet och inte har något emot att större delen av tiden befinna sig i bakgrunden. Schemat bygger till stor del på dygnspass, så du jobbar normalt ett dygn och är därför mycket ledig och har tid till studier eller dyl. Idag är arbetstiderna 24h pass. Detta innebär att du i nuläget skulle arbeta ca 2 pass i veckan med en veckas ledighet i månaden. Ett plus är att kunna följa med på resor, även om det inte är aktuellt idag pga Corona.  Om mig  Jag söker personlig assistent för mig, en kvinna i 40års åldern med muskelsjukdom, boende i centrala Västerås. Jag värdesätter respekt för min integritet, lojalitet och ansvarskänsla. Jag är en flexibel person som gillar fart och fläkt men även hemmakvällar i soffan. Det är av största vikt att allting blir utfört på mitt sätt för att jag ska känna att jag lever mitt egna liv. Jag är tydlig med vad jag vill så jag behöver någon som är trygg i dig själv, kan anpassa sig till olika situationer och ha en stor känsla för integritet. Bekväm med att hålla sig i bakgrunden och inte vara social.  Om dig  Jag söker någon som kan vara mina armar och ben. Någon som är bekväm med vardagens alla sysslor men även vågar utmana sig, varesig det handlar om att måla om eller byta lampa i bilen. Du behöver trivas med variationen att vissa dagar går iett medan andra är betydligt lugnare. Att du förstår att du som assistent gör det möjligt för mig att äta chips till middag om jag vill men även delta i manifestationer, utföra mitt arbete eller gå på kalas. Det är också du som gör det möjligt för mig att leva mitt liv på mitt sätt genom att ta ett steg tillbaka och invänta min blick, nick eller önskan.  Kraven på dig som personlig assistent är att du:  - har ett flexibelt sinnelag  - inte har problem med rygg, axlar eller knän. God fysik krävs då lyft förekommer  - inte är allergisk (mot t.ex. djur, nötter, parfym och rök) då du stöter på detta under arbetet  - kan tala svenska eller engelska  - är respektfull att det är mitt hem du arbetar i  - har B-körkort och körvana  - är mellan 25 och 55 år  - är vaccinerad mot Corona/Covid  - är rökfri  Tillträde till tjänsten sker snarast/efter överenskommelse.  Jag lägger stor vikt vid personligheten och ingen tidigare erfarenhet krävs, du får nödvändig intro av mig.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Heltidstjänst som **Personlig assistent** i centrala Västerås För dig s... | x |  |  | 19 | [Personlig assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eU1q_zvL_9Rf) |
| 2 | ...ersonlig assistent i centrala **Västerås** För dig som vill jobba heltid... | x | x | 8 | 8 | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| 3 | ...sterås För dig som vill jobba **heltid** och dygn. Detta är jobbet för... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 4 | ...ga Corona.  Om mig  Jag söker **personlig assistent** för mig, en kvinna i 40års ål... | x |  |  | 19 | [Personlig assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eU1q_zvL_9Rf) |
| 5 | ...kelsjukdom, boende i centrala **Västerås**. Jag värdesätter respekt för ... | x | x | 8 | 8 | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| 6 | ...min integritet, lojalitet och **ansvarskänsla**. Jag är en flexibel person so... |  | x |  | 13 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 7 | ...er önskan.  Kraven på dig som **personlig assistent** är att du:  - har ett flexibe... | x |  |  | 19 | [Personlig assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eU1q_zvL_9Rf) |
| 8 | ...m med rygg, axlar eller knän. **God fysik** krävs då lyft förekommer  - i... | x |  |  | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 9 | ... inte är allergisk (mot t.ex. **djur**, nötter, parfym och rök) då d... |  | x |  | 4 | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| 10 | ...gisk (mot t.ex. djur, nötter, **parfym** och rök) då du stöter på dett... |  | x |  | 6 | [Partihandel med parfym och kosmetika, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/32Yc_kp9_UJT) |
| 10 | ...gisk (mot t.ex. djur, nötter, **parfym** och rök) då du stöter på dett... |  | x |  | 6 | [Partihandel med parfym och kosmetika, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/LikT_frr_oYm) |
| 10 | ...gisk (mot t.ex. djur, nötter, **parfym** och rök) då du stöter på dett... |  | x |  | 6 | [Parfym/Kosmetika, **skill**](http://data.jobtechdev.se/taxonomy/concept/bPCR_BeT_Jo2) |
| 10 | ...gisk (mot t.ex. djur, nötter, **parfym** och rök) då du stöter på dett... |  | x |  | 6 | [butikschef, kosmetika och parfym, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/gK7M_i5j_1KR) |
| 10 | ...gisk (mot t.ex. djur, nötter, **parfym** och rök) då du stöter på dett... |  | x |  | 6 | [Tillverkning av rengöringsmedel, parfym och toalettartiklar, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/yxnR_S1p_XvV) |
| 11 | ...tta under arbetet  - kan tala **svenska** eller engelska  - är respektf... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 12 | ...tet  - kan tala svenska eller **engelska**  - är respektfull att det är ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 13 | ... mitt hem du arbetar i  - har **B-körkort** och körvana  - är mellan 25 o... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 14 | ...rbetar i  - har B-körkort och **körvana**  - är mellan 25 och 55 år  - ... | x |  |  | 7 | [köra fordon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/8eY1_8ME_8rC) |
| | **Overall** | | | **40** | **166** | 40/166 = **24%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Partihandel med parfym och kosmetika, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/32Yc_kp9_UJT) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Västerås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8deT_FRF_2SP) |
| x |  |  | [köra fordon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/8eY1_8ME_8rC) |
|  | x |  | [Partihandel med parfym och kosmetika, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/LikT_frr_oYm) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
|  | x |  | [Parfym/Kosmetika, **skill**](http://data.jobtechdev.se/taxonomy/concept/bPCR_BeT_Jo2) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Personlig assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eU1q_zvL_9Rf) |
|  | x |  | [butikschef, kosmetika och parfym, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/gK7M_i5j_1KR) |
| x |  |  | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
|  | x |  | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
|  | x |  | [Tillverkning av rengöringsmedel, parfym och toalettartiklar, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/yxnR_S1p_XvV) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/15 = **27%** |