# Results for 'e1d12e2766860982a56b8a9283acc04432bd051a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e1d12e2766860982a56b8a9283acc04432bd051a](README.md) | 1 | 3044 | 10 | 20 | 51/243 = **21%** | 4/11 = **36%** |

## Source text

Timvikarier sökes till Städ och Vårdnära service, Kungsbacka Om dig Du är en positiv person som är engagerad och bidrar med en god kamratanda för att skapa trivsel på arbetsplatsen.  Du tar ansvar för ditt arbete och förstår att du är en del av en viktig helhet. I ditt arbete kan du komma att ha kontakt med patienter, anhöriga och vårdpersonal, därför är det viktigt att du är social, utåtriktad och har lätt för att kommunicera. Vi ser också att du har förmåga att vara flexibel och kan prioritera mellan arbetsuppgifter då det behövs.  Av säkerhetsskäl måste du kunna förstå skriftliga instruktioner och kunna kommunicera med kollegor och övriga samarbetspartners, därför krävs mycket goda kunskaper i svenska, i såväl tal som skrift. Stor vikt läggs vid personlig lämplighet.  Du som söker måste ha fyllt 18 år och vi ser gärna att du har erfarenhet av servicearbete som exempelvis städ.  Om arbetet Arbetsuppgifterna innefattar städning av mottagningar, administrativa lokaler och allmänna ytor på Kungsbacka sjukhus samt vårdcentraler inom Kungsbacka kommun. Vårdnära service innebär att ett team ansvarar för den vårdnära servicen på avdelningen. Serviceteamet samarbetar nära vårdpersonalen för att göra patientflödet så smidigt som möjligt och bidra till hög kvalitet på avdelningen. Arbetsuppgifterna för teamet kan bland annat innefatta ansvar för patientkök, städning och förrådshantering och innebär mycket samarbete, både inom teamet och med vårdpersonalen. Arbetet innefattar också kontakt med patienter och anhöriga, där serviceteamet stärker patientens upplevelse av kvalitet. Arbetet är främst schemalagt med tjänstgöring dagtid men kan variera mellan kl. 06.00-23.30. Helgtjänstgöring kan ingå.   Varmt välkommen att skicka in din ansökan!  Urval och intervjuer sker löpande under ansökningstiden, så skicka gärna in din ansökan så fort som möjligt.  Inför eventuell anställning måste utdrag ur belastningsregistret kunna uppvisas.  Om Region Halland   Region Halland är till för alla som bor och arbetar här i Halland. Vi är över 8 000 medarbetare inom 370 olika yrken som alla arbetar för att erbjuda en god hälso- och sjukvård och att främja tillväxt och utveckling. Vi ansvarar också för kollektivtrafiken i Halland och driver frågor inom områden som näringsliv, kultur och utbildning. Vår vision är ”Halland ­ – bästa livsplatsen” och med det menar vi att Halland ska bli den bästa platsen att bo på, utbilda sig, arbeta och driva företag i. Vi har kommit en bra bit på vägen, välkommen att följa med oss på vår resa! https://www.regionhalland.se/om-region-halland/  Strävan efter jämställdhet, mångfald och likvärdiga villkor är både en kvalitetsfråga och en självklar del av Region Hallands värdegrund.   Region Halland krigsplacerar all tillsvidareanställd personal.   Till bemannings- och rekryteringsföretag och till dig som är försäljare: Vi undanber oss vänligt men bestämt direktkontakt med bemannings- och rekryteringsföretag samt försäljare av ytterligare jobbannonser. Region Halland har upphandlade avtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ll Städ och Vårdnära service, **Kungsbacka** Om dig Du är en positiv perso... | x | x | 10 | 10 | [Kungsbacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3JKV_KSK_x6z) |
| 2 | ...ivsel på arbetsplatsen.  Du **tar ansvar** för ditt arbete och förstår a... | x |  |  | 10 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 3 | ...t med patienter, anhöriga och **vårdpersonal**, därför är det viktigt att du... |  | x |  | 12 | [Övrig vård- och omsorgspersonal, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/GiNX_ESA_AL1) |
| 4 | ...är social, utåtriktad och har **lätt för att kommunicera**. Vi ser också att du har förm... | x |  |  | 24 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 5 | ...åga att vara flexibel och kan **prioritera mellan arbetsuppgifter** då det behövs.  Av säkerhetss... | x |  |  | 33 | [prioritera uppgifter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/j5rg_b5X_nBS) |
| 6 | ...krävs mycket goda kunskaper i **svenska**, i såväl tal som skrift. Stor... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 7 | ... servicearbete som exempelvis **städ**.  Om arbetet Arbetsuppgiftern... | x |  |  | 4 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 8 | ... Arbetsuppgifterna innefattar **städning** av mottagningar, administrati... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 9 | ... lokaler och allmänna ytor på **Kungsbacka** sjukhus samt vårdcentraler in... | x | x | 10 | 10 | [Kungsbacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3JKV_KSK_x6z) |
| 10 | ...ukhus samt vårdcentraler inom **Kungsbacka** kommun. Vårdnära service inne... | x |  |  | 10 | [Kungsbacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3JKV_KSK_x6z) |
| 11 | ...Serviceteamet samarbetar nära **vårdpersonalen** för att göra patientflödet så... |  | x |  | 14 | [Övrig vård- och omsorgspersonal, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/GiNX_ESA_AL1) |
| 12 | ...efatta ansvar för patientkök, **städning** och förrådshantering och inne... | x | x | 8 | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 13 | ... för patientkök, städning och **förrådshantering** och innebär mycket samarbete,... | x | x | 16 | 16 | [Förrådshantering, **skill**](http://data.jobtechdev.se/taxonomy/concept/vvPg_scR_Pp6) |
| 14 | ...ete, både inom teamet och med **vårdpersonalen**. Arbetet innefattar också kon... |  | x |  | 14 | [Övrig vård- och omsorgspersonal, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/GiNX_ESA_AL1) |
| 15 | ...et kunna uppvisas.  Om Region **Halland**   Region Halland är till för ... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 16 | ....  Om Region Halland   Region **Halland** är till för alla som bor och ... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 17 | ...rbetar för att erbjuda en god **hälso- oc**h sjukvård och att främja till... |  | x |  | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 18 | ...också för kollektivtrafiken i **Halland** och driver frågor inom område... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 19 | ...ch utbildning. Vår vision är ”**Halland** ­ – bästa livsplatsen” och me... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 20 | ...egion-halland/  Strävan efter **jämställdhet**, mångfald och likvärdiga vill... |  | x |  | 12 | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| 21 | ...Hallands värdegrund.   Region **Halland** krigsplacerar all tillsvidare... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 22 | ...erligare jobbannonser. Region **Halland** har upphandlade avtal. |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| | **Overall** | | | **51** | **243** | 51/243 = **21%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x | x | x | [Kungsbacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3JKV_KSK_x6z) |
|  | x |  | [Övrig vård- och omsorgspersonal, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/GiNX_ESA_AL1) |
|  | x |  | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
|  | x |  | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x |  |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [prioritera uppgifter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/j5rg_b5X_nBS) |
| x | x | x | [Förrådshantering, **skill**](http://data.jobtechdev.se/taxonomy/concept/vvPg_scR_Pp6) |
|  | x |  | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/11 = **36%** |