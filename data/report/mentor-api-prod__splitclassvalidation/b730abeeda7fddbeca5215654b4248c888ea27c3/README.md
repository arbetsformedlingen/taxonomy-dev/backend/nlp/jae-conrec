# Results for 'b730abeeda7fddbeca5215654b4248c888ea27c3'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b730abeeda7fddbeca5215654b4248c888ea27c3](README.md) | 1 | 770 | 6 | 7 | 58/90 = **64%** | 5/6 = **83%** |

## Source text

Lokalvårdare Det finns ingen arbetsplatsen, det variera det beror på jobbet som man måste göra. Men, det mesta är i Stockholm. Det är en små familjeföretag med fokus på underhåll, lokalvårdare, ta bort snö från tak och garage, även små reparationer, etc. Förut jobbade jag som trädgårdsmästare och med städning så har jag mycket kunskap och erfarhet på det, så har jag kontrakt med människor som jag jobbade med förut. Just nu, söker jag någon som kan hjälpa mig med jobbet. Jag har en enskilsdfirma och söker någon som kan hjälpa mig med mina jobb, det är inte bara städning, det finns byggstädning, trappstädning, allt i städningen rubrik, dessutom vi jobbar i byggbranch och det beror på säsongen. Om du är intresserad kan du skicka din meritförteckning genom e-post.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Lokalvårdare** Det finns ingen arbetsplatsen... | x | x | 12 | 12 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 2 | ...ste göra. Men, det mesta är i **Stockholm**. Det är en små familjeföretag... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 3 | ...retag med fokus på underhåll, **lokalvårdare**, ta bort snö från tak och gar... |  | x |  | 12 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 4 | ...r, etc. Förut jobbade jag som **trädgårdsmästare** och med städning så har jag m... | x | x | 16 | 16 | [Trädgårdsmästare, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JZur_dmf_EPY) |
| 5 | ... som trädgårdsmästare och med **städning** så har jag mycket kunskap och... | x | x | 8 | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 6 | ...d mina jobb, det är inte bara **städning**, det finns byggstädning, trap... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 7 | ...inte bara städning, det finns **byggstädning**, trappstädning, allt i städni... | x |  |  | 12 | [Byggstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/T7dX_M3r_SDj) |
| 8 | ...ning, det finns byggstädning, **trappstädning**, allt i städningen rubrik, de... | x | x | 13 | 13 | [Trappstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/iLB3_qb9_a1D) |
| | **Overall** | | | **58** | **90** | 58/90 = **64%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Trädgårdsmästare, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JZur_dmf_EPY) |
| x |  |  | [Byggstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/T7dX_M3r_SDj) |
| x | x | x | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| x | x | x | [Trappstädning, **skill**](http://data.jobtechdev.se/taxonomy/concept/iLB3_qb9_a1D) |
| | | **5** | 5/6 = **83%** |