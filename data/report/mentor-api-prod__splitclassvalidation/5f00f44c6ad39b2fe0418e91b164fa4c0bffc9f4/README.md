# Results for '5f00f44c6ad39b2fe0418e91b164fa4c0bffc9f4'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [5f00f44c6ad39b2fe0418e91b164fa4c0bffc9f4](README.md) | 1 | 2500 | 27 | 15 | 110/561 = **20%** | 8/27 = **30%** |

## Source text

Kommunikatör och Content creator till Improvisationsteater  Göteborgs Improvisationsteater (Gbgimpro) söker en kommunikatör med inriktning på marknadsföring, omfattning 30%. Gbgimpro är en av Sveriges mest etablerade improvisationsgrupper med ambitionen att vara navet för högkvalitativ impro i Göteborg/Västsverige. Du kommer ingå i en ledningsgrupp tillsammans med teaterchef, konstnärlig ledare och producent, där du kommer ha huvudansvaret för att planera kommunikationsinsatser och marknadsföra det dagliga arbetet. Du gillar att ta egna initiativ, är van att jobba självständigt och arbetar gärna uppsökande med att marknadsföra och sälja in verksamhetens olika områden till respektive målgrupp. Kvalifikationer Vi söker dig som antingen har en eftergymnasial utbildning med inriktning på kommunikation och marknadsföring eller likvärdig kompetens/arbetslivserfarenhet. Du har erfarenhet av kvalificerat kommunikationsarbete, att arbeta i sociala medier, och är självgående i att skapa marknadsföringsplaner. Du är väl insatt i sociala och digitala medier och är nyfiken på att hitta nya effektiva kanaler. Du är en duktig skribent och van att anpassa budskap och innehåll till olika målgrupper och plattformar, samt är bekväm i att uttrycka dig i tal och skrift, på både svenska och engelska. Det är meriterande om du har erfarenhet av att arbeta med videoproduktion och fotografering och har färdighet inom grafisk design. Erfarenhet av teater/scenkonst och ännu hellre improvisationsteater är också meriterande, men vi välkomnar även olika livserfarenheter och bakgrunder till tjänsten och Gbgimpro. Välkommen med din ansökan senast 1 augusti. Om tjänsten Arbetsuppgifter innefattar ansvar för skapande av content på sociala medier, marknadsföring av Gbgimpros produkter, utskick av nyhetsbrev, införsäljning av gästspel och dyl. Tjänsten omfattar 30% och är en tidsbegränsad anställning på 1 år, som sedan ev kan förlängas. Tillträde i mitten av augusti/början av september, exakt datum efter överenskommelse. Placering: Arbetet sker främst hemifrån, samt på plats på Redbergsteatern i samband med föreställningar. Arbetstider: Flexibel, en variation av kontorstid samt kvällar och helger. Intervjuer kommer att ske löpande från det att annonsen lagts ut och maximalt fram till 15 augusti. Sök tjänsten senast 1 aug genom att skicka CV och personligt brev till oss till info@gbgimpro.se För Frågor Om Tjänsten Kontaktas: Anton Romanus, teaterchef, via email info@gbgimpro.se Jobbtyp: Deltid

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kommunikatör** och Content creator till Impr... | x | x | 12 | 12 | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| 2 | ...onsteater (Gbgimpro) söker en **kommunikatör** med inriktning på marknadsför... | x | x | 12 | 12 | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| 3 | ...ommunikatör med inriktning på **marknadsföring**, omfattning 30%. Gbgimpro är ... | x |  |  | 14 | [Marknadsföring/PR, **skill**](http://data.jobtechdev.se/taxonomy/concept/5yDE_FiQ_bMn) |
| 4 | ...vet för högkvalitativ impro i **Göteborg**/Västsverige. Du kommer ingå i... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 5 | ...ledningsgrupp tillsammans med **teaterchef**, konstnärlig ledare och produ... | x | x | 10 | 10 | [Teaterchef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/LnRE_5bo_nYh) |
| 6 | ...p tillsammans med teaterchef, **konstnärlig ledare** och producent, där du kommer ... | x |  |  | 18 | [konstnärlig ledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/hnbt_2W9_7kN) |
| 7 | ...rchef, konstnärlig ledare och **producent**, där du kommer ha huvudansvar... |  | x |  | 9 | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| 7 | ...rchef, konstnärlig ledare och **producent**, där du kommer ha huvudansvar... | x |  |  | 9 | [Producent: kultur, media, film, dataspel, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FcaP_vhz_Cuy) |
| 8 | ...ta egna initiativ, är van att **jobba självständigt** och arbetar gärna uppsökande ... | x |  |  | 19 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 9 | ...älvständigt och arbetar gärna **uppsökande** med att marknadsföra och sälj... | x |  |  | 10 | [Uppsökande försäljning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJBT_yzk_BWe) |
| 10 | ...söker dig som antingen har en **eftergymnasial utbildning** med inriktning på kommunikati... | x | x | 25 | 25 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 11 | ... utbildning med inriktning på **kommunikation och marknadsföring** eller likvärdig kompetens/arb... | x |  |  | 32 | [Marknadsföring, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/myT2_Uu2_Xbr) |
| 12 | ...ar erfarenhet av kvalificerat **kommunikationsarbete**, att arbeta i sociala medier,... | x |  |  | 20 | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| 12 | ...ar erfarenhet av kvalificerat **kommunikationsarbete**, att arbeta i sociala medier,... |  | x |  | 20 | [Informatörer, kommunikatörer och PR-specialister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/k1Nx_auG_sNh) |
| 13 | ...ier, och är självgående i att **skapa marknadsföringsplaner**. Du är väl insatt i sociala o... | x |  |  | 27 | [planera marknadsföringsstrategi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/yDMq_rzN_R2D) |
| 14 | ... marknadsföringsplaner. Du är **väl insatt i sociala och digitala medier** och är nyfiken på att hitta n... | x |  |  | 40 | [hantering av sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MgRt_szJ_YYJ) |
| 15 | ...tiva kanaler. Du är en duktig **skribent** och van att anpassa budskap o... |  | x |  | 8 | [Skribent, **job-title**](http://data.jobtechdev.se/taxonomy/concept/bbKc_hAw_snP) |
| 16 | ...n duktig skribent och van att **anpassa budskap och innehåll till olika målgrupper** och plattformar, samt är bekv... | x |  |  | 50 | [kommunicera med målgrupper, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/D5G5_7Vs_iSU) |
| 17 | ...dig i tal och skrift, på både **svenska** och engelska. Det är meritera... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 18 | ...h skrift, på både svenska och **engelska**. Det är meriterande om du har... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 19 | ... erfarenhet av att arbeta med **videoproduktion** och fotografering och har fär... | x |  |  | 15 | (not found in taxonomy) |
| 20 | ...rbeta med videoproduktion och **fotografering** och har färdighet inom grafis... | x | x | 13 | 13 | [Fotografering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zpaf_mDu_foK) |
| 21 | ...fering och har färdighet inom **grafisk design**. Erfarenhet av teater/scenkon... | x | x | 14 | 14 | [Grafisk design, **skill**](http://data.jobtechdev.se/taxonomy/concept/dTK5_R7G_p56) |
| 22 | ...grafisk design. Erfarenhet av **teater**/scenkonst och ännu hellre imp... | x |  |  | 6 | [Teater, **skill**](http://data.jobtechdev.se/taxonomy/concept/43v8_evE_cUE) |
| 22 | ...grafisk design. Erfarenhet av **teater**/scenkonst och ännu hellre imp... |  | x |  | 6 | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| 23 | ... design. Erfarenhet av teater/**scenkonst** och ännu hellre improvisation... | x | x | 9 | 9 | [Scenkonst, **skill**](http://data.jobtechdev.se/taxonomy/concept/ryXf_E4d_FYU) |
| 24 | ...pgifter innefattar ansvar för **skapande av content på sociala medier**, marknadsföring av Gbgimpros ... | x |  |  | 37 | [Publicering, webben och sociala medier, **skill**](http://data.jobtechdev.se/taxonomy/concept/k29D_fT4_Wso) |
| 25 | ...av content på sociala medier, **marknadsföring** av Gbgimpros produkter, utski... | x |  |  | 14 | [Marknadsföring/PR, **skill**](http://data.jobtechdev.se/taxonomy/concept/5yDE_FiQ_bMn) |
| 26 | ...öring av Gbgimpros produkter, **utskick av nyhetsbrev**, införsäljning av gästspel oc... | x |  |  | 21 | [utföra marknadsföring via sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/faVk_ypF_GaJ) |
| 27 | ...ukter, utskick av nyhetsbrev, **införsäljning av gästspel och dyl**. Tjänsten omfattar 30% och är... | x |  |  | 33 | [Marknadsföring/PR, **skill**](http://data.jobtechdev.se/taxonomy/concept/5yDE_FiQ_bMn) |
| 28 | ...änsten omfattar 30% och är en **tidsbegränsad anställning** på 1 år, som sedan ev kan för... | x |  |  | 25 | [Tidsbegränsad anställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/sTu5_NBQ_udq) |
| 29 | ...ten Kontaktas: Anton Romanus, **teaterchef**, via email info@gbgimpro.se J... |  | x |  | 10 | [Teaterchef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/LnRE_5bo_nYh) |
| | **Overall** | | | **110** | **561** | 110/561 = **20%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Teater, **skill**](http://data.jobtechdev.se/taxonomy/concept/43v8_evE_cUE) |
|  | x |  | [producent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/5LR4_huF_RWx) |
| x |  |  | [Marknadsföring/PR, **skill**](http://data.jobtechdev.se/taxonomy/concept/5yDE_FiQ_bMn) |
| x |  |  | [kommunicera med målgrupper, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/D5G5_7Vs_iSU) |
| x |  |  | [Uppsökande försäljning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJBT_yzk_BWe) |
| x |  |  | [Producent: kultur, media, film, dataspel, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FcaP_vhz_Cuy) |
| x | x | x | [Teaterchef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/LnRE_5bo_nYh) |
| x |  |  | [hantering av sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MgRt_szJ_YYJ) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
|  | x |  | [Skribent, **job-title**](http://data.jobtechdev.se/taxonomy/concept/bbKc_hAw_snP) |
| x | x | x | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| x | x | x | [Grafisk design, **skill**](http://data.jobtechdev.se/taxonomy/concept/dTK5_R7G_p56) |
| x |  |  | [utföra marknadsföring via sociala medier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/faVk_ypF_GaJ) |
| x |  |  | (not found in taxonomy) |
| x |  |  | [konstnärlig ledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/hnbt_2W9_7kN) |
|  | x |  | [Informatörer, kommunikatörer och PR-specialister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/k1Nx_auG_sNh) |
| x |  |  | [Publicering, webben och sociala medier, **skill**](http://data.jobtechdev.se/taxonomy/concept/k29D_fT4_Wso) |
| x |  |  | [Marknadsföring, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/myT2_Uu2_Xbr) |
| x | x | x | [Scenkonst, **skill**](http://data.jobtechdev.se/taxonomy/concept/ryXf_E4d_FYU) |
| x |  |  | [Tidsbegränsad anställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/sTu5_NBQ_udq) |
|  | x |  | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| x |  |  | [planera marknadsföringsstrategi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/yDMq_rzN_R2D) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [Fotografering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zpaf_mDu_foK) |
| | | **8** | 8/27 = **30%** |