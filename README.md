# jae-conrec

jae-conrec is a concept recognition software that uses Jobad Enrichments under the hood to find terms in text. It then attempts to map those terms to taxonomy concepts. Report generation and evaluation is performed using the [conrec-utils](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils) library.

## Getting Started

You need to install at least [Python3](https://www.python.org/downloads/), [Poetry](https://python-poetry.org/) and [Make](https://www.gnu.org/software/make/) in order to run the code. Once these programs have been installed, you are ready to try out the code.

1. Git clone this repository
2. `cd` into the root directory of this repository.
3. Create a virtual env by calling `poetry shell`
4. Install the dependencies using `poetry install`
5. Run the code on a dataset: `make process-dataset`

If the last step, `make process-dataset`, fails then please report it in an issue.

## Launching a Mentor API-compatible Web Service

1. Follow steps 1-4 under [Getting Started](getting-started).
2. Run `make start-server`
3. Call the API with text to annotate, e.g. [`http://127.0.0.1:5000/nlp/education-description?text=vi%20s%C3%B6ker%20mjuvaruutvecklare`](http://127.0.0.1:5000/nlp/education-description?text=vi%20s%C3%B6ker%20mjuvaruutvecklare).

## How the Algorithm Works

The target `concept-synonym-table` builds a mapping from terms in the JAE synonym directory to concepts by analyzing the concept labels using Jobad Enrichments.

The target `compute-frequencies` analyzes the training dataset using Jobad Enrichments to compute the frequencies of various concepts given a synonym.

The target `process-dataset` uses the results of `concept-synonym-table` and `compute-frequencies` to analyze the dataset.

## Testing

Call `make test` to run the tests.

## License

Copyright 2022 Arbetsförmedlingen JobTech.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

