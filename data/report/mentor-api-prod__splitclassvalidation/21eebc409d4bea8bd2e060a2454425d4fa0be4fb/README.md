# Results for '21eebc409d4bea8bd2e060a2454425d4fa0be4fb'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [21eebc409d4bea8bd2e060a2454425d4fa0be4fb](README.md) | 1 | 1826 | 17 | 9 | 24/339 = **7%** | 3/20 = **15%** |

## Source text

Drifttekniker, skift - Övik Energi Vi söker nu en ny kollega som drifttekniker till Kraftvärmeverket i Örnsköldsvik! Om du tycker om ny teknik, ständig förbättring och vill bidra till en bättre miljö är detta tjänsten för dig. Vårt erbjudande: Hörneborgsverket är en biobränslebaserad anläggning och en viktig del i Övik Energis energiproduktion. Din huvudarbetsuppgift är att säkerställa drift och övervakning av energiproduktionen. Du och dina kollegor kommer att utföra drift- och underhållsåtgärder, analysera driftsdata och dokumentera ditt arbete. Du kommer också att delta aktivt i vårt optimerings- och förbättringsarbete och ta del i ansvaret för att anläggningen sköts på ett miljö- och driftekonomiskt sätt. Omfattning; tillsvidareanställning, heltid, skift. 12 timmarspass. Arbetstiderna är 06-18 och 18-06.  Kvalifikationer: Vi tror att du har driftteknikerutbildning/driftingenjör eller likvärdig utbildning, gärna med erfarenhet från processindustrin eller energibranschen. Som person är du noggrann, systematisk och självgående i ditt arbetssätt. Du drivs av att lösa problem både på egen hand och i grupp. Du är lugn och stabil och behärskar situationer under press. Goda datakunskaper, B-körkort samt god kommunikativ förmåga i svenska i tal och skrift. Vi lägger stor vikt vid personlig lämplighet och färdigheter. Välkommen med din ansökan: Vi önskar ha din ansökan senast den 21 augusti. Vi arbetar aktivt för att öka vår mångfald och ser varje medarbetare som en unik resurs. Kommunkoncernen ingår i ett finskt-, samiskt förvaltningsområde. För mer information: Kontakta Jonas Nordin, chef Producera telefon 070-19 19 859, Helen Eriksson, personalutvecklare 072 -54 28 904 Fackliga kontakter: Vision: Anna Sjölander 073-274 14 26 Ledarna: Stefan Fällström 070-680 34 46 Seko: Tapani Pohjola 070-347 50 85

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Drifttekniker**, skift - Övik Energi Vi söker... |  | x |  | 13 | [Fastighetstekniker/Drifttekniker, fastighet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9NVZ_LfD_vqT) |
| 1 | **Drifttekniker**, skift - Övik Energi Vi söker... | x |  |  | 13 | [Drifttekniker, värmeverk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GaC9_y4i_o8S) |
| 2 | ...Vi söker nu en ny kollega som **drifttekniker** till Kraftvärmeverket i Örnsk... |  | x |  | 13 | [Fastighetstekniker/Drifttekniker, fastighet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9NVZ_LfD_vqT) |
| 2 | ...Vi söker nu en ny kollega som **drifttekniker** till Kraftvärmeverket i Örnsk... | x |  |  | 13 | [Drifttekniker, värmeverk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GaC9_y4i_o8S) |
| 3 | ...niker till Kraftvärmeverket i **Örnsköldsvik**! Om du tycker om ny teknik, s... |  | x |  | 12 | [Örnsköldsvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zBmE_n6s_MnQ) |
| 4 | ...na kollegor kommer att utföra **drift- och underhållsåtgärder**, analysera driftsdata och dok... | x |  |  | 29 | [Värmeanläggning, underhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/jZzL_yV6_PmA) |
| 5 | ...rift- och underhållsåtgärder, **analysera driftsdata** och dokumentera ditt arbete. ... | x |  |  | 20 | [analysera testdata, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PGgM_Grf_VJZ) |
| 6 | ...der, analysera driftsdata och **dokumentera ditt arbete**. Du kommer också att delta ak... | x |  |  | 23 | [dokumentera sitt eget arbete, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EbfD_wK3_VuL) |
| 7 | ...tekonomiskt sätt. Omfattning; **tillsvidareanställning**, heltid, skift. 12 timmarspas... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 8 | ...ning; tillsvidareanställning, **heltid**, skift. 12 timmarspass. Arbet... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 9 | ...llsvidareanställning, heltid, **skift**. 12 timmarspass. Arbetstidern... | x |  |  | 5 | [Skiftarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/YnG9_nVP_CsZ) |
| 10 | ...ikationer: Vi tror att du har **driftteknikerutbildning**/driftingenjör eller likvärdig... | x |  |  | 23 | [Teknik och teknisk industri, allmän utbildning, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/LDWD_zzm_jeY) |
| 10 | ...ikationer: Vi tror att du har **driftteknikerutbildning**/driftingenjör eller likvärdig... |  | x |  | 23 | [Driftteknikerutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kXWG_wZe_X1t) |
| 11 | ...u har driftteknikerutbildning/**driftingenjör** eller likvärdig utbildning, g... |  | x |  | 13 | [Driftingenjör, elkraft, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JaTP_TuX_3wA) |
| 11 | ...u har driftteknikerutbildning/**driftingenjör** eller likvärdig utbildning, g... | x |  |  | 13 | [Ingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/SNvJ_i6V_DZH) |
| 12 | ...t från processindustrin eller **energibranschen**. Som person är du noggrann, s... |  | x |  | 15 | [energi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cNSw_KWL_Jbv) |
| 13 | ...gibranschen. Som person är du **noggrann**, systematisk och självgående ... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 14 | ... du noggrann, systematisk och **självgående i ditt arbetssätt**. Du drivs av att lösa problem... | x |  |  | 29 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ...t arbetssätt. Du drivs av att **lösa problem** både på egen hand och i grupp... | x |  |  | 12 | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| 16 | ...skar situationer under press. **Goda datakunskaper**, B-körkort samt god kommunika... | x |  |  | 18 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 17 | ...er press. Goda datakunskaper, **B-körkort** samt god kommunikativ förmåga... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 18 | ...mt god kommunikativ förmåga i **svenska** i tal och skrift. Vi lägger s... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **24** | **339** | 24/339 = **7%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Fastighetstekniker/Drifttekniker, fastighet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9NVZ_LfD_vqT) |
| x |  |  | [dokumentera sitt eget arbete, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EbfD_wK3_VuL) |
| x |  |  | [Drifttekniker, värmeverk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GaC9_y4i_o8S) |
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
|  | x |  | [Driftingenjör, elkraft, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JaTP_TuX_3wA) |
| x |  |  | [Teknik och teknisk industri, allmän utbildning, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/LDWD_zzm_jeY) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [analysera testdata, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PGgM_Grf_VJZ) |
| x |  |  | [Ingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/SNvJ_i6V_DZH) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [hitta lösningar på problem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WFSY_Bwt_QWE) |
| x |  |  | [Skiftarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/YnG9_nVP_CsZ) |
|  | x |  | [energi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cNSw_KWL_Jbv) |
| x |  |  | [Värmeanläggning, underhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/jZzL_yV6_PmA) |
|  | x |  | [Driftteknikerutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kXWG_wZe_X1t) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [Örnsköldsvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zBmE_n6s_MnQ) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/20 = **15%** |