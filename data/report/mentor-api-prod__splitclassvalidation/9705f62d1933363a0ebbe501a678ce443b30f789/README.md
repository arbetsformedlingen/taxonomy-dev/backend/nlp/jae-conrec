# Results for '9705f62d1933363a0ebbe501a678ce443b30f789'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9705f62d1933363a0ebbe501a678ce443b30f789](README.md) | 1 | 2212 | 20 | 15 | 61/309 = **20%** | 5/19 = **26%** |

## Source text

Frisör sökes till Göteborg! Vi söker frisörer som brinner för yrket och att ge fantastisk kundservice! Är du vår nya frisörstjärna?  Vi söker nu frisörer på heltid eller deltid till vår salong i Västra Frölunda med start omgående. Vi ger effektiva och kvalitetssäkra klipp med samma pris för alla! Vi har 123 frisörsalonger i Sverige, Norge och Finland med ambitionen att fortsätta växa!  Hos oss slipper du att att skapa dig en egen kundkrets, avbokningar i sista sekund och att jobba med kemikalier. Så om du är en frisör som älskar att klippa och älskar att ge bra service kommer du att trivas hos oss - vi tar hand om varandra, och vårt mål är att skapa den bästa arbetsmiljön för våra medarbetare 💛  Arbetsuppgifter  - Utför effektiva och kvalitetssäkrade hårklippningar. - Ge god kundservice och produktförsäljning. - Bidra till ett gott samarbete och en inkluderande arbetsmiljö. - Arbetstider: Dag-, kvälls- och helgpass enligt gällande schema.     Yrkes kvalifikationer  - Erfarenhet som frisör - Utbildning som frisör (Lång erfarenhet kan kompensera för bristande utbildningskrav). - God digital kompetens. - Behärska svenska eller engelska i tal och skrift.     Personliga kvalifikationer  - Serviceinriktad och självgående. - Flexibel. - Gillar att arbeta i team och är inkluderande mot andra kollegor. - God kommunikationsförmåga. - Brinner för yrket, och är motiverad för nya utmaningar och lärande.     Varför ska du välja oss?  - Vi har roligt på jobbet! - Tillsvidareanställning med fast lön + bonus - Pension, försäkringar och friskvårdsbidrag. - 1 veckas betald utbildning på Cutters Academy. - Roliga evenemang och AWs. - Internationella karriärmöjligheter.   Om oss  Cutters är Norges mest innovativa och snabbast växande frisörkoncept, och etablerades i Bergen 2015. Idag har vi 122 salonger i Norge, Sverige och Finland. Våra frisörsalonger är bland de mest besökta i hela landet och vi får mycket uppmärksamhet från både media och kunder. Vi växer ständigt, och söker därför inspirerande och engagerade personer som vill arbeta hos oss. Det finns mycket goda utvecklingsmöjligheter för de som utmärker sig inom Cutters, då vi har ambitiösa framtidsplaner i både Norge och internationellt.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Frisör** sökes till Göteborg! Vi söker... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 2 | Frisör sökes till **Göteborg**! Vi söker frisörer som brinne... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 3 | ...sökes till Göteborg! Vi söker **frisörer** som brinner för yrket och att... | x | x | 8 | 8 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 4 | ...r yrket och att ge fantastisk **kundservice**! Är du vår nya frisörstjärna?... |  | x |  | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 5 | ...a frisörstjärna?  Vi söker nu **frisörer** på heltid eller deltid till v... | x | x | 8 | 8 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 6 | ...rna?  Vi söker nu frisörer på **heltid eller deltid** till vår salong i Västra Fröl... | x |  |  | 19 | (not found in taxonomy) |
| 7 | ...! Vi har 123 frisörsalonger i **Sverige**, Norge och Finland med ambiti... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 8 | ...123 frisörsalonger i Sverige, **Norge** och Finland med ambitionen at... | x |  |  | 5 | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
| 9 | ...salonger i Sverige, Norge och **Finland** med ambitionen att fortsätta ... | x |  |  | 7 | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
| 10 | ...ed kemikalier. Så om du är en **frisör** som älskar att klippa och äls... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 11 | ...ifter  - Utför effektiva och k**valitetssäkrade hårklippningar.** - Ge god kundservice och prod... | x |  |  | 31 | [hårklippningstekniker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ctoB_kAr_kEo) |
| 12 | ...rade hårklippningar. - Ge god **kundservice** och produktförsäljning. - Bid... |  | x |  | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 13 | ...ar. - Ge god kundservice och p**roduktförsäljning.** - Bidra till ett gott samarbe... | x |  |  | 18 | [sälja hårprodukter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gZrf_JsN_wsT) |
| 14 | ...samarbete och en inkluderande **arbetsmiljö**. - Arbetstider: Dag-, kvälls-... |  | x |  | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 15 | ...ifikationer  - Erfarenhet som **f**risör - Utbildning som frisör ... |  | x |  | 1 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 16 | ...fikationer  - Erfarenhet som f**risör** - Utbildning som frisör (Lång... | x | x | 5 | 5 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 17 | ...ioner  - Erfarenhet som frisör** **- Utbildning som frisör (Lång ... | x |  |  | 1 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 18 | ...r  - Erfarenhet som frisör - U**tbildning som frisör **(Lång erfarenhet kan kompenser... | x |  |  | 21 | [Hårvård, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/yzM8_Js8_zYQ) |
| 19 | ...t som frisör - Utbildning som **frisör** (Lång erfarenhet kan kompense... |  | x |  | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 20 | ...ristande utbildningskrav). - G**od digital kompetens.** - Behärska svenska eller enge... | x |  |  | 21 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 21 | ...digital kompetens. - Behärska **s**venska eller engelska i tal oc... |  | x |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 22 | ...igital kompetens. - Behärska s**venska** eller engelska i tal och skri... | x | x | 6 | 6 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 23 | ... kompetens. - Behärska svenska** **eller engelska i tal och skrif... | x |  |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 24 | ...ens. - Behärska svenska eller **e**ngelska i tal och skrift.     ... |  | x |  | 1 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 25 | ...ns. - Behärska svenska eller e**ngelska** i tal och skrift.     Personl... | x | x | 7 | 7 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 26 | ...ehärska svenska eller engelska** **i tal och skrift.     Personli... | x |  |  | 1 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 27 | ...ioner  - Serviceinriktad och s**jälvgående.** - Flexibel. - Gillar att arbe... | x |  |  | 11 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 28 | ...de. - Flexibel. - Gillar att a**rbeta i team **och är inkluderande mot andra ... | x |  |  | 13 | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| 29 | ...erande mot andra kollegor. - G**od kommunikationsförmåga.** - Brinner för yrket, och är m... | x |  |  | 25 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 30 | ...- Vi har roligt på jobbet! - T**illsvidareanställning **med fast lön + bonus - Pension... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 31 | ...ademy. - Roliga evenemang och **AWs**. - Internationella karriärmöj... |  | x |  | 3 | [Amazon Web Services (AWS), molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/KRDJ_nhm_wLb) |
| 32 | ... har vi 122 salonger i Norge, **Sverige** och Finland. Våra frisörsalon... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **61** | **309** | 61/309 = **20%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
|  | x |  | [Amazon Web Services (AWS), molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/KRDJ_nhm_wLb) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x |  |  | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
| x |  |  | [arbeta som ett team, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VFVZ_4YG_Ydi) |
| x |  |  | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
|  | x |  | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x |  |  | [hårklippningstekniker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ctoB_kAr_kEo) |
| x |  |  | [sälja hårprodukter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gZrf_JsN_wsT) |
| x |  |  | (not found in taxonomy) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| x |  |  | [Hårvård, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/yzM8_Js8_zYQ) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
|  | x |  | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **5** | 5/19 = **26%** |