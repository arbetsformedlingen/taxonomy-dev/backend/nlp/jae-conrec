# Results for 'd9119a7aaeefaf6cae8f63e5ba3fc407714d2d9b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d9119a7aaeefaf6cae8f63e5ba3fc407714d2d9b](README.md) | 1 | 616 | 8 | 6 | 19/107 = **18%** | 3/9 = **33%** |

## Source text

Pizzabagare Vi söker dig som vill jobba som Pizzabagare! Du som söker ska vara stresstålig samt kunna ha många bollar i luften och kunna arbeta i ett högt tempo, Arbetsuppgifterna för den här rollen är mycket varierande, men du kommer huvudsakligen förbereda råvaror, baka pizza, städa, diska och ge råd kring mat och dryck samt ansvara för att restaurangen alltid är presentabel och inbjudande. Meriterande : - Har kassavana - Är van vid att arbeta kvällar och helger. - Jobbat minst 1 år i kök.  Tjänsten startas omgående och är på heltid. Ansökan skickas till: hejdegarden@outlook.com Telefonnummer: 073-997 95 66

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Pizzabagare** Vi söker dig som vill jobba s... | x |  |  | 11 | [Pizzabagare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9YaE_Syg_5ZB) |
| 1 | **Pizzabagare** Vi söker dig som vill jobba s... |  | x |  | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 2 | ... söker dig som vill jobba som **Pizzabagare**! Du som söker ska vara stress... | x |  |  | 11 | [Pizzabagare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9YaE_Syg_5ZB) |
| 2 | ... söker dig som vill jobba som **Pizzabagare**! Du som söker ska vara stress... |  | x |  | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 3 | ...bagare! Du som söker ska vara **stresstålig** samt kunna ha många bollar i ... |  | x |  | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 4 | ...udsakligen förbereda råvaror, **baka **pizza, städa, diska och ge råd... |  | x |  | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 5 | ...ligen förbereda råvaror, baka **pizza**, städa, diska och ge råd krin... | x | x | 5 | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 6 | ...örbereda råvaror, baka pizza, **städa**, diska och ge råd kring mat o... | x |  |  | 5 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 7 | ...a råvaror, baka pizza, städa, **diska** och ge råd kring mat och dryc... | x | x | 5 | 5 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 8 | ...bjudande. Meriterande : - Har **kassavana** - Är van vid att arbeta kväll... | x | x | 9 | 9 | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| 9 | ... arbeta kvällar och helger. - **Jobbat minst 1 år** i kök.  Tjänsten startas omgå... | x |  |  | 17 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| 10 | ...en startas omgående och är på **heltid**. Ansökan skickas till: hejdeg... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **19** | **107** | 19/107 = **18%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Pizzabagare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/9YaE_Syg_5ZB) |
| x | x | x | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x |  |  | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
|  | x |  | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| x | x | x | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| x | x | x | [Kassavana, **skill**](http://data.jobtechdev.se/taxonomy/concept/hYpk_RYx_y3V) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
|  | x |  | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **3** | 3/9 = **33%** |