# Results for '677869e762dfa8f9722133d1657625cc00484712'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [677869e762dfa8f9722133d1657625cc00484712](README.md) | 1 | 4173 | 20 | 35 | 107/551 = **19%** | 5/19 = **26%** |

## Source text

Sjuksköterska/Distriktssköterska till Vårdcentralen i Älvdalen Hos oss på Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hälsosamt Dalarna!  ARBETSBESKRIVNING  Vårdcentral Älvdalen söker distriktssköterska eller sjuksköterska. Är du utbildad distriktssköterska eller sjuksköterska, flexibel, driven och serviceinriktad? Då kan du vara en av våra blivande medarbetare.   Vårdcentral Älvdalen är en väl fungerande och trevlig vårdcentral med ett upptagningsområde på cirka 5000 invånare. Vi är cirka 30 stycken anställda på en vårdcentral som omfattar mottagning inom primärvårdens samtliga uppdrag. Vi arbetar nära varandra och känner stor gemenskap. Vi lägger stor vikt vid kvalité och tillgänglighet samt har ett gott samarbetsklimat på vårdcentralen.  Vi är måna om att ta tillvara våra medarbetares kompetens och uppmuntrar till utbildning och kompetensutveckling utifrån ditt och verksamhetens behov. Vi arbetar mycket med att utveckla arbetsgruppen och våra arbetssätt.  Hos oss får du ett stimulerande arbete som växlar mellan telefonrådgivning och mottagningsarbete. Mottagningsarbetet kan vara både planerad och akut mottagning, Du förväntas kunna ta egna beslut och färdigbehandla när det är möjligt. Utöver det dagliga mottagningsarbetet har alla specifika ansvarsområden.  För att du ska trivas hos oss bör du kunna arbeta självständigt och gärna ta egna initiativ. Samt även ha förmåga och förstå värdet av samarbete, eftersom teamsamverkan är en viktig del av vårt arbetssätt. Vi värdesätter egenskaper som engagemang och positiv attityd.   Vi vill ständigt utvecklas och bli bättre för att kunna ge våra patienter bästa möjliga vård och vi vill därför ge våra medarbetare förutsättningar till samverkan och erfarenhetsutbyte. Vår arbetsmiljö är viktig och den ska präglas av dialog och trivsel.  Arbetstiden är mellan måndag till fredag klockan 8-17, enligt schema.   KVALIFIKATIONER  - Leg. distriktssköterska/sjuksköterska alternativt annan relevant vidare utbildning.  Erfarenhet och vana av telefonrådgivning och mottagningsarbete på vårdcentral/mottagning alternativ verksamhetserfarenhet inom något av våra ansvarsområden är också meriterande.  Stor vikt läggs vid personlig lämplighet.  Personliga egenskaper: - Vi sätter stor vikt vid att du tar ansvar och är konstruktiv. - Din samarbetsförmåga är avgörande och du behöver ha en hög arbetskapacitet. - Som person är du stabil, trygg och empatisk. Du har förmåga att anpassa dig till olika situationer och ändrade omständigheter. Du är strukturerad och kan planera och organisera ditt arbete på ett effektivt sätt.   Region Dalarna värnar om en hälsosam arbetsmiljö för våra medarbetare samt säkra och trygga möten med medborgare, patienter, kunder och samarbetspartners. Som en del i vår rekryteringsprocess genomför vi därför drogtest innan erbjudande om anställning, samt kontrollerar ditt vaccinationsbevis då Region Dalarna har som krav att våra medarbetare ska vara vaccinerade mot covid-19.   Alla anställda inom Region Dalarna som arbetar i en verksamhet där direkt och regelbunden kontakt med barn och ungdomar förekommer ska uppvisa godtagbart utdrag ur Polisens belastningsregister. Före erbjudande av anställning ska ett giltigt utdrag uppvisas i osprättat kuvert.  Region Dalarna tillämpar krigsplacering i samband med nyanställning. Detta innebär att du som arbetstagare har ett lagkrav att ta dig till ordinarie arbetsplats i händelse av högsta beredskap.  Region Dalarnas uppgift är att göra Dalarna till en ännu bättre plats att leva på. Vi har ett brett uppdrag så möjligheterna är stora att du hittar ditt drömjobb hos oss! Vi ansvarar för hälso- och sjukvård, tandvård, hjälpmedel, kollektivtrafik och folkhögskolor, samt arbetar med folkbildning, forskning, folkhälsa och kultur. Vi arbetar också med utvecklingsfrågor inom infrastruktur, näringsliv, arbetsmarknad och miljö.   Vår värdegrund Öppenhet, Respekt och Ansvar ska genomsyra hela vår organisation. Det ledarskap Region Dalarna strävar mot beskrivs utifrån orden: tydlighet, mod och tillit.  Läs mer om hur det är att jobba hos oss på regiondalarna.se/jobb

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sjuksköterska**/Distriktssköterska till Vårdc... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 2 | Sjuksköterska/**Distriktssköterska** till Vårdcentralen i Älvdalen... | x |  |  | 18 | [Distriktssköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n2Tp_B8E_gED) |
| 2 | Sjuksköterska/**Distriktssköterska** till Vårdcentralen i Älvdalen... |  | x |  | 18 | [Distriktssköterskor, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/oj4c_P4b_cja) |
| 3 | ...köterska till Vårdcentralen i **Älvdalen** Hos oss på Region Dalarna får... |  | x |  | 8 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 3 | ...köterska till Vårdcentralen i **Älvdalen** Hos oss på Region Dalarna får... | x |  |  | 8 | [Älvdalen, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cZtt_qGo_oBr) |
| 4 | ... i Älvdalen Hos oss på Region **Dalarna** får du ett meningsfullt arbet... | x | x | 7 | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 5 | ... arbetar vi för ett hälsosamt **Dalarna**!  ARBETSBESKRIVNING  Vård... | x | x | 7 | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 6 | ...arna!  ARBETSBESKRIVNING  **Vårdcentral** Älvdalen söker distriktssköte... |  | x |  | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 7 | ...ETSBESKRIVNING  Vårdcentral **Älvdalen** söker distriktssköterska elle... |  | x |  | 8 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 8 | ...  Vårdcentral Älvdalen söker **distriktssköterska** eller sjuksköterska. Är du ut... | x |  |  | 18 | [Distriktssköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n2Tp_B8E_gED) |
| 8 | ...  Vårdcentral Älvdalen söker **distriktssköterska** eller sjuksköterska. Är du ut... |  | x |  | 18 | [Distriktssköterskor, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/oj4c_P4b_cja) |
| 9 | ...öker distriktssköterska eller **sjuksköterska**. Är du utbildad distriktssköt... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 10 | ...sjuksköterska. Är du utbildad **distriktssköterska** eller sjuksköterska, flexibel... | x |  |  | 18 | [Distriktssköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n2Tp_B8E_gED) |
| 10 | ...sjuksköterska. Är du utbildad **distriktssköterska** eller sjuksköterska, flexibel... |  | x |  | 18 | [Distriktssköterskor, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/oj4c_P4b_cja) |
| 11 | ...ldad distriktssköterska eller **sjuksköterska**, flexibel, driven och service... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 12 | ... våra blivande medarbetare.   **Vårdcentral** Älvdalen är en väl fungerande... |  | x |  | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 13 | ...de medarbetare.   Vårdcentral **Älvdalen** är en väl fungerande och trev... |  | x |  | 8 | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
| 14 | ...ka 30 stycken anställda på en **vårdcentral** som omfattar mottagning inom ... |  | x |  | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 15 | ...å en vårdcentral som omfattar **mottagning** inom primärvårdens samtliga u... |  | x |  | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 16 | ...ivning och mottagningsarbete. **Mottagning**sarbetet kan vara både planera... |  | x |  | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 17 | ...ottagning, Du förväntas kunna **ta egna beslut** och färdigbehandla när det är... | x |  |  | 14 | [fatta beslut, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7akt_v7j_ggK) |
| 18 | ...a trivas hos oss bör du kunna **arbeta självständigt** och gärna ta egna initiativ. ... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 19 | ... förmåga och förstå värdet av **samarbete**, eftersom teamsamverkan är en... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 20 | ...an och erfarenhetsutbyte. Vår **arbetsmiljö** är viktig och den ska präglas... |  | x |  | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 21 | ...   KVALIFIKATIONER  - Leg. **distriktssköterska**/sjuksköterska alternativt ann... | x |  |  | 18 | [Distriktssköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n2Tp_B8E_gED) |
| 21 | ...   KVALIFIKATIONER  - Leg. **distriktssköterska**/sjuksköterska alternativt ann... |  | x |  | 18 | [Distriktssköterskor, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/oj4c_P4b_cja) |
| 22 | ...  - Leg. distriktssköterska/**sjuksköterska** alternativt annan relevant vi... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 23 | ...ning.  Erfarenhet och vana av **telefonrådgivning** och mottagningsarbete på vård... | x |  |  | 17 | [kommunicera via telefon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6VMf_ZtR_fAb) |
| 24 | ...vana av telefonrådgivning och **mottagning**sarbete på vårdcentral/mottagn... | x | x | 10 | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 25 | ...lefonrådgivning och mottagning**sarbete på vårdcentral**/mottagning alternativ verksam... | x |  |  | 22 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 26 | ...ning och mottagningsarbete på **vårdcentral**/mottagning alternativ verksam... |  | x |  | 11 | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
| 27 | ...tagningsarbete på vårdcentral/**mottagning** alternativ verksamhetserfaren... | x |  |  | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 28 | ...i sätter stor vikt vid att du **tar ansvar** och är konstruktiv. - Din sam... | x |  |  | 10 | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
| 29 | ...var och är konstruktiv. - Din **samarbetsförmåga** är avgörande och du behöver h... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 30 | ...ett effektivt sätt.   Region **Dalarna** värnar om en hälsosam arbetsm... |  | x |  | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 31 | ...Dalarna värnar om en hälsosam **arbetsmiljö** för våra medarbetare samt säk... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 32 | ...t vaccinationsbevis då Region **Dalarna** har som krav att våra medarbe... |  | x |  | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 33 | ...   Alla anställda inom Region **Dalarna** som arbetar i en verksamhet d... |  | x |  | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 34 | ...s i osprättat kuvert.  Region **Dalarna** tillämpar krigsplacering i sa... |  | x |  | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 35 | ... av högsta beredskap.  Region **Dalarna**s uppgift är att göra Dalarna ... |  | x |  | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 36 | ...jobb hos oss! Vi ansvarar för **hälso- oc**h sjukvård, tandvård, hjälpmed... |  | x |  | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 37 | ...arar för hälso- och sjukvård, **tandvård**, hjälpmedel, kollektivtrafik ... |  | x |  | 8 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 38 | ...ukvård, tandvård, hjälpmedel, **kollektivtrafik** och folkhögskolor, samt arbet... |  | x |  | 15 | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| 39 | ...lkhögskolor, samt arbetar med **folkbildning**, forskning, folkhälsa och kul... |  | x |  | 12 | [Folkbildningsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/8V12_wQM_NWp) |
| 40 | ... med folkbildning, forskning, **folkhälsa** och kultur. Vi arbetar också ... |  | x |  | 9 | [folkhälsa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jEdA_1Fe_Jji) |
| 41 | ...isation. Det ledarskap Region **Dalarna** strävar mot beskrivs utifrån ... |  | x |  | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| | **Overall** | | | **107** | **551** | 107/551 = **19%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [kommunicera via telefon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6VMf_ZtR_fAb) |
| x |  |  | [fatta beslut, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7akt_v7j_ggK) |
|  | x |  | [Folkbildningsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/8V12_wQM_NWp) |
| x |  |  | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
|  | x |  | [Vårdcentral, **skill**](http://data.jobtechdev.se/taxonomy/concept/FJjr_4pF_LEY) |
|  | x |  | [Försvarsmakten, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GSC3_yU4_K5V) |
|  | x |  | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
|  | x |  | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| x | x | x | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
|  | x |  | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| x |  |  | [Älvdalen, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cZtt_qGo_oBr) |
|  | x |  | [folkhälsa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jEdA_1Fe_Jji) |
| x |  |  | [Distriktssköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/n2Tp_B8E_gED) |
| x | x | x | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
|  | x |  | [Distriktssköterskor, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/oj4c_P4b_cja) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **5** | 5/19 = **26%** |