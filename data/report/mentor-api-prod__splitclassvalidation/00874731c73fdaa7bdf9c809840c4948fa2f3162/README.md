# Results for '00874731c73fdaa7bdf9c809840c4948fa2f3162'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [00874731c73fdaa7bdf9c809840c4948fa2f3162](README.md) | 1 | 304 | 4 | 3 | 21/36 = **58%** | 2/3 = **67%** |

## Source text

Frisör Frisör med erfarenhet som vill jobba heltid / deltid i mitt ihjärtat av Stockholm i stressfri arbets miljö med hög kvalitet i focus. #jobbjustnu Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Frisör** Frisör med erfarenhet som vil... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 2 | Frisör **Frisör** med erfarenhet som vill jobba... | x | x | 6 | 6 | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| 3 | ...med erfarenhet som vill jobba **heltid / deltid** i mitt ihjärtat av Stockholm ... | x |  |  | 15 | (not found in taxonomy) |
| 4 | ...d / deltid i mitt ihjärtat av **Stockholm** i stressfri arbets miljö med ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| | **Overall** | | | **21** | **36** | 21/36 = **58%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | (not found in taxonomy) |
| x | x | x | [Frisör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/tuoE_9Nj_fAS) |
| | | **2** | 2/3 = **67%** |