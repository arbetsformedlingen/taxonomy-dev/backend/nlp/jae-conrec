# Results for 'd0c3df23018ef3371ebd523dded6dac92d99288e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d0c3df23018ef3371ebd523dded6dac92d99288e](README.md) | 1 | 2804 | 14 | 32 | 93/588 = **16%** | 5/18 = **28%** |

## Source text

Visual Merchandiser/Butikssäljare ca. 50% Cubus Karlshamn Drottninggatan Vi söker en Visual Merchandiser/Butikssäljare som vill vara med och vidareutveckla Cubus och säkra att vi är det bästa valet när folk ska handla sina favoritplagg!  Första intrycket och helhetsupplevelsen av butiken är avgörande för om kunden väljer att handla på Cubus. Som Visual Merchandiser ska du se till att presentera varorna så att butiken är kommersiell, inspirerande och säljande. Som Butikssäljare hos oss är din uppgift att ta hand om våra kunder på bästa möjliga sätt. Detta gör du genom att hjälpa och se till att butiken är attraktiv och spännande. Tillsammans med dina kollegor sprider du glädje och gör det enkelt för kunden att hitta sina favoritplagg och må bra! Viktigaste ansvar i rollen:   - Dra kundens uppmärksamhet till butiken  - Hålla kundens uppmärksamhet  - Säkra att kunden rör sig genom hela butiken och hittar sina favoriter  Viktigaste arbetsuppgifter:   - I samarbete med butikschef, stötta och bidra med visuell kompetens till teamet som du är en del av  - Säkra att koncept och visuella guidelines följs, med lokala anpassningar  - Bidra och planlägga för maximal effekt av kampanjer  - Med kunden i fokus skapar du en inspirerande, relevant och överskådlig köpupplevelse  - Jobba aktivt och kontinuerligt med golvstruktur, varubärare och ljussättning  - Använda externa och interna kommunikationskanaler för att inhämta och dela bästa praxis  - Ge bästa tänkbara kundservice och se till att våra kunder får en köpupplevelse utöver det vanliga  Vi kan erbjuda:   - En varierande och spännande arbetsvardag i en inkluderande gemenskap, där vår «big family» kultur genomsyrar allt vi gör  - Bonussystem  - Förmånlig personalrabatt i Varners butiker   Arbetstiderna är varierande, dag-, kväll-, och helg.   Har du ett «big smile» och de egenskaperna vi letar efter? Då hoppas vi att du vill skicka in din ansökan redan i dag.  Vi ser fram emot att höra ifrån dig!  Cubus är en av Nordens största modekedjor med 300 butiker i Norge, Sverige och Finland. Vi betonar garderobens viktigaste plagg, bra material och rimliga priser, vi erbjuder kläder för spädbarn, barn, kvinnor och män. Naturmaterial, moderna, bekväma passformar och bra funktion anpassad till alla årstider beskriver konceptet väl. På Cubus älskar vi tempo och strävar mot att skapa den perfekta shoppingupplevelsen, vi arbetar tillsammans och firar framgångar. Vårt mål är att erbjuda kundens favoriter, och vi uppnår detta eftersom vi har världens bästa kollegor!     Cubus är en del av modekoncernen Varner som består av flera profilerade kedjor; Dressmann, Carlings, Bik Bok, Junkyard och Levi’s Store. Verksamheten omfattar för närvarande 6 nätbutiker och över 1200 fysiska butiker i Norge, Sverige, Finland, Island och Danmark.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Visual Merchandiser**/Butikssäljare ca. 50% Cubus K... |  | x |  | 19 | [Analytisk förmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/2HCr_Mkj_UNr) |
| 1 | **Visual Merchandiser**/Butikssäljare ca. 50% Cubus K... |  | x |  | 19 | [Kreativ förmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/5bhk_AGv_HNA) |
| 1 | **Visual Merchandiser**/Butikssäljare ca. 50% Cubus K... | x | x | 19 | 19 | [Butikskommunikatör/Visual merchandiser, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GTFy_iPM_msj) |
| 1 | **Visual Merchandiser**/Butikssäljare ca. 50% Cubus K... |  | x |  | 19 | [Ansvarstagande - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/GeDs_WtB_Zwe) |
| 1 | **Visual Merchandiser**/Butikssäljare ca. 50% Cubus K... |  | x |  | 19 | [Resultatorienterad - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/PuYR_VvL_4AQ) |
| 1 | **Visual Merchandiser**/Butikssäljare ca. 50% Cubus K... |  | x |  | 19 | [Kommunikationsförmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/aiab_pmE_an8) |
| 1 | **Visual Merchandiser**/Butikssäljare ca. 50% Cubus K... |  | x |  | 19 | [Digital kompetens - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/hkyz_wHi_Eut) |
| 1 | **Visual Merchandiser**/Butikssäljare ca. 50% Cubus K... |  | x |  | 19 | [Samarbetsförmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/nzAf_9WF_y8g) |
| 2 | Visual Merchandiser/**Butikssäljare **ca. 50% Cubus Karlshamn Drottn... | x |  |  | 14 | [Butikssäljare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/JVsN_YXp_9mq) |
| 3 | ...erchandiser/Butikssäljare ca. **50%** Cubus Karlshamn Drottninggata... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 4 | ...r/Butikssäljare ca. 50% Cubus **Karlshamn** Drottninggatan Vi söker en Vi... | x | x | 9 | 9 | [Karlshamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/HtGW_WgR_dpE) |
| 5 | ...mn Drottninggatan Vi söker en **Visual Merchandiser**/Butikssäljare som vill vara m... |  | x |  | 19 | [Analytisk förmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/2HCr_Mkj_UNr) |
| 5 | ...mn Drottninggatan Vi söker en **Visual Merchandiser**/Butikssäljare som vill vara m... |  | x |  | 19 | [Kreativ förmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/5bhk_AGv_HNA) |
| 5 | ...mn Drottninggatan Vi söker en **Visual Merchandiser**/Butikssäljare som vill vara m... | x | x | 19 | 19 | [Butikskommunikatör/Visual merchandiser, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GTFy_iPM_msj) |
| 5 | ...mn Drottninggatan Vi söker en **Visual Merchandiser**/Butikssäljare som vill vara m... |  | x |  | 19 | [Ansvarstagande - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/GeDs_WtB_Zwe) |
| 5 | ...mn Drottninggatan Vi söker en **Visual Merchandiser**/Butikssäljare som vill vara m... |  | x |  | 19 | [Resultatorienterad - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/PuYR_VvL_4AQ) |
| 5 | ...mn Drottninggatan Vi söker en **Visual Merchandiser**/Butikssäljare som vill vara m... |  | x |  | 19 | [Kommunikationsförmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/aiab_pmE_an8) |
| 5 | ...mn Drottninggatan Vi söker en **Visual Merchandiser**/Butikssäljare som vill vara m... |  | x |  | 19 | [Digital kompetens - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/hkyz_wHi_Eut) |
| 5 | ...mn Drottninggatan Vi söker en **Visual Merchandiser**/Butikssäljare som vill vara m... |  | x |  | 19 | [Samarbetsförmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/nzAf_9WF_y8g) |
| 6 | ... söker en Visual Merchandiser/**Butikssäljare** som vill vara med och vidareu... | x |  |  | 13 | [Butikssäljare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/JVsN_YXp_9mq) |
| 7 | ...ket och helhetsupplevelsen av **butiken** är avgörande för om kunden vä... | x |  |  | 7 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 8 | ...ljer att handla på Cubus. Som **Visual Merchandiser** ska du se till att presentera... |  | x |  | 19 | [Analytisk förmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/2HCr_Mkj_UNr) |
| 8 | ...ljer att handla på Cubus. Som **Visual Merchandiser** ska du se till att presentera... |  | x |  | 19 | [Kreativ förmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/5bhk_AGv_HNA) |
| 8 | ...ljer att handla på Cubus. Som **Visual Merchandiser** ska du se till att presentera... | x | x | 19 | 19 | [Butikskommunikatör/Visual merchandiser, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GTFy_iPM_msj) |
| 8 | ...ljer att handla på Cubus. Som **Visual Merchandiser** ska du se till att presentera... |  | x |  | 19 | [Ansvarstagande - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/GeDs_WtB_Zwe) |
| 8 | ...ljer att handla på Cubus. Som **Visual Merchandiser** ska du se till att presentera... |  | x |  | 19 | [Resultatorienterad - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/PuYR_VvL_4AQ) |
| 8 | ...ljer att handla på Cubus. Som **Visual Merchandiser** ska du se till att presentera... |  | x |  | 19 | [Kommunikationsförmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/aiab_pmE_an8) |
| 8 | ...ljer att handla på Cubus. Som **Visual Merchandiser** ska du se till att presentera... |  | x |  | 19 | [Digital kompetens - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/hkyz_wHi_Eut) |
| 8 | ...ljer att handla på Cubus. Som **Visual Merchandiser** ska du se till att presentera... |  | x |  | 19 | [Samarbetsförmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/nzAf_9WF_y8g) |
| 9 | ...att presentera varorna så att **butiken** är kommersiell, inspirerande ... | x |  |  | 7 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 10 | ...nspirerande och säljande. Som **Butikssäljare** hos oss är din uppgift att ta... | x |  |  | 13 | [Butikssäljare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/JVsN_YXp_9mq) |
| 11 | ...om att hjälpa och se till att **butiken** är attraktiv och spännande. T... | x |  |  | 7 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 12 | ...ppgifter:   - I samarbete med **butikschef**, stötta och bidra med visuell... | x | x | 10 | 10 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 13 | ... golvstruktur, varubärare och **ljussättning**  - Använda externa och intern... |  | x |  | 12 | [Ljussättning, scenproduktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/3gX3_7Ha_7np) |
| 14 | ...a praxis  - Ge bästa tänkbara **kundservice** och se till att våra kunder f... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 15 | ...djor med 300 butiker i Norge, **Sverige** och Finland. Vi betonar garde... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 16 | ...h rimliga priser, vi erbjuder **kläder** för spädbarn, barn, kvinnor o... | x | x | 6 | 6 | [Kläder/Mode, **skill**](http://data.jobtechdev.se/taxonomy/concept/agGK_Zte_X2y) |
| 17 | ...1200 fysiska butiker i Norge, **Sverige**, Finland, Island och Danmark. |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 18 | ...er i Norge, Sverige, Finland, **Island** och Danmark. |  | x |  | 6 | [Island, **country**](http://data.jobtechdev.se/taxonomy/concept/ZDbX_yJc_zin) |
| | **Overall** | | | **93** | **588** | 93/588 = **16%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Analytisk förmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/2HCr_Mkj_UNr) |
|  | x |  | [Ljussättning, scenproduktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/3gX3_7Ha_7np) |
|  | x |  | [Kreativ förmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/5bhk_AGv_HNA) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Butikskommunikatör/Visual merchandiser, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GTFy_iPM_msj) |
|  | x |  | [Ansvarstagande - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/GeDs_WtB_Zwe) |
| x | x | x | [Karlshamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/HtGW_WgR_dpE) |
| x |  |  | [Butikssäljare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/JVsN_YXp_9mq) |
|  | x |  | [Resultatorienterad - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/PuYR_VvL_4AQ) |
| x | x | x | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
|  | x |  | [Island, **country**](http://data.jobtechdev.se/taxonomy/concept/ZDbX_yJc_zin) |
| x | x | x | [Kläder/Mode, **skill**](http://data.jobtechdev.se/taxonomy/concept/agGK_Zte_X2y) |
|  | x |  | [Kommunikationsförmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/aiab_pmE_an8) |
| x | x | x | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
|  | x |  | [Digital kompetens - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/hkyz_wHi_Eut) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Samarbetsförmåga - Visual Merchandiser, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/nzAf_9WF_y8g) |
| x |  |  | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| | | **5** | 5/18 = **28%** |