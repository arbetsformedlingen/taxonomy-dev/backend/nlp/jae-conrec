# Results for '2332793eb21259dd3b0f13eced6a326bddde5ba5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2332793eb21259dd3b0f13eced6a326bddde5ba5](README.md) | 1 | 1532 | 7 | 10 | 40/148 = **27%** | 3/11 = **27%** |

## Source text

Mötesbokare på telefon Är du Entreprenör? Vi erbjuder stora möjligheter för dig som vill driva eget och tjäna riktigt bra med pengar från start. Vi har ett färdigt affärskoncept och du är igång på 1-2 dagar. Vi söker flera självständiga personer som låter seriösa och professionella på telefon och är hungrig på att tjäna stora pengar. Att du inte tar ett nej för ett nej och går den extra milen, det är essentiellt för att lyckas hos oss. Högt tempo och entusiasm är andra viktiga stickord för att uppnå succé! Vi hjälper dig komma i gång med din egen verksamhet. Vi lär upp dig i vår beprövade metodik och coachar dig regelbundet så du får ut din potential. Det finns även möjligheter att klättra i bolaget och bli t. ex teamledare. Konkret så bokar du möten till beslutsfattare i svenska bolag per telefon. Det kan vara allt från VDar, marknadschefer till produktspecialister, så viktigt att du låter professionell och seriös på telefon. Du jobbar hos oss i centrala Stockholm, vi sitter i Hammarby Sjöstad. Är du självständig med självdisciplin så går det även kombinera med att jobba hemifrån. Vi erbjuder mycket höga provisioner per genomförda möten. Du väljer själva om du vill fakturera ditt konsultarvode via egen firma/bolag eller via faktureringsbolag. Startdatum är från augusti 2022 och arbetstiden är måndag till fredag 0800-1700. Vi är ett bolag i Stockholm specialiserad på mötesbokning med mycket goda kundreferenser. Se vår hemsida: www.aceconsultinggroup.se Kontakta oss för mer info på: ove@aceconsultinggroup.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Mötesbokare** på telefon Är du Entreprenör?... |  | x |  | 11 | [Mötesbokare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yRCN_doP_hfd) |
| 2 | Mötesbokare på **telefon** Är du Entreprenör? Vi erbjude... | x |  |  | 7 | [Telefonister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/4aPu_2nd_8X6) |
| 3 | ... på 1-2 dagar. Vi söker flera **självständiga** personer som låter seriösa oc... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 4 | ...ättra i bolaget och bli t. ex **teamledare**. Konkret så bokar du möten ti... | x | x | 10 | 10 | [Teamledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FC86_VDF_rqB) |
| 5 | ...onkret så bokar du möten till **beslutsfattare** i svenska bolag per telefon. ... |  | x |  | 14 | [påverka beslutsfattare i frågor som rör socialtjänsten, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BjNm_4fR_2t7) |
| 5 | ...onkret så bokar du möten till **beslutsfattare** i svenska bolag per telefon. ... |  | x |  | 14 | [ge kostrekommendationer till offentliga beslutsfattare, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WVtA_3ZB_e3T) |
| 5 | ...onkret så bokar du möten till **beslutsfattare** i svenska bolag per telefon. ... |  | x |  | 14 | [ge råd till beslutsfattare inom hälso- och sjukvården, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tieT_MzT_Vp8) |
| 5 | ...onkret så bokar du möten till **beslutsfattare** i svenska bolag per telefon. ... |  | x |  | 14 | [informera beslutsfattare om hälsorelaterade utmaningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uMFP_cGd_FtZ) |
| 6 | ...u möten till beslutsfattare i **svenska** bolag per telefon. Det kan va... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 7 | ... Du jobbar hos oss i centrala **Stockholm**, vi sitter i Hammarby Sjöstad... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 8 | ...ter i Hammarby Sjöstad. Är du **självständig** med självdisciplin så går det... | x | x | 12 | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 9 | ...år det även kombinera med att **jobba hemifrån**. Vi erbjuder mycket höga prov... | x |  |  | 14 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 10 | ... 0800-1700. Vi är ett bolag i **Stockholm** specialiserad på mötesbokning... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| | **Overall** | | | **40** | **148** | 40/148 = **27%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Telefonister, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/4aPu_2nd_8X6) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [påverka beslutsfattare i frågor som rör socialtjänsten, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BjNm_4fR_2t7) |
| x | x | x | [Teamledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FC86_VDF_rqB) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [ge kostrekommendationer till offentliga beslutsfattare, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WVtA_3ZB_e3T) |
|  | x |  | [ge råd till beslutsfattare inom hälso- och sjukvården, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tieT_MzT_Vp8) |
|  | x |  | [informera beslutsfattare om hälsorelaterade utmaningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uMFP_cGd_FtZ) |
| x |  |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
|  | x |  | [Mötesbokare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/yRCN_doP_hfd) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/11 = **27%** |