# Results for '52890ecd1ebc4383c6091095fb204ff6b8dfbaa5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [52890ecd1ebc4383c6091095fb204ff6b8dfbaa5](README.md) | 1 | 2332 | 17 | 19 | 86/398 = **22%** | 6/24 = **25%** |

## Source text

Intresseanmälan: Vikarier till vård och omsorg Rättvik – där tradition möter framtid! Här finns de mest attraktiva boendemiljöerna med hög livskvalitet, trygghet och service för våra invånare och besökare; mycket tack vare vårt gedigna hållbarhetsarbete.    Beskrivning Rättviks kommun söker vikarier till våra verksamheter inom vård och omsorg: LSS (människor med funktionsnedsättning), hemtjänst och särskilt boende.  I ditt uppdrag ingår att ge god omvårdnad och social omsorg med utgångspunkt i brukarens behov.  Vi erbjuder;   - En individuell inskolning   - Fantastiska möten med människor    - Goda erfarenheter för framtiden   Arbetsuppgifter I ditt uppdrag ingår att ge god omvårdnad och social omsorg med utgångspunkt i brukarens behov. Vi strävar efter att få våra brukare allt mera delaktiga i vardagen både när det gäller aktiviteter och digital delaktighet. Tillsammans med övrig personal är du en viktig del för verksamhetens utveckling. Du kan få utföra delegerade arbetsuppgifter som gör att du utvecklas i din roll som undersköterska/vårdbiträde.     Kvalifikationer Vi söker dig som är 18 år fyllda och har ett stort intresse att arbeta med människor med fokus på dennes behov. Du behöver ha ett gott bemötande och fallenhet för att samarbeta med andra. Du ska även kunna arbeta självständigt där det behövs. Eftersom vi har dokumentationsskyldighet så behöver du behärska svenska språket väl i tal och skrift.   Det är meriterande om du har erfarenhet inom dessa verksamheter och/eller omvårdnadsutbildning.    För att arbeta inom hemtjänsten är körkort B ett krav.  I din ansökan vill vi att du bifogar kontaktuppgifter till 2 referenter.    Urval och rekrytering sker löpande.  Övrig information:  Arbetsgivaren begär utdrag ur polisensbelastningsregister vid nyanställning.  https://polisen.se/tjanster-tillstand/belastningsregistret/kontrollera-dina-uppgifter-i-belastningsregistret/  Om arbetsgivaren:  I Rättviks kommun skapar vi förutsättningar för utvecklande arbetsplatser med trygga ledare och medarbetare. Vi värnar om varandras arbetsmiljö och vill att alla medarbetare ska trivas och utvecklas hos oss. För oss är det viktigt att alla går till jobbet med en positiv känsla och känner arbetsglädje. Vi skapar livskvalitet och gör skillnad för människor varje dag, vi har Sveriges Viktigaste Jobb!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ntresseanmälan: Vikarier till **vård och omsorg** Rättvik – där tradition möter... | x |  |  | 15 | [Vård och omsorg; sociala tjänster, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/eJVQ_Uye_8UP) |
| 2 | ...Vikarier till vård och omsorg **Rättvik** – där tradition möter framtid... | x | x | 7 | 7 | [Rättvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Jy3D_2ux_dg8) |
| 3 | ...r till våra verksamheter inom **vård och omsorg**: LSS (människor med funktions... | x |  |  | 15 | [Vård och omsorg; sociala tjänster, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/eJVQ_Uye_8UP) |
| 4 | ...or med funktionsnedsättning), **hemtjänst** och särskilt boende.  I dit... | x |  |  | 9 | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
| 5 | ...nsnedsättning), hemtjänst och **särskilt boende**.  I ditt uppdrag ingår att ... | x | x | 15 | 15 | [Sjukhem/Särskilt boende, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZpXZ_PG5_B9s) |
| 6 | ...ditt uppdrag ingår att ge god **omvårdnad** och social omsorg med utgångs... |  | x |  | 9 | [Omvårdnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/MR4H_FFT_mTj) |
| 6 | ...ditt uppdrag ingår att ge god **omvårdnad** och social omsorg med utgångs... | x |  |  | 9 | [Omvårdnad, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/fQMU_GjY_AkC) |
| 7 | ...ngår att ge god omvårdnad och **social omsorg** med utgångspunkt i brukarens ... | x | x | 13 | 13 | [Social omsorg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sw4N_2Vt_PVD) |
| 8 | ...ditt uppdrag ingår att ge god **omvårdnad** och social omsorg med utgångs... |  | x |  | 9 | [Omvårdnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/MR4H_FFT_mTj) |
| 8 | ...ditt uppdrag ingår att ge god **omvårdnad** och social omsorg med utgångs... | x |  |  | 9 | [Omvårdnad, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/fQMU_GjY_AkC) |
| 9 | ...ngår att ge god omvårdnad och **social omsorg** med utgångspunkt i brukarens ... | x | x | 13 | 13 | [Social omsorg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sw4N_2Vt_PVD) |
| 10 | ...tighet. Tillsammans med övrig **personal** är du en viktig del för verks... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 11 | ...t du utvecklas i din roll som **undersköterska**/vårdbiträde.     Kvalifikatio... |  | x |  | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 11 | ...t du utvecklas i din roll som **undersköterska**/vårdbiträde.     Kvalifikatio... | x |  |  | 14 | [Undersköterska, hemtjänst, äldreboende och habilitering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3QA_5pk_uXd) |
| 12 | ...i din roll som undersköterska/**vårdbiträde**.     Kvalifikationer Vi söker... |  | x |  | 11 | [Vårdbiträden, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/tAJS_JNb_hDH) |
| 12 | ...i din roll som undersköterska/**vårdbiträde**.     Kvalifikationer Vi söker... | x |  |  | 11 | [vårdbiträde, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xX9z_5A5_THm) |
| 13 | ... med andra. Du ska även kunna **arbeta självständigt** där det behövs. Eftersom vi h... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 14 | ...dighet så behöver du behärska **svenska** språket väl i tal och skrift.... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 15 | ...dighet så behöver du behärska **svenska språket** väl i tal och skrift.   Det ä... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 16 | ... dessa verksamheter och/eller **omvårdnadsutbildning**.    För att arbeta inom hemtj... |  | x |  | 20 | [Annan omvårdnadsutbildning, psykiatrisk vård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/DUV9_brd_omr) |
| 16 | ... dessa verksamheter och/eller **omvårdnadsutbildning**.    För att arbeta inom hemtj... |  | x |  | 20 | [Annan omvårdnadsutbildning, äldreomsorg, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/DoLa_iGA_bhU) |
| 16 | ... dessa verksamheter och/eller **omvårdnadsutbildning**.    För att arbeta inom hemtj... |  | x |  | 20 | [Annan omvårdnadsutbildning, övriga inriktningar, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/LJxt_RHL_4yC) |
| 16 | ... dessa verksamheter och/eller **omvårdnadsutbildning**.    För att arbeta inom hemtj... |  | x |  | 20 | [Annan omvårdnadsutbildning, funktionsnedsatta, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/Y8WH_Nam_wZN) |
| 16 | ... dessa verksamheter och/eller **omvårdnadsutbildning**.    För att arbeta inom hemtj... |  | x |  | 20 | [Annan omvårdnadsutbildning, sjukvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/YqYV_41r_yYu) |
| 16 | ... dessa verksamheter och/eller **omvårdnadsutbildning**.    För att arbeta inom hemtj... |  | x |  | 20 | [Annan omvårdnadsutbildning, allmän vård och omsorg, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/hLkE_Bx6_riR) |
| 16 | ... dessa verksamheter och/eller **omvårdnadsutbildning**.    För att arbeta inom hemtj... |  | x |  | 20 | [Annan omvårdnadsutbildning, psykiskt funktionsnedsatta, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/ohi7_T1Y_65Z) |
| 17 | ...tt arbeta inom hemtjänsten är **körkort** B ett krav.  I din ansökan vi... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 18 | ...ta inom hemtjänsten är körkort** B** ett krav.  I din ansökan vill... | x |  |  | 2 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 19 | ...istret/  Om arbetsgivaren:  I **Rättviks kommun** skapar vi förutsättningar för... | x |  |  | 15 | [Rättvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Jy3D_2ux_dg8) |
| 20 | ...etare. Vi värnar om varandras **arbetsmiljö** och vill att alla medarbetare... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | **Overall** | | | **86** | **398** | 86/398 = **22%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
|  | x |  | [Annan omvårdnadsutbildning, psykiatrisk vård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/DUV9_brd_omr) |
|  | x |  | [Annan omvårdnadsutbildning, äldreomsorg, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/DoLa_iGA_bhU) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x | x | x | [Rättvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Jy3D_2ux_dg8) |
|  | x |  | [Annan omvårdnadsutbildning, övriga inriktningar, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/LJxt_RHL_4yC) |
|  | x |  | [Omvårdnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/MR4H_FFT_mTj) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Social omsorg, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sw4N_2Vt_PVD) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Undersköterska, hemtjänst, äldreboende och habilitering, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3QA_5pk_uXd) |
|  | x |  | [Annan omvårdnadsutbildning, funktionsnedsatta, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/Y8WH_Nam_wZN) |
|  | x |  | [Annan omvårdnadsutbildning, sjukvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/YqYV_41r_yYu) |
| x | x | x | [Sjukhem/Särskilt boende, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZpXZ_PG5_B9s) |
| x |  |  | [Vård och omsorg; sociala tjänster, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/eJVQ_Uye_8UP) |
| x |  |  | [Omvårdnad, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/fQMU_GjY_AkC) |
|  | x |  | [Annan omvårdnadsutbildning, allmän vård och omsorg, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/hLkE_Bx6_riR) |
|  | x |  | [Annan omvårdnadsutbildning, psykiskt funktionsnedsatta, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/ohi7_T1Y_65Z) |
|  | x |  | [Vårdbiträden, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/tAJS_JNb_hDH) |
| x |  |  | [vårdbiträde, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xX9z_5A5_THm) |
| x |  |  | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **6** | 6/24 = **25%** |