# Results for 'c8426830cea9b04b6292ef62c63192509c8cf6c7'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c8426830cea9b04b6292ef62c63192509c8cf6c7](README.md) | 1 | 3695 | 18 | 20 | 151/343 = **44%** | 9/18 = **50%** |

## Source text

Sjuksköterska, Thoraxvårdavdelning, Umeå Region Västerbotten arbetar för att god hälsa och hållbar utveckling ska stärka varandra. Vi tar ansvar för en jämlik välfärd och för att forskning och innovation ger resultat.  Thoraxvårdavdelning är en av avdelningarna inom Hjärtcentrum och här arbetar 50 medarbetare i rollerna sjuksköterskor, vårdnäraservicemedarbetare och undersköterskor tillsammans kring patienterna. Thoraxvårdavdelning har 18 vårdplatser. På Thorax opereras drygt 1000 patienter/år.  På thoraxvårdavdelning bedrivs specialiserad vård av patienter som opereras för hjärt-, kärl-, och lungsjukdomar. Thorax har regionansvar och hit kommer patienter från alla de nordligaste länen. Patienterna vårdas här både före och efter operationen.  På thoraxvårdavdelning välkomnas du in i en avdelning med väl utvecklade rutiner och som är intresserad av nytänkande. Vi erbjuder ett varierande och utvecklade arbete där din kompetens nyttjas för verksamheten och patientens bästa. Om du väljer att arbeta hos oss möts du av flera olika vägar för din personliga utveckling i yrket  ARBETSUPPGIFTER Som sjuksköterska hos oss bidrar du med kunskap och engagemang som får människor att känna sig trygga.   På thoraxvårdavdelning bedrivs kirurgisk vård av patienter som opereras för hjärt-, kärl-, och lungsjukdomar. Thorax har regionansvar och hit kommer patienter från alla de nordligaste länen. Patienterna vårdas här både före och efter operationen.   Arbetet består i omvårdnadsarbete kring patienterna, dokumentation, läkemedelsdelning och övervakning av vitala parametrar m.m. Tillsammans med annan omvårdnadspersonal arbetar du på ett patientsäkert sätt och arbetet sker både självständigt och tillsammans med övrig personal.   I arbetet ingår även att bidra i utvecklingsarbete och handledning av studenter.   Det är ett roligt, omväxlande och utmanande arbete där du har möjlighet att lära nya saker varje dag i en härlig arbetsgrupp. Vi hjälper alltid varandra och vi har ett gott samarbete med övriga yrkesgrupper. Fokus är alltid på patienten.   På thoraxvårdavdelning månar vi om personlig utveckling och har därför utbildning i form av temadagar, internutbildning samt efter en tids arbete möjlighet till rotation mot intermediär avdelning samt även möjlighet att söka aspirantprogram för intensivvårdssköterska.   KVALIFIKATIONER Vi söker dig som är legitimerad sjuksköterska.  Vi värdesätter tidigare kunskaper inom hjärtsjukvård eller kirurgivård, men det är inget krav.   Det vi söker hos dig är att du har mycket god samarbetsförmåga. Vi förväntar oss att du är initiativrik samtidigt som du är strukturerad och kan prioritera dina arbetsuppgifter. Det är viktigt att du har god kommunikativ förmåga gentemot patienter och kollegor. Hos oss bidrar du med din kompetens för att tillsammans med oss hitta bra lösningar på vår avdelning.   ÖVRIGT Varmt välkommen in med din ansökan, intervjuer kommer att ske löpande.   Läs mer om våra förmåner för dig som jobbar med oss på webbsidan https://www.regionvasterbotten.se/nar-du-jobbar-hos-oss/att-vara-medarbetare  Vi strävar efter en jämn könsfördelning och ser mångfald som en styrka och välkomnar därför sökande med olika bakgrund.  Inför rekryteringsarbetet har vi tagit ställning till rekryteringskanaler och marknadsföring. Vi undanbeder oss därför bestämt kontakt med mediesäljare, rekryteringssajter och liknande.  Vi använder e-rekrytering för att effektivisera och kvalitetssäkra rekryteringsarbetet. Alla dina uppgifter sparas på ditt CV-konto och du kan enkelt logga in och uppdatera dina uppgifter när du vill. Fyll därför i dina uppgifter så omsorgsfullt som möjligt för att ge en rättvisande bild av dig själv.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sjuksköterska**, Thoraxvårdavdelning, Umeå Re... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 2 | ...öterska, Thoraxvårdavdelning, **Umeå** Region Västerbotten arbetar f... | x | x | 4 | 4 | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| 3 | ...ka, Thoraxvårdavdelning, Umeå **Region **Västerbotten arbetar för att g... | x |  |  | 7 | [Västerbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/g5Tt_CAV_zBd) |
| 4 | ...raxvårdavdelning, Umeå Region **Västerbotten** arbetar för att god hälsa och... | x | x | 12 | 12 | [Västerbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/g5Tt_CAV_zBd) |
| 5 | ...terbotten arbetar för att god **hälsa** och hållbar utveckling ska st... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 6 | ...arbetar för att god hälsa och **hållbar utveckling** ska stärka varandra. Vi tar a... | x |  |  | 18 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 7 | ...tar 50 medarbetare i rollerna **sjuksköterskor**, vårdnäraservicemedarbetare o... |  | x |  | 14 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 7 | ...tar 50 medarbetare i rollerna **sjuksköterskor**, vårdnäraservicemedarbetare o... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 8 | ...årdnäraservicemedarbetare och **undersköterskor** tillsammans kring patienterna... | x | x | 15 | 15 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 9 | ...av patienter som opereras för **hjärt-, kärl-**, och lungsjukdomar. Thorax ha... | x |  |  | 13 | [Hjärt-kärlsjukdomar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Kq4d_VLP_FMg) |
| 10 | ...nter som opereras för hjärt-, **kärl-, och lu**ngsjukdomar. Thorax har region... |  | x |  | 13 | [diagnostisera kärlsjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uNec_9uG_yz5) |
| 11 | ...ereras för hjärt-, kärl-, och **lungsjukdomar**. Thorax har regionansvar och ... |  | x |  | 13 | [Lungsjukdomar, **skill**](http://data.jobtechdev.se/taxonomy/concept/6UZC_Dda_ohv) |
| 11 | ...ereras för hjärt-, kärl-, och **lungsjukdomar**. Thorax har regionansvar och ... | x | x | 13 | 13 | [lungsjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/H45w_pbK_PwV) |
| 11 | ...ereras för hjärt-, kärl-, och **lungsjukdomar**. Thorax har regionansvar och ... |  | x |  | 13 | [Läkarutbildning, lungsjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/np5b_UZM_G6T) |
| 12 | ... yrket  ARBETSUPPGIFTER Som **sjuksköterska** hos oss bidrar du med kunskap... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 13 | ...å thoraxvårdavdelning bedrivs **kirurgisk vård** av patienter som opereras för... | x |  |  | 14 | [Kirurgisk vård, **skill**](http://data.jobtechdev.se/taxonomy/concept/4tg9_s2B_Kyw) |
| 14 | ...av patienter som opereras för **hjärt-, kärl-**, och lungsjukdomar. Thorax ha... | x |  |  | 13 | [Hjärt-kärlsjukdomar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Kq4d_VLP_FMg) |
| 15 | ...nter som opereras för hjärt-, **kärl-, och lu**ngsjukdomar. Thorax har region... |  | x |  | 13 | [diagnostisera kärlsjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uNec_9uG_yz5) |
| 16 | ...ereras för hjärt-, kärl-, och **lungsjukdomar**. Thorax har regionansvar och ... |  | x |  | 13 | [Lungsjukdomar, **skill**](http://data.jobtechdev.se/taxonomy/concept/6UZC_Dda_ohv) |
| 16 | ...ereras för hjärt-, kärl-, och **lungsjukdomar**. Thorax har regionansvar och ... | x | x | 13 | 13 | [lungsjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/H45w_pbK_PwV) |
| 16 | ...ereras för hjärt-, kärl-, och **lungsjukdomar**. Thorax har regionansvar och ... |  | x |  | 13 | [Läkarutbildning, lungsjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/np5b_UZM_G6T) |
| 17 | ...nadsarbete kring patienterna, **dokumentation**, läkemedelsdelning och överva... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 18 | ...igt och tillsammans med övrig **personal**.   I arbetet ingår även att b... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 19 | ...rbetet ingår även att bidra i **utvecklingsarbete** och handledning av studenter.... | x | x | 17 | 17 | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| 20 | ...oraxvårdavdelning månar vi om **personlig utveckling** och har därför utbildning i f... | x | x | 20 | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 21 | ... söker dig som är legitimerad **sjuksköterska**.  Vi värdesätter tidigare kun... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 22 | ...ätter tidigare kunskaper inom **hjärtsjukvård** eller kirurgivård, men det är... |  | x |  | 13 | [Specialistsjuksköterskeutbildning, hjärtsjukvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dTMp_6dP_Xu3) |
| | **Overall** | | | **151** | **343** | 151/343 = **44%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [Kirurgisk vård, **skill**](http://data.jobtechdev.se/taxonomy/concept/4tg9_s2B_Kyw) |
|  | x |  | [Lungsjukdomar, **skill**](http://data.jobtechdev.se/taxonomy/concept/6UZC_Dda_ohv) |
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x | x | x | [lungsjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/H45w_pbK_PwV) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x |  |  | [Hjärt-kärlsjukdomar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Kq4d_VLP_FMg) |
| x | x | x | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| x | x | x | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| x |  |  | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| x | x | x | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
|  | x |  | [Specialistsjuksköterskeutbildning, hjärtsjukvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dTMp_6dP_Xu3) |
| x | x | x | [Västerbottens län, **region**](http://data.jobtechdev.se/taxonomy/concept/g5Tt_CAV_zBd) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x |  |  | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
|  | x |  | [Läkarutbildning, lungsjukdomar, **keyword**](http://data.jobtechdev.se/taxonomy/concept/np5b_UZM_G6T) |
|  | x |  | [diagnostisera kärlsjukdomar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uNec_9uG_yz5) |
| x | x | x | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| | | **9** | 9/18 = **50%** |