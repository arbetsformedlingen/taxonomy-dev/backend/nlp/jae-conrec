# Results for '6df6ada741a00e8081090eb37f0ed34f0f135375'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [6df6ada741a00e8081090eb37f0ed34f0f135375](README.md) | 1 | 719 | 5 | 5 | 56/56 = **100%** | 4/4 = **100%** |

## Source text

Sjuksköterska, bemanning Jämför och chatta med bemanningsbolagen enkelt via Vårdföretag App (vardforetag.se) Skapa din profil på mindre än 1 minut och lägg upp dina önskemål. Få erbjudanden på uppdrag som kan passa dina önskemål och chatta med flera av de ledande bemanningsbolagen inom vården. Enkelt och kostnadsfritt! Just nu finns ett ledigt uppdrag som sjuksköterska på lungavdelning i Gävle, Gävleborgs län. Uppdraget finns att söka bland flera bemanningsbolag i Vårdföretag.se   Fördelar: -Hög ersättning -Inga onödiga Mail Eller telefonsamtal -Enkelt och kostnadsfritt -Inget bindande   Vill du starta eget aktiebolag? Vi på vardforetag.se hjälper dig att starta aktiebolag och redovisning till fasta kostnader.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sjuksköterska**, bemanning Jämför och chatta ... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 2 | ... finns ett ledigt uppdrag som **sjuksköterska** på lungavdelning i Gävle, Gäv... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 3 | ...ksköterska på lungavdelning i **Gävle**, Gävleborgs län. Uppdraget fi... | x | x | 5 | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 4 | ...ska på lungavdelning i Gävle, **Gävleborgs län**. Uppdraget finns att söka bla... | x | x | 14 | 14 | [Gävleborgs län, **region**](http://data.jobtechdev.se/taxonomy/concept/zupA_8Nt_xcD) |
| 5 | ...dig att starta aktiebolag och **redovisning** till fasta kostnader. | x | x | 11 | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| | **Overall** | | | **56** | **56** | 56/56 = **100%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| x | x | x | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| x | x | x | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| x | x | x | [Gävleborgs län, **region**](http://data.jobtechdev.se/taxonomy/concept/zupA_8Nt_xcD) |
| | | **4** | 4/4 = **100%** |