# Results for 'e68210162f6b28344999f30670e5c25e3f279c6f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e68210162f6b28344999f30670e5c25e3f279c6f](README.md) | 1 | 2489 | 20 | 19 | 172/279 = **62%** | 13/20 = **65%** |

## Source text

Restaurangbiträde till ISS Mjällby Var 660:e person som går till arbetet i Sverige är en ISS-medarbetare och det är vi stolta över. Vi är ett av Sveriges och Nordens största tjänsteföretag samt har över 450 000 medarbetare världen över. Det är våra medarbetare som är kärnan i vår verksamhet och förutsättningen för att kunna leva upp till vårt syfte, Connecting people and places to make the world work better. På ISS Facility Services skapar vi platser och miljöer som bidrar till bättre prestationer och en enklare, mer effektiv och trivsam tillvaro - levererade med omtanke och hög kvalitet av engagerade medarbetare. Vill du också bli en del i vår globala organisation där ditt jobb verkligen gör skillnad?  ISS Mat & Dryck serverar över 42 000 måltider varje dag - med traditionell husmanskost eller nyskapande crossover. Till förskolebarn, elever, sjukhusens patienter, omsorgens äldreboende, personalmåltider. Behoven är olika, smakerna är olika, miljöerna är olika. Men alla förenas i kraven på kvalitet.  Nu söker vi Restaurangbiträde till Titan X personalrestaurang  Arbetsuppgifter Till våra uppdrag söker vi nu dig som vill jobba som restaurang- och köksbiträde. Vilket innebär att under ledning av kökschefen producera, underhålla och sälja våra produkter i den dagliga driften i restaurangen.   I ditt ansvar ingår förutom sedvanliga restaurangbiträdesuppgifter även portionering av mat, diska, stå i kassan samt att vara en tillgänglig och flexibel person i vår organisation.etc. Du arbetar enligt gällande instruktioner med hög kvalitet och god egenkontroll. Även lättare matlagning kan ingå beroende på kvalifikationer.  Kvalifikationer Vi söker dig som har ett genuint intresse för mat. Du har erfarenhet och arbetat inom restaurangbranschen sedan tidigare Erfarenhet från olika typer av restaurangverksamhet är meriterande.  Du har IT-kunskaper samt besitter en god kommunikativ förmåga i svenska i tal och skrift. Du är målinriktad, självgående och ansvarstagande. God samarbetsförmåga och ett intresse för människor är en förutsättning för att lyckas i arbetet.  B Körkort är ett krav.  Övrigt Tjänsten är en tillsvidareanställning på 100 med inledande 6 månader provanställning. Arbetet är förlagt till 8 timmar per dag.   Tillträde snarast 2022. Vi arbetar med löpande urval varför tjänsten kan tillsättas före sista ansökningsdagen.  Vid frågor om tjänsten kontaktar du Staffan Johansson mailadressen staffan.johansson@se.issworld.com  Välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Restaurangbiträde** till ISS Mjällby Var 660:e pe... | x | x | 17 | 17 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 2 | ...person som går till arbetet i **Sverige** är en ISS-medarbetare och det... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 3 | ... vi stolta över. Vi är ett av **Sveriges** och Nordens största tjänstefö... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ... varje dag - med traditionell **husmanskost** eller nyskapande crossover. T... | x | x | 11 | 11 | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| 5 | ...ukhusens patienter, omsorgens **äldreboende**, personalmåltider. Behoven är... | x | x | 11 | 11 | [Äldreboende, **skill**](http://data.jobtechdev.se/taxonomy/concept/tykJ_BZQ_Wer) |
| 6 | ...ven på kvalitet.  Nu söker vi **Restaurangbiträde** till Titan X personalrestaura... | x | x | 17 | 17 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 7 | ...ker vi Restaurangbiträde till **Titan** X personalrestaurang  Arbetsu... |  | x |  | 5 | [Titan, materialkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/aDUJ_oEN_aDa) |
| 7 | ...ker vi Restaurangbiträde till **Titan** X personalrestaurang  Arbetsu... |  | x |  | 5 | [Titan, flygplansplåtslagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/rrEP_SZN_b3y) |
| 7 | ...ker vi Restaurangbiträde till **Titan** X personalrestaurang  Arbetsu... |  | x |  | 5 | [Titan, telefonväxel, **skill**](http://data.jobtechdev.se/taxonomy/concept/vnsR_9UF_m4M) |
| 7 | ...ker vi Restaurangbiträde till **Titan** X personalrestaurang  Arbetsu... |  | x |  | 5 | [Titan, specialisering tandteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/wRrP_jcM_Mz2) |
| 8 | ... vi nu dig som vill jobba som **restaurang**- och köksbiträde. Vilket inne... | x | x | 10 | 10 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 9 | ... som vill jobba som restaurang**- och k**öksbiträde. Vilket innebär att... |  | x |  | 7 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 10 | ...ill jobba som restaurang- och **köksbiträde**. Vilket innebär att under led... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 11 | ... innebär att under ledning av **kökschefen** producera, underhålla och säl... | x |  |  | 10 | [Kökschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/kntm_Uen_7Bn) |
| 12 | ...svar ingår förutom sedvanliga **restaurangbiträdesuppgifter** även portionering av mat, dis... | x |  |  | 27 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 13 | ...taurangbiträdesuppgifter även **portionering** av mat, diska, stå i kassan s... | x | x | 12 | 12 | [Portionering, **skill**](http://data.jobtechdev.se/taxonomy/concept/XCMK_vWW_njd) |
| 14 | ...ter även portionering av mat, **diska**, stå i kassan samt att vara e... | x | x | 5 | 5 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 15 | ...r erfarenhet och arbetat inom **restaurangbranschen** sedan tidigare Erfarenhet frå... | x | x | 19 | 19 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 16 | ...rfarenhet från olika typer av **restaurangverksamhet** är meriterande.  Du har IT-ku... | x | x | 20 | 20 | [Restaurangverksamhet, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/byvM_cVn_X7Q) |
| 17 | ...amhet är meriterande.  Du har **IT**-kunskaper samt besitter en go... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 18 | ...het är meriterande.  Du har IT**-kunskaper** samt besitter en god kommunik... |  | x |  | 10 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 19 | ...en god kommunikativ förmåga i **svenska** i tal och skrift. Du är målin... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 20 | ... målinriktad, självgående och **ansvarstagande**. God samarbetsförmåga och ett... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 21 | ...ng för att lyckas i arbetet.  **B Körkort** är ett krav.  Övrigt Tjänsten... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 22 | ... krav.  Övrigt Tjänsten är en **tillsvidareanställning** på 100 med inledande 6 månade... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 23 | ... en tillsvidareanställning på **100** med inledande 6 månader prova... | x |  |  | 3 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **172** | **279** | 172/279 = **62%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| x | x | x | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Portionering, **skill**](http://data.jobtechdev.se/taxonomy/concept/XCMK_vWW_njd) |
|  | x |  | [Titan, materialkunskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/aDUJ_oEN_aDa) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Restaurangverksamhet, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/byvM_cVn_X7Q) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Kökschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/kntm_Uen_7Bn) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
|  | x |  | [Titan, flygplansplåtslagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/rrEP_SZN_b3y) |
| x | x | x | [Äldreboende, **skill**](http://data.jobtechdev.se/taxonomy/concept/tykJ_BZQ_Wer) |
|  | x |  | [Titan, telefonväxel, **skill**](http://data.jobtechdev.se/taxonomy/concept/vnsR_9UF_m4M) |
|  | x |  | [Titan, specialisering tandteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/wRrP_jcM_Mz2) |
| x | x | x | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **13** | 13/20 = **65%** |