# Results for '4f034eda1474a494f5cc559aa609c3225c58ee1c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4f034eda1474a494f5cc559aa609c3225c58ee1c](README.md) | 1 | 1217 | 8 | 10 | 37/151 = **25%** | 5/13 = **38%** |

## Source text

Trädgårds Veteraner  Veterangig söker Trädgårdsveteraner till vårt populära affärsområde Träd & Trädgård.  Veterangig söker just nu ett flertal Arborister/Trädvårdare till vår uthyrningsverksamhet.  Vi levererar våra tjänster i Botkyrka, Salem, Huddinge, Haninge, Tyresö, samt Stockholmskommuns södra förorter som Älvsjö, Farsta, Hägersten, Skärholmen och Enskede. Vill du vara med? För att bli en i gänget behöver du vara 55år eller äldre och det gör absolut ingenting om du är pensionär.   Uppdraget: Vi jobba nästan alltid i team om 2 veteraner och tar oss an uppdrag inom det mesta när det gäller trädgårdsarbete. Från enklare vårstädningar till större planterings uppdrag. Beskärning av häckar och fruktträd. Vi klipper allt från små syrenbuskar till oändligt långa amerikanska hagtornshäckar.   Utbildningar:  Vi har och köper in kurser löpande.      Villkor:  Hos oss jobbar du helt på dina egna villkor. Du tar precis så stora eller små uppdrag du orkar med samt när du vill att det ska utföras. Möjlighet att lära nya angränsande trädgårdsområden finns då våra Trädgårdsmästare gärna har med sig en lärling/elev på sina uppdrag. Kollektivavtals enliga löner och arbetsvillkor ligger till grund hur vi jobbar.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...s Veteraner  Veterangig söker **Trädgård**sveteraner till vårt populära ... |  | x |  | 8 | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| 2 | ...s Veteraner  Veterangig söker **Trädgårdsveteraner** till vårt populära affärsområ... | x |  |  | 18 | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| 3 | ...gig söker just nu ett flertal **Arborister/**Trädvårdare till vår uthyrning... | x |  |  | 11 | [Arborist/Trädvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QBXT_ESH_eoP) |
| 4 | ...ust nu ett flertal Arborister/**Trädvårdare** till vår uthyrningsverksamhet... | x | x | 11 | 11 | [Arborist/Trädvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QBXT_ESH_eoP) |
| 4 | ...ust nu ett flertal Arborister/**Trädvårdare** till vår uthyrningsverksamhet... |  | x |  | 11 | [trädvårdare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/QH4z_TsL_kxT) |
| 5 | ... Vi levererar våra tjänster i **Botkyrka**, Salem, Huddinge, Haninge, Ty... | x |  |  | 8 | [Botkyrka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCVZ_JA7_d3y) |
| 6 | ...rar våra tjänster i Botkyrka, **Salem**, Huddinge, Haninge, Tyresö, s... | x | x | 5 | 5 | [Salem, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4KBw_CPU_VQv) |
| 7 | ...a tjänster i Botkyrka, Salem, **Huddinge**, Haninge, Tyresö, samt Stockh... | x | x | 8 | 8 | [Huddinge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g1Gc_aXK_EKu) |
| 8 | ... i Botkyrka, Salem, Huddinge, **Haninge**, Tyresö, samt Stockholmskommu... | x | x | 7 | 7 | [Haninge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Q7gp_9dT_k2F) |
| 9 | ...ka, Salem, Huddinge, Haninge, **Tyresö**, samt Stockholmskommuns södra... | x | x | 6 | 6 | [Tyresö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/sTPc_k2B_SqV) |
| 10 | ...ddinge, Haninge, Tyresö, samt **Stockholmskommuns** södra förorter som Älvsjö, Fa... | x |  |  | 17 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 11 | ...inom det mesta när det gäller **trädgårdsarbete**. Från enklare vårstädningar t... |  | x |  | 15 | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| 12 | ...l större planterings uppdrag. **Beskärning** av häckar och fruktträd. Vi k... |  | x |  | 10 | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
| 13 | ...rädgårdsområden finns då våra **Trädgårdsmästare** gärna har med sig en lärling/... |  | x |  | 16 | [Trädgårdsmästare, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JZur_dmf_EPY) |
| | **Overall** | | | **37** | **151** | 37/151 = **25%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Salem, **municipality**](http://data.jobtechdev.se/taxonomy/concept/4KBw_CPU_VQv) |
|  | x |  | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| x |  |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Botkyrka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/CCVZ_JA7_d3y) |
|  | x |  | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
|  | x |  | [Trädgårdsmästare, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JZur_dmf_EPY) |
| x | x | x | [Haninge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Q7gp_9dT_k2F) |
| x | x | x | [Arborist/Trädvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QBXT_ESH_eoP) |
|  | x |  | [trädvårdare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/QH4z_TsL_kxT) |
|  | x |  | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| x | x | x | [Huddinge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g1Gc_aXK_EKu) |
| x |  |  | [Trädgårdsarbetare/Plantskolearbetare, trädgård, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/geNh_Yah_F3J) |
| x | x | x | [Tyresö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/sTPc_k2B_SqV) |
| | | **5** | 5/13 = **38%** |