# Results for '89fbd5b29f4bdbe3603e9fff502254898a7f98d2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [89fbd5b29f4bdbe3603e9fff502254898a7f98d2](README.md) | 1 | 2523 | 20 | 17 | 175/254 = **69%** | 9/10 = **90%** |

## Source text

Flertalet maskinoperatörer till Diab i Laholm Just nu söker vi på Jobandtalent flertalet personer som vill ha ett jobb med goda utvecklingsmöjligheter till rollen som maskinoperatör/processoperatör hos vår kund Diab i Laholm. Vi söker framför allt dig som är lösningsfokuserad och har ett stort driv och mycket framåtanda. Välkommen med din ansökan till oss på Jobandtalent!    Om jobbet som maskinoperatör  Som processoperatör får du ett omväxlande arbete med systemövervakning, olika fysiska arbetsmoment, kvalitetstester, truckkörning och samt arbeta med att driva och utveckla förbättringsarbeten. I rollen som maskinoperatör ansvarar du för att kontrollera tillflöden av material i maskinerna samt ansvara för den dagliga driften på avdelningen. Du kommer att ingå i ett skiftlag där samarbetet är viktigt. Arbetstiden är skiftgång med 2-skift alt 5-skift.    Är du rätt för rollen som maskinoperatör?  Vi på Jobandtalent söker dig som har B-körkort och goda kunskaper i svenska i tal och skrift. Vidare har du erfarenhet av arbete som maskinoperatör och av industriproduktion.  För att lyckas i uppdraget är det viktigt att du har ett stort driv och att du fokuserar på att hitta lösningar. Det är också viktigt att du strävar efter att göra det lilla extra och att du har ett metodiskt tillvägagångssätt.  Vi tror att Du har några års erfarenhet av arbete inom process-/tillverkningsindustrin och har nyfikenheten och intresset av den samma, med goda referenser och hög arbetsmoral!  Vi tror att du som söker har:  Goda IT-kunskaper  Maskinkunskap  Vana att hantera processteknik är meriterande  Problemlösning, felsökning  Truckkort för ledstaplare och motvikt är meriterande  Det är ett krav att du behärskar svenska flytande i tal och skrift.  För anställning krävs uppvisande av utdrag ur belastningsregistret.    Intresserad?  För att söka tjänsten skickar du in CV och personligt brev via länken. Vid frågor är du välkommen att kontakta rekryteringsansvarig Sandra via e-post: sandra.lindulf@jobandtalent.com eller telefonnummer: 0733-898799.  Vi arbetar med löpande urval och kan komma att tillsätta uppdraget så fort vi hittat rätt person. Skicka därför in din ansökan så snart som möjligt!    Vi ser fram emot din ansökan!  Övrigt    Placeringsort: Laholm    Typ av anställning: allmän visstidsanställning  Tillsättning: så snart som möjligt    Lön: enligt kollektivavtal  Om Jobandtalent    Som konsult hos Jobandtalent arbetar du hos välkända företag i olika branscher, med fast lön och trygg anställning.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Flertalet **maskinoperatörer** till Diab i Laholm Just nu sö... | x | x | 16 | 16 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 2 | ... maskinoperatörer till Diab i **Laholm** Just nu söker vi på Jobandtal... | x | x | 6 | 6 | [Laholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/c1iL_rqh_Zja) |
| 3 | ...gsmöjligheter till rollen som **maskinoperatör**/processoperatör hos vår kund ... | x | x | 14 | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 4 | ...till rollen som maskinoperatör**/processoperatör** hos vår kund Diab i Laholm. V... | x |  |  | 16 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 5 | ...soperatör hos vår kund Diab i **Laholm**. Vi söker framför allt dig so... | x | x | 6 | 6 | [Laholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/c1iL_rqh_Zja) |
| 6 | ...obandtalent!    Om jobbet som **maskinoperatör**  Som processoperatör får du e... | x | x | 14 | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 7 | ...obbet som maskinoperatör  Som **processoperatör** får du ett omväxlande arbete ... | x |  |  | 15 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 8 | ...rbetsmoment, kvalitetstester, **truckkörning** och samt arbeta med att driva... | x |  |  | 12 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 9 | ...ättringsarbeten. I rollen som **maskinoperatör** ansvarar du för att kontrolle... | x | x | 14 | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 10 | ...    Är du rätt för rollen som **maskinoperatör**?  Vi på Jobandtalent söker di... | x | x | 14 | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 11 | ...obandtalent söker dig som har **B-körkort** och goda kunskaper i svenska ... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 12 | ...-körkort och goda kunskaper i **svenska** i tal och skrift. Vidare har ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 13 | ...r du erfarenhet av arbete som **maskinoperatör** och av industriproduktion.  F... | x | x | 14 | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 14 | ...r att du som söker har:  Goda **IT**-kunskaper  Maskinkunskap  Van... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 15 | ...att du som söker har:  Goda IT**-kunskaper**  Maskinkunskap  Vana att hant... |  | x |  | 10 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 16 | ...skinkunskap  Vana att hantera **processteknik** är meriterande  Problemlösnin... | x | x | 13 | 13 | [Processteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Jgmc_3hD_JEt) |
| 17 | ... meriterande  Problemlösning, **felsökning**  Truckkort för ledstaplare oc... | x | x | 10 | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 18 | ...  Problemlösning, felsökning  **Truckkort** för ledstaplare och motvikt ä... | x | x | 9 | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 19 | ... är ett krav att du behärskar **svenska** flytande i tal och skrift.  F... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 20 | ...an!  Övrigt    Placeringsort: **Laholm**    Typ av anställning: allmän... | x | x | 6 | 6 | [Laholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/c1iL_rqh_Zja) |
| 21 | ...Laholm    Typ av anställning: **allmän visstidsanställning**  Tillsättning: så snart som m... | x |  |  | 26 | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| 22 | ...rt som möjligt    Lön: enligt **kollektivavtal**  Om Jobandtalent    Som konsu... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **175** | **254** | 175/254 = **69%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| x | x | x | [Processteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Jgmc_3hD_JEt) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [Laholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/c1iL_rqh_Zja) |
| x |  |  | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| x | x | x | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **9** | 9/10 = **90%** |