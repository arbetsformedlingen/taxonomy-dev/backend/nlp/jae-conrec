# Results for '83b04a2f6d77e294976f0684e60a2a2dfc721c7d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [83b04a2f6d77e294976f0684e60a2a2dfc721c7d](README.md) | 1 | 1566 | 8 | 7 | 40/116 = **34%** | 2/6 = **33%** |

## Source text

Höstens roligaste extrajobb! Meet a Student är en jobbplattform för studenter och juniora talanger.  För kunds räkning söker vi nu: Vi söker dig som har ett genuint intresse för barn och gillar ett flexibelt och socialt extrajobb! Jobbet som barnvakt passar dig som vill jobba som på ett flexibelt schema med där man känner att man har tid för minst 15 timmar per vecka. Stort plus om du är tillgänglig på förmiddagar och eftermiddagar. Vi ser att man helst har en parallell sysselsättning som studier eller annat deltidsjobb. Vill du även jobba som timvikarie på förskola finns den möjligheten. Vi som arbetsgivare erbjuder dig:  Konkurrenskraftig lön, mellan 110-150 kronor per timme, Barnvaktscertifiering utformad av specialpedagoger med tillhörande hjärt och lungräddningsutbildning. Toppservice från kontoret som hjälper dig vid frågor eller funderingar Och såklart ett väldigt roligt extrajobb med stora möjligheter till personlig och yrkesmässig utveckling.  Det är viktigt att du har erfarenhet av arbete som barnvakt eller Nanny och alltid tycker det är roligt att vara med barn. Som person ser vi att du är ansvarsfull, social och kreativ för att komma på nya lekar med barnen. Viktigt att du känner dig trygg som barnvakt och kan ta stort ansvar. Då barnen vanligtvis är i åldrarna 0-5 år är det viktigt att du har erfarenhet av dessa åldrar sen tidigare. Om våra barnvaktstjänster: Du väljer själv vilken familj du vill jobba för utifrån var de bor, arbetstider och åldrar på barnen. Vi hjälper dig att hitta den perfekta familjen utifrån dina önskemål.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...socialt extrajobb! Jobbet som **barnvakt** passar dig som vill jobba som... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 2 | ... även jobba som timvikarie på **förskola** finns den möjligheten. Vi som... |  | x |  | 8 | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| 3 | ...lan 110-150 kronor per timme, **Barnvaktscertifiering** utformad av specialpedagoger ... | x |  |  | 21 | [Certifikat/licenser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/s48H_Sft_DTn) |
| 4 | ...vaktscertifiering utformad av **specialpedagoger** med tillhörande hjärt och lun... | x |  |  | 16 | [socialpedagog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/NUhh_NSS_itU) |
| 4 | ...vaktscertifiering utformad av **specialpedagoger** med tillhörande hjärt och lun... |  | x |  | 16 | [Specialpedagog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/f1zq_tpA_QtN) |
| 5 | ... har erfarenhet av arbete som **barnvakt** eller Nanny och alltid tycker... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 6 | ... av arbete som barnvakt eller **Nanny** och alltid tycker det är roli... | x | x | 5 | 5 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 7 | .... Som person ser vi att du är **ansvarsfull**, social och kreativ för att k... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 8 | ...t att du känner dig trygg som **barnvakt** och kan ta stort ansvar. Då b... | x | x | 8 | 8 | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| 9 | ...ig trygg som barnvakt och kan **ta stort ansvar**. Då barnen vanligtvis är i ål... | x |  |  | 15 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| | **Overall** | | | **40** | **116** | 40/116 = **34%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [socialpedagog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/NUhh_NSS_itU) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Specialpedagog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/f1zq_tpA_QtN) |
|  | x |  | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| x |  |  | [Certifikat/licenser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/s48H_Sft_DTn) |
| x | x | x | [Barnvakt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zVkQ_wLL_78i) |
| | | **2** | 2/6 = **33%** |