# Results for '929704a78e007aaadbe4c1833781237621108b7d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [929704a78e007aaadbe4c1833781237621108b7d](README.md) | 1 | 1609 | 6 | 12 | 12/244 = **5%** | 1/11 = **9%** |

## Source text

En fastighetsskötare ansvarar för den inre och yttre skötseln av fastigheten. Arbetsuppgifterna kan variera beroende på var man är anställd. Hos små fastighetsbolag är det vanligt att en och samma person har arbetsuppgifter inom ett bredare område, medan det inom ett större företag är vanligare att man är specialiserad i en yrkesroll.   Exempel på övergripande uppgifter som ofta ingår i arbetet: -Felavhjälpande underhåll  -Kontakt med hyresgäster  -Skötsel av grönytor, rabatter, häckar och träd  -Snöröjning, grusning och sandning på vintern  -Felsökning och mindre reparationer   En fastighetsvärd är fastighetsägarens ansikte utåt. Arbetsuppgifterna varierar beroende på var man är anställd men den främsta uppgiften är att se till så att hyresgästerna känner sig trygga och nöjda i sin arbets- och boendemiljö. Fastighetsvärden välkomnar hyresgästerna vid inflyttning och träffar dem vid utflyttning. Arbetet som fastighetsvärd kan även innefatta budgetansvar och vissa ekonomiska arbetsuppgifter.  -Exempel på övergripande arbetsuppgifter som ingår i arbetet: -Röra sig ute i bostadsområdena  -Träffa och skapa kontakt med hyresgäster  -Inspektion och bedömningar av skador  -Besiktningar vid in- och avflyttning   Utbildningens innehåll ska motsvara karaktärsämnena i gymnasieskolans nationella VVS och fastighetsprogram med programfördjupningar.   RIKTTID: Rikttiden på hela fastighetsutbildningen Fastighetsskötare eller Fastighetsvärd är 43 veckor på heltid.  STUDIER: Undervisningen består av såväl teori som praktiska övningar och arbetsplatsförlagt lärande på företag. 

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | En **fastighetsskötare** ansvarar för den inre och ytt... |  | x |  | 17 | [Fastighetsskötare/Fastighetsarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/C6Cv_9ZR_7Gb) |
| 1 | En **fastighetsskötare** ansvarar för den inre och ytt... | x |  |  | 17 | [Fastighetsskötare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/pMWJ_cJp_NDb) |
| 2 | ...d  -Snöröjning, grusning och **sandning** på vintern  -Felsökning och ... |  | x |  | 8 | [Sandning, körvana/Plogning, körvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/Enuq_cMG_7pw) |
| 3 | ...ng och sandning på vintern  -**Felsökning** och mindre reparationer   E... |  | x |  | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 4 | ...ch mindre reparationer   En **fastighetsvärd** är fastighetsägarens ansikte ... |  | x |  | 14 | [Bovärd/Fastighetsvärd/Husvärd, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/AjxW_jR6_J7P) |
| 4 | ...ch mindre reparationer   En **fastighetsvärd** är fastighetsägarens ansikte ... | x |  |  | 14 | [fastighetsvärd, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/oGoC_D1N_i5Y) |
| 5 | ...er sig trygga och nöjda i sin **arbets- och** boendemiljö. Fastighetsvärden... |  | x |  | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 6 | ... vid utflyttning. Arbetet som **fastighetsvärd** kan även innefatta budgetansv... |  | x |  | 14 | [Bovärd/Fastighetsvärd/Husvärd, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/AjxW_jR6_J7P) |
| 6 | ... vid utflyttning. Arbetet som **fastighetsvärd** kan även innefatta budgetansv... | x |  |  | 14 | [fastighetsvärd, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/oGoC_D1N_i5Y) |
| 7 | ...ighetsvärd kan även innefatta **budgetansvar** och vissa ekonomiska arbetsup... | x | x | 12 | 12 | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
| 8 | ...sieskolans nationella VVS och **fastighetsprogram** med programfördjupningar.  ... |  | x |  | 17 | [Gymnasieskolans VVS- och fastighetsprogram, inriktning kyl- och värmepumpsteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/6fod_Vd5_Lot) |
| 8 | ...sieskolans nationella VVS och **fastighetsprogram** med programfördjupningar.  ... |  | x |  | 17 | [Gymnasieskolans VVS- och fastighetsprogram, inriktning ventilationsteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Tt87_pLZ_Qad) |
| 8 | ...sieskolans nationella VVS och **fastighetsprogram** med programfördjupningar.  ... |  | x |  | 17 | [Gymnasieskolans VVS- och fastighetsprogram, inriktning fastighet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xtBJ_FRn_GRE) |
| 9 | ...å hela fastighetsutbildningen **Fastighetsskötare** eller Fastighetsvärd är 43 ve... |  | x |  | 17 | [Fastighetsskötare/Fastighetsarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/C6Cv_9ZR_7Gb) |
| 9 | ...å hela fastighetsutbildningen **Fastighetsskötare** eller Fastighetsvärd är 43 ve... | x |  |  | 17 | [Fastighetsskötare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/pMWJ_cJp_NDb) |
| 10 | ...ingen Fastighetsskötare eller **Fastighetsvärd** är 43 veckor på heltid.  ST... |  | x |  | 14 | [Bovärd/Fastighetsvärd/Husvärd, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/AjxW_jR6_J7P) |
| 10 | ...ingen Fastighetsskötare eller **Fastighetsvärd** är 43 veckor på heltid.  ST... | x |  |  | 14 | [fastighetsvärd, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/oGoC_D1N_i5Y) |
| | **Overall** | | | **12** | **244** | 12/244 = **5%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Gymnasieskolans VVS- och fastighetsprogram, inriktning kyl- och värmepumpsteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/6fod_Vd5_Lot) |
|  | x |  | [Bovärd/Fastighetsvärd/Husvärd, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/AjxW_jR6_J7P) |
|  | x |  | [Fastighetsskötare/Fastighetsarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/C6Cv_9ZR_7Gb) |
|  | x |  | [Sandning, körvana/Plogning, körvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/Enuq_cMG_7pw) |
| x | x | x | [Budgetansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gt5f_wdi_FSJ) |
|  | x |  | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
|  | x |  | [Gymnasieskolans VVS- och fastighetsprogram, inriktning ventilationsteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Tt87_pLZ_Qad) |
| x |  |  | [fastighetsvärd, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/oGoC_D1N_i5Y) |
| x |  |  | [Fastighetsskötare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/pMWJ_cJp_NDb) |
|  | x |  | [Gymnasieskolans VVS- och fastighetsprogram, inriktning fastighet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xtBJ_FRn_GRE) |
|  | x |  | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **1** | 1/11 = **9%** |