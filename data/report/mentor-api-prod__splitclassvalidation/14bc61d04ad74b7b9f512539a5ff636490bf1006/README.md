# Results for '14bc61d04ad74b7b9f512539a5ff636490bf1006'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [14bc61d04ad74b7b9f512539a5ff636490bf1006](README.md) | 1 | 2830 | 15 | 10 | 82/177 = **46%** | 6/11 = **55%** |

## Source text

Administrativ stjärna till det engagerade IT-företaget Telia Cygate Är du en person som redan nu jobbar med administrativa uppgifter inom IT eller inte gör det just nu men känner att du är öppen för utmaningen och har det som krävs. Då är du varmt välkommen att ansöka till positionen som IT-administratör för Cygate som är en del av Telia där du kommer arbeta inom ett erfaret, stöttande och engagerat team. Där du även kommer få all möjlighet till att utvecklas och lyfta din arbetskarriär till nästa steg.  OM TJÄNSTEN  Vi söker nu en IT-administratör till Cygate där du kommer arbeta med kollegor som har mycket senior kompetens och expert kunskap inom deras respektive områden. Cygate är ett företag som brinner mycket för teknik men de brinner minst lika mycket för människorna inom företaget och deras kunder. Det är därför Cygate strävar efter att alla ska kunna utvecklas inom företaget och leverera IT-service i världsklass till sina kunder. Du kommer även som IT-administratör på sikt få ansvara över ett par kunder själv som omsätter mellan 10-50 miljoner kronor. Utöver det kommer du även få åka ut på kundbesök och på så sätt växa i din roll som IT-adminstratör.  Detta är ett konsultuppdrag som sträcker sig i 9-12 månader, men där det finns goda chanser till vidare anställning för den med rätt ambitioner och driv!  Vi söker:   * Medarbetare till ett mindre team som ska arbeta med ett specifikt projekt som drar igång i september * Samt till ett annat team som arbetar mer varierande och med olika projekt.   Du erbjuds   * Arbete i ett team som har bra sammanhållning och hjälper varandra i det dagliga arbetet * Möjlighet att arbeta både på plats och remote * En engagerad konsultchef som stöttar dig i din utveckling   Som konsult för Academic Work erbjuder vi stora möjligheter för dig att växa professionellt, bygga ditt nätverk och skapa värdefulla kontakter för framtiden. Läs mer om vårt konsulterbjudande.  ARBETSUPPGIFTER  - Uppdatera priser i avtal Visma - Skapa nya avtal - Hantera fakturering - Rapportera avvikelser till ekonomiavdelningen  VI SÖKER DIG SOM  För att lyckas i denna roll tror vi att du har minst en avslutad gymnasieutbildning. Du har ett bra sinne för siffror och har ett noggrant arbetssätt och behärskar både svenska och engelska obehindrat i tal och skrift.  Som person ser vi gärna att du är:   * Noggrann och strukturerad * Problemlösande * Initiativtagande   #  Övrig information   * Start: Slutet av augusti * Omfattning: Heltid * Placering: Solna, Stockholm. Möjlighet till arbete remote 2 dagar i veckan   Rekryteringsprocessen hanteras av Academic Work och kundens önskemål är att alla frågor rörande tjänsten hanteras av Academic Work.  Vi går igenom urvalet löpande och annonsen kan stängas ner innan tjänsten är tillsatt om vi gått över till urvals- och intervjufasen.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...v stjärna till det engagerade **IT**-företaget Telia Cygate Är du ... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 2 | ...administrativa uppgifter inom **IT** eller inte gör det just nu me... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 3 | ...tt ansöka till positionen som **IT-administratör** för Cygate som är en del av T... | x | x | 16 | 16 | [IT-administratör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/oKni_R4v_4Mt) |
| 4 | ...  OM TJÄNSTEN  Vi söker nu en **IT-administratör** till Cygate där du kommer arb... | x | x | 16 | 16 | [IT-administratör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/oKni_R4v_4Mt) |
| 5 | ...s inom företaget och leverera **IT**-service i världsklass till si... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 6 | ...inom företaget och leverera IT**-service** i världsklass till sina kunde... |  | x |  | 8 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 7 | ...na kunder. Du kommer även som **IT-administratör** på sikt få ansvara över ett p... | x | x | 16 | 16 | [IT-administratör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/oKni_R4v_4Mt) |
| 8 | ...å så sätt växa i din roll som **IT-adminstratör**.  Detta är ett konsultuppdrag... | x |  |  | 15 | [IT-administratör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/oKni_R4v_4Mt) |
| 9 | ... att du har minst en avslutad **gymnasieutbildning**. Du har ett bra sinne för sif... | x |  |  | 18 | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| 9 | ... att du har minst en avslutad **gymnasieutbildning**. Du har ett bra sinne för sif... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 10 | ...sinne för siffror och har ett **noggrant** arbetssätt och behärskar både... | x |  |  | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 11 | ...arbetssätt och behärskar både **svenska** och engelska obehindrat i tal... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 12 | ...ch behärskar både svenska och **engelska** obehindrat i tal och skrift. ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 13 | ...n ser vi gärna att du är:   * **Noggrann** och strukturerad * Problemlös... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 14 | ...utet av augusti * Omfattning: **Heltid** * Placering: Solna, Stockholm... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 15 | ...fattning: Heltid * Placering: **Solna**, Stockholm. Möjlighet till ar... |  | x |  | 5 | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| 16 | ...g: Heltid * Placering: Solna, **Stockholm**. Möjlighet till arbete remote... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 17 | ...na, Stockholm. Möjlighet till **arbete remote** 2 dagar i veckan   Rekryterin... | x |  |  | 13 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| | **Overall** | | | **82** | **177** | 82/177 = **46%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Gymnasial utbildning, 3 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/7wEe_uui_q7S) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [IT-administratör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/oKni_R4v_4Mt) |
| x |  |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
|  | x |  | [Solna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zHxw_uJZ_NJ8) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/11 = **55%** |