# Results for 'cecdfc719d8a29695d160fd2282427f7f3c83c1e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [cecdfc719d8a29695d160fd2282427f7f3c83c1e](README.md) | 1 | 3684 | 24 | 15 | 58/315 = **18%** | 8/20 = **40%** |

## Source text

Bibliotekarie med fokus på unga Sigtuna kommun är en plats med stor spännvidd. Här ryms levande landsbygd, småstad och en internationell flygplats. Ett rikt kulturarv och kulturell mångfald. En stolt historia och stora framtidsplaner. Kanske är det därför drivkraften är så stark att växa, förändras och utvecklas. Det är också därför det är så spännande att arbeta här. Hos oss blir du del av en trevlig och prestigelös kultur där vi hjälps åt och tar tillvara på varandras kunskap, olikheter och erfarenheter. Arbetet är ofta omväxlande, ibland utmanande men alltid meningsfullt. Välkommen hit!  Kultur- och fritidsförvaltningen i Sigtuna kommun arbetar för att främja kultur- och fritidsverksamheten i kommunen, vi är en arbetsplats som vill göra skillnad! Vi är en politiskt styrd serviceorganisation där våra uppdragsgivare är invånarna. Vi arbetar tillsammans i våra olika verksamheter för att hitta nya idéer och lösningar som bidrar till invånarnas livskvalitet, hållbarhet och hälsa samt ökad integration och att motverka utanförskap.   Förvaltningen består av fem verksamhetsområden som rymmer: - Fritid, idrott, sport, simhall, ishall och föreningsverksamhet - Scenkonst, film, teater, konst, musik, museum, bibliotek och kulturarv - Ungdomskultur, fritidsverksamhet, kulturskola och fältarbete    ARBETSUPPGIFTER Vi vill utveckla bibliotekens verksamhet för unga. Från hösten 2022 och under ett år framåt kommer vi genomföra ett projekt med fokus på målgruppen unga. Vi vill utveckla bibliotekens utbud och testa nya arbetssätt. Syftet med projektet är att ta några rejäla kliv framåt i vårt arbete för målgruppen unga.  Vi söker en person med ett stort engagemang för läsning och att arbeta läsfrämjande för unga. Du har en öppen syn för vad bibliotek kan innebära för unga människor och god insyn i hur ett bibliotek på bästa sätt kan bidra till hela kommunens arbete för deras trygghet och utveckling.  Du kommer arbeta med bibliotekets utbud för unga, planera och genomföra aktiviteter för unga. Sedvanlig bibliotekarietjänst i informationsdisk i bibliotekens avdelning för barn och unga kan också ingå.  Du kan arbeta under skiftande omständigheter och med människor med olika bakgrunder och förutsättningar. Du är stabil och mogen och kan ta egna initiativ och driva dem självständigt och i grupp. Du kan skapa och upprätthålla relationer och du är empatisk och tydlig i din kommunikation.  Denna tjänst erbjuder stort utrymme för kreativitet och testa idéer. Du får möjlighet att vidareutveckla en verksamhet tillsammans i en grupp med driv framåt.  KVALIFIKATIONER Vi söker dig som - har akademisk examen eller motsvarande som bedöms relevant för tjänsten - goda kunskaper om litteratur, läsfrämjande och läsningens betydelse för samhället.  Det är meriterande om du har dokumenterad erfarenhet av - av att planera och genomföra aktiviteter för unga. -att arbete läsfrämjande för unga.  Personliga egenskaper Vi lägger stor vikt vid dina personliga egenskaper och lämplighet för tjänsten och söker dig som - har hög förmåga att planera och genomföra ditt arbete och att hålla deadlines. - är kreativ och kan forma nya arbetssätt. - är drivande och van att ta egna initiativ, driva dem och att sätta dig in i nya uppgifter. - har gott omdöme, är lyhörd och samarbetsinriktad.  ÖVRIGT Som anställd i Sigtuna kommun får du ta del av en rad förmåner, bl.a. blir du automatiskt medlem i Personalklubben där du kan ta delta i olika aktiviteter. Läs gärna mer om våra andra förmåner och hur det är att jobba hos oss i Sigtuna kommun! https://www.sigtuna.se/naringsliv-och-arbete/jobba-hos-oss.html  Vi undanber oss erbjudanden om annonserings- och rekryteringshjälp.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bibliotekarie** med fokus på unga Sigtuna kom... |  | x |  | 13 | [Bibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GujS_MtW_PRH) |
| 1 | **Bibliotekarie** med fokus på unga Sigtuna kom... | x |  |  | 13 | [Bibliotekarier m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/qidE_JGZ_MdL) |
| 2 | ...bliotekarie med fokus på unga **Sigtuna** kommun är en plats med stor s... | x | x | 7 | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 3 | ...arie med fokus på unga Sigtuna** kommun** är en plats med stor spännvid... | x |  |  | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 4 | .... Välkommen hit!  Kultur- och **fritidsförvaltningen** i Sigtuna kommun arbetar för ... |  | x |  | 20 | [Kommunal förvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/detQ_YNb_9nG) |
| 5 | ...r- och fritidsförvaltningen i **Sigtuna** kommun arbetar för att främja... | x | x | 7 | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 6 | ...fritidsförvaltningen i Sigtuna** kommun** arbetar för att främja kultur... | x |  |  | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 7 | ... livskvalitet, hållbarhet och **hälsa** samt ökad integration och att... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 8 | ...ksamhetsområden som rymmer: - **Fritid**, idrott, sport, simhall, isha... | x |  |  | 6 | [Fritidshem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Aq3e_Bdv_v45) |
| 9 | ...områden som rymmer: - Fritid, **idrott**, sport, simhall, ishall och f... | x | x | 6 | 6 | [Idrott, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QV1z_Wdx_HaM) |
| 10 | ...som rymmer: - Fritid, idrott, **sport**, simhall, ishall och förening... | x | x | 5 | 5 | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
| 11 | ...all och föreningsverksamhet - **Scenkonst**, film, teater, konst, musik, ... | x | x | 9 | 9 | [Scenkonst, **skill**](http://data.jobtechdev.se/taxonomy/concept/ryXf_E4d_FYU) |
| 12 | ...och föreningsverksamhet - Scen**konst**, film, teater, konst, musik, ... |  | x |  | 5 | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| 13 | ...eningsverksamhet - Scenkonst, **film**, teater, konst, musik, museum... | x |  |  | 4 | (not found in taxonomy) |
| 14 | ...verksamhet - Scenkonst, film, **teater**, konst, musik, museum, biblio... | x | x | 6 | 6 | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| 15 | ...et - Scenkonst, film, teater, **konst**, musik, museum, bibliotek och... | x |  |  | 5 | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| 16 | ...enkonst, film, teater, konst, **musik**, museum, bibliotek och kultur... | x |  |  | 5 | [Musik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/muBr_pr4_MvG) |
| 17 | ..., film, teater, konst, musik, **museum**, bibliotek och kulturarv - Un... | x |  |  | 6 | [Museum, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rXhg_8kE_9zx) |
| 18 | ...teater, konst, musik, museum, **bibliotek** och kulturarv - Ungdomskultur... | x |  |  | 9 | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
| 19 | ...ETSUPPGIFTER Vi vill utveckla **bibliotekens verksamhet** för unga. Från hösten 2022 oc... | x |  |  | 23 | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
| 20 | ...ruppen unga. Vi vill utveckla **bibliotekens** utbud och testa nya arbetssät... | x |  |  | 12 | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
| 21 | .... Du har en öppen syn för vad **bibliotek** kan innebära för unga människ... | x |  |  | 9 | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
| 22 | ...iskor och god insyn i hur ett **bibliotek** på bästa sätt kan bidra till ... | x |  |  | 9 | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
| 23 | ...ckling.  Du kommer arbeta med **bibliotekets** utbud för unga, planera och g... | x |  |  | 12 | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
| 24 | ...ietjänst i informationsdisk i **bibliotekens** avdelning för barn och unga k... | x |  |  | 12 | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
| 25 | ... egna initiativ och driva dem **självständigt** och i grupp. Du kan skapa och... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 26 | ...TIONER Vi söker dig som - har **akademisk **examen eller motsvarande som b... |  | x |  | 10 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 27 | ...TIONER Vi söker dig som - har **akademisk examen** eller motsvarande som bedöms ... |  | x |  | 16 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 28 | ...söker dig som - har akademisk **examen** eller motsvarande som bedöms ... | x | x | 6 | 6 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 29 | ... tjänsten - goda kunskaper om **litteratur**, läsfrämjande och läsningens ... | x |  |  | 10 | [litteratur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ttUh_HPM_np2) |
| 30 | ...ar gott omdöme, är lyhörd och **samarbetsinriktad**.  ÖVRIGT Som anställd i Sigt... |  | x |  | 17 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 31 | ...ktad.  ÖVRIGT Som anställd i **Sigtuna** kommun får du ta del av en ra... | x | x | 7 | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 32 | ... ÖVRIGT Som anställd i Sigtuna** kommun** får du ta del av en rad förmå... | x |  |  | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 33 | ...ur det är att jobba hos oss i **Sigtuna** kommun! https://www.sigtuna.s... |  | x |  | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| | **Overall** | | | **58** | **315** | 58/315 = **18%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [Fritidshem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Aq3e_Bdv_v45) |
|  | x |  | [Bibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GujS_MtW_PRH) |
| x | x | x | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| x |  |  | (not found in taxonomy) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Idrott, **keyword**](http://data.jobtechdev.se/taxonomy/concept/QV1z_Wdx_HaM) |
|  | x |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
|  | x |  | [Kommunal förvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/detQ_YNb_9nG) |
| x |  |  | [Biblioteksverksamhet, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/g8r7_YEp_eSp) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x |  |  | [Musik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/muBr_pr4_MvG) |
| x | x | x | [Antikviteter/Konst, **skill**](http://data.jobtechdev.se/taxonomy/concept/nPhE_Q9x_sbs) |
| x | x | x | [Sport, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oiV9_drz_NYC) |
| x |  |  | [Bibliotekarier m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/qidE_JGZ_MdL) |
| x |  |  | [Museum, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rXhg_8kE_9zx) |
| x | x | x | [Scenkonst, **skill**](http://data.jobtechdev.se/taxonomy/concept/ryXf_E4d_FYU) |
| x |  |  | [litteratur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ttUh_HPM_np2) |
| x | x | x | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| | | **8** | 8/20 = **40%** |