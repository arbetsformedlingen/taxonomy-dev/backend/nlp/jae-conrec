# Results for 'b1d42214e88526a28fde29ae4dd8495e568d2562'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b1d42214e88526a28fde29ae4dd8495e568d2562](README.md) | 1 | 5322 | 31 | 25 | 159/734 = **22%** | 7/25 = **28%** |

## Source text

Operationssjuksköterska Tillsammans skapar vi på Sahlgrenska Universitetssjukhuset vård av högsta kvalitet med patienten i fokus. Med stort engagemang och ständigt lärande är vi ett av landets ledande universitetssjukhus. Vi bedriver allt från länssjukvård till nationell högspecialiserad vård. Genom intressant forskning har vi en inriktning mot framtidens sjukvård. Hos oss är det viktigt med digital kompetens och att alla bidrar till den digitala utvecklingen. I vårt arbete tar vi tillvara den bredd av kunskap och erfarenhet vi gemensamt har – Tillsammans med dig vill vi skapa värde för våra patienter.    Om oss  Operation 2 bedriver operationsverksamhet med inriktning mot gynekologi och obstetrik och servar Nordens största förlossningsklinik dygnets alla timmar.  Benign gynekologi bedrivs parallellt med planerade och akuta obstetriska ingrepp. Vi ger vård till kvinnor med gynekologiska tillstånd för att ge en förbättrad livskvalitet samt till gravida kvinnor som behöver kirurgiska åtgärder för att kunna fullfölja sin graviditet eller förlossning. Den obstetriska kirurgin består till stor del av elektiva kejsarsnitt men också ett stort akut uppdrag med urakuta snitt, placentalösningar, rupturer. På Operation 2 finns även en postoperativ enhet.  På enheten arbetar operationssjuksköterskor, anestesisjuksköterskor och undersköterskor tillsammans med instruktör, sektionsledare, bemanningsassistent och vårdenhetschefer. Totalt är vi cirka 70 medarbetare.  Vi arbetar med rotationstjänstgöring till Operation 1, Östra för att få variation, stimulans och en bred kompetens. Operation 1 är en av Nordens största colorektala enheter med varierande operationer med allt från enklare ingrepp till omfattande avancerade elektiva ingrepp inom colo- och övre gastrokirurgi för både vuxna och barn. Här ges även anestesiservice till tandvården, psykiatriska kliniken och GEA. Enheten bedriver också en stor akut verksamhet.  Vad kan vi erbjuda dig?  Vi erbjuder grundschema dag/kväll med tjänstgöring var fjärde helg.  Verksamheten har ett tydligt fokus på personcentrerad vård och vi är måna om att arbeta med vår arbetsmiljö. Medarbetare arbetar tillsammans med utveckling på avdelningen, bland annat genom regelbundna utbildningstillfällen där vi lyfter matnyttig information om viktiga arbetsuppgifter och genom Gröna korset som är ett verktyg för patient- och kvalitetssäker vård.  Vi kan erbjuda dig som söker ett omväxlande och stimulerande arbete tillsammans med engagerade och kompetenta kollegor. Du kommer att få en gedigen introduktion som anpassas efter dina tidigare erfarenheter samt kontinuerlig utveckling med interna och externa utbildningar.  Arbetsuppgifter  Dina huvudsakliga arbetsuppgifter kommer vara som operationssjuksköterska.  Om dig  Vi söker dig som är legitimerad sjuksköterska med specialistutbildning i operationssjukvård eller som snart är klar specialistsjuksköterska inom operationssjukvård. Det är meriterande om du har tidigare erfarenhet av arbete som operationssjuksköterska.  Vi söker dig som är legitimerad sjuksköterska med specialistutbildning inom operationssjukvård. Du har god samarbetsförmåga och visar respekt och omtanke för dina kollegor. Du har ett stort engagemang i arbetet, bidrar med nytänkande, stimuleras av utmaningar och är flexibel. Vidare är du självständig och kan hantera både elektiva och akuta situationer. Stor vikt läggs vid personliga egenskaper och personlig lämplighet.  Välkommen med din ansökan!  https://www.sahlgrenska.se/jobb-och-framtid/tillsammans-for-patienten/https://www.sahlgrenska.se/jobb-och-framtid/tillsammans-for-patienten/https://www.sahlgrenska.se/jobb-och-framtid/tillsammans-for-patienten/  Vid eventuell rekrytering av läkare/sjuksköterska med utländsk legitimation/specialistbevis föregås den aktuella tjänsten av process för att uppfylla kraven för svensk legitimation/specialistbevis.  Om Västra Götalandsregionen Västra Götalandsregionen finns till för människorna i Västra Götaland. Vi ser till att det finns god hälso- och sjukvård för alla. Vi arbetar för en hållbar utveckling och tillväxt, bra miljö, förbättrad folkhälsa, ett rikt kulturliv och goda kommunikationer i hela Västra Götaland. Västra Götalandsregionen arbetar aktivt för att digitalisera fler arbetssätt. För att vara en del av den digitala omvandlingen har du med dig grundläggande färdigheter och kan använda digitala verktyg och tjänster. Du kan söka information, kommunicera, interagera digitalt, är riskmedveten och har motivation att delta i utvecklingen för att lära nytt.   Vill du veta mer om Västra Götalandsregionen kan du besöka vår introduktion till nya medarbetare på länken https://www.vgregion.se/introduktion   Ansökan Västra Götalandsregionen ser helst att du registrerar din ansökan via rekryteringssystemet. Om du som sökande har frågor om den utannonserade tjänsten eller av särskilda och speciella skäl inte kan registrera dina uppgifter i ett offentligt system - kontakta kontaktperson för respektive annons.   Till bemannings-, förmedlings- och rekryteringsföretag och till dig som är försäljare: Vi undanber oss vänligen men bestämt direktkontakt med bemannings-, förmedlings- och rekryteringsföretag samt andra externa aktörer och försäljare av ytterligare jobbannonser. Västra Götalandsregionen har upphandlade avtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Operationssjuksköterska** Tillsammans skapar vi på Sahl... | x | x | 23 | 23 | [Operationssjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YJKc_yXU_DUH) |
| 2 | ... Om oss  Operation 2 bedriver **operationsverksamhet** med inriktning mot gynekologi... |  | x |  | 20 | [operation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uHRe_vtW_Bev) |
| 3 | ...verksamhet med inriktning mot **gynekologi** och obstetrik och servar Nord... | x |  |  | 10 | [Gynekolog, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ASoB_Zaq_wFL) |
| 4 | ...inriktning mot gynekologi och **obstetrik** och servar Nordens största fö... | x |  |  | 9 | [Obstetrik, **skill**](http://data.jobtechdev.se/taxonomy/concept/UtTP_C3o_hR3) |
| 5 | ...ygnets alla timmar.  Benign **gynekologi** bedrivs parallellt med planer... | x |  |  | 10 | [Gynekolog, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ASoB_Zaq_wFL) |
| 6 | ...iv enhet.  På enheten arbetar **operationssjuksköterskor**, anestesisjuksköterskor och u... |  | x |  | 24 | [Operationssjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YJKc_yXU_DUH) |
| 6 | ...iv enhet.  På enheten arbetar **operationssjuksköterskor**, anestesisjuksköterskor och u... | x |  |  | 24 | [Operationssjuksköterskor, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/cuaN_Rj5_YCc) |
| 7 | ...tar operationssjuksköterskor, **anestesisjuksköterskor** och undersköterskor tillsamma... |  | x |  | 22 | [Anestesisjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4qYQ_tWb_QjD) |
| 7 | ...tar operationssjuksköterskor, **anestesisjuksköterskor** och undersköterskor tillsamma... | x |  |  | 22 | [Anestesisjuksköterskor, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/nrVt_xUL_KdQ) |
| 8 | ...r, anestesisjuksköterskor och **undersköterskor** tillsammans med instruktör, s... | x | x | 15 | 15 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 9 | ...d instruktör, sektionsledare, **bemanningsassistent** och vårdenhetschefer. Totalt ... | x | x | 19 | 19 | [Bemanningsassistent, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1eUR_54w_v1C) |
| 10 | ...ges även anestesiservice till **tandvården**, psykiatriska kliniken och GE... |  | x |  | 10 | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
| 11 | ...är måna om att arbeta med vår **arbetsmiljö**. Medarbetare arbetar tillsamm... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 12 | ...genom Gröna korset som är ett **verktyg** för patient- och kvalitetssäk... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 13 | ...betsuppgifter kommer vara som **operationssjuksköterska**.  Om dig  Vi söker dig som är... | x | x | 23 | 23 | [Operationssjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YJKc_yXU_DUH) |
| 14 | ... söker dig som är legitimerad **sjuksköterska** med specialistutbildning i op... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 14 | ... söker dig som är legitimerad **sjuksköterska** med specialistutbildning i op... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 15 | ...legitimerad sjuksköterska med **specialistutbildning** i operationssjukvård eller so... |  | x |  | 20 | [Specialistsjuksköterskeutbildning, operationssjukvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/bf2X_Hfm_De9) |
| 16 | ...ka med specialistutbildning i **operationssjukvård** eller som snart är klar speci... | x |  |  | 18 | [Operationssjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/2KV1_FD8_vGp) |
| 16 | ...ka med specialistutbildning i **operationssjukvård** eller som snart är klar speci... |  | x |  | 18 | [Specialistsjuksköterskeutbildning, operationssjukvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/bf2X_Hfm_De9) |
| 17 | ...kvård eller som snart är klar **specialistsjuksköterska** inom operationssjukvård. Det ... |  | x |  | 23 | [Specialistsjuksköterskeutbildning, infektionssjukvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1dUg_Jaj_yvZ) |
| 18 | ... specialistsjuksköterska inom **operationssjukvård**. Det är meriterande om du har... | x |  |  | 18 | [Operationssjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/2KV1_FD8_vGp) |
| 19 | ...gare erfarenhet av arbete som **operationssjuksköterska**.  Vi söker dig som är legitim... | x | x | 23 | 23 | [Operationssjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YJKc_yXU_DUH) |
| 20 | ... söker dig som är legitimerad **sjuksköterska** med specialistutbildning inom... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 20 | ... söker dig som är legitimerad **sjuksköterska** med specialistutbildning inom... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 21 | ...legitimerad sjuksköterska med **specialistutbildning** inom operationssjukvård. Du h... |  | x |  | 20 | [Specialistsjuksköterskeutbildning, operationssjukvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/bf2X_Hfm_De9) |
| 22 | ...med specialistutbildning inom **operationssjukvård**. Du har god samarbetsförmåga ... | x |  |  | 18 | [Operationssjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/2KV1_FD8_vGp) |
| 22 | ...med specialistutbildning inom **operationssjukvård**. Du har god samarbetsförmåga ... |  | x |  | 18 | [Specialistsjuksköterskeutbildning, operationssjukvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/bf2X_Hfm_De9) |
| 23 | ...och är flexibel. Vidare är du **självständig** och kan hantera både elektiva... |  | x |  | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 24 | ... Vid eventuell rekrytering av **läkare**/sjuksköterska med utländsk le... | x | x | 6 | 6 | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| 25 | ...entuell rekrytering av läkare/**sjuksköterska** med utländsk legitimation/spe... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 25 | ...entuell rekrytering av läkare/**sjuksköterska** med utländsk legitimation/spe... | x |  |  | 13 | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| 26 | ...timation/specialistbevis.  Om **Västra Götalandsregionen** Västra Götalandsregionen finn... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 27 | ...  Om Västra Götalandsregionen **Västra Götaland**sregionen finns till för männi... | x | x | 15 | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 28 | ...alandsregionen Västra Götaland**sregionen** finns till för människorna i ... | x |  |  | 9 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 29 | ... finns till för människorna i **Västra Götaland**. Vi ser till att det finns go... | x |  |  | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 30 | ...Vi ser till att det finns god **hälso**- och sjukvård för alla. Vi ar... | x |  |  | 5 | [Hälso, **keyword**](http://data.jobtechdev.se/taxonomy/concept/5TsD_dN8_Uco) |
| 31 | ...Vi ser till att det finns god **hälso- oc**h sjukvård för alla. Vi arbeta... |  | x |  | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 32 | ...d för alla. Vi arbetar för en **hållbar utveckling** och tillväxt, bra miljö, förb... | x |  |  | 18 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 33 | ...llväxt, bra miljö, förbättrad **folkhälsa**, ett rikt kulturliv och goda ... | x | x | 9 | 9 | [folkhälsa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jEdA_1Fe_Jji) |
| 34 | ..., ett rikt kulturliv och goda **kommunikationer** i hela Västra Götaland. Västr... | x |  |  | 15 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 35 | ...h goda kommunikationer i hela **Västra Götaland**. Västra Götalandsregionen arb... | x | x | 15 | 15 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 36 | ...ioner i hela Västra Götaland. **Västra Götalandsregionen** arbetar aktivt för att digita... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 37 | ...eter och kan använda digitala **verktyg** och tjänster. Du kan söka inf... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 38 | ...a nytt.   Vill du veta mer om **Västra Götalandsregionen** kan du besöka vår introduktio... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| 39 | ...ion.se/introduktion   Ansökan **Västra Götalandsregionen** ser helst att du registrerar ... | x |  |  | 24 | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| | **Overall** | | | **159** | **734** | 159/734 = **22%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Specialistsjuksköterskeutbildning, infektionssjukvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1dUg_Jaj_yvZ) |
| x | x | x | [Bemanningsassistent, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1eUR_54w_v1C) |
| x |  |  | [Operationssjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/2KV1_FD8_vGp) |
| x | x | x | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
|  | x |  | [Anestesisjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4qYQ_tWb_QjD) |
| x |  |  | [Hälso, **keyword**](http://data.jobtechdev.se/taxonomy/concept/5TsD_dN8_Uco) |
| x |  |  | [Gynekolog, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ASoB_Zaq_wFL) |
| x |  |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
|  | x |  | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
|  | x |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| x |  |  | [Obstetrik, **skill**](http://data.jobtechdev.se/taxonomy/concept/UtTP_C3o_hR3) |
| x | x | x | [Operationssjuksköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YJKc_yXU_DUH) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
|  | x |  | [Tandvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ba6n_RTw_PEr) |
|  | x |  | [Specialistsjuksköterskeutbildning, operationssjukvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/bf2X_Hfm_De9) |
| x |  |  | [Operationssjuksköterskor, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/cuaN_Rj5_YCc) |
| x | x | x | [folkhälsa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jEdA_1Fe_Jji) |
| x |  |  | [Sjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jrjV_wcz_fxg) |
| x |  |  | [Anestesisjuksköterskor, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/nrVt_xUL_KdQ) |
| x | x | x | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
|  | x |  | [operation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uHRe_vtW_Bev) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| x | x | x | [Västra Götalands län, **region**](http://data.jobtechdev.se/taxonomy/concept/zdoY_6u5_Krt) |
| | | **7** | 7/25 = **28%** |