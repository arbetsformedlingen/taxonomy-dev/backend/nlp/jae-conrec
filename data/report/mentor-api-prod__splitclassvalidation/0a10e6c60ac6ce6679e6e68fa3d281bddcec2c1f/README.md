# Results for '0a10e6c60ac6ce6679e6e68fa3d281bddcec2c1f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0a10e6c60ac6ce6679e6e68fa3d281bddcec2c1f](README.md) | 1 | 1737 | 14 | 11 | 90/218 = **41%** | 5/12 = **42%** |

## Source text

Besiktningstekniker till vår kund i Strängnäs! Vi söker besiktningstekniker!  Som besiktningstekniker jobbar du med att besikta fordon och kontrollerar att de uppfyller uppställda miljö- och säkerhetskrav utifrån ett digitalt protokoll. Du får ett arbete som lägger stort fokus på att skapa nya kontakter inom branschen samt service och säkerhet. Det är ett ansvarsfullt och noggrant arbete med tidvis högt tempo då det är vänte kunder, det är därför viktigt att kunna vara flexibel och se till att alla kunder är nöjda. Du kommer att arbeta dels självständigt men och i team tillsammans med övriga kollegor. Tjänsten kommer att vara på heltid måndag – fredag med goda möjligheter till arbeta övertid.  Vi söker dig Som är motiverad, engagerad och van vid att arbeta under ett högt tempo. Du bör även ha tidigare fordonskunskap via arbetserfarenheter eventuellt utbildning. Vi ser gärna att du har ett stort tekniskt intresse och trivs att arbeta med människor. Du har en positiv inställning och tycker om att samarbeta med dina kollegor.  Vi söker dig som: • Service intresse • B-körkort utan begränsningar • Teknisk fordonskunskap • Grundläggande datakunskaper Meriterande: Tillgång till egen bil. Språkkunskaper utöver svenska och engelska.  Om oss: Aura personal tror på människor och att alla som vill kan. Våra år i branschen har gjort oss övertygade om att många fler kan skapa sin egen framgång bara någon ser dem och ger dem chansen. Vårt fokus ligger på att hitta just de som vill utvecklas och se möjligheter. Aura personal vill hjälpa dig i din utveckling genom att erbjuda Aura Academy. Det är en kostnadsfri utbildning inom lager, verkstad samt heta arbeten. För mer information besök gärna vår hemsida eller kontakt oss!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Besiktningstekniker** till vår kund i Strängnäs! Vi... |  | x |  | 19 | [Besiktningstekniker, bilprovning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2Wq4_Wed_tBz) |
| 1 | **Besiktningstekniker** till vår kund i Strängnäs! Vi... | x | x | 19 | 19 | [Besiktningstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ifu8_Sui_fUM) |
| 2 | ...ningstekniker till vår kund i **Strängnäs**! Vi söker besiktningstekniker... | x | x | 9 | 9 | [Strängnäs, **municipality**](http://data.jobtechdev.se/taxonomy/concept/shnD_RiE_RKL) |
| 3 | ...år kund i Strängnäs! Vi söker **besiktningstekniker**!  Som besiktningstekniker job... |  | x |  | 19 | [Besiktningstekniker, bilprovning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2Wq4_Wed_tBz) |
| 3 | ...år kund i Strängnäs! Vi söker **besiktningstekniker**!  Som besiktningstekniker job... | x | x | 19 | 19 | [Besiktningstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ifu8_Sui_fUM) |
| 4 | ...ker besiktningstekniker!  Som **besiktningstekniker** jobbar du med att besikta for... |  | x |  | 19 | [Besiktningstekniker, bilprovning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2Wq4_Wed_tBz) |
| 4 | ...ker besiktningstekniker!  Som **besiktningstekniker** jobbar du med att besikta for... | x | x | 19 | 19 | [Besiktningstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ifu8_Sui_fUM) |
| 5 | ...vice och säkerhet. Det är ett **ansvarsfullt** och noggrant arbete med tidvi... | x |  |  | 12 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 6 | .... Det är ett ansvarsfullt och **noggrant** arbete med tidvis högt tempo ... | x |  |  | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 7 | ...under är nöjda. Du kommer att **arbeta** dels självständigt men och i ... | x |  |  | 6 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 8 | ...da. Du kommer att arbeta dels **självständigt** men och i team tillsammans me... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 9 | .... Tjänsten kommer att vara på **heltid** måndag – fredag med goda möjl... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 10 | ...inställning och tycker om att **samarbeta med** dina kollegor.  Vi söker dig ... | x |  |  | 13 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 11 | ...ker om att samarbeta med dina **kollegor**.  Vi söker dig som: • Service... | x |  |  | 8 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 12 | ...dig som: • Service intresse • **B-körkort** utan begränsningar • Teknisk ... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 13 | ...en bil. Språkkunskaper utöver **svenska** och engelska.  Om oss: Aura p... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 14 | ...kkunskaper utöver svenska och **engelska**.  Om oss: Aura personal tror ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 15 | ...n kostnadsfri utbildning inom **lager**, verkstad samt heta arbeten. ... |  | x |  | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| | **Overall** | | | **90** | **218** | 90/218 = **41%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Besiktningstekniker, bilprovning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2Wq4_Wed_tBz) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Besiktningstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ifu8_Sui_fUM) |
| x |  |  | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Strängnäs, **municipality**](http://data.jobtechdev.se/taxonomy/concept/shnD_RiE_RKL) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/12 = **42%** |