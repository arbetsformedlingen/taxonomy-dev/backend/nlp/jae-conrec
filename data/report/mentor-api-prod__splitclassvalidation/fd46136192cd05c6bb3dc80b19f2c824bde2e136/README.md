# Results for 'fd46136192cd05c6bb3dc80b19f2c824bde2e136'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [fd46136192cd05c6bb3dc80b19f2c824bde2e136](README.md) | 1 | 2076 | 13 | 2 | 0/93 = **0%** | 0/11 = **0%** |

## Source text

Har du något viktigt att berätta? Vill du vara med och skapa framtidens narrativ,  historier, upplevelser och innehåll för immersiva medier?    Inom utbildningen Immersive Experience Creator lär du dig att regissera, skapa berättande och ta fram det estetiska innehållet för VR, AR, MR som inkluderar video, grafik, bild och ljud. Programmet som sträcker sig över en period på 1,5 år ger dig kunskaper inom innovativ teknik för att på bästa sätt kunna producera immersiva upplevelser, kreationer och komplexa narrativ för en internationellt växande marknad.  Inom de olika kurserna lär du dig att analysera, utvärdera och producera projekt inom  VR/AR/MR och andra immersiva medier av hög kvalitet genom att se till teknisk  utformning, innehåll, formspråk, visuella trender, och målgrupp. Teoretiska kunskaper  blandas med praktiska övningar inom storytelling, berättande, manus och narrativ för  immersiva medier. Stort fokus ligger också på att införskaffa kunskaper inom den  ständigt växande forskningssektorn, mediets nya möjligheter inom utvecklingen av  både mjukvara och hårdvara, samt mottagarens perception av dessa. Vi eftersträvar att alla studenter som avslutar utbildningen har en gedigen kunskap i att analysera resultat inom mediet för att kunna avgöra om de motsvarar de krav som slutanvändaren och kunden ställer. Detta innefattar också kunskaper inom strategi för produktion av immersiva upplevelser som direkt är kopplat till målgrupp och affärsnyttan. För att lyckas som kreatör inom branschen behövs också kunskaper inom hur man pitchar idéer, skapar visuella presentationer, avklarar kundmöten och genomför workshops.  Programmet ger dig också viktiga kunskaper inom olika tekniska format, uppspelning,  leverans och lagring av immersiva produktioner. Dessutom erbjuder vi dig en nära  kontakt till branschen, ett internationellt perspektiv och möjligt att kommunicera mot  en globalt växande marknad.    Vi söker dig som vill, vågar och brinner för att berätta historier på ett nytt sätt,  genom banbrytande innehåll för den växande immersiva världen!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... medier?    Inom utbildningen **Immersive Experience Creator** lär du dig att regissera, ska... | x |  |  | 28 | [Grafisk teknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6h9K_ehL_YSK) |
| 2 | ...rience Creator lär du dig att **regissera**, skapa berättande och ta fram... | x |  |  | 9 | [tekniker för skådespeleri och regissering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zSiN_tCC_TdP) |
| 3 | ...tor lär du dig att regissera, **skapa berättande** och ta fram det estetiska inn... | x |  |  | 16 | [skapa animerade berättelser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r7gp_rnX_274) |
| 4 | ... det estetiska innehållet för **VR**, AR, MR som inkluderar video,... | x |  |  | 2 | [Virtual reality/VR, **skill**](http://data.jobtechdev.se/taxonomy/concept/NfDv_1zP_NsD) |
| 5 | ... estetiska innehållet för VR, **AR**, MR som inkluderar video, gra... | x |  |  | 2 | [Augmented reality/AR, **skill**](http://data.jobtechdev.se/taxonomy/concept/hY1R_jKB_zNr) |
| 6 | ...för VR, AR, MR som inkluderar **video**, grafik, bild och ljud. Progr... | x |  |  | 5 | [Videoredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/QBAx_kfV_k7Q) |
| 7 | ... AR, MR som inkluderar video, **grafik**, bild och ljud. Programmet so... | x |  |  | 6 | [Digital grafik, **skill**](http://data.jobtechdev.se/taxonomy/concept/RBEr_gJP_ECq) |
| 8 | ...som inkluderar video, grafik, **bild** och ljud. Programmet som strä... | x |  |  | 4 | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
| 9 | ...derar video, grafik, bild och **ljud**. Programmet som sträcker sig ... | x |  |  | 4 | [Ljudredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/Aj1W_rmu_niY) |
| 10 | ...a och producera projekt inom  **VR**/AR/MR och andra immersiva med... | x |  |  | 2 | [Virtual reality/VR, **skill**](http://data.jobtechdev.se/taxonomy/concept/NfDv_1zP_NsD) |
| 11 | ...ch producera projekt inom  VR/**AR**/MR och andra immersiva medier... | x |  |  | 2 | [Augmented reality/AR, **skill**](http://data.jobtechdev.se/taxonomy/concept/hY1R_jKB_zNr) |
| 12 | ...nom storytelling, berättande, **manus** och narrativ för  immersiva m... |  | x |  | 5 | [Manus, **skill**](http://data.jobtechdev.se/taxonomy/concept/Rz5y_53L_7Tk) |
| 13 | ...klingen av  både mjukvara och **hårdvara**, samt mottagarens perception ... |  | x |  | 8 | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
| | **Overall** | | | **0** | **93** | 0/93 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Hårdvara, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/22jK_Hje_3Ec) |
| x |  |  | [Grafisk teknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6h9K_ehL_YSK) |
| x |  |  | [Ljudredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/Aj1W_rmu_niY) |
| x |  |  | [Virtual reality/VR, **skill**](http://data.jobtechdev.se/taxonomy/concept/NfDv_1zP_NsD) |
| x |  |  | [Videoredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/QBAx_kfV_k7Q) |
| x |  |  | [Digital grafik, **skill**](http://data.jobtechdev.se/taxonomy/concept/RBEr_gJP_ECq) |
|  | x |  | [Manus, **skill**](http://data.jobtechdev.se/taxonomy/concept/Rz5y_53L_7Tk) |
| x |  |  | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
| x |  |  | [Augmented reality/AR, **skill**](http://data.jobtechdev.se/taxonomy/concept/hY1R_jKB_zNr) |
| x |  |  | [skapa animerade berättelser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/r7gp_rnX_274) |
| x |  |  | [tekniker för skådespeleri och regissering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zSiN_tCC_TdP) |
| | | **0** | 0/11 = **0%** |