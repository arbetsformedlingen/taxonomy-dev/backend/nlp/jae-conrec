# Results for '2572378814659b4b919f4015904ec46d4c03e830'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2572378814659b4b919f4015904ec46d4c03e830](README.md) | 1 | 1454 | 4 | 3 | 52/60 = **87%** | 2/3 = **67%** |

## Source text

Servicefokuserade restaurangmedarbetare Vi du skapa upplevelser? Vill du se dina kunder njuta av din service och vår goda mat?  Är du en utåtriktad och positiv ”people person” som gärna hugger i och gillar att ge bra service? Vill du göra karriär? Ha kul på jobbet! Nu har du chansen - sök jobbet idag!  Vi på Burger King söker just nu restaurangmedarbetare  Du blir vårt ansikte utåt i vår restaurang och får ett mångsidigt jobb som innebär att du roterar mellan olika arbetsuppgifter. Du kommer att få arbeta både i köket, matsalen och med att servera gäster.  Vad vi erbjuder: Burger King är ett av världens största restaurangföretag. Och självklart är engagerade, motiverade och trevliga medarbetare nyckeln till vår framgång. För oss är det viktigt att våra medarbetare får chansen att utvecklas. Vi erbjuder därför en tydlig karriärstege och goda utvecklingsmöjligheter. Tanken med våra utbildningar är att du ska växa med ansvar och utvecklas i din yrkesroll under hela din tid hos oss.  Vem är du? För att lyckas i jobbet tror vi att du har en utpräglad servicekänsla och tycker att det är roligt att arbeta med människor.  Låter det som något för dig? Sök jobbet Nu!  Burger King är ett av världens största restaurangföretag. För oss är självklart engagerade, motiverade och trevliga medarbetare nyckeln till vår framgång. Därför värderar vi vår personal högt och erbjuder en tydlig karriärstege med fortbildning och goda utvecklingsmöjligheter.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Servicefokuserade **restaurangmedarbetare** Vi du skapa upplevelser? Vill... | x | x | 21 | 21 | [Restaurangpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZYjR_db3_jN5) |
| 2 | ... på Burger King söker just nu **restaurangmedarbetare**  Du blir vårt ansikte utåt i ... | x | x | 21 | 21 | [Restaurangpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZYjR_db3_jN5) |
| 3 | ... blir vårt ansikte utåt i vår **restaurang** och får ett mångsidigt jobb s... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 4 | ...mgång. Därför värderar vi vår **personal** högt och erbjuder en tydlig k... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| | **Overall** | | | **52** | **60** | 52/60 = **87%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x | x | x | [Restaurangpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZYjR_db3_jN5) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| | | **2** | 2/3 = **67%** |