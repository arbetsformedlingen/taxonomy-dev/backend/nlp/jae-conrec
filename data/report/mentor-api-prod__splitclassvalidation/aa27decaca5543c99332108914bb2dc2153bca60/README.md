# Results for 'aa27decaca5543c99332108914bb2dc2153bca60'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [aa27decaca5543c99332108914bb2dc2153bca60](README.md) | 1 | 2969 | 31 | 18 | 75/876 = **9%** | 5/24 = **21%** |

## Source text

Vill du arbeta med att göra vardagen mer aktiv och meningsfull för såväl äldre personer som personer i behov av rehabilitering    I vår unika yrkeshögskoleutbildning lär du dig metoder och synsätt som bidrar till utveckling för både individ och verksamhet. Under YH-utbildningen finns det många tillfällen för reflektion och dialog. De här tillfällena har betydelse för utvecklingen av dina personliga egenskaper som är viktiga för din kommande yrkesroll.    SNABBFAKTA  Examen Yrkeshögskoleexamen  Start 13 september 2021  Längd 300 YH-poäng (2 år)  Studietakt 75%, bunden, skolförlagd 3 dagar/vecka   Studieort Mölndal  Antal platser 20 st  LIA 23 veckor (praktik)    UNDER STUDIETIDEN  I vår unika yrkeshögskoleutbildning lär du om metoder och synsätt som bidrar till utveckling för individ och verksamhet. Utbildningen innehåller många tillfällen till reflektion och dialog. Dessa tillfällen har betydelse för utveckling av dina personliga egenskaper som är viktiga för din kommande yrkesroll.    Under utbildningen fördjupar du din kunskap om rehabilitering och om den äldre människans livsvillkor. Du utvecklar kompetens inom aktivering, kommunikation, träning och stimulans ur ett hälsofrämjande perspektiv. Du lär dig att använda skapande, hantverk, rörelse, natur och kultur som medel i rehabiliteringsarbete. Under utbildningen fördjupar du din kunskap om dokumentation och presentation.    Du utvecklar också ditt professionella förhållningssätt som grund för att kvalitetsutveckla och pedagogiskt vägleda vardagsarbete inom exempelvis dagverksamhet, träffpunkter, olika typer av bostäder och rehabiliterande verksamheter inom kommun och region. Yrkeshögskoleutbildningen genomförs i nära samarbete med arbetslivet.    EFTER UTBILDNINGEN  Efter avslutad utbildning kan du arbeta till exempel som aktiveringspedagog, arbetsterapibiträde, rehabiliteringsassistent, äldresamordnare/äldrekonsulent eller stödpedagog.    KURSER SOM DU LÄSER (YH-poäng*):  *5 YH-poäng motsvarar 1 veckas helstudier    Utbildningen är uppbyggd av tre delar/block    Block 1   - Förutsättningar för yrkesrollen, 70 YH-poäng  - Människans åldrande, hälsa och funktion i aktivitet, 20 YH-poäng  - Professionellt, etiskt och hälsofrämjande förhållningssätt, 30 YH-poäng  - LIA 1 Den aktiva och skapande människan i samspel med andra, 20 YH-poäng    Block 2  - Aktivitet, kommunikation, anpassningar, 140 YH-poäng  - Vardagsaktivitet, aktivitetshinder och hälsofrämjande arbete, 25 YH-poäng  - Tillgänglighet, delaktighet och välfärdsteknik och dokumentation, 25 YH-poäng  - Kommunikation kultur och samspel, 25 YH-poäng  - Kreativitet, skapande och hantverk, 25 YH-poäng  - LIA 2 Vardagsaktivitet, kommunikation, tillgänglighet och dokumentation, 40 YH-poäng    Block 3   - Vardagsarbete, vägledning, lärande, 90 YH-poäng  - Ett vägledande förhållningssätt och evidensbaserad praktik, 40 YH-poäng  - LIA 3 Utveckling för evidensbaserad praktik, 25 YH-poäng  - Examensarbete, 25 YH-poäng

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...soner som personer i behov av **rehabilitering**    I vår unika yrkeshögskoleu... |  | x |  | 14 | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| 2 | ...nde yrkesroll.    SNABBFAKTA  **Examen** Yrkeshögskoleexamen  Start 13... | x |  |  | 6 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 3 | ...esroll.    SNABBFAKTA  Examen **Yrkeshögskoleexamen**  Start 13 september 2021  Län... | x |  |  | 19 | [Högskoleutbildning, yrkesinriktad, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/zdKB_pTL_HMS) |
| 4 | ...agd 3 dagar/vecka   Studieort **Mölndal**  Antal platser 20 st  LIA 23 ... | x | x | 7 | 7 | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
| 5 | ...n fördjupar du din kunskap om **rehabilitering** och om den äldre människans l... |  | x |  | 14 | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| 6 | ...ar kompetens inom aktivering, **kommunikation**, träning och stimulans ur ett... | x |  |  | 13 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 7 | ...om aktivering, kommunikation, **träning** och stimulans ur ett hälsofrä... | x |  |  | 7 | [organisera träning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xn1f_njL_MMU) |
| 8 | ...lär dig att använda skapande, **hantverk**, rörelse, natur och kultur so... |  | x |  | 8 | [Hantverk, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/PaxQ_o1G_wWH) |
| 9 | ... natur och kultur som medel i **rehabiliteringsarbete**. Under utbildningen fördjupar... | x |  |  | 21 | [Rehabilitering, **skill**](http://data.jobtechdev.se/taxonomy/concept/aoVs_Qfu_U3j) |
| 10 | ...n fördjupar du din kunskap om **dokumentation** och presentation.    Du utvec... |  | x |  | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 11 | ...n fördjupar du din kunskap om **dokumentation och presentation**.    Du utvecklar också ditt p... | x |  |  | 30 | [dokumentation inom hälso- och sjukvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2hA5_AC8_bsP) |
| 12 | ...llningssätt som grund för att **kvalitetsutveckla** och pedagogiskt vägleda varda... | x |  |  | 17 | [Kvalitetsutveckling, omsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/gtVT_cXB_d7f) |
| 13 | ...för att kvalitetsutveckla och **pedagogiskt vägleda vardagsarbete** inom exempelvis dagverksamhet... | x |  |  | 33 | [Pedagogiskt förhållningssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/qMfa_U8e_Lto) |
| 14 | ...an du arbeta till exempel som **aktiveringspedagog**, arbetsterapibiträde, rehabil... | x |  |  | 18 | [Pedagogik, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/MVqp_eS8_kDZ) |
| 14 | ...an du arbeta till exempel som **aktiveringspedagog**, arbetsterapibiträde, rehabil... |  | x |  | 18 | [Aktiveringspedagog, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jWh3_qhF_c1x) |
| 15 | ...empel som aktiveringspedagog, **arbetsterapibiträde**, rehabiliteringsassistent, äl... |  | x |  | 19 | [arbetsterapibiträde, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/aukF_pgC_EPE) |
| 15 | ...empel som aktiveringspedagog, **arbetsterapibiträde**, rehabiliteringsassistent, äl... | x | x | 19 | 19 | [Arbetsterapibiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yiFS_2Zq_NzF) |
| 16 | ...pedagog, arbetsterapibiträde, **rehabiliteringsassistent**, äldresamordnare/äldrekonsule... | x | x | 24 | 24 | [Rehabiliteringsassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Qgk2_se1_PZR) |
| 17 | ...de, rehabiliteringsassistent, **äldresamordnare/äldrekonsulent** eller stödpedagog.    KURSER ... | x |  |  | 30 | [Omsorgsyrken, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/2U3x_Aub_tg1) |
| 18 | ...ngsassistent, äldresamordnare/**äldrekonsulent** eller stödpedagog.    KURSER ... |  | x |  | 14 | [Äldrekonsulent, **job-title**](http://data.jobtechdev.se/taxonomy/concept/28Qp_gxp_omp) |
| 19 | ...mordnare/äldrekonsulent eller **stödpedagog**.    KURSER SOM DU LÄSER (YH-p... | x | x | 11 | 11 | [Stödpedagog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NSEG_DmQ_waj) |
| 20 | ...re delar/block    Block 1   - **Förutsättningar för yrkesrollen**, 70 YH-poäng  - Människans ål... | x |  |  | 31 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 21 | ...r yrkesrollen, 70 YH-poäng  - **Människans åldrande, hälsa och funktion i aktivitet**, 20 YH-poäng  - Professionell... | x |  |  | 51 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 22 | ...poäng  - Människans åldrande, **hälsa** och funktion i aktivitet, 20 ... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 23 | ...n i aktivitet, 20 YH-poäng  - **Professionellt, etiskt och hälsofrämjande förhållningssätt**, 30 YH-poäng  - LIA 1 Den akt... | x |  |  | 58 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 24 | ...ra, 20 YH-poäng    Block 2  - **Aktivitet, kommunikation, anpassningar**, 140 YH-poäng  - Vardagsaktiv... | x |  |  | 38 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 25 | ...anpassningar, 140 YH-poäng  - **Vardagsaktivitet, aktivitetshinder och hälsofrämjande arbete**, 25 YH-poäng  - Tillgänglighe... | x |  |  | 60 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 26 | ...mjande arbete, 25 YH-poäng  - **Tillgänglighet, delaktighet och **välfärdsteknik och dokumentati... | x |  |  | 32 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 27 | ...llgänglighet, delaktighet och **välfärdsteknik** och dokumentation, 25 YH-poän... | x | x | 14 | 14 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 28 | ...delaktighet och välfärdsteknik** och dokumentation**, 25 YH-poäng  - Kommunikation... | x |  |  | 18 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 29 | ...tighet och välfärdsteknik och **dokumentation**, 25 YH-poäng  - Kommunikation... |  | x |  | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 30 | ...dokumentation, 25 YH-poäng  - **Kommunikation kultur och samspel**, 25 YH-poäng  - Kreativitet, ... | x |  |  | 32 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 31 | ...r och samspel, 25 YH-poäng  - **Kreativitet, skapande och hantverk**, 25 YH-poäng  - LIA 2 Vardags... | x |  |  | 34 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 32 | ...  - Kreativitet, skapande och **hantverk**, 25 YH-poäng  - LIA 2 Vardags... |  | x |  | 8 | [Hantverk, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/PaxQ_o1G_wWH) |
| 33 | ...n, 40 YH-poäng    Block 3   - **Vardagsarbete, vägledning, lärande**, 90 YH-poäng  - Ett vägledand... | x |  |  | 34 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 34 | ..., lärande, 90 YH-poäng  - Ett **vägledande förhållningssätt** och evidensbaserad praktik, 4... | x |  |  | 27 | [Vägledande arbetssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/a8dY_8nt_E87) |
| 35 | ...gledande förhållningssätt och **evidensbaserad praktik**, 40 YH-poäng  - LIA 3 Utveckl... |  | x |  | 22 | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| 35 | ...gledande förhållningssätt och **evidensbaserad praktik**, 40 YH-poäng  - LIA 3 Utveckl... | x |  |  | 22 | [Evidensbaserad praktik, **skill**](http://data.jobtechdev.se/taxonomy/concept/vzoU_stw_Utd) |
| 36 | ...poäng  - LIA 3 Utveckling för **evidensbaserad praktik**, 25 YH-poäng  - Examensarbete... | x |  |  | 22 | [Evidensbaserad praktik, **skill**](http://data.jobtechdev.se/taxonomy/concept/vzoU_stw_Utd) |
| | **Overall** | | | **75** | **876** | 75/876 = **9%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Äldrekonsulent, **job-title**](http://data.jobtechdev.se/taxonomy/concept/28Qp_gxp_omp) |
| x | x | x | [Annan utbildning inom omvårdnad, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2K8B_GSo_Rbw) |
| x |  |  | [Omsorgsyrken, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/2U3x_Aub_tg1) |
| x |  |  | [dokumentation inom hälso- och sjukvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2hA5_AC8_bsP) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| x |  |  | [Pedagogik, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/MVqp_eS8_kDZ) |
| x | x | x | [Stödpedagog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NSEG_DmQ_waj) |
|  | x |  | [Hantverk, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/PaxQ_o1G_wWH) |
| x | x | x | [Rehabiliteringsassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Qgk2_se1_PZR) |
| x |  |  | [Vägledande arbetssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/a8dY_8nt_E87) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x |  |  | [Rehabilitering, **skill**](http://data.jobtechdev.se/taxonomy/concept/aoVs_Qfu_U3j) |
|  | x |  | [arbetsterapibiträde, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/aukF_pgC_EPE) |
| x |  |  | [Kvalitetsutveckling, omsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/gtVT_cXB_d7f) |
|  | x |  | [Aktiveringspedagog, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jWh3_qhF_c1x) |
|  | x |  | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x | x | x | [Mölndal, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mc45_ki9_Bv3) |
|  | x |  | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| x |  |  | [Pedagogiskt förhållningssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/qMfa_U8e_Lto) |
| x |  |  | [Evidensbaserad praktik, **skill**](http://data.jobtechdev.se/taxonomy/concept/vzoU_stw_Utd) |
| x |  |  | [organisera träning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xn1f_njL_MMU) |
| x | x | x | [Arbetsterapibiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yiFS_2Zq_NzF) |
| x |  |  | [Högskoleutbildning, yrkesinriktad, 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/zdKB_pTL_HMS) |
| | | **5** | 5/24 = **21%** |