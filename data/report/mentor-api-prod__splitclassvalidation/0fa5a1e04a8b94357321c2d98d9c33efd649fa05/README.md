# Results for '0fa5a1e04a8b94357321c2d98d9c33efd649fa05'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0fa5a1e04a8b94357321c2d98d9c33efd649fa05](README.md) | 1 | 2315 | 9 | 5 | 27/188 = **14%** | 2/10 = **20%** |

## Source text

Kundansvarig rekryterare iobb är ett samhällsnyttigt rekryteringsföretag som verkar för att fler arbetssökande ska komma in på arbetsmarknaden genom socialt hållbara rekryteringar.  Vi erbjuder våra kunder en smidig rekryteringsprocess, social hållbarhet och affärsnytta i ett. Genom iobbs rekryteringsprocess får våra kunder hjälp att hitta rätt person och samtidigt är de med och stärker både individen och arbetsmarknaden.  Nu söker vi vår nästa stjärna till rekryteringsteamet! Är det du?  Alla iobbs kandidater står utanför arbetsmarknaden av olika anledningar. Vi möter dagligen nyanlända, ungdomar och långtidsarbetslösa. Med ambitionen att erbjuda våra kunder lönsamma och långsiktiga anställningar krävs noggrannhet, vinnarinstinkt och fingertoppskänsla. Vi söker dig som vill vara med på vår resa och delar vårt synsätt och tron på idéen att alla har en plats på arbetsmarknaden.  Vi söker dig som vill arbeta med rekrytering och alla moment som innefattas av det. Du är van vid och vill fortsatt träffa människor i ditt dagliga arbete. Du trivs med kandidat- och kundkontakt, administration och målinriktat arbete. Service och kundupplevelse är en stor del av den här tjänsten. Vidare är struktur a och o för att lyckas med uppdraget.  För att trivas hos oss behöver du:  - Kunna arbeta självständigt och strukturera upp din arbetsdag, - Kunna ansvara för dina processer från start till slut, - Ha vana att arbeta administrativt och tycka om målinriktad arbete.   Vi söker dig som:  - Har erfarenhet av rekryteringsbranschen, - Vill utvecklas och känner sig hemma i en miljö där mycket är på gång, - Har ett intresse för social hållbarhet och vill vara med och arbeta för ökad inkludering.   Det är meriterande med erfarenhet inom arbetsmarknadsområdet och från servicebranschen. Du kanske har arbetat med stöd och matchning, arbetsmarknadsförvaltning, bemanning eller som rekryterare.  I din ansökan vill vi har svar på varför du tror att du passar för den här rollen, vad du tror att du kommer tillföra och på vilket sätt du arbetat målinriktat tidigare.  Vi sitter i toppmoderna lokaler i centrala Sundbyberg. Det finns möjlighet till variation i att arbeta på kontoret och distans. Krav på körkort då många av våra kunder ligger utanför stan. Intervjuer sker löpande så skicka din ansökan redan idag!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Kundansvarig **rekryterare** iobb är ett samhällsnyttigt r... | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| 2 | ...n smidig rekryteringsprocess, **social hållbarhet** och affärsnytta i ett. Genom ... |  | x |  | 17 | [Social hållbarhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/9MoM_m8y_Em5) |
| 3 | ...ngsiktiga anställningar krävs **noggrannhet**, vinnarinstinkt och fingertop... |  | x |  | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 4 | ... hos oss behöver du:  - Kunna **arbeta självständigt** och strukturera upp din arbet... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 5 | ...tart till slut, - Ha vana att **arbeta administrativt** och tycka om målinriktad arbe... | x |  |  | 21 | [utföra administrativt arbete, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2x13_sLi_EzG) |
| 6 | ...e.   Vi söker dig som:  - Har **erfarenhet av rekryteringsbranschen**, - Vill utvecklas och känner ... | x |  |  | 35 | [Rekryteringsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/cDU5_op5_Tuk) |
| 7 | ... gång, - Har ett intresse för **social hållbarhet** och vill vara med och arbeta ... |  | x |  | 17 | [Social hållbarhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/9MoM_m8y_Em5) |
| 8 | ...riterande med erfarenhet inom **arbetsmarknadsområdet** och från servicebranschen. Du... | x |  |  | 21 | [Arbetsmarknadsfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/gx8F_mte_Nm5) |
| 9 | ...valtning, bemanning eller som **rekryterare**.  I din ansökan vill vi har s... | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| 10 | ...oppmoderna lokaler i centrala **Sundbyberg**. Det finns möjlighet till var... | x |  |  | 10 | [Sundbyberg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UTJZ_zHH_mJm) |
| 11 | ... i att arbeta på kontoret och **distans**. Krav på körkort då många av ... | x |  |  | 7 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 12 | ...kontoret och distans. Krav på **körkort** då många av våra kunder ligge... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| | **Overall** | | | **27** | **188** | 27/188 = **14%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [utföra administrativt arbete, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2x13_sLi_EzG) |
|  | x |  | [Social hållbarhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/9MoM_m8y_Em5) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| x |  |  | [Sundbyberg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UTJZ_zHH_mJm) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Rekryteringsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/cDU5_op5_Tuk) |
| x |  |  | [Arbetsmarknadsfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/gx8F_mte_Nm5) |
|  | x |  | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| | | **2** | 2/10 = **20%** |