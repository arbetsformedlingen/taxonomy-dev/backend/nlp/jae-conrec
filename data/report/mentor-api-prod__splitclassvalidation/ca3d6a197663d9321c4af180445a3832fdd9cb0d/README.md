# Results for 'ca3d6a197663d9321c4af180445a3832fdd9cb0d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ca3d6a197663d9321c4af180445a3832fdd9cb0d](README.md) | 1 | 4246 | 24 | 29 | 179/456 = **39%** | 15/29 = **52%** |

## Source text

Sjuksköterska natt. Vårdavdelning för hemlösa. Capio Psykiatri är en av Sveriges största privata vårdgivare inom psykiatrisk specialistvård, med verksamheter inom områdena allmänpsykiatri, specialiserad barn- och ungdomspsykiatrisk, beroendemedicin och ätstörningar. Vi finns i Stockholmsområdet, Östergötland, Örebro och Halland. Vi arbetar på uppdrag från regioner både på vårdavtal och vårdval.   Vi vänder oss till personer som är 18 år och äldre som lever i hemlöshet och är i behov av hälso- och sjukvårdsinsatser.  Målgruppen är i första hand personer som tillhör situation 1 enligt Socialstyrelsens definition av hemlöshet; en person som är hänvisad till akutboende, härbärge, jourboende eller är uteliggare.  I slutenvården är målgruppen personer folkbokförda i Stockholms län med somatiska sjukdomar och komplexa vårdbehov, oftast i kombination med beroende och/eller psykisk sjuklighet/funktionshinder. På vår vårdavdelning erbjuder vi medicinskt omhändertagande och omvårdnad till patienter som lever i hemlöshet. Avdelningen har 14 vårdplatser varav en är för patienter med palliativ vård.      Välkommen till heldygnsvården för hemlösa  Heldygnsvården för hemlösa är en slutenvårdsavdelning för patienter i hemlöshet.  Vi är belägna på Maria Sjukhus på Södermalm i Stockholm. På avdelningen arbetar undersköterskor, sjuksköterskor, läkare, vårdsamordnare, kurator, arbetsterapeut och fysioterapeut.  Patienter remitteras hit från akutsjukhus, geriatrik, eller från Pelarbacken, öppenvården för hemlösa.  Vanliga diagnoser och vårdbehov är bensår, diabetes, hjärtsvikt, kol, infektioner, cancer med kurativ eller palliativ behandling, minnessvikt eller rehabilitering efter skador och akuta operationer. De somatiska sjukdomarna och vårdbehov är många gånger i kombination med psykisk ohälsa och/eller beroendeproblematik.  Vi arbetar personcentrerat, skapar tillit, motiverar och strävar efter att tillgodose patientens vård och stöd utifrån ett helhetsperspektiv, ofta i samverkan med socialtjänst och andra vårdgivare.  Kvalifikationer Legitimerad sjuksköterska med erfarenhet eller intresse av att arbeta med människor i utsatta situationer.   Vi lägger stor vikt vid personlig lämplighet och ser gärna att du är flexibel och lösningsinriktad. Du har ett gott bemötande, en positiv grundinställning och en god förmåga att arbeta både självständigt och i grupp.  Vi erbjuder:  Ett givande arbete tillsammans med kollegor som är stolta över sitt arbete och den betydelse vår insats har för patienterna.  - Individuellt planerad introduktion  - Möjligheter att vara med och utveckla vården  - Friskvårdsbidrag på 3000 kr  - Ersättning för sjukvårdsbesök upp till högkostnadskort  - Fredagsfrukost  - Dubbelt nattob för sjuksköterskor  Övrig information Det finns möjlighet att hospitera.  Rekrytering och intervjuer sker löpande under pågående ansökningstid.  I vår verksamhet är det svenska språket ett viktigt redskap i arbetet med våra patienter. Vi strävar efter en jämn könsfördelning och ser mångfald som en styrka och välkomnar därför sökande med olika bakgrund.     Välkommen att söka tjänsten!        Capio – Nordens största privata vårdgivare Capios vision är att förbättra hälsan för människor varje dag. Vi vill göra skillnad för våra patienter genom att se, lyssna och finnas här – idag och genom hela livet. Våra medarbetare är viktiga för oss och vi vill ge dem kraften att finnas där för våra patienter men också för att utveckla vården i stort. I vårt dagliga arbete jobbar vi utifrån våra värderingar: Quality, Empowerment, Innovation och Social responsibility. De hjälper oss att uppfylla vårt patientlöfte om att vara en pålitlig partner – närvarande och nytänkande. Tillsammans förbättrar vi folkhälsan – en individ i taget!   Capio erbjuder ett brett och högkvalitativt vårdutbud genom våra sjukhus, specialistkliniker och vårdcentraler i Sverige, Norge och Danmark. Våra patienter kan även få hjälp via våra digitala tjänster. Capio är sedan 2018 en del av Ramsay Santé - en ledande vårdgivare med 36 000 medarbetare i sex länder. För mer information, besök https://capio.se/.   Vi undanber oss vänligt men bestämt all direktkontakt med bemannings- och rekryteringsföretag samt försäljare av jobbannonser.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sjuksköterska** natt. Vårdavdelning för hemlö... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 1 | **Sjuksköterska** natt. Vårdavdelning för hemlö... | x |  |  | 13 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 2 | Sjuksköterska natt. **Vårdavdelning** för hemlösa. Capio Psykiatri ... | x | x | 13 | 13 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 3 | ...davdelning för hemlösa. Capio **Psykiatri** är en av Sveriges största pri... | x | x | 9 | 9 | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| 4 | ...ösa. Capio Psykiatri är en av **Sveriges** största privata vårdgivare in... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 5 | ...a vårdgivare inom psykiatrisk **specialistvård**, med verksamheter inom område... |  | x |  | 14 | [specialistvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uXRf_eEH_Qwz) |
| 6 | ...kiatrisk, beroendemedicin och **ätstörningar**. Vi finns i Stockholmsområdet... | x | x | 12 | 12 | [ätstörningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LKeT_vxf_bPp) |
| 7 | ...Vi finns i Stockholmsområdet, **Östergötland**, Örebro och Halland. Vi arbet... |  | x |  | 12 | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
| 8 | ...ckholmsområdet, Östergötland, **Örebro** och Halland. Vi arbetar på up... |  | x |  | 6 | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
| 9 | ...det, Östergötland, Örebro och **Halland**. Vi arbetar på uppdrag från r... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 10 | ...i hemlöshet och är i behov av **hälso- och **sjukvårdsinsatser.  Målgruppen... | x |  |  | 11 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 11 | ... och är i behov av hälso- och **sjukvårdsinsatser**.  Målgruppen är i första hand... | x | x | 17 | 17 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 12 | ...uppen personer folkbokförda i **Stockholms län** med somatiska sjukdomar och k... | x | x | 14 | 14 | [Stockholms län, **region**](http://data.jobtechdev.se/taxonomy/concept/CifL_Rzy_Mku) |
| 13 | ...ighet/funktionshinder. På vår **vårdavdelning** erbjuder vi medicinskt omhänd... | x | x | 13 | 13 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 14 | ...edicinskt omhändertagande och **omvårdnad** till patienter som lever i he... |  | x |  | 9 | [Omvårdnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/MR4H_FFT_mTj) |
| 15 | ...varav en är för patienter med **palliativ vård**.      Välkommen till heldyg... | x | x | 14 | 14 | [Palliativ vård, **skill**](http://data.jobtechdev.se/taxonomy/concept/ryxY_Uwr_pkw) |
| 16 | ... Maria Sjukhus på Södermalm i **Stockholm**. På avdelningen arbetar under... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 17 | ...kholm. På avdelningen arbetar **undersköterskor**, sjuksköterskor, läkare, vård... | x | x | 15 | 15 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 18 | ...ngen arbetar undersköterskor, **sjuksköterskor**, läkare, vårdsamordnare, kura... |  | x |  | 14 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 18 | ...ngen arbetar undersköterskor, **sjuksköterskor**, läkare, vårdsamordnare, kura... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 19 | ...ersköterskor, sjuksköterskor, **läkare**, vårdsamordnare, kurator, arb... | x | x | 6 | 6 | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
| 20 | ...skor, sjuksköterskor, läkare, **vårdsamordnare**, kurator, arbetsterapeut och ... | x |  |  | 14 | [Vårdsamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1q9Y_LGj_Up6) |
| 21 | ...skor, läkare, vårdsamordnare, **kurator**, arbetsterapeut och fysiotera... | x | x | 7 | 7 | [Kurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YpRs_ybt_47a) |
| 22 | ...are, vårdsamordnare, kurator, **arbetsterapeut** och fysioterapeut.  Patienter... | x | x | 14 | 14 | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| 23 | ..., kurator, arbetsterapeut och **fysioterapeut**.  Patienter remitteras hit fr... | x | x | 13 | 13 | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| 23 | ..., kurator, arbetsterapeut och **fysioterapeut**.  Patienter remitteras hit fr... |  | x |  | 13 | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| 24 | ...itteras hit från akutsjukhus, **geriatrik**, eller från Pelarbacken, öppe... | x | x | 9 | 9 | [Geriatrik, **skill**](http://data.jobtechdev.se/taxonomy/concept/zi7e_S96_W9k) |
| 25 | ...oser och vårdbehov är bensår, **diabetes**, hjärtsvikt, kol, infektioner... |  | x |  | 8 | [Diabetesvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/GkJU_4dd_Cit) |
| 26 | ...behandling, minnessvikt eller **rehabilitering** efter skador och akuta operat... | x | x | 14 | 14 | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| 27 | ... med psykisk ohälsa och/eller **beroendeproblematik**.  Vi arbetar personcentrerat,... |  | x |  | 19 | [Annan utbildning inom socialt arbete, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/kCNc_iUC_hh4) |
| 28 | ...  Kvalifikationer Legitimerad **sjuksköterska** med erfarenhet eller intresse... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 28 | ...  Kvalifikationer Legitimerad **sjuksköterska** med erfarenhet eller intresse... | x |  |  | 13 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 29 | ...llning och en god förmåga att **arbeta** både självständigt och i grup... | x |  |  | 6 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 30 | ...n god förmåga att arbeta både **självständigt** och i grupp.  Vi erbjuder:  E... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 31 | ...frukost  - Dubbelt nattob för **sjuksköterskor**  Övrig information Det finns ... |  | x |  | 14 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 31 | ...frukost  - Dubbelt nattob för **sjuksköterskor**  Övrig information Det finns ... | x |  |  | 14 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 32 | ...tid.  I vår verksamhet är det **svenska** språket ett viktigt redskap i... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 33 | ...tid.  I vår verksamhet är det **svenska språket** ett viktigt redskap i arbetet... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 34 | ...tkliniker och vårdcentraler i **Sverige**, Norge och Danmark. Våra pati... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **179** | **456** | 179/456 = **39%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Vårdsamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1q9Y_LGj_Up6) |
| x | x | x | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| x | x | x | [Stockholms län, **region**](http://data.jobtechdev.se/taxonomy/concept/CifL_Rzy_Mku) |
|  | x |  | [Diabetesvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/GkJU_4dd_Cit) |
| x | x | x | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| x | x | x | [ätstörningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LKeT_vxf_bPp) |
|  | x |  | [Omvårdnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/MR4H_FFT_mTj) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| x | x | x | [Kurator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/YpRs_ybt_47a) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| x | x | x | [Arbetsterapeut, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/heGV_uHh_o8W) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
|  | x |  | [Annan utbildning inom socialt arbete, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/kCNc_iUC_hh4) |
|  | x |  | [Örebro, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kuMn_feU_hXx) |
|  | x |  | [Östergötlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/oLT3_Q9p_3nn) |
| x | x | x | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| x | x | x | [Fysioterapeut/Sjukgymnast, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/raVz_oTw_N3e) |
| x | x | x | [Palliativ vård, **skill**](http://data.jobtechdev.se/taxonomy/concept/ryxY_Uwr_pkw) |
|  | x |  | [Fysioterapeuter och sjukgymnaster, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/s7vU_FhY_L5Z) |
| x | x | x | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| x | x | x | [läkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/snUT_QSf_LG9) |
|  | x |  | [specialistvård, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uXRf_eEH_Qwz) |
|  | x |  | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [Geriatrik, **skill**](http://data.jobtechdev.se/taxonomy/concept/zi7e_S96_W9k) |
| | | **15** | 15/29 = **52%** |