# Results for '9f9956fb8779768c6e3d5d5d91841c8d0f2cf196'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9f9956fb8779768c6e3d5d5d91841c8d0f2cf196](README.md) | 1 | 1219 | 9 | 7 | 36/99 = **36%** | 3/10 = **30%** |

## Source text

Servitör/Servitris sökes Just nu söker vi förstärkning till vårt matsalsteam! Rice är en familjär restaurang belägen på en av Stockholms mest attraktiva och centrala adresser. Här är en mötesplats med en avslappnad och varm känsla där gäster kan avnjuta en unik, utsökt och oförglömlig asiatisk upplevelse. Vår meny med inspiration från olika delar av Asien såsom Kina, Japan, Thailand och Vietnam återspeglar århundraden av matlagning. Så hos oss är det väldigt viktigt att man som anställd har ett genuint intresse av att vilja förstå och lära sig mycket om den asiatiska matkulturen. Rice är en av Stockholm äldsta asiatiska restauranger. Alla som jobbar här är en del av familjen Rice, så tanken är att arbetsplatsen ska kännas som ett andra hem för oss som jobbar här och det är väldigt viktigt för oss att vi hjälper varandra och visar varandra respekt och välvilja. Älskar du att träffa nya människor och precis som vi blir lyckliga av att få ge gästerna en förstklassig service och en upplevelse som gör de lyckliga? Är du utåtriktad och positiv? Kan hålla ett högt tempo? För vi har hög beläggning så hos oss går det undan. Passar du in på beskrivningen? Kul! Sök direkt. Vi ser fram emot att läsa din ansökan.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Servitör**/Servitris sökes Just nu söker... | x | x | 8 | 8 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | Servitör**/**Servitris sökes Just nu söker ... | x |  |  | 1 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 3 | Servitör/**Servitris** sökes Just nu söker vi förstä... | x | x | 9 | 9 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 4 | ...salsteam! Rice är en familjär **restaurang** belägen på en av Stockholms m... | x | x | 10 | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 5 | ...r restaurang belägen på en av **Stockholms** mest attraktiva och centrala ... | x |  |  | 10 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 6 | ...ån olika delar av Asien såsom **Kina**, Japan, Thailand och Vietnam ... | x |  |  | 4 | [Kinesiskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZMdB_i8G_L77) |
| 6 | ...ån olika delar av Asien såsom **Kina**, Japan, Thailand och Vietnam ... |  | x |  | 4 | [Kina, **country**](http://data.jobtechdev.se/taxonomy/concept/hwyG_7pq_yHR) |
| 7 | ...ka delar av Asien såsom Kina, **Japan**, Thailand och Vietnam återspe... |  | x |  | 5 | [Japan, **country**](http://data.jobtechdev.se/taxonomy/concept/DBob_NQg_ayR) |
| 7 | ...ka delar av Asien såsom Kina, **Japan**, Thailand och Vietnam återspe... | x |  |  | 5 | [Japanskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/EvAm_oco_RcM) |
| 8 | ...r av Asien såsom Kina, Japan, **Thailand** och Vietnam återspeglar århun... | x |  |  | 8 | [Thailändskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/zJFj_sZ8_z4E) |
| 9 | ...som Kina, Japan, Thailand och **Vietnam** återspeglar århundraden av ma... | x |  |  | 7 | [Vietnam, **country**](http://data.jobtechdev.se/taxonomy/concept/7RXF_Wx9_mfY) |
| 9 | ...som Kina, Japan, Thailand och **Vietnam** återspeglar århundraden av ma... |  | x |  | 7 | [Vietnamesiska, **language**](http://data.jobtechdev.se/taxonomy/concept/RqHU_aEE_tJA) |
| 10 | ...ka matkulturen. Rice är en av **Stockholm** äldsta asiatiska restauranger... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 11 | ...av Stockholm äldsta asiatiska **restauranger**. Alla som jobbar här är en de... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| | **Overall** | | | **36** | **99** | 36/99 = **36%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Vietnam, **country**](http://data.jobtechdev.se/taxonomy/concept/7RXF_Wx9_mfY) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [Japan, **country**](http://data.jobtechdev.se/taxonomy/concept/DBob_NQg_ayR) |
| x |  |  | [Japanskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/EvAm_oco_RcM) |
|  | x |  | [Vietnamesiska, **language**](http://data.jobtechdev.se/taxonomy/concept/RqHU_aEE_tJA) |
| x |  |  | [Kinesiskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZMdB_i8G_L77) |
|  | x |  | [Kina, **country**](http://data.jobtechdev.se/taxonomy/concept/hwyG_7pq_yHR) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x |  |  | [Thailändskt kök, **skill**](http://data.jobtechdev.se/taxonomy/concept/zJFj_sZ8_z4E) |
| | | **3** | 3/10 = **30%** |