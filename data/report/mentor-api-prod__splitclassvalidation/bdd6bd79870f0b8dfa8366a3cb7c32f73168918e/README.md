# Results for 'bdd6bd79870f0b8dfa8366a3cb7c32f73168918e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [bdd6bd79870f0b8dfa8366a3cb7c32f73168918e](README.md) | 1 | 707 | 6 | 5 | 48/89 = **54%** | 5/6 = **83%** |

## Source text

Nagelteknolog sökes Hej,  Vi söker nu en ny medarbetare till våran nagelsalong Viet Beauty som ligger centralt belägen i Göteborg. Vi eftersträvar att göra alla våra kunder så nöjda som möjligt. Vi har lång och god erfarenhet inom branschen. Du skall arbeta med de moment som ingår i yrket. Det innebär bland annat att du skall arbeta med naglar och både använda dig av gel och akryl. Du skall utföra manikyr och pedikyr. Du skall hålla ordning och reda på din arbetsplats. Vara tillmötesgående och serviceinriktad gentemot kunderna. Du skall ha erfarenhet eller motsvarande utbildning inom yrket. Du skall vara lätt lärd för nya moment och kunskaper inom branschen.  Att kunna Vietnamesiska är meriterande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Nagelteknolog** sökes Hej,  Vi söker nu en ny... | x | x | 13 | 13 | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| 2 | ...som ligger centralt belägen i **Göteborg**. Vi eftersträvar att göra all... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 3 | ...el och akryl. Du skall utföra **manikyr** och pedikyr. Du skall hålla o... | x | x | 7 | 7 | [Handvård/Manikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/RVLi_2no_Eyg) |
| 4 | .... Du skall utföra manikyr och **pedikyr**. Du skall hålla ordning och r... | x | x | 7 | 7 | [Fotvård/Pedikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/5APZ_gLs_AB4) |
| 5 | ...manikyr och pedikyr. Du skall **hålla ordning och reda på din arbetsplats**. Vara tillmötesgående och ser... | x |  |  | 41 | [hålla arbetsområdet rent, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6Kri_s2M_pdm) |
| 6 | ...er inom branschen.  Att kunna **Vietnamesiska** är meriterande. | x | x | 13 | 13 | [Vietnamesiska, **language**](http://data.jobtechdev.se/taxonomy/concept/RqHU_aEE_tJA) |
| | **Overall** | | | **48** | **89** | 48/89 = **54%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Fotvård/Pedikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/5APZ_gLs_AB4) |
| x |  |  | [hålla arbetsområdet rent, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/6Kri_s2M_pdm) |
| x | x | x | [Nagelterapeut/Nagelteknolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JjXV_QEg_EQ1) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Handvård/Manikyr, **skill**](http://data.jobtechdev.se/taxonomy/concept/RVLi_2no_Eyg) |
| x | x | x | [Vietnamesiska, **language**](http://data.jobtechdev.se/taxonomy/concept/RqHU_aEE_tJA) |
| | | **5** | 5/6 = **83%** |