# Results for '3f9131e596d8ca16404932d7a9e322cf9cadd6ff'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [3f9131e596d8ca16404932d7a9e322cf9cadd6ff](README.md) | 1 | 3543 | 32 | 36 | 196/482 = **41%** | 14/26 = **54%** |

## Source text

Supporttekniker till Briljant i Norrköping Nu finns chansen för dig som vill bli en del av ett starkt och kompetent utvecklingsbolag med huvudkontor i Norrköping. Som Supporttekniker hos Briljant blir du en del av det team som arbetar med teknisk support av Briljants molntjänst. Rollen passar både dig som är i starten av din karriär eller dig som har några års erfarenhet inom teknisk support. Här får du möjlighet att ta ett stort eget ansvar och utvecklas i din roll!  OM TJÄNSTEN  Briljant Ekonomisystem AB äger och utvecklar Briljant Ekonomisystem. Bakom produkten står ett utvecklingsbolag samt åtta fristående återförsäljarbolag. Briljant växer och välkomnar nu ytterligare en kollega till avdelningen för Briljants molntjänst. Briljant ASP är en molnbaserad version av Briljant Ekonomisystem som idag har ca 6400 användare. Du blir en del av teamet som sköter support, drift och administration av ASP. Teamet, som präglas av gemenskap och tätt samarbete, består idag av 3 Supporttekniker med stor kompetens inom området som kommer att stötta dig i din introduktion och i arbetet.  Såhär beskriver Jack varför han trivs i rollen som Supporttekniker:  ”Jag är både 1st, 2nd och 3rd linesupport vilket gör rollen varierande och utvecklande. Vi är ett litet bolag med härlig stämning där alla jobbar för att utveckla Briljant tillsammans. Vi har också trevliga kunder som vi har en bra relation med vilket gör jobbet extra roligt.”  #  Du erbjuds   * Ett utvecklande och utmanande arbete med mycket eget ansvar * Goda möjligheter till kompetensutveckling och att fördjupa din tekniska kompetens inom områden som du tycker är spännande * Arbete i en organisation där alla, oavsett roll, bidrar till att utveckla ekonomisystemet för att möta efterfrågan från kund * Möjlighet att arbeta i ett team som präglas av stark samhörighet med stor kompetens * Möjligheten att på sikt välja om du vill arbeta hemifrån eller på kontoret vissa dagar i veckan   ARBETSUPPGIFTER  - Hantera beställningar och administrera konton - Hantera inkommande ärenden via mejl och telefon - Felsökning och problemlösning i Briljant ASP - Felsökning hos kunder med egen drift av Briljant Ekonomisystem - För dig med intresse och kunskap finns det även möjlighet att vidareutveckla och underhålla underliggande system för molntjänsten  VI SÖKER DIG SOM  - Har erfarenhet av liknande arbetsuppgifter inom teknisk support eller har läst tekniska ämnen på eftergymnasial nivå och har ett genuint intresse för IT - Har grundläggande kunskap i Windows Server, Active Directory, databaser och nätverk - Är obehindrad i svenska i tal och skrift samt har goda kunskaper i engelska  Det är meriterande om du:   * Har goda kunskaper i C# och VB.NET samt PowerShell   Förutom detta är du:   * Serviceinriktad och trivs med kundkontakt * Noggrann * Har en god problemlösningsförmåga * Har en god samarbetsförmåga   Övrig information   * Start: September * Omfattning: Heltid * Placering: Norrköping * Rekryteringsprocessen hanteras av Academic Work och Briljants önskemål är att alla frågor rörande tjänsten hanteras av Academic Work.   Vi går igenom urvalet löpande och annonsen kan stängas ner innan tjänsten är tillsatt om vi gått över till urvals- och intervjufasen.  INFORMATION OM FÖRETAGET  Briljant Ekonomisystem är utvecklat i Sverige av personer med bakgrund från redovisning, revision, ekonomi och administration. Vi har varit verksamma sedan 1994 och är idag 35 personer och omsätter cirka 35 miljoner kronor. Mer än 60 000 företag sköter sin redovisning i Briljant.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Supporttekniker** till Briljant i Norrköping Nu... | x | x | 15 | 15 | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| 2 | Supporttekniker till **Briljant** i Norrköping Nu finns chansen... |  | x |  | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 3 | ...pporttekniker till Briljant i **Norrköping** Nu finns chansen för dig som ... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 4 | ...klingsbolag med huvudkontor i **Norrköping**. Som Supporttekniker hos Bril... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 5 | ...huvudkontor i Norrköping. Som **Supporttekniker** hos Briljant blir du en del a... | x | x | 15 | 15 | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| 6 | ...ping. Som Supporttekniker hos **Briljant** blir du en del av det team so... |  | x |  | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 7 | ...las i din roll!  OM TJÄNSTEN  **Briljant** Ekonomisystem AB äger och utv... |  | x |  | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 8 | ...isystem AB äger och utvecklar **Briljant Ekonomisystem.** Bakom produkten står ett utve... | x |  |  | 23 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 9 | ...ristående återförsäljarbolag. **Briljant** växer och välkomnar nu ytterl... |  | x |  | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 10 | ...gen för Briljants molntjänst. **Briljant** ASP är en molnbaserad version... |  | x |  | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 11 | ...riljants molntjänst. Briljant **ASP** är en molnbaserad version av ... |  | x |  | 3 | [ASP, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/QxVK_WLK_MgJ) |
| 12 | ... är en molnbaserad version av **Briljant Ekonomisystem** som idag har ca 6400 användar... | x |  |  | 22 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 13 | ..., drift och administration av **ASP**. Teamet, som präglas av gemen... |  | x |  | 3 | [ASP, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/QxVK_WLK_MgJ) |
| 14 | ...t samarbete, består idag av 3 **Supporttekniker** med stor kompetens inom områd... | x | x | 15 | 15 | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| 15 | ...varför han trivs i rollen som **Supporttekniker**:  ”Jag är både 1st, 2nd och 3... | x | x | 15 | 15 | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| 16 | ...”Jag är både 1st, 2nd och 3rd **linesupport** vilket gör rollen varierande ... | x |  |  | 11 | [Line-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/k2U3_c4q_FjL) |
| 17 | ... alla jobbar för att utveckla **Briljant** tillsammans. Vi har också tre... |  | x |  | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 18 | ...nsvar * Goda möjligheter till **kompetensutveckling** och att fördjupa din tekniska... | x |  |  | 19 | [Kompetensutveckling, personalarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/oTKR_GSK_e9B) |
| 19 | ... att på sikt välja om du vill **arbeta hemifrån** eller på kontoret vissa dagar... | x |  |  | 15 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 20 | ...renden via mejl och telefon - **Felsökning** och problemlösning i Briljant... | x | x | 10 | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 21 | ...lsökning och problemlösning i **Briljant** ASP - Felsökning hos kunder m... |  | x |  | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 22 | ...och problemlösning i Briljant **ASP** - Felsökning hos kunder med e... |  | x |  | 3 | [ASP, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/QxVK_WLK_MgJ) |
| 23 | ...oblemlösning i Briljant ASP - **Felsökning** hos kunder med egen drift av ... | x | x | 10 | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 24 | ... hos kunder med egen drift av **Briljant** Ekonomisystem - För dig med i... | x | x | 8 | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 25 | ...der med egen drift av Briljant** Ekonomisystem** - För dig med intresse och ku... | x |  |  | 14 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 26 | ... har ett genuint intresse för **IT** - Har grundläggande kunskap i... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 27 | ...- Har grundläggande kunskap i **Windows Server**, Active Directory, databaser ... | x |  |  | 14 | [Windows Server, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/LZBg_ARY_3qS) |
| 27 | ...- Har grundläggande kunskap i **Windows Server**, Active Directory, databaser ... |  | x |  | 14 | [Windows Server, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/pnX9_gFn_CLt) |
| 28 | ...nde kunskap i Windows Server, **Active Directory**, databaser och nätverk - Är o... | x | x | 16 | 16 | [Active Directory, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/xGPG_PpL_zWN) |
| 29 | ...ows Server, Active Directory, **databaser** och nätverk - Är obehindrad i... | x | x | 9 | 9 | [Databaser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/Uj5W_dft_Ssg) |
| 30 | ...tive Directory, databaser och **nätverk** - Är obehindrad i svenska i t... | x |  |  | 7 | [Nätverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/nHDU_dpj_R9H) |
| 31 | ...och nätverk - Är obehindrad i **svenska** i tal och skrift samt har god... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 32 | ...ift samt har goda kunskaper i **engelska**  Det är meriterande om du:   ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 33 | ... du:   * Har goda kunskaper i **C#** och VB.NET samt PowerShell   ... | x | x | 2 | 2 | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| 34 | ...unskaper i C# och VB.NET samt **PowerShell**   Förutom detta är du:   * Se... | x | x | 10 | 10 | [Windows Powershell, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/SnKF_v7d_qUH) |
| 35 | ...d och trivs med kundkontakt * **Noggrann** * Har en god problemlösningsf... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 36 | ...tart: September * Omfattning: **Heltid** * Placering: Norrköping * Rek... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 37 | ...fattning: Heltid * Placering: **Norrköping** * Rekryteringsprocessen hante... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 38 | ...n.  INFORMATION OM FÖRETAGET  **Briljant** Ekonomisystem är utvecklat i ... |  | x |  | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| 39 | ...MATION OM FÖRETAGET  Briljant **Ekonomi**system är utvecklat i Sverige ... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 40 | ... Ekonomisystem är utvecklat i **Sverige** av personer med bakgrund från... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 41 | ...av personer med bakgrund från **redovisning**, revision, ekonomi och admini... | x | x | 11 | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| 42 | ...ed bakgrund från redovisning, **revision**, ekonomi och administration. ... | x |  |  | 8 | [Revision, **skill**](http://data.jobtechdev.se/taxonomy/concept/hVq9_mTt_xkv) |
| 43 | ...d från redovisning, revision, **ekonomi** och administration. Vi har va... | x |  |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 44 | ...isning, revision, ekonomi och **administration**. Vi har varit verksamma sedan... | x |  |  | 14 | [sköta administration, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NEWh_NMX_RCZ) |
| 45 | ... än 60 000 företag sköter sin **redovisning** i Briljant. |  | x |  | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| 45 | ... än 60 000 företag sköter sin **redovisning** i Briljant. | x |  |  | 11 | [redovisning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aAbn_jKY_u2g) |
| 46 | ...etag sköter sin redovisning i **Briljant**. |  | x |  | 8 | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| | **Overall** | | | **196** | **482** | 196/482 = **41%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| x | x | x | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| x | x | x | [Briljant, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/KN5V_Gxm_nGL) |
| x | x | x | [Helpdesktekniker/Supporttekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/KQty_E1u_cia) |
| x |  |  | [Windows Server, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/LZBg_ARY_3qS) |
| x |  |  | [sköta administration, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NEWh_NMX_RCZ) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
|  | x |  | [ASP, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/QxVK_WLK_MgJ) |
| x | x | x | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| x | x | x | [Windows Powershell, systemutvecklingsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/SnKF_v7d_qUH) |
| x | x | x | [Databaser, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/Uj5W_dft_Ssg) |
| x |  |  | [redovisning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aAbn_jKY_u2g) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Revision, **skill**](http://data.jobtechdev.se/taxonomy/concept/hVq9_mTt_xkv) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| x |  |  | [Line-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/k2U3_c4q_FjL) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [Nätverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/nHDU_dpj_R9H) |
| x |  |  | [Kompetensutveckling, personalarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/oTKR_GSK_e9B) |
|  | x |  | [Windows Server, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/pnX9_gFn_CLt) |
| x |  |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| x | x | x | [Active Directory, programmeringsverktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/xGPG_PpL_zWN) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **14** | 14/26 = **54%** |