# Results for 'cf023d9c40b8c06ca506c980c6973adb882b4e59'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [cf023d9c40b8c06ca506c980c6973adb882b4e59](README.md) | 1 | 3265 | 18 | 21 | 132/390 = **34%** | 7/14 = **50%** |

## Source text

Sjuksköterska till medicinavdelning i Örnsköldsvik hösten 2022 Skräddarsy ditt jobb! Hos oss på Centric Care hjälper vi dig att hitta ditt drömjobb oavsett om det gäller arbete nära eller långt håll, korta som långa uppdrag. Trygga villkor och bra lön ingår! Bemanning behöver inte vara krångligt. Kontakta oss redan idag så berättar vi mer!  OM UPPDRAGET Är du legitimerad Sjuksköterska med erfarenhet av akutsjukvård och letar efter nya spännande uppdrag i höst? Då kan du vara den vi söker till ett bemanningsuppdrag i Örnsköldsvik!   För vår kunds räkning söker vi nu dig som är legitimerad sjuksköterska till en medicinavdelning i Örnsköldsvik. Till uppdraget söker vi dig som är tillgänglig under perioden v.37 till och med v.45. Arbetstid; rotation (dag/kväll/natt) samt tjänstgöring varannan helg. Kontinuitet genom hela uppdraget är meriterade, men även du som kan jobba delar av perioden är varmt välkommen in med en ansökan. Vi kan hjälpa till med resor och boende vid behov!  DINA ARBETSUPPGIFTER Som sjuksköterska arbetar du med sedvanliga sjuksköterskeuppgifter på en akutvårdsavdelning. På avdelningen arbetar du i datorprogram som NCS Cross.    DIN PROFIL Du är legitimerad Sjuksköterska och har minst 2 års dokumenterad yrkeserfarenhet. Du har godkänd samt erfarenhet av A HLR. Önskvärt är om du har vana att tolka telemetri (arytmitolkning).  ATT JOBBA SOM BEMANNINGSSJUKSKÖTERSKA  Hos oss får du ett välplanerat äventyr eller större flexibilitet på hemmaplan. Du väljer själv! Möjligheterna är många och du kommer att få bra betalt i både lönekuvertet och på ditt pensionskonto. Vi lovar aldrig mer än vad vi kan hålla och du och din personliga konsultchef har tät kontakt under hela uppdragets gång.   Vem passar som bemanningssjuksköterska? Du som är självständig i ditt arbete och trygg i din profession samt mån om både kvalitet och patientsäkerhet. Du har god samarbetsförmåga då arbetet sker i team runt patienten samt  kan anpassa ditt bemötande och förhållningssätt utifrån individ och situation. Om den här beskrivningen passar in på dig så tror vi att du kommer att trivas bra i rollen som bemanningssjuksköterska.   Vi på Centric Care tror på ärlighet, transparens och personlig service. Tillsammans gör vi vården bättre, med oss är du trygg.  OM CENTRIC CARE  Centric Care har bemannat hälso- och sjukvård sedan år 2000. Vi kan branschen och många som jobbar internt hos oss har en sjukvårdsutbildning i botten. Centric Care är ett auktoriserat bemanningsbolag och är medlem i Almega Kompetensföretagarna. Auktorisationen betyder att Centric Care lever upp till svensk lagstiftning och de etiska branschkrav som Almega ställer. Auktorisationen är en kvalitetsstämpel både för våra medarbetare, underleverantörer och kunder.   Centric Care har kollektivavtal för alla våra anställda. Det betyder att vi erbjuder trygga arbetsvillkor utöver vad lagstiftningen kräver. Alla anställda hos oss omfattas av försäkringar, extra hög semesterersättning och tjänstepension enligt ITP. Som anställd hos oss får du dessutom tillgång till ett förmånligt friskvårdbidrag, årlig HLR-utbildning och mycket mer.    Nyfiken på att veta mer? Sökt tjänsten eller gör en intresseanmälan så tar en av våra erfarna konsultchefer kontakt med dig.  Välkommen!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sjuksköterska** till medicinavdelning i Örnsk... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 1 | **Sjuksköterska** till medicinavdelning i Örnsk... | x |  |  | 13 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 2 | ...erska till medicinavdelning i **Örnsköldsvik** hösten 2022 Skräddarsy ditt j... | x | x | 12 | 12 | [Örnsköldsvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zBmE_n6s_MnQ) |
| 3 | ...M UPPDRAGET Är du legitimerad **Sjuksköterska** med erfarenhet av akutsjukvår... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 3 | ...M UPPDRAGET Är du legitimerad **Sjuksköterska** med erfarenhet av akutsjukvår... | x |  |  | 13 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 4 | ...uksköterska med erfarenhet av **akutsjukvård** och letar efter nya spännande... | x | x | 12 | 12 | [Akutsjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/azXF_rgu_EWv) |
| 5 | ... till ett bemanningsuppdrag i **Örnsköldsvik**!   För vår kunds räkning söke... | x | x | 12 | 12 | [Örnsköldsvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zBmE_n6s_MnQ) |
| 6 | ... vi nu dig som är legitimerad **sjuksköterska** till en medicinavdelning i Ör... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 6 | ... vi nu dig som är legitimerad **sjuksköterska** till en medicinavdelning i Ör... | x |  |  | 13 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 7 | ...ka till en medicinavdelning i **Örnsköldsvik**. Till uppdraget söker vi dig ... | x | x | 12 | 12 | [Örnsköldsvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zBmE_n6s_MnQ) |
| 8 | ...ov!  DINA ARBETSUPPGIFTER Som **sjuksköterska** arbetar du med sedvanliga sju... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 8 | ...ov!  DINA ARBETSUPPGIFTER Som **sjuksköterska** arbetar du med sedvanliga sju... | x |  |  | 13 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 9 | ...ska arbetar du med sedvanliga **sjuksköterskeuppgifter** på en akutvårdsavdelning. På ... |  | x |  | 22 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 10 | .... På avdelningen arbetar du i **datorprogram** som NCS Cross.    DIN PROFIL ... |  | x |  | 12 | [använda datorprogram för att förbättra patienters färdigheter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1TV8_iNC_Yap) |
| 10 | .... På avdelningen arbetar du i **datorprogram** som NCS Cross.    DIN PROFIL ... |  | x |  | 12 | [sälja datorprogram, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nJJD_mvm_o4d) |
| 10 | .... På avdelningen arbetar du i **datorprogram** som NCS Cross.    DIN PROFIL ... |  | x |  | 12 | [använda datorprogram för stenografi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qUEQ_xCX_EVD) |
| 11 | ... DIN PROFIL Du är legitimerad **Sjuksköterska** och har minst 2 års dokumente... |  | x |  | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 11 | ... DIN PROFIL Du är legitimerad **Sjuksköterska** och har minst 2 års dokumente... | x |  |  | 13 | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| 12 | ...d Sjuksköterska och har minst **2 års** dokumenterad yrkeserfarenhet.... | x |  |  | 5 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 13 | ... har minst 2 års dokumenterad **yrkeserfarenhet**. Du har godkänd samt erfarenh... | x |  |  | 15 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 14 | ...ar godkänd samt erfarenhet av **A **HLR. Önskvärt är om du har van... | x |  |  | 2 | [Första hjälpen - HLR-intyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/mwCM_MmB_6Ks) |
| 15 | ... godkänd samt erfarenhet av A **HLR**. Önskvärt är om du har vana a... | x | x | 3 | 3 | [Första hjälpen - HLR-intyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/mwCM_MmB_6Ks) |
| 16 | ...t är om du har vana att tolka **telemetri** (arytmitolkning).  ATT JOBBA ... |  | x |  | 9 | [utföra telemetri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UPu5_Xf8_tgN) |
| 17 | ...ytmitolkning).  ATT JOBBA SOM **BEMANNINGSSJUKSKÖTERSKA**  Hos oss får du ett välplaner... |  | x |  | 23 | [Bemanningssjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/KJLV_h2y_PG9) |
| 18 | ...ragets gång.   Vem passar som **bemanningssjuksköterska**? Du som är självständig i dit... | x | x | 23 | 23 | [Bemanningssjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/KJLV_h2y_PG9) |
| 19 | ...ningssjuksköterska? Du som är **självständig** i ditt arbete och trygg i din... | x | x | 12 | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 20 | ...Du som är självständig i ditt **arbete** och trygg i din profession sa... | x |  |  | 6 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 21 | ...r att trivas bra i rollen som **bemanningssjuksköterska**.   Vi på Centric Care tror på... | x | x | 23 | 23 | [Bemanningssjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/KJLV_h2y_PG9) |
| 22 | ...RE  Centric Care har bemannat **hälso- oc**h sjukvård sedan år 2000. Vi k... | x | x | 9 | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 23 | ...ic Care har bemannat hälso- oc**h sjukvård** sedan år 2000. Vi kan bransch... | x |  |  | 10 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 24 | ...ch kunder.   Centric Care har **kollektivavtal** för alla våra anställda. Det ... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **132** | **390** | 132/390 = **34%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [använda datorprogram för att förbättra patienters färdigheter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/1TV8_iNC_Yap) |
| x | x | x | [Bemanningssjuksköterska, **job-title**](http://data.jobtechdev.se/taxonomy/concept/KJLV_h2y_PG9) |
| x | x | x | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
|  | x |  | [utföra telemetri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UPu5_Xf8_tgN) |
| x | x | x | [Akutsjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/azXF_rgu_EWv) |
|  | x |  | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| x |  |  | [Sjuksköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/jzaN_Vqk_TP1) |
| x | x | x | [Första hjälpen - HLR-intyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/mwCM_MmB_6Ks) |
|  | x |  | [sälja datorprogram, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nJJD_mvm_o4d) |
|  | x |  | [använda datorprogram för stenografi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qUEQ_xCX_EVD) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Örnsköldsvik, **municipality**](http://data.jobtechdev.se/taxonomy/concept/zBmE_n6s_MnQ) |
| | | **7** | 7/14 = **50%** |