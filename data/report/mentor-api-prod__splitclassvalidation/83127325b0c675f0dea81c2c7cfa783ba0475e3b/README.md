# Results for '83127325b0c675f0dea81c2c7cfa783ba0475e3b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [83127325b0c675f0dea81c2c7cfa783ba0475e3b](README.md) | 1 | 1404 | 7 | 4 | 27/51 = **53%** | 4/6 = **67%** |

## Source text

Husvärd Övergripande beskrivning av tjänsten: Ge Folkets hus kunder en bra upplevelse av Folkets hus Lövånger, genom ett gott första intryck av oss som leverantör av våra tjänster och produkter, att hålla hög servicenivå, som ger oss nöjda och återkommande kunder. Huvudsakliga uppgifter: Service till gäster i den dagliga verksamheten i huset/området. Oavsett aktivitet i huset. Konferens & arrangemangsmöblering. Plock och städ efter konferens och arrangemang. Servering vid dagskonferenser och mindre möten. Städ av lokaler. Kunna tekniken i våra lokaler. Kassatjänst. Majoritet av olika aktiviteter i huset ska kunna genomföras, direktutbildning på plats kan ske ifall kunskap saknas.(allt från gruppträning till planering evenemang) Servering – disk och pustning av porslin, dukning, plocka disk, diska och efterarbete. Sedvanligt FHP arbete vid behov. Bistå andra sysslor när verksamheten kräver det, samt vid sjukdom och frånvaro av andra kollegor. Personalansvar-NEJ   Övrigt Medarbetare är Folkets Hus ansikte utåt och det är mycket viktigt att tycka om att ge förstklassig service och support till Folkets hus kunder, existerande och blivande. Att utstråla energi, glädje och ansvar är ledord för personal på plats. Utbildningskrav -utbildning som krävs för rollen kan ske på plats Grundskola-JA Erfarenhetskrav-MÅNGSIDIGHET Simkunskap-KRAV Språkkunskaper Svenska och engelska i tal och skrift.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Husvärd** Övergripande beskrivning av t... | x | x | 7 | 7 | [Bovärd/Fastighetsvärd/Husvärd, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/AjxW_jR6_J7P) |
| 2 | ...eten i huset/området. Oavsett **aktivitet** i huset. Konferens & arrangem... | x |  |  | 9 | [Idrott/aktivitet, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/LeJk_Xa1_TuS) |
| 3 | ...unna tekniken i våra lokaler. **Kassatjänst**. Majoritet av olika aktivitet... | x |  |  | 11 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 4 | ...nering evenemang) Servering – **disk** och pustning av porslin, dukn... | x |  |  | 4 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 5 | ...orslin, dukning, plocka disk, **diska** och efterarbete. Sedvanligt F... | x | x | 5 | 5 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 6 | ...imkunskap-KRAV Språkkunskaper **Svenska** och engelska i tal och skrift... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 7 | ...AV Språkkunskaper Svenska och **engelska** i tal och skrift. | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| | **Overall** | | | **27** | **51** | 27/51 = **53%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Bovärd/Fastighetsvärd/Husvärd, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/AjxW_jR6_J7P) |
| x | x | x | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x |  |  | [Idrott/aktivitet, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/LeJk_Xa1_TuS) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/6 = **67%** |