# Results for 'f08da2f9f09565b0633f8d017db9484a73b82190'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f08da2f9f09565b0633f8d017db9484a73b82190](README.md) | 1 | 794 | 8 | 11 | 35/135 = **26%** | 4/12 = **33%** |

## Source text

Bagare - Irakiska bröd Vi söker dig som är utbildad konditor/bagare, med stor känsla för service, då våra kunder alltid kommer i första hand. Bröden som vi producerar kommer då vara irakiska bröd (samon). Som person är du engagerad, positiv och ordningsam. Du har förmåga att prioritera och samarbeta och du tycker om att träffa många människor. Och kan tala arabiska och engelska. Du kommer att medverka till en optimal drift i avdelningen och själv utföra daglig drift tillsammans med driftledare och övriga medarbetare, som innebär: produktion, rengöring & hygien, datumkoll, prisinformation, egenkontroll, kundservice etc. Tjänsten är på heltid med varierad arbetstid, dag, kväll, helg. Vi tar endast emot ansökan via vår e-post, eller skicka direkt till vår adress. Varaktighet Tillsvidare

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bagare** - Irakiska bröd Vi söker dig ... |  | x |  | 6 | [Bagare/Konditor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XugJ_6hw_wVV) |
| 1 | **Bagare** - Irakiska bröd Vi söker dig ... | x |  |  | 6 | [bagare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Zo8A_5ML_j3t) |
| 2 | ... Vi söker dig som är utbildad **konditor**/bagare, med stor känsla för s... | x | x | 8 | 8 | [Bagare/Konditor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XugJ_6hw_wVV) |
| 3 | ... dig som är utbildad konditor/**bagare**, med stor känsla för service,... |  | x |  | 6 | [Bagare/Konditor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XugJ_6hw_wVV) |
| 3 | ... dig som är utbildad konditor/**bagare**, med stor känsla för service,... | x |  |  | 6 | [bagare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Zo8A_5ML_j3t) |
| 4 | ...många människor. Och kan tala **arabiska** och engelska. Du kommer att m... | x | x | 8 | 8 | [Arabiska, **language**](http://data.jobtechdev.se/taxonomy/concept/3iEY_RGP_qXq) |
| 5 | ...or. Och kan tala arabiska och **engelska**. Du kommer att medverka till ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 6 | ... daglig drift tillsammans med **driftledare** och övriga medarbetare, som i... |  | x |  | 11 | [Driftledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GRLx_b9q_zqN) |
| 6 | ... daglig drift tillsammans med **driftledare** och övriga medarbetare, som i... |  | x |  | 11 | [Driftledare, lantbruk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TT1h_Km4_mNS) |
| 6 | ... daglig drift tillsammans med **driftledare** och övriga medarbetare, som i... |  | x |  | 11 | [Driftledare, aktivitetsanläggning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r8cW_gQN_diW) |
| 6 | ... daglig drift tillsammans med **driftledare** och övriga medarbetare, som i... |  | x |  | 11 | [Driftledare, skogsbruk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vVhN_YyP_Xrv) |
| 7 | ...engöring & hygien, datumkoll, **prisinformation**, egenkontroll, kundservice et... |  | x |  | 15 | [ge kunder prisinformation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dy2R_NBF_TCw) |
| 8 | ...risinformation, egenkontroll, **kundservice** etc. Tjänsten är på heltid me... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 9 | ...ndservice etc. Tjänsten är på **heltid** med varierad arbetstid, dag, ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 10 | ... till vår adress. Varaktighet **Tillsvidare** | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| | **Overall** | | | **35** | **135** | 35/135 = **26%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Arabiska, **language**](http://data.jobtechdev.se/taxonomy/concept/3iEY_RGP_qXq) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Driftledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GRLx_b9q_zqN) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Driftledare, lantbruk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TT1h_Km4_mNS) |
| x | x | x | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| x | x | x | [Bagare/Konditor, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XugJ_6hw_wVV) |
| x |  |  | [bagare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Zo8A_5ML_j3t) |
|  | x |  | [ge kunder prisinformation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dy2R_NBF_TCw) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Driftledare, aktivitetsanläggning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r8cW_gQN_diW) |
|  | x |  | [Driftledare, skogsbruk, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/vVhN_YyP_Xrv) |
| | | **4** | 4/12 = **33%** |