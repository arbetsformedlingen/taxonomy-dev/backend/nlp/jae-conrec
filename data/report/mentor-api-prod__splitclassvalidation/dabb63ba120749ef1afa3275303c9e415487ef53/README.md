# Results for 'dabb63ba120749ef1afa3275303c9e415487ef53'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [dabb63ba120749ef1afa3275303c9e415487ef53](README.md) | 1 | 1492 | 3 | 2 | 0/52 = **0%** | 0/3 = **0%** |

## Source text

Medarbetare som vill servera världens godaste burgare Vi du skapa upplevelser? Vill du se dina kunder njuta av din service och vår goda mat?  Är du en utåtriktad och positiv ”people person” som gärna hugger i och gillar att ge bra service? Vill du göra karriär? Ha kul på  jobbet! Nu har du chansen - sök jobbet idag!  Vi på Burger King söker just nu restaurangmedarbetare (helg - dag/kväll)  Du blir vårt ansikte utåt i vår restaurang och får ett mångsidigt jobb som innebär att du roterar mellan olika arbetsuppgifter. Du kommer att få arbeta både i köket, matsalen och med att servera gäster.  Vad vi erbjuder:  Burger King är ett av världens största restaurangföretag. Och självklart är engagerade, motiverade och trevliga medarbetare nyckeln till vår framgång. För oss är det viktigt att våra medarbetare får chansen att utvecklas. Vi erbjuder därför en tydlig karriärstege och goda  utvecklingsmöjligheter. Tanken med våra utbildningar är att du ska växa med ansvar och utvecklas i din yrkesroll under hela din tid hos oss.  Vem är du?  För att lyckas i jobbet tror vi att du har en utpräglad servicekänsla och tycker att det är roligt att arbeta med människor.   Låter det som något för dig? Sök jobbet Nu!  Burger King är ett av världens största restaurangföretag. För oss är självklart engagerade, motiverade och trevliga medarbetare nyckeln till vår framgång. Därför värderar vi vår personal högt och erbjuder en tydlig karriärstege med fortbildning och goda utvecklingsmöjligheter.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... på Burger King söker just nu **restaurangmedarbetare** (helg - dag/kväll)  Du blir v... |  | x |  | 21 | [Restaurangpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZYjR_db3_jN5) |
| 1 | ... på Burger King söker just nu **restaurangmedarbetare** (helg - dag/kväll)  Du blir v... | x |  |  | 21 | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| 2 | ... blir vårt ansikte utåt i vår **restaurang** och får ett mångsidigt jobb s... |  | x |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| | **Overall** | | | **0** | **52** | 0/52 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Restaurangpersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ZYjR_db3_jN5) |
|  | x |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x |  |  | [Restaurangbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/yXRQ_Ad9_E8V) |
| | | **0** | 0/3 = **0%** |