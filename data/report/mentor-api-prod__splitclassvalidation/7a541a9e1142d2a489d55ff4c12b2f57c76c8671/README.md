# Results for '7a541a9e1142d2a489d55ff4c12b2f57c76c8671'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [7a541a9e1142d2a489d55ff4c12b2f57c76c8671](README.md) | 1 | 381 | 4 | 0 | 0/35 = **0%** | 0/2 = **0%** |

## Source text

Entrevärd med sinne för service! Nu önskar vi bredda vårt fantastiska team och utöka styrkan med en entrevärd. Vår nya lagspelare ska vara social, utåtriktad och vara duktig på att hantera eventuella konflikter. Arbetsuppgifterna är bland annat att stå vid dörren och välkomna folk, kolla leg och se till att berusningsnivån är bra. Tjänsten avser Onsdagar- Söndagar kl 20.00-01.15

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Entrevärd** med sinne för service! Nu öns... | x |  |  | 9 | [Entrévakt, **job-title**](http://data.jobtechdev.se/taxonomy/concept/AtN6_CkN_A79) |
| 2 | ...team och utöka styrkan med en **entrevärd**. Vår nya lagspelare ska vara ... | x |  |  | 9 | [Entrévakt, **job-title**](http://data.jobtechdev.se/taxonomy/concept/AtN6_CkN_A79) |
| 3 | ...riktad och vara duktig på att **hantera** eventuella konflikter. Arbets... | x |  |  | 7 | [hantera konflikter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/q2pa_bUy_w3L) |
| 4 | ...tig på att hantera eventuella **konflikter**. Arbetsuppgifterna är bland a... | x |  |  | 10 | [hantera konflikter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/q2pa_bUy_w3L) |
| | **Overall** | | | **0** | **35** | 0/35 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Entrévakt, **job-title**](http://data.jobtechdev.se/taxonomy/concept/AtN6_CkN_A79) |
| x |  |  | [hantera konflikter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/q2pa_bUy_w3L) |
| | | **0** | 0/2 = **0%** |