# Results for '21c07e29c01ec244afb0b481817025b2ee4da92c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [21c07e29c01ec244afb0b481817025b2ee4da92c](README.md) | 1 | 2340 | 22 | 20 | 161/235 = **69%** | 10/14 = **71%** |

## Source text

Cirkus- och varietéartist till Halloween på Liseberg 2022 Upplevelse- och åkattraktionerna är för många gäster anledningen till ett Lisebergsbesök.  Med bemötande och säkerhet i fokus ansluter Gästverksamhetsområde Attraktion starkt till ansvaret för gästens helhetsupplevelse och förstärker Lisebergsupplevelsen ytterligare.  Beskrivning  Gästverksamhetsområde Attraktion på Liseberg söker Cirkus- och varietéartister till Skräckenheten Cirkus Bisarr. I din roll uppträder du tillsammans med dels övriga Cirkus- och varietéartister samt våra Aktörer i området. Som Cirkus- och varietéartist uppträder du med egna eller manusbaserade nummer. Beroende på vilken roll man har är det olika talanger som krävs. Du gillar att leka med eld, sticka nålar i huden, jonglör, fakir, lindansare eller gillar att vara lite galen med ett annat eget nummer är du kanske en av oss till Halloween 2022.  Arbetsuppgifter  En arbetsdag innefattar förutom schemalagda framträdanden, även uppvärmning, förberedelser och iordningställande. Tjänsterna vi erbjuder är säsongstjänster och arbetstiden följer Lisebergsparkens öppettider och är förlagd till såväl vardagar som helger, dagtid som kvällstid.  Kvalifikationer  De framträdanden som spelas är till viss del manusbundna och regisserade, men innehåller även improviserade delar, vilket gör att vi lägger stor vikt vid din förmåga att improvisera. Vi söker dig som besitter unika talanger/nummer inom varieté. Har du erfarenheter eller utbildning inom fysisk teater är detta meriterande.  För att arbeta som Cirkus- och varietéartist på Liseberg ser vi att du har utbildning och/eller professionell erfarenhet inom området teater/sång/dans eller liknande utbildning inom cirkus. Samt goda kunskaper inom improvisation.  Vi vill att du förutom din ansökan och CV spelar in en stycke film där du berättar om vem du är, varför vill du arbeta på Halloween och presentera ditt nummer? 60 sek.  Vill du får du gärna länka din showreal.  Vill du veta mer om våra karaktärer eller övrig information om tjänsten är du välkommen att kontakta vår konceptkoordinator/arbetsledare Elin Ljungberg, 031-766 70 48, områdeschef Emil Aronsson, 031-766 73 43 alt via mail till skrack@liseberg.se.  Observera att sista ansökningsdag är söndagen den 7 augusti och audition kommer att hållas på Liseberg måndag den 29 augusti.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Cirkus**- och varietéartist till Hallo... | x | x | 6 | 6 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 2 | Cirkus**- och **varietéartist till Halloween p... |  | x |  | 6 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 3 | Cirkus- och **varietéartist** till Halloween på Liseberg 20... | x | x | 13 | 13 | [Varietéartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/uaUH_r3u_ygB) |
| 4 | ... Attraktion på Liseberg söker **Cirkus**- och varietéartister till Skr... | x | x | 6 | 6 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 5 | ...ktion på Liseberg söker Cirkus**- och va**rietéartister till Skräckenhet... |  | x |  | 8 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 6 | ...på Liseberg söker Cirkus- och **varietéartister** till Skräckenheten Cirkus Bis... | x | x | 15 | 15 | [Varietéartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/uaUH_r3u_ygB) |
| 7 | ...u tillsammans med dels övriga **Cirkus**- och varietéartister samt vår... | x | x | 6 | 6 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 8 | ...sammans med dels övriga Cirkus**- och va**rietéartister samt våra Aktöre... |  | x |  | 8 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 9 | ...s med dels övriga Cirkus- och **varietéartister** samt våra Aktörer i området. ... | x | x | 15 | 15 | [Varietéartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/uaUH_r3u_ygB) |
| 10 | ...och varietéartister samt våra **Aktörer** i området. Som Cirkus- och va... | x |  |  | 7 | [Skådespelare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Xoz2_dQe_9KC) |
| 11 | ...t våra Aktörer i området. Som **Cirkus**- och varietéartist uppträder ... | x | x | 6 | 6 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 12 | ... Aktörer i området. Som Cirkus**- och **varietéartist uppträder du med... |  | x |  | 6 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 13 | ...er i området. Som Cirkus- och **varietéartist** uppträder du med egna eller m... | x | x | 13 | 13 | [Varietéartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/uaUH_r3u_ygB) |
| 14 | ...ed eld, sticka nålar i huden, **jonglör**, fakir, lindansare eller gill... | x | x | 7 | 7 | [Jonglör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/svyq_Vf5_vAD) |
| 15 | ...ticka nålar i huden, jonglör, **fakir**, lindansare eller gillar att ... | x | x | 5 | 5 | [Fakir, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3p1G_X4E_cy1) |
| 16 | ...ålar i huden, jonglör, fakir, **lindansare** eller gillar att vara lite ga... | x | x | 10 | 10 | [Lindansare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/SvF6_mSB_UCb) |
| 17 | ...de. Tjänsterna vi erbjuder är **säsongstjänster** och arbetstiden följer Lisebe... | x |  |  | 15 | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
| 18 | ...stor vikt vid din förmåga att **improvisera**. Vi söker dig som besitter un... | x |  |  | 11 | [improvisera, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kkHG_byL_75a) |
| 19 | ...er unika talanger/nummer inom **varieté**. Har du erfarenheter eller ut... | x |  |  | 7 | [Varieténummer, **skill**](http://data.jobtechdev.se/taxonomy/concept/VB6Y_VoL_Cnz) |
| 20 | ...enheter eller utbildning inom **fysisk teater** är detta meriterande.  För at... | x | x | 13 | 13 | [Fysisk teater, **skill**](http://data.jobtechdev.se/taxonomy/concept/RG5k_D9y_iW5) |
| 21 | ...iterande.  För att arbeta som **Cirkus**- och varietéartist på Liseber... | x | x | 6 | 6 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 22 | ...de.  För att arbeta som Cirkus**- och **varietéartist på Liseberg ser ... |  | x |  | 6 | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| 23 | ...ör att arbeta som Cirkus- och **varietéartist** på Liseberg ser vi att du har... | x | x | 13 | 13 | [Varietéartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/uaUH_r3u_ygB) |
| 24 | ...onell erfarenhet inom området **teater**/sång/dans eller liknande utbi... | x | x | 6 | 6 | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| 25 | ...rfarenhet inom området teater/**sång**/dans eller liknande utbildnin... | x | x | 4 | 4 | [Sång, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tKeW_sAm_D72) |
| 26 | ...nhet inom området teater/sång/**dans** eller liknande utbildning ino... | x | x | 4 | 4 | [Dans, **skill**](http://data.jobtechdev.se/taxonomy/concept/kusY_oZZ_Wbv) |
| 27 | ...kus. Samt goda kunskaper inom **improvisation**.  Vi vill att du förutom din ... | x | x | 13 | 13 | [Improvisation, **skill**](http://data.jobtechdev.se/taxonomy/concept/qwHa_w4A_tTc) |
| | **Overall** | | | **161** | **235** | 161/235 = **69%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Fakir, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3p1G_X4E_cy1) |
| x |  |  | [Säsongsanställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/EBhX_Qm2_8eX) |
| x | x | x | [Fysisk teater, **skill**](http://data.jobtechdev.se/taxonomy/concept/RG5k_D9y_iW5) |
| x | x | x | [Lindansare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/SvF6_mSB_UCb) |
| x |  |  | [Varieténummer, **skill**](http://data.jobtechdev.se/taxonomy/concept/VB6Y_VoL_Cnz) |
| x |  |  | [Skådespelare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Xoz2_dQe_9KC) |
| x | x | x | [Cirkusartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fxx3_XHC_cCg) |
| x |  |  | [improvisera, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/kkHG_byL_75a) |
| x | x | x | [Dans, **skill**](http://data.jobtechdev.se/taxonomy/concept/kusY_oZZ_Wbv) |
| x | x | x | [Improvisation, **skill**](http://data.jobtechdev.se/taxonomy/concept/qwHa_w4A_tTc) |
| x | x | x | [Jonglör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/svyq_Vf5_vAD) |
| x | x | x | [Sång, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tKeW_sAm_D72) |
| x | x | x | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| x | x | x | [Varietéartist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/uaUH_r3u_ygB) |
| | | **10** | 10/14 = **71%** |