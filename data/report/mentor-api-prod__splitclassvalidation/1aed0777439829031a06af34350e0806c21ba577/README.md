# Results for '1aed0777439829031a06af34350e0806c21ba577'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1aed0777439829031a06af34350e0806c21ba577](README.md) | 1 | 3361 | 18 | 38 | 82/545 = **15%** | 3/12 = **25%** |

## Source text

Elevassistent med förmåga att skapa goda relationer Österängens skolområde  Vill du ha ett av de viktigaste och roligaste uppdragen som finns? Hos oss får du inspirera till ett lustfyllt lärande – varje dag, hela vägen. Vi kallar det lärglädje.  Ditt nya jobb Som elevassistent i grundsärskolan kommer du att arbeta med elever som har en intellektuell funktionsnedsättning. I rollen som elevassistent arbetar du både i skolan och i fritidsverksamheten i nära samarbete med lärare i skolan, lärare i fritidshemmet och elevassistenter. Tillsammans skapar vi en lärmiljö där eleverna får växa och utvecklas så långt som möjligt. Du får möjlighet att vara flexibel i ditt arbete och stötta elever där vi har behov. Det innebär att du är basplacerad i vår grundsärskola men kan få vikariera i grundskolan, både på skoltid och fritidshemstid. Tjänsten är på visstid men kan komma att förlängas.   Välkommen till Österängens skolområde Totalt går det drygt 200 elever på Österängens skolområde. Österängsskolan ligger i Jönköpings östra stadsdelar och är världen i miniatyr. Vi är stolta över vår mångfald och vet att den berikar hela skoldagen. I Österängsskolans byggnader samsas grundsärskola åk 1–6 med grundskolans låg-, och mellanstadium. Grundsärskolan har i dagsläget 23 elever fördelade på 5 klasser och här arbetar ca. 24 medarbetare. Vi är en arbetsplats som präglas av hög behörighet och kompetens. Atmosfären är trygg och positiv, vilket elevernas årliga trygghetsenkät och medarbetarenkäten visar.  På vår hemsida kan du läsa mer om vår skola. (https://www.jonkoping.se/barnutbildning/grundskola/grundskolorao/osterangsskolanf6.4.74fef9ab15548f0b8001938.html)  Läs mer om hur det är att jobba i Jönköpings kommun på jonkoping.se (https://www.jonkoping.se/naringslivarbete/jobbaijonkopingskommun.4.74fef9ab15548f0b8001425.html)  Din kompetens Som elevassistent har du utbildning inom pedagogik/vård/omsorg/behandling/fritidsverksamhet eller annan utbildning som bedöms likvärdig. Du har erfarenhet av att ha arbetat med elever som har en intellektuell funktionsnedsättning i grundsärskolan eller i annan verksamhet. Det är meriterande om du kan TAKK. Du har förmåga att skapa goda relationer med elever, vårdnadshavare och kollegor. Stor vikt läggs vid personlig lämplighet.  Bra att veta Bifoga relevanta intyg och betyg för tjänsten du söker. Tänk på att skollagen kräver att samtliga medarbetare visar upp ett utdrag ur belastningsregistret och handläggningstiden för detta dokument kan vara lång.  Här på polisen.se beställer du ett utdrag för arbete inom skola eller förskola (https://polisen.se/tjanster-tillstand/belastningsregistret/skola-eller-forskola/)  Vill du veta mer?  Cecilia Larsson, bitr. rektor 036-10 31 27 Kajsa Johansson, rektor, 036-10 37 73 Här hittar du kontaktuppgifter till fackliga företrädare på utbildningsförvaltningen. (https://www.jonkoping.se/fackligaubf)  Tillsammans gör vi Jönköpings kommun bättre – för alla. Hos oss får du mer än ett jobb, välkommen att söka tjänsten som elevassistent.  Jönköpings kommun strävar efter att erbjuda en attraktiv arbetsplats som är fri från diskriminering och ger lika möjligheter för alla.  Inför rekryteringsarbetet har vi tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför vänligt men bestämt alla samtal om annonserings- och rekryteringshjälp.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Elevassistent** med förmåga att skapa goda re... |  | x |  | 13 | [Barnskötare och elevassistenter m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DvKy_gyT_dAv) |
| 1 | **Elevassistent** med förmåga att skapa goda re... |  | x |  | 13 | [Elevassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/GMJ4_pEJ_BX1) |
| 1 | **Elevassistent** med förmåga att skapa goda re... |  | x |  | 13 | [Elevassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MS47_iZs_tog) |
| 1 | **Elevassistent** med förmåga att skapa goda re... |  | x |  | 13 | [Elevassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Sbzf_iBA_MAS) |
| 1 | **Elevassistent** med förmåga att skapa goda re... |  | x |  | 13 | [Elevassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/XW9L_tA9_WaX) |
| 1 | **Elevassistent** med förmåga att skapa goda re... | x | x | 13 | 13 | [elevassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/se8V_D14_eph) |
| 2 | ...lärglädje.  Ditt nya jobb Som **elevassistent** i grundsärskolan kommer du at... |  | x |  | 13 | [Barnskötare och elevassistenter m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DvKy_gyT_dAv) |
| 2 | ...lärglädje.  Ditt nya jobb Som **elevassistent** i grundsärskolan kommer du at... |  | x |  | 13 | [Elevassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/GMJ4_pEJ_BX1) |
| 2 | ...lärglädje.  Ditt nya jobb Som **elevassistent** i grundsärskolan kommer du at... |  | x |  | 13 | [Elevassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MS47_iZs_tog) |
| 2 | ...lärglädje.  Ditt nya jobb Som **elevassistent** i grundsärskolan kommer du at... |  | x |  | 13 | [Elevassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Sbzf_iBA_MAS) |
| 2 | ...lärglädje.  Ditt nya jobb Som **elevassistent** i grundsärskolan kommer du at... |  | x |  | 13 | [Elevassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/XW9L_tA9_WaX) |
| 2 | ...lärglädje.  Ditt nya jobb Som **elevassistent** i grundsärskolan kommer du at... | x | x | 13 | 13 | [elevassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/se8V_D14_eph) |
| 3 | ...ionsnedsättning. I rollen som **elevassistent** arbetar du både i skolan och ... |  | x |  | 13 | [Barnskötare och elevassistenter m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DvKy_gyT_dAv) |
| 3 | ...ionsnedsättning. I rollen som **elevassistent** arbetar du både i skolan och ... |  | x |  | 13 | [Elevassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/GMJ4_pEJ_BX1) |
| 3 | ...ionsnedsättning. I rollen som **elevassistent** arbetar du både i skolan och ... |  | x |  | 13 | [Elevassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MS47_iZs_tog) |
| 3 | ...ionsnedsättning. I rollen som **elevassistent** arbetar du både i skolan och ... |  | x |  | 13 | [Elevassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Sbzf_iBA_MAS) |
| 3 | ...ionsnedsättning. I rollen som **elevassistent** arbetar du både i skolan och ... |  | x |  | 13 | [Elevassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/XW9L_tA9_WaX) |
| 3 | ...ionsnedsättning. I rollen som **elevassistent** arbetar du både i skolan och ... | x | x | 13 | 13 | [elevassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/se8V_D14_eph) |
| 4 | ...n, lärare i fritidshemmet och **elevassistenter**. Tillsammans skapar vi en lär... |  | x |  | 15 | [Barnskötare och elevassistenter m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DvKy_gyT_dAv) |
| 4 | ...n, lärare i fritidshemmet och **elevassistenter**. Tillsammans skapar vi en lär... |  | x |  | 15 | [Elevassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/GMJ4_pEJ_BX1) |
| 4 | ...n, lärare i fritidshemmet och **elevassistenter**. Tillsammans skapar vi en lär... |  | x |  | 15 | [Elevassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MS47_iZs_tog) |
| 4 | ...n, lärare i fritidshemmet och **elevassistenter**. Tillsammans skapar vi en lär... |  | x |  | 15 | [Elevassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Sbzf_iBA_MAS) |
| 4 | ...n, lärare i fritidshemmet och **elevassistenter**. Tillsammans skapar vi en lär... |  | x |  | 15 | [Elevassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/XW9L_tA9_WaX) |
| 4 | ...n, lärare i fritidshemmet och **elevassistenter**. Tillsammans skapar vi en lär... |  | x |  | 15 | [elevassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/se8V_D14_eph) |
| 5 | ...nf6.4.74fef9ab15548f0b8001938.**html**)  Läs mer om hur det är att j... | x |  |  | 4 | [HTML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/K8eq_sEz_eXV) |
| 6 | ...mun.4.74fef9ab15548f0b8001425.**html**)  Din kompetens Som elevassis... | x |  |  | 4 | [HTML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/K8eq_sEz_eXV) |
| 7 | ...1425.html)  Din kompetens Som **elevassistent** har du utbildning inom pedago... |  | x |  | 13 | [Barnskötare och elevassistenter m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DvKy_gyT_dAv) |
| 7 | ...1425.html)  Din kompetens Som **elevassistent** har du utbildning inom pedago... |  | x |  | 13 | [Elevassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/GMJ4_pEJ_BX1) |
| 7 | ...1425.html)  Din kompetens Som **elevassistent** har du utbildning inom pedago... |  | x |  | 13 | [Elevassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MS47_iZs_tog) |
| 7 | ...1425.html)  Din kompetens Som **elevassistent** har du utbildning inom pedago... |  | x |  | 13 | [Elevassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Sbzf_iBA_MAS) |
| 7 | ...1425.html)  Din kompetens Som **elevassistent** har du utbildning inom pedago... |  | x |  | 13 | [Elevassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/XW9L_tA9_WaX) |
| 7 | ...1425.html)  Din kompetens Som **elevassistent** har du utbildning inom pedago... | x | x | 13 | 13 | [elevassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/se8V_D14_eph) |
| 8 | ...istent har du utbildning inom **pedagogik**/vård/omsorg/behandling/fritid... | x | x | 9 | 9 | [Pedagogik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zzEU_5xS_cJw) |
| 9 | ...kument kan vara lång.  Här på **polisen**.se beställer du ett utdrag fö... | x |  |  | 7 | [Polisen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/B7tQ_J1q_UNn) |
| 10 | ...g för arbete inom skola eller **förskola** (https://polisen.se/tjanster-... | x | x | 8 | 8 | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| 11 | ...skola eller förskola (https://**polisen**.se/tjanster-tillstand/belastn... | x |  |  | 7 | [Polisen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/B7tQ_J1q_UNn) |
| 12 | ...u veta mer?  Cecilia Larsson, **bitr. rektor** 036-10 31 27 Kajsa Johansson,... | x |  |  | 12 | [Bitr. rektor, **job-title**](http://data.jobtechdev.se/taxonomy/concept/bwcF_6L7_NVy) |
| 13 | ...lkommen att söka tjänsten som **elevassistent**.  Jönköpings kommun strävar... |  | x |  | 13 | [Barnskötare och elevassistenter m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DvKy_gyT_dAv) |
| 13 | ...lkommen att söka tjänsten som **elevassistent**.  Jönköpings kommun strävar... |  | x |  | 13 | [Elevassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/GMJ4_pEJ_BX1) |
| 13 | ...lkommen att söka tjänsten som **elevassistent**.  Jönköpings kommun strävar... |  | x |  | 13 | [Elevassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MS47_iZs_tog) |
| 13 | ...lkommen att söka tjänsten som **elevassistent**.  Jönköpings kommun strävar... |  | x |  | 13 | [Elevassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Sbzf_iBA_MAS) |
| 13 | ...lkommen att söka tjänsten som **elevassistent**.  Jönköpings kommun strävar... |  | x |  | 13 | [Elevassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/XW9L_tA9_WaX) |
| 13 | ...lkommen att söka tjänsten som **elevassistent**.  Jönköpings kommun strävar... | x | x | 13 | 13 | [elevassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/se8V_D14_eph) |
| 14 | ... till rekryteringskanaler och **marknadsföring**. Vi undanber oss därför vänli... | x |  |  | 14 | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| | **Overall** | | | **82** | **545** | 82/545 = **15%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Polisen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/B7tQ_J1q_UNn) |
|  | x |  | [Barnskötare och elevassistenter m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DvKy_gyT_dAv) |
|  | x |  | [Elevassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/GMJ4_pEJ_BX1) |
| x |  |  | [HTML, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/K8eq_sEz_eXV) |
|  | x |  | [Elevassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/MS47_iZs_tog) |
|  | x |  | [Elevassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Sbzf_iBA_MAS) |
|  | x |  | [Elevassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/XW9L_tA9_WaX) |
| x |  |  | [Bitr. rektor, **job-title**](http://data.jobtechdev.se/taxonomy/concept/bwcF_6L7_NVy) |
| x | x | x | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| x | x | x | [elevassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/se8V_D14_eph) |
| x |  |  | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| x | x | x | [Pedagogik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zzEU_5xS_cJw) |
| | | **3** | 3/12 = **25%** |