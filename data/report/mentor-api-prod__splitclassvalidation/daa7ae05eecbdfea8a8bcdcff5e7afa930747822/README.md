# Results for 'daa7ae05eecbdfea8a8bcdcff5e7afa930747822'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [daa7ae05eecbdfea8a8bcdcff5e7afa930747822](README.md) | 1 | 1505 | 8 | 10 | 32/115 = **28%** | 4/8 = **50%** |

## Source text

Nya investeringar i infrastrukturen, pensionsavgångar och ändrade upphandlingsvillkor leder till att behovet av VA-projektörer ökar markant. Enligt branschorganisationen Svenskt Vatten måste minst 1 000 personer rekryteras inom den kommande tioårsperioden. Genom att utbilda dig till VA-projektör får du en eftertraktad yrkesprofil och goda möjligheter till anställning hos entreprenörer och konsulter men också hos kommuner. En framtida arbetsgivare kan vara företag som Ramböll, WSP, Tyrens, Sweco, Afry eller Sigma Civil.    Omfattning  400 YH-poäng, 80 veckor fördelade på 56 veckor teori och 24 veckor LIA.         Satellitorter: på plats men samtidigt online  Att läsa på en satellitort innebär att utbildningen är platsbunden till en ort (Umeå) men att det också finns möjlighet att läsa utbildningen på plats på annan ort (Gävle, Sundsvall, Östersund).    Du som väljer att läsa utbildningen i t.ex. Gävle kommer ha ditt klassrum på Folkuniversitetet i Gävle och motsvarande på de andra orterna. Läraren och dina kurskamrater från andra orter finns med via Zoom som alla kopplar upp sig till.    Detta är alltså inte en distansutbildning, eftersom du kommer att delta på lektionerna i realtid tillsammans med din studerandegrupp på orten du läser vid.    Metodik och pedagogik  Under teoriperioderna varvas lektioner, grupparbeten och hemstudier.  Våra lärare är själva yrkesverksamma och därför väl uppdaterade om vad som sker i branschen samt mycket kompetenta såväl praktiskt som pedagogiskt.  

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...kor leder till att behovet av **VA-projektörer** ökar markant. Enligt branscho... | x |  |  | 14 | [vatten- och avloppstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8Jmo_w98_Nvi) |
| 1 | ...kor leder till att behovet av **VA-projektörer** ökar markant. Enligt branscho... |  | x |  | 14 | [VA-projektör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/AkaZ_Zv9_gRW) |
| 2 | ...n. Genom att utbilda dig till **VA-projektör** får du en eftertraktad yrkesp... | x |  |  | 12 | [vatten- och avloppstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8Jmo_w98_Nvi) |
| 2 | ...n. Genom att utbilda dig till **VA-projektör** får du en eftertraktad yrkesp... |  | x |  | 12 | [VA-projektör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/AkaZ_Zv9_gRW) |
| 3 | ...n är platsbunden till en ort (**Umeå**) men att det också finns möjl... | x | x | 4 | 4 | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| 4 | ...ningen på plats på annan ort (**Gävle**, Sundsvall, Östersund).    Du... | x | x | 5 | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 5 | ...på plats på annan ort (Gävle, **Sundsvall**, Östersund).    Du som väljer... | x | x | 9 | 9 | [Sundsvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dJbx_FWY_tK6) |
| 6 | ... annan ort (Gävle, Sundsvall, **Östersund**).    Du som väljer att läsa u... | x | x | 9 | 9 | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| 7 | ...att läsa utbildningen i t.ex. **Gävle** kommer ha ditt klassrum på Fo... | x | x | 5 | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 8 | ...assrum på Folkuniversitetet i **Gävle** och motsvarande på de andra o... | x |  |  | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 9 | ...l.    Detta är alltså inte en **distansutbildning**, eftersom du kommer att delta... |  | x |  | 17 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 10 | ... du läser vid.    Metodik och **pedagogik**  Under teoriperioderna varvas... |  | x |  | 9 | [Pedagogik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zzEU_5xS_cJw) |
| | **Overall** | | | **32** | **115** | 32/115 = **28%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [vatten- och avloppstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8Jmo_w98_Nvi) |
|  | x |  | [VA-projektör, **job-title**](http://data.jobtechdev.se/taxonomy/concept/AkaZ_Zv9_gRW) |
| x | x | x | [Umeå, **municipality**](http://data.jobtechdev.se/taxonomy/concept/QiGt_BLu_amP) |
| x | x | x | [Östersund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Vt7P_856_WZS) |
| x | x | x | [Sundsvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dJbx_FWY_tK6) |
| x | x | x | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
|  | x |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
|  | x |  | [Pedagogik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zzEU_5xS_cJw) |
| | | **4** | 4/8 = **50%** |