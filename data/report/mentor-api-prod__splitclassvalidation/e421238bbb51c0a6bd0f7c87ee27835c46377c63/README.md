# Results for 'e421238bbb51c0a6bd0f7c87ee27835c46377c63'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e421238bbb51c0a6bd0f7c87ee27835c46377c63](README.md) | 1 | 1304 | 24 | 13 | 61/370 = **16%** | 4/16 = **25%** |

## Source text

Utbildning leder till examen som tandsköterska och du kommer vara behörig att arbeta på klinik både inom den offentliga och den privata sektorn. Idag står yrkesgruppen inför flera år av stora pensionsavgångar och därför finns ett stort behov av nyrekryteringar.    Tandsköterskeyrket är ett serviceyrke med mycket patientkontakt. Som tandsköterska arbetar du i tandvårdsteam tillsammans med tandläkare och tandhygienister. Du får assistera vid behandlingar av patienter samt utföra ett preventivt tandvårdsarbete. Många administrativa uppgifter som journalföring, patientbokning och receptionsarbete ingår i yrket.    Utbildningen är upplagd på tre terminer och under varje termin finns en LIA- period. Då är de studerande ute på tandvårdsklinik för att träna sina kunskaper i praktiken. Vi erbjuder LIA-platser inom Region Uppsala och Folktandvården Stockholms läns AB.    Kurser:  Anatomi, anestesi och farmakologii, 25 yhp  Etik, bemötande och kommunikation, 15 yhp  Examensarbete, 20 yhp  Lärande i arbete 1,15 yhp  Lärande i arbete 2,125 yhp  Lärande i arbete 31,140 yhp  Material, miljö och hantering av farligt avfall, 20 yhp  Odontologi 1, 40 yhp  Odontologi 2, 30 yhp  Oral hälsa 1, 25 yhp  Oral hälsa 2, 30 yhp  Tandvårdens verksamhet och organisation, 15 yhp  Vårdhygien och smittskydd, 25 yhp

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...ildning leder till examen som **tandsköterska** och du kommer vara behörig at... | x | x | 13 | 13 | [Tandsköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ckkD_JhH_Sc6) |
| 2 | ...ed mycket patientkontakt. Som **tandsköterska** arbetar du i tandvårdsteam ti... | x | x | 13 | 13 | [Tandsköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ckkD_JhH_Sc6) |
| 3 | ...tandvårdsteam tillsammans med **tandläkare** och tandhygienister. Du får a... | x |  |  | 10 | [Tandläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TdvW_S8R_CFH) |
| 3 | ...tandvårdsteam tillsammans med **tandläkare** och tandhygienister. Du får a... |  | x |  | 10 | [tandläkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/teYK_qML_UE3) |
| 4 | ...illsammans med tandläkare och **tandhygienister**. Du får assistera vid behandl... | x | x | 15 | 15 | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
| 5 | ...e och tandhygienister. Du får **assistera vid behandlingar av patienter** samt utföra ett preventivt ta... | x |  |  | 39 | [assistera tandläkaren vid tandvårdsbehandlingar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5LfH_HF6_dBU) |
| 6 | ... administrativa uppgifter som **journalföring**, patientbokning och reception... | x | x | 13 | 13 | [föra journal, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wc5w_aw2_KBh) |
| 7 | ...nalföring, patientbokning och **receptionsarbete** ingår i yrket.    Utbildninge... | x |  |  | 16 | [sköta reception, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/K6XL_6aL_AQ6) |
| 8 | ...juder LIA-platser inom Region **Uppsala** och Folktandvården Stockholms... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 9 | ...on Uppsala och Folktandvården **Stockholms** läns AB.    Kurser:  Anatomi,... | x |  |  | 10 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 10 | ...esi och farmakologii, 25 yhp  **Etik**, bemötande och kommunikation,... |  | x |  | 4 | [Etik, **skill**](http://data.jobtechdev.se/taxonomy/concept/jqZ2_JnS_pZs) |
| 11 | ... Lärande i arbete 31,140 yhp  **Material, miljö och hantering av farligt avfall**, 20 yhp  Odontologi 1, 40 yhp... | x |  |  | 47 | [Miljövetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6n7M_QBo_ryQ) |
| 12 | ...,140 yhp  Material, miljö och **hantering av farligt avfall**, 20 yhp  Odontologi 1, 40 yhp... |  | x |  | 27 | [hantering av farligt avfall, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/s4Zp_iXB_48j) |
| 12 | ...,140 yhp  Material, miljö och **hantering av farligt avfall**, 20 yhp  Odontologi 1, 40 yhp... |  | x |  | 27 | [utveckla strategier för hantering av farligt avfall, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sGWR_RbS_oAW) |
| 13 | ...ng av farligt avfall, 20 yhp  **Odontologi** 1, 40 yhp  Odontologi 2, 30 y... | x |  |  | 10 | [Annan utbildning inom tandvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/J8Fy_QgV_Zin) |
| 13 | ...ng av farligt avfall, 20 yhp  **Odontologi** 1, 40 yhp  Odontologi 2, 30 y... |  | x |  | 10 | [Tandvård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/LXNM_AzU_2G5) |
| 14 | ...20 yhp  Odontologi 1, 40 yhp  **Odontologi** 2, 30 yhp  Oral hälsa 1, 25 y... | x |  |  | 10 | [Annan utbildning inom tandvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/J8Fy_QgV_Zin) |
| 15 | ...40 yhp  Odontologi 2, 30 yhp  **Oral hälsa** 1, 25 yhp  Oral hälsa 2, 30 y... | x |  |  | 10 | [Annan utbildning inom tandvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/J8Fy_QgV_Zin) |
| 16 | ...p  Odontologi 2, 30 yhp  Oral **hälsa** 1, 25 yhp  Oral hälsa 2, 30 y... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 17 | ...30 yhp  Oral hälsa 1, 25 yhp  **Oral hälsa** 2, 30 yhp  Tandvårdens verksa... | x |  |  | 10 | [Annan utbildning inom tandvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/J8Fy_QgV_Zin) |
| 18 | ...25 yhp  Oral hälsa 2, 30 yhp  **Tandvårdens verksamhet och organisation**, 15 yhp  Vårdhygien och smitt... | x |  |  | 39 | [Annan utbildning inom tandvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/J8Fy_QgV_Zin) |
| 19 | ...het och organisation, 15 yhp  **Vårdhygien och smittskydd**, 25 yhp | x |  |  | 25 | [Annan utbildning inom tandvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/J8Fy_QgV_Zin) |
| | **Overall** | | | **61** | **370** | 61/370 = **16%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [assistera tandläkaren vid tandvårdsbehandlingar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5LfH_HF6_dBU) |
| x |  |  | [Miljövetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6n7M_QBo_ryQ) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [Annan utbildning inom tandvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/J8Fy_QgV_Zin) |
| x |  |  | [sköta reception, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/K6XL_6aL_AQ6) |
|  | x |  | [Tandvård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/LXNM_AzU_2G5) |
| x | x | x | [Tandhygienist, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TPRY_Wze_q6x) |
| x |  |  | [Tandläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/TdvW_S8R_CFH) |
| x | x | x | [Tandsköterska, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ckkD_JhH_Sc6) |
|  | x |  | [Etik, **skill**](http://data.jobtechdev.se/taxonomy/concept/jqZ2_JnS_pZs) |
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
|  | x |  | [hantering av farligt avfall, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/s4Zp_iXB_48j) |
|  | x |  | [utveckla strategier för hantering av farligt avfall, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sGWR_RbS_oAW) |
|  | x |  | [tandläkare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/teYK_qML_UE3) |
| x | x | x | [föra journal, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wc5w_aw2_KBh) |
| | | **4** | 4/16 = **25%** |