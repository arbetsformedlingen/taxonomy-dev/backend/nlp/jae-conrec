# Results for '562ac00d3f5f1b9549207dfcb5e47a5136b6543e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [562ac00d3f5f1b9549207dfcb5e47a5136b6543e](README.md) | 1 | 3138 | 17 | 31 | 130/341 = **38%** | 8/28 = **29%** |

## Source text

CNC-operatör till GasiQ GasiQ är ett litet familjärt bolag där personalomsättningen är låg, nu finns den unika möjligheten att bli en del av deras team. Är du en driven och noggrann CNC-operatör med några års erfarenhet från branschen? Då är det dig vi söker!  Dina arbetsuppgifter GasiQs maskinpark är modern, ljus och välvårdad. De har en toppmodern maskin som är unik i sitt slag då det bara existerar två av de i Sverige idag.  Tjänsten som CNC-operatör innebär varierande och stimulerande arbetsuppgifter för dig som trivs i en verkstadsmiljö. Som CNC-operatör förekommer arbetsuppgifter som: 	Svarvning och fräsning i CNC-styrda maskiner 	Verktygsbyten, ställ och programmering 	Kvalitetskontroller och avsyning   Din profil Som person är du duktig på att samarbeta, ta initiativ och bidrar med en härlig energi. Du som söker har med fördel någon form av utbildning eller praktisk erfarenhet inom CNC, meriterande om du jobbat med siemens. Du har god förståelse och kunskap vid att läsa ritningar, är kvalitetsmedveten i allt du gör samt trivs med att arbeta i en självständig roll. Krav:  	God förståelse av ritningsläsning 	Erfarenhet/utbildning inom CNC   Övrig information 	Placeringsort: Stenkullen 	Start: Efter överenskommelse 	Varaktighet: Tillsvidare 	Arbetstid: Dagtid 	Lön: Efter överenskommelse   GasiQ har valt att samarbeta med Maxkompetens i denna rekryteringsprocess.  Har du frågor angående tjänsten så kontakta gärna ansvarig rekryterare på Maxkompetens, Emmi Persson på telefon 073 407 33 75.  Urval och intervjuer kommer ske löpande i denna process så ansök gärna på www.maxkompetens.se redan idag!  Ansökningar via e-post mottages ej på grund av GDPR.   Om Maxkompetens Maxkompetens är ett av Sveriges ledande rekrytering och bemanningsbolag sedan 2003. Vi har kunduppdrag i hela landet och kontor i; Stockholm, Växjö, Kristianstad, Halmstad, Borås & Göteborg. Varje år tillsätter vi över 1200 jobb och har 250 anställda. Våra kompetensområden är HR & Lön, Ekonomi & Finans, Kundtjänst & Administration, Marknad & Försäljning, It & Teknik, Inköp & Logistik, Industri.    Varje människa har en fantastisk potential. Det är viktigt att ta vara på alla människors kompetens och erbjuda dem möjligheten att uppnå sina drömmar. Genom omsorgsfullhet, väl fungerande rekryteringsprocesser och hög kompetens hjälper vi människor och företag att mötas och lyckas tillsammans. En bättre värld helt enkelt! Vi är ett auktoriserat bemanningsföretag med kollektivavtal, försäkringar, friskvård och tjänstepension.   Om kunden GasiQ är en helsvensk tillverkare av produkter för gassvetsning, lödning, skärning samt reglerutrustning. Våra produkter säljs genom grossister och specialister i Norden, Europa samt i många andra delar av världen.Företaget tog över verksamheten i februari 2007 från ELGA AB (ITW) som har tillverkat gasutrustning i över 70 år till den krävande industrin. GasIQ har även en omfattande Legotillverkning bland annat till VVS industrin.  Vi är certifierade enligt ISO 9001:2015 och ISO 14001:2015 av Lloyd's Register of Quality Assurance.  Vi finns i Stenkullen strax utanför Göteborg i Lerums Kommun.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **CNC-operatör** till GasiQ GasiQ är ett litet... | x | x | 12 | 12 | [CNC-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ezxx_3Jf_Li6) |
| 2 | ...ras team. Är du en driven och **noggrann** CNC-operatör med några års er... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 3 | ... Är du en driven och noggrann **CNC-operatör** med några års erfarenhet från... | x | x | 12 | 12 | [CNC-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ezxx_3Jf_Li6) |
| 4 | ...et bara existerar två av de i **Sverige** idag.  Tjänsten som CNC-opera... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 5 | ...i Sverige idag.  Tjänsten som **CNC-operatör** innebär varierande och stimul... | x | x | 12 | 12 | [CNC-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ezxx_3Jf_Li6) |
| 6 | ...rivs i en verkstadsmiljö. Som **CNC-operatör** förekommer arbetsuppgifter so... | x | x | 12 | 12 | [CNC-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ezxx_3Jf_Li6) |
| 7 | ...ekommer arbetsuppgifter som: 	**Svarvning** och fräsning i CNC-styrda mas... | x | x | 9 | 9 | [Svarvning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/d2yX_cZM_fSy) |
| 8 | ...uppgifter som: 	Svarvning och **fräsning** i CNC-styrda maskiner 	Verkty... | x | x | 8 | 8 | [Fräsning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/wpVJ_UXf_uSb) |
| 9 | ...ner 	Verktygsbyten, ställ och **programmering** 	Kvalitetskontroller och avsy... | x | x | 13 | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 10 | ...ten, ställ och programmering 	**Kvalitetskontroller** och avsyning   Din profil Som... |  | x |  | 19 | [Kvalitetskontroll, **skill**](http://data.jobtechdev.se/taxonomy/concept/4y7C_Lsh_EPM) |
| 11 | ...ring 	Kvalitetskontroller och **avsyning**   Din profil Som person är du... | x | x | 8 | 8 | [Avsyning, **skill**](http://data.jobtechdev.se/taxonomy/concept/X4EU_ZC7_QmK) |
| 12 | ...om person är du duktig på att **samarbeta**, ta initiativ och bidrar med ... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 13 | ...ller praktisk erfarenhet inom **CNC**, meriterande om du jobbat med... |  | x |  | 3 | [CNC, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zXvT_Gvb_mPj) |
| 14 | ... meriterande om du jobbat med **siemens**. Du har god förståelse och ku... | x |  |  | 7 | [CNC-programmering, Siemens, **skill**](http://data.jobtechdev.se/taxonomy/concept/B8dd_RVE_soo) |
| 15 | ...örståelse och kunskap vid att **läsa ritningar**, är kvalitetsmedveten i allt ... |  | x |  | 14 | [läsa vanliga ritningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4eLk_Z9j_rJZ) |
| 15 | ...örståelse och kunskap vid att **läsa ritningar**, är kvalitetsmedveten i allt ... | x | x | 14 | 14 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 16 | ...amt trivs med att arbeta i en **självständig** roll. Krav:  	God förståelse ... |  | x |  | 12 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 17 | ...ll. Krav:  	God förståelse av **ritningsläsning** 	Erfarenhet/utbildning inom C... | x | x | 15 | 15 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 18 | ...g 	Erfarenhet/utbildning inom **CNC**   Övrig information 	Placerin... |  | x |  | 3 | [CNC, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zXvT_Gvb_mPj) |
| 19 | ...överenskommelse 	Varaktighet: **Tillsvidare** 	Arbetstid: Dagtid 	Lön: Efte... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 20 | ...g i hela landet och kontor i; **Stockholm**, Växjö, Kristianstad, Halmsta... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 21 | ...ndet och kontor i; Stockholm, **Växjö**, Kristianstad, Halmstad, Borå... |  | x |  | 5 | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| 22 | ...h kontor i; Stockholm, Växjö, **Kristianstad**, Halmstad, Borås & Göteborg. ... |  | x |  | 12 | [Kristianstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/vrvW_sr8_1en) |
| 23 | ...ockholm, Växjö, Kristianstad, **Halmstad**, Borås & Göteborg. Varje år t... |  | x |  | 8 | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| 24 | ...istianstad, Halmstad, Borås & **Göteborg**. Varje år tillsätter vi över ... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 25 | ...kompetensområden är HR & Lön, **Ekonomi** & Finans, Kundtjänst & Admini... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 26 | ...r HR & Lön, Ekonomi & Finans, **Kundtjänst** & Administration, Marknad & F... |  | x |  | 10 | [Kundtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/aMyW_hB7_TpU) |
| 27 | ...äljning, It & Teknik, Inköp & **Logistik**, Industri.    Varje människa ... |  | x |  | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 28 | ...riserat bemanningsföretag med **kollektivavtal**, försäkringar, friskvård och ... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 29 | ... tillverkare av produkter för **gassvetsning**, lödning, skärning samt regle... |  | x |  | 12 | [EN 287-1, 311 Gassvetsning/ISO 9606-1, **skill**](http://data.jobtechdev.se/taxonomy/concept/VJNb_jou_bzn) |
| 29 | ... tillverkare av produkter för **gassvetsning**, lödning, skärning samt regle... |  | x |  | 12 | [Gassvetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/xShN_Vkr_s7D) |
| 30 | ...v produkter för gassvetsning, **lödning**, skärning samt reglerutrustni... |  | x |  | 7 | [lödningsmetoder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jBFV_VDW_GPb) |
| 31 | ...A AB (ITW) som har tillverkat **gasutrustning** i över 70 år till den krävand... |  | x |  | 13 | [sköta gasutrustning för drycker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VbdX_i6r_mAX) |
| 32 | ...ns i Stenkullen strax utanför **Göteborg** i Lerums Kommun. |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| | **Overall** | | | **130** | **341** | 130/341 = **38%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [läsa vanliga ritningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4eLk_Z9j_rJZ) |
|  | x |  | [Kvalitetskontroll, **skill**](http://data.jobtechdev.se/taxonomy/concept/4y7C_Lsh_EPM) |
|  | x |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [CNC-programmering, Siemens, **skill**](http://data.jobtechdev.se/taxonomy/concept/B8dd_RVE_soo) |
|  | x |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
|  | x |  | [EN 287-1, 311 Gassvetsning/ISO 9606-1, **skill**](http://data.jobtechdev.se/taxonomy/concept/VJNb_jou_bzn) |
|  | x |  | [sköta gasutrustning för drycker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VbdX_i6r_mAX) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Avsyning, **skill**](http://data.jobtechdev.se/taxonomy/concept/X4EU_ZC7_QmK) |
| x | x | x | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
|  | x |  | [Kundtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/aMyW_hB7_TpU) |
| x | x | x | [Svarvning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/d2yX_cZM_fSy) |
| x | x | x | [CNC-operatör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ezxx_3Jf_Li6) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
|  | x |  | [lödningsmetoder, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jBFV_VDW_GPb) |
|  | x |  | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Växjö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/mmot_H3A_auW) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [Kristianstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/vrvW_sr8_1en) |
| x | x | x | [Fräsning, CNC, **skill**](http://data.jobtechdev.se/taxonomy/concept/wpVJ_UXf_uSb) |
|  | x |  | [Gassvetsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/xShN_Vkr_s7D) |
|  | x |  | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
|  | x |  | [CNC, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zXvT_Gvb_mPj) |
| | | **8** | 8/28 = **29%** |