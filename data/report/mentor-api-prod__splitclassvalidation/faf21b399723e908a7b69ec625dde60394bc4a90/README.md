# Results for 'faf21b399723e908a7b69ec625dde60394bc4a90'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [faf21b399723e908a7b69ec625dde60394bc4a90](README.md) | 1 | 3396 | 29 | 61 | 240/767 = **31%** | 15/44 = **34%** |

## Source text

Projektör till ISG Nordic i Kungsbacka! Är du noggrann, har erfarenhet av AutoCad och ett intresse för teknik/säkerhet? Vi söker dig som vill vara med på ISG Nordic tillväxtresa som projektör i Kungsbacka på heltid.  ISG Nordic är specialister på integrerade intelligenta säkerhets- och kamerasystem. Företaget använder avancerade kameror/video/radar samt säkerhetssystem, och integrerar dessa in i kundernas egna verksamhetssystem, för att lösa viktiga uppgifter i deras verksamhet. Företagets kunder finns inom både offentliga och privata sektorn. ISG Nordic arbetar mot begreppet ”Tekniska kreatörer och Härliga människor” och värdesätter därför en arbetsplats där man arbetar mot satta mål, och samtidigt har kul på vägen. Till deras kontor i Kungsbacka söker de nu en kollega till teamet där man i en roll som projektör får goda utvecklingsmöjligheter i en fartfylld, spännande miljö och växande verksamhet!  I din roll som projektör på ISG Nordic kommer du att arbeta med projektering och konstruktion av el- och säkerhetssystem, detta innefattar bland annat passersystem, brandlarm, inbrottslarm och CCTV system. Ditt arbete kommer att innebära att du granskar och framställer teknisk dokumentation, ritningar och underlag för installationer av säkerhetsanläggningar. Samtidigt som ditt arbete innebär att du stöttar säljorganisation med material, samarbetar du också med projektledare för att tillsammans skapa arbetshandlingar till projektets säkerhetstekniker och installatörer. Om ändringar sker i samband med installation justerar du dokumentationen och upprättar relationshandlingar. Man driver egna ansvarsområden i projekten och är delaktig hela vägen från uppstart fram till leverans. Rollen som projektör kräver att du är tekniskt lagd, lösningsorienterad och kan hålla många bollar i luften samtidigt. Du behöver vara bekväm med att arbeta systematiskt och noggrant.  Rätt person kommer att anställas direkt av ISG Nordic och därmed tillhöra deras kontor i Kungsbacka, Göteborg. I denna tjänst kommer det att finnas goda möjligheter till en långsiktig anställning och även utvecklingsmöjligheter inom företaget.  Arbetstiderna är belagda mellan måndag-fredag kl. 08:00-17:00.  Din profil Du har relevant eftergymnasial utbildning inom exempelvis projektering, säkerhet/elteknik eller liknande tekniska områden. • Du är trygg med att kommunicera på svenska och engelska. • Du har erfarenhet av och behärskar AutoCad. • Du har en god datorvana. • Du har ett anmärkningsfritt belastningsregister (kontroller kommer göras i processen). • Är serviceinriktad, lösningsorienterad och har ett intresse för teknik/säkerhet. • Det är meriterande med erfarenhet av branschen, om du exempelvis är behörig ingenjör inom inbrottslarm eller CCTV, eller om du tidigare arbetat som elektriker, säkerhetstekniker eller med projektering.  Om företaget Mångfaldigt prisbelönta StudentConsulting är ett av Skandinaviens största och ledande rekryterings- och bemanningsföretag med fokus på studenter, akademiker och yrkesutbildade. Tack vare ett stort nätverk och lång erfarenhet har vi rekryterat över 11 000 personer det senaste året. Vi erbjuder intressanta och utmanande tjänster på både hel- och deltid inom områden som IT, teknik, ekonomi, administration, HR, marknadsföring, kundtjänst, försäljning, industri, produktion, logistik och transport. Hitta din framtid på www.studentconsulting.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Projektör till ISG Nordic i **Kungsbacka**! Är du noggrann, har erfarenh... | x | x | 10 | 10 | [Kungsbacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3JKV_KSK_x6z) |
| 2 | ...SG Nordic i Kungsbacka! Är du **noggrann**, har erfarenhet av AutoCad oc... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 3 | ...u noggrann, har erfarenhet av **AutoCad** och ett intresse för teknik/s... |  | x |  | 7 | [Allmänkonstruktion-AutoCAD, **skill**](http://data.jobtechdev.se/taxonomy/concept/K5JR_yfk_8oj) |
| 4 | ... tillväxtresa som projektör i **Kungsbacka** på heltid.  ISG Nordic är spe... | x | x | 10 | 10 | [Kungsbacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3JKV_KSK_x6z) |
| 5 | ...som projektör i Kungsbacka på **heltid**.  ISG Nordic är specialister ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 6 | ...r på integrerade intelligenta **säkerhets- och **kamerasystem. Företaget använd... |  | x |  | 15 | [övervaka planering av säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Dh2p_Fdc_4K8) |
| 6 | ...r på integrerade intelligenta **säkerhets- och **kamerasystem. Företaget använd... |  | x |  | 15 | [underhålla anläggningens säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EcC1_K6N_sVp) |
| 6 | ...r på integrerade intelligenta **säkerhets- och **kamerasystem. Företaget använd... |  | x |  | 15 | [underhålla säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gZSZ_3bG_GHL) |
| 6 | ...r på integrerade intelligenta **säkerhets- och **kamerasystem. Företaget använd... |  | x |  | 15 | [sköta tekniska säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qyk1_xqA_gid) |
| 6 | ...r på integrerade intelligenta **säkerhets- och **kamerasystem. Företaget använd... |  | x |  | 15 | [Lås- och säkerhetssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/txb5_eet_Huj) |
| 7 | ...Företaget använder avancerade **kameror**/video/radar samt säkerhetssys... |  | x |  | 7 | [kameror, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4ofB_Fhq_QwW) |
| 7 | ...Företaget använder avancerade **kameror**/video/radar samt säkerhetssys... |  | x |  | 7 | [förbjuda kameror, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4pob_58Y_cJa) |
| 7 | ...Företaget använder avancerade **kameror**/video/radar samt säkerhetssys... | x | x | 7 | 7 | [Fotoartiklar/Kameror, **skill**](http://data.jobtechdev.se/taxonomy/concept/QPQS_oPf_9RU) |
| 7 | ...Företaget använder avancerade **kameror**/video/radar samt säkerhetssys... |  | x |  | 7 | [förbereda kameror, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Tau3_nEc_APc) |
| 7 | ...Företaget använder avancerade **kameror**/video/radar samt säkerhetssys... |  | x |  | 7 | [montera kameror, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gnAx_cUm_Kjc) |
| 8 | ...nder avancerade kameror/video/**radar** samt säkerhetssystem, och int... | x |  |  | 5 | [radarutrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aYgv_veo_xjm) |
| 9 | ...rade kameror/video/radar samt **säkerhetssystem**, och integrerar dessa in i ku... |  | x |  | 15 | [övervaka planering av säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Dh2p_Fdc_4K8) |
| 9 | ...rade kameror/video/radar samt **säkerhetssystem**, och integrerar dessa in i ku... |  | x |  | 15 | [underhålla anläggningens säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EcC1_K6N_sVp) |
| 9 | ...rade kameror/video/radar samt **säkerhetssystem**, och integrerar dessa in i ku... |  | x |  | 15 | [underhålla säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gZSZ_3bG_GHL) |
| 9 | ...rade kameror/video/radar samt **säkerhetssystem**, och integrerar dessa in i ku... |  | x |  | 15 | [sköta tekniska säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qyk1_xqA_gid) |
| 9 | ...rade kameror/video/radar samt **säkerhetssystem**, och integrerar dessa in i ku... |  | x |  | 15 | [Lås- och säkerhetssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/txb5_eet_Huj) |
| 10 | ... har kul på vägen. Till deras **kontor** i Kungsbacka söker de nu en k... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 11 | ...på vägen. Till deras kontor i **Kungsbacka** söker de nu en kollega till t... | x | x | 10 | 10 | [Kungsbacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3JKV_KSK_x6z) |
| 12 | ...rdic kommer du att arbeta med **projektering** och konstruktion av el- och s... | x | x | 12 | 12 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 13 | ...jektering och konstruktion av **el- och **säkerhetssystem, detta innefat... |  | x |  | 8 | [Installationer och reparationer av elsystem till motorfordon utom motorcyklar, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4b8M_gCf_5p5) |
| 13 | ...jektering och konstruktion av **el- och **säkerhetssystem, detta innefat... |  | x |  | 8 | [flygplatsers elsystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7UPu_rox_bx8) |
| 13 | ...jektering och konstruktion av **el- och **säkerhetssystem, detta innefat... |  | x |  | 8 | [se till att ett mobilt elsystem är säkert, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FQiC_xow_6wu) |
| 13 | ...jektering och konstruktion av **el- och **säkerhetssystem, detta innefat... |  | x |  | 8 | [arbeta säkert med mobila elsystem under övervakning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hq2Y_kWc_rw6) |
| 13 | ...jektering och konstruktion av **el- och **säkerhetssystem, detta innefat... |  | x |  | 8 | [Elsystem, flyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q7Zu_TZq_CUi) |
| 14 | ...g och konstruktion av el- och **säkerhetssystem**, detta innefattar bland annat... |  | x |  | 15 | [övervaka planering av säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Dh2p_Fdc_4K8) |
| 14 | ...g och konstruktion av el- och **säkerhetssystem**, detta innefattar bland annat... |  | x |  | 15 | [underhålla anläggningens säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EcC1_K6N_sVp) |
| 14 | ...g och konstruktion av el- och **säkerhetssystem**, detta innefattar bland annat... |  | x |  | 15 | [underhålla säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gZSZ_3bG_GHL) |
| 14 | ...g och konstruktion av el- och **säkerhetssystem**, detta innefattar bland annat... |  | x |  | 15 | [sköta tekniska säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qyk1_xqA_gid) |
| 14 | ...g och konstruktion av el- och **säkerhetssystem**, detta innefattar bland annat... |  | x |  | 15 | [Lås- och säkerhetssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/txb5_eet_Huj) |
| 15 | ... detta innefattar bland annat **passersystem**, brandlarm, inbrottslarm och ... |  | x |  | 12 | [Passersystem, installationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/btbx_pYC_XDK) |
| 16 | ...tar bland annat passersystem, **brandlarm**, inbrottslarm och CCTV system... | x |  |  | 9 | [Brandlarm, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ib1W_p61_X6k) |
| 17 | ...nnat passersystem, brandlarm, **inbrottslarm** och CCTV system. Ditt arbete ... |  | x |  | 12 | [Mästarbrev, installatör - inbrottslarm, **skill**](http://data.jobtechdev.se/taxonomy/concept/4uR1_7ct_Tmi) |
| 17 | ...nnat passersystem, brandlarm, **inbrottslarm** och CCTV system. Ditt arbete ... |  | x |  | 12 | [Inbrottslarm, reparationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/C4Qy_ahB_v2u) |
| 17 | ...nnat passersystem, brandlarm, **inbrottslarm** och CCTV system. Ditt arbete ... |  | x |  | 12 | [Inbrottslarm, skötselvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/CCAj_Xaj_avU) |
| 17 | ...nnat passersystem, brandlarm, **inbrottslarm** och CCTV system. Ditt arbete ... |  | x |  | 12 | [reagera på inbrottslarm, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DBDa_izU_A5Z) |
| 17 | ...nnat passersystem, brandlarm, **inbrottslarm** och CCTV system. Ditt arbete ... |  | x |  | 12 | [Gesällbrev, installatör - inbrottslarm, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gxy1_dZ2_UFm) |
| 17 | ...nnat passersystem, brandlarm, **inbrottslarm** och CCTV system. Ditt arbete ... |  | x |  | 12 | [Inbrottslarm, installationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/TqmK_A4H_EAS) |
| 18 | ...t du granskar och framställer **teknisk dokumentation**, ritningar och underlag för i... | x | x | 21 | 21 | [Teknisk dokumentation, **skill**](http://data.jobtechdev.se/taxonomy/concept/AEgv_4gP_ndv) |
| 19 | ...rial, samarbetar du också med **projektledare** för att tillsammans skapa arb... | x | x | 13 | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 20 | ...etshandlingar till projektets **säkerhetstekniker** och installatörer. Om ändring... | x | x | 17 | 17 | [Säkerhetstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/upLS_W6r_KMv) |
| 21 | ...d att arbeta systematiskt och **noggrant**.  Rätt person kommer att anst... | x |  |  | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 22 | ...dic och därmed tillhöra deras **kontor** i Kungsbacka, Göteborg. I den... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 23 | ...ärmed tillhöra deras kontor i **Kungsbacka**, Göteborg. I denna tjänst kom... | x | x | 10 | 10 | [Kungsbacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3JKV_KSK_x6z) |
| 24 | ...ra deras kontor i Kungsbacka, **Göteborg**. I denna tjänst kommer det at... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 25 | ...  Din profil Du har relevant **eftergymnasial utbildning** inom exempelvis projektering,... | x | x | 25 | 25 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 26 | ...al utbildning inom exempelvis **projektering**, säkerhet/elteknik eller likn... | x | x | 12 | 12 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 27 | ...pelvis projektering, säkerhet/**elteknik** eller liknande tekniska områd... |  | x |  | 8 | [Projektledare, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1jm3_XFQ_TdJ) |
| 27 | ...pelvis projektering, säkerhet/**elteknik** eller liknande tekniska områd... | x | x | 8 | 8 | [Elteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/TGZ1_Xb5_77h) |
| 28 | ... trygg med att kommunicera på **svenska** och engelska. • Du har erfare... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 29 | ...tt kommunicera på svenska och **engelska**. • Du har erfarenhet av och b... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 30 | ...r erfarenhet av och behärskar **AutoCad**. • Du har en god datorvana. •... |  | x |  | 7 | [Allmänkonstruktion-AutoCAD, **skill**](http://data.jobtechdev.se/taxonomy/concept/K5JR_yfk_8oj) |
| 31 | ...lvis är behörig ingenjör inom **inbrottslarm** eller CCTV, eller om du tidig... |  | x |  | 12 | [Mästarbrev, installatör - inbrottslarm, **skill**](http://data.jobtechdev.se/taxonomy/concept/4uR1_7ct_Tmi) |
| 31 | ...lvis är behörig ingenjör inom **inbrottslarm** eller CCTV, eller om du tidig... |  | x |  | 12 | [Inbrottslarm, reparationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/C4Qy_ahB_v2u) |
| 31 | ...lvis är behörig ingenjör inom **inbrottslarm** eller CCTV, eller om du tidig... |  | x |  | 12 | [Inbrottslarm, skötselvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/CCAj_Xaj_avU) |
| 31 | ...lvis är behörig ingenjör inom **inbrottslarm** eller CCTV, eller om du tidig... |  | x |  | 12 | [reagera på inbrottslarm, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DBDa_izU_A5Z) |
| 31 | ...lvis är behörig ingenjör inom **inbrottslarm** eller CCTV, eller om du tidig... |  | x |  | 12 | [Gesällbrev, installatör - inbrottslarm, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gxy1_dZ2_UFm) |
| 31 | ...lvis är behörig ingenjör inom **inbrottslarm** eller CCTV, eller om du tidig... |  | x |  | 12 | [Inbrottslarm, installationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/TqmK_A4H_EAS) |
| 32 | ...igare arbetat som elektriker, **säkerhetstekniker** eller med projektering.  Om f... | x | x | 17 | 17 | [Säkerhetstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/upLS_W6r_KMv) |
| 33 | ..., säkerhetstekniker eller med **projektering**.  Om företaget Mångfaldigt pr... | x | x | 12 | 12 | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| 34 | ...- och deltid inom områden som **IT**, teknik, ekonomi, administrat... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 35 | ... inom områden som IT, teknik, **ekonomi**, administration, HR, marknads... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 36 | ... ekonomi, administration, HR, **marknadsföring**, kundtjänst, försäljning, ind... | x |  |  | 14 | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| 37 | ...stration, HR, marknadsföring, **kundtjänst**, försäljning, industri, produ... | x | x | 10 | 10 | [Kundtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/aMyW_hB7_TpU) |
| 38 | ...ljning, industri, produktion, **logistik** och transport. Hitta din fram... | x | x | 8 | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| | **Overall** | | | **240** | **767** | 240/767 = **31%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Projektledare, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1jm3_XFQ_TdJ) |
| x | x | x | [Kungsbacka, **municipality**](http://data.jobtechdev.se/taxonomy/concept/3JKV_KSK_x6z) |
|  | x |  | [Installationer och reparationer av elsystem till motorfordon utom motorcyklar, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4b8M_gCf_5p5) |
|  | x |  | [kameror, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4ofB_Fhq_QwW) |
|  | x |  | [förbjuda kameror, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4pob_58Y_cJa) |
|  | x |  | [Mästarbrev, installatör - inbrottslarm, **skill**](http://data.jobtechdev.se/taxonomy/concept/4uR1_7ct_Tmi) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [flygplatsers elsystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7UPu_rox_bx8) |
| x | x | x | [Teknisk dokumentation, **skill**](http://data.jobtechdev.se/taxonomy/concept/AEgv_4gP_ndv) |
|  | x |  | [Inbrottslarm, reparationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/C4Qy_ahB_v2u) |
|  | x |  | [Inbrottslarm, skötselvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/CCAj_Xaj_avU) |
|  | x |  | [reagera på inbrottslarm, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DBDa_izU_A5Z) |
|  | x |  | [övervaka planering av säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Dh2p_Fdc_4K8) |
|  | x |  | [underhålla anläggningens säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/EcC1_K6N_sVp) |
|  | x |  | [se till att ett mobilt elsystem är säkert, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FQiC_xow_6wu) |
|  | x |  | [Gesällbrev, installatör - inbrottslarm, **skill**](http://data.jobtechdev.se/taxonomy/concept/Gxy1_dZ2_UFm) |
|  | x |  | [arbeta säkert med mobila elsystem under övervakning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hq2Y_kWc_rw6) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
|  | x |  | [Allmänkonstruktion-AutoCAD, **skill**](http://data.jobtechdev.se/taxonomy/concept/K5JR_yfk_8oj) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
|  | x |  | [Elsystem, flyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q7Zu_TZq_CUi) |
| x | x | x | [Fotoartiklar/Kameror, **skill**](http://data.jobtechdev.se/taxonomy/concept/QPQS_oPf_9RU) |
| x | x | x | [Elteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/TGZ1_Xb5_77h) |
|  | x |  | [förbereda kameror, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Tau3_nEc_APc) |
|  | x |  | [Inbrottslarm, installationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/TqmK_A4H_EAS) |
| x | x | x | [Kundtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/aMyW_hB7_TpU) |
| x |  |  | [radarutrustning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aYgv_veo_xjm) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
|  | x |  | [Passersystem, installationsvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/btbx_pYC_XDK) |
|  | x |  | [underhålla säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gZSZ_3bG_GHL) |
|  | x |  | [montera kameror, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gnAx_cUm_Kjc) |
| x |  |  | [Brandlarm, **keyword**](http://data.jobtechdev.se/taxonomy/concept/ib1W_p61_X6k) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
|  | x |  | [sköta tekniska säkerhetssystem, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qyk1_xqA_gid) |
|  | x |  | [Lås- och säkerhetssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/txb5_eet_Huj) |
| x | x | x | [Säkerhetstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/upLS_W6r_KMv) |
| x | x | x | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| x | x | x | [Projektering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zRtu_fYj_XC4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x |  |  | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| | | **15** | 15/44 = **34%** |