# Results for 'bbe2d55264b6a48cd5bde933bc225c0cb963222b'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [bbe2d55264b6a48cd5bde933bc225c0cb963222b](README.md) | 1 | 2155 | 18 | 9 | 18/441 = **4%** | 1/20 = **5%** |

## Source text

Akademi Båstad Yrkeshögskola utbildar framtidens ledare inom affärsutveckling, digital transformation och Revenue Management.    Utbildningen riktar sig främst till personer i besöksnäringen som önskar personlig utveckling, bli tryggare i sin ledarroll eller är redo att ta en större ansvarsroll och ta ett steg vidare i karriären.   Utbildningen riktar sig även till arbetsgivare som vill kompetensutveckla och lyfta sin personal. Med utbildningen har ni arbetsgivare möjligheten att få framtidens ledare som får verksamheten att växa på ett hållbart sätt!    Du lär dig mer om hur Revenue Management, räkna på hur du kan optimera din verksamhet och öka intäkterna och när du arbetat igenom affärsplanen och förstår din business går ni in på nya sätt att sälja och tillsammans med verktyg inom ledarskap förstå hur du motiverar dig själv, ditt team och verksamheten i ett förändringsarbete. Idag gäller det att konstant utveckla din verksamhet och dig själv på det personliga planet. Många utvecklas på det personliga planet och känner sig tryggare och säkrare i sin arbetsroll efter utbildningen.  Som operativ ledare är det viktigt att få en helhetsyn på ekonomin med fokus på optimering och lönsamhet för att kunna ta lönsamma beslut. Därför har vi valt ut det bästa och viktigaste som krävs för denna yrkesroll och det får du genom kurserna Operativt ledarskap, Ekonomi - Revenue Management, Affärsstrategi och Projektarbete inom operativt ledarskap.    Du kan med fördel kombinera ditt ordinarie arbete med studier. Under studieåret lär du dig hur olika faktorer påverkar en verksamhet, hur utvecklingsstrategier skapas och du får verktyg för hur du och dina medarbetare ska arbeta för att nå uppsatta mål på ett hållbart sätt.    Efter utbildningen kan du exempelvis jobba som  • Operativ ledare inom en verksamhet inom besöksnäringen  • Revenue Manager - junior  • Chef, platsansvarig, anläggningsansvarig, avdelningschef på en mindre, medelstort eller stort företag inom besöksnäringen.  • Fortsätta och utveckla din egna verksamhet inom besöksnäringen    Hör en av våra tidigare studerande och arbetsgivare berätta: https://youtu.be/vuh4GbmgNIA 

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...shögskola utbildar framtidens **ledare inom affärsutveckling**, digital transformation och R... | x |  |  | 28 | [Affärsutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Zwhm_5tT_Zms) |
| 2 | ...bildar framtidens ledare inom **affärsutveckling**, digital transformation och R... |  | x |  | 16 | [Affärsutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TzHP_38y_mhm) |
| 3 | ...g, digital transformation och **Revenue Management**.    Utbildningen riktar sig f... | x |  |  | 18 | [Manager, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/X9jv_K2b_m48) |
| 3 | ...g, digital transformation och **Revenue Management**.    Utbildningen riktar sig f... |  | x |  | 18 | [Revenue Management/Yield management, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZxKX_dN4_8DJ) |
| 4 | ...r i besöksnäringen som önskar **personlig utveckling**, bli tryggare i sin ledarroll... |  | x |  | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 5 | ...ätt!    Du lär dig mer om hur **Revenue Management**, räkna på hur du kan optimera... | x | x | 18 | 18 | [Revenue Management/Yield management, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZxKX_dN4_8DJ) |
| 6 | ...roll efter utbildningen.  Som **operativ ledare** är det viktigt att få en helh... | x |  |  | 15 | [Operativ ledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aB7R_JQN_sDE) |
| 7 | ...och det får du genom kurserna **Operativt ledarskap**, Ekonomi - Revenue Management... | x |  |  | 19 | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
| 8 | ...kurserna Operativt ledarskap, **Ekonomi** - Revenue Management, Affärss... | x |  |  | 7 | [Ekonomi, undervisning högskolenivå, **skill**](http://data.jobtechdev.se/taxonomy/concept/9Ra9_2yh_jH4) |
| 8 | ...kurserna Operativt ledarskap, **Ekonomi** - Revenue Management, Affärss... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 9 | ...perativt ledarskap, Ekonomi - **Revenue Management**, Affärsstrategi och Projektar... | x |  |  | 18 | [Företagsekonomi, organisation och management, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/Z73N_8Zf_dqt) |
| 9 | ...perativt ledarskap, Ekonomi - **Revenue Management**, Affärsstrategi och Projektar... |  | x |  | 18 | [Revenue Management/Yield management, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZxKX_dN4_8DJ) |
| 10 | ...Ekonomi - Revenue Management, **Affärsstrategi** och Projektarbete inom operat... | x |  |  | 14 | [Annan utbildning i ledning och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/CGF1_o4T_wCo) |
| 11 | ...anagement, Affärsstrategi och **Projektarbete inom operativt ledarskap**.    Du kan med fördel kombine... | x |  |  | 38 | [Annan utbildning i ledning och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/CGF1_o4T_wCo) |
| 12 | ...der studieåret lär du dig hur **olika faktorer påverkar en verksamhet**, hur utvecklingsstrategier sk... | x |  |  | 37 | [Verksamhetsanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/G7Qq_r7L_5Jo) |
| 13 | ...r påverkar en verksamhet, hur **utvecklingsstrategier** skapas och du får verktyg för... | x |  |  | 21 | [utföra strategisk planering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/JP7Q_2aZ_6ax) |
| 14 | ...an du exempelvis jobba som  • **Operativ ledare** inom en verksamhet inom besök... | x |  |  | 15 | [Manager, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/X9jv_K2b_m48) |
| 15 | ...samhet inom besöksnäringen  • **Revenue Manager** - junior  • Chef, platsansvar... |  | x |  | 15 | [revenue manager, hotellbranschen, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/PS4Y_zbR_E7W) |
| 15 | ...samhet inom besöksnäringen  • **Revenue Manager** - junior  • Chef, platsansvar... | x |  |  | 15 | [Manager, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/X9jv_K2b_m48) |
| 15 | ...samhet inom besöksnäringen  • **Revenue Manager** - junior  • Chef, platsansvar... |  | x |  | 15 | [Revenue manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/uc75_iiG_N9M) |
| 16 | ...• Revenue Manager - junior  • **Chef**, platsansvarig, anläggningsan... | x |  |  | 4 | [Chefer och verksamhetsledare, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/bh3H_Y3h_5eD) |
| 17 | ...nue Manager - junior  • Chef, **platsansvarig**, anläggningsansvarig, avdelni... | x |  |  | 13 | [platschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/7ffE_ZTJ_Mix) |
| 18 | ...unior  • Chef, platsansvarig, **anläggningsansvarig**, avdelningschef på en mindre,... |  | x |  | 19 | [Anläggningsansvarig, **job-title**](http://data.jobtechdev.se/taxonomy/concept/XwYL_Do3_SAx) |
| 18 | ...unior  • Chef, platsansvarig, **anläggningsansvarig**, avdelningschef på en mindre,... | x |  |  | 19 | [Platschef, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fCCr_Mp8_6oE) |
| 19 | ...nsvarig, anläggningsansvarig, **avdelningschef** på en mindre, medelstort elle... | x |  |  | 14 | [avdelningschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/9Aoj_BL5_m3b) |
| | **Overall** | | | **18** | **441** | 18/441 = **4%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [platschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/7ffE_ZTJ_Mix) |
| x |  |  | [avdelningschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/9Aoj_BL5_m3b) |
| x |  |  | [Ekonomi, undervisning högskolenivå, **skill**](http://data.jobtechdev.se/taxonomy/concept/9Ra9_2yh_jH4) |
| x |  |  | [Ledarskap, organisation och styrning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/AUNw_Uki_1ig) |
| x |  |  | [Annan utbildning i ledning och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/CGF1_o4T_wCo) |
| x |  |  | [Verksamhetsanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/G7Qq_r7L_5Jo) |
| x |  |  | [utföra strategisk planering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/JP7Q_2aZ_6ax) |
|  | x |  | [revenue manager, hotellbranschen, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/PS4Y_zbR_E7W) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
|  | x |  | [Affärsutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TzHP_38y_mhm) |
| x |  |  | [Manager, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/X9jv_K2b_m48) |
|  | x |  | [Anläggningsansvarig, **job-title**](http://data.jobtechdev.se/taxonomy/concept/XwYL_Do3_SAx) |
| x |  |  | [Företagsekonomi, organisation och management, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/Z73N_8Zf_dqt) |
| x |  |  | [Affärsutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Zwhm_5tT_Zms) |
| x | x | x | [Revenue Management/Yield management, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZxKX_dN4_8DJ) |
| x |  |  | [Operativ ledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aB7R_JQN_sDE) |
| x |  |  | [Chefer och verksamhetsledare, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/bh3H_Y3h_5eD) |
| x |  |  | [Platschef, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fCCr_Mp8_6oE) |
|  | x |  | [Revenue manager, **job-title**](http://data.jobtechdev.se/taxonomy/concept/uc75_iiG_N9M) |
|  | x |  | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| | | **1** | 1/20 = **5%** |