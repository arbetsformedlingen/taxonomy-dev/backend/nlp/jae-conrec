# Results for '975efbd24abd8f41fec487461649aa25dce870b8'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [975efbd24abd8f41fec487461649aa25dce870b8](README.md) | 1 | 1796 | 16 | 12 | 85/190 = **45%** | 8/16 = **50%** |

## Source text

Vi söker Operatörer till Combi Wear Parts i Ljungby Till vår kund Combi Wear Parts i Ljungby söker vi flera Operatörer! Combi Wear Parts erbjuder ett stabilt företag med produktion av världsledande patenterade produkter. Multikulturellt och internationellt företagsklimat. Komplex tillverkningsprocess med gammal tradition i modern tappning. Combi Wear Parts bryr sig om varje individ och förstår att summan av vad var och en presterar blir till en bra produkt och fint varumärke. Varje individ har möjlighet till ett varierat arbete med rotation och utveckling. God sammanhållning och bra kamratskap. Ett företag med framåtanda och visioner.   Läs mer på www.combiwearparts.com   Operatörer sökes till följande avdelningar inom produktionsenheterna:   Gjutning   Kärnmakeri/Formning   Svetsning/Slipning   Kontroll   Din profil  Vi söker dig som är intresserad av nya utmaningar. Vi lägger stor vikt vid personlig lämplighet. Som person har du ett engagemang och en god förmåga att effektivt driva ditt arbete framåt. Du har lätt för att samarbeta i team och är bra på att planera och organisera ditt arbete. Du är angelägen om att nå resultat och hålla deadlines med hög kvalitet.   Är du vår nya kollega? Du behärskar svenska i tal och skrift.   Meriterande kvalifikationer:   Gjuterierfarenhet   Erfarenhet av metallurgiska verksamheter   Ritningsläsning   Truckkort   Traverskort    Tjänsten är en hyrrekrytering, som för rätt person erbjuder en tillsvidareanställning.  Vi rekryterar löpande och tjänsten kan därför komma att tillsättas innan ansökningstiden har gått ut. Då behovet hos kund är omgående är du varmt välkommen med din ansökan redan idag!  Du ansöker direkt på www.empleo.se  Uppstart: Omgående Ort/Placering: Ljungby Omfattning: Heltid  Ansökningstid: Senast 19 augusti 2022

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...törer till Combi Wear Parts i **Ljungby** Till vår kund Combi Wear Part... | x | x | 7 | 7 | [Ljungby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/GzKo_S48_QCm) |
| 2 | ...l vår kund Combi Wear Parts i **Ljungby** söker vi flera Operatörer! Co... | x | x | 7 | 7 | [Ljungby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/GzKo_S48_QCm) |
| 3 | ... inom produktionsenheterna:   **Gjutning**   Kärnmakeri/Formning   Svets... | x | x | 8 | 8 | [gjutning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/TAet_KRY_icS) |
| 4 | ...oduktionsenheterna:   Gjutning** **  Kärnmakeri/Formning   Svetsn... | x |  |  | 1 | [gjutning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/TAet_KRY_icS) |
| 5 | ...erna:   Gjutning   Kärnmakeri/**Formning**   Svetsning/Slipning   Kontro... | x | x | 8 | 8 | [typer av formning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/grAD_2aB_bqN) |
| 6 | ...Gjutning   Kärnmakeri/Formning** **  Svetsning/Slipning   Kontrol... | x |  |  | 1 | [typer av formning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/grAD_2aB_bqN) |
| 7 | ...tning   Kärnmakeri/Formning   **Svetsning**/Slipning   Kontroll   Din pro... | x | x | 9 | 9 | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| 8 | ...rnmakeri/Formning   Svetsning/**Slipning**   Kontroll   Din profil  Vi s... | x | x | 8 | 8 | [Slipning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/8TGJ_EdB_b7b) |
| 9 | .../Formning   Svetsning/Slipning** **  Kontroll   Din profil  Vi sö... | x |  |  | 1 | [Slipning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/8TGJ_EdB_b7b) |
| 10 | ...e framåt. Du har lätt för att **samarbeta** i team och är bra på att plan... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 11 | ...och är bra på att planera och **organisera** ditt arbete. Du är angelägen ... | x |  |  | 10 | [arbeta organiserat, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/he5L_b6P_QN8) |
| 12 | ...vår nya kollega? Du behärskar **svenska** i tal och skrift.   Meriteran... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 13 | ...eriterande kvalifikationer:   **Gjuterierfarenhet**   Erfarenhet av metallurgiska... |  | x |  | 17 | [Gjuteritekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/SyqF_4hx_APM) |
| 14 | ...erierfarenhet   Erfarenhet av **metallurgiska** verksamheter   Ritningsläsnin... | x |  |  | 13 | [metallurgi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tc4J_jPq_EF9) |
| 15 | ... metallurgiska verksamheter   **Ritningsläsning**   Truckkort   Traverskort    ... | x | x | 15 | 15 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 16 | ...ksamheter   Ritningsläsning   **Truckkort**   Traverskort    Tjänsten är ... | x | x | 9 | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 17 | ...   Ritningsläsning   Truckkort** **  Traverskort    Tjänsten är e... | x |  |  | 1 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 18 | ...Ritningsläsning   Truckkort   **Traverskort**    Tjänsten är en hyrrekryter... |  | x |  | 11 | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
| 19 | ...Ritningsläsning   Truckkort   **Traverskort **   Tjänsten är en hyrrekryteri... | x |  |  | 12 | [Traversförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/mBA1_h3T_Aaw) |
| 20 | ...m för rätt person erbjuder en **tillsvidareanställning**.  Vi rekryterar löpande och t... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 21 | ...tart: Omgående Ort/Placering: **Ljungby** Omfattning: Heltid  Ansökning... | x | x | 7 | 7 | [Ljungby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/GzKo_S48_QCm) |
| 22 | ...lacering: Ljungby Omfattning: **Heltid ** Ansökningstid: Senast 19 augu... | x |  |  | 7 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **85** | **190** | 85/190 = **45%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Slipning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/8TGJ_EdB_b7b) |
| x | x | x | [Svets, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Emhe_ce6_MYC) |
| x | x | x | [Ljungby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/GzKo_S48_QCm) |
|  | x |  | [Traverskörning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LhvF_NZc_r6N) |
|  | x |  | [Gjuteritekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/SyqF_4hx_APM) |
| x | x | x | [gjutning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/TAet_KRY_icS) |
| x | x | x | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| x | x | x | [typer av formning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/grAD_2aB_bqN) |
| x |  |  | [arbeta organiserat, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/he5L_b6P_QN8) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [Traversförare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/mBA1_h3T_Aaw) |
| x |  |  | [metallurgi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tc4J_jPq_EF9) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/16 = **50%** |