# Results for '970e89f1793568737aefbbedc4a98087514df6cb'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [970e89f1793568737aefbbedc4a98087514df6cb](README.md) | 1 | 1692 | 8 | 10 | 55/149 = **37%** | 5/11 = **45%** |

## Source text

Maskinoperatör Om tjänsten Vi söker en maskinoperatör till en av våra kunder i Halmstad området.   Dina arbetsuppgifter kommer innebära operera stansmaskin och bockning. även arbete efter ritningar och att justera maskin kommer vara en del av arbetsuppgifterna.  Vi ser gärna att du har tidigare erfarenhet av liknande arbete.   Starttid: Omgående eller enligt överrenskommelse.  Arbetstider: Dagtid      Din profil Önskemål/ krav   - Tidigare erfarenhet.   Som person vill vi att du är ansvarstagande, målinriktad, flexibel, positiv, ordningsam och har en god samarbetsförmåga eftersom arbetet sker ute hos våra kunder. Vi arbetar löpande med inkomna ansökningar och kontaktar endast de som motsvarar ansökningskraven.  Välkommen med din ansökan idag!  Om Ikett Personalpartner AB Ikett Personalpartner AB är ett auktoriserat bemanningsföretag som arbetar med uthyrning och rekrytering av kvalificerad kollektivpersonal och tjänstemän. Främst inom yrkesområdena industri, el, bygg och administration.   Som konsult hos Ikett har du möjligheten att söka lokala arbeten och researbeten i Sverige samt Norge. Vi arbetar kontinuerligt för att du och kunden ska trivas så bra som möjligt med varandra och för att era önskemål ska tillgodoses.   Ikett är medlem i Almega Kompetensföretagen och har kollektivavtal med LO, Unionen och Akademikerförbunden. Ikett är certifierat enligt ISO 9001.  Målet med vårt arbete är nöjda kunder och skickliga medarbetare som trivs. Detta vill vi uppnå genom kvalitet, kompetens och effektivitet.  För mer information och nyheter, följ oss gärna på:  Facebook: www.facebook.com/ikettpersonalpartner  LinkedIn: www.linkedin.com/company/ikett-personalpartner-ab-as

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Maskinoperatör** Om tjänsten Vi söker en maski... | x |  |  | 14 | [Process- och maskinoperatörer, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/XnLr_FQp_c82) |
| 1 | **Maskinoperatör** Om tjänsten Vi söker en maski... |  | x |  | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 2 | ...ratör Om tjänsten Vi söker en **maskinoperatör** till en av våra kunder i Halm... | x |  |  | 14 | [Process- och maskinoperatörer, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/XnLr_FQp_c82) |
| 2 | ...ratör Om tjänsten Vi söker en **maskinoperatör** till en av våra kunder i Halm... |  | x |  | 14 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 3 | ...atör till en av våra kunder i **Halmstad** området.   Dina arbetsuppgift... | x | x | 8 | 8 | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| 4 | ...ifter kommer innebära operera **stansmaskin** och bockning. även arbete eft... |  | x |  | 11 | [sköta stansmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/C46y_Cdx_rnv) |
| 4 | ...ifter kommer innebära operera **stansmaskin** och bockning. även arbete eft... | x | x | 11 | 11 | [använda stansmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MMuh_d9u_myu) |
| 4 | ...ifter kommer innebära operera **stansmaskin** och bockning. även arbete eft... |  | x |  | 11 | [typer av stansmaskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Yseh_MV1_gub) |
| 5 | ...ebära operera stansmaskin och **bockning**. även arbete efter ritningar ... | x | x | 8 | 8 | [Bockning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/9VNa_4MW_EYC) |
| 6 | ...h bockning. även arbete efter **ritningar** och att justera maskin kommer... | x |  |  | 9 | [ritningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/RcYZ_omY_is7) |
| 7 | ... Som person vill vi att du är **ansvarstagande**, målinriktad, flexibel, posit... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 8 | ...ala arbeten och researbeten i **Sverige** samt Norge. Vi arbetar kontin... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 9 | ...ga Kompetensföretagen och har **kollektivavtal** med LO, Unionen och Akademike... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **55** | **149** | 55/149 = **37%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Bockning, konventionell maskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/9VNa_4MW_EYC) |
|  | x |  | [sköta stansmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/C46y_Cdx_rnv) |
| x | x | x | [använda stansmaskin, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MMuh_d9u_myu) |
| x |  |  | [ritningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/RcYZ_omY_is7) |
| x |  |  | [Process- och maskinoperatörer, **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/XnLr_FQp_c82) |
|  | x |  | [typer av stansmaskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Yseh_MV1_gub) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **5** | 5/11 = **45%** |