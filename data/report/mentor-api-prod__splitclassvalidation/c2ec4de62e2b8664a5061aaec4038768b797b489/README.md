# Results for 'c2ec4de62e2b8664a5061aaec4038768b797b489'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [c2ec4de62e2b8664a5061aaec4038768b797b489](README.md) | 1 | 1945 | 9 | 4 | 31/90 = **34%** | 4/7 = **57%** |

## Source text

Pythonutvecklare till bolag med spetskompetens inom IT Erfaren pythonutvecklare med vana av django och intresse för arkitektur. Skapa tekniklösningar med fokus på långsiktig positiv påverkan på samhället som tar företaget hela vägen till toppen. Drakryggen skapar tekniklösningar med människan i fokus och vill att det arbete de gör ska bidra till en bättre framtid på ett eller annat sätt. Med expertkompetens inom mjukvaruutveckling och ledarskap drivs de av viljan att utveckla och förbättra sig själva, sina kunders verksamheter och omvärlden i stort. De vill gärna att allt de gör ska vara antingen roligt, viktigt eller utvecklande. Uppfyller projekten alla tre känner de att de verkligen har lyckats och de hoppas att du känner detsamma. Som pythonutvecklare hos Drakryggen kommer du få jobba med Django, AWS och Rest APIer, så känner du dig hemma i dessa kan detta vara rollen för dig. Du arbetar i ett långsiktigt uppdrag där du tillsammans med dina kollegor kommer lösa problem genom att skriva kod som skapar värde i den kontext den ska användas.  Kompetenser Erfarenhet av utveckling i Python och Django samt erfarenhet av API-utveckling gör dig god för den här rollen. Har du dessutom med dig erfarenhet av cloud lösningar och e-com lösningar så är det extra meriterande. Personliga egenskaper Då du blir en del av ett litet team är det väsentligt om du en stjärna på att samarbeta och anpassa dig till olika situationer samtidigt som du är villig och intresserad av att dela med dig av din kunskap till andra runt omkring dig. Du får gärna identifiera dig med egenskaper som ödmjuk och positiv samtidigt som du har fötterna stadigt på jorden. — Gå in direkt och gör en anonym lönematchning: https://jobnet.se/pythonutvecklare-drakryggen?utm_source=arbetsformedlingen&utm_medium=annons&utm_content=drakryggen&utm_term=pythonutvecklare --- Rekryteringen sker på Jobnets plattform där du även hittar bilder, video och mer information.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Pythonutvecklare** till bolag med spetskompetens... | x |  |  | 16 | [Python (datorprogrammering), **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hQrd_6zY_p1y) |
| 2 | ...bolag med spetskompetens inom **IT** Erfaren pythonutvecklare med ... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 3 | ...petskompetens inom IT Erfaren **pythonutvecklare** med vana av django och intres... | x |  |  | 16 | [Python (datorprogrammering), **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hQrd_6zY_p1y) |
| 4 | ...ätt. Med expertkompetens inom **mjukvaruutveckling** och ledarskap drivs de av vil... | x | x | 18 | 18 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 5 | ...s att du känner detsamma. Som **pythonutvecklare** hos Drakryggen kommer du få j... | x |  |  | 16 | [Python (datorprogrammering), **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hQrd_6zY_p1y) |
| 6 | ...ommer du få jobba med Django, **AWS** och Rest APIer, så känner du ... | x | x | 3 | 3 | [Amazon Web Services (AWS), molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/KRDJ_nhm_wLb) |
| 7 | ... få jobba med Django, AWS och **Rest** APIer, så känner du dig hemma... | x | x | 4 | 4 | [REST, **skill**](http://data.jobtechdev.se/taxonomy/concept/jTYU_TTp_4iW) |
| 8 | ...er Erfarenhet av utveckling i **Python** och Django samt erfarenhet av... | x | x | 6 | 6 | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| 9 | ...tligt om du en stjärna på att **samarbeta** och anpassa dig till olika si... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| | **Overall** | | | **31** | **90** | 31/90 = **34%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| x | x | x | [Amazon Web Services (AWS), molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/KRDJ_nhm_wLb) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Python (datorprogrammering), **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hQrd_6zY_p1y) |
| x | x | x | [REST, **skill**](http://data.jobtechdev.se/taxonomy/concept/jTYU_TTp_4iW) |
| x | x | x | [Python, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/qfkh_ZRK_w4W) |
| | | **4** | 4/7 = **57%** |