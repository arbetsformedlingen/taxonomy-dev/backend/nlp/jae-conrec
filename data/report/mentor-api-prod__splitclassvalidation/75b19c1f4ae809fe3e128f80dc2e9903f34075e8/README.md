# Results for '75b19c1f4ae809fe3e128f80dc2e9903f34075e8'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [75b19c1f4ae809fe3e128f80dc2e9903f34075e8](README.md) | 1 | 3132 | 16 | 21 | 92/424 = **22%** | 8/21 = **38%** |

## Source text

Transportplanerare till Ragn-Sells i Jönköping Vill du kunna göra skillnad genom ditt dagliga arbete? Som medarbetare på Ragn-Sells gör du det, oavsett roll inom företaget. Vårt arbete bidrar nämligen till ett bättre samhälle och en bättre miljö. Dessutom verkar vi inom en spännande framtidsbransch där vår gedigna historia och stora kunnande är en enorm styrka. Som företag vill vi vara ett bevis på att omsorg om jorden och bra affärer går hand i hand. Nu söker vi en Transportplanerare till Ragn-Sells Recyclings Region Syd Öst som har erfarenhet av transportplanering och logistikfrågor. Om du är analytisk, strukturerad och trivs i samarbetet med andra samt har ett intresse för miljöfrågor kan det här vara nästa utmaning för dig! Intervjuer och urval sker löpande. Varmt välkommen med din ansökan redan idag!   Om tjänsten Arbetet innebär i huvudsak att:  - Planera den operativa driften av transporterna för uppdrag inom regionen - både egna fordon och underentreprenörer - Hantera och planera ut arbetsorder på rätt resurs utifrån uppdragets krav - Skapa nya synergieffekter då du planerar fordon i en större geografi - Vid behov göra översyn och ruttoptimering. Du identifierar och rapporterar reklamationer och avvikelser samt tillser att företagets system för kvalitet, miljö- och arbetsmiljöfrågor följs i samband med transportplaneringen - Ha daglig kontakt med kunder, chaufförer, sektionschefer, kundtjänstmedarbetare samt övriga kollegor  Vi erbjuder dig ett arbete med logistik inom en av Sveriges viktigaste branscher, goda anställningsvillkor med kollektivavtal, pension och friskvårdsbidrag. Ragn-Sells erbjuder även för rätt person kompetensutvecklingsmöjligheter genom egen Akademi. Tjänsten är en visstidanställning på 1 år med start senast oktober. För rätt person finns stora möjligheter till fast anställning hos Ragn-Sells.   Vem är du? Vi söker dig som har tidigare erfarenhet från arbete med logistikstöd eller kundservice inom logistik-eller transportbranschen. Du har relevant utbildning inom logistik, eller erfarenhet som bedöms likvärdig. Det är meriterande med erfarenhet från återvinningsbranschen.  Som person är du strukturerad, kund- och lösningsorienterad. Du kommunicerar väl och obehindrat på svenska och har lätt för att samarbeta och skapa kontaktytor såväl externt som internt. Du är engagerad och vill bidra till utveckling och effektivisering. Vidare är du tydlig, flexibel och klarar snabba omställningar med bibehållen kvalitet och engagemang. Naturligtvis har du ett intresse för miljöfrågor. Vi lägger stor vikt vid personlig lämplighet.   Om verksamheten Ragn-Sells är ett av Sveriges främsta kompetensföretag inom återvinning och miljö. Vi samlar in, behandlar och återvinner avfall och restprodukter från näringsliv, organisationer och hushåll över hela Sverige. Våra erfarna och professionella medarbetare tror på enkla lösningar, ansvarstagande, helhetssyn och framåtanda. Vi är drygt 1500 medarbetare i Sverige. Ragn-Sells koncernen finns dessutom i Danmark, Norge och Estland. Totalt är vi ca 2400 personer i hela koncernen och 2021 omsatte koncernen ca 7,5 miljarder SEK.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Transportplanerare** till Ragn-Sells i Jönköping V... |  | x |  | 18 | [transportplanerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ngxm_jjS_hXk) |
| 1 | **Transportplanerare** till Ragn-Sells i Jönköping V... | x | x | 18 | 18 | [Transportplanerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/T9bK_xR7_wvn) |
| 2 | ...rtplanerare till Ragn-Sells i **Jönköping** Vill du kunna göra skillnad g... | x | x | 9 | 9 | [Jönköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KURg_KJF_Lwc) |
| 3 | ...r hand i hand. Nu söker vi en **Transportplanerare** till Ragn-Sells Recyclings Re... |  | x |  | 18 | [transportplanerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ngxm_jjS_hXk) |
| 3 | ...r hand i hand. Nu söker vi en **Transportplanerare** till Ragn-Sells Recyclings Re... | x | x | 18 | 18 | [Transportplanerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/T9bK_xR7_wvn) |
| 4 | ...Syd Öst som har erfarenhet av **transportplanering** och logistikfrågor. Om du är ... | x | x | 18 | 18 | [Transportplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/sKLW_XyF_UGt) |
| 5 | ... och logistikfrågor. Om du är **analytisk**, strukturerad och trivs i sam... | x |  |  | 9 | [tänka analytiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/osu5_TsT_qGo) |
| 6 | ...dra samt har ett intresse för **miljöfrågor** kan det här vara nästa utmani... | x | x | 11 | 11 | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
| 7 | ...u planerar fordon i en större **geografi** - Vid behov göra översyn och ... |  | x |  | 8 | [geografi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fKCo_Mj7_EhK) |
| 8 | ...ör kvalitet, miljö- och arbets**miljöfrågor** följs i samband med transport... |  | x |  | 11 | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
| 9 | ..., chaufförer, sektionschefer, **kundtjänstmedarbetare** samt övriga kollegor  Vi erbj... |  | x |  | 21 | [Kundtjänstmedarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8Uhp_XYo_z5f) |
| 10 | ...i erbjuder dig ett arbete med **logistik** inom en av Sveriges viktigast... |  | x |  | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 11 | ...rbete med logistik inom en av **Sveriges** viktigaste branscher, goda an... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 12 | ... goda anställningsvillkor med **kollektivavtal**, pension och friskvårdsbidrag... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 13 | ... egen Akademi. Tjänsten är en **visstidanställning** på 1 år med start senast okto... | x |  |  | 18 | [Tidsbegränsad anställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/sTu5_NBQ_udq) |
| 14 | ...m är du? Vi söker dig som har **tidigare erfarenhet från arbete med logistikstöd** eller kundservice inom logist... | x |  |  | 48 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 15 | ...arbete med logistikstöd eller **kundservice** inom logistik-eller transport... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 16 | ...ikstöd eller kundservice inom **logistik**-eller transportbranschen. Du ... | x |  |  | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 17 | ...ndservice inom logistik-eller **transportbranschen**. Du har relevant utbildning i... | x |  |  | 18 | [Transportteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/aJz8_o6C_Kgy) |
| 18 | ...ortbranschen. Du har relevant **utbildning inom logistik**, eller erfarenhet som bedöms ... | x |  |  | 24 | [Annan utbildning inom transporttjänster, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/qMSb_Gxw_yJ3) |
| 19 | ... har relevant utbildning inom **logistik**, eller erfarenhet som bedöms ... |  | x |  | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 20 | ...riterande med erfarenhet från **återvinningsbranschen**.  Som person är du strukturer... | x |  |  | 21 | [Återvinningsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/P755_zTS_nta) |
| 21 | ...nicerar väl och obehindrat på **svenska** och har lätt för att samarbet... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 22 | ... svenska och har lätt för att **samarbeta** och skapa kontaktytor såväl e... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 23 | ...gtvis har du ett intresse för **miljöfrågor**. Vi lägger stor vikt vid pers... |  | x |  | 11 | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
| 24 | ...främsta kompetensföretag inom **återvinning** och miljö. Vi samlar in, beha... |  | x |  | 11 | [Återvinning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/dHL8_syv_oST) |
| 25 | ...lar och återvinner avfall och **restprodukter** från näringsliv, organisation... |  | x |  | 13 | [Hållbart byggande och förvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/Cxha_8ST_5u1) |
| 26 | ...ationer och hushåll över hela **Sverige**. Våra erfarna och professione... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 27 | ...tare tror på enkla lösningar, **ansvarstagande**, helhetssyn och framåtanda. V... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 28 | ...i är drygt 1500 medarbetare i **Sverige**. Ragn-Sells koncernen finns d... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| | **Overall** | | | **92** | **424** | 92/424 = **22%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Kundtjänstmedarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8Uhp_XYo_z5f) |
|  | x |  | [Hållbart byggande och förvaltning, **skill**](http://data.jobtechdev.se/taxonomy/concept/Cxha_8ST_5u1) |
| x | x | x | [Jönköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/KURg_KJF_Lwc) |
|  | x |  | [transportplanerare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ngxm_jjS_hXk) |
| x |  |  | [Återvinningsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/P755_zTS_nta) |
| x | x | x | [Transportplanerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/T9bK_xR7_wvn) |
| x | x | x | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Transportteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/aJz8_o6C_Kgy) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Återvinning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/dHL8_syv_oST) |
|  | x |  | [geografi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fKCo_Mj7_EhK) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [tänka analytiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/osu5_TsT_qGo) |
| x |  |  | [Annan utbildning inom transporttjänster, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/qMSb_Gxw_yJ3) |
| x | x | x | [Transportplanering, **skill**](http://data.jobtechdev.se/taxonomy/concept/sKLW_XyF_UGt) |
| x |  |  | [Tidsbegränsad anställning, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/sTu5_NBQ_udq) |
| x | x | x | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
| x | x | x | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/21 = **38%** |