# Results for 'a7689f967f011895ecabbc4ab677107088b332ee'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [a7689f967f011895ecabbc4ab677107088b332ee](README.md) | 1 | 1500 | 19 | 14 | 80/257 = **31%** | 7/17 = **41%** |

## Source text

Teamledare inom lokalvård i Uppsala  JUST NU SÖKER VI EN TEAMLEDARE INOM LOKALVÅRD Vi stärker vår organisation och letar efter en teamledare som vill bli en del av vårt team med erfarna kollegor där uppdraget till störst del handlar om lokalvård men såväl planering, samordning, leda och fördela arbetsuppgifter och personaluppföljning för optimal leverans. Din profil:  För att trivas i rollen bör du vara noggrann, positiv, kvalitetsmedveten och inneha servicekänsla. Vi söker dig som båda kan leda och samtidigt entusiasmera din personal till att varje dag prestera på hög nivå. Förmåga att växla mellan att strukturerat arbete men ändå ha känslan för en hög flexibilitet i arbetet. Du bör kunna utveckla goda relationer med såväl personal, leverantörer och kunder, då det innefattar kontakter med dessa på daglig basis. ﻿Vi ser gärna att du har:  ·      Erfarenhet av lokalvård ·      Goda ledaregenskaper med några års erfarenhet som ledare ·      En god planeringsförmåga ·      En riktigt hög servicekänsla med flexibilitet i fokus ·      En god koordinationsförmåga ·      God administrativ förmåga samt goda kunskaper i Word & Excel Krav:  ·      Kan börja kl. 06.00  ·      B-körkort  ·      Förstår svenska i tal och skrift   Vi ser fram emot din ansökan som ni mer än välkommen att skicka till desiree.fejax@gmail.com och märk ansökan med teamledare.  Vi undanbeder oss att ni lämnar ansökningar till vårt kontor och uppskattar om ni mejlar in den till oss istället. Välkommen till Fejax!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Teamledare** inom lokalvård i Uppsala  JUS... | x | x | 10 | 10 | [Teamledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FC86_VDF_rqB) |
| 2 | Teamledare inom **lokalvård** i Uppsala  JUST NU SÖKER VI E... |  | x |  | 9 | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| 3 | Teamledare inom lokalvård i **Uppsala**  JUST NU SÖKER VI EN TEAMLEDA... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 4 | ... Uppsala  JUST NU SÖKER VI EN **TEAMLEDARE** INOM LOKALVÅRD Vi stärker vår... | x | x | 10 | 10 | [Teamledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FC86_VDF_rqB) |
| 5 | ...U SÖKER VI EN TEAMLEDARE INOM **LOKALVÅRD** Vi stärker vår organisation o... |  | x |  | 9 | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| 6 | ...ganisation och letar efter en **teamledare** som vill bli en del av vårt t... | x | x | 10 | 10 | [Teamledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FC86_VDF_rqB) |
| 7 | ...et till störst del handlar om **lokalvård** men såväl planering, samordni... |  | x |  | 9 | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| 8 | ...t trivas i rollen bör du vara **noggrann**, positiv, kvalitetsmedveten o... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 9 | ...la. Vi söker dig som båda kan **leda** och samtidigt entusiasmera di... | x |  |  | 4 | [leda en grupp, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/QZKT_724_B6i) |
| 10 | ...m båda kan leda och samtidigt **entusiasmera din personal** till att varje dag prestera p... | x |  |  | 25 | [motivera anställda, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Nvoo_An5_U2n) |
| 11 | ...litet i arbetet. Du bör kunna **utveckla goda relationer** med såväl personal, leverantö... | x |  |  | 24 | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| 12 | ...du har:  ·      Erfarenhet av **lokalvård** ·      Goda ledaregenskaper m... | x |  |  | 9 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 12 | ...du har:  ·      Erfarenhet av **lokalvård** ·      Goda ledaregenskaper m... |  | x |  | 9 | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| 13 | ...nhet av lokalvård ·      Goda **ledaregenskaper** med några års erfarenhet som ... | x |  |  | 15 | [Operativ ledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aB7R_JQN_sDE) |
| 14 | ...     Goda ledaregenskaper med **några års erfarenhet** som ledare ·      En god plan... | x |  |  | 20 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| 15 | ... med några års erfarenhet som **ledare** ·      En god planeringsförmå... | x |  |  | 6 | [Arbetsledare, städ/Husfru/Städledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cYP6_Tur_q6m) |
| 16 | ...nhet som ledare ·      En god **planeringsförmåga** ·      En riktigt hög service... | x |  |  | 17 | [planera arbetet för team och enskilda, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/16XC_dHo_GUT) |
| 17 | ...ordinationsförmåga ·      God **administrativ förmåga** samt goda kunskaper i Word & ... | x |  |  | 21 | [utföra administrativt arbete, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2x13_sLi_EzG) |
| 18 | ...förmåga samt goda kunskaper i **Word** & Excel Krav:  ·      Kan bör... | x | x | 4 | 4 | [MS Word, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Ei3_fZV_YKM) |
| 19 | ... samt goda kunskaper i Word & **Excel** Krav:  ·      Kan börja kl. 0... | x | x | 5 | 5 | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| 20 | ...  Kan börja kl. 06.00  ·      **B-körkort**  ·      Förstår svenska i tal... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 21 | ...    B-körkort  ·      Förstår **svenska** i tal och skrift   Vi ser fra... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 22 | ...mail.com och märk ansökan med **teamledare**.  Vi undanbeder oss att ni lä... | x | x | 10 | 10 | [Teamledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FC86_VDF_rqB) |
| | **Overall** | | | **80** | **257** | 80/257 = **31%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [planera arbetet för team och enskilda, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/16XC_dHo_GUT) |
| x |  |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x |  |  | [utföra administrativt arbete, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2x13_sLi_EzG) |
| x | x | x | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| x | x | x | [MS Word, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Ei3_fZV_YKM) |
| x | x | x | [Teamledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/FC86_VDF_rqB) |
| x |  |  | [upprätthålla arbetsmässiga relationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FSSe_5dD_kXc) |
| x |  |  | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| x |  |  | [motivera anställda, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Nvoo_An5_U2n) |
| x |  |  | [leda en grupp, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/QZKT_724_B6i) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Operativ ledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aB7R_JQN_sDE) |
| x |  |  | [Arbetsledare, städ/Husfru/Städledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cYP6_Tur_q6m) |
|  | x |  | [Lokalvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dpoa_kdf_VRa) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/17 = **41%** |