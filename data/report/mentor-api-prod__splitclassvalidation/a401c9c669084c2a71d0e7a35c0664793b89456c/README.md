# Results for 'a401c9c669084c2a71d0e7a35c0664793b89456c'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [a401c9c669084c2a71d0e7a35c0664793b89456c](README.md) | 1 | 3866 | 28 | 18 | 177/322 = **55%** | 9/19 = **47%** |

## Source text

Undersköterskor till Hemtjänstgruppen Gullvik! Ref: 20222395   Välkommen till Gullviksborg hemtjänstgrupp! I vårt område finns både bostadsrätter och hyresrätter. Vi förflyttar oss med cykel inom vårt område och arbetar i lag utifrån Malmömodellen för att höja kvalite, kontinuitet, delaktighet och inflytande för brukaren. Vi utgår från Västra Hindbyvägen 14 med gångavstånd till bra bussförbindelser.  Som medarbetare hos oss kommer du att ingå i lag där samverkan, respekt, lojalitet och arbetsglädje är viktigt samt brukarfokus.  Välkommen med din ansökan!  Arbetsuppgifter Som undersköterska hjälper, motiverar och stödjer du brukarna med omvårdnadsinsatser utifrån deras individuella behov och önskemål. I arbetet ingår bland annat den personliga omvårdnaden, medicinska insatser som utförs på delegation av legitimerad personal, matlagning, städning, inköp och tvätt.  Du dokumenterar händelser kring brukarna och har kontakt med deras anhöriga. I det dagliga arbetet samarbetar du med hemsjukvård, rehab, biståndshandläggare och andra hemtjänstgrupper.  Kvalifikationer Vi söker dig som är utbildad undersköterska med tidigare erfarenhet av omvårdnadsarbete och erfarenhet av arbete inom ordinärt boende (hemtjänst).  För att du ska trivas med uppdraget behöver du vara flexibel och snabbt kunna ställa om när förutsättningarna förändras, göra nödvändiga prioriteringar samt vara aktiv i den dagliga planeringen av arbetet. Arbetet kräver att du har förmågan att arbeta självständigt såväl som att ha ett gott och nära samarbete tillsammans med dina kollegor och övriga personer som är involverade runt brukaren.  Då en stor del av arbetsuppgifterna handlar om att kommunicera samt att arbeta med dokumentation, måste du kunna uttrycka dig väl på svenska i både tal och skrift. Kunskaper i andra språk är meriterande, liksom erfarenhet av dokumentation i systemet Lifecare, Flexite och Senior Alert.  Att kunna cykla är ett krav eftersom vi använder cykel för att ta oss till våra brukare.  Viktig information till dig som söker jobb i Hälsa-, Vård- & Omsorgsförvaltningen. När du söker jobb hos oss kommer vi be dig att visa upp ett utdrag ur Polisens Belastningsregister. Du behöver själv begära ett utdrag ur Belastningsregistret och det gör du via en blankett som du hittar på polisens hemsida: http://bit.ly/polisens-blankett  När du fått ditt utdrag uppvisar du sedan det för rekryterande chef.   Observera att den normala handläggningstiden hos polisen är cirka två veckor, så beställ gärna utdraget i samband med att du söker tjänsten. Här kan du läsa mer: http://bit.ly/Information-om-utdrag  Om arbetsplatsen Vi i hälsa-, vård- och omsorgsförvaltningen är med våra ca 5 500 medarbetare Malmö stads näst största förvaltning. Vi har det samlade ansvaret för Malmös hemtjänst, vårdboende, mötesplatser, hemsjukvård och rehabilitering och dess biståndshandläggning. Tillsammans skapar vi en dynamisk och framtidssmart organisation. En organisation som kan möta dagens behov och framtidens utmaningar. Med engagemang, nyfikenhet och hjärta gör vi vardagen enklare för tiotusentals Malmöbor.  Malmö stads verksamheter är organiserade i 14 förvaltningar. Vårt gemensamma uppdrag är att skapa välfärd och hållbar samhällsutveckling. Tillsammans med Malmöborna utvecklar vi en ännu bättre stad att bo, verka och vara i; olikheter, livserfarenheter och talanger gör Malmö till en smart stad i hjärtat av Europa. Världens kunskap finns här.   Läs om Malmö stad som arbetsgivare här malmo.se/jobb   Information om tjänsten Anställningsform:Tillsvidare Omfattning: Heltid Antal tjänster: 6 kvällstjänster Tillträde: 1 september eller enligt överenskommelse  Övrigt Malmö stad strävar efter att medarbetarna ska representera den mångfald som finns i vår stad. Vi välkomnar därför sökande som kan bidra till att vi som arbetsgivare kan uppfylla Malmöbornas behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Undersköterskor** till Hemtjänstgruppen Gullvik... | x | x | 15 | 15 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 2 | Undersköterskor till **Hemtjänstgruppen** Gullvik! Ref: 20222395   Välk... | x |  |  | 16 | [Hemtjänst, **keyword**](http://data.jobtechdev.se/taxonomy/concept/AbwX_q9G_THp) |
| 3 | ...  Välkommen till Gullviksborg **hemtjänstgrupp**! I vårt område finns både bos... | x |  |  | 14 | [Hemtjänst, **keyword**](http://data.jobtechdev.se/taxonomy/concept/AbwX_q9G_THp) |
| 4 | ...rätter. Vi förflyttar oss med **cykel** inom vårt område och arbetar ... | x |  |  | 5 | [Cykel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YoQ5_F8M_9hs) |
| 5 | ...ansökan!  Arbetsuppgifter Som **undersköterska** hjälper, motiverar och stödje... | x | x | 14 | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 6 | ... på delegation av legitimerad **personal**, matlagning, städning, inköp ... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 7 | ...timerad personal, matlagning, **städning**, inköp och tvätt.  Du dokumen... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 7 | ...timerad personal, matlagning, **städning**, inköp och tvätt.  Du dokumen... | x |  |  | 8 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 8 | ...iga arbetet samarbetar du med **hemsjukvård**, rehab, biståndshandläggare o... | x | x | 11 | 11 | [Hemsjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/NbjJ_WBY_FoA) |
| 9 | ...ar du med hemsjukvård, rehab, **biståndshandläggare** och andra hemtjänstgrupper.  ... | x | x | 19 | 19 | [Biståndsbedömare/Biståndshandläggare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pYiF_Z6V_ULQ) |
| 10 | ... Vi söker dig som är utbildad **undersköterska** med tidigare erfarenhet av om... | x | x | 14 | 14 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 11 | ... arbete inom ordinärt boende (**hemtjänst**).  För att du ska trivas med ... | x |  |  | 9 | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
| 12 | ...räver att du har förmågan att **arbeta självständigt** såväl som att ha ett gott och... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 13 | ... som att ha ett gott och nära **samarbete** tillsammans med dina kollegor... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 14 | ...mmunicera samt att arbeta med **dokumentation**, måste du kunna uttrycka dig ... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 15 | ... du kunna uttrycka dig väl på **svenska** i både tal och skrift. Kunska... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 16 | ...terande, liksom erfarenhet av **dokumentation** i systemet Lifecare, Flexite ... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 17 | ...lexite och Senior Alert.  Att **kunna cykla** är ett krav eftersom vi använ... | x | x | 11 | 11 | [Kunna cykla, **skill**](http://data.jobtechdev.se/taxonomy/concept/NL17_f8c_AqZ) |
| 18 | ...ett krav eftersom vi använder **cykel** för att ta oss till våra bruk... | x |  |  | 5 | [Cykel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YoQ5_F8M_9hs) |
| 19 | ...ion till dig som söker jobb i **Hälsa**-, Vård- & Omsorgsförvaltninge... | x |  |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 20 | ...utdrag  Om arbetsplatsen Vi i **hälsa**-, vård- och omsorgsförvaltnin... | x |  |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 21 | ...med våra ca 5 500 medarbetare **Malmö** stads näst största förvaltnin... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 22 | ...åra ca 5 500 medarbetare Malmö** stads** näst största förvaltning. Vi ... | x |  |  | 6 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 23 | ...t samlade ansvaret för Malmös **hemtjänst**, vårdboende, mötesplatser, he... | x |  |  | 9 | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
| 24 | ...st, vårdboende, mötesplatser, **hemsjukvård** och rehabilitering och dess b... | x | x | 11 | 11 | [Hemsjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/NbjJ_WBY_FoA) |
| 25 | ...mötesplatser, hemsjukvård och **rehabilitering** och dess biståndshandläggning... | x | x | 14 | 14 | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| 26 | ...e för tiotusentals Malmöbor.  **Malmö** stads verksamheter är organis... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 27 | ... tiotusentals Malmöbor.  Malmö** stads** verksamheter är organiserade ... | x |  |  | 6 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 28 | ...lsutveckling. Tillsammans med **Malmö**borna utvecklar vi en ännu bät... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 29 | ...s kunskap finns här.   Läs om **Malmö** stad som arbetsgivare här mal... |  | x |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 30 | ... om tjänsten Anställningsform:**Tillsvidare** Omfattning: Heltid Antal tjän... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 31 | ...sform:Tillsvidare Omfattning: **Heltid** Antal tjänster: 6 kvällstjäns... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 32 | ...nligt överenskommelse  Övrigt **Malmö** stad strävar efter att medarb... | x | x | 5 | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| 33 | ... överenskommelse  Övrigt Malmö** stad** strävar efter att medarbetarn... | x |  |  | 5 | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| | **Overall** | | | **177** | **322** | 177/322 = **55%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x | x | x | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Hemtjänst, **keyword**](http://data.jobtechdev.se/taxonomy/concept/AbwX_q9G_THp) |
| x |  |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x | x | x | [Kunna cykla, **skill**](http://data.jobtechdev.se/taxonomy/concept/NL17_f8c_AqZ) |
| x | x | x | [Hemsjukvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/NbjJ_WBY_FoA) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Cykel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YoQ5_F8M_9hs) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Malmö, **municipality**](http://data.jobtechdev.se/taxonomy/concept/oYPt_yRA_Smm) |
| x | x | x | [rehabilitering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ok5p_sUV_QKj) |
| x | x | x | [Biståndsbedömare/Biståndshandläggare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pYiF_Z6V_ULQ) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| x |  |  | [Hemtjänst, **skill**](http://data.jobtechdev.se/taxonomy/concept/xowU_8Wu_K7C) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **9** | 9/19 = **47%** |