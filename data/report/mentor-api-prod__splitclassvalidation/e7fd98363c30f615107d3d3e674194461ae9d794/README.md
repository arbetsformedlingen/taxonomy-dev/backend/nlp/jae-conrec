# Results for 'e7fd98363c30f615107d3d3e674194461ae9d794'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e7fd98363c30f615107d3d3e674194461ae9d794](README.md) | 1 | 2357 | 12 | 12 | 97/209 = **46%** | 7/12 = **58%** |

## Source text

Miljöinspektör Vill du vara med och skapa framtidens Karlstad? Hos oss i koncernen Karlstads kommun kan du göra skillnad på riktigt. Här är varje medarbetare en viktig del i helheten och tillsammans hjälps vi åt att förverkliga vår vision om "Ett bättre liv i Solstaden". Kom och väx med oss!   Miljöförvaltningens cirka 40 medarbetare arbetar förebyggande för människors hälsa och livsmiljö. Genom myndighetsutövning ser vi till att lagar och regler följs som rör livsmedel och miljö. Vi driver flera miljö- och klimatprojekt, ger rådgivning i miljö- och klimatfrågor och arbetar strategiskt för att Karlstad ska utvecklas på ett hållbart sätt.    På tillsynsavdelningen arbetar idag cirka 25 inspektörer som är indelade i tre olika team: livsmedels-, hälsoskydds- respektive miljöteam.  Arbetsuppgifter Vi söker en engagerad miljöinspektör till vårt miljöteam med huvudsakliga arbetsuppgifter inom tillsyn och handläggning av miljöfarlig verksamhet inklusive förorenade områden.  Du kommer att utöva myndighetsutövning genom inspektioner och beslut, samt ha kontakt med medborgare och företag genom rådgivning och information.  Kvalifikationer Du har en relevant högskoleutbildning med inriktning mot miljö- och hälsoskydd eller annan naturvetenskaplig utbildning som vi bedömer likvärdig. Du har arbetslivserfarenhet av tillsyn enligt miljöbalken.   Vi söker dig som är positiv och driven i ditt arbete. Du tycker om att ta ansvar och arbeta strukturerat. Du har en förmåga att styra mot uppsatta mål. Vidare är du serviceinriktad, kan arbeta självständigt men också samarbeta och bidra till ett gott arbetsklimat. Du behöver också kunna tillämpa ett gott bemötande och ha förmåga att skapa tillit och förtroende i både din muntliga och skriftliga kommunikation. Vi kommer att lägga stor vikt vid dina personliga egenskaper.   B-körkort är ett krav.  För att kvalitetssäkra rekryteringsprocessen och förenkla kommunikationen med våra sökande ber vi dig skicka in din ansökan digitalt och inte via e-post eller pappersformat. Vi undanber oss alla erbjudanden om annonserings- och rekryteringshjälp i samband med denna annons.   Har du skyddade personuppgifter ska du inte registrera dina ansökningshandlingar i vårt rekryteringssystem och inte heller skicka in dem via e-post. Vänd dig istället till kommunens växel 054-540 00 00 för vidare hantering.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Miljöinspektör** Vill du vara med och skapa fr... | x | x | 14 | 14 | [miljöinspektör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xYHb_aKs_Cky) |
| 2 | ...vara med och skapa framtidens **Karlstad**? Hos oss i koncernen Karlstad... | x | x | 8 | 8 | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| 3 | ...Karlstad? Hos oss i koncernen **Karlstads kommun** kan du göra skillnad på rikti... | x |  |  | 16 | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| 4 | ...r förebyggande för människors **hälsa** och livsmiljö. Genom myndighe... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 5 | ...agar och regler följs som rör **livsmedel** och miljö. Vi driver flera mi... | x | x | 9 | 9 | [Livsmedel, **skill**](http://data.jobtechdev.se/taxonomy/concept/tdbu_fUL_MnC) |
| 6 | ...imatprojekt, ger rådgivning i **miljö- och **klimatfrågor och arbetar strat... |  | x |  | 11 | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
| 7 | ...h arbetar strategiskt för att **Karlstad** ska utvecklas på ett hållbart... | x | x | 8 | 8 | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| 8 | ...pgifter Vi söker en engagerad **miljöinspektör** till vårt miljöteam med huvud... | x | x | 14 | 14 | [miljöinspektör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xYHb_aKs_Cky) |
| 9 | ...fikationer Du har en relevant **högskoleutbildning** med inriktning mot miljö- och... | x |  |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 9 | ...fikationer Du har en relevant **högskoleutbildning** med inriktning mot miljö- och... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 10 | ...utbildning med inriktning mot **miljö- och** hälsoskydd eller annan naturv... |  | x |  | 10 | [Miljöskydd, **skill**](http://data.jobtechdev.se/taxonomy/concept/UXhY_wj1_QSW) |
| 11 | ...utbildning med inriktning mot **miljö- och **hälsoskydd eller annan naturve... | x |  |  | 11 | [Miljö- och hälsoskydd, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jqei_k5E_u48) |
| 12 | ...med inriktning mot miljö- och **hälsoskydd** eller annan naturvetenskaplig... | x | x | 10 | 10 | [Miljö- och hälsoskydd, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jqei_k5E_u48) |
| 13 | ...ö- och hälsoskydd eller annan **naturvetenskaplig utbildning** som vi bedömer likvärdig. Du ... | x |  |  | 28 | [Naturvetenskap, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/kJeN_wmw_9wX) |
| 14 | ...re är du serviceinriktad, kan **arbeta självständigt** men också samarbeta och bidra... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ...dina personliga egenskaper.   **B-körkort** är ett krav.  För att kvalite... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| | **Overall** | | | **97** | **209** | 97/209 = **46%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x | x | x | [Miljö- och hälsoskydd, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jqei_k5E_u48) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Miljöskydd, **skill**](http://data.jobtechdev.se/taxonomy/concept/UXhY_wj1_QSW) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Karlstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hRDj_PoV_sFU) |
| x |  |  | [Naturvetenskap, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/kJeN_wmw_9wX) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x | x | x | [Livsmedel, **skill**](http://data.jobtechdev.se/taxonomy/concept/tdbu_fUL_MnC) |
|  | x |  | [Miljöfrågor, **skill**](http://data.jobtechdev.se/taxonomy/concept/wLYv_Pyd_uSZ) |
| x | x | x | [miljöinspektör, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/xYHb_aKs_Cky) |
| | | **7** | 7/12 = **58%** |