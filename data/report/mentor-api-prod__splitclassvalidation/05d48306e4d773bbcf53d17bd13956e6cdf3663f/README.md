# Results for '05d48306e4d773bbcf53d17bd13956e6cdf3663f'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [05d48306e4d773bbcf53d17bd13956e6cdf3663f](README.md) | 1 | 4058 | 20 | 25 | 43/412 = **10%** | 4/28 = **14%** |

## Source text

SAP HCM Specialist/Projektledare a  Sammanfattning av tjänsten  Vill du vara med på vår digitaliseringsresa? Nu söker vi en ny stjärna till vårt team HR Organization & Transformation. Som Specialist inom SAP HCM kan du med ditt lösningsorienterade och relationsskapande arbetssätt, stärka vårt dynamiska och engagerade team och hjälpa oss driva förändringar.   Din roll  I rollen som SAP HCM Specialist på Lidl får du ett stort förtroende och möjlighet att ansvara för vår systemtekniska och processmässiga utveckling. Du får utlopp för din kreativa sida genom att skapa lösningar och driva digitaliseringen framåt. Här finns möjligheten att påverka sitt vardagliga arbete och att skapa ramar för det samma. Ditt arbete har en nyckelfunktion för vår digitaliseringsresa då det omfattar kompletta processer - från ax till limpa Som Specialist hos oss på Lidl kommer du bland annat att: •    Ansvara för systemmässig och processmässig utveckling av SAP HCM •    Förvalta SAP HCM genom implementering och systemtestning •    Vara projektledare för olika projekt inom Lidls digitaliseringsresa •    Supportera och agera expert åt övriga delar av verksamheten gällande systemet •    Ha mycket nationellt och internationellt utbyte med kollegor    Din profil  För att bli en framgångsrik Specialist ser vi att du som person är dynamisk, trygg och värdesätter att vara suverän inom din kompetens. Du har ett strukturerat arbetssätt och motiveras av konceptuella utmaningar som uppmuntrar komplext tänkande, att se samband mellan problem och att skapa kreativa lösningar. Vidare har du en pedagogisk förmåga och integritet som hjälper dig få med andra i förändringsarbete och beslut. Eftersom rollen har många kontaktytor ställs även stora krav på att du är en god kommunikatör och har ett genomgående relationsskapande förhållningssätt. Vidare har du: •    Relevant akademisk examen, troligen inom ekonomi, data- eller systemvetenskap •    Erfarenhet av SAP HCM, antingen av affärssystemet som helhet eller en djupare förståelse av specifika moduler •    Mycket goda kunskaper i svenska och engelska är väsentligt, tyskakunskaper är meriterande •    Tidigare erfarenhet av projektarbete eller projektledning   Vi erbjuder  För oss är det viktigt att våra medarbetare trivs - på alla sätt. Vi erbjuder därför ett arbete i en miljö som karaktäriseras av våra värdeord: engagemang, ansvar, smart, tillsammans och ha kul. Eftersom vi växer, kan du växa. Snabbt. Inom din roll, eller mot en annan. På samma plats, eller någon annanstans. Inom butik, lager, kontor. Lokalt, nationellt eller globalt. Vi stöttar dig på din resa mot mer ansvar och nya utmaningar.  Du blir del av ett stort team där alla kavlar upp ärmarna och hjälps åt. Ibland hittar du oss på vårt toppmoderna huvudkontor, där vi har tillgång till gym och har härligt rabatterade priser i restaurangen, men givetvis har vi också möjlighet till flexibelt distansarbete. Som övriga förmåner har du allt från friskvårdsbidrag och medarbetarrabatt i våra butiker till bra semestervillkor samt tillgång till mängder av erbjudanden via en förmånsportal. Självklart omfattas du också av kollektivavtal. Vi möjliggör en rättvis lönesättning inom företaget genom ett icke-diskriminerande lönesystem. Vid fackliga frågor kontakta Unionens lokala fackklubb via e-postadressen; unionenklubb_hk@lidl.se   Vill du vara med på vår resa?  Om ditt svar är ja, då söker du jobbet såhär: o    Gå in via länken ”ansök nu” o    Fyll i formuläret  o    Bifoga CV samt personligt brev Denna rekrytering sker med löpande urval, varför vi gärna ser att du skickar din ansökan snarast, dock senast den 1/7 2022. Tjänsten är en tillsvidareanställning med 6 månaders provanställning. Vår rekryteringsprocess består av telefonintervju, urvalstester, personlig intervju och referenstagning. Vid fackliga frågor kontakta Unionens lokala fackklubb via e-postadressen; unionenklubb_hk@lidl.se Har du frågor om tjänsten eller ansökningsprocessen, vänligen kontakta Rekryteringsteamet på mail: jobb@lidl.se. Vi ser fram emot din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **SAP** HCM Specialist/Projektledare ... |  | x |  | 3 | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
| 2 | SAP HCM Specialist/**Projektledare** a  Sammanfattning av tjänst... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 2 | SAP HCM Specialist/**Projektledare** a  Sammanfattning av tjänst... | x |  |  | 13 | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
| 3 | ...ormation. Som Specialist inom **SAP** HCM kan du med ditt lösningso... |  | x |  | 3 | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
| 4 | ...gar.   Din roll  I rollen som **SAP** HCM Specialist på Lidl får du... |  | x |  | 3 | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
| 5 | ...ystemmässig och processmässig **utveckling** av SAP HCM •    Förvalta SAP ... | x |  |  | 10 | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| 6 | ...h processmässig utveckling av **SAP** HCM •    Förvalta SAP HCM gen... |  | x |  | 3 | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
| 7 | ...ling av SAP HCM •    Förvalta **SAP** HCM genom implementering och ... |  | x |  | 3 | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
| 8 | ... HCM genom implementering och **systemtestning** •    Vara projektledare för o... | x | x | 14 | 14 | [Systemtestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/xAbu_uKA_Jeu) |
| 9 | ... och systemtestning •    Vara **projektledare** för olika projekt inom Lidls ... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 9 | ... och systemtestning •    Vara **projektledare** för olika projekt inom Lidls ... | x |  |  | 13 | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
| 10 | ...idls digitaliseringsresa •    **Supportera och agera expert** åt övriga delar av verksamhet... | x |  |  | 27 | [ge support till IT-systemanvändare, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GoXo_6GM_3Qr) |
| 11 | ...a lösningar. Vidare har du en **pedagogisk** förmåga och integritet som hj... | x |  |  | 10 | [Pedagogiskt förhållningssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/qMfa_U8e_Lto) |
| 12 | ...om hjälper dig få med andra i **förändringsarbete** och beslut. Eftersom rollen h... | x |  |  | 17 | [Förändringsledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LLQd_jDh_gdd) |
| 13 | ...en stora krav på att du är en **god kommunikatör** och har ett genomgående relat... | x |  |  | 16 | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
| 14 | ...tora krav på att du är en god **kommunikatör** och har ett genomgående relat... |  | x |  | 12 | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| 15 | ... Vidare har du: •    Relevant **akademisk examen**, troligen inom ekonomi, data-... |  | x |  | 16 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 15 | ... Vidare har du: •    Relevant **akademisk examen**, troligen inom ekonomi, data-... | x |  |  | 16 | [Eftergymnasial utbildning, minst 2 år, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/KBjB_gpS_ZJL) |
| 15 | ... Vidare har du: •    Relevant **akademisk examen**, troligen inom ekonomi, data-... |  | x |  | 16 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 16 | ...ademisk examen, troligen inom **ekonomi**, data- eller systemvetenskap ... | x |  |  | 7 | [Företagsekonomi, handel och administration, **sun-education-field-2**](http://data.jobtechdev.se/taxonomy/concept/1nfZ_zkU_RyS) |
| 16 | ...ademisk examen, troligen inom **ekonomi**, data- eller systemvetenskap ... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 17 | ...xamen, troligen inom ekonomi, **data- eller s**ystemvetenskap •    Erfarenhet... |  | x |  | 13 | [datavetenskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VxiM_uBZ_V4d) |
| 18 | ...xamen, troligen inom ekonomi, **data- eller systemvetenskap** •    Erfarenhet av SAP HCM, a... | x |  |  | 27 | [Datavetenskap och systemvetenskap, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Ai2t_uy2_1KN) |
| 19 | ...gen inom ekonomi, data- eller **systemvetenskap** •    Erfarenhet av SAP HCM, a... |  | x |  | 15 | [Systemvetenskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/k2nm_7ss_NnX) |
| 20 | ...mvetenskap •    Erfarenhet av **SAP** HCM, antingen av affärssystem... |  | x |  | 3 | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
| 21 | ... •    Mycket goda kunskaper i **svenska** och engelska är väsentligt, t... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 22 | ... goda kunskaper i svenska och **engelska** är väsentligt, tyskakunskaper... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 23 | ...a och engelska är väsentligt, **tyskakunskaper** är meriterande •    Tidigare ... | x | x | 14 | 14 | [Tyska, **language**](http://data.jobtechdev.se/taxonomy/concept/S368_jFS_2hP) |
| 24 | ...renhet av projektarbete eller **projektledning**   Vi erbjuder  För oss är det... | x |  |  | 14 | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| 24 | ...renhet av projektarbete eller **projektledning**   Vi erbjuder  För oss är det... |  | x |  | 14 | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| 25 | ... eller någon annanstans. Inom **butik**, lager, kontor. Lokalt, natio... |  | x |  | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 26 | ...någon annanstans. Inom butik, **lager**, kontor. Lokalt, nationellt e... |  | x |  | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 27 | ...ckså möjlighet till flexibelt **distansarbete**. Som övriga förmåner har du a... |  | x |  | 13 | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| 28 | ...älvklart omfattas du också av **kollektivavtal**. Vi möjliggör en rättvis löne... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 29 | ...enom ett icke-diskriminerande **lönesystem**. Vid fackliga frågor kontakta... |  | x |  | 10 | [Lönesystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/hjUM_GZe_Bgd) |
| 30 | ...rekryteringsprocess består av **telefonintervju**, urvalstester, personlig inte... |  | x |  | 15 | [Telefonintervjuare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sDjW_zkk_bXW) |
| | **Overall** | | | **43** | **412** | 43/412 = **10%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Företagsekonomi, handel och administration, **sun-education-field-2**](http://data.jobtechdev.se/taxonomy/concept/1nfZ_zkU_RyS) |
| x |  |  | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| x |  |  | [Datavetenskap och systemvetenskap, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Ai2t_uy2_1KN) |
| x |  |  | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| x |  |  | [ge support till IT-systemanvändare, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GoXo_6GM_3Qr) |
|  | x |  | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
|  | x |  | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x |  |  | [Eftergymnasial utbildning, minst 2 år, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/KBjB_gpS_ZJL) |
| x |  |  | [Förändringsledning, **skill**](http://data.jobtechdev.se/taxonomy/concept/LLQd_jDh_gdd) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x | x | x | [Tyska, **language**](http://data.jobtechdev.se/taxonomy/concept/S368_jFS_2hP) |
|  | x |  | [datavetenskap, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VxiM_uBZ_V4d) |
|  | x |  | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
|  | x |  | [Lönesystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/hjUM_GZe_Bgd) |
|  | x |  | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
|  | x |  | [Systemvetenskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/k2nm_7ss_NnX) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
|  | x |  | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| x |  |  | [Pedagogiskt förhållningssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/qMfa_U8e_Lto) |
|  | x |  | [Telefonintervjuare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sDjW_zkk_bXW) |
|  | x |  | [Distansarbete, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/uwzb_xcR_vxS) |
| x | x | x | [Systemtestning, **skill**](http://data.jobtechdev.se/taxonomy/concept/xAbu_uKA_Jeu) |
|  | x |  | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| x |  |  | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/28 = **14%** |