# Results for '88978f186054863811fc58a9d27db3779723cdc5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [88978f186054863811fc58a9d27db3779723cdc5](README.md) | 1 | 5510 | 38 | 32 | 310/575 = **54%** | 12/20 = **60%** |

## Source text

Sjuksköterska till kombinerad tjänst inom VO vuxenpsykiatri Lund Gör skillnad. Varje dag.   Verksamhetsområde (VO) vuxenpsykiatri Lund är en verksamhet i framkant som kombinerar lång erfarenhet med nytänkande och utveckling av psykiatrisk vård. VO vuxenpsykiatri och dess 550 medarbetare är involverade i flera forsknings- och utvecklingsprojekt. Vill du vara med att skapa framtidens psykiatri? Då ska du ta chansen att söka dig till oss nu!   Vuxenpsykiatrimottagning 1 i Lund är en specialistpsykiatrisk mottagning. Här utreds och behandlas patienter med affektiva sjukdomar, ångesttillstånd, depressioner samt olika former av personlighetssyndrom och neuropsykiatriska tillstånd.   Vuxenpsykiatriavdelning 2 är en allmänpsykiatrisk verksamhet som främst behandlar patienter med allmänpsykiatriska sjukdomstillstånd så som depression, kris, personlighetssyndrom och ätstörningsproblematik.   Vuxenpsykiatrimottagning Emotionell instabilitet är en specialiserad behandlingsenhet som erbjuder Dialektisk beteendeterapi (DBT), Flexible Assertive Community Treatment (FACT), Emotion Regulation Group Therapy (ERGT) och Prolonged Exposure (PE). Här erbjuds även anhörigutbildning Familjeband samt handledning och utbildning till andra vårdgivare och verksamheter.   Vår psykiatriska akutmottagning är öppen dygnet runt och ansvarar för akuta psykiatriska läkarbedömningar samt telefonrådgivning till patienter, närstående, vårdgrannar samt myndigheter. Till mottagningen hör även teamet för hjärnstimulering som ansvarar för upptagningsområdets elektrokonvulsiva terapi (ECT-verksamhet) samt transkraniell magnetstimulering (rTMS).   Ätstörningscentrum erbjuder högspecialiserad heldygnsvård, dagsjukvård och öppenvård. Här behandlas personer med ätstörningsproblematik i behov av inneliggande vård.   ARBETSUPPGIFTER För att bättre möta framtidens behov av flexibel och patientanpassad vård satsar vi nu på en ny roll för specialistsjuksköterskor med delad tjänstgöring mellan två av våra verksamheter. Med detta unika upplägg möjliggörs en mer sammanhängande vård för patienten och en möjlighet att underlätta samarbete mellan olika enheter. Alternativen för delad tjänstgöring är: - Specialiserad öppenvårdsmottagning och heldygnsvård. - Specialiserad öppenvårdsmottagning och akutmottagning. - Öppenvårdsmottagning och heldygnsvård. - Heldygnsvård och öppenvård/dagvård inom Ätstörningscentrum  På mottagning 1 får du arbeta med nära patientarbete, vilket inkluderar insättning och uppföljning av läkemedelsinsatser, psykiatriskt mående samt somatiska kontroller. Du arbetar även med mottagningens telefonrådgivning och stödinsatser.  På avdelning 2 arbetar du med individuella stödsamtal, somatisk och psykiatrisk omvårdnad, rondarbete, läkemedelsadministration samt samarbete med vårdgrannar och kommun.   På mottagning Emotionell instabilitet består dina arbetsuppgifter av att agera fast vårdkontakt och kontaktperson för patienter samt insättning och uppföljning av läkemedelsinsatser, somatiska kontroller och deltagande i det multidisciplinära teamarbetet kring patienterna. Likaså kan du komma att arbeta med att erbjuda stöd- och utbildningsinsatser både individuellt och i grupp.  På Akutmottagningen består dina arbetsuppgifter bland annat av att ta emot akutpsykiatriska patienter och bedöma dem utifrån deras akuta omvårdnadsbehov. I arbetsuppgifterna ingår även samverkan med vårdgrannar samt med myndigheter så som Polisen, Kriminalvården och Socialtjänsten.  På Ätstörningscenters mottagning och dagavdelning består dina arbetsuppgifter av bedömning av hälsotillstånd, omvårdnadsbehov, pedagogiska måltider tillsammans med patienter, erbjuda stödsamtal och rådgivning via telefon samt hålla i behandlande grupper tillsammans med dina kollegor. Det ingår även somatiska kontroller, blodprovstagning och EKG vid behov. Arbetet fokuserar på ett motiverande och stödjande förhållningssätt för att stötta patienten till en förändring.  KVALIFIKATIONER Vi välkomnar dig som är legitimerad sjuksköterska med behörighet att verka inom svensk sjukvård. Språkkunskap i svenska motsvarande lägst nivå C1 enligt Europarådets nivåskala är ett krav. Det är meriterande om du har erfarenhet av arbete och/eller specialistutbildning inom psykiatrin.   Som person ser vi gärna att du har ett gott bemötande och förmåga att självständigt organisera och strukturera ditt arbete. För att trivas i rollen bör du vara trygg i din yrkesroll och trivas med att arbeta såväl självständigt som i team. Vi lägger stor vikt vid personlig lämplighet.   Vid eventuell anställning görs kontroll mot misstanke- och belastningsregistret.   I denna rekrytering tillämpar vi löpande urval, varmt välkommen med din ansökan!  Observera att denna annons är publicerad under flera orter eller yrkeskategorier, du kommer därmed att slussas vidare när du söker jobbet.  ÖVRIGT Region Skåne finns till för att alla som bor i Skåne ska må bra och känna framtidstro. Genom gränslösa samarbeten och omtanke skapas de bästa förutsättningar för ett hälsosamt liv inom näringsliv, kollektivtrafik, kultur och hälso- och sjukvård i Skåne. Tillsammans gör vi livet mera möjligt.   På http://www.skane.se/rekryteringsstatus kan du se i vilket skede Region Skånes rekryteringar befinner sig och hur många som har sökt tjänsterna.  Till bemannings- och rekryteringsföretag och till dig som är försäljare: Vi undanber oss vänligen men bestämt direktkontakt med bemannings- och rekryteringsföretag samt försäljare av ytterligare jobbannonser.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Sjuksköterska** till kombinerad tjänst inom V... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 2 | ...ill kombinerad tjänst inom VO **vuxenpsykiatri** Lund Gör skillnad. Varje dag.... | x | x | 14 | 14 | [Vuxenpsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/btyH_9au_jQS) |
| 3 | ...tjänst inom VO vuxenpsykiatri **Lund** Gör skillnad. Varje dag.   Ve... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 4 | ...dag.   Verksamhetsområde (VO) **vuxenpsykiatri** Lund är en verksamhet i framk... | x | x | 14 | 14 | [Vuxenpsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/btyH_9au_jQS) |
| 5 | ...etsområde (VO) vuxenpsykiatri **Lund** är en verksamhet i framkant s... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 6 | ... nytänkande och utveckling av **psykiatrisk vård**. VO vuxenpsykiatri och dess 5... | x |  |  | 16 | [Psykiatrisk vård, **skill**](http://data.jobtechdev.se/taxonomy/concept/5JNC_VaM_zve) |
| 6 | ... nytänkande och utveckling av **psykiatrisk vård**. VO vuxenpsykiatri och dess 5... |  | x |  | 16 | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| 7 | ...kling av psykiatrisk vård. VO **vuxenpsykiatri** och dess 550 medarbetare är i... | x | x | 14 | 14 | [Vuxenpsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/btyH_9au_jQS) |
| 8 | ...vara med att skapa framtidens **psykiatri**? Då ska du ta chansen att sök... | x | x | 9 | 9 | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| 9 | ...n att söka dig till oss nu!   **Vuxenpsykiatrimottagning** 1 i Lund är en specialistpsyk... | x | x | 24 | 24 | [Vuxenpsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/btyH_9au_jQS) |
| 10 | ... till oss nu!   Vuxenpsykiatri**mottagning** 1 i Lund är en specialistpsyk... |  | x |  | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 11 | ... Vuxenpsykiatrimottagning 1 i **Lund** är en specialistpsykiatrisk m... | x | x | 4 | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 12 | ...d är en specialistpsykiatrisk **mottagning**. Här utreds och behandlas pat... | x |  |  | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 13 | ...r av personlighetssyndrom och **neuropsykiatriska** tillstånd.   Vuxenpsykiatriav... | x |  |  | 17 | [neuropsykiatri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hVTC_kQL_qY1) |
| 14 | ...europsykiatriska tillstånd.   **Vuxenpsykiatriavdelning** 2 är en allmänpsykiatrisk ver... | x |  |  | 23 | [Vuxenpsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/btyH_9au_jQS) |
| 15 | ...och ätstörningsproblematik.   **Vuxenpsykiatrimottagning** Emotionell instabilitet är en... | x | x | 24 | 24 | [Vuxenpsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/btyH_9au_jQS) |
| 16 | ...behandlingsenhet som erbjuder **Dialektisk beteendeterapi** (DBT), Flexible Assertive Com... | x | x | 25 | 25 | [Dialektisk beteendeterapi/DBT, **skill**](http://data.jobtechdev.se/taxonomy/concept/2Mbj_579_yZj) |
| 17 | ...er Dialektisk beteendeterapi (**DBT**), Flexible Assertive Communit... | x | x | 3 | 3 | [Dialektisk beteendeterapi/DBT, **skill**](http://data.jobtechdev.se/taxonomy/concept/2Mbj_579_yZj) |
| 18 | ...atsar vi nu på en ny roll för **specialistsjuksköterskor** med delad tjänstgöring mellan... |  | x |  | 24 | [Specialistsjuksköterskeutbildning, infektionssjukvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1dUg_Jaj_yvZ) |
| 19 | ...nstgöring är: - Specialiserad **öppenvårdsmottagning** och heldygnsvård. - Specialis... | x | x | 20 | 20 | [Psykiatrisk öppenvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/N9zh_vwb_Wht) |
| 20 | ...heldygnsvård. - Specialiserad **öppenvårdsmottagning** och akutmottagning. - Öppenvå... | x | x | 20 | 20 | [Psykiatrisk öppenvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/N9zh_vwb_Wht) |
| 21 | ...tagning och akutmottagning. - **Öppenvårdsmottagning** och heldygnsvård. - Heldygnsv... | x | x | 20 | 20 | [Psykiatrisk öppenvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/N9zh_vwb_Wht) |
| 22 | ...dygnsvård. - Heldygnsvård och **öppenvård**/dagvård inom Ätstörningscentr... | x |  |  | 9 | [Psykiatrisk öppenvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/N9zh_vwb_Wht) |
| 23 | ...d inom Ätstörningscentrum  På **mottagning** 1 får du arbeta med nära pati... | x | x | 10 | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 24 | ...ella stödsamtal, somatisk och **psykiatrisk omvårdnad**, rondarbete, läkemedelsadmini... |  | x |  | 21 | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| 25 | ...tal, somatisk och psykiatrisk **omvårdnad**, rondarbete, läkemedelsadmini... | x |  |  | 9 | [Omvårdnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/MR4H_FFT_mTj) |
| 26 | ... vårdgrannar och kommun.   På **mottagning** Emotionell instabilitet bestå... | x | x | 10 | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 27 | ...r samt med myndigheter så som **Polisen**, Kriminalvården och Socialtjä... | x |  |  | 7 | [Polisen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/B7tQ_J1q_UNn) |
| 28 | ...d myndigheter så som Polisen, **Kriminalvården** och Socialtjänsten.  På Ätstö... | x | x | 14 | 14 | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| 29 | ...nsten.  På Ätstörningscenters **mottagning** och dagavdelning består dina ... | x | x | 10 | 10 | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| 30 | ...komnar dig som är legitimerad **sjuksköterska** med behörighet att verka inom... | x | x | 13 | 13 | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
| 31 | ...ensk sjukvård. Språkkunskap i **svenska** motsvarande lägst nivå C1 enl... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 32 | ...rfarenhet av arbete och/eller **specialistutbildning** inom psykiatrin.   Som person... |  | x |  | 20 | [Specialistsjuksköterskeutbildning, operationssjukvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/bf2X_Hfm_De9) |
| 33 | ...ler specialistutbildning inom **psykiatrin**.   Som person ser vi gärna at... |  | x |  | 10 | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| 34 | ...ott bemötande och förmåga att **självständigt** organisera och strukturera di... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 35 | ... yrkesroll och trivas med att **arbeta** såväl självständigt som i tea... | x |  |  | 6 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 36 | ...h trivas med att arbeta såväl **självständigt** som i team. Vi lägger stor vi... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 37 | ...rbeta såväl självständigt som **i team**. Vi lägger stor vikt vid pers... | x |  |  | 6 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 38 | ...när du söker jobbet.  ÖVRIGT **Region **Skåne finns till för att alla ... | x |  |  | 7 | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| 39 | ...söker jobbet.  ÖVRIGT Region **Skåne** finns till för att alla som b... | x | x | 5 | 5 | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| 40 | ...s till för att alla som bor i **Skåne** ska må bra och känna framtids... | x |  |  | 5 | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| 41 | ...älsosamt liv inom näringsliv, **kollektivtrafik**, kultur och hälso- och sjukvå... | x | x | 15 | 15 | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| 42 | ..., kollektivtrafik, kultur och **hälso- oc**h sjukvård i Skåne. Tillsamman... | x | x | 9 | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 43 | ...ivtrafik, kultur och hälso- oc**h sjukvård** i Skåne. Tillsammans gör vi l... | x |  |  | 10 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 44 | ...tur och hälso- och sjukvård i **Skåne**. Tillsammans gör vi livet mer... | x | x | 5 | 5 | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| 45 | ...atus kan du se i vilket skede **Region Skånes** rekryteringar befinner sig oc... | x |  |  | 13 | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| | **Overall** | | | **310** | **575** | 310/575 = **54%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Specialistsjuksköterskeutbildning, infektionssjukvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1dUg_Jaj_yvZ) |
| x | x | x | [Dialektisk beteendeterapi/DBT, **skill**](http://data.jobtechdev.se/taxonomy/concept/2Mbj_579_yZj) |
| x |  |  | [Psykiatrisk vård, **skill**](http://data.jobtechdev.se/taxonomy/concept/5JNC_VaM_zve) |
| x |  |  | [Polisen, **keyword**](http://data.jobtechdev.se/taxonomy/concept/B7tQ_J1q_UNn) |
| x | x | x | [Skåne län, **region**](http://data.jobtechdev.se/taxonomy/concept/CaRE_1nn_cSU) |
| x | x | x | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| x |  |  | [Omvårdnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/MR4H_FFT_mTj) |
| x | x | x | [Psykiatrisk öppenvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/N9zh_vwb_Wht) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Kriminalvården, **keyword**](http://data.jobtechdev.se/taxonomy/concept/S84z_MKP_aV6) |
| x | x | x | [Vårdavdelning/Mottagning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TuUX_3Jo_wyq) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x | x | x | [Kollektivtrafik, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/XVG5_tVe_RFd) |
| x | x | x | [Sjuksköterska, grundutbildad, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/bXNH_MNX_dUR) |
|  | x |  | [Specialistsjuksköterskeutbildning, operationssjukvård, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/bf2X_Hfm_De9) |
| x | x | x | [Vuxenpsykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/btyH_9au_jQS) |
| x |  |  | [neuropsykiatri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hVTC_kQL_qY1) |
| x | x | x | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| x | x | x | [Psykiatri, **skill**](http://data.jobtechdev.se/taxonomy/concept/sXoR_gGY_ycN) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **12** | 12/20 = **60%** |