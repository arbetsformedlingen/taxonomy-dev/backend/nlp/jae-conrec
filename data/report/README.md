# Report for annotated documents

| Dataset | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [mentor-api-prod: validation](mentor-api-prod__splitclassvalidation/README.md) | 341 | 779348 | 5332 | 4942 | 26790/86481 = **31%** | 1920/5132 = **37%** |
| **Total** | 341 | 779348 | 5332 | 4942 | 26790/86481 = **31%** | 1920/5132 = **37%** |