# Results for '23e558a19fd6a812f6f3bc93dbb6aeaf66fa9b53'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [23e558a19fd6a812f6f3bc93dbb6aeaf66fa9b53](README.md) | 1 | 1748 | 13 | 11 | 98/144 = **68%** | 6/10 = **60%** |

## Source text

Nattreceptionist (80%) Vi söker dig med ett stort leende! Tillsammans med vårt fantastiska team kommer du att arbeta på natten i Torekov Hotells reception och vara en serviceinriktad och positiv person som alltid möter våra gäster med vänlighet och ett stort leende. Som nattreceptionist hos oss är dina arbetsuppgifter varierande: bl.a. in-/utcheckning, möblering av konferenslokaler och städning av gym. Om oss: Torekov Hotell är ett familjärt hotell längst ute på Bjärehalvön. Vår vision är att bli södra Sveriges och Själlands mest eftertraktade ”hemliga gömställe” och det betyder att vi skall skapa en plats för våra gäster så att de känner att de har hittat sitt smultronställe. Detta är ett arbete som vi gör tillsammans över alla avdelningar. Hos oss är alla medarbetare lika viktiga då alla bidrar på sitt sätt för att nå vår vision.    Supernöjd gäst och supernöjd medarbetare är ledstjärnor för oss. Om du delar våra värderingar där ditt jobb präglas av stolthet, att vi arbetar tillsammans, att vi är lyhörda och ser varandras och gästens behov och där omtanke är viktigt då passar du för oss och vi för dig.  Om dig: Som person ska du ha en genuin känsla för service och ett stort intresse för våra gäster. Du måste vara ansvarsfull, flexibel, ta egna initiativ och har problemlösningsförmåga. Du är organiserad och ordningsam och bör ha mycket goda kunskaper i svenska och engelska. Vi vill naturligtvis att du har goda referenser och trivs att arbeta självständigt och ensam som nattreceptionist.  Tjänstens omfattning och arbetstider: Vikariat från 10 oktober 2022 till 1 november 2023. 80% arbetstid 22.00–07.00 varannan vecka måndag–söndag (jämna veckor)  Urval och intervjuer sker löpande, så skicka din ansökan så snart du kan.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Nattreceptionist** (80%) Vi söker dig med ett st... | x | x | 16 | 16 | [Nattreceptionist/Nattportier, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zHXG_Wfc_TY7) |
| 2 | Nattreceptionist (**80%**) Vi söker dig med ett stort l... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 3 | ...tt arbeta på natten i Torekov **Hotells** reception och vara en service... | x |  |  | 7 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 4 | ...a på natten i Torekov Hotells **reception** och vara en serviceinriktad o... | x | x | 9 | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 5 | ...het och ett stort leende. Som **nattreceptionist** hos oss är dina arbetsuppgift... | x | x | 16 | 16 | [Nattreceptionist/Nattportier, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zHXG_Wfc_TY7) |
| 6 | ...ering av konferenslokaler och **städning** av gym. Om oss: Torekov Hotel... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 7 | ...dning av gym. Om oss: Torekov **Hotell** är ett familjärt hotell längs... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 8 | ...rekov Hotell är ett familjärt **hotell** längst ute på Bjärehalvön. Vå... | x |  |  | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 9 | ...ör våra gäster. Du måste vara **ansvarsfull**, flexibel, ta egna initiativ ... |  | x |  | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 10 | ...ör ha mycket goda kunskaper i **svenska** och engelska. Vi vill naturli... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 11 | ... goda kunskaper i svenska och **engelska**. Vi vill naturligtvis att du ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 12 | ...goda referenser och trivs att **arbeta självständigt** och ensam som nattreceptionis... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 13 | ...a självständigt och ensam som **nattreceptionist**.  Tjänstens omfattning och ar... | x | x | 16 | 16 | [Nattreceptionist/Nattportier, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zHXG_Wfc_TY7) |
| 14 | ...s omfattning och arbetstider: **Vikariat** från 10 oktober 2022 till 1 n... | x |  |  | 8 | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| 15 | ...er 2022 till 1 november 2023. **80%** arbetstid 22.00–07.00 varanna... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| | **Overall** | | | **98** | **144** | 98/144 = **68%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| x | x | x | [Nattreceptionist/Nattportier, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zHXG_Wfc_TY7) |
| x | x | x | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/10 = **60%** |