# Results for '414754a64b126c8cbda62894a3b7785fcb33e522'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [414754a64b126c8cbda62894a3b7785fcb33e522](README.md) | 1 | 2151 | 18 | 18 | 179/257 = **70%** | 11/13 = **85%** |

## Source text

Engagerad och driftiga kyltekniker Vår kund är en av de större aktörerna som har inlett ett spännande samarbete inom fordonsindustrin med flertalet siter i Sverige. Vi söker nu kyltekniker med uppdrag i Göteborg för att nå gemensamt uppsatta mål tillsammans med kunden och säkerställa service i världsklass. Har du erfarenhet, gillar att utveckla, effektivisera och ge kunden exceptionell service, då är du rätt person för oss!  Arbetsbeskrivning Som kyltekniker arbetar du dagligen med felsökning, reparationer samt service av diverse varierande kyl och värmeanläggningar. Variationen är bred och innefattar alltifrån komfortkyla till industrikyla. Arbetet är på heltid, dagtid. Tillträde 1 maj.  Arbetsuppgifterna innefattar bl.a. Service och underhåll, kalibrering/optimering av främst kyltekniska aggregat och anläggningar  Felsökning och reparationer, eventuellt vissa nyinstallationer i mindre skala  Årliga/periodiska kontroller inkl. dokumentation till berörda myndigheter  Självständigt arbete, utförande och ansvar för olika kundprojekt i mindre skala  Daglig kundkontakt där kundservice och bemötande är av största vikt  Vem är du? Du är certifierad kyltekniker kategori 1 med minst några års erfarenhet från branschen  Du har god erfarenhet av felsökning  reparationer samt kalibrering/optimering av kyltekniska installationer  Du är van vid att jobba självständigt och trivs med att ha frihet under ansvar  Du har förmågan att skapa goda kundrelationer och är ansvarstagande  Eftersom du kommer arbeta med olika uppgifter och personer är flexibilitet och god samarbetsförmåga ett måste  Vad kan ISS erbjuda dig? Trygga anställningsvillkor med kollektivavtal  Friskvårdsbidrag  Utbildningar och talangprogram för att du ska engageras och utvecklas  Värdefulla erfarenheter, vilket ger dig många olika karriärmöjligheter framåt  Intervjuer hålls löpande och tjänsten kan komma att tillsättas innan sista ansökningsdag 24 april 2022, skicka därför gärna in din ansökan redan nu.  Vid frågor om tjänsten är du välkommen att kontakta Mattias Molin +46 (0)73 523 08 90 eller Benjamin Sanros +46 (0)76 114 05 50  Varmt välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Engagerad och driftiga **kyltekniker** Vår kund är en av de större a... | x | x | 11 | 11 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 2 | ...dustrin med flertalet siter i **Sverige**. Vi söker nu kyltekniker med ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 3 | ... siter i Sverige. Vi söker nu **kyltekniker** med uppdrag i Göteborg för at... | x | x | 11 | 11 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 4 | ... nu kyltekniker med uppdrag i **Göteborg** för att nå gemensamt uppsatta... | x | x | 8 | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 5 | ...r oss!  Arbetsbeskrivning Som **kyltekniker** arbetar du dagligen med felsö... | x | x | 11 | 11 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 6 | ...niker arbetar du dagligen med **felsökning**, reparationer samt service av... | x | x | 10 | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 7 | ...av diverse varierande kyl och **värmeanläggningar**. Variationen är bred och inne... |  | x |  | 17 | [Värmeanläggning, underhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/jZzL_yV6_PmA) |
| 8 | ...ar alltifrån komfortkyla till **industrikyla**. Arbetet är på heltid, dagtid... | x | x | 12 | 12 | [Industrikyla, **skill**](http://data.jobtechdev.se/taxonomy/concept/wfV5_K2z_mBE) |
| 9 | ... bl.a. Service och underhåll, **kalibrering**/optimering av främst kyltekni... | x | x | 11 | 11 | [Kalibrering, **skill**](http://data.jobtechdev.se/taxonomy/concept/1BKz_4at_cuy) |
| 10 | ...ka aggregat och anläggningar  **Felsökning** och reparationer, eventuellt ... | x | x | 10 | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 11 | ...a/periodiska kontroller inkl. **dokumentation** till berörda myndigheter  Sjä... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 12 | ...ion till berörda myndigheter  **Självständigt arbete**, utförande och ansvar för oli... | x |  |  | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 13 | ...skala  Daglig kundkontakt där **kundservice** och bemötande är av största v... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 14 | ...törsta vikt  Vem är du? Du är **certifierad **kyltekniker kategori 1 med min... |  | x |  | 12 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 15 | ... Vem är du? Du är certifierad **kyltekniker** kategori 1 med minst några år... | x | x | 11 | 11 | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| 16 | ...hen  Du har god erfarenhet av **felsökning**  reparationer samt kalibrerin... |  | x |  | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 17 | ...felsökning  reparationer samt **kalibrering**/optimering av kyltekniska ins... | x | x | 11 | 11 | [Kalibrering, **skill**](http://data.jobtechdev.se/taxonomy/concept/1BKz_4at_cuy) |
| 18 | ...allationer  Du är van vid att **jobba självständigt** och trivs med att ha frihet u... | x |  |  | 19 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 19 | ...u har förmågan att skapa goda **kundrelationer** och är ansvarstagande  Efters... | x | x | 14 | 14 | [Kundrelationer, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hPWL_HNB_UFP) |
| 20 | ...pa goda kundrelationer och är **ansvarstagande**  Eftersom du kommer arbeta me... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 21 | ...rygga anställningsvillkor med **kollektivavtal**  Friskvårdsbidrag  Utbildning... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **179** | **257** | 179/257 = **70%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Kalibrering, **skill**](http://data.jobtechdev.se/taxonomy/concept/1BKz_4at_cuy) |
| x | x | x | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| x | x | x | [Kylmontör/Kyltekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JM78_Rwi_sHx) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x | x | x | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Kundrelationer, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hPWL_HNB_UFP) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Värmeanläggning, underhåll, **skill**](http://data.jobtechdev.se/taxonomy/concept/jZzL_yV6_PmA) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x | x | x | [Industrikyla, **skill**](http://data.jobtechdev.se/taxonomy/concept/wfV5_K2z_mBE) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **11** | 11/13 = **85%** |