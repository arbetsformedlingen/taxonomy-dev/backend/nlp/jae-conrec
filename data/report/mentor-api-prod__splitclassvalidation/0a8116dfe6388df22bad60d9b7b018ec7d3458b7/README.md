# Results for '0a8116dfe6388df22bad60d9b7b018ec7d3458b7'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0a8116dfe6388df22bad60d9b7b018ec7d3458b7](README.md) | 1 | 558 | 5 | 7 | 7/147 = **5%** | 1/7 = **14%** |

## Source text

Medicinsk Sekreterare är för dig som vill arbeta med vårdadministration offentligt eller privat. Stora förändringar i samband med hantering av vårdens information i form av journaler, diagnoser etc pågår. Processerna kring vårdadministration digitaliseras alltmer. Den nya yrkesrollen får ett allt tydligare IT-stöd.  Du får under fyra terminer en heltäckande utbildning som medicinsk sekreterare, i samarbete med Region i Dalarna. Utbildningen är på heltid och LIA - lärande i arbete – ingår.  I vår utbildning går 90 % ut i relevant arbete efter studier.  

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Medicinsk Sekreterare** är för dig som vill arbeta me... | x |  |  | 21 | [Läkarsekreterare/Vårdadministratör/Medicinsk sekreterare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/J5oq_tNd_6RU) |
| 1 | **Medicinsk Sekreterare** är för dig som vill arbeta me... |  | x |  | 21 | [medicinsk sekreterare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/QGm6_5mD_bry) |
| 2 | ...r för dig som vill arbeta med **vårdadministration** offentligt eller privat. Stor... |  | x |  | 18 | [Hälso- och sjukvårdsadministration, **keyword**](http://data.jobtechdev.se/taxonomy/concept/TE56_DgF_dGa) |
| 3 | ... etc pågår. Processerna kring **vårdadministration** digitaliseras alltmer. Den ny... |  | x |  | 18 | [Hälso- och sjukvårdsadministration, **keyword**](http://data.jobtechdev.se/taxonomy/concept/TE56_DgF_dGa) |
| 4 | ...rollen får ett allt tydligare **IT-stöd**.  Du får under fyra terminer ... |  | x |  | 7 | [IT-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1Nz1_z46_39V) |
| 4 | ...rollen får ett allt tydligare **IT-stöd**.  Du får under fyra terminer ... |  | x |  | 7 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 5 | ...en heltäckande utbildning som **medicinsk sekreterare**, i samarbete med Region i Dal... | x |  |  | 21 | [Läkarsekreterare/Vårdadministratör/Medicinsk sekreterare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/J5oq_tNd_6RU) |
| 5 | ...en heltäckande utbildning som **medicinsk sekreterare**, i samarbete med Region i Dal... |  | x |  | 21 | [medicinsk sekreterare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/QGm6_5mD_bry) |
| 6 | ...are, i samarbete med Region i **Dalarna**. Utbildningen är på heltid oc... | x | x | 7 | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 7 | ...i Dalarna. Utbildningen är på **heltid** och LIA - lärande i arbete – ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **7** | **147** | 7/147 = **5%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [IT-support, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1Nz1_z46_39V) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Läkarsekreterare/Vårdadministratör/Medicinsk sekreterare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/J5oq_tNd_6RU) |
|  | x |  | [medicinsk sekreterare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/QGm6_5mD_bry) |
|  | x |  | [Hälso- och sjukvårdsadministration, **keyword**](http://data.jobtechdev.se/taxonomy/concept/TE56_DgF_dGa) |
|  | x |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| | | **1** | 1/7 = **14%** |