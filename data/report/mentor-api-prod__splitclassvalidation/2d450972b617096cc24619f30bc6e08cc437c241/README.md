# Results for '2d450972b617096cc24619f30bc6e08cc437c241'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2d450972b617096cc24619f30bc6e08cc437c241](README.md) | 1 | 4819 | 29 | 25 | 124/577 = **21%** | 12/38 = **32%** |

## Source text

Är du en prestigelös och lösningsorienterad ledare med koll på vattenrening? Vill du ta nästa kliv i karriären och samtidigt bli en del av en dynamisk och väletablerad verksamhet som tar ansvar för och gör skillnad för miljön? Då har vi rollen för dig! Sök tjänsten som Production Manager Water Treatment hos Fortum Recycling and Waste redan idag.  Om Fortum   Fortum Recycling and Waste erbjuder tjänster inom miljövård och materialeffektivitet i Norden. Vårt mål är att spara på naturresurser och främja en cirkulär ekonomi. Vi förbättrar material- och energieffektiviteten för våra kunder genom att erbjuda lösningar för återanvändning, återvinning, avfallshantering och slutdeponering samt tjänster för marksanering och miljöbyggnation. Fortum Recycling and Waste Solutions har nästan 650 medarbetare i Sverige, Finland, Danmark och Norge.  Om rollen   Som Production Manager Water Treatment ansvarar du för reningen av det vatten som används och som regnar på Fortums anläggning i Norrtorp, Kumla. På anläggningen i Norrtorp finns två reningsanläggningar, varav det ena är rening för utgående vatten. Tillsammans med två processoperatörer, som du också har personal- och arbetsmiljöansvar för, ansvarar du för   - Genomförande av tester för olika behandlingsprocesser  - Provtagning av inkommande flöden och processvatten  - Ansvara för löpande fortbildning av personal  - Rapportering och myndighetskontakter  - Driva processutvecklingen av olika reningstekniker  - Utveckling av driftsrutiner och instruktioner.  Rollen är placerad på Fortums anläggning i Norrtorp, Kumla, med enstaka resor till andra anläggningar i Hakunge och Sundsvall. Du ingår i ledningsgruppen för Material Recycling Center, som servar samtliga anläggningar inom Fortum Waste Solutions Sweden.  Varför Fortum?  Är du redo för ditt livs största utmaning? För att kunna driva på övergången till en renare värld måste vi omforma energisystemet. En öppen kultur är nyckeln till att nå våra mål. Med ömsesidigt förtroende, en tro på människors förmåga och genom att se till att alla mår bra kan vi nå ännu bättre resultat. Flexibilitet är det nya normala och vårt hybrida arbetssätt tillämpas när det är möjligt.  För dig innebär ett arbete på Fortum spännande möjligheter att utveckla din expertis och skapa en meningsfull karriär – och vi kommer stötta dig på den resan. Vi tror att mångfald och inkludering inspirerar oss alla att vara innovativa och växa tillsammans. Därför strävar vi efter att bygga team där alla känner sig inkluderade och behandlas lika.  Dina kvalifikationer  Vi söker dig som har relevant utbildningsbakgrund, helst högskoleutbildning inom kemi- eller miljövetenskap, men även genomförd yrkeshögskoleutbildning inom exempelvis vatten- och miljöteknik i kombination med relevant arbetslivserfarenhet är av intresse. Vi tror att du är tekniskt kunnig och har tidigare arbetslivserfarenhet av vattenrening i någon form, antingen inom industrin, konsultbranschen eller inom offentlig sektor, med fördel i ledande position.  Det är meriterande om du ha erfarenhet av ledarskap och projektledning. Truckkort är också meriterande.    Dina personliga egenskaper   Som person är du prestigelös, empatisk och lösningsorienterad. Du kommer ha många samarbetsytor inom organisationen och arbeta nära platsansvariga och marknadssidan, varför det är viktigt att du är kommunikativ och kan samarbeta brett inom organisationen.  Du gillar att arbeta operativt och verksamhetsnära, vilket också syns i ditt ledarskap. Du finns till hands för dina medarbetare, leder och fördelar arbetet på ett stöttande och coachande sätt för att ni tillsammans ska bidra till att verksamheten utvecklas och når uppsatta mål. Du står för trygghet och stabilitet samtidigt som du tycker om att jobba med utveckling och förbättringar, där du kommer ha goda möjligheter att påverka och utforma verksamhetens processer.  Vi lägger stor vikt vid dina personliga egenskaper i den här rekryteringen.  Information och ansökan I den här rekryteringen samarbetar Fortum Recycling and Waste med AxÖ Consulting. Ansökan görs via www.axoconsulting.se senast den 21 augusti 2022. Vi jobbar med löpande urval så tjänsten kan komma att tillsättas innan sista ansökningsdatum, men kontakter och intervjuer kommer att påbörjas först i augusti.  Vi svarar gärna på frågor från dig. Ring eller mejla rekryteringskonsult Kajsa Rosén, kajsa.rosen@axoconsulting.se tfn: 072-398 02 67. Alla ansökningar behandlas konfidentiellt.  Varmt välkommen med din ansökan!  AxÖ Consulting AB har lång och gedigen erfarenhet från rekryterings- och konsultbranschen och har arbetat med såväl privat marknad som inom offentlig sektor. För mer information och ansökan besök www.axoconsulting.se. Vi tar tyvärr inte emot ansökningar via brev eller e-post.  Varmt välkommen med din ansökan.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...orienterad ledare med koll på **vattenrening**? Vill du ta nästa kliv i karr... | x |  |  | 12 | (not found in taxonomy) |
| 2 | ...len för dig! Sök tjänsten som **Production Manager Water Treatment** hos Fortum Recycling and Wast... | x |  |  | 34 | [produktionschef, vattenbruk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/fq4j_P5n_KnR) |
| 3 | ... Waste erbjuder tjänster inom **miljövård** och materialeffektivitet i No... | x | x | 9 | 9 | [Miljövård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/v5Fo_6Gp_imd) |
| 4 | ...tet i Norden. Vårt mål är att **spara på **naturresurser och främja en ci... | x |  |  | 9 | [bevara naturresurser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZVFf_YCU_bmH) |
| 5 | ...den. Vårt mål är att spara på **naturresurser** och främja en cirkulär ekonom... | x | x | 13 | 13 | [bevara naturresurser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZVFf_YCU_bmH) |
| 6 | ...å naturresurser och främja en **cirkulär ekonomi**. Vi förbättrar material- och ... | x |  |  | 16 | [cirkulär ekonomi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USnm_nT7_jqG) |
| 7 | ...surser och främja en cirkulär **ekonomi**. Vi förbättrar material- och ... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 8 | .... Vi förbättrar material- och **energieffektiviteten** för våra kunder genom att erb... | x |  |  | 20 | [energieffektivitet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BKi5_ZKS_iyz) |
| 9 | ...lösningar för återanvändning, **återvinning**, avfallshantering och slutdep... | x | x | 11 | 11 | [Återvinning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/dHL8_syv_oST) |
| 10 | ... återanvändning, återvinning, **avfallshantering** och slutdeponering samt tjäns... | x | x | 16 | 16 | [avfallshantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gnUk_f4X_HnN) |
| 11 | ... har nästan 650 medarbetare i **Sverige**, Finland, Danmark och Norge. ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 12 | ...an 650 medarbetare i Sverige, **Finland**, Danmark och Norge.  Om rolle... | x |  |  | 7 | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
| 13 | ...darbetare i Sverige, Finland, **Danmark** och Norge.  Om rollen   Som P... | x |  |  | 7 | [Danmark, **country**](http://data.jobtechdev.se/taxonomy/concept/Co3h_1xq_Cwb) |
| 14 | ...Sverige, Finland, Danmark och **Norge**.  Om rollen   Som Production ... | x |  |  | 5 | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
| 15 | ...k och Norge.  Om rollen   Som **Production Manager Water Treatment** ansvarar du för reningen av d... | x |  |  | 34 | [produktionschef, vattenbruk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/fq4j_P5n_KnR) |
| 16 | ...ortums anläggning i Norrtorp, **Kumla**. På anläggningen i Norrtorp f... | x | x | 5 | 5 | [Kumla, **municipality**](http://data.jobtechdev.se/taxonomy/concept/viCA_36P_pQp) |
| 17 | ...soperatörer, som du också har **personal**- och arbetsmiljöansvar för, a... | x | x | 8 | 8 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| 18 | ...rer, som du också har personal**- och **arbetsmiljöansvar för, ansvara... |  | x |  | 6 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| 19 | ...e flöden och processvatten  - **Ansvara för löpande fortbildning** av personal  - Rapportering o... | x |  |  | 32 | [genomföra återkommande fortbildning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5USM_HMS_kpV) |
| 20 | ...a för löpande fortbildning av **personal**  - Rapportering och myndighet... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 21 | ... processutvecklingen av olika **reningstekniker**  - Utveckling av driftsrutine... | x |  |  | 15 | (not found in taxonomy) |
| 22 | ...ortums anläggning i Norrtorp, **Kumla**, med enstaka resor till andra... | x | x | 5 | 5 | [Kumla, **municipality**](http://data.jobtechdev.se/taxonomy/concept/viCA_36P_pQp) |
| 23 | ... Norrtorp, Kumla, med enstaka **resor** till andra anläggningar i Hak... | x |  |  | 5 | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| 24 | ...ra anläggningar i Hakunge och **Sundsvall**. Du ingår i ledningsgruppen f... | x | x | 9 | 9 | [Sundsvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dJbx_FWY_tK6) |
| 25 | ...nt utbildningsbakgrund, helst **högskoleutbildning** inom kemi- eller miljövetensk... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 26 | ...nt utbildningsbakgrund, helst **högskoleutbildning inom kemi-** eller miljövetenskap, men äve... | x |  |  | 29 | [Kemi, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/vpZY_6iv_NPL) |
| 27 | ...leutbildning inom kemi- eller **miljövetenskap**, men även genomförd yrkeshögs... |  | x |  | 14 | [Miljövetenskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/2T3v_Cnr_6hp) |
| 27 | ...leutbildning inom kemi- eller **miljövetenskap**, men även genomförd yrkeshögs... |  | x |  | 14 | [Biologi och miljövetenskap, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2mWM_Ngb_xQh) |
| 27 | ...leutbildning inom kemi- eller **miljövetenskap**, men även genomförd yrkeshögs... |  | x |  | 14 | [Biologi och miljövetenskap, allmän utbildning, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/3q44_gdP_4fx) |
| 27 | ...leutbildning inom kemi- eller **miljövetenskap**, men även genomförd yrkeshögs... |  | x |  | 14 | [Biologi och miljövetenskap, **sun-education-field-2**](http://data.jobtechdev.se/taxonomy/concept/4rRq_Nyp_tCB) |
| 27 | ...leutbildning inom kemi- eller **miljövetenskap**, men även genomförd yrkeshögs... |  | x |  | 14 | [Miljövetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6n7M_QBo_ryQ) |
| 27 | ...leutbildning inom kemi- eller **miljövetenskap**, men även genomförd yrkeshögs... |  | x |  | 14 | [Biologi och miljövetenskap, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/7NR9_sfW_HE8) |
| 27 | ...leutbildning inom kemi- eller **miljövetenskap**, men även genomförd yrkeshögs... |  | x |  | 14 | [Miljövetenskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/KrzH_5hF_XVN) |
| 27 | ...leutbildning inom kemi- eller **miljövetenskap**, men även genomförd yrkeshögs... | x | x | 14 | 14 | [Miljövetenskap, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/cs1N_Zp4_KgB) |
| 27 | ...leutbildning inom kemi- eller **miljövetenskap**, men även genomförd yrkeshögs... |  | x |  | 14 | [Biologi och miljövetenskap, övrig och ospecificerad utbildning, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/sTMt_rKA_EHs) |
| 28 | ...vetenskap, men även genomförd **yrkeshögskoleutbildning** inom exempelvis vatten- och m... | x |  |  | 23 | [Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/Dxdt_jZu_Je3) |
| 29 | ...oleutbildning inom exempelvis **vatten-** och miljöteknik i kombination... | x | x | 7 | 7 | [Vattenteknik, ej ingenjörsutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rJgz_Q8j_umn) |
| 30 | ...oleutbildning inom exempelvis **vatten- och **miljöteknik i kombination med ... |  | x |  | 12 | [Civilingenjörsutbildning, miljö- och vattenteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/f6Z8_Kjv_jbQ) |
| 31 | ...ldning inom exempelvis vatten-** och **miljöteknik i kombination med ... |  | x |  | 5 | [Vattenteknik, ej ingenjörsutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rJgz_Q8j_umn) |
| 32 | ...g inom exempelvis vatten- och **miljöteknik** i kombination med relevant ar... | x | x | 11 | 11 | [Miljöteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/S4Ku_uT7_yuV) |
| 33 | ...a erfarenhet av ledarskap och **projektledning**. Truckkort är också meriteran... | x |  |  | 14 | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
| 33 | ...a erfarenhet av ledarskap och **projektledning**. Truckkort är också meriteran... |  | x |  | 14 | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| 34 | ...ledarskap och projektledning. **Truckkort** är också meriterande.    Dina... | x | x | 9 | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 35 | ... arbetet på ett stöttande och **coachande** sätt för att ni tillsammans s... | x |  |  | 9 | [Coaching/Coachning, **skill**](http://data.jobtechdev.se/taxonomy/concept/91Ef_58Y_jX4) |
| | **Overall** | | | **124** | **577** | 124/577 = **21%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Miljövetenskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/2T3v_Cnr_6hp) |
|  | x |  | [Biologi och miljövetenskap, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/2mWM_Ngb_xQh) |
| x |  |  | [Projektledning, erfarenhet, **skill**](http://data.jobtechdev.se/taxonomy/concept/3TSx_fJE_Z26) |
|  | x |  | [Biologi och miljövetenskap, allmän utbildning, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/3q44_gdP_4fx) |
|  | x |  | [Biologi och miljövetenskap, **sun-education-field-2**](http://data.jobtechdev.se/taxonomy/concept/4rRq_Nyp_tCB) |
| x |  |  | [genomföra återkommande fortbildning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/5USM_HMS_kpV) |
|  | x |  | [Miljövetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/6n7M_QBo_ryQ) |
|  | x |  | [Biologi och miljövetenskap, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/7NR9_sfW_HE8) |
| x |  |  | [Coaching/Coachning, **skill**](http://data.jobtechdev.se/taxonomy/concept/91Ef_58Y_jX4) |
| x |  |  | [energieffektivitet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/BKi5_ZKS_iyz) |
| x |  |  | [Danmark, **country**](http://data.jobtechdev.se/taxonomy/concept/Co3h_1xq_Cwb) |
| x |  |  | [Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/Dxdt_jZu_Je3) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
|  | x |  | [Miljövetenskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/KrzH_5hF_XVN) |
|  | x |  | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
| x | x | x | [Miljöteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/S4Ku_uT7_yuV) |
| x |  |  | [cirkulär ekonomi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USnm_nT7_jqG) |
| x |  |  | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
| x | x | x | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x | x | x | [bevara naturresurser, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZVFf_YCU_bmH) |
| x | x | x | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| x | x | x | [Miljövetenskap, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/cs1N_Zp4_KgB) |
| x | x | x | [Återvinning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/dHL8_syv_oST) |
| x | x | x | [Sundsvall, **municipality**](http://data.jobtechdev.se/taxonomy/concept/dJbx_FWY_tK6) |
|  | x |  | [Civilingenjörsutbildning, miljö- och vattenteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/f6Z8_Kjv_jbQ) |
| x |  |  | [produktionschef, vattenbruk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/fq4j_P5n_KnR) |
| x | x | x | [avfallshantering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/gnUk_f4X_HnN) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | (not found in taxonomy) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x |  |  | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| x | x | x | [Vattenteknik, ej ingenjörsutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/rJgz_Q8j_umn) |
|  | x |  | [Biologi och miljövetenskap, övrig och ospecificerad utbildning, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/sTMt_rKA_EHs) |
| x | x | x | [Miljövård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/v5Fo_6Gp_imd) |
| x | x | x | [Kumla, **municipality**](http://data.jobtechdev.se/taxonomy/concept/viCA_36P_pQp) |
| x |  |  | [Kemi, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/vpZY_6iv_NPL) |
| | | **12** | 12/38 = **32%** |