# Results for '9a780393f3289fe6180f1dc5474899dc6a509439'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [9a780393f3289fe6180f1dc5474899dc6a509439](README.md) | 1 | 2505 | 26 | 26 | 147/499 = **29%** | 8/31 = **26%** |

## Source text

Trädgårdsmästare till Helsingborg Ramlösa Trädgårdsservice AB är ett familjeföretag som har över 15 års erfarenhet av branschen. Vi arbetar med fastighetsskötsel av grönytor för företag, samfälligheter, kommunala förvaltningar samt privatpersoner. Ramlösa Trädgårdsservice AB anlägger, sköter och underhåller den yttre miljön åt ägare och förvaltare av park och fastighetsmark. Verksamheten har fast etablering i Helsingborgsregionen men etableringsplaner för närliggande kommuner finns för framtiden. Arbetsgruppen du kommer att ingå i utgår från vårt platskontor i Ramlösa som ligger i de södra delarna av Helsingborg i närheten till Ramlösa station. De primära arbetsuppgifterna är bland annat beskärning av buskar, träd och perenner, plantering, gräsklippning, häckklippning, skötsel av rabatter, röjningsarbete, renhållning och fastighetsservice. Vi söker dig som är tillgänglig under hela perioden och kan arbeta heltid från mars till den sista Oktober med god chans till förlängning. Arbetsgruppen består under högsäsong av 6-10 personer som arbetar i mindre lag men ensamarbete kan även förekomma. För att jobba som trädgårdsmästare krävs god fysik, arbetsvilja och ett öga för detaljer. Det är ett plus om du har maskinvana och känner dig bekväm med att använda maskiner och verktyg. Tjänsten är heltid, vardagar 07.00-16:00 med start i Juni fram till siste oktober med god chans till förlängning. Vi söker dig Du bör vara flexibel, ha en stor arbetsvilja och vara redo att ta i där det behövs. Du trivs utomhus, är prestigelös och kan tänka dig att ta dig an alla sorters arbetsuppgifter. För att lyckas i tjänsten behöver du vara noggrann och mån om att hålla hög kvalitet i det arbete du utför. Du har förmågan att arbeta självständigt, driva ditt eget arbete framåt men också ha en god förmåga att samarbeta med andra. Punktlig och kommer alltid i tid. Du besitter kunskapen och förstår vad som krävs av dig. Kvalifikationer för arbetet ⦁   Gymnasial utbildning. ⦁ Dokumenterad utbildning som Trädgårdsmästare ⦁   Svenskt B körkort och körvana med släp. ⦁   Goda kunskaper inom svenska språket. ⦁   Vana vid maskiner, gärna dokumenterad (motorsåg etc) Det är meriterande om du ⦁   Har arbetat som trädgårdsmästare tidigare. ⦁   Har gått utbildningar såsom Heta arbeten, Arbete på väg, röjsåg, motorsåg mfl. ⦁ Har BE-körkort Vi ser fram emot er ansökan och hoppas på att få träffa er!   Då vi kommer att hålla intervjuer löpande ser vi gärna att du skickar in din ansökan så snart som möjligt.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Trädgårdsmästare** till Helsingborg Ramlösa Träd... | x | x | 16 | 16 | [Trädgårdsmästare, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JZur_dmf_EPY) |
| 2 | Trädgårdsmästare till **Helsingborg** Ramlösa Trädgårdsservice AB ä... | x | x | 11 | 11 | [Helsingborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qj3q_oXH_MGR) |
| 3 | ... av branschen. Vi arbetar med **fastighetsskötsel** av grönytor för företag, samf... |  | x |  | 17 | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| 3 | ... av branschen. Vi arbetar med **fastighetsskötsel** av grönytor för företag, samf... |  | x |  | 17 | [VVS, fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/jXSx_Bm8_bNX) |
| 3 | ... av branschen. Vi arbetar med **fastighetsskötsel** av grönytor för företag, samf... |  | x |  | 17 | [Fastighetsskötsel, teknisk utbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/whKP_y3y_2ry) |
| 4 | ... ligger i de södra delarna av **Helsingborg** i närheten till Ramlösa stati... | x | x | 11 | 11 | [Helsingborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qj3q_oXH_MGR) |
| 5 | ...etsuppgifterna är bland annat **beskärning** av buskar, träd och perenner,... |  | x |  | 10 | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
| 6 | ...av buskar, träd och perenner, **plantering**, gräsklippning, häckklippning... |  | x |  | 10 | [plantera gröna växter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Bz5Z_Zc2_YkF) |
| 7 | ...räd och perenner, plantering, **gräsklippning**, häckklippning, skötsel av ra... |  | x |  | 13 | [använda olika typer av utrustning för gräsklippning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/b2w6_YKJ_qZX) |
| 8 | ...r, plantering, gräsklippning, **häckklippning**, skötsel av rabatter, röjning... |  | x |  | 13 | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| 8 | ...r, plantering, gräsklippning, **häckklippning**, skötsel av rabatter, röjning... |  | x |  | 13 | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
| 9 | ... av rabatter, röjningsarbete, **renhållning** och fastighetsservice. Vi sök... |  | x |  | 11 | [Renhållning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1eVm_yZa_9yi) |
| 10 | ... hela perioden och kan arbeta **heltid** från mars till den sista Okto... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 11 | ... förekomma. För att jobba som **trädgårdsmästare** krävs god fysik, arbetsvilja ... | x | x | 16 | 16 | [Trädgårdsmästare, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JZur_dmf_EPY) |
| 12 | ...ba som trädgårdsmästare krävs **god fysik**, arbetsvilja och ett öga för ... | x | x | 9 | 9 | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| 13 | ...och känner dig bekväm med att **använda maskiner och verktyg**. Tjänsten är heltid, vardagar... | x |  |  | 28 | [maskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Ka3C_YyD_qTA) |
| 14 | ...iner och verktyg. Tjänsten är **heltid**, vardagar 07.00-16:00 med sta... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 15 | ...as i tjänsten behöver du vara **noggrann** och mån om att hålla hög kval... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 16 | ...du utför. Du har förmågan att **arbeta självständigt**, driva ditt eget arbete framå... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 17 | ...n också ha en god förmåga att **samarbeta** med andra. Punktlig och komme... | x |  |  | 9 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 18 | ...alifikationer för arbetet ⦁   **Gymnasial utbildning**. ⦁ Dokumenterad utbildning so... | x |  |  | 20 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 18 | ...alifikationer för arbetet ⦁   **Gymnasial utbildning**. ⦁ Dokumenterad utbildning so... |  | x |  | 20 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 19 | ...t ⦁   Gymnasial utbildning. ⦁ **Dokumenterad utbildning** som Trädgårdsmästare ⦁   Sven... | x |  |  | 23 | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| 20 | ...⦁ Dokumenterad utbildning som **Trädgårdsmästare** ⦁   Svenskt B körkort och kör... | x | x | 16 | 16 | [Trädgårdsmästare, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JZur_dmf_EPY) |
| 21 | ... Trädgårdsmästare ⦁   Svenskt **B körkort** och körvana med släp. ⦁   God... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 22 | ...are ⦁   Svenskt B körkort och **körvana med släp**. ⦁   Goda kunskaper inom sven... | x |  |  | 16 | [Släp, körvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/XGCK_5qV_wPd) |
| 23 | ...släp. ⦁   Goda kunskaper inom **svenska** språket. ⦁   Vana vid maskine... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 24 | ...   Goda kunskaper inom svenska** språket**. ⦁   Vana vid maskiner, gärna... | x |  |  | 8 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 25 | ...svenska språket. ⦁   Vana vid **maskiner**, gärna dokumenterad (motorsåg... | x |  |  | 8 | [maskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Ka3C_YyD_qTA) |
| 26 | ...maskiner, gärna dokumenterad (**motorsåg** etc) Det är meriterande om du... | x | x | 8 | 8 | [hantera motorsåg, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/B3Cd_AUp_eCF) |
| 27 | ...nde om du ⦁   Har arbetat som **trädgårdsmästare** tidigare. ⦁   Har gått utbild... | x | x | 16 | 16 | [Trädgårdsmästare, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JZur_dmf_EPY) |
| 28 | ...  Har gått utbildningar såsom **Heta arbeten**, Arbete på väg, röjsåg, motor... | x |  |  | 12 | [Certifikat, heta arbeten, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q7eC_B7g_QiU) |
| 29 | ...ildningar såsom Heta arbeten, **Arbete på väg**, röjsåg, motorsåg mfl. ⦁ Har ... | x |  |  | 13 | [Arbete på väg, nivå 1 och 2, **skill**](http://data.jobtechdev.se/taxonomy/concept/drj3_CKp_Mpp) |
| 30 | ... Heta arbeten, Arbete på väg, **röjsåg**, motorsåg mfl. ⦁ Har BE-körko... | x |  |  | 6 | [Utbildning på röjsåg, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Rxx_zRM_Dqc) |
| 31 | ...beten, Arbete på väg, röjsåg, **motorsåg** mfl. ⦁ Har BE-körkort Vi ser ... |  | x |  | 8 | [hantera motorsåg, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/B3Cd_AUp_eCF) |
| 31 | ...beten, Arbete på väg, röjsåg, **motorsåg** mfl. ⦁ Har BE-körkort Vi ser ... | x |  |  | 8 | [Utbildning på motorsåg, **skill**](http://data.jobtechdev.se/taxonomy/concept/ba8g_LRR_Sj2) |
| 32 | ..., röjsåg, motorsåg mfl. ⦁ Har **BE-körkort** Vi ser fram emot er ansökan o... |  | x |  | 10 | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
| 32 | ..., röjsåg, motorsåg mfl. ⦁ Har **BE-körkort** Vi ser fram emot er ansökan o... |  | x |  | 10 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 32 | ..., röjsåg, motorsåg mfl. ⦁ Har **BE-körkort** Vi ser fram emot er ansökan o... | x |  |  | 10 | [BE, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/bcFd_Vkt_KXL) |
| 32 | ..., röjsåg, motorsåg mfl. ⦁ Har **BE-körkort** Vi ser fram emot er ansökan o... |  | x |  | 10 | [Utökad B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/ftCQ_gFu_L4b) |
| | **Overall** | | | **147** | **499** | 147/499 = **29%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Renhållning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/1eVm_yZa_9yi) |
| x |  |  | [Utbildning på röjsåg, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Rxx_zRM_Dqc) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| x | x | x | [hantera motorsåg, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/B3Cd_AUp_eCF) |
|  | x |  | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
|  | x |  | [plantera gröna växter, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Bz5Z_Zc2_YkF) |
|  | x |  | [beskära häckar och träd, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/GShq_Sut_Wp9) |
|  | x |  | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| x | x | x | [Trädgårdsmästare, anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JZur_dmf_EPY) |
| x |  |  | [maskiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Ka3C_YyD_qTA) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Certifikat, heta arbeten, **skill**](http://data.jobtechdev.se/taxonomy/concept/Q7eC_B7g_QiU) |
|  | x |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Trädgård, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/Vr5b_b9P_3mS) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [Släp, körvana, **skill**](http://data.jobtechdev.se/taxonomy/concept/XGCK_5qV_wPd) |
|  | x |  | [använda olika typer av utrustning för gräsklippning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/b2w6_YKJ_qZX) |
| x |  |  | [Utbildning på motorsåg, **skill**](http://data.jobtechdev.se/taxonomy/concept/ba8g_LRR_Sj2) |
| x |  |  | [BE, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/bcFd_Vkt_KXL) |
| x |  |  | [Arbete på väg, nivå 1 och 2, **skill**](http://data.jobtechdev.se/taxonomy/concept/drj3_CKp_Mpp) |
|  | x |  | [Utökad B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/ftCQ_gFu_L4b) |
|  | x |  | [VVS, fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/jXSx_Bm8_bNX) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Helsingborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qj3q_oXH_MGR) |
| x | x | x | [God fysik (tunga lyft), **skill**](http://data.jobtechdev.se/taxonomy/concept/rwir_xvu_443) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
|  | x |  | [Fastighetsskötsel, teknisk utbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/whKP_y3y_2ry) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/31 = **26%** |