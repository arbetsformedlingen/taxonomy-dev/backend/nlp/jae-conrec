# Results for '238a2214088cc75c14445ef380f98f446d7e42e0'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [238a2214088cc75c14445ef380f98f446d7e42e0](README.md) | 1 | 2888 | 23 | 17 | 104/310 = **34%** | 7/24 = **29%** |

## Source text

Köksbiträde timvikarie Sigtuna kommun är en plats med stor spännvidd. Här ryms levande landsbygd, småstad och en internationell flygplats. Ett rikt kulturarv och kulturell mångfald. En stolt historia och stora framtidsplaner. Kanske är det därför drivkraften är så stark att växa, förändras och utvecklas. Det är också därför det är så spännande att arbeta här. Hos oss blir du del av en trevlig och prestigelös kultur där vi hjälps åt och tar tillvara på varandras kunskap, olikheter och erfarenheter. Arbetet är ofta omväxlande, ibland utmanande men alltid meningsfullt. Välkommen hit!  Måltidsservice ansvarar för maten och måltiden ända fram till matgästen i förskola, skola och gymnasiet samt fram till mottagningsköket inom äldreomsorgen respektive externa kunder. Inom ansvarsområdet finns även strategi och vidareutveckling gällande måltidsverksamheten.  Vi har idag ca 100 medarbetare totalt fördelat på 44 tillagnings- och mottagningskök i kommunen. Måltidsservice ledningsgrupp består förutom administratör av verksamhetschef och fyra områdeschefer. Vi söker nu köksbiträden som kommer att vara anställda på Måltidsservice och ha sin arbetsplats i något av våra 44 kök på där vi lagar och serverar frukost, lunch och mellanmål till varje dag.   ARBETSUPPGIFTER I dina huvudsakliga arbetsuppgifter ingår salladsberedning, vara behjälplig i varma köket, diska, varuplock, servering i buffé och städning. Registrering av matsvinn och dokumentation av kökets egenkontroll är också arbetsuppgifter som är en del av din vardag.  Som timvikarie inom Måltidsservice behöver du ha ett flexibelt arbetssätt då du kan ha olika arbetsplatser samma vecka och ibland arbetar du ensam i mottagningskök.  KVALIFIKATIONER Du behöver ha minst två års erfarenhet av arbete i kalla och / eller varma köket och goda kunskaper i kökets hygien- och egenkontrollrutiner. Goda kunskaper och kännedom om olika allergier är ett krav. Viktigt är att du talar, läser, skriver och förstår svenska språket för att alltid kunna garantera säkerheten i den mat vi serverar.  Du älskar att ge service och brinner för att barn och elever ska få god och näringsriktig mat under hela dagen. Du är en självgående lagspelare med ett flexibelt arbetssätt och sätter våra gäster och kvalitet i fokus.  Personliga egenskaper vi värderar högt är att du klarar ett högt arbetstempo, är strukturerad och organiserar och planerar ditt arbete väl. Du hittar snabbt lösningar på uppkomna problem och ser möjligheter i varje förändring.   ÖVRIGT Som anställd i Sigtuna kommun får du ta del av en rad förmåner, bl.a. blir du automatiskt medlem i Personalklubben där du kan ta delta i olika aktiviteter. Läs gärna mer om våra andra förmåner och hur det är att jobba hos oss i Sigtuna kommun! https://www.sigtuna.se/naringsliv-och-arbete/jobba-hos-oss.html  Vi undanber oss erbjudanden om annonserings- och rekryteringshjälp.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Köksbiträde** timvikarie Sigtuna kommun är ... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 2 | Köksbiträde **tim**vikarie Sigtuna kommun är en p... | x |  |  | 3 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 3 | Köksbiträde tim**vikarie** Sigtuna kommun är en plats me... | x |  |  | 7 | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| 4 | Köksbiträde timvikarie **Sigtuna** kommun är en plats med stor s... | x | x | 7 | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 5 | Köksbiträde timvikarie Sigtuna** kommun** är en plats med stor spännvid... | x |  |  | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 6 | ...en ända fram till matgästen i **förskola**, skola och gymnasiet samt fra... | x |  |  | 8 | [Förskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/6o2S_cvR_GwA) |
| 6 | ...en ända fram till matgästen i **förskola**, skola och gymnasiet samt fra... |  | x |  | 8 | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| 7 | ...am till mottagningsköket inom **äldreomsorgen** respektive externa kunder. In... | x | x | 13 | 13 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 8 | ...agnings- och mottagningskök i **kommunen**. Måltidsservice ledningsgrupp... | x |  |  | 8 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 9 | ... ledningsgrupp består förutom **administratör** av verksamhetschef och fyra o... | x | x | 13 | 13 | [Administratör/Administrativ assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s5pR_WNm_R8W) |
| 10 | ...ra områdeschefer. Vi söker nu **köksbiträden** som kommer att vara anställda... | x | x | 12 | 12 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 11 | ...sakliga arbetsuppgifter ingår **salladsberedning**, vara behjälplig i varma köke... | x | x | 16 | 16 | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| 12 | ...ara behjälplig i varma köket, **diska**, varuplock, servering i buffé... | x | x | 5 | 5 | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| 13 | ...jälplig i varma köket, diska, **varuplock**, servering i buffé och städni... | x |  |  | 9 | [Varuplockare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/FxzD_RWY_mUE) |
| 14 | ...uplock, servering i buffé och **städning**. Registrering av matsvinn och... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 14 | ...uplock, servering i buffé och **städning**. Registrering av matsvinn och... | x |  |  | 8 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 15 | ...och städning. Registrering av **matsvinn** och dokumentation av kökets e... | x |  |  | 8 | [System för övervakning av matsvinn, **skill**](http://data.jobtechdev.se/taxonomy/concept/MuoH_pDL_nFJ) |
| 16 | ... Registrering av matsvinn och **dokumentation** av kökets egenkontroll är ock... | x | x | 13 | 13 | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| 17 | ...är en del av din vardag.  Som **tim**vikarie inom Måltidsservice be... | x |  |  | 3 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 18 | ...en del av din vardag.  Som tim**vikarie** inom Måltidsservice behöver d... | x |  |  | 7 | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| 19 | ...IKATIONER Du behöver ha minst **två års erfarenhet** av arbete i kalla och / eller... | x |  |  | 18 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 20 | ...vå års erfarenhet av arbete i **kalla** och / eller varma köket och g... | x |  |  | 5 | [Kallkök, **skill**](http://data.jobtechdev.se/taxonomy/concept/HtgZ_Ukp_YEX) |
| 21 | ...av arbete i kalla och / eller **varma köket** och goda kunskaper i kökets h... | x |  |  | 11 | [Varmkök, **skill**](http://data.jobtechdev.se/taxonomy/concept/7kNn_jPk_Cm3) |
| 22 | ...t och goda kunskaper i kökets **hygien- och e**genkontrollrutiner. Goda kunsk... |  | x |  | 13 | [tillämpa hygienrutiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/17hG_JyL_3PT) |
| 22 | ...t och goda kunskaper i kökets **hygien- och e**genkontrollrutiner. Goda kunsk... |  | x |  | 13 | [följa hygienrutiner vid fiske, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DUL9_kBT_jXk) |
| 22 | ...t och goda kunskaper i kökets **hygien- och e**genkontrollrutiner. Goda kunsk... |  | x |  | 13 | [ansvara för hygienrutiner i jordbrukssammanhang, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PdRZ_cjG_m3k) |
| 22 | ...t och goda kunskaper i kökets **hygien- och e**genkontrollrutiner. Goda kunsk... |  | x |  | 13 | [följa arbetsplatsens hygienrutiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hTiv_DE1_68M) |
| 23 | ...r, läser, skriver och förstår **svenska** språket för att alltid kunna ... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 24 | ...r, läser, skriver och förstår **svenska språket** för att alltid kunna garanter... | x |  |  | 15 | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
| 25 | ...ing.   ÖVRIGT Som anställd i **Sigtuna** kommun får du ta del av en ra... | x | x | 7 | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 26 | ... ÖVRIGT Som anställd i Sigtuna** kommun** får du ta del av en rad förmå... | x |  |  | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 27 | ...ur det är att jobba hos oss i **Sigtuna** kommun! https://www.sigtuna.s... | x | x | 7 | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| 28 | ...är att jobba hos oss i Sigtuna** kommun**! https://www.sigtuna.se/narin... | x |  |  | 7 | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| | **Overall** | | | **104** | **310** | 104/310 = **34%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [tillämpa hygienrutiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/17hG_JyL_3PT) |
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x |  |  | [Förskola, **keyword**](http://data.jobtechdev.se/taxonomy/concept/6o2S_cvR_GwA) |
| x |  |  | [Varmkök, **skill**](http://data.jobtechdev.se/taxonomy/concept/7kNn_jPk_Cm3) |
| x | x | x | [Sigtuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/8ryy_X54_xJj) |
| x |  |  | [Svenska språket, **keyword**](http://data.jobtechdev.se/taxonomy/concept/BiGm_58D_SSn) |
|  | x |  | [följa hygienrutiner vid fiske, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DUL9_kBT_jXk) |
| x |  |  | [Varuplockare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/FxzD_RWY_mUE) |
| x | x | x | [diska, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/Hkh7_SuX_4k1) |
| x |  |  | [Kallkök, **skill**](http://data.jobtechdev.se/taxonomy/concept/HtgZ_Ukp_YEX) |
| x |  |  | [System för övervakning av matsvinn, **skill**](http://data.jobtechdev.se/taxonomy/concept/MuoH_pDL_nFJ) |
|  | x |  | [ansvara för hygienrutiner i jordbrukssammanhang, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PdRZ_cjG_m3k) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| x | x | x | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| x |  |  | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
|  | x |  | [följa arbetsplatsens hygienrutiner, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/hTiv_DE1_68M) |
| x | x | x | [Dokumentation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/jodz_kWf_iKx) |
| x |  |  | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| x | x | x | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
|  | x |  | [Förskola, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/p3Nu_zzc_L7T) |
| x | x | x | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| x | x | x | [Administratör/Administrativ assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/s5pR_WNm_R8W) |
|  | x |  | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/24 = **29%** |