# Results for '0a6331f8e9b00200bf4f4adcb3744b22822549ad'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0a6331f8e9b00200bf4f4adcb3744b22822549ad](README.md) | 1 | 996 | 9 | 8 | 76/129 = **59%** | 5/9 = **56%** |

## Source text

Händig fastighetsskötare i Kristinehamn -gärna med erfarenhet av bygg Reflex Fastighetsförvaltning AB förvaltar i dagsläget omkring 300 hyreslägenheter i Kristinehamn. Företaget ansvarar för all skötsel och underhåll av fastigheterna. Vi strävar efter att ge våra kunder flexibel service till högsta kvalitet. Vi söker nu ytterligare medarbetare till våra team. Arbetsuppgifterna kommer att bestå av all sorts fastighetsskötsel, inre och yttre, men även en hel del renovering och byggnationer. Erfarenhet av bygg, måleri, golvläggning etc. är ett plus. Som person är du driven, handlingskraftig och engagerad. Du har höga ambitioner och är villig att lära dig nya saker. Vidare är du flexibel och prestigelös och fungerar lika bra i grupp som självständigt med en god känsla för ansvar och initiativtagande. Du ska kunna jobba med kort varsel. Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Händig **fastighetsskötare** i Kristinehamn -gärna med erf... | x | x | 17 | 17 | [Fastighetsskötare/Fastighetsarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/C6Cv_9ZR_7Gb) |
| 2 | Händig fastighetsskötare i **Kristinehamn** -gärna med erfarenhet av bygg... | x | x | 12 | 12 | [Kristinehamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SVQS_uwJ_m2B) |
| 3 | ...omkring 300 hyreslägenheter i **Kristinehamn**. Företaget ansvarar för all s... | x | x | 12 | 12 | [Kristinehamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SVQS_uwJ_m2B) |
| 4 | ...kommer att bestå av all sorts **fastighetsskötsel**, inre och yttre, men även en ... | x | x | 17 | 17 | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| 4 | ...kommer att bestå av all sorts **fastighetsskötsel**, inre och yttre, men även en ... |  | x |  | 17 | [VVS, fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/jXSx_Bm8_bNX) |
| 4 | ...kommer att bestå av all sorts **fastighetsskötsel**, inre och yttre, men även en ... |  | x |  | 17 | [Fastighetsskötsel, teknisk utbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/whKP_y3y_2ry) |
| 5 | ...nationer. Erfarenhet av bygg, **måleri**, golvläggning etc. är ett plu... | x | x | 6 | 6 | [Måleri, **skill**](http://data.jobtechdev.se/taxonomy/concept/EfCw_Dhn_vMP) |
| 6 | .... Erfarenhet av bygg, måleri, **golvläggning** etc. är ett plus. Som person ... | x | x | 12 | 12 | [Golvläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/BHXE_FVm_nNT) |
| 7 | ...fungerar lika bra i grupp som **självständigt** med en god känsla för ansvar ... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 8 | ...tändigt med en god känsla för **ansvar** och initiativtagande. Du ska ... | x |  |  | 6 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| | **Overall** | | | **76** | **129** | 76/129 = **59%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Golvläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/BHXE_FVm_nNT) |
| x | x | x | [Fastighetsskötare/Fastighetsarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/C6Cv_9ZR_7Gb) |
| x | x | x | [Måleri, **skill**](http://data.jobtechdev.se/taxonomy/concept/EfCw_Dhn_vMP) |
| x | x | x | [Fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/J5Xc_doF_TUR) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Kristinehamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SVQS_uwJ_m2B) |
| x |  |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [VVS, fastighetsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/jXSx_Bm8_bNX) |
|  | x |  | [Fastighetsskötsel, teknisk utbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/whKP_y3y_2ry) |
| | | **5** | 5/9 = **56%** |