# Results for '35d0de2127d00f201a8907125760eaf1a4b60865'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [35d0de2127d00f201a8907125760eaf1a4b60865](README.md) | 1 | 3752 | 24 | 10 | 77/267 = **29%** | 7/21 = **33%** |

## Source text

Personlig assistent till trädgårdsintresserad kvinna i Kungälv! Vill du jobba på ett företag där du varje dag bidrar till att ge andra människor ett bra liv och där du erbjuds ett viktigt och spännande jobb? Välkommen till Humana.  Om tjänsten: Humana söker nu en personlig assistent till vår kund i Kungälv. Kunden är en kvinna i 70-årsåldern som har en led/muskelsjukdom och en whiplashskada. Hon behöver hjälp med olika sysslor för att vardagen ska fungera.  Du kommer att hjälpa henne i det dagliga livet med arbetsuppgifter som hushållssysslor, hjälp vid förflyttningar samt att fungera som stöd vid övriga dagliga aktiviteter. Vår kund vill gärna komma ut och hitta på saker och andra intressen är sömnad, matlagning, shopping, trädgårdsarbete.  Vi söker dig som: Vi tror att du om söker är en kvinna som delar dessa intressen och har båda fötterna på jorden och hjärtat på rätt ställe. Vidare är du rökfri och inte allergisk mot hundar då fyrbenta vänner ibland kommer på besök. Du är mogen, plikttrogen och ute efter ett långsiktigt samarbete.  Har du erfarenhet av personlig assistans eller annat arbete inom vårdyrket är det ett plus, men avgörande är personkemi med kunden.  Krav: Körkort Ej rädd/allergisk mot hund Rökfri  Vi erbjuder: Ett spännande och varierat arbete med goda anställningsvillkor, som präglas av ett stort engagemang för våra arbetsuppgifter. Varje framgång för våra kunder är en framgång för oss. Vi är aldrig färdiga utan alltid på väg att bli bättre.  Arbetstid/Varaktighet: Tjänstens omfattning är ca 60%. Passen är förlagda tre dagar i veckan 9:30-17.30 (måndag onsdag och torsdag). Du arbetar inga helger eller storhelger. Vår kund reser ca 7–8 veckor per år då arbetar man inte. Då sägs avtalet upp och tar vid då kunden är åter ifrån sin resa. Vi söker även dig som vill vara timvikarie och bli inringd då ordinarie assistent är sjuk eller vid planerad ledighet.  Lön:Fast timlön enligt överenskommelse. Övriga ersättningar enligt kollektivavtal  Tillträde: När vi har hittat rätt kandidat för vår kund. Så vänta inte med din ansökan då tjänsten kan bli tillsatt innan sista ansökningsdag.  En förutsättning för att påbörja en anställning hos oss på Humana är att du som söker kan uppvisa giltig ID-handling och eventuella dokument som styrker rätten till arbete inom Sverige.  Att jobba på Humana Humana är ett ledande nordiskt omsorgsföretag som erbjuder tjänster inom personlig assistans, individ- och familjeomsorg, äldreomsorg och bostäder med särskild service enligt LSS, Lagen om stöd och service för vissa funktionshindrade. Humana har 16 000 engagerade medarbetare i Sverige, Norge, Danmark och Finland som utför omsorgstjänster till över 9 000 människor.  Hos Humana erbjuds du ett meningsfullt jobb med trygga anställningar. Vi har en prestigelös arbetsmiljö där vi uppmuntrar varandra till delaktighet och att sprida glädje på jobbet. Här drivs vi av utveckling och arbetar ständigt för att förbättra kvaliteten i vår verksamhet, där du som medarbetare är en viktig del av detta arbete. Våra värdegrundsord - Engagemang, glädje och ansvar är ledande i vårt dagliga arbete. I rekryteringsprocessen innebär det att vi värdesätter egenskaper som dessa hos dig.  Läs mer om hur det är att jobba som personlig assistent hos oss på Humana HÄR   Vad händer nu? Vi ser gärna att du ansöker snarast då vi går igenom urvalet löpande och kan komma att tillsätta tjänsten innan sista ansökningsdag.  Din ansökan läses av personal på Humana, av kunden och dennes anhöriga. För anställning hos Humana Assistans krävs att du har ett BankID och godkänner att använda ditt BankID i tjänsten.   Vill du veta mer om tjänsten eller rekryteringsprocessen är du varmt välkommen att kontakta oss!  Välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Personlig assistent** till trädgårdsintresserad kvi... | x |  |  | 19 | [personlig assistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qnSi_h1g_ENs) |
| 2 | ...trädgårdsintresserad kvinna i **Kungälv**! Vill du jobba på ett företag... | x | x | 7 | 7 | [Kungälv, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ZkZf_HbK_Mcr) |
| 3 | ... tjänsten: Humana söker nu en **personlig assistent** till vår kund i Kungälv. Kund... | x |  |  | 19 | [personlig assistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qnSi_h1g_ENs) |
| 4 | ...lig assistent till vår kund i **Kungälv**. Kunden är en kvinna i 70-års... | x | x | 7 | 7 | [Kungälv, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ZkZf_HbK_Mcr) |
| 5 | ... saker och andra intressen är **sömnad**, matlagning, shopping, trädgå... | x | x | 6 | 6 | [Sömnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/2H6W_q67_ygJ) |
| 6 | ...ch andra intressen är sömnad, **matlagning**, shopping, trädgårdsarbete.  ... | x |  |  | 10 | [leda matlagningen, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xUVF_gHj_me2) |
| 7 | ...sömnad, matlagning, shopping, **trädgårdsarbete**.  Vi söker dig som: Vi tror a... |  | x |  | 15 | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| 7 | ...sömnad, matlagning, shopping, **trädgårdsarbete**.  Vi söker dig som: Vi tror a... | x |  |  | 15 | [Trädgårdsanläggare m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/XBh3_Xrm_C4R) |
| 8 | ...arbete.  Har du erfarenhet av **personlig assistans** eller annat arbete inom vårdy... | x |  |  | 19 | [Personliga assistenter, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/sq3e_WVv_Fjd) |
| 9 | ...personkemi med kunden.  Krav: **Körkort** Ej rädd/allergisk mot hund Rö... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 10 | ...Körkort Ej rädd/allergisk mot **hund** Rökfri  Vi erbjuder: Ett spän... | x |  |  | 4 | [Hund, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YVnr_qQp_tH2) |
| 11 | ...t: Tjänstens omfattning är ca **60%**. Passen är förlagda tre dagar... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 12 | ... söker även dig som vill vara **timvikarie** och bli inringd då ordinarie ... | x |  |  | 10 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 13 | ...r vid planerad ledighet.  Lön:**Fast timlön** enligt överenskommelse. Övrig... | x |  |  | 11 | [Fast månads- vecko- eller timlön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/oG8G_9cW_nRf) |
| 14 | ...e. Övriga ersättningar enligt **kollektivavtal**  Tillträde: När vi har hittat... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 15 | ...yrker rätten till arbete inom **Sverige**.  Att jobba på Humana Humana ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 16 | ...ag som erbjuder tjänster inom **personlig assistans**, individ- och familjeomsorg, ... | x |  |  | 19 | [Personlig assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eU1q_zvL_9Rf) |
| 17 | ..., individ- och familjeomsorg, **äldreomsorg** och bostäder med särskild ser... | x | x | 11 | 11 | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| 18 | ... 000 engagerade medarbetare i **Sverige**, Norge, Danmark och Finland s... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 19 | ...gerade medarbetare i Sverige, **Norge**, Danmark och Finland som utfö... | x |  |  | 5 | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
| 20 | ...medarbetare i Sverige, Norge, **Danmark** och Finland som utför omsorgs... | x |  |  | 7 | [Danmark, **country**](http://data.jobtechdev.se/taxonomy/concept/Co3h_1xq_Cwb) |
| 21 | ...i Sverige, Norge, Danmark och **Finland** som utför omsorgstjänster til... | x |  |  | 7 | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
| 22 | ...ningar. Vi har en prestigelös **arbetsmiljö** där vi uppmuntrar varandra ti... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 23 | ...r om hur det är att jobba som **personlig assistent** hos oss på Humana HÄR   Vad h... | x |  |  | 19 | [personlig assistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qnSi_h1g_ENs) |
| 24 | ...ngsdag.  Din ansökan läses av **personal** på Humana, av kunden och denn... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| | **Overall** | | | **77** | **267** | 77/267 = **29%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Sömnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/2H6W_q67_ygJ) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
|  | x |  | [Trädgårdsskötsel, **skill**](http://data.jobtechdev.se/taxonomy/concept/AAaS_o41_sEg) |
| x |  |  | [Danmark, **country**](http://data.jobtechdev.se/taxonomy/concept/Co3h_1xq_Cwb) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x |  |  | [Norge, **country**](http://data.jobtechdev.se/taxonomy/concept/QJgN_Zge_BzJ) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Finland, **country**](http://data.jobtechdev.se/taxonomy/concept/WgbG_whJ_E7J) |
| x |  |  | [Trädgårdsanläggare m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/XBh3_Xrm_C4R) |
| x |  |  | [Hund, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YVnr_qQp_tH2) |
| x | x | x | [Äldreomsorg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Yt2Z_1pY_MNz) |
| x | x | x | [Kungälv, **municipality**](http://data.jobtechdev.se/taxonomy/concept/ZkZf_HbK_Mcr) |
| x |  |  | [Personlig assistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/eU1q_zvL_9Rf) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| x |  |  | [Fast månads- vecko- eller timlön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/oG8G_9cW_nRf) |
| x |  |  | [personlig assistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qnSi_h1g_ENs) |
| x |  |  | [Personliga assistenter, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/sq3e_WVv_Fjd) |
| x |  |  | [leda matlagningen, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xUVF_gHj_me2) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **7** | 7/21 = **33%** |