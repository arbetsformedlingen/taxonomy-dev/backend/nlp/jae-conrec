# Results for '31ba6c0cc9f69c9de589da8aaaecdfd4bcf506ee'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [31ba6c0cc9f69c9de589da8aaaecdfd4bcf506ee](README.md) | 1 | 4678 | 21 | 16 | 120/273 = **44%** | 11/21 = **52%** |

## Source text

Arkivarie Länsstyrelsen är Sveriges mest mångsidiga myndighet med viktiga uppgifter inom beredskap, förvaltning och tillsyn. Vi har bred kunskap inom frågor som rör miljö, natur, samhällsutveckling, landsbygd, krisberedskap och många andra samhällsnyttiga områden.  Vi ser till att riksdagens och regeringens beslut genomförs och samordnar länets intressen för en hållbar utveckling i länet. Vi har en viktig uppgift att väga samman perspektiv och tillföra kunskap för att fatta de bästa besluten för Gävleborgs människor, djur och natur.   På Länsstyrelsen Gävleborg arbetar cirka 250 personer, där de flesta är placerade i Gävle. Vi strävar efter att erbjuda en attraktiv arbetsplats som är fri från diskriminering och ger lika möjligheter för alla.  Länsstyrelsen är en HBTQI-certifierad arbetsplats. Handlingskraft, professionalitet och förståelse är ledorden i vår värdegrund.   I övrigt erbjuder vi en trivsam arbetsplats och har förmåner som friskvårdstimme, flextid med breda ramar, klämdagsledigt och möjlighet att arbeta delvis på distans.  Du kommer organisatoriskt att ingå i vårt centraldiarium på Enheten för diarium och juridik. Arbetsgruppen har ett teambaserat arbetssätt där samtliga ska kunna vissa grunduppdrag för att minska sårbarheten och det är viktigt att du trivs att vara en del av teamet och att du aktivt bidrar till teamets utveckling. Du behöver vara flexibel och ha lätt för att samarbeta. Visst arbete i reception förekommer.  Arbetsuppgifter Som arkivarie hos oss kommer du att arbeta tillsammans med arkivassistent och registratorer. Tillsammans med era olika kompetenser stödjer ni våra verksamheter med ärende- och dokumenthantering, tillhandahåller handlingar åt allmänheten samt förvarar och vårdar handlingar.  Arbetet innebär många kontakter, både internt inom länsstyrelsen och externt.  Du kommer även att delta i länsstyrelsernas nationella arbetsgrupper inom arkivfrågor.  Länsstyrelsen arbetar till största delen med en digital ärendehandläggning. I arbetet ingår även att förbereda och leverera äldre analogt material till Riksarkivet.  Du kommer att arbeta med planering, utredning och kartläggning, arbete med styrande och stödjande dokument, genomföra leveranser, ordna och förteckna, sekretessprövningar, gallring, ge råd och service samt utbildning i arkivfrågor.   Kvalifikationer Vi söker dig som:   • har goda kunskaper inom gällande regelverk och föreskrifter som gäller för uppdraget • har god samarbets- och kommunikationsförmåga • har god förmåga att planera och organisera ditt arbete • är van vid självständigt arbete men också i grupp   Du ska:   • ha högskoleexamen med lämplig ämneskombination där arkivvetenskap ingår med minst 60 hp eller annan kompetens som vi bedömer likvärdig • ha kännedom om de lagar och förordningar som styr offentlig verksamhet • ha goda kunskaper i svenska, både i tal och skrift  Det är meriterande om du har:    • erfarenhet av arbete som arkivarie inom länsstyrelsen eller inom annan offentlig förvaltning • kunskaper i MS Office • har erfarenhet från dokument- och ärendehanteringssystemet Platina samt förteckningssystemet Klara  Stor vikt kommer läggas vid de personliga egenskaperna  Vi vill att du:   • Är strukturerad och självgående med hög integritet • Har lätt för att samarbeta, i din roll behöver du samarbeta internt i gruppen samt skapa goda relationer med övriga på myndigheten • Kommunicerar på ett tydligt och pedagogiskt sätt • Är positiv och bidrar till ett bra arbetsklimat  Tjänsten är placerad i säkerhetsklass. Säkerhetsprövning med registerkontroll genomförs enligt säkerhetsskyddslagen (2018:585) innan beslut om anställning fattas. För denna tjänst krävs svenskt medborgarskap.  Med anställningen följer en skyldighet att krigsplaceras samt att tjänstgöra i vår krisledningsorganisation vid en händelse.  Anställningsvillkor Anställningsform: Tillsvidare, vi tillämpar 6 månaders provanställning Omfattning: Heltid Tillträde: Enligt överenskommelse Diarienummer: 4775-2022 Senaste ansökningsdag: 2022-08-07  Ansökan För att underlätta vår hantering av inkomna ansökningar samt för att säkerställa kvaliteten i vår urvalsprocess görs ansökan via rekryteringssystem Reachmee. Ansök genom att klicka på knappen Ansök längst ner på sidan. Det är av största vikt att du är noggrann när du fyller i din ansökan då den bedöms gentemot en fastställd kompetensprofil samt mot andra sökande.  Information Läs mer om Länsstyrelsen på vår hemsida www.lansstyrelsen.se/gavleborg    Inför rekryteringsarbetet har Länsstyrelsen tagit ställning till rekryteringskanaler och gjort vårt medieval. Vi undanber oss därför bestämt kontakt med mediesäljare och rekryteringssajter.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Arkivarie** Länsstyrelsen är Sveriges mes... | x | x | 9 | 9 | [Arkivarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E7nX_ux2_hF9) |
| 2 | Arkivarie Länsstyrelsen är **Sveriges** mest mångsidiga myndighet med... | x |  |  | 8 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 3 | ...rdnar länets intressen för en **hållbar utveckling** i länet. Vi har en viktig upp... | x |  |  | 18 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 4 | ...t fatta de bästa besluten för **Gävleborgs** människor, djur och natur.   ... | x |  |  | 10 | [Gävleborgs län, **region**](http://data.jobtechdev.se/taxonomy/concept/zupA_8Nt_xcD) |
| 5 | ...ten för Gävleborgs människor, **djur** och natur.   På Länsstyrelsen... | x | x | 4 | 4 | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| 6 | ...och natur.   På Länsstyrelsen **Gävle**borg arbetar cirka 250 persone... |  | x |  | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 7 | ...och natur.   På Länsstyrelsen **Gävleborg** arbetar cirka 250 personer, d... | x | x | 9 | 9 | [Gävleborgs län, **region**](http://data.jobtechdev.se/taxonomy/concept/zupA_8Nt_xcD) |
| 8 | ... där de flesta är placerade i **Gävle**. Vi strävar efter att erbjuda... | x |  |  | 5 | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| 9 | ...um på Enheten för diarium och **juridik**. Arbetsgruppen har ett teamba... | x | x | 7 | 7 | [Juridik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z9ZH_7XU_Kkb) |
| 10 | ...att samarbeta. Visst arbete i **reception** förekommer.  Arbetsuppgifter ... | x | x | 9 | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 11 | ...ekommer.  Arbetsuppgifter Som **arkivarie** hos oss kommer du att arbeta ... | x | x | 9 | 9 | [Arkivarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E7nX_ux2_hF9) |
| 12 | ...du att arbeta tillsammans med **arkivassistent** och registratorer. Tillsamman... | x |  |  | 14 | [Arkivassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/kxKW_LiD_FuW) |
| 13 | ...ammans med arkivassistent och **registratorer**. Tillsammans med era olika ko... |  | x |  | 13 | [registrator, djurpark, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qHJp_zeo_7Ju) |
| 13 | ...ammans med arkivassistent och **registratorer**. Tillsammans med era olika ko... | x | x | 13 | 13 | [Registrator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xf1u_Fjo_du4) |
| 14 | ...sera ditt arbete • är van vid **självständigt arbete** men också i grupp   Du ska:  ... | x |  |  | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ...ckså i grupp   Du ska:   • ha **högskoleexamen** med lämplig ämneskombination ... |  | x |  | 14 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 16 | ... lämplig ämneskombination där **arkivvetenskap** ingår med minst 60 hp eller a... | x | x | 14 | 14 | [Arkivvetenskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/qTjY_Xjh_6UA) |
| 17 | ...ksamhet • ha goda kunskaper i **svenska**, både i tal och skrift  Det ä... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 18 | ...   • erfarenhet av arbete som **arkivarie** inom länsstyrelsen eller inom... | x | x | 9 | 9 | [Arkivarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E7nX_ux2_hF9) |
| 19 | ...änsstyrelsen eller inom annan **offentlig förvaltning** • kunskaper i MS Office • har... | x | x | 21 | 21 | [Offentlig förvaltning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dBcK_fLJ_PEj) |
| 20 | ...lig förvaltning • kunskaper i **MS Office** • har erfarenhet från dokumen... | x | x | 9 | 9 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 21 | ...attas. För denna tjänst krävs **svenskt medborgarskap**.  Med anställningen följer en... | x |  |  | 21 | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| 22 | ...ingsvillkor Anställningsform: **Tillsvidare**, vi tillämpar 6 månaders prov... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 23 | ...s provanställning Omfattning: **Heltid** Tillträde: Enligt överenskomm... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 24 | ... är av största vikt att du är **noggrann** när du fyller i din ansökan d... |  | x |  | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| | **Overall** | | | **120** | **273** | 120/273 = **44%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Arkivarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E7nX_ux2_hF9) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| x | x | x | [Juridik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Z9ZH_7XU_Kkb) |
| x |  |  | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| x | x | x | [Offentlig förvaltning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/dBcK_fLJ_PEj) |
| x |  |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [Arkivassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/kxKW_LiD_FuW) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
|  | x |  | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
|  | x |  | [registrator, djurpark, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qHJp_zeo_7Ju) |
| x | x | x | [Arkivvetenskap, **keyword**](http://data.jobtechdev.se/taxonomy/concept/qTjY_Xjh_6UA) |
| x | x | x | [Gävle, **municipality**](http://data.jobtechdev.se/taxonomy/concept/qk8Y_2b6_82D) |
| x | x | x | [Djur, **keyword**](http://data.jobtechdev.se/taxonomy/concept/tcCL_d5A_iF4) |
| x | x | x | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| x | x | x | [Registrator, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/xf1u_Fjo_du4) |
| x | x | x | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [Gävleborgs län, **region**](http://data.jobtechdev.se/taxonomy/concept/zupA_8Nt_xcD) |
| | | **11** | 11/21 = **52%** |