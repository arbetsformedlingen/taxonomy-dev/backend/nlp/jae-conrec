# Results for 'da78ae007473eef666443f27c8eaf087fdadf2d7'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [da78ae007473eef666443f27c8eaf087fdadf2d7](README.md) | 1 | 3575 | 14 | 17 | 21/268 = **8%** | 4/18 = **22%** |

## Source text

Senior Growth Analyst VÅRT ERBJUDANDE  I rollen som Senior Growth Analyst hos Jobandtalent ansvarar du för revenue rapporteringen samt diverse ad hoc dataanalyser för att skapa underlag på tillväxtmöjligheter på den svenska marknaden. Det innebär att du kommer driva kritiska projekt med direkta rekommendationer till ledningsgruppen i Sverige. Du blir vår Growth representant i ett nära samarbete med vår centrala Revenue-Operations team, vilket ger dig en unik möjlighet att direkt påverka den globala kommersiella ledningen för ett stort och växande bolag. Din placeringsort är remote eller på ett av våra kontor i Sverige.  ARBETSBESKRIVNING  I den här positionen är du Sveriges growth expert och blir därmed första kontakten för våra globala stakeholders kring frågor som rör våra intäktstrender, performance och försäljningsstrategier. Som Senior Growth Analyst förväntas du skapa och utveckla nästa generation av rapporter, instrumentpaneler och analytiska verktyg för att bedöma resultaten (i CRM system) och tillförse hela organisationen med rätt förutsättningar för vår kommande tillväxt. Det är en väldigt varierande roll där du kommer att arbeta brett inom hela organisationen.  DIN PROFIL  Du är en högst analytisk person som älskar att grotta ned dig i data och insights för att besvara komplexa affärsfrågor. Vidare är du extremt detaljorienterad, vilket innebär att du upptäcker och åtgärdar inkonsekvenser eller misstag i data vid tidigt skede. Du drivs av utmaningar och kan effektivt förmedla insikter till seniora nyckelpersoner i ledningsgruppen globalt.  Värderingarna är en viktig del av vår företagskultur och länkar samman alla medarbetare, på varje kontor, hos varje kund, i hela Sverige, i hela världen. Du bör dela och leda efter våra värderingar; Extreme Ownership, High standards, Data Driven, Hands-on, Communication, Bootstrapper, Innovation.  DINA KVALIFIKATIONER  Relevant akademisk utbildning inom dataanalyser, ekonomi eller annan arbetslivserfarenhet relaterade till säljprocesser och tolkning av finansiell information. Tidigare erfarenhet i datadrivna roller med visualiseringssystem (Tableau, PowerBI, etc.) är bra att ha. Kunskap inom SQL och familjaritet med Salesforce är önskvärt. God kommunikation och att uttrycka dig väl i tal och skrift, svenska såväl som engelska.  OM OSS  Jobandtalent vill förbättra möjligheten för miljontals medarbetare på arbetsmarknaden. Vi fokuserar på teknologiska lösningar och användning av data som nyckeln till att förbättra arbetsmarknaden.  Vi har huvudkontoret i Madrid, där Jobandtalent grundades 2009. Jobandtalent värderas för närvarande till 2,4 miljarder dollar och stöds av ledande investerare inklusive SoftBank, Kinnevik, Atomico och BlackRock. Med en tillväxt på 130 % och 1 miljard dollar i intäkter förra året är Jobandtalent den 10:e snabbast växande startupen i Europa 2021.  I Sverige har vi en aggressiv tillväxtplan både via förvärv och fortsatt organisk tillväxt.   ANSÖKAN  Jobandtalent strävar efter att ha en rättvis och inkluderande rekryteringsprocess. Vi är engagerade i att skapa en mångfaldig och inkluderande miljö och vi söker aktivt efter kvalificerade kandidater oavsett ras, kön, könsidentitet, sexuell läggning, etnicitet, religion, nationellt ursprung, funktionshinder eller ålder. Därför ber vi dig vänligt att inte bifoga ett personligt brev i din ansökan då de ofta innehåller information som lätt kan utlösa oavsiktliga fördomar.  Sista ansökningsdagen 31-08-2022, intervjuer sker löpande och tjänsten kan kommas att tillsättas innan sista ansökningsdatumet.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...e rapporteringen samt diverse **ad** hoc dataanalyser för att skap... |  | x |  | 2 | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| 2 | ...rteringen samt diverse ad hoc **dataanalyser** för att skapa underlag på til... |  | x |  | 12 | [Dataanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/5Gmv_EZs_BPe) |
| 3 | ...på tillväxtmöjligheter på den **svenska** marknaden. Det innebär att du... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 4 | ...tioner till ledningsgruppen i **Sverige**. Du blir vår Growth represent... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 5 | ...eller på ett av våra kontor i **Sverige**.  ARBETSBESKRIVNING  I den hä... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 6 | ...täktstrender, performance och **försäljningsstrategier**. Som Senior Growth Analyst fö... |  | x |  | 22 | [försäljningsstrategier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7YE7_q6g_ADx) |
| 6 | ...täktstrender, performance och **försäljningsstrategier**. Som Senior Growth Analyst fö... |  | x |  | 22 | [ta fram försäljningsstrategier för bilhall, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/85HG_Xu5_E9h) |
| 6 | ...täktstrender, performance och **försäljningsstrategier**. Som Senior Growth Analyst fö... |  | x |  | 22 | [implementera försäljningsstrategier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dsGz_xVn_e2T) |
| 7 | ... för att bedöma resultaten (i **CRM** system) och tillförse hela or... | x | x | 3 | 3 | [Customer Relationship Management/CRM, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQre_aBF_3dE) |
| 8 | ....  DIN PROFIL  Du är en högst **analytisk** person som älskar att grotta ... | x |  |  | 9 | [arbeta analytiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wLyJ_Zpf_tgC) |
| 9 | ...sfrågor. Vidare är du extremt **detaljorienterad**, vilket innebär att du upptäc... | x |  |  | 16 | [se till detaljerna, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fAnK_cRC_byZ) |
| 10 | ...ontor, hos varje kund, i hela **Sverige**, i hela världen. Du bör dela ... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 11 | ...ant akademisk utbildning inom **dataanalyser**, ekonomi eller annan arbetsli... |  | x |  | 12 | [Dataanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/5Gmv_EZs_BPe) |
| 11 | ...ant akademisk utbildning inom **dataanalyser**, ekonomi eller annan arbetsli... | x |  |  | 12 | [Datavetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/8XpF_GHr_9KW) |
| 12 | ...utbildning inom dataanalyser, **ekonomi** eller annan arbetslivserfaren... |  | x |  | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| 12 | ...utbildning inom dataanalyser, **ekonomi** eller annan arbetslivserfaren... | x |  |  | 7 | [Annan bred utbildning i företagsekonomi, handel och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/aVfH_nz2_E5C) |
| 13 | ...terade till säljprocesser och **tolkning av finansiell information**. Tidigare erfarenhet i datadr... | x |  |  | 34 | [Finansiella analyser, **skill**](http://data.jobtechdev.se/taxonomy/concept/CcpL_8iG_q46) |
| 14 | ...) är bra att ha. Kunskap inom **SQL** och familjaritet med Salesfor... | x | x | 3 | 3 | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| 15 | ...inom SQL och familjaritet med **Salesforce** är önskvärt. God kommunikatio... | x |  |  | 10 | [Customer Relationship Management/CRM, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQre_aBF_3dE) |
| 16 | ...t med Salesforce är önskvärt. **God kommunikation** och att uttrycka dig väl i ta... | x |  |  | 17 | [Kommunikationsförmåga - Dataanalytiker, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/2dWL_HJx_H9y) |
| 17 | ...cka dig väl i tal och skrift, **svenska** såväl som engelska.  OM OSS  ... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 18 | ...och skrift, svenska såväl som **engelska**.  OM OSS  Jobandtalent vill f... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 19 | ...e startupen i Europa 2021.  I **Sverige** har vi en aggressiv tillväxtp... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 20 | ... sexuell läggning, etnicitet, **religion**, nationellt ursprung, funktio... |  | x |  | 8 | [Religion, **keyword**](http://data.jobtechdev.se/taxonomy/concept/pLEo_8vZ_jG5) |
| | **Overall** | | | **21** | **268** | 21/268 = **8%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Kommunikationsförmåga - Dataanalytiker, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/2dWL_HJx_H9y) |
|  | x |  | [Dataanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/5Gmv_EZs_BPe) |
|  | x |  | [försäljningsstrategier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7YE7_q6g_ADx) |
|  | x |  | [ta fram försäljningsstrategier för bilhall, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/85HG_Xu5_E9h) |
| x |  |  | [Datavetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/8XpF_GHr_9KW) |
| x |  |  | [Finansiella analyser, **skill**](http://data.jobtechdev.se/taxonomy/concept/CcpL_8iG_q46) |
|  | x |  | [Art Director/AD, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/E4KE_68y_6ag) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x | x | x | [SQL, frågespråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/Xnmr_kYS_YKY) |
| x |  |  | [Annan bred utbildning i företagsekonomi, handel och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/aVfH_nz2_E5C) |
| x | x | x | [Customer Relationship Management/CRM, **skill**](http://data.jobtechdev.se/taxonomy/concept/dQre_aBF_3dE) |
|  | x |  | [implementera försäljningsstrategier, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/dsGz_xVn_e2T) |
| x |  |  | [se till detaljerna, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fAnK_cRC_byZ) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Religion, **keyword**](http://data.jobtechdev.se/taxonomy/concept/pLEo_8vZ_jG5) |
| x |  |  | [arbeta analytiskt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wLyJ_Zpf_tgC) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **4** | 4/18 = **22%** |