# Results for '79a4b109abf66f3e61c45f6e2ea398c8929b86e8'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [79a4b109abf66f3e61c45f6e2ea398c8929b86e8](README.md) | 1 | 1057 | 7 | 13 | 42/277 = **15%** | 3/17 = **18%** |

## Source text

Här är utbildningen för dig som vill förbättra människors hälsa och livskvalitet genom  ett kvalificerat praktiskt arbete i framtidens hållbara näring inom svensk läke- och livsmedelsindustri.  Life science och biotechbranschen utvecklas ständigt och växer starkt i vår region. Efterfrågan på utbildade medarbetare inom teknikbranschen är stor idag och anställningsbehovet tros öka.  Utbildningen är på heltid och varvar lärarledd undervisning med laborationer, självstudier, grupparbeten, handledning, LIA, studiebesök och gästföreläsningar från arbetslivet.    Efter utbildningen kan du arbeta som läkemedelstekniker eller livsmedelstekniker i offentlig och privat regi och du har praktiska färdigheter och kompetenser inom läke-och livsmedelsrelaterad produktion.    Kurserna omfattar hela kedjan från idé till produkt med hygienkrav, processteknik, processkemi samt kännedom om relevant lagstiftning.  Du lär dig också ett kvalitetstänkande inom säkerhet, produktkvalitet och miljö samt praktiskt användning av olika säkerhets- och förbättringsverktyg. 

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...som vill förbättra människors **hälsa** och livskvalitet genom  ett k... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 2 | ... näring inom svensk läke- och **livsmedelsindustri**.  Life science och biotechbra... |  | x |  | 18 | [Livsmedelsindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/AP6y_xf5_4WK) |
| 2 | ... näring inom svensk läke- och **livsmedelsindustri**.  Life science och biotechbra... |  | x |  | 18 | [livsmedelsindustri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KoPq_DE2_Cj4) |
| 2 | ... näring inom svensk läke- och **livsmedelsindustri**.  Life science och biotechbra... |  | x |  | 18 | [Maskinoperatörer, livsmedelsindustri, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/fsXW_BQk_kni) |
| 2 | ... näring inom svensk läke- och **livsmedelsindustri**.  Life science och biotechbra... |  | x |  | 18 | [Maskinskötsel, livsmedelsindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/i3kd_fCX_jxz) |
| 2 | ... näring inom svensk läke- och **livsmedelsindustri**.  Life science och biotechbra... |  | x |  | 18 | [Övriga maskinoperatörer, livsmedelsindustri m.m., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/icae_Y69_H8E) |
| 2 | ... näring inom svensk läke- och **livsmedelsindustri**.  Life science och biotechbra... |  | x |  | 18 | [Maskinoperatörer, livsmedelsindustri m.m., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/pe1u_Nzp_E4z) |
| 2 | ... näring inom svensk läke- och **livsmedelsindustri**.  Life science och biotechbra... |  | x |  | 18 | [processoperatör, livsmedelsindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qYJv_89v_aBc) |
| 3 | ...ion. Efterfrågan på utbildade **medarbetare inom teknikbranschen** är stor idag och anställnings... | x |  |  | 32 | [Övriga ingenjörer och tekniker, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/mis4_feV_uex) |
| 4 | ...tros öka.  Utbildningen är på **heltid** och varvar lärarledd undervis... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 5 | ...tbildningen kan du arbeta som **läkemedelstekniker** eller livsmedelstekniker i of... | x |  |  | 18 | [Tekniker inom medicinteknik, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/UGsm_23D_Vmx) |
| 5 | ...tbildningen kan du arbeta som **läkemedelstekniker** eller livsmedelstekniker i of... |  | x |  | 18 | [Läkemedelstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/WGEx_rgb_Czn) |
| 6 | ... som läkemedelstekniker eller **livsmedelstekniker** i offentlig och privat regi o... | x | x | 18 | 18 | [Livsmedelstekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4ZyQ_56y_MwM) |
| 6 | ... som läkemedelstekniker eller **livsmedelstekniker** i offentlig och privat regi o... |  | x |  | 18 | [livsmedelstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/cJ44_bSi_cyh) |
| 7 | ... till produkt med hygienkrav, **processteknik**, processkemi samt kännedom om... | x | x | 13 | 13 | [Processteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Jgmc_3hD_JEt) |
| 8 | ...ed hygienkrav, processteknik, **processkemi** samt kännedom om relevant lag... | x | x | 11 | 11 | [Processkemi, **skill**](http://data.jobtechdev.se/taxonomy/concept/z6zn_oa6_E3d) |
| 9 | ...emi samt kännedom om relevant **lagstiftning**.  Du lär dig också ett kvalit... | x |  |  | 12 | [lagstiftning om hälsa, säkerhet och hygien, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cJPx_utG_TWG) |
| | **Overall** | | | **42** | **277** | 42/277 = **15%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Livsmedelstekniker, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4ZyQ_56y_MwM) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Livsmedelsindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/AP6y_xf5_4WK) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x | x | x | [Processteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Jgmc_3hD_JEt) |
|  | x |  | [livsmedelsindustri, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/KoPq_DE2_Cj4) |
| x |  |  | [Tekniker inom medicinteknik, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/UGsm_23D_Vmx) |
|  | x |  | [Läkemedelstekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/WGEx_rgb_Czn) |
|  | x |  | [livsmedelstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/cJ44_bSi_cyh) |
| x |  |  | [lagstiftning om hälsa, säkerhet och hygien, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cJPx_utG_TWG) |
|  | x |  | [Maskinoperatörer, livsmedelsindustri, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/fsXW_BQk_kni) |
|  | x |  | [Maskinskötsel, livsmedelsindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/i3kd_fCX_jxz) |
|  | x |  | [Övriga maskinoperatörer, livsmedelsindustri m.m., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/icae_Y69_H8E) |
| x |  |  | [Övriga ingenjörer och tekniker, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/mis4_feV_uex) |
|  | x |  | [Maskinoperatörer, livsmedelsindustri m.m., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/pe1u_Nzp_E4z) |
|  | x |  | [processoperatör, livsmedelsindustri, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qYJv_89v_aBc) |
| x | x | x | [Processkemi, **skill**](http://data.jobtechdev.se/taxonomy/concept/z6zn_oa6_E3d) |
| | | **3** | 3/17 = **18%** |