# Results for '871f5d07292ceb98cc822ed1c356bf1789caa081'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [871f5d07292ceb98cc822ed1c356bf1789caa081](README.md) | 1 | 373 | 3 | 3 | 18/32 = **56%** | 2/4 = **50%** |

## Source text

Diskare /Städare Sökes Är du en social och stresstålig person som gillar när det är mycket att göra. Du har lätt för att samarbeta och uppskattar ett skinande resultat. Har du tidigare erfarenhet är det meriterande, men framförallt ska du vara serviceminded och ha hög arbetsmoral.     Stämmer detta in på dig?   Välkommen med din ansökan snarast!   berk.bozlak@pitchers.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Diskare** /Städare Sökes Är du en socia... | x | x | 7 | 7 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| 2 | Diskare /**Städare** Sökes Är du en social och str... | x |  |  | 7 | [Städare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Z6TY_xDf_Yup) |
| 2 | Diskare /**Städare** Sökes Är du en social och str... |  | x |  | 7 | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| 3 | ...are Sökes Är du en social och **stresstålig** person som gillar när det är ... | x | x | 11 | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | **Overall** | | | **18** | **32** | 18/32 = **56%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Städare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/Z6TY_xDf_Yup) |
|  | x |  | [Städare/Lokalvårdare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/dYo1_D8c_87U) |
| x | x | x | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| x | x | x | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| | | **2** | 2/4 = **50%** |