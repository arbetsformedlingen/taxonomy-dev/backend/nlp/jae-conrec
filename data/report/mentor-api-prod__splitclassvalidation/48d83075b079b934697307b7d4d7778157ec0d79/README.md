# Results for '48d83075b079b934697307b7d4d7778157ec0d79'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [48d83075b079b934697307b7d4d7778157ec0d79](README.md) | 1 | 3492 | 33 | 55 | 241/675 = **36%** | 16/44 = **36%** |

## Source text

Biolog till Klinisk mikrobiologi och vårdhygien Arbetsuppgifter Vi söker en biolog som arbetskamrat på vår trivsamma klinik som, efter sin upplärning, kan ta hand om en del av vårt diagnostiska rutinarbete.  Arbetet inkluderar bedömning av bakterieväxt, bedömning av serologiska och molekylärbiologiska resultat samt kvalitetsarbete, utvecklingsarbete och kommunikation med vårdenheter.   Vår hjälpsamma arbetsgrupp består av två överläkare (en är samtidigt verksamhetschef), två specialistläkare (alla specialistkompetenta inom klinisk mikrobiologi, en specialistläkare är även specialistkompetent inom infektionsmedicin), en utbildningsläkare och två mikrobiologer. På kliniken jobbar dessutom 30 BMA, substrattekniker och sekreterare.   Om arbetsplatsen Förvaltningen Ambulans, diagnostik och hälsa är länsövergripande och omfattar cirka 1100 medarbetare. Utifrån Region Hallands vision ”Bästa livsplatsen” verkar Ambulans, diagnostik och hälsa för en god hälsa hos befolkningen. Våra medarbetare är nyckeln till att bedriva en verksamhet med hög kvalitet. Vår utgångspunkt är alltid patienten och den halländske medborgaren.  Klinisk mikrobiologi erbjuder ett brett analysutbud inom bakteriologi, virologi, mykologi och parasitologi och även inom immunologi. Våra analyser och provflöden har kontinuerligt utvecklats för att möta framtida behov.  Kvalifikationer Vi söker dig som har minst magisterexamen eller motsvarande inom biologi. Meriterande är om du har arbetat med mikrobiologisk diagnostik, framför allt bakterieodling/resistensbestämning och serologi samt om du har erfarenhet inom forskning/doktorsavhandling.  Inom Klinisk mikrobiologi och vårdhygien är det viktigt med ett fungerande nätverk. På vår klinik har ett nära samarbete med samtliga olika yrkeskategorier samt en tät kontakt med kollegor på andra klinker inom regionen och över regionens gränser. Därför värdesätter vi god samarbetsförmåga och kommunikativ förmåga. Arbetet ställer även stort krav på att du kan arbeta självständigt och att du tar egna initiativ. Vi ser gärna att du är strukturerad, serviceinriktad och flexibel när arbetet kräver det. Stor vikt läggs vid personlig lämplighet.  Varmt välkommen med din ansökan!  Vårt erbjudande För oss är det viktigt att du ska trivas och utvecklas i ditt arbete.  https://www.regionhalland.se/jobb-och-karriar/att-jobba-i-region-halland/formaner-for-dig-som-anstalld/  Om Region Halland   Region Halland är till för alla som bor och arbetar här i Halland. Vi är över 8 000 medarbetare inom 370 olika yrken som alla arbetar för att erbjuda en god hälso- och sjukvård och att främja tillväxt och utveckling. Vi ansvarar också för kollektivtrafiken i Halland och driver frågor inom områden som näringsliv, kultur och utbildning. Vår vision är ”Halland ­ – bästa livsplatsen” och med det menar vi att Halland ska bli den bästa platsen att bo på, utbilda sig, arbeta och driva företag i. Vi har kommit en bra bit på vägen, välkommen att följa med oss på vår resa! https://www.regionhalland.se/om-region-halland/  Strävan efter jämställdhet, mångfald och likvärdiga villkor är både en kvalitetsfråga och en självklar del av Region Hallands värdegrund.   Region Halland krigsplacerar all tillsvidareanställd personal.   Till bemannings- och rekryteringsföretag och till dig som är försäljare: Vi undanber oss vänligt men bestämt direktkontakt med bemannings- och rekryteringsföretag samt försäljare av ytterligare jobbannonser. Region Halland har upphandlade avtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Biolog** till Klinisk mikrobiologi och... |  | x |  | 6 | [Biologer, botanister, zoologer m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/DiUc_G8m_PKy) |
| 1 | **Biolog** till Klinisk mikrobiologi och... |  | x |  | 6 | [Biologer, farmakologer och specialister inom lant- och skogsbruk m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/FLdv_Nyd_Lic) |
| 1 | **Biolog** till Klinisk mikrobiologi och... |  | x |  | 6 | [biolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/GUfo_o3h_JnM) |
| 1 | **Biolog** till Klinisk mikrobiologi och... |  | x |  | 6 | [biolog, vattenbruk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/JeVU_Zna_ion) |
| 1 | **Biolog** till Klinisk mikrobiologi och... | x | x | 6 | 6 | [Biolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ZNyx_1tG_w4E) |
| 2 | Biolog till **Klinisk mikrobiologi** och vårdhygien Arbetsuppgifte... | x | x | 20 | 20 | [klinisk mikrobiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/j794_7DV_GAf) |
| 3 | ...n Arbetsuppgifter Vi söker en **biolog** som arbetskamrat på vår trivs... |  | x |  | 6 | [Biologer, botanister, zoologer m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/DiUc_G8m_PKy) |
| 3 | ...n Arbetsuppgifter Vi söker en **biolog** som arbetskamrat på vår trivs... |  | x |  | 6 | [Biologer, farmakologer och specialister inom lant- och skogsbruk m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/FLdv_Nyd_Lic) |
| 3 | ...n Arbetsuppgifter Vi söker en **biolog** som arbetskamrat på vår trivs... |  | x |  | 6 | [biolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/GUfo_o3h_JnM) |
| 3 | ...n Arbetsuppgifter Vi söker en **biolog** som arbetskamrat på vår trivs... |  | x |  | 6 | [biolog, vattenbruk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/JeVU_Zna_ion) |
| 3 | ...n Arbetsuppgifter Vi söker en **biolog** som arbetskamrat på vår trivs... | x | x | 6 | 6 | [Biolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ZNyx_1tG_w4E) |
| 4 | ... bedömning av serologiska och **molekylärbiologiska** resultat samt kvalitetsarbete... | x |  |  | 19 | [Molekylärbiologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/eD1A_sx6_r1V) |
| 5 | ...esultat samt kvalitetsarbete, **utvecklingsarbete** och kommunikation med vårdenh... | x | x | 17 | 17 | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| 6 | ...mma arbetsgrupp består av två **överläkare** (en är samtidigt verksamhetsc... |  | x |  | 10 | [Överläkare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/qSbw_XHD_Ycd) |
| 7 | ...mtidigt verksamhetschef), två **specialistläkare** (alla specialistkompetenta in... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 8 | ...lla specialistkompetenta inom **klinisk mikrobiologi**, en specialistläkare är även ... |  | x |  | 20 | [klinisk mikrobiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/j794_7DV_GAf) |
| 9 | ...ialistkompetenta inom klinisk **mikrobiologi**, en specialistläkare är även ... | x |  |  | 12 | [Mikrobiologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/6de1_7CM_XL6) |
| 10 | ...inom klinisk mikrobiologi, en **specialistläkare** är även specialistkompetent i... | x |  |  | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 11 | ...även specialistkompetent inom **infektionsmedicin**), en utbildningsläkare och tv... |  | x |  | 17 | [Läkarutbildning, infektionsmedicin, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WKfW_Xcr_5F3) |
| 12 | ... en utbildningsläkare och två **mikrobiologer**. På kliniken jobbar dessutom ... | x | x | 13 | 13 | [Mikrobiolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PiQ3_Wzy_U1X) |
| 12 | ... en utbildningsläkare och två **mikrobiologer**. På kliniken jobbar dessutom ... |  | x |  | 13 | [mikrobiolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/VgVA_zyh_wrP) |
| 12 | ... en utbildningsläkare och två **mikrobiologer**. På kliniken jobbar dessutom ... |  | x |  | 13 | [forskare, mikrobiolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/v8xu_35E_Fjw) |
| 13 | ...å kliniken jobbar dessutom 30 **BMA**, substrattekniker och sekrete... | x | x | 3 | 3 | [Biomedicinsk analytiker/BMA, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Pj8T_bST_f7D) |
| 14 | ... 30 BMA, substrattekniker och **sekreterare**.   Om arbetsplatsen Förvaltni... |  | x |  | 11 | [Medicinska sekreterare, vårdadministratörer m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/2kLc_pto_DpV) |
| 14 | ... 30 BMA, substrattekniker och **sekreterare**.   Om arbetsplatsen Förvaltni... | x |  |  | 11 | [Sekreterare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zAEZ_tNJ_8Gr) |
| 15 | ...m arbetsplatsen Förvaltningen **Ambulans**, diagnostik och hälsa är läns... | x |  |  | 8 | [Ambulans, **keyword**](http://data.jobtechdev.se/taxonomy/concept/VYLr_fEP_8Yh) |
| 16 | ...ngen Ambulans, diagnostik och **hälsa** är länsövergripande och omfat... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 17 | ...rka 1100 medarbetare. Utifrån **Region Halland**s vision ”Bästa livsplatsen” v... | x |  |  | 14 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 18 | ...on ”Bästa livsplatsen” verkar **Ambulans**, diagnostik och hälsa för en ... | x |  |  | 8 | [Ambulans, **keyword**](http://data.jobtechdev.se/taxonomy/concept/VYLr_fEP_8Yh) |
| 19 | ...rkar Ambulans, diagnostik och **hälsa** för en god hälsa hos befolkni... | x | x | 5 | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 20 | ...agnostik och hälsa för en god **hälsa** hos befolkningen. Våra medarb... | x |  |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 21 | ... den halländske medborgaren.  **Klinisk mikrobiologi** erbjuder ett brett analysutbu... | x | x | 20 | 20 | [klinisk mikrobiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/j794_7DV_GAf) |
| 22 | ...er ett brett analysutbud inom **bakteriologi**, virologi, mykologi och paras... | x | x | 12 | 12 | [Bakteriologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/5jZo_oN5_xFM) |
| 22 | ...er ett brett analysutbud inom **bakteriologi**, virologi, mykologi och paras... |  | x |  | 12 | [laboratorietekniker, bakteriologi, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/S7ny_Pu2_sFF) |
| 22 | ...er ett brett analysutbud inom **bakteriologi**, virologi, mykologi och paras... |  | x |  | 12 | [Forskare, bakteriologi, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ms96_qce_p79) |
| 23 | ...nalysutbud inom bakteriologi, **virologi**, mykologi och parasitologi oc... |  | x |  | 8 | [Forskare, virologi, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/5RkB_Zeq_aH6) |
| 23 | ...nalysutbud inom bakteriologi, **virologi**, mykologi och parasitologi oc... |  | x |  | 8 | [virologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/8xye_119_7CX) |
| 23 | ...nalysutbud inom bakteriologi, **virologi**, mykologi och parasitologi oc... | x | x | 8 | 8 | [Virologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/DcY9_nMk_QsT) |
| 24 | ... inom bakteriologi, virologi, **mykologi** och parasitologi och även ino... |  | x |  | 8 | [Mykologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/1xwv_8x9_T59) |
| 24 | ... inom bakteriologi, virologi, **mykologi** och parasitologi och även ino... | x | x | 8 | 8 | [mykologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MoZC_9cw_kPS) |
| 24 | ... inom bakteriologi, virologi, **mykologi** och parasitologi och även ino... |  | x |  | 8 | [medicinsk mykologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/j1NY_qLB_7px) |
| 25 | ...ologi, virologi, mykologi och **parasitologi** och även inom immunologi. Vår... | x | x | 12 | 12 | [parasitologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FXA5_SmR_hWD) |
| 25 | ...ologi, virologi, mykologi och **parasitologi** och även inom immunologi. Vår... |  | x |  | 12 | [medicinsk parasitologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HcpX_bqH_Ttb) |
| 26 | ...ch parasitologi och även inom **immunologi**. Våra analyser och provflöden... | x | x | 10 | 10 | [immunologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZFDz_fmH_yV7) |
| 27 | ...er Vi söker dig som har minst **magisterexamen** eller motsvarande inom biolog... |  | x |  | 14 | [Bibliotekarieutbildning samt biblioteks- och informationsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/FsKa_aFC_yPT) |
| 27 | ...er Vi söker dig som har minst **magisterexamen** eller motsvarande inom biolog... | x | x | 14 | 14 | [Högskoleutbildning, generell, 4 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/P5XD_6Qj_hEk) |
| 27 | ...er Vi söker dig som har minst **magisterexamen** eller motsvarande inom biolog... |  | x |  | 14 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 28 | ...examen eller motsvarande inom **biologi**. Meriterande är om du har arb... | x |  |  | 7 | [Biologi, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/64R6_Xoz_yPo) |
| 28 | ...examen eller motsvarande inom **biologi**. Meriterande är om du har arb... |  | x |  | 7 | [Biologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o6US_5HP_iDM) |
| 29 | ...ande är om du har arbetat med **mikrobiologisk** diagnostik, framför allt bakt... | x |  |  | 14 | [Mikrobiologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4uyB_i5y_sS5) |
| 30 | ...ning/doktorsavhandling.  Inom **Klinisk mikrobiologi** och vårdhygien är det viktigt... | x | x | 20 | 20 | [klinisk mikrobiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/j794_7DV_GAf) |
| 31 | ...kontakt med kollegor på andra **klinker** inom regionen och över region... |  | x |  | 7 | [lägga klinker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FgLx_yiP_Mc7) |
| 32 | ...även stort krav på att du kan **arbeta självständigt** och att du tar egna initiativ... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 33 | ...-dig-som-anstalld/  Om Region **Halland**   Region Halland är till för ... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 34 | .../  Om Region Halland   Region **Halland** är till för alla som bor och ... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 35 | ...rbetar för att erbjuda en god **hälso- oc**h sjukvård och att främja till... |  | x |  | 9 | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| 36 | ...rbetar för att erbjuda en god **hälso- och sjukvård** och att främja tillväxt och u... | x |  |  | 19 | [Hälso- och sjukvård, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/NYW6_mP6_vwf) |
| 37 | ...också för kollektivtrafiken i **Halland** och driver frågor inom område... | x | x | 7 | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 38 | ...ch utbildning. Vår vision är ”**Halland** ­ – bästa livsplatsen” och me... |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 39 | ...egion-halland/  Strävan efter **jämställdhet**, mångfald och likvärdiga vill... | x | x | 12 | 12 | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| 40 | ...fråga och en självklar del av **Region Hallands** värdegrund.   Region Halland ... | x |  |  | 15 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 41 | ...Region Hallands värdegrund.   **Region **Halland krigsplacerar all till... | x |  |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 42 | ...Hallands värdegrund.   Region **Halland** krigsplacerar all tillsvidare... | x | x | 7 | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| 43 | ...erligare jobbannonser. Region **Halland** har upphandlade avtal. |  | x |  | 7 | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| | **Overall** | | | **241** | **675** | 241/675 = **36%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Mykologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/1xwv_8x9_T59) |
|  | x |  | [Medicinska sekreterare, vårdadministratörer m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/2kLc_pto_DpV) |
| x |  |  | [Mikrobiologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/4uyB_i5y_sS5) |
|  | x |  | [Forskare, virologi, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/5RkB_Zeq_aH6) |
| x | x | x | [Bakteriologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/5jZo_oN5_xFM) |
| x |  |  | [Biologi, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/64R6_Xoz_yPo) |
| x |  |  | [Mikrobiologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/6de1_7CM_XL6) |
|  | x |  | [virologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/8xye_119_7CX) |
| x | x | x | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| x | x | x | [Virologi, **skill**](http://data.jobtechdev.se/taxonomy/concept/DcY9_nMk_QsT) |
|  | x |  | [Biologer, botanister, zoologer m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/DiUc_G8m_PKy) |
|  | x |  | [Biologer, farmakologer och specialister inom lant- och skogsbruk m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/FLdv_Nyd_Lic) |
| x | x | x | [parasitologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FXA5_SmR_hWD) |
|  | x |  | [lägga klinker, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/FgLx_yiP_Mc7) |
|  | x |  | [Bibliotekarieutbildning samt biblioteks- och informationsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/FsKa_aFC_yPT) |
|  | x |  | [biolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/GUfo_o3h_JnM) |
|  | x |  | [medicinsk parasitologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HcpX_bqH_Ttb) |
|  | x |  | [biolog, vattenbruk, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/JeVU_Zna_ion) |
|  | x |  | [Hälso- och sjukvård, **sni-level-2**](http://data.jobtechdev.se/taxonomy/concept/KhNL_Quq_Xjk) |
| x | x | x | [Utvecklingsarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/LsGo_Kh2_3kH) |
| x | x | x | [mykologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/MoZC_9cw_kPS) |
| x |  |  | [Hälso- och sjukvård, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/NYW6_mP6_vwf) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [Högskoleutbildning, generell, 4 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/P5XD_6Qj_hEk) |
| x | x | x | [Mikrobiolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PiQ3_Wzy_U1X) |
| x | x | x | [Biomedicinsk analytiker/BMA, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Pj8T_bST_f7D) |
|  | x |  | [laboratorietekniker, bakteriologi, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/S7ny_Pu2_sFF) |
| x |  |  | [Ambulans, **keyword**](http://data.jobtechdev.se/taxonomy/concept/VYLr_fEP_8Yh) |
|  | x |  | [mikrobiolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/VgVA_zyh_wrP) |
|  | x |  | [Läkarutbildning, infektionsmedicin, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WKfW_Xcr_5F3) |
| x | x | x | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| x | x | x | [immunologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ZFDz_fmH_yV7) |
| x | x | x | [Biolog, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ZNyx_1tG_w4E) |
| x |  |  | [Molekylärbiologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/eD1A_sx6_r1V) |
|  | x |  | [medicinsk mykologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/j1NY_qLB_7px) |
| x | x | x | [klinisk mikrobiologi, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/j794_7DV_GAf) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
|  | x |  | [Forskare, bakteriologi, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ms96_qce_p79) |
|  | x |  | [Biologi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o6US_5HP_iDM) |
| x | x | x | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
|  | x |  | [Överläkare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/qSbw_XHD_Ycd) |
|  | x |  | [forskare, mikrobiolog, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/v8xu_35E_Fjw) |
| x | x | x | [Hallands län, **region**](http://data.jobtechdev.se/taxonomy/concept/wjee_qH2_yb6) |
| x |  |  | [Sekreterare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zAEZ_tNJ_8Gr) |
| | | **16** | 16/44 = **36%** |