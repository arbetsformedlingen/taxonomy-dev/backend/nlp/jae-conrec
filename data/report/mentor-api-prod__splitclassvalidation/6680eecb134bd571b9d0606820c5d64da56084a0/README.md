# Results for '6680eecb134bd571b9d0606820c5d64da56084a0'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [6680eecb134bd571b9d0606820c5d64da56084a0](README.md) | 1 | 3515 | 14 | 6 | 43/183 = **23%** | 4/14 = **29%** |

## Source text

Serveringsstjärnor till roliga uppdrag Om tjänsten  Vi är nu på jakt efter serveringspersonal som brinner för att leverera service i världsklass. Uppdragen varierar, allt från stora events och middagar ute hos våra välrenommerade kunder till mer intima cateringuppdrag hos privatpersoner. Vi har en bred kundbas som innefattar allt ifrån konferensanläggningar, á la carterestauranger, kontorshotell och festvåningar. Med andra ord finns möjligheten till olika typer av serveringsuppdrag utifrån vad just du har för erfarenhet och mål.  Du kommer ha ett flexibelt schema där du har möjligheten att skräddarsy din vardag och kombinera jobb med t.ex. studier eller annan sidosysselsättning. Vi erbjuder dig härliga kollegor, marknadsmässig lön och en dynamisk vardag där inget uppdrag är det andra likt.  Har du ingen erfarenhet eller utbildning inom servering och catering? Ingen fara! Vi kommer att erbjuda dig en grundläggande utbildning för att du ska känna trygg i att hantera arbetsuppgifterna och leverera förstklassig service till våra kunder.  Arbetsuppgifter  Du kommer att vara en del av ett drivet team med passion för att leverera grym service. Dina arbetsuppgifter inkluderar att duka upp bord, välkomna gäster, servera mat och dryck, kassahantering, plocka disk och det viktigaste av allt; leverera service i världsklass.  Din profil  Du är glad, utåtriktad, har förmågan att ge utmärkt service och utför ditt arbete prestigelöst med ett glatt humör. Som Carottestjärna är du ansvarstagande, gillar att ta egna initiativ och har lätt för att anpassa dig till nya situationer, kan du vidare hantera ett högt arbetstempo och har en god samarbetsförmåga så är du definitivt rätt person. Vi förutsätter naturligtvis att du är professionell och personlig i ditt möte med gäster och kollegor. Våra värdeord är förebilder, kvalitet, personligt och nyskapande- precis så vill vi att våra kunder och deras gäster ska uppleva oss.  I den här rekryteringsprocessen letar vi efter stjärnor som har en annan huvudsaklig sysselsättning och vill jobba extra hos oss. Vi kommer lägga stor vikt på din personliga lämplighet och din motivation för rollen. För att trivas i arbetet tror vi att du är en riktig teamplayer, gillar att ha många bollar i luften och förstår vikten av att alltid leverera den bästa servicen till våra kunder.  Om Carotte Academy  I Carotte Academy utbildar vi personal inom besöks- och servicenäringen genom en kostnadsfri utbildning i flera steg. I introduktionsutbildningen får du som ny i branschen de teoretiska och praktiska grundkunskaper som krävs för att kunna arbeta med service hos Carotte Staff.  Om oss  Carotte Staff är bemanningsföretaget inom Carotte Group som hjälper våra kunder med allt från nyckelpositioner och andra tjänster på heltid till korta deltidsuppdrag. Vi skräddarsyr uppdragen efter våra kunders behov och drivs av att förstå varje kunds specifika behov på djupet. Carotte Staff är specialiserade på roller inom servicebranschen och besöksnäringen med fokus på personal inom hotell, konferens och event. Vårt moderbolag Carotte har över 40 års erfarenhet av att arbeta med service och vi har under åren blivit experter på att rekrytera och utbilda personal som alltid gör det lilla extra.  ÖVRIG INFORMATION  Start: Enligt överenskommelse  Omfattning: Extra vid behov, utöver din huvudsakliga sysselsättning  Lön: Timlön  Placering: Stockholm  Vi tar inte emot ansökningar via mejl men om du har några frågor eller funderingar kontakta gärna ansvarig rekryterare.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...nsten  Vi är nu på jakt efter **serveringspersonal** som brinner för att leverera ... | x |  |  | 18 | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| 1 | ...nsten  Vi är nu på jakt efter **serveringspersonal** som brinner för att leverera ... |  | x |  | 18 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | ... ifrån konferensanläggningar, **á la carte**restauranger, kontorshotell oc... | x |  |  | 10 | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| 3 | ...ferensanläggningar, á la carte**restauranger**, kontorshotell och festvåning... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 4 | ...arenhet eller utbildning inom **servering** och catering? Ingen fara! Vi ... | x |  |  | 9 | [Restaurang, servering, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GqXD_ArB_Mmg) |
| 5 | ...ka upp bord, välkomna gäster, **servera mat och dryck**, kassahantering, plocka disk ... |  | x |  | 21 | [servera mat och dryck, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/AaYq_MQh_c9h) |
| 6 | ...äster, servera mat och dryck, **kassahantering**, plocka disk och det viktigas... | x | x | 14 | 14 | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| 7 | ...at och dryck, kassahantering, **plocka disk** och det viktigaste av allt; l... | x |  |  | 11 | [Diskplockare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/pEsS_jVe_EMQ) |
| 8 | ...mör. Som Carottestjärna är du **ansvarstagande**, gillar att ta egna initiativ... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 9 | ...I Carotte Academy utbildar vi **personal** inom besöks- och servicenärin... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 10 | ...h besöksnäringen med fokus på **personal** inom hotell, konferens och ev... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 11 | ...en med fokus på personal inom **hotell**, konferens och event. Vårt mo... | x | x | 6 | 6 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 12 | ... på att rekrytera och utbilda **personal** som alltid gör det lilla extr... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 13 | ...dsakliga sysselsättning  Lön: **Timlön**  Placering: Stockholm  Vi tar... | x |  |  | 6 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 14 | ...ning  Lön: Timlön  Placering: **Stockholm**  Vi tar inte emot ansökningar... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 15 | ...ingar kontakta gärna ansvarig **rekryterare**. | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| | **Overall** | | | **43** | **183** | 43/183 = **23%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [servera mat och dryck, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/AaYq_MQh_c9h) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x |  |  | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| x |  |  | [Restaurang, servering, **keyword**](http://data.jobtechdev.se/taxonomy/concept/GqXD_ArB_Mmg) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| x |  |  | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| x | x | x | [sköta en kassa, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/USNU_LMY_prY) |
| x |  |  | [Serveringspersonal, **job-title**](http://data.jobtechdev.se/taxonomy/concept/VqRa_738_v98) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x |  |  | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| x |  |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x |  |  | [Diskplockare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/pEsS_jVe_EMQ) |
|  | x |  | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| | | **4** | 4/14 = **29%** |