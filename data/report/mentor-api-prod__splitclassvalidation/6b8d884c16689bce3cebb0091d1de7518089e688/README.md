# Results for '6b8d884c16689bce3cebb0091d1de7518089e688'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [6b8d884c16689bce3cebb0091d1de7518089e688](README.md) | 1 | 1698 | 13 | 10 | 66/116 = **57%** | 5/10 = **50%** |

## Source text

Team Member 70-100% - MISTER YORK Alingsås Mister York expanderar och söker stjärnor till Alingsås! Det började som en food truck på Stortorget i Kalmar direkt efter studenten år 2020. Coronapandemin slog hårt och arbetslösheten steg. Två vänner bestämde sig då för att laga burgare i sin nyinköpta food truck för att sprida glädje till folket. Burgarna visade sig bli en fortsatt enorm succé. Verksamheten finns idag på flera olika orter och har en tydlig expansionsplan för framtiden där Alingsås är näst på tur.   Tjänst Denna tjänst varierar mellan 70-100%. Arbetstid kan variera alla dagar i veckan. Tjänsten beräknas starta i slutet av augusti.   Rollen som Team Member Vi söker dig som är ansvarstagande, utåtriktad, engagerad & duktig på att samarbeta väl i grupp. Du kommer att jobba i ett sammansvetsat team för att leverera mat & service i världsklass. Dina arbetsuppgifter som Team Member kommer innefatta bland annat: Förberedelser av tillbehör, tillagning av våra prisbelönta burgare, städning samt mottagande av kunder i kassan.     Hög kvalitet är något som genomsyrar hela vår företagskultur. Alla som arbetar inom vår verksamhet ska veta varför de går till jobbet varje morgon och hur de är med och bidrar.   VÅRA FÖRVÄNTNINGAR PÅ DIG: - Du har rätt approach och vill ha roligt på jobbet. - Du är en person som vill utvecklas och lära dig nya saker. - Du är en lagspelare som samarbetar bra med andra.  - Du uppskattar att jobba i ett högt tempo. - Du är noggrann och effektiv.    På Mister York så rekryterar vi utifrån personlighet och potential och inte endast utifrån tidigare erfarenheter. Vi erbjuder er full utbildning på plats. Mister York är anslutna till kollektivavtal.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Team Member **70**-100% - MISTER YORK Alingsås M... | x |  |  | 2 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 2 | Team Member 70-**100%** - MISTER YORK Alingsås Mister... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 3 | ... Member 70-100% - MISTER YORK **Alingsås** Mister York expanderar och sö... | x | x | 8 | 8 | [Alingsås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UQ75_1eU_jaC) |
| 4 | ...derar och söker stjärnor till **Alingsås**! Det började som en food truc... | x | x | 8 | 8 | [Alingsås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UQ75_1eU_jaC) |
| 5 | ... Alingsås! Det började som en **food truck** på Stortorget i Kalmar direkt... | x |  |  | 10 | [Food truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/b57b_Nce_ahb) |
| 6 | ...gsås! Det började som en food **truck** på Stortorget i Kalmar direkt... |  | x |  | 5 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 7 | ...en food truck på Stortorget i **Kalmar** direkt efter studenten år 202... | x | x | 6 | 6 | [Kalmar, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Pnmg_SgP_uHQ) |
| 8 | ... laga burgare i sin nyinköpta **food truck** för att sprida glädje till fo... | x |  |  | 10 | [Food truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/b57b_Nce_ahb) |
| 9 | ... burgare i sin nyinköpta food **truck** för att sprida glädje till fo... |  | x |  | 5 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 10 | ...ansionsplan för framtiden där **Alingsås** är näst på tur.   Tjänst Denn... | x | x | 8 | 8 | [Alingsås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UQ75_1eU_jaC) |
| 11 | ... Denna tjänst varierar mellan **70**-100%. Arbetstid kan variera a... | x |  |  | 2 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 12 | ...nna tjänst varierar mellan 70-**100%**. Arbetstid kan variera alla d... | x |  |  | 4 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 13 | ...am Member Vi söker dig som är **ansvarstagande**, utåtriktad, engagerad & dukt... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 14 | ... av våra prisbelönta burgare, **städning** samt mottagande av kunder i k... |  | x |  | 8 | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| 15 | ...bba i ett högt tempo. - Du är **noggrann** och effektiv.    På Mister Yo... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 16 | ... Mister York är anslutna till **kollektivavtal**. | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **66** | **116** | 66/116 = **57%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [ansvara för städning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2pc1_7xW_uYA) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Kalmar, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Pnmg_SgP_uHQ) |
| x | x | x | [Alingsås, **municipality**](http://data.jobtechdev.se/taxonomy/concept/UQ75_1eU_jaC) |
|  | x |  | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x |  |  | [Food truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/b57b_Nce_ahb) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **5** | 5/10 = **50%** |