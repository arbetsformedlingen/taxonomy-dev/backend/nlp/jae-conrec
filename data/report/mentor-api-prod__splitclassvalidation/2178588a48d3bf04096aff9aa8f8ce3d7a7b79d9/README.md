# Results for '2178588a48d3bf04096aff9aa8f8ce3d7a7b79d9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2178588a48d3bf04096aff9aa8f8ce3d7a7b79d9](README.md) | 1 | 1533 | 7 | 6 | 50/70 = **71%** | 5/8 = **62%** |

## Source text

Positiva industriarbetare sökes till höglandet Till våra kunder på höglandet söker vi positiva och glada Industrimedarbetare.  Våra kunder inom industrin behöver förstärkning på flertalet positioner inom produktionen, varför vi gärna vill komma i kontakt med dig som trivs inom industrin eller som vill byta jobb.  Din profil  Du som söker denna tjänst har troligen någon form av praktisk utbildning eller motsvarande arbetslivserfarenhet inom industri eller lager. För att ta dig an utmaningen som industriarbetare är det väsentligt att du gillar fysiskt krävande arbete och är en mycket god lagspelare där du har lätt för samarbete. Du är även praktiskt lagt med positiv inställning och hög arbetsmoral. Du förstår även vikten av ett gott samarbete, att hålla tiderna och göra ett bra jobb.  Du har körkort och tillgång till bil, då det kan förekomma skiftarbete. Har du truckkort är detta klart meriterande. Vi erbjuder dig ett stimulerande industriarbete med glada och kompetenta medarbetare och ledare.  Är det dig vi söker? Tveka inte att ansöka redan idag! Du ansöker direkt på www.empleo.se  Du är välkommen med din ansökan och förhoppningsvis kan vi träffas på ett proaktivt möte där vi kan diskutera framtida jobbmöjligheter. Vi rekryterar löpande och tjänsterna kan komma att tillsättas innan ansökningstiden har gått ut.  Uppstart: Omgående eller enligt överenskommelse Ort/Placering: Vetlanda med omnejd Omfattning: Heltid, skiftgång kan förekomma Lön: Enligt gällande kollektivavtal Ansökningstid: Senast den 23 augusti

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...rfarenhet inom industri eller **lager**. För att ta dig an utmaningen... | x | x | 5 | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 2 | ...ch göra ett bra jobb.  Du har **körkort** och tillgång till bil, då det... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 2 | ...ch göra ett bra jobb.  Du har **körkort** och tillgång till bil, då det... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 3 | ...förekomma skiftarbete. Har du **truckkort** är detta klart meriterande. V... | x | x | 9 | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 4 | ...erbjuder dig ett stimulerande **industriarbete** med glada och kompetenta meda... | x | x | 14 | 14 | [Industriarbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fpA8_XFc_122) |
| 5 | ...verenskommelse Ort/Placering: **Vetlanda** med omnejd Omfattning: Heltid... | x | x | 8 | 8 | [Vetlanda, **municipality**](http://data.jobtechdev.se/taxonomy/concept/xJqx_SLC_415) |
| 6 | ...tlanda med omnejd Omfattning: **Heltid**, skiftgång kan förekomma Lön:... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 7 | ...örekomma Lön: Enligt gällande **kollektivavtal** Ansökningstid: Senast den 23 ... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **50** | **70** | 50/70 = **71%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x | x | x | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x | x | x | [Industriarbete, **keyword**](http://data.jobtechdev.se/taxonomy/concept/fpA8_XFc_122) |
| x | x | x | [Vetlanda, **municipality**](http://data.jobtechdev.se/taxonomy/concept/xJqx_SLC_415) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **5** | 5/8 = **62%** |