import unittest
from jae_conrec import core
from conrec_utils import taxonomy
from conrec_utils.env import default_env

tax = taxonomy.load_taxonomy(default_env())

elements = [3, 4, 9, 119]

cps = [{'synonym_key': 'trait/Trovärdig', 'synonym_type': 'trait', 'synonym_label': 'Trovärdig', 'candidates': [{'concept_id': 'nmJj_j6j_efT', 'concept_preferred_label': 'agera trovärdigt', 'concept_type': 'esco-skill', 'label_count': 1, 'type_cost': 1000, 'candidate_annotation': ''}]}, {'synonym_key': 'trait/Uthållig', 'synonym_type': 'trait', 'synonym_label': 'Uthållig', 'candidates': [{'concept_id': 'XVSr_yhQ_BZu', 'concept_preferred_label': 'vara uthållig', 'concept_type': 'esco-skill', 'label_count': 1, 'type_cost': 1000, 'candidate_annotation': ''}]}, {'synonym_key': 'trait/Vänlig', 'synonym_type': 'trait', 'synonym_label': 'Vänlig', 'candidates': [{'concept_id': 'SvXq_P4S_q9p', 'concept_preferred_label': 'vara vänlig mot passagerare', 'concept_type': 'esco-skill', 'label_count': 1, 'type_cost': 1000, 'candidate_annotation': ''}]}]

class TestCore(unittest.TestCase):
    def test_cps(self):
        self.assertEqual(
            cps,
            core.unflatten_concepts_per_synonym(
                core.flatten_concepts_per_synonym(cps)))
        self.assertTrue(core.concepts_per_synonym_spec.is_valid(cps))
