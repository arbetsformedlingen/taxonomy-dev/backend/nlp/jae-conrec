# Results for '53c7908ee5e704bbc224fdff3bbc8042e76ac6c6'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [53c7908ee5e704bbc224fdff3bbc8042e76ac6c6](README.md) | 1 | 781 | 15 | 6 | 21/262 = **8%** | 2/9 = **22%** |

## Source text

Utbildningen ger dig allt du behöver för att vara med och utveckla framtidens IT-system. Agil Javautvecklare är en mycket eftertraktad utbildning och företagen i branschen har ett stort rekryteringsbehov av systemutvecklare med just den kompetens som utbildningen ger. Du får lära dig agila utvecklingsmetoder, jobba med moderna ramverk och plattformar samt nya populära javascriptbibliotek.    En systemutvecklare inom Java arbetar med att ta fram och utveckla data/IT-system eller delar av system. Kärnkompetensen för systemutvecklaren är programmering. Man förväntas behärska agila utvecklingsmetoder, moderna ramverk och plattformar samt nya populära javabibliotek. Java finns idag på ett stort antal företag och myndigheter, och dessa behöver löpande underhåll och utveckling.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... behöver för att vara med och **utveckla framtidens IT-system**. Agil Javautvecklare är en my... | x |  |  | 29 | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| 2 | ...a med och utveckla framtidens **IT-system**. Agil Javautvecklare är en my... |  | x |  | 9 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 3 | ...la framtidens IT-system. Agil **Javautvecklare** är en mycket eftertraktad utb... |  | x |  | 14 | [Javautvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F8PV_jpm_qwT) |
| 3 | ...la framtidens IT-system. Agil **Javautvecklare** är en mycket eftertraktad utb... | x |  |  | 14 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 4 | ...tt stort rekryteringsbehov av **systemutvecklare** med just den kompetens som ut... | x |  |  | 16 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 5 | ...ldningen ger. Du får lära dig **agila utvecklingsmetoder**, jobba med moderna ramverk oc... | x |  |  | 24 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 6 | ...ngsmetoder, jobba med moderna **ramverk** och plattformar samt nya popu... | x |  |  | 7 | [Ramverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/srsj_GQb_CE1) |
| 7 | ...plattformar samt nya populära **javascriptbibliotek**.    En systemutvecklare inom ... | x |  |  | 19 | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
| 8 | ...ra javascriptbibliotek.    En **systemutvecklare** inom Java arbetar med att ta ... | x |  |  | 16 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 9 | ....    En systemutvecklare inom **Java** arbetar med att ta fram och u... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 10 | ...a arbetar med att ta fram och **utveckla data/IT-system** eller delar av system. Kärnko... | x |  |  | 23 | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| 11 | ...att ta fram och utveckla data/**IT-system** eller delar av system. Kärnko... |  | x |  | 9 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 12 | ...v system. Kärnkompetensen för **systemutvecklaren** är programmering. Man förvänt... | x |  |  | 17 | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| 13 | ...nsen för systemutvecklaren är **programmering**. Man förväntas behärska agila... | x | x | 13 | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 14 | ...ering. Man förväntas behärska **agila utvecklingsmetoder**, moderna ramverk och plattfor... | x |  |  | 24 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 15 | ...a utvecklingsmetoder, moderna **ramverk** och plattformar samt nya popu... | x |  |  | 7 | [Ramverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/srsj_GQb_CE1) |
| 16 | ...plattformar samt nya populära **javabibliotek**. Java finns idag på ett stort... | x |  |  | 13 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 17 | ...t nya populära javabibliotek. **Java** finns idag på ett stort antal... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| | **Overall** | | | **21** | **262** | 21/262 = **8%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Systemutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/D7Ns_RG6_hD2) |
| x |  |  | [JavaScript, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/DHhX_uVf_y6X) |
|  | x |  | [Javautvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/F8PV_jpm_qwT) |
| x |  |  | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
|  | x |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Systemutvecklare/Programmerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/fg7B_yov_smw) |
| x | x | x | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x | x | x | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| x |  |  | [Ramverk, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/srsj_GQb_CE1) |
| | | **2** | 2/9 = **22%** |