# Results for '2f20f13844dfe5604e23bf553e2fc137bc860add'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [2f20f13844dfe5604e23bf553e2fc137bc860add](README.md) | 1 | 5074 | 24 | 34 | 120/627 = **19%** | 7/32 = **22%** |

## Source text

Bibliotekarie/Biblioteksassistent I Falu kommun finns en mängd intressanta och kvalificerade arbetsuppgifter. Vi har ingenjörer, kommunikatörer, lärare, undersköterskor, projektledare och jurister samt 214 andra yrkesbefattningar. Vår vision är Ett större Falun. Vi ska vara enkla att samarbeta med och till nytta för faluborna. Vi gör skillnad och vi har jobb som märks. Vi är dessutom en arbetsgivare som vill ta tillvara våra medarbetares fulla kompetens - det vet vi leder till utveckling både för individen och för organisationen. För oss är det viktigt att ge våra medarbetare ett hållbart arbetsliv, vilket vi kallar ”Balans i livet”. Vi satsar alltså mycket på de mjuka värdena – hur människor mår, blir bemötta och kan växa – eftersom det är vår definition av att vara en riktigt bra arbetsgivare. I Falu kommun eftersträvar vi jämställdhet och vi ser gärna en personalsammansättning som speglar mångfalden i samhället. Vi välkomnar därför sökande som bidrar till detta.  Västra skolan är en skola för åk 7-9 med ca. 530 elever som är centralt belägen i Falun. Vi har gångavstånd till teater, Dalarnas museum, Stadsbiblioteket och Världsarvet i Falun. Vi på Västra skolan arbetar med visionen att Västra är det självklara valet, för såväl elever som medarbetare! Här tar vi ansvar, är engagerade och har kul på jobbet.    På Västra skolan arbetar vi utifrån en gemensam plattform som innefattar ömsesidig respekt mellan alla på skolan och betonar vikten av trygga och lugna lärmiljöer. Våra verksamhetsmål för skolan har under innevarande år varit; Trygghet och studiero, lust till lärande och ökad måluppfyllelse. Vi vill ge våra elever de bästa förutsättningarna till individuell utveckling och goda studieresultat. Vi kallar detta "Vårt Västra 2.0", och nu vill vi att du blir en del av detta.    Vi söker nu en driven medarbetare till vårt skolbibliotek. Vi önskar en person som brinner för läsning och läsfrämjande arbete med ungdomar och som är intresserad och insatt i litteratur för unga.  Skolbiblioteket ska bidra till att skapa en kreativ, utvecklande miljö i skolan genom att erbjuda biblioteket inte bara som rum för bokutlåning och läsning, utan även för samtal, skrivande och informationssökning.   Gillar du teamarbete och lösningsfokus så är detta ett arbete för dig.  Varmt välkommen med din ansökan!  Arbetsuppgifter I arbetet ingår att katalogisera böcker, köpa in, registrera och låna ut böcker. I arbetet kommer du ha daglig kontakt med elever som besöker biblioteket både klassvis och enskilt. Vi önskar forma ett bibliotek som bjuder in till läsning genom att exempelvis ha olika teman som frontas, boktips i biblioteket och via våra sociala medier. Rätt bok ska kunna erbjudas till rätt elev både vad gäller svårighetsnivå och genre.   Du kommer att ingå i ett ämneslag och samarbeta med pedagogerna när det gäller läsfrämjande insatser. I tjänsten ingår också arbete kring MIK (medie- och informationskunnighet) och att i samarbete med pedagoger ge eleverna kunskap i detta. Vi önskar också återuppta vårt biblioteksråd där elever ska ingå för att skapa större elevinflytande när det gäller bokinköp.   Kvalifikationer För rollen krävs:   - Att du är utbildad bibliotekarie  - Aktuell arbetslivserfarenhet utifrån likande arbete, t.ex. som biblioteksassistent   - Gärna tidigare erfarenhet av att arbeta inom skolan   Personliga egenskaper Du är trygg i din arbetsroll och förstår att goda relationer främjar undervisningssituationer och skapar trygghet för dig och skolans elever. Vidare ser vi att du är en ansvarsfull, tydlig och strukturerad i ditt arbetet samt har ett positivt och lösningsfokuserat förhållningssätt. Du inspireras av ungdomars nyfikenhet och engagemang och bidrar på så sätt till ett aktivt lärande hos varje enskild elev. Vi ser gärna att du är engagerad i biblioteksutvecklingsfrågor, delar skolans verksamhetsmål och vill utvecklas tillsammans med övriga kollegor i ett positivt klimat på Västra skolan.  Anställningsvillkor Tjänsten är en visstidsanställning/vikarierat med tillträde inför läsårsstart aug 2022.   För den som erbjuds tjänsten krävs enligt Lagen om registerkontroll (2010:479) utdrag från Rikspolisens belastningsregister. Beställ gärna detta i samband med ansökan så att det finns tillgängligt ifall du blir aktuell för tjänsten.   Kopior på betyg lämnas till Falu kommun i samband med intervju.  Upplysningar  Rektor: Christian Lund, 023-821 12; christian.lund@falun.se    Fackliga företrädare   SACO: Ulrika Bergman, ulrika.bergman@falun.se  Lärarnas riksförbund: Åsa Österlund, asa.osterlund@falun.se  Lärarförbundet: Susanne Kyrö, susanne.kyro@falun.se  Övrigt  Inför denna rekrytering har vi tagit ställning till rekryteringskanaler och därför undanber vi oss ytterligare erbjudanden om kompetensförmedling, annonserings- och rekryteringshjälp. Vi tar endast emot ansökningar via vårt rekryteringssystem i enlighet med GDPR. Vid problem med att genomföra din ansökan är du välkommen att kontakta rekryteringscenter@falun.se.  Kontaktinformation Christian Lund, Rektor  Arbetsplats   Falun

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Bibliotekarie**/Biblioteksassistent I Falu ko... | x | x | 13 | 13 | [Bibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GujS_MtW_PRH) |
| 2 | Bibliotekarie/**Biblioteksassistent** I Falu kommun finns en mängd ... |  | x |  | 19 | [Biblioteksassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/CWFv_5ge_vH2) |
| 2 | Bibliotekarie/**Biblioteksassistent** I Falu kommun finns en mängd ... | x | x | 19 | 19 | [Biblioteksassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Mx7K_ThX_jz4) |
| 2 | Bibliotekarie/**Biblioteksassistent** I Falu kommun finns en mängd ... |  | x |  | 19 | [Biblioteks- och arkivassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/QQti_mfk_g16) |
| 2 | Bibliotekarie/**Biblioteksassistent** I Falu kommun finns en mängd ... |  | x |  | 19 | [biblioteksassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Scyu_7g4_zQY) |
| 2 | Bibliotekarie/**Biblioteksassistent** I Falu kommun finns en mängd ... |  | x |  | 19 | [Arkiv- och biblioteksassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/TQkt_eeK_eNp) |
| 2 | Bibliotekarie/**Biblioteksassistent** I Falu kommun finns en mängd ... |  | x |  | 19 | [Arkiv- och biblioteksassistenter, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/ftHf_awu_8vL) |
| 3 | ...erade arbetsuppgifter. Vi har **ingenjörer**, kommunikatörer, lärare, unde... | x |  |  | 10 | [Ingenjörer och tekniker, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DhEP_m8M_qoi) |
| 4 | ...uppgifter. Vi har ingenjörer, **kommunikatörer**, lärare, undersköterskor, pro... | x | x | 14 | 14 | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| 5 | ...örer, kommunikatörer, lärare, **undersköterskor**, projektledare och jurister s... |  | x |  | 15 | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| 5 | ...örer, kommunikatörer, lärare, **undersköterskor**, projektledare och jurister s... | x |  |  | 15 | [Undersköterska, vård- och specialavdelning och mottagning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PQkQ_Dmk_ZF8) |
| 6 | ...rer, lärare, undersköterskor, **projektledare** och jurister samt 214 andra y... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 6 | ...rer, lärare, undersköterskor, **projektledare** och jurister samt 214 andra y... | x |  |  | 13 | [Projektledare, offentlig verksamhet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/r47H_2uB_RDF) |
| 7 | ...sköterskor, projektledare och **jurister** samt 214 andra yrkesbefattnin... | x | x | 8 | 8 | [Jurist, allmän domstol, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2wQ2_9vW_jPL) |
| 7 | ...sköterskor, projektledare och **jurister** samt 214 andra yrkesbefattnin... |  | x |  | 8 | [Jurister, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/qWdm_MBz_r99) |
| 8 | ...gar. Vår vision är Ett större **Falun**. Vi ska vara enkla att samarb... | x | x | 5 | 5 | [Falun, **municipality**](http://data.jobtechdev.se/taxonomy/concept/N1wJ_Cuu_7Cs) |
| 9 | ...våra medarbetare ett hållbart **arbetsliv**, vilket vi kallar ”Balans i l... |  | x |  | 9 | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| 10 | ...I Falu kommun eftersträvar vi **jämställdhet** och vi ser gärna en personals... |  | x |  | 12 | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
| 11 | ...ver som är centralt belägen i **Falun**. Vi har gångavstånd till teat... | x | x | 5 | 5 | [Falun, **municipality**](http://data.jobtechdev.se/taxonomy/concept/N1wJ_Cuu_7Cs) |
| 12 | ...alun. Vi har gångavstånd till **teater**, Dalarnas museum, Stadsbiblio... |  | x |  | 6 | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| 13 | ...biblioteket och Världsarvet i **Falun**. Vi på Västra skolan arbetar ... |  | x |  | 5 | [Falun, **municipality**](http://data.jobtechdev.se/taxonomy/concept/N1wJ_Cuu_7Cs) |
| 14 | ...uppgifter I arbetet ingår att **katalogisera böcker**, köpa in, registrera och låna... | x |  |  | 19 | [Katalogiseringsvana, bibliotek, **skill**](http://data.jobtechdev.se/taxonomy/concept/G91h_2aK_aXY) |
| 15 | ...rbetet ingår att katalogisera **böcker**, köpa in, registrera och låna... |  | x |  | 6 | [Böcker/Tidskrifter, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2VQ_P7u_GB4) |
| 16 | ...ngår att katalogisera böcker, **köpa in**, registrera och låna ut böcke... | x |  |  | 7 | [Inköpsansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/xqBL_5Lz_21K) |
| 17 | ...katalogisera böcker, köpa in, **registrera och låna ut böcker**. I arbetet kommer du ha dagli... | x |  |  | 29 | [leda daglig biblioteksverksamhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2JYK_1ZW_qfh) |
| 18 | ...vad gäller svårighetsnivå och **genre**.   Du kommer att ingå i ett ä... |  | x |  | 5 | [specialisera sig inom en musikalisk genre, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ECPa_DfQ_Z9U) |
| 19 | ...r att ingå i ett ämneslag och **samarbeta med pedagogerna** när det gäller läsfrämjande i... | x |  |  | 25 | [samarbeta med utbildningspersonal, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/K7p4_CoU_jXE) |
| 20 | ... arbete kring MIK (medie- och **informationskunnighet**) och att i samarbete med peda... |  | x |  | 21 | [Bibliotekarieutbildning samt biblioteks- och informationsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/FsKa_aFC_yPT) |
| 21 | ...ormationskunnighet) och att i **samarbete med pedagoger** ge eleverna kunskap i detta. ... | x |  |  | 23 | [samarbeta med utbildningspersonal, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/K7p4_CoU_jXE) |
| 22 | ...i önskar också återuppta vårt **biblioteksråd** där elever ska ingå för att s... | x | x | 13 | 13 | [Biblioteksråd, **job-title**](http://data.jobtechdev.se/taxonomy/concept/HEzy_pvG_yxh) |
| 23 | ...krävs:   - Att du är utbildad **bibliotekarie**  - Aktuell arbetslivserfarenh... | x | x | 13 | 13 | [Bibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GujS_MtW_PRH) |
| 24 | ...rån likande arbete, t.ex. som **biblioteksassistent**   - Gärna tidigare erfarenhet... |  | x |  | 19 | [Biblioteksassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/CWFv_5ge_vH2) |
| 24 | ...rån likande arbete, t.ex. som **biblioteksassistent**   - Gärna tidigare erfarenhet... | x | x | 19 | 19 | [Biblioteksassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Mx7K_ThX_jz4) |
| 24 | ...rån likande arbete, t.ex. som **biblioteksassistent**   - Gärna tidigare erfarenhet... |  | x |  | 19 | [Biblioteks- och arkivassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/QQti_mfk_g16) |
| 24 | ...rån likande arbete, t.ex. som **biblioteksassistent**   - Gärna tidigare erfarenhet... |  | x |  | 19 | [biblioteksassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Scyu_7g4_zQY) |
| 24 | ...rån likande arbete, t.ex. som **biblioteksassistent**   - Gärna tidigare erfarenhet... |  | x |  | 19 | [Arkiv- och biblioteksassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/TQkt_eeK_eNp) |
| 24 | ...rån likande arbete, t.ex. som **biblioteksassistent**   - Gärna tidigare erfarenhet... |  | x |  | 19 | [Arkiv- och biblioteksassistenter, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/ftHf_awu_8vL) |
| 25 | ...r. Vidare ser vi att du är en **ansvarsfull**, tydlig och strukturerad i di... | x | x | 11 | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 26 | ...tet samt har ett positivt och **lösningsfokuserat förhållningssätt**. Du inspireras av ungdomars n... | x |  |  | 34 | [Lösningsfokuserat arbetssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/94uF_bkH_QF5) |
| 27 | ...llningsvillkor Tjänsten är en **visstidsanställning**/vikarierat med tillträde infö... | x |  |  | 19 | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
| 28 | ...ten är en visstidsanställning/**vikarierat** med tillträde inför läsårssta... | x |  |  | 10 | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| 29 | ...plysningar  Rektor: Christian **Lund**, 023-821 12; christian.lund@f... |  | x |  | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 30 | ... Kontaktinformation Christian **Lund**, Rektor  Arbetsplats   Falun |  | x |  | 4 | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
| 31 | ...n Lund, Rektor  Arbetsplats   **Falun** |  | x |  | 5 | [Falun, **municipality**](http://data.jobtechdev.se/taxonomy/concept/N1wJ_Cuu_7Cs) |
| | **Overall** | | | **120** | **627** | 120/627 = **19%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [leda daglig biblioteksverksamhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/2JYK_1ZW_qfh) |
| x | x | x | [Jurist, allmän domstol, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/2wQ2_9vW_jPL) |
|  | x |  | [Undersköterskor, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/3EJj_WLs_kgD) |
| x |  |  | [Lösningsfokuserat arbetssätt, **skill**](http://data.jobtechdev.se/taxonomy/concept/94uF_bkH_QF5) |
|  | x |  | [Biblioteksassistenter m.fl., **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/CWFv_5ge_vH2) |
| x |  |  | [Ingenjörer och tekniker, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/DhEP_m8M_qoi) |
|  | x |  | [specialisera sig inom en musikalisk genre, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ECPa_DfQ_Z9U) |
|  | x |  | [Bibliotekarieutbildning samt biblioteks- och informationsvetenskap, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/FsKa_aFC_yPT) |
| x |  |  | [Katalogiseringsvana, bibliotek, **skill**](http://data.jobtechdev.se/taxonomy/concept/G91h_2aK_aXY) |
| x | x | x | [Bibliotekarie, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GujS_MtW_PRH) |
| x | x | x | [Biblioteksråd, **job-title**](http://data.jobtechdev.se/taxonomy/concept/HEzy_pvG_yxh) |
| x |  |  | [samarbeta med utbildningspersonal, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/K7p4_CoU_jXE) |
| x | x | x | [Biblioteksassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Mx7K_ThX_jz4) |
| x | x | x | [Falun, **municipality**](http://data.jobtechdev.se/taxonomy/concept/N1wJ_Cuu_7Cs) |
| x |  |  | [Undersköterska, vård- och specialavdelning och mottagning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/PQkQ_Dmk_ZF8) |
|  | x |  | [Biblioteks- och arkivassistenter, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/QQti_mfk_g16) |
|  | x |  | [biblioteksassistent, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Scyu_7g4_zQY) |
|  | x |  | [Arkiv- och biblioteksassistenter m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/TQkt_eeK_eNp) |
|  | x |  | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
|  | x |  | [Jämställdhet, **keyword**](http://data.jobtechdev.se/taxonomy/concept/WgGj_zNt_Pzz) |
|  | x |  | [Böcker/Tidskrifter, **skill**](http://data.jobtechdev.se/taxonomy/concept/Y2VQ_P7u_GB4) |
| x | x | x | [Informatör/Kommunikatör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/aRp4_qjZ_tPV) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Arkiv- och biblioteksassistenter, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/ftHf_awu_8vL) |
| x |  |  | [Vikariat, **employment-type**](http://data.jobtechdev.se/taxonomy/concept/gro4_cWF_6D7) |
| x |  |  | [Allmän visstidsanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hxfz_ayC_9Vd) |
|  | x |  | [Lund, **municipality**](http://data.jobtechdev.se/taxonomy/concept/muSY_tsR_vDZ) |
|  | x |  | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
|  | x |  | [Jurister, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/qWdm_MBz_r99) |
| x |  |  | [Projektledare, offentlig verksamhet, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/r47H_2uB_RDF) |
|  | x |  | [Teater, **keyword**](http://data.jobtechdev.se/taxonomy/concept/u7zK_VKN_8Ht) |
| x |  |  | [Inköpsansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/xqBL_5Lz_21K) |
| | | **7** | 7/32 = **22%** |