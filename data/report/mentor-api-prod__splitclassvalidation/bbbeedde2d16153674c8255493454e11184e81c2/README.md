# Results for 'bbbeedde2d16153674c8255493454e11184e81c2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [bbbeedde2d16153674c8255493454e11184e81c2](README.md) | 1 | 1955 | 4 | 0 | 0/40 = **0%** | 0/2 = **0%** |

## Source text

McDonald's Ättekulla söker nya glada medarbetare Vill du få möjligheten att arbeta tillsammans med ett härligt team där lagandan står i fokus? Är du även den som har förmågan att se när något behöver göras och strävar efter att ge en service i världsklass?   Om du svarade ja på frågorna ovan och dessutom uppskattar en Big Mac nästan lika mycket som oss – då kanske vi är helt rätt för varandra!   Hos oss är det av största vikt att varje kund får en unik upplevelse och det är naturligtvis inte möjligt utan våra medarbetare. McDonald’s erbjuder ett omväxlande och fartfyllt arbete med varierande arbetsuppgifter, alla med fokuset att leverera en ultimat restaurangupplevelse för våra gäster.    Arbetsuppgifterna kan variera från att tillaga och servera vår goda mat till att säkerställa att matsal och kök är i toppskick.   En anställning på McDonald’s ger dig erfarenheter som du har nytta av resten av livet och för den som vill utvecklas finns en tydlig karriärtrappa med stora möjligheter. Våra förväntningar på dig:    - Att du är en team player som sätter laget före jaget  - Att du vill lära dig nya saker och utvecklas  - Att du har rätt inställning och vill ha roligt på jobbet  - Att du älskar burgare nästan lika mycket som vi  Tidigare arbetslivserfarenhet är inte ett krav, vi ser till att du får den utbildning du behöver för att kunna utföra ett arbete av högsta kvalitet.  Med över 200 restauranger från norr till söder är McDonald’s Sveriges ledande restaurangkedja. De flesta av restaurangerna ägs och drivs lokalt av egna företagare. Med våra ca 12 000 medarbetare är vi Sveriges största privata ungdomsarbetsgivare. Vi vill göra det goda valet enkelt för våra gäster och har därför en bred meny med något för alla smaker, preferenser och plånböcker och serverar bara mat av högsta kvalitet med snabb och vänlig service i fräscha, moderna restauranger. Läs mer på www.mcdonalds.se eller följ oss på Facebook, Instagram och Twitter.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **McDonald's** Ättekulla söker nya glada med... | x |  |  | 10 | [McDonald's, **keyword**](http://data.jobtechdev.se/taxonomy/concept/JqBx_uAB_4D4) |
| 2 | ...öjligt utan våra medarbetare. **McDonald’s** erbjuder ett omväxlande och f... | x |  |  | 10 | [McDonalds, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kGdP_NgW_cz4) |
| 3 | ...oppskick.   En anställning på **McDonald’s** ger dig erfarenheter som du h... | x |  |  | 10 | [McDonalds, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kGdP_NgW_cz4) |
| 4 | ...anger från norr till söder är **McDonald’s** Sveriges ledande restaurangke... | x |  |  | 10 | [McDonalds, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kGdP_NgW_cz4) |
| | **Overall** | | | **0** | **40** | 0/40 = **0%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [McDonald's, **keyword**](http://data.jobtechdev.se/taxonomy/concept/JqBx_uAB_4D4) |
| x |  |  | [McDonalds, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kGdP_NgW_cz4) |
| | | **0** | 0/2 = **0%** |