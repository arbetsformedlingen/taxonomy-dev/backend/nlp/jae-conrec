# Results for 'a44cb9e1050b38bf2d668965fa56a4ac972cecdb'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [a44cb9e1050b38bf2d668965fa56a4ac972cecdb](README.md) | 1 | 2264 | 11 | 19 | 61/414 = **15%** | 6/21 = **29%** |

## Source text

Konferens- & Receptionsmedarbetare till Elite Plaza Hotel Vi söker dig som älskar Höga Kusten & service lika mycket som vi gör!    Som anställd på konferensavdelningen hanterar du både individuella bokningar via mail samt grupp-& mötesförfrågningar via både mail och telefon genom hela säljprocessen ifrån inkommande förfrågan, behovsanalys, offerthantering, uppföljning, konvertering fram till signerat kontrakt.  Utöver det tar du sedan emot våra hotellgäster på plats i receptionen genom ett varmt och personligt värdskap.  Receptionen är navet på hotellet och ansiktet utåt mot våra gäster. Det är här som både första och sista intrycket skapas. I arbetsuppgifterna ingår in- och utcheckning, merförsäljning, hantering av telefonväxel, bokningar, receptionsadministration samt allmän service till våra gäster.  Vi söker dig som har en väl utvecklad känsla för förstklassig service vilket är en förutsättning för att leverera gästupplevelser på hög nivå. Du är en naturlig lagspelare, ansvarstagande, engagerad och initiativrik. Du har lätt för att lära och du har förmågan att agera med gott omdöme även i lite mer krävande situationer. Du har erfarenhet av liknande arbetsuppgifter samt goda språkkunskaper i svenska och engelska i både tal och skrift. Starkt meriterande är om du har erfarenhet av något hotellbokningssystem, och till stor fördel Infor HMS & SCS.  Främsta ansvarsområden:   Konferens och hantering av befintliga kunder och återkommande möten.   Tillhörande administration, tex offerter, avtal, uppföljning av möten m.m.   Reception, bokning och drift  Tjänsten omfattar en stor variation av arbetsuppgifter därför är det viktigt att du kan hantera flera olika projekt parallellt utan att tappa fokus på målbild och detaljer.    Praktikaliteter:  - Arbetstider: Varierande, dag, kväll & natt - Omfattning: 70% tillsvidareanställning med 6 månader provanställning - Du ska ha fyllt 20 år för att kunna jobba i reception. - Start: Helst september 2022, enl. överenskommelse - Lön: Enligt kollektivavtal - För frågor om tjänsten kontakta: Petra Lundqvist, petra.lundqvist@elite.se - Sista ansökningsdag: 31 augusti 2022. Urval kommer ske löpande, vi rekommenderar därför att du inte väntar med din ansökan!     Vi ser framemot att höra från dig!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Konferens- & Receptionsmedarbetare** till Elite Plaza Hotel Vi sök... | x |  |  | 34 | [receptionist, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/CuKc_tvM_gzc) |
| 2 | ...smedarbetare till Elite Plaza **Hotel** Vi söker dig som älskar Höga ... |  | x |  | 5 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 3 | ...rmt och personligt värdskap.  **Receptionen** är navet på hotellet och ansi... | x |  |  | 11 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 4 | ...kap.  Receptionen är navet på **hotellet** och ansiktet utåt mot våra gä... | x |  |  | 8 | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
| 5 | ...Du är en naturlig lagspelare, **ansvarstagande**, engagerad och initiativrik. ... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 6 | ...er samt goda språkkunskaper i **svenska** och engelska i både tal och s... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 7 | ... språkkunskaper i svenska och **engelska** i både tal och skrift. Starkt... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [Bibbo, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/HQPe_PkY_RLF) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [HappyBooking, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/M7Sn_U9s_HSx) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [Lodgistix, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/R1Yt_c9y_JDD) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [OPERA, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/S3Pj_Law_3Xy) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [Fidelio, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Vrfm_UET_aZT) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [Vivaldi, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/XyNa_nvJ_k8w) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [Nordic Team, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2B8_cXY_G2N) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [CDI, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/fbNJ_NMc_P8R) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [Techotel, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/fn7z_KSK_YAg) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [WINNIN, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/n1no_RNy_Jdo) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [Inmax, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/vtVE_rrX_hhD) |
| 8 | ...om du har erfarenhet av något **hotellbokningssystem**, och till stor fördel Infor H... |  | x |  | 20 | [Hotsoft, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/ySGC_R1c_Dcu) |
| 9 | ..., uppföljning av möten m.m.   **Reception**, bokning och drift  Tjänsten ... | x | x | 9 | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 10 | ...g, kväll & natt - Omfattning: **70%** tillsvidareanställning med 6 ... | x |  |  | 3 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 11 | ...väll & natt - Omfattning: 70% **tillsvidareanställning med 6 månader provanställning** - Du ska ha fyllt 20 år för a... | x |  |  | 52 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 12 | ...t 20 år för att kunna jobba i **reception**. - Start: Helst september 202... | x | x | 9 | 9 | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| 13 | ...överenskommelse - Lön: Enligt **kollektivavtal** - För frågor om tjänsten kont... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **61** | **414** | 61/414 = **15%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x |  |  | [receptionist, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/CuKc_tvM_gzc) |
|  | x |  | [Bibbo, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/HQPe_PkY_RLF) |
| x | x | x | [Hotell, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Jrrg_iiS_2yp) |
|  | x |  | [HappyBooking, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/M7Sn_U9s_HSx) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Lodgistix, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/R1Yt_c9y_JDD) |
|  | x |  | [OPERA, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/S3Pj_Law_3Xy) |
|  | x |  | [Fidelio, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/Vrfm_UET_aZT) |
|  | x |  | [Vivaldi, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/XyNa_nvJ_k8w) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Nordic Team, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2B8_cXY_G2N) |
|  | x |  | [CDI, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/fbNJ_NMc_P8R) |
|  | x |  | [Techotel, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/fn7z_KSK_YAg) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [WINNIN, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/n1no_RNy_Jdo) |
|  | x |  | [Inmax, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/vtVE_rrX_hhD) |
|  | x |  | [Hotsoft, hotellbokningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/ySGC_R1c_Dcu) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Reception, **keyword**](http://data.jobtechdev.se/taxonomy/concept/zRKv_kwY_uw4) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **6** | 6/21 = **29%** |