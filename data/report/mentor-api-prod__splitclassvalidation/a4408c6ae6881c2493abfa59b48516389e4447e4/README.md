# Results for 'a4408c6ae6881c2493abfa59b48516389e4447e4'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [a4408c6ae6881c2493abfa59b48516389e4447e4](README.md) | 1 | 1849 | 15 | 22 | 133/250 = **53%** | 10/23 = **43%** |

## Source text

Borrvagnsförare / Bergarbetare / Bergsprängare. Vi söker en Borrare / Borrvagnsförare till en Epiroc T30 med nya radiolådan. Du måste ha erfarenhet av att borra med Epiroc el Atlas D3 rigg. Samt att kunna hantera Maskinens funktioner såsom GPS och stånghantering samt övrig skötsel av maskinen. Du kommer genast få hoppa in i ett stort projekt med krav på bra borr resultat Körning av andra borriggar. samt att arbete med Darda både C12 och C20 kommer att förekomma i olika projekt. Extra merit *Om du har sprängkort klass A. *Om du har vana att hantera grävmaskiner och lastmaskiner är önskvärt *Svenska och Engelska i Tal och skrift *Obs minst *B-Körkort är ett krav för tjänsten. *Inför anställning kommer ett "utökat utdrag" ur belastningsregistret att genomföras på grund av arbetets art. detta sköts av polisen själva och inte via egen beställning av utdrag.   Personliga kvalifikationer. Vi sätter större värde på att du är en bra person i gruppen än att du har en massa utbildning som inte efterfrågas. Det viktigaste är att du trivs med att borra och kan sköta om riggen, såsom att byta slangar, byta nacke och sköta enklare servicejobb själv och så vidare. Om utbildning för något krävs på en arbetsplats så kan vi nog lösa det. Att Passa tider. Vara ordningsam och att ha god förståelse av att säkerhet efterföljs runt maskinen och på arbetsplatsen är det viktigaste för oss. vi vill alla komma hem till våra familjer efter varje arbetsdag. Heltid. Timlön enl kollektivavtal. Provanställning 6 mån. Referenser och utdrag i belastningsregistret krävs och kommer att kontrolleras.   Om företaget Vi är ett litet företag som växer så det knakar. Vi arbetar endast i Stockholmsregionen så man får sova i egen säng varje natt. vi har fyra borriggar i olika storlekar som du kommer att få bekanta dig med, samt två grävmaskiner och Hjullastare.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Borrvagnsförare** / Bergarbetare / Bergsprängar... | x | x | 15 | 15 | [Borrvagnsförare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/mSma_YbQ_9VD) |
| 2 | Borrvagnsförare / **Bergarbetare** / Bergsprängare. Vi söker en ... | x |  |  | 12 | [Bergarbetare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/nJ4r_QkK_ASp) |
| 3 | ...rvagnsförare / Bergarbetare / **Bergsprängare**. Vi söker en Borrare / Borrva... |  | x |  | 13 | [Bergsprängare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3RP3_h9J_Hc9) |
| 3 | ...rvagnsförare / Bergarbetare / **Bergsprängare**. Vi söker en Borrare / Borrva... |  | x |  | 13 | [bergsprängare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/HGH5_pUg_j8t) |
| 3 | ...rvagnsförare / Bergarbetare / **Bergsprängare**. Vi söker en Borrare / Borrva... |  | x |  | 13 | [Bergsprängare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/sX9n_CWa_KVC) |
| 3 | ...rvagnsförare / Bergarbetare / **Bergsprängare**. Vi söker en Borrare / Borrva... | x | x | 13 | 13 | [Bergsprängare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/v7Fb_Xt6_Ai2) |
| 4 | ... / Bergsprängare. Vi söker en **Borrare** / Borrvagnsförare till en Epi... |  | x |  | 7 | [borrare, metall, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/2xEe_Py4_yjH) |
| 4 | ... / Bergsprängare. Vi söker en **Borrare** / Borrvagnsförare till en Epi... |  | x |  | 7 | [Borrare, metall, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CpcX_B9b_vzN) |
| 4 | ... / Bergsprängare. Vi söker en **Borrare** / Borrvagnsförare till en Epi... |  | x |  | 7 | [borrare, trä, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bCpg_JTy_8nk) |
| 4 | ... / Bergsprängare. Vi söker en **Borrare** / Borrvagnsförare till en Epi... |  | x |  | 7 | [Borrare, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cYto_Nr2_4jw) |
| 4 | ... / Bergsprängare. Vi söker en **Borrare** / Borrvagnsförare till en Epi... |  | x |  | 7 | [Borrare, gruva, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gsmf_rT9_yuR) |
| 4 | ... / Bergsprängare. Vi söker en **Borrare** / Borrvagnsförare till en Epi... | x | x | 7 | 7 | [borrare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/jb6M_kRb_6KD) |
| 5 | ...ängare. Vi söker en Borrare / **Borrvagnsförare** till en Epiroc T30 med nya ra... | x | x | 15 | 15 | [Borrvagnsförare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/mSma_YbQ_9VD) |
| 6 | ...ra Maskinens funktioner såsom **GPS** och stånghantering samt övrig... | x |  |  | 3 | [Geografiskt positionssystem/GPS, **skill**](http://data.jobtechdev.se/taxonomy/concept/pTEz_GD2_J9b) |
| 7 | ...ojekt. Extra merit *Om du har **sprängkort** klass A. *Om du har vana att ... | x | x | 10 | 10 | [Sprängkort, **skill**](http://data.jobtechdev.se/taxonomy/concept/QM4g_Xik_4gR) |
| 7 | ...ojekt. Extra merit *Om du har **sprängkort** klass A. *Om du har vana att ... |  | x |  | 10 | [Sprängkort, undervattenssprängning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TVDZ_JRf_Knw) |
| 8 | .... *Om du har vana att hantera **grävmaskiner** och lastmaskiner är önskvärt ... | x | x | 12 | 12 | [Grävmaskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/swHZ_UXk_UaE) |
| 9 | ... att hantera grävmaskiner och **lastmaskiner** är önskvärt *Svenska och Enge... |  | x |  | 12 | [Yrkesbevis, lastmaskiner, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Nga_Uap_9dB) |
| 10 | ...och lastmaskiner är önskvärt ***Svenska** och Engelska i Tal och skrift... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 11 | ...iner är önskvärt *Svenska och **Engelska** i Tal och skrift *Obs minst *... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 12 | ... i Tal och skrift *Obs minst ***B-Körkort** är ett krav för tjänsten. *In... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 13 | ...miljer efter varje arbetsdag. **Heltid**. Timlön enl kollektivavtal. P... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 14 | ...arbetsdag. Heltid. Timlön enl **kollektivavtal**. Provanställning 6 mån. Refer... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 15 | ... få bekanta dig med, samt två **grävmaskiner** och Hjullastare. | x | x | 12 | 12 | [Grävmaskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/swHZ_UXk_UaE) |
| 16 | ...ed, samt två grävmaskiner och **Hjullastare**. | x | x | 11 | 11 | [Hjullastare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/WUHH_Jpr_Zeh) |
| | **Overall** | | | **133** | **250** | 133/250 = **53%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [borrare, metall, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/2xEe_Py4_yjH) |
|  | x |  | [Bergsprängare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/3RP3_h9J_Hc9) |
|  | x |  | [Yrkesbevis, lastmaskiner, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Nga_Uap_9dB) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Borrare, metall, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CpcX_B9b_vzN) |
|  | x |  | [bergsprängare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/HGH5_pUg_j8t) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Sprängkort, **skill**](http://data.jobtechdev.se/taxonomy/concept/QM4g_Xik_4gR) |
|  | x |  | [Sprängkort, undervattenssprängning, **skill**](http://data.jobtechdev.se/taxonomy/concept/TVDZ_JRf_Knw) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Hjullastare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/WUHH_Jpr_Zeh) |
|  | x |  | [borrare, trä, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bCpg_JTy_8nk) |
|  | x |  | [Borrare, trä, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cYto_Nr2_4jw) |
|  | x |  | [Borrare, gruva, **job-title**](http://data.jobtechdev.se/taxonomy/concept/gsmf_rT9_yuR) |
| x | x | x | [borrare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/jb6M_kRb_6KD) |
| x | x | x | [Borrvagnsförare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/mSma_YbQ_9VD) |
| x |  |  | [Bergarbetare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/nJ4r_QkK_ASp) |
| x |  |  | [Geografiskt positionssystem/GPS, **skill**](http://data.jobtechdev.se/taxonomy/concept/pTEz_GD2_J9b) |
|  | x |  | [Bergsprängare, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/sX9n_CWa_KVC) |
| x | x | x | [Grävmaskin, **skill**](http://data.jobtechdev.se/taxonomy/concept/swHZ_UXk_UaE) |
| x | x | x | [Bergsprängare, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/v7Fb_Xt6_Ai2) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **10** | 10/23 = **43%** |