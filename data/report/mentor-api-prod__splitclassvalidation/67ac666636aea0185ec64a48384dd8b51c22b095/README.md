# Results for '67ac666636aea0185ec64a48384dd8b51c22b095'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [67ac666636aea0185ec64a48384dd8b51c22b095](README.md) | 1 | 3408 | 21 | 17 | 140/273 = **51%** | 11/19 = **58%** |

## Source text

Tekniker till Shapeline i Linköping Hej du som söker nya tekniska utmaningar! Här erbjuds du en roll med inslag av projektledning, avancerad teknik och installation av en världsledande produkt på plats hos våra kunder i Asien, USA och Europa. Låter det spännande? Läs mer om den här tjänsten på Shapeline i Linköping!  Om företaget Shapeline AB är en Linköpingsbaserad tillverkare av laserbaserade system för planhetsmätning inom den globala stål- och metallindustrin. Vi utvecklar, marknadsför och installerar mätsystem för beröringsfri planhetsmätning till våra kunder. Tekniken i systemen, som idag är världsledande, bygger på lasertriangulering och avancerad bildbehandling i realtid för att uppnå optimal prestanda.  Shapeline är i nuläget ett relativt litet företag med tydliga tillväxtambitioner. Vi är i dagsläget 11 anställda. Det innebär att varje medarbetare är en viktig pusselbit och kan påverka företagets framtid i hög grad.  Dina arbetsuppgifter I grova drag kommer du, självständigt eller i team, arbeta med tillverkning, slutmontage och test till färdig installation och integration av vår produkt. Systemen i sig innefattar mekanik, optik, elektronik och mjukvara och du arbetar med samtliga delar med stöd från dina kollegor i produktionen samt våra mjukvaruutvecklare.  Du kan även förvänta dig att; - Vara supportfunktion till befintliga kunder - Utbilda våra kunders användare av Shapeline’s produkt  Tjänsten innefattar kontakter med kunder världen över, både via mejl och telefon samt direkt på plats hos kund. Du får ett stort eget ansvar och måste kunna arbeta både enskilt och i team. Tjänsten innebär resande, ca 60–80 dagar, främst internationellt där Asien, USA och Europa är våra största marknader.  Din profil För att lyckas i rollen tror vi att du har en utbildningsgrund som gymnasieingenjör med några års erfarenhet av ”brett tekniskt arbete”. Det är viktigt att du har ett eget driv, tar ansvar och arbetar strukturerat och systematiskt för att säkerställa kvalitet i alla led mot kund. Du har även förmågan att hantera flera parallella projekt. Du är tekniskt intresserad, gärna inom ett eller fler av dessa områden; mekanik, elektronik, mjukvara samt är införstådd i mekanik- och elritningar. Viktigast för oss är dock att du tycker det är stimulerande att lära dig nya saker. Vi ser det som självklart att du har datavana och du kommunicerar obehindrat på svenska och engelska i både tal och skrift. B-körkort är ett krav.  Vad erbjuder vi dig? En stimulerande och ansvarsfull roll med stor möjlighet att påverka i ett företag med en världsledande produkt. Du kommer till ett företag som levererat och fortsätter leverera produkter till branschledande kunder, med högt fokus på service och kvalitet. Företagskulturen är öppen och hjälpsam. Chef och övriga kollegor står redo för att ge dig de verktyg du behöver för att lyckas och trivas i rollen.  Ansökningsförfarande I denna rekrytering samarbetar Shapeline med Skill. Vid frågor kontakta gärna ansvarig rekryteringskonsult Rasmus Rosander, rasmus.rosander@skill.se. Du ansöker genom att logga in eller registrera CV och personligt brev på www.skill.se. Registrering i vår databas innebär ett enklare rekryteringsförfarande för dig och för oss. Intervjuer och urval sker löpande så tjänsten kan komma att tillsättas innan sista datum. Passa därför på att skicka in din ansökan redan idag! Välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Tekniker till Shapeline i **Linköping** Hej du som söker nya tekniska... | x | x | 9 | 9 | [Linköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bm2x_1mr_Qhx) |
| 2 | ...juds du en roll med inslag av **projektledning**, avancerad teknik och install... | x | x | 14 | 14 | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
| 3 | ...n här tjänsten på Shapeline i **Linköping**!  Om företaget Shapeline AB ä... | x | x | 9 | 9 | [Linköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bm2x_1mr_Qhx) |
| 4 | ... företaget Shapeline AB är en **Linköpingsbaserad** tillverkare av laserbaserade ... | x |  |  | 17 | [Linköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bm2x_1mr_Qhx) |
| 5 | ...ertriangulering och avancerad **bildbehandling** i realtid för att uppnå optim... |  | x |  | 14 | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
| 6 | ...ifter I grova drag kommer du, **självständigt** eller i team, arbeta med till... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 7 | ...digt eller i team, arbeta med **tillverkning**, slutmontage och test till fä... | x | x | 12 | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 8 | ...kt. Systemen i sig innefattar **mekanik**, optik, elektronik och mjukva... | x | x | 7 | 7 | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| 9 | ...men i sig innefattar mekanik, **optik**, elektronik och mjukvara och ... |  | x |  | 5 | [Optiker, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/PUhV_DVQ_MM6) |
| 9 | ...men i sig innefattar mekanik, **optik**, elektronik och mjukvara och ... | x | x | 5 | 5 | [optik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aR94_6EA_fqH) |
| 10 | ...ig innefattar mekanik, optik, **elektronik** och mjukvara och du arbetar m... | x | x | 10 | 10 | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| 11 | ...egor i produktionen samt våra **mjukvaruutvecklare**.  Du kan även förvänta dig at... | x | x | 18 | 18 | [Mjukvaruutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rQds_YGd_quU) |
| 12 | ...även förvänta dig att; - Vara **supportfunktion** till befintliga kunder - Utbi... | x | x | 15 | 15 | [Supportfunktion, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3ejN_ycL_sdm) |
| 13 | ...beta både enskilt och i team. **Tjänsten innebär resande**, ca 60–80 dagar, främst inter... | x |  |  | 24 | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| 14 | ...u har en utbildningsgrund som **gymnasieingenjör** med några års erfarenhet av ”... | x |  |  | 16 | [Gymnasieingenjörsutbildning, inriktning produktionsteknik (T4), **keyword**](http://data.jobtechdev.se/taxonomy/concept/4Zza_Ceq_LKH) |
| 15 | ... eller fler av dessa områden; **mekanik**, elektronik, mjukvara samt är... | x | x | 7 | 7 | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| 16 | ...er av dessa områden; mekanik, **elektronik**, mjukvara samt är införstådd ... | x | x | 10 | 10 | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| 17 | ...mjukvara samt är införstådd i **mekanik**- och elritningar. Viktigast f... | x |  |  | 7 | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| 18 | ...du kommunicerar obehindrat på **svenska** och engelska i både tal och s... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 19 | ...rar obehindrat på svenska och **engelska** i både tal och skrift. B-körk... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 20 | ...gelska i både tal och skrift. **B-körkort** är ett krav.  Vad erbjuder vi... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 21 | ...r vi dig? En stimulerande och **ansvarsfull** roll med stor möjlighet att p... |  | x |  | 11 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 22 | ...r står redo för att ge dig de **verktyg** du behöver för att lyckas och... | x |  |  | 7 | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| 23 | ...rågor kontakta gärna ansvarig **rekryteringskonsult** Rasmus Rosander, rasmus.rosan... | x |  |  | 19 | [rekryteringskonsult, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tGyt_via_HNb) |
| | **Overall** | | | **140** | **273** | 140/273 = **51%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Supportfunktion, **job-title**](http://data.jobtechdev.se/taxonomy/concept/3ejN_ycL_sdm) |
| x |  |  | [Gymnasieingenjörsutbildning, inriktning produktionsteknik (T4), **keyword**](http://data.jobtechdev.se/taxonomy/concept/4Zza_Ceq_LKH) |
| x |  |  | [Verktyg, **skill**](http://data.jobtechdev.se/taxonomy/concept/KG9j_WxP_ym2) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [projektledning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/PPNS_ZLX_YsQ) |
|  | x |  | [Optiker, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/PUhV_DVQ_MM6) |
| x | x | x | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [optik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/aR94_6EA_fqH) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Linköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/bm2x_1mr_Qhx) |
|  | x |  | [Bildredigering, **skill**](http://data.jobtechdev.se/taxonomy/concept/eVFF_obx_bKN) |
| x |  |  | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| x | x | x | [Mjukvaruutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rQds_YGd_quU) |
| x |  |  | [rekryteringskonsult, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tGyt_via_HNb) |
| x | x | x | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| x | x | x | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **11** | 11/19 = **58%** |