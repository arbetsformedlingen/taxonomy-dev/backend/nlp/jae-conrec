# Results for 'aa94d4705246e9ad2aee0ebd016a06e4802b3cdb'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [aa94d4705246e9ad2aee0ebd016a06e4802b3cdb](README.md) | 1 | 649 | 10 | 7 | 54/91 = **59%** | 3/6 = **50%** |

## Source text

Pizzeria Campino söker Pizzabagare till Märsta Pizzeria Campino i Märsta är en pizzeria och salladsbar som även erbjuder kebab och grillrätter. Restaurangen har ett bra läge nära Märsta Station. Vi har 50 platser inne samt en uteservering med uppemot 30 platser.   Vi behöver nu förstärka vårat team med Pizzabagare.   I arbetsuppgifterna ingår att förbereda, baka pizza, städa och servera. Vi hjälps åt med det mesta.   Vi ser helst att du som söker har tidigare erfarenhet av att arbeta som pizzabagare. Introduktion ges på plats. Vi tar endast emot ansökningar via telefon numret nedan och efter kl 21.00. 070-1851172   Välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Pizzeria** Campino söker Pizzabagare til... | x | x | 8 | 8 | [Pizzeria, **skill**](http://data.jobtechdev.se/taxonomy/concept/DmFL_vHQ_j7e) |
| 2 | Pizzeria Campino söker **Pizzabagare** till Märsta Pizzeria Campino ... | x | x | 11 | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 3 | ...söker Pizzabagare till Märsta **Pizzeria** Campino i Märsta är en pizzer... | x | x | 8 | 8 | [Pizzeria, **skill**](http://data.jobtechdev.se/taxonomy/concept/DmFL_vHQ_j7e) |
| 4 | ...zzeria Campino i Märsta är en **pizzeria** och salladsbar som även erbju... | x |  |  | 8 | [Pizzeria, **skill**](http://data.jobtechdev.se/taxonomy/concept/DmFL_vHQ_j7e) |
| 5 | ...bjuder kebab och grillrätter. **Restaurangen** har ett bra läge nära Märsta ... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 6 | ...r nu förstärka vårat team med **Pizzabagare**.   I arbetsuppgifterna ingår ... | x | x | 11 | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| 7 | ...gifterna ingår att förbereda, **baka **pizza, städa och servera. Vi h... |  | x |  | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 8 | ...rna ingår att förbereda, baka **pizza**, städa och servera. Vi hjälps... | x | x | 5 | 5 | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| 9 | ...år att förbereda, baka pizza, **städa** och servera. Vi hjälps åt med... | x |  |  | 5 | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| 10 | ...bereda, baka pizza, städa och **servera**. Vi hjälps åt med det mesta. ... | x |  |  | 7 | [servera mat och dryck, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/AaYq_MQh_c9h) |
| 11 | ... erfarenhet av att arbeta som **pizzabagare**. Introduktion ges på plats. V... | x | x | 11 | 11 | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| | **Overall** | | | **54** | **91** | 54/91 = **59%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [servera mat och dryck, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/AaYq_MQh_c9h) |
| x | x | x | [Pizzeria, **skill**](http://data.jobtechdev.se/taxonomy/concept/DmFL_vHQ_j7e) |
| x | x | x | [Pizzabagare, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/SyvN_uBW_USt) |
| x | x | x | [Pizza, **keyword**](http://data.jobtechdev.se/taxonomy/concept/aoCa_BaF_g6Z) |
| x |  |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x |  |  | [Städa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/r35T_iyj_ueU) |
| | | **3** | 3/6 = **50%** |