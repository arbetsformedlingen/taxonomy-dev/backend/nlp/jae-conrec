# Results for '4d11d02602671c3efb9bf382c4adebfae28baaa5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4d11d02602671c3efb9bf382c4adebfae28baaa5](README.md) | 1 | 3964 | 22 | 20 | 240/385 = **62%** | 7/16 = **44%** |

## Source text

Gruppchef inom Landskapsarkitektur till Stockholm Vill du jobba i ett internationellt företag under tillväxt? Nu utökar vi och söker en gruppchef inom landskapsarkitektur! Om jobbet Som gruppchef kommer du att ha personalansvar för två landskapsarkitekter i Stockholm, en grupp du kommer att vara med och utöka. Tillsammans med regionchef och i samarbete med dina gruppchefskollegor inom väg & gata och miljö kommer du att fortsätta utveckla och bygga upp verksamheten. Som alla våra chefer kommer du också att arbeta i uppdrag inom ditt område. Att leda och utveckla din personal är viktiga delar i ditt arbete såväl som att utveckla verksamheten framåt. Som gruppchef kommer du att ansvara för: resursplanering marknad och anbud inom ditt område rekrytering och utveckling av dina medarbetare.  Du kommer ha ett nära samarbete med våra övriga kontor och resor i tjänsten förekommer emellanåt. Om dig Vi söker dig som trivs med ett arbete där du får möjlighet att utveckla arbetet och verksamheten framåt. Genom ditt ledarskap får du andra att utvecklas. Du är inspirerande och bidrar till nytänkande och förändring. Ditt sätt att möta människor med värme, energi och respekt skapar entusiasm och arbetsglädje hos dina medarbetare, kollegor och kunder. Vi söker dig som har goda kunskaper inom landskapsarkitektur gärna med fokus på transportinfrastruktur. Vi tror att du har en eftergymnasial utbildning inom landskapsarkitektur och att du har arbetat minst fem år inom området. Vi ser gärna att du haft en ledande roll och har du arbetet som chef är det en merit. Du arbetar bra tillsammans med andra samtidigt som du har förmåga att arbeta självständigt. Du trivs i en roll där du får dela med dig av din kunskap och fungera som coach åt andra. Vårt erbjudande Vi erbjuder en möjlighet att vara med och utveckla verksamheten inom landskapsarkitektur i både Stockholm och resten av Sverige. Vi är ett kraftigt expanderande internationellt företag. Du blir en viktig del av vår verksamhet, vi är öppna för nya idéer och du har stora möjligheter att påverka utvecklingen framåt. Du kommer att få möjlighet att jobba med kollegor i hela Sverige såväl som i andra länder. Du utgår från vårt kontor i Kista science Tower där vi sitter tillsammans med kollegor både inom det egna och övriga affärsområden. Hos oss är du med och utvecklar innovativa och hållbara lösningar. På SYSTRA deltar du i skapandet av ett hållbart samhälle genom att arbeta i en organisation som tar ansvar för miljön, projekten, medarbetarna och som skapar verklighet av sina visioner. På SYSTRA vet vi att det är medarbetarnas olikheter som får organisationen att växa. Vi lägger därför stor vikt vid dina personliga egenskaper och eftersträvar en mångfald på arbetsplatsen. Om SYSTRA SYSTRA är ett internationellt företag med 9 000 anställda runtom i världen. Vi är en av världens ledande aktörer inom infrastrukturlösningar inom transportinfrastruktur så som höghastighetsjärnväg, tunnelbana, spårväg, väg & trafik. Vi har bland annat designat 50 procent av världens byggda och planerade höghastighetsjärnvägar, automatiserade metro och tunnelbanor. SYSTRA AB har funnits i sin nuvarande form sedan 2018 och har under den perioden vuxit från 50 till 250 medarbetare, en expansion vi ser kommer att fortsätta de närmaste åren. Vi är idag 8 medarbetare inom området landskapsarkitektur i Sverige. Läs gärna mer om oss på vår hemsida www.systra.se. Ansökan Varmt välkommen med din ansökan. Urval kan komma att ske löpande. Du ansöker om jobbet här: https://jobs.systra.com/job/Stockholm-Gruppchef-inom-Landskapsarkitektur-till-Stockholm/814117301/ Välkommen att höra av dig om du är intresserad och vill veta mer Regionchef Civil Öst & Mitt Mattias Källander 070-395 69 36 mkallander@systra.com Inför rekryteringsarbetet har SYSTRA noggrant tagit ställning till rekryteringskanaler och marknadsföring. Vi undanber oss därför bestämt kontakt med mediesäljare, rekryteringssajter eller liknande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Gruppchef inom **Landskapsarkitektur** till Stockholm Vill du jobba ... | x | x | 19 | 19 | [landskapsarkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VryZ_dNm_o4a) |
| 2 | ...inom Landskapsarkitektur till **Stockholm** Vill du jobba i ett internati... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 3 | ...i och söker en gruppchef inom **landskapsarkitektur**! Om jobbet Som gruppchef komm... | x | x | 19 | 19 | [landskapsarkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VryZ_dNm_o4a) |
| 4 | ...om gruppchef kommer du att ha **personalansvar** för två landskapsarkitekter i... | x | x | 14 | 14 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| 5 | ...att ha personalansvar för två **landskapsarkitekter** i Stockholm, en grupp du komm... |  | x |  | 19 | [Landskapsarkitekter, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/2f9G_ES5_3Bm) |
| 5 | ...att ha personalansvar för två **landskapsarkitekter** i Stockholm, en grupp du komm... | x | x | 19 | 19 | [Landskapsarkitekter, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/ETAR_ggZ_Wuw) |
| 5 | ...att ha personalansvar för två **landskapsarkitekter** i Stockholm, en grupp du komm... |  | x |  | 19 | [Landskapsarkitekt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/SXAF_LMk_j4S) |
| 5 | ...att ha personalansvar för två **landskapsarkitekter** i Stockholm, en grupp du komm... |  | x |  | 19 | [landskapsarkitekt, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/kHrh_P7k_rLy) |
| 6 | ...för två landskapsarkitekter i **Stockholm**, en grupp du kommer att vara ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 7 | ...de. Att leda och utveckla din **personal** är viktiga delar i ditt arbet... | x |  |  | 8 | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| 8 | ...ära samarbete med våra övriga **kontor** och resor i tjänsten förekomm... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 9 | ...te med våra övriga kontor och **resor i tjänsten förekommer** emellanåt. Om dig Vi söker di... | x |  |  | 27 | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| 10 | ...g som har goda kunskaper inom **landskapsarkitektur** gärna med fokus på transporti... | x | x | 19 | 19 | [landskapsarkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VryZ_dNm_o4a) |
| 11 | ...ruktur. Vi tror att du har en **eftergymnasial utbildning** inom landskapsarkitektur och ... | x | x | 25 | 25 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 12 | ...ftergymnasial utbildning inom **landskapsarkitektur** och att du har arbetat minst ... | x | x | 19 | 19 | [landskapsarkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VryZ_dNm_o4a) |
| 13 | ...kapsarkitektur och att du har **arbetat minst fem år** inom området. Vi ser gärna at... | x |  |  | 20 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 14 | ...tidigt som du har förmåga att **arbeta självständigt**. Du trivs i en roll där du få... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ...ch utveckla verksamheten inom **landskapsarkitektur** i både Stockholm och resten a... | x | x | 19 | 19 | [landskapsarkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VryZ_dNm_o4a) |
| 16 | ...om landskapsarkitektur i både **Stockholm** och resten av Sverige. Vi är ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 17 | ... både Stockholm och resten av **Sverige**. Vi är ett kraftigt expandera... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 18 | ...att jobba med kollegor i hela **Sverige** såväl som i andra länder. Du ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 19 | ...ra länder. Du utgår från vårt **kontor** i Kista science Tower där vi ... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 20 | ...astighetsjärnväg, tunnelbana, **spårväg**, väg & trafik. Vi har bland a... |  | x |  | 7 | [trafikledare, spårväg, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/XXvn_JRM_eMu) |
| 21 | ...ag 8 medarbetare inom området **landskapsarkitektur** i Sverige. Läs gärna mer om o... | x | x | 19 | 19 | [landskapsarkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VryZ_dNm_o4a) |
| 22 | ...området landskapsarkitektur i **Sverige**. Läs gärna mer om oss på vår ... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 23 | ... till rekryteringskanaler och **marknadsföring**. Vi undanber oss därför bestä... | x |  |  | 14 | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| | **Overall** | | | **240** | **385** | 240/385 = **62%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Landskapsarkitekter, **isco-level-4**](http://data.jobtechdev.se/taxonomy/concept/2f9G_ES5_3Bm) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Landskapsarkitekter, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/ETAR_ggZ_Wuw) |
| x |  |  | [Personal, **keyword**](http://data.jobtechdev.se/taxonomy/concept/HVxM_6MS_y1c) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Landskapsarkitekt, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/SXAF_LMk_j4S) |
| x | x | x | [landskapsarkitektur, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/VryZ_dNm_o4a) |
|  | x |  | [trafikledare, spårväg, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/XXvn_JRM_eMu) |
| x | x | x | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| x | x | x | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [landskapsarkitekt, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/kHrh_P7k_rLy) |
| x |  |  | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| x |  |  | [Marknadsföring, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/zTc5_e4U_Qsf) |
| | | **7** | 7/16 = **44%** |