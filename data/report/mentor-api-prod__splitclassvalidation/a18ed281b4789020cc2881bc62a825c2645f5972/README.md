# Results for 'a18ed281b4789020cc2881bc62a825c2645f5972'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [a18ed281b4789020cc2881bc62a825c2645f5972](README.md) | 1 | 732 | 5 | 4 | 53/70 = **76%** | 3/4 = **75%** |

## Source text

Specialistläkare, Bemanning Jämför och chatta med bemanningsbolagen enkelt via Vårdföretag App (vardforetag.se) Skapa din profil på mindre än 1 minut och lägg upp dina önskemål. Få erbjudanden på uppdrag som kan passa dina önskemål och chatta med flera av de ledande bemanningsbolagen inom vården. Enkelt och kostnadsfritt! Just nu finns ett ledigt uppdrag som specialistläkare på kvinnoklinik i Eskilstuna, Södermanlands län. Uppdraget finns att söka bland flera bemanningsbolag i Vårdföretag.se   Fördelar: -Hög ersättning -Inga onödiga Mail Eller telefonsamtal -Enkelt och kostnadsfritt -Inget bindande   Vill du starta eget aktiebolag? Vi på vardforetag.se hjälper dig att starta aktiebolag och redovisning till fasta kostnader.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Specialistläkare**, Bemanning Jämför och chatta ... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 2 | ... finns ett ledigt uppdrag som **specialistläkare** på kvinnoklinik i Eskilstuna,... | x | x | 16 | 16 | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| 3 | ...alistläkare på kvinnoklinik i **Eskilstuna**, Södermanlands län. Uppdraget... | x | x | 10 | 10 | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| 4 | ...på kvinnoklinik i Eskilstuna, **Södermanlands län**. Uppdraget finns att söka bla... | x |  |  | 17 | [Södermanlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/s93u_BEb_sx2) |
| 5 | ...dig att starta aktiebolag och **redovisning** till fasta kostnader. | x | x | 11 | 11 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| | **Overall** | | | **53** | **70** | 53/70 = **76%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| x | x | x | [Eskilstuna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kMxr_NiX_YrU) |
| x | x | x | [Specialistläkare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oHyy_M2j_HUP) |
| x |  |  | [Södermanlands län, **region**](http://data.jobtechdev.se/taxonomy/concept/s93u_BEb_sx2) |
| | | **3** | 3/4 = **75%** |