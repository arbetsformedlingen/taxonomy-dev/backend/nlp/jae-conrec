# Results for 'fc913e4787bb4a34dcf088735f2cbd5bae3ea34e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [fc913e4787bb4a34dcf088735f2cbd5bae3ea34e](README.md) | 1 | 1853 | 15 | 42 | 84/553 = **15%** | 6/38 = **16%** |

## Source text

Junior embedded utvecklare till NFO Drives! NFO Drives AB söker nu dig som är nyexaminerad civilingenjör och vill axla rollen som embedded utvecklare. Du kommer jobba en utmanande och teknisk miljö och få möjligheten att utveckla din kunskapsnivå inom embedded.  NFO Drives är ett mindre svenskt bolag placerat i Karlshamn. De är ett cleantech-företag, som jobbar med utveckling och tillverkning av produkter för energi­effektivisering av elmotorer till maskiner, fläktar och pumpar med mera. De är idag börsnoterade på Spotlight Stockmarket!  Om dig  För denna roll söker vi dig som har en civilingenjörsexamen inom elektroteknik, data, teknisk fysik eller motsvarande relevant inriktning, eller som tar examen inom de ämnena nu i sommar. Du har ett stort intresse för embedded programmering, specifikt embedded C.  Som person är du social och utåtriktad. Du trivs i dialog med både kunder och kollegor. Du är självgående i ditt arbete och är inte rädd för att ta egna initiativ.  Arbetsuppgifter  Din huvudsakliga arbetsuppgift blir att utveckla nya funktioner i NFO Drives frekvensomriktare, vilket innebär embedded SW med mycket reglerteknik. Du kommer jobba med utvecklingsteamet med att bygga upp lab och testa nya funktioner. Vid din sida kommer du ha en specialist som agerar mentor till dig, men du förväntas att arbeta självständigt.  Tjänsten  Detta är ett konsultuppdrag, eller gig som vi kallar det på Gigstep. Du blir inledningsvis anställd av Gigstep med ambition att det ska leda till en anställning direkt hos NFO Drives. Arbetet kommer i huvudsak att utföras i NFO Drives lokaler i Karlshamn, därför är det viktigt att du bor eller vill bo i Blekinge. Startdatum är tänkt vara nu i vår men det kan vara flexibilitet för rätt kandidat med en grym inställning.  Är du den utåtriktade & tekniska utvecklaren vi letar efter? Sök nu direkt!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Junior **embedded** utvecklare till NFO Drives! N... |  | x |  | 8 | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| 1 | Junior **embedded** utvecklare till NFO Drives! N... |  | x |  | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 2 | Junior **embedded utvecklare** till NFO Drives! NFO Drives A... | x |  |  | 19 | [Embeddedutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/sEFD_3Zs_WiA) |
| 3 | ...er nu dig som är nyexaminerad **civilingenjör** och vill axla rollen som embe... |  | x |  | 13 | [Civilingenjörsyrken, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/4jZs_twz_epW) |
| 3 | ...er nu dig som är nyexaminerad **civilingenjör** och vill axla rollen som embe... | x |  |  | 13 | [Övriga civilingenjörsyrken, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/mp2Y_vyC_RFV) |
| 4 | ...njör och vill axla rollen som **embedded** utvecklare. Du kommer jobba e... |  | x |  | 8 | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| 4 | ...njör och vill axla rollen som **embedded** utvecklare. Du kommer jobba e... |  | x |  | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 5 | ...njör och vill axla rollen som **embedded utvecklare**. Du kommer jobba en utmanande... | x |  |  | 19 | [Embeddedutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/sEFD_3Zs_WiA) |
| 6 | ...tveckla din kunskapsnivå inom **embedded**.  NFO Drives är ett mindre sv... |  | x |  | 8 | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| 6 | ...tveckla din kunskapsnivå inom **embedded**.  NFO Drives är ett mindre sv... |  | x |  | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 7 | ...ndre svenskt bolag placerat i **Karlshamn**. De är ett cleantech-företag,... | x | x | 9 | 9 | [Karlshamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/HtGW_WgR_dpE) |
| 8 | ...som jobbar med utveckling och **tillverkning** av produkter för energi­effek... | x | x | 12 | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 9 | ...g av elmotorer till maskiner, **fläktar** och pumpar med mera. De är id... |  | x |  | 7 | [sköta fläkt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fAaJ_YCG_vGP) |
| 9 | ...g av elmotorer till maskiner, **fläktar** och pumpar med mera. De är id... |  | x |  | 7 | [Maskinskötsel, metall- och mineralindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/myAP_hPr_RFe) |
| 10 | ... roll söker vi dig som har en **civilingenjörsexamen** inom elektroteknik, data, tek... | x | x | 20 | 20 | [Civilingenjörsexamen, **skill**](http://data.jobtechdev.se/taxonomy/concept/ND2K_ivB_6RA) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Högskoleingenjörsutbildning, elektroteknik och elektronik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/9RnM_HtM_S5g) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Högskoleingenjörsutbildning, elektroteknik och ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/CLZG_riA_sNF) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Ingenjörer och tekniker inom elektroteknik, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/DSZ8_GiW_dVy) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Yrkesteknisk högskoleutbildning,  elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KTPX_3gJ_Uud) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Ingenjörsutbildning, energi- och elektroteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/P2vF_zbH_Nd8) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Elektroteknik (generell examen), **keyword**](http://data.jobtechdev.se/taxonomy/concept/Qcoz_HMf_oa2) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [tekniker, elektroteknik, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Qhc6_RGL_yJ5) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Civilingenjörsyrken inom elektroteknik, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/SPYW_7Z1_ShT) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Civilingenjörsutbildning, elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Vzg5_p73_x7u) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Annan utbildning i energi- och elektroteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/deFk_Dyb_tmV) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Ingenjörer och tekniker inom elektroteknik, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/nDaB_vdy_eAy) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Civilingenjörsutbildning, elektroteknik och elektronik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/sfv8_adf_ALj) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... | x | x | 13 | 13 | [elektroteknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tohW_adM_nh4) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Högskoleingenjörsutbildning, data- och elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/uSZs_cM9_S8E) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Civilingenjörsutbildning, energi- och elektroteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/uY1H_ZvY_FHg) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Civilingenjörer inom elektroteknik, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/w88C_jP2_5Jq) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Energi- och elektroteknik, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/wuXX_ZG5_31i) |
| 11 | ... en civilingenjörsexamen inom **elektroteknik**, data, teknisk fysik eller mo... |  | x |  | 13 | [Civilingenjörsutbildning, teknisk fysik och elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yFJ4_6SC_DN3) |
| 12 | ...örsexamen inom elektroteknik, **data**, teknisk fysik eller motsvara... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 13 | ...men inom elektroteknik, data, **teknisk fysik** eller motsvarande relevant in... | x | x | 13 | 13 | [Teknisk fysik, **skill**](http://data.jobtechdev.se/taxonomy/concept/meMN_wmY_RPf) |
| 14 | ...ant inriktning, eller som tar **examen** inom de ämnena nu i sommar. D... | x |  |  | 6 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 15 | ...Du har ett stort intresse för **embedded** programmering, specifikt embe... |  | x |  | 8 | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| 15 | ...Du har ett stort intresse för **embedded** programmering, specifikt embe... |  | x |  | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 16 | ...Du har ett stort intresse för **embedded programmering**, specifikt embedded C.  Som p... | x |  |  | 22 | [Embeddedprogrammerare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1EJH_WhT_PGj) |
| 17 | ...t stort intresse för embedded **programmering**, specifikt embedded C.  Som p... |  | x |  | 13 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 18 | ...rammering, specifikt embedded **C**.  Som person är du social och... | x |  |  | 1 | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| 19 | ...vensomriktare, vilket innebär **embedded** SW med mycket reglerteknik. D... |  | x |  | 8 | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
| 19 | ...vensomriktare, vilket innebär **embedded** SW med mycket reglerteknik. D... |  | x |  | 8 | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
| 20 | ...nnebär embedded SW med mycket **reglerteknik**. Du kommer jobba med utveckli... |  | x |  | 12 | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
| 20 | ...nnebär embedded SW med mycket **reglerteknik**. Du kommer jobba med utveckli... | x |  |  | 12 | [Reglerteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/d7m6_Nh4_LLF) |
| 21 | ...ill dig, men du förväntas att **arbeta självständigt**.  Tjänsten  Detta är ett kons... |  | x |  | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 22 | ...tföras i NFO Drives lokaler i **Karlshamn**, därför är det viktigt att du... | x | x | 9 | 9 | [Karlshamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/HtGW_WgR_dpE) |
| 23 | ...gt att du bor eller vill bo i **Blekinge**. Startdatum är tänkt vara nu ... | x | x | 8 | 8 | [Blekinge län, **region**](http://data.jobtechdev.se/taxonomy/concept/DQZd_uYs_oKb) |
| | **Overall** | | | **84** | **553** | 84/553 = **15%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Embeddedprogrammerare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/1EJH_WhT_PGj) |
|  | x |  | [Embedded-sw-utvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/2M8s_Xwu_Yek) |
|  | x |  | [Civilingenjörsyrken, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/4jZs_twz_epW) |
|  | x |  | [Högskoleingenjörsutbildning, elektroteknik och elektronik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/9RnM_HtM_S5g) |
|  | x |  | [Högskoleingenjörsutbildning, elektroteknik och ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/CLZG_riA_sNF) |
| x | x | x | [Blekinge län, **region**](http://data.jobtechdev.se/taxonomy/concept/DQZd_uYs_oKb) |
|  | x |  | [Ingenjörer och tekniker inom elektroteknik, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/DSZ8_GiW_dVy) |
| x |  |  | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| x | x | x | [Karlshamn, **municipality**](http://data.jobtechdev.se/taxonomy/concept/HtGW_WgR_dpE) |
|  | x |  | [Yrkesteknisk högskoleutbildning,  elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/KTPX_3gJ_Uud) |
| x | x | x | [Civilingenjörsexamen, **skill**](http://data.jobtechdev.se/taxonomy/concept/ND2K_ivB_6RA) |
|  | x |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Ingenjörsutbildning, energi- och elektroteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/P2vF_zbH_Nd8) |
|  | x |  | [Elektroteknik (generell examen), **keyword**](http://data.jobtechdev.se/taxonomy/concept/Qcoz_HMf_oa2) |
|  | x |  | [tekniker, elektroteknik, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Qhc6_RGL_yJ5) |
|  | x |  | [Civilingenjörsyrken inom elektroteknik, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/SPYW_7Z1_ShT) |
|  | x |  | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
|  | x |  | [Civilingenjörsutbildning, elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Vzg5_p73_x7u) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Reglerteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/d7m6_Nh4_LLF) |
|  | x |  | [Annan utbildning i energi- och elektroteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/deFk_Dyb_tmV) |
|  | x |  | [sköta fläkt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/fAaJ_YCG_vGP) |
|  | x |  | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x | x | x | [Teknisk fysik, **skill**](http://data.jobtechdev.se/taxonomy/concept/meMN_wmY_RPf) |
| x |  |  | [Övriga civilingenjörsyrken, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/mp2Y_vyC_RFV) |
|  | x |  | [Maskinskötsel, metall- och mineralindustri, **skill**](http://data.jobtechdev.se/taxonomy/concept/myAP_hPr_RFe) |
|  | x |  | [Ingenjörer och tekniker inom elektroteknik, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/nDaB_vdy_eAy) |
| x |  |  | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| x |  |  | [Embeddedutvecklare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/sEFD_3Zs_WiA) |
|  | x |  | [Civilingenjörsutbildning, elektroteknik och elektronik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/sfv8_adf_ALj) |
| x | x | x | [elektroteknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/tohW_adM_nh4) |
| x | x | x | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
|  | x |  | [Högskoleingenjörsutbildning, data- och elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/uSZs_cM9_S8E) |
|  | x |  | [Civilingenjörsutbildning, energi- och elektroteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/uY1H_ZvY_FHg) |
|  | x |  | [Civilingenjörer inom elektroteknik, **forecast-occupation**](http://data.jobtechdev.se/taxonomy/concept/w88C_jP2_5Jq) |
|  | x |  | [Energi- och elektroteknik, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/wuXX_ZG5_31i) |
|  | x |  | [inbäddade system, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/xbkg_6TH_up9) |
|  | x |  | [Civilingenjörsutbildning, teknisk fysik och elektroteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yFJ4_6SC_DN3) |
| | | **6** | 6/38 = **16%** |