# Results for '896887092a16ebf2a624c21c76134edb8ffff5a1'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [896887092a16ebf2a624c21c76134edb8ffff5a1](README.md) | 1 | 2393 | 13 | 14 | 73/261 = **28%** | 4/15 = **27%** |

## Source text

Montörer sökes till Pheenix Alpha! Har du jobbat som Montör tidigare och letar efter nya möjligheter? Trivs du i en produktionsmiljö och vill utvecklas?  Sök denna tjänst idag!  Om tjänsten Vi söker nu montörer till ett spännande projekt där du kommer montera ihop detaljer till mer avancerade komponenter samt slut montering av en pressmaskin för LP skivor.  Du kommer att säkerställa att alla delar passar korrekt och fungerar som de ska. Dina händer är främsta hjälpmedlet men kommer också använda maskiner vid större montage och få till rätt precision.     Dina arbetsuppgifter i huvudsak • Du kommer vara en del av monteringsprocessen där fokus är på noggrannhet och  precision. • Läs och förstå instruktioner och följ fastställda procedurer • Gör precisionsmätningar för att säkerställa perfekt passform av komponenter • Rikta in material och sätt ihop delar för att bygga mer komplexa enheter • Rapportera om problem, fel eller defekta delar   Vem är du? Vi söker dig som har minst tre års erfarenhet av montering samt ritningsläsning. Det är viktigt att du håller ett högt tempo, är noggrann och har arbetsvilja och en nyfikenhet inför dina arbetsuppgifter.  • Meriterande för tjänsten är truckkort A+B men detta är inget krav.  • God förståelse för kvalitetskontroll principer • Utåtriktad, lagspelare • Uppskattar högt tempo • Strukturerad och planeringsförmåga    Om verksamheten OBS Just nu sitter dem i Bromma men kommer flytta ut till Skepptuna Borgen 299 Märsta i Q4.     Eventuella namngivna referenter bör ha kännedom om att de finns i dina ansökningshandlingar.   Uniflex är ett auktoriserat bemanningsföretag som arbetar med uthyrning och rekrytering. Uniflex finns i Sverige, Norge och Finland. I Sverige är vi drygt 3 000 anställda och finns på ett 40 tal orter. Vi är medlemmar i Almega Kompetensföretagen samt Byggföretagen och har kollektivavtal med LO, Byggnads, Unionen och Akademikerförbunden. Uniflex är kvalitets-, miljö- och arbetsmiljöcertifierade enligt ISO 9001, ISO 14001 samt ISO 45001.  På Uniflex ser vi våra medarbetare som vår viktigaste tillgång och därför erbjuder vi följande:  • Kollektivavtal • Tjänstepension • Friskvårdsbidrag och andra personalförmåner • Försäkringar • Marknadsmässig lön • Karriär och utveckling  Vi söker ständigt efter nya kollegor som arbetar i enighet med våra värderingar affärsfokus, engagemang, glädje och ansvarstagande.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Montörer** sökes till Pheenix Alpha! Har... | x | x | 8 | 8 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 2 | ...enix Alpha! Har du jobbat som **Montör** tidigare och letar efter nya ... | x | x | 6 | 6 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 3 | ...dag!  Om tjänsten Vi söker nu **montörer** till ett spännande projekt dä... | x | x | 8 | 8 | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| 4 | ...ingsprocessen där fokus är på **noggrannhet** och  precision. • Läs och för... | x | x | 11 | 11 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 5 | ...noggrannhet och  precision. • **Läs och förstå instruktioner** och följ fastställda procedur... | x |  |  | 28 | [följa en instruktion, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i7Gi_HpC_RTF) |
| 6 | ... fastställda procedurer • Gör **precisionsmätningar** för att säkerställa perfekt p... | x |  |  | 19 | [precisionsmätinstrument, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4Z9w_F2A_Pxb) |
| 6 | ... fastställda procedurer • Gör **precisionsmätningar** för att säkerställa perfekt p... |  | x |  | 19 | [använda utrustning för precisionsmätning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cA7S_2dQ_rrC) |
| 7 | ... bygga mer komplexa enheter • **Rapportera om problem**, fel eller defekta delar   Ve... | x |  |  | 21 | [Allmän rapportering, **skill**](http://data.jobtechdev.se/taxonomy/concept/vGPg_Tih_VSw) |
| 8 | ...m är du? Vi söker dig som har **minst tre års erfarenhet** av montering samt ritningsläs... | x |  |  | 24 | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
| 9 | ... erfarenhet av montering samt **ritningsläsning**. Det är viktigt att du håller... | x | x | 15 | 15 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 10 | ... du håller ett högt tempo, är **noggrann** och har arbetsvilja och en ny... | x | x | 8 | 8 | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| 11 | ...• Meriterande för tjänsten är **truckkort** A+B men detta är inget krav. ... |  | x |  | 9 | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| 12 | ...• Meriterande för tjänsten är **truckkort A**+B men detta är inget krav.  •... | x |  |  | 11 | [A-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Zk4h_jP4_e85) |
| 13 | ...e för tjänsten är truckkort A+**B** men detta är inget krav.  • G... | x |  |  | 1 | [B-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/HeXq_amc_ABd) |
| 14 | ...t krav.  • God förståelse för **kvalitetskontroll** principer • Utåtriktad, lagsp... | x | x | 17 | 17 | [Kvalitetskontroll, **skill**](http://data.jobtechdev.se/taxonomy/concept/4y7C_Lsh_EPM) |
| 15 | ... rekrytering. Uniflex finns i **Sverige**, Norge och Finland. I Sverige... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 16 | ...Sverige, Norge och Finland. I **Sverige** är vi drygt 3 000 anställda o... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 17 | ...en samt Byggföretagen och har **kollektivavtal** med LO, Byggnads, Unionen och... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 18 | ...rför erbjuder vi följande:  • **Kollektivavtal** • Tjänstepension • Friskvårds... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 19 | ...fokus, engagemang, glädje och **ansvarstagande**. |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| | **Overall** | | | **73** | **261** | 73/261 = **28%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [precisionsmätinstrument, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/4Z9w_F2A_Pxb) |
| x | x | x | [Kvalitetskontroll, **skill**](http://data.jobtechdev.se/taxonomy/concept/4y7C_Lsh_EPM) |
| x |  |  | [B-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/HeXq_amc_ABd) |
| x |  |  | [2-4 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) |
|  | x |  | [Truck, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Wz82_4Xv_ZFr) |
| x | x | x | [Montörer, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/Wz8B_WeK_vUB) |
| x |  |  | [A-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/Zk4h_jP4_e85) |
| x | x | x | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [använda utrustning för precisionsmätning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cA7S_2dQ_rrC) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [följa en instruktion, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i7Gi_HpC_RTF) |
| x | x | x | [noggrannhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mrsW_EkL_KRD) |
| x |  |  | [Allmän rapportering, **skill**](http://data.jobtechdev.se/taxonomy/concept/vGPg_Tih_VSw) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **4** | 4/15 = **27%** |