# Results for 'f82658463b3e8d149b74a33102e110bb10cf79f5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [f82658463b3e8d149b74a33102e110bb10cf79f5](README.md) | 1 | 3093 | 10 | 14 | 28/272 = **10%** | 3/17 = **18%** |

## Source text

SSAB - Driftassistent Betsträcka 2 Vill du ha ett spännande, omväxlande och ansvarstagande arbete i en miljö som präglas av kreativitet? Gillar du att kontinuerligt arbeta med att förbättra säkerhet, kvalitet och tillgänglighet i produktionslinjen genom ett aktivt förbättringsarbete? Sök då denna tjänst som Driftassistent. Placeringsort Borlänge. Som driftassistent vid Betsträcka 2 kommer du tillsammans med skiftlag och ledningsteam att kontinuerligt arbeta med att förbättra säkerhet, kvalitet och tillgänglighet i linjen genom ett aktivt förbättringsarbete. Du ingår i ledningsteamet för Betsträcka 2. Kallvalsverket består av Avsnitten Betning & Emballering, Valsning & Spaltning samt Glödgning. Vi producerar betat, kallvalsat och glödgat material till interna och externa kunder. Betsträcka 2, som ingår i avsnittet Betning & Emballering, är en kontinuerlig processlinje för betning av plåt. Utvecklingen mot nya material med högre hållfasthet ger en utmanande uppgift i arbetet att ständigt förbättra kvalitet, produktivitet och tillgänglighet i linjerna. Vår ambition är att göra detta parallellt med att vi utvecklar medarbetare och organisation mot ett säkert och utvecklande arbete. Din uppgift som driftassistent vid Betsträcka 2 blir att koordinera den dagliga verksamheten i arbetsområdet samt att arbeta för att ökad tillförlitlighet och kvalitet nås till budgeterad kostnad och på ett säkert sätt. Drivandet av SSABs förbättringsarbete och utveckling av det grupporganiserade arbetssättet är centralt. I vardagen arbetar du tätt tillsammans med skiftlagen, ledningsteamet, processutvecklare, kvalitetstekniker samt med medarbetare från driftnära underhåll. Du deltar även i planering och genomförande av olika projekt. Arbetet ger stora möjligheter till personlig utveckling och ger dig möjlighet att växa med uppgiften. Tjänsten är en tillsvidareanställning. Dina kvalifikationer Vi fäster stor vikt vid dina personliga egenskaper såsom ansvarskänsla, initiativförmåga, samarbetsförmåga samt att du är systematisk och medskapande. Du har lätt för att kommunicera med människor på olika nivåer och i olika roller. Du har ett intresse för teknik, människor och arbetssätt och vill lära dig nya saker. Vi ser gärna att du har gymnasieutbildning alt. praktiskt erfarenhet, datorvana och att du har en god förmåga att uttrycka dig i tal och skrift. Vi erbjuder Ett spännande, omväxlande och ansvarstagande arbete i en miljö som präglas av kreativitet. I samband med den här rekryteringen har vi möjlighet att erbjuda medflyttarservice till dig och din familj om ni flyttar till Dalarna. Läs mer på Medflyttarservice - Rekryteringslots Dalarna Sista ansökningsdag 14 augusti 2022 Vid frågor om tjänsten kontakta gärna Mikael Larsson, chef sektion Betsträcka 2, tfn 0243-710 87 Facklig information lämnas av Akademikerföreningen, Unionen och Ledarna som nås via vår växel på tfn 0243-700 00. Bakgrundskontroll kan komma att genomföras som en del i rekryteringsprocessen. Vi undanber oss kontakter från rekryteringsföretag, annonssäljare och liknande beträffande denna annonsering.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | SSAB - **Driftassistent** Betsträcka 2 Vill du ha ett s... | x |  |  | 14 | [Produktionsassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Ym1L_d8s_cad) |
| 2 | ...ett spännande, omväxlande och **ansvarstagande** arbete i en miljö som präglas... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 3 | ...bete? Sök då denna tjänst som **Driftassistent**. Placeringsort Borlänge. Som ... | x |  |  | 14 | [Produktionsassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Ym1L_d8s_cad) |
| 4 | ...Driftassistent. Placeringsort **Borlänge**. Som driftassistent vid Betst... | x | x | 8 | 8 | [Borlänge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cpya_jJg_pGp) |
| 5 | ...nitten Betning & Emballering, **Valsning** & Spaltning samt Glödgning. V... |  | x |  | 8 | [Smidning, pressning, prägling och valsning av metall; pulvermetallurgi, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/4BUz_XUD_FrE) |
| 5 | ...nitten Betning & Emballering, **Valsning** & Spaltning samt Glödgning. V... |  | x |  | 8 | [Smidning, pressning, prägling och valsning av metall; pulvermetallurgi, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/GsRk_ged_Hc8) |
| 5 | ...nitten Betning & Emballering, **Valsning** & Spaltning samt Glödgning. V... |  | x |  | 8 | [Smidning, pressning, prägling och valsning av metall; pulvermetallurgi, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/X5c2_arb_orY) |
| 6 | ...ng, Valsning & Spaltning samt **Glödgning**. Vi producerar betat, kallval... |  | x |  | 9 | [Glödgning, **skill**](http://data.jobtechdev.se/taxonomy/concept/cmqD_ZW4_nLQ) |
| 7 | ...d skiftlagen, ledningsteamet, **processutvecklare**, kvalitetstekniker samt med m... |  | x |  | 17 | [Processutvecklare, kemi, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/5ssa_t3w_Ghh) |
| 8 | ...ngsteamet, processutvecklare, **kvalitetstekniker** samt med medarbetare från dri... |  | x |  | 17 | [kvalitetstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ew7i_cMD_8eg) |
| 9 | ...et ger stora möjligheter till **personlig utveckling** och ger dig möjlighet att väx... |  | x |  | 20 | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
| 10 | ...med uppgiften. Tjänsten är en **tillsvidareanställning**. Dina kvalifikationer Vi fäst... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 11 | ...a personliga egenskaper såsom **ansvarskänsla**, initiativförmåga, samarbetsf... | x | x | 13 | 13 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 12 | ...varskänsla, initiativförmåga, **samarbetsförmåga** samt att du är systematisk oc... | x |  |  | 16 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 13 | ...skapande. Du har lätt för att **kommunicera** med människor på olika nivåer... | x |  |  | 11 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 14 | ...aker. Vi ser gärna att du har **gymnasieutbildning** alt. praktiskt erfarenhet, da... | x |  |  | 18 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 14 | ...aker. Vi ser gärna att du har **gymnasieutbildning** alt. praktiskt erfarenhet, da... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 15 | ...ng alt. praktiskt erfarenhet, **datorvana** och att du har en god förmåga... | x |  |  | 9 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 16 | ...Ett spännande, omväxlande och **ansvarstagande** arbete i en miljö som präglas... |  | x |  | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 17 | ...din familj om ni flyttar till **Dalarna**. Läs mer på Medflyttarservice... | x | x | 7 | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| 18 | ...tarservice - Rekryteringslots **Dalarna** Sista ansökningsdag 14 august... |  | x |  | 7 | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| | **Overall** | | | **28** | **272** | 28/272 = **10%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Smidning, pressning, prägling och valsning av metall; pulvermetallurgi, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/4BUz_XUD_FrE) |
|  | x |  | [Processutvecklare, kemi, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/5ssa_t3w_Ghh) |
|  | x |  | [Smidning, pressning, prägling och valsning av metall; pulvermetallurgi, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/GsRk_ged_Hc8) |
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
|  | x |  | [Smidning, pressning, prägling och valsning av metall; pulvermetallurgi, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/X5c2_arb_orY) |
| x |  |  | [Produktionsassistent, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Ym1L_d8s_cad) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Glödgning, **skill**](http://data.jobtechdev.se/taxonomy/concept/cmqD_ZW4_nLQ) |
| x | x | x | [Borlänge, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cpya_jJg_pGp) |
|  | x |  | [kvalitetstekniker, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/ew7i_cMD_8eg) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x | x | x | [Dalarnas län, **region**](http://data.jobtechdev.se/taxonomy/concept/oDpK_oZ2_WYt) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
|  | x |  | [personlig utveckling, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/wkQj_Dah_2GD) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| | | **3** | 3/17 = **18%** |