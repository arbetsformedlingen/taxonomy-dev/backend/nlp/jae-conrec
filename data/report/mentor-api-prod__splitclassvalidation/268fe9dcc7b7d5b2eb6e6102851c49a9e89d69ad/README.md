# Results for '268fe9dcc7b7d5b2eb6e6102851c49a9e89d69ad'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [268fe9dcc7b7d5b2eb6e6102851c49a9e89d69ad](README.md) | 1 | 2196 | 19 | 23 | 60/450 = **13%** | 5/23 = **22%** |

## Source text

Erfarna handläggande byggnadskonstruktörer  sökes! ARBETSUPPGIFTER;   Vår kund behöver stärka upp sin organisation inom Byggprojektering. Vi söker kandidater som är självgående och redan idag handlägger mindre till medelstora uppdrag inom allmän konstruktion. Vår kund arbetar brett inom husbyggnad med allt från grundläggningar och stommar till fasader och tak. Uppdragen återfinns inom ny-, om och tillbyggnad.   Huvudsakliga arbetsuppgifter;  • Intern projektledare för uppdragen • Leda och planera uppdragen tillsammans med Uppdragsansvarig • Säkerställa vår värdegrund i uppdragen – Engagemang och Excellens   UTBILDNING;  • Högskoleutbildning minst 3år med Byggkonstruktionsinriktning eller likvärdigt, eller minst 5 års erfarenhet av konsultuppdrag inom byggnadskonstruktion med allmän inriktning.   ERFARENHET;  • Minst 5 års erfarenhet av allmän konstruktion husbyggnad • Nybyggnad, om- och tillbyggnad, grundläggning, stomkomplettering, stomme • Revit och Autocad  • Träbyggnadskompetens/kunskap är meriterande • Certifieringar är meriterande • Minst Handläggare för mindre uppdrag (>150 kkr) • Varit handläggare i minst ett år, helst 3år • Kundkontakter är meriterande   KUNSKAPSKRAV;  • Krav på flytande svenska i tal och skrift. • Mycket goda kunskaper i Microsoft Officepaketet. • Revit, Autocad • Byggteknikkompetens • Redovisningskompetens – BH90   FÄRDIGHETER OCH FÖRMÅGOR;   • Handlägga mindre uppdrag – resursplanering för uppdraget, kostnadsstyrning och genomförande tekniskt • Granskning av ritningar, modeller och övriga handlingar • Visst mått av självständigt vid genomförande av uppdraget   EGENSKAPER OCH BETEENDEN;  • Social • Utåtagerande, vilja ha kundkontakt (ej specialist) • Involverande av uppdragsgruppen, skapa resultat tillsammans • Laget före jaget   UTVECKLINGSPOTENTIAL;  • Handläggare för större uppdrag • Uppdragsledare och Uppdragsansvarig på sikt   ANDRA RELEVANTA KRAV;  • B-körkort är meriterande  För mer frågor om tjänsten  Detta är en direktrekrytering och du kommer att bli anställd hos kund. Tveka inte att ansöka redan idag genom att skicka in ditt CV och personligt brev till jimmy.eriksson@prowork.se. Eller kontakta mig via mobil 0733-324014.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Erfarna handläggande **byggnadskonstruktörer**  sökes! ARBETSUPPGIFTER;   Vå... | x | x | 21 | 21 | [Byggnadskonstruktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/HAQu_4kR_S5h) |
| 2 | .... Vår kund arbetar brett inom **husbyggnad** med allt från grundläggningar... |  | x |  | 10 | [Byggande av bostadshus och andra byggnader, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/GLJP_J7o_Nmw) |
| 2 | .... Vår kund arbetar brett inom **husbyggnad** med allt från grundläggningar... | x |  |  | 10 | [Husbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/X9ta_N3H_1is) |
| 3 | ...inom husbyggnad med allt från **grundläggningar** och stommar till fasader och ... |  | x |  | 15 | [Yrkesbevis, grundläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/t2C9_CEe_qdq) |
| 4 | ...allt från grundläggningar och **stommar** till fasader och tak. Uppdrag... | x |  |  | 7 | [installera stommar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eakt_GDz_6a6) |
| 5 | ...ga arbetsuppgifter;  • Intern **projektledare** för uppdragen • Leda och plan... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 5 | ...ga arbetsuppgifter;  • Intern **projektledare** för uppdragen • Leda och plan... | x |  |  | 13 | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| 6 | ...ch Excellens   UTBILDNING;  • **Högskoleutbildning** minst 3år med Byggkonstruktio... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 7 | ...ch Excellens   UTBILDNING;  • **Högskoleutbildning minst 3år** med Byggkonstruktionsinriktni... | x |  |  | 28 | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| 8 | ...skoleutbildning minst 3år med **Byggkonstruktionsinriktning** eller likvärdigt, eller minst... | x |  |  | 27 | [Byggnadskonstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/pYkm_Er1_z4a) |
| 9 | ...tning eller likvärdigt, eller **minst 5 års erfarenhet** av konsultuppdrag inom byggna... | x |  |  | 22 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 10 | ...renhet av konsultuppdrag inom **byggnadskonstruktion** med allmän inriktning.   ERFA... | x |  |  | 20 | [Byggnadskonstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/pYkm_Er1_z4a) |
| 11 | ... inriktning.   ERFARENHET;  • **Minst 5 års erfarenhet** av allmän konstruktion husbyg... | x |  |  | 22 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 12 | ...renhet av allmän konstruktion **husbyggnad** • Nybyggnad, om- och tillbygg... |  | x |  | 10 | [Byggande av bostadshus och andra byggnader, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/GLJP_J7o_Nmw) |
| 12 | ...renhet av allmän konstruktion **husbyggnad** • Nybyggnad, om- och tillbygg... | x |  |  | 10 | [Husbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/X9ta_N3H_1is) |
| 13 | ...byggnad, om- och tillbyggnad, **grundläggning**, stomkomplettering, stomme • ... |  | x |  | 13 | [Yrkesbevis, grundläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/t2C9_CEe_qdq) |
| 14 | ...h tillbyggnad, grundläggning, **stomkomplettering**, stomme • Revit och Autocad  ... | x |  |  | 17 | [installera stommar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eakt_GDz_6a6) |
| 15 | ...dläggning, stomkomplettering, **stomme** • Revit och Autocad  • Träbyg... | x |  |  | 6 | [installera stommar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eakt_GDz_6a6) |
| 16 | ..., stomkomplettering, stomme • **Revit** och Autocad  • Träbyggnadskom... |  | x |  | 5 | [Arkitekt-AutoCAD Revit Architecture Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6MJf_3Va_KQD) |
| 16 | ..., stomkomplettering, stomme • **Revit** och Autocad  • Träbyggnadskom... |  | x |  | 5 | [VVS, el, tele-AutoCAD Revit MEP Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6ZKB_QVh_xPs) |
| 16 | ..., stomkomplettering, stomme • **Revit** och Autocad  • Träbyggnadskom... |  | x |  | 5 | [Byggkonstruktion-Autodesk Revit Structure, **skill**](http://data.jobtechdev.se/taxonomy/concept/MsNS_1ea_u4p) |
| 16 | ..., stomkomplettering, stomme • **Revit** och Autocad  • Träbyggnadskom... | x | x | 5 | 5 | [Byggkonstruktion-AutoCAD Revit Structure Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/bgjg_cQD_QgM) |
| 16 | ..., stomkomplettering, stomme • **Revit** och Autocad  • Träbyggnadskom... |  | x |  | 5 | [Arkitekt-Autodesk Revit Architecture, **skill**](http://data.jobtechdev.se/taxonomy/concept/wZg2_hFD_TPv) |
| 17 | ...mkomplettering, stomme • Revit** och Autocad ** • Träbyggnadskompetens/kunska... | x |  |  | 13 | [Byggkonstruktion-AutoCAD Revit Structure Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/bgjg_cQD_QgM) |
| 18 | ...lettering, stomme • Revit och **Autocad**  • Träbyggnadskompetens/kunsk... |  | x |  | 7 | [Allmänkonstruktion-AutoCAD, **skill**](http://data.jobtechdev.se/taxonomy/concept/K5JR_yfk_8oj) |
| 19 | ...KAPSKRAV;  • Krav på flytande **svenska** i tal och skrift. • Mycket go... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 20 | ...et goda kunskaper i Microsoft **Officepaketet**. • Revit, Autocad • Byggtekni... | x | x | 13 | 13 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 21 | ... i Microsoft Officepaketet. • **Revit**, Autocad • Byggteknikkompeten... |  | x |  | 5 | [Arkitekt-AutoCAD Revit Architecture Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6MJf_3Va_KQD) |
| 21 | ... i Microsoft Officepaketet. • **Revit**, Autocad • Byggteknikkompeten... |  | x |  | 5 | [VVS, el, tele-AutoCAD Revit MEP Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6ZKB_QVh_xPs) |
| 21 | ... i Microsoft Officepaketet. • **Revit**, Autocad • Byggteknikkompeten... |  | x |  | 5 | [Byggkonstruktion-Autodesk Revit Structure, **skill**](http://data.jobtechdev.se/taxonomy/concept/MsNS_1ea_u4p) |
| 21 | ... i Microsoft Officepaketet. • **Revit**, Autocad • Byggteknikkompeten... | x | x | 5 | 5 | [Byggkonstruktion-AutoCAD Revit Structure Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/bgjg_cQD_QgM) |
| 21 | ... i Microsoft Officepaketet. • **Revit**, Autocad • Byggteknikkompeten... |  | x |  | 5 | [Arkitekt-Autodesk Revit Architecture, **skill**](http://data.jobtechdev.se/taxonomy/concept/wZg2_hFD_TPv) |
| 22 | ...crosoft Officepaketet. • Revit**, Autocad** • Byggteknikkompetens • Redov... | x |  |  | 9 | [Byggkonstruktion-AutoCAD Revit Structure Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/bgjg_cQD_QgM) |
| 23 | ...osoft Officepaketet. • Revit, **Autocad** • Byggteknikkompetens • Redov... |  | x |  | 7 | [Allmänkonstruktion-AutoCAD, **skill**](http://data.jobtechdev.se/taxonomy/concept/K5JR_yfk_8oj) |
| 24 | ...cepaketet. • Revit, Autocad • **Byggteknikkompetens** • Redovisningskompetens – BH9... | x |  |  | 19 | [Byggtekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/CkA6_cuX_2ws) |
| 25 | ...tocad • Byggteknikkompetens • **Redovisningskompetens** – BH90   FÄRDIGHETER OCH FÖRM... |  | x |  | 21 | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
| 26 | ...ga handlingar • Visst mått av **självständigt** vid genomförande av uppdraget... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 27 | ...kt   ANDRA RELEVANTA KRAV;  • **B-körkort** är meriterande  För mer frågo... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| | **Overall** | | | **60** | **450** | 60/450 = **13%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Arkitekt-AutoCAD Revit Architecture Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6MJf_3Va_KQD) |
|  | x |  | [VVS, el, tele-AutoCAD Revit MEP Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/6ZKB_QVh_xPs) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| x |  |  | [Byggtekniker, **job-title**](http://data.jobtechdev.se/taxonomy/concept/CkA6_cuX_2ws) |
|  | x |  | [Redovisning, **skill**](http://data.jobtechdev.se/taxonomy/concept/FYk9_RXp_7c5) |
|  | x |  | [Byggande av bostadshus och andra byggnader, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/GLJP_J7o_Nmw) |
| x | x | x | [Byggnadskonstruktör, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/HAQu_4kR_S5h) |
|  | x |  | [Allmänkonstruktion-AutoCAD, **skill**](http://data.jobtechdev.se/taxonomy/concept/K5JR_yfk_8oj) |
|  | x |  | [Byggkonstruktion-Autodesk Revit Structure, **skill**](http://data.jobtechdev.se/taxonomy/concept/MsNS_1ea_u4p) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Husbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/X9ta_N3H_1is) |
| x | x | x | [Byggkonstruktion-AutoCAD Revit Structure Suite, **skill**](http://data.jobtechdev.se/taxonomy/concept/bgjg_cQD_QgM) |
| x |  |  | [installera stommar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/eakt_GDz_6a6) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
|  | x |  | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| x |  |  | [Projektledare, bygg och anläggning, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pTTz_5DC_itW) |
| x |  |  | [Byggnadskonstruktion, **skill**](http://data.jobtechdev.se/taxonomy/concept/pYkm_Er1_z4a) |
|  | x |  | [Yrkesbevis, grundläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/t2C9_CEe_qdq) |
| x |  |  | [Högskoleutbildning, generell, 3 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/u9Qr_2wJ_nVu) |
| x | x | x | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
|  | x |  | [Arkitekt-Autodesk Revit Architecture, **skill**](http://data.jobtechdev.se/taxonomy/concept/wZg2_hFD_TPv) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/23 = **22%** |