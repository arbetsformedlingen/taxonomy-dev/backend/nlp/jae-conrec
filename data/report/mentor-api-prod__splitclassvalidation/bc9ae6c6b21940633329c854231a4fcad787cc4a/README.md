# Results for 'bc9ae6c6b21940633329c854231a4fcad787cc4a'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [bc9ae6c6b21940633329c854231a4fcad787cc4a](README.md) | 1 | 4835 | 30 | 11 | 123/301 = **41%** | 7/13 = **54%** |

## Source text

Data- och informationsstrateg till Finansinspektionen Vill du vara med och driva och samordna frågor kopplade till data, information och digitalisering på Finansinspektionen (FI)?  Avdelningen för Data- och verksamhetsutveckling har ett övergripande ansvar för att leda och samordna myndighetens data- och digitaliseringsfrågor och arbetar i hög utsträckning med utvecklande effektiviserande förändringsaktiviteter. Dessutom arbetar avdelningen kontinuerligt med införande av ny rapportering till följd av förändrade nationella och internationella regelverk.  Om jobbet  Ditt fokus som data- och informationsstrateg är ansvar för att driva och samordna verksamhetens frågor kopplade till målbilden i FI:s strategier gällande data och digitalisering. Du säkerställer rätt nivå på information, kvalitet, säkerhet och tillgänglighet samt verkar för att data behandlas som en strategisk resurs och datakunskaper som en strategisk kompetens.  I rollen ingår att medverka till ett beslutfattande baserat på data inom FI samt bidra till att utveckla myndighetens angreppssätt kring data och information för att möjliggöra en data- och insiktsdriven verksamhet. Du verkar tvärfunktionellt för att säkerställa att FI har kapacitet och kompetens att använda artificiell intelligens (AI), maskininlärning och textanalys i arbetet med tillsyn och analys. Du verkar också för att driva arbetet med att FI:s medarbetare som arbetar med tillsyn och analys ska ha enkel och effektiv åtkomst till relevanta data- och informationskällor.  I din roll arbetar du aktivt med att inspirera organisationen att dra nytta av data i det dagliga arbetet och bidrar till att öka den interna kompetensen inom data och analys. Du säkerställer att FI arbetar kontinuerligt med att automatisera processer inom data- och informationshanteringen. Du fångar upp nya behov av data från verksamheten samt stöttar dem i resan från behov till tillgänglighet. Du kommer att vara delaktig i arbetet med att säkerställa att FI:s datakällor har en uttalad informationsägare samt att data och information är informationsklassad.  Rollen innebär ett nära samarbete med ledningsgrupper på områden och staber samt chefer på FI och kontakter med alla delar av organisationen. Även samverkan med andra externa aktörer och myndigheter inom området ingår.  Din utbildning och erfarenhet  Vi söker dig som   • har en högskoleutbildning, eller motsvarande aktuell erfarenhet inom relevant område • har erfarenhet av förändringsarbete kopplat till data och information och komplexa leveranser • har erfarenhet av att driva digitalisering och strategiskt arbete • har tidigare erfarenhet av att arbeta inom data- och informationsområdet • uttrycker dig väl på svenska och engelska i både tal och skrift.  Det är meriterande om du   • har erfarenhet från arbete på annan myndighet • har erfarenhet från att samarbeta med andra för att nå gemensamma resultat.  Om dig   I det här jobbet behöver du   • vara självgående, framåtblickande och initiativtagande • bidra till ett gott samarbete • arbeta självständigt utifrån de värderingar och riktlinjer som styr verksamheten • kommunicera tydligt med olika målgrupper.       FI bidrar till ett stabilt finansiellt system, en hållbar utveckling och ett högt skydd för konsumenter. Vi erbjuder dig att arbeta med samhällsviktiga uppgifter, där du är med och påverkar den finansiella marknaden.     På FI arbetar cirka 600 personer och våra värderingar präglas av att vi är engagerade, beslutsamma, målinriktade och hjälpsamma. Vår arbetsplats ligger centralt i Stockholm. Det finns även möjlighet att delvis arbeta på distans.    Bra att veta  Vi baserar vår rekrytering på kompetens. Det innebär att vi noggrant definierar vilka kvalifikationer som efterfrågas och sedan utgår från dem genom hela rekryteringsprocessen.  Vi arbetar aktivt för mångfald och likabehandling och välkomnar därför medarbetare med olika erfarenheter, bakgrunder och kompetenser.  Vi lägger stor vikt vid dina personliga egenskaper.  Arbetspsykologiska tester ingår i rekryteringsprocessen.  På FI arbetar vi aktivt för att vara en attraktiv arbetsgivare där du som medarbetare trivs och utvecklas. Läs mer om hur det är att jobba på FI och vad vi erbjuder:  https://www.fi.se/sv/om-fi/lediga-jobb/vi-erbjuder/  En anställning på FI kan innebära att säkerhetsprövning och registerkontroll görs enligt säkerhetsskyddslagen.  Sök jobbet  Välkommen med din ansökan som ska innehålla cv, personligt brev och relevanta intyg eller examensbevis.  Vi avböjer all hjälp med rekrytering och annonsering.      Anställningsform:    Anställningen är tillsvidare och inleds med sex månaders provanställning. Tillträde enligt överenskommelse.      Omfattning:    Heltid      Kontaktperson:    Ann-Louise Ulvenvall, avdelningschef, 08-408 984 11      Sista ansökningsdag:    2022-08-31

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Data**- och informationsstrateg till... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 2 | Data- och **informationsstrateg** till Finansinspektionen Vill ... | x | x | 19 | 19 | [Informationsstrateg, **job-title**](http://data.jobtechdev.se/taxonomy/concept/GzyH_LLy_Mok) |
| 3 | ...samordna frågor kopplade till **data**, information och digitaliseri... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 4 | ...tionen (FI)?  Avdelningen för **Data**- och verksamhetsutveckling ha... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 5 | ...eda och samordna myndighetens **data**- och digitaliseringsfrågor oc... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 6 | ...k.  Om jobbet  Ditt fokus som **data**- och informationsstrateg är a... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 7 | ...bet  Ditt fokus som data- och **informationsstrateg** är ansvar för att driva och s... | x | x | 19 | 19 | [Informationsstrateg, **job-title**](http://data.jobtechdev.se/taxonomy/concept/GzyH_LLy_Mok) |
| 8 | ...en i FI:s strategier gällande **data** och digitalisering. Du säkers... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 9 | ...änglighet samt verkar för att **data** behandlas som en strategisk r... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 10 | ...ett beslutfattande baserat på **data** inom FI samt bidra till att u... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 11 | ...ndighetens angreppssätt kring **data** och information för att möjli... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 12 | ...rmation för att möjliggöra en **data**- och insiktsdriven verksamhet... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 13 | ...tet och kompetens att använda **artificiell intelligens** (AI), maskininlärning och tex... | x | x | 23 | 23 | [Artificial Intelligence/Artificiell intelligens/AI, **skill**](http://data.jobtechdev.se/taxonomy/concept/SaTd_K5z_ex1) |
| 14 | ...ända artificiell intelligens (**AI**), maskininlärning och textana... | x | x | 2 | 2 | [Artificial Intelligence/Artificiell intelligens/AI, **skill**](http://data.jobtechdev.se/taxonomy/concept/SaTd_K5z_ex1) |
| 15 | ...artificiell intelligens (AI), **maskininlärning** och textanalys i arbetet med ... | x | x | 15 | 15 | [Machine learning/Maskininlärning, **skill**](http://data.jobtechdev.se/taxonomy/concept/XEhm_7YA_qKM) |
| 16 | ...ens (AI), maskininlärning och **textanalys** i arbetet med tillsyn och ana... | x | x | 10 | 10 | [Textanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/xBPm_pm2_s95) |
| 17 | ...fektiv åtkomst till relevanta **data**- och informationskällor.  I d... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 18 | ...ganisationen att dra nytta av **data** i det dagliga arbetet och bid... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 19 | ... den interna kompetensen inom **data** och analys. Du säkerställer a... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 20 | ...t automatisera processer inom **data**- och informationshanteringen.... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 21 | ...n. Du fångar upp nya behov av **data** från verksamheten samt stötta... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 22 | ...ad informationsägare samt att **data** och information är informatio... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 23 | ...  Vi söker dig som   • har en **högskoleutbildning**, eller motsvarande aktuell er... |  | x |  | 18 | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| 24 | ...örändringsarbete kopplat till **data** och information och komplexa ... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 25 | ...erfarenhet av att arbeta inom **data**- och informationsområdet • ut... | x |  |  | 4 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 26 | ...mrådet • uttrycker dig väl på **svenska** och engelska i både tal och s... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 27 | ...rycker dig väl på svenska och **engelska** i både tal och skrift.  Det ä... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 28 | ...dra till ett gott samarbete • **arbeta självständigt** utifrån de värderingar och ri... | x | x | 20 | 20 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 29 | ...tabilt finansiellt system, en **hållbar utveckling** och ett högt skydd för konsum... | x |  |  | 18 | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| 30 | ...arbetsplats ligger centralt i **Stockholm**. Det finns även möjlighet att... |  | x |  | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 31 | ...ingsform:    Anställningen är **tillsvidare och inleds med sex månaders provanställning**. Tillträde enligt överenskomm... | x |  |  | 55 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 32 | ...kommelse.      Omfattning:    **Heltid**      Kontaktperson:    Ann-Lo... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **123** | **301** | 123/301 = **41%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Informationsstrateg, **job-title**](http://data.jobtechdev.se/taxonomy/concept/GzyH_LLy_Mok) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| x |  |  | [Hållbar utveckling, **keyword**](http://data.jobtechdev.se/taxonomy/concept/RKw3_zWW_1pf) |
| x | x | x | [Artificial Intelligence/Artificiell intelligens/AI, **skill**](http://data.jobtechdev.se/taxonomy/concept/SaTd_K5z_ex1) |
| x | x | x | [Machine learning/Maskininlärning, **skill**](http://data.jobtechdev.se/taxonomy/concept/XEhm_7YA_qKM) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Universitets- eller högskoleutbildning, **sni-level-4**](http://data.jobtechdev.se/taxonomy/concept/m1vC_Ssq_5Y7) |
| x | x | x | [Textanalys, **skill**](http://data.jobtechdev.se/taxonomy/concept/xBPm_pm2_s95) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/13 = **54%** |