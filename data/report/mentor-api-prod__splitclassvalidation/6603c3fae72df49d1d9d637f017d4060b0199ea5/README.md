# Results for '6603c3fae72df49d1d9d637f017d4060b0199ea5'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [6603c3fae72df49d1d9d637f017d4060b0199ea5](README.md) | 1 | 3358 | 16 | 10 | 116/226 = **51%** | 8/14 = **57%** |

## Source text

Är du en frontend-/fullstack-utvecklare som letar efter en ny utmaning? Är du en front-/fullstack-utvecklare som letar efter en ny utmaning nu till hösten? Då är vi nog rätt för dig. 😊 Hos oss på Cilbuper Group består vi av ett härligt team, som vi gärna vill ska utökas. Vi söker därför dig som är redo för att hänga med på vårt roliga äventyr och hjälpa våra kunder att bygga på morgondagens produkter.  Vad innebär det att vara anställd på Cilbuper Group?  Du kommer att arbeta som konsult ute hos en av våra kunder, och det kan handla om nyutveckling eller vidareutveckling av befintliga produkter. Vilken av våra kunder du kommer att arbeta för har du stor möjlighet att påverka själv. Vi är flexibla och vill att ditt arbete ska fungera för just din livssituation.  Vilka är vi på Cilbuper Group?  Vi är en koncern som består av fyra bolag, där Cilbuper IT, Pagesplit, Agoshi och Republify omsluts inom Cilbuper Group. Vi har en mångfald av olika profiler som består bådadera av juniora profiler som vi vill ge en chans in i arbetslivet men även seniora utvecklare som får nya spännande uppdrag framför sig.  Vårt härliga kontor är beläget vid Järntorget i Göteborg och vi arbetar systematiskt med att skapa möten mellan våra kunder och våra konsulter inom IT-branschen. Vi tillgodoser våra anställda utbildningsmöjligheter, komfortabla nödvändigheter som friskvård och pensionstillägg samt även roliga aktiviteter utanför arbetet. Vi tror på att om man har en bra inställning, ett glatt humör och ett brinnande intresse för programmering så passar man perfekt i vårt team. Därför tror vi på de människor som befinner sig i nystarten på sin karriär men även de som varit inom branschen ett tag. Hos oss är man alltid en del av vårt Cilbuper team och får under sin resa utvecklas och ha roligt på vägen!  Varför skulle du vilja jobba hos oss?  Trots att vi är ett relativt ungt bolag som grundades 2016 och vi befinner oss i en expanderande bransch som kan upplevas som en djungel för dig som arbetssökande med alla potentiella arbetsgivare, så är vi en arbetsplats som är ödmjuk och flexibel. Vi erbjuder dig att bli välkommen till en stark gemenskap och ett väletablerat kontaktnät som möjliggör att våra förfrågningar och kunder varierar. Hos oss får man vara en del av en gemenskap som främjar såväl personlig som professionell utveckling och vi kan garantera dig att du alltid kommer ha roligt. Cilbuper Group är en växande koncern som letar efter en ny kollega som vill utvecklas tillsammans med oss.   Vem är vår nya kollega?  Vi tror att du har: Eftergymnasial utbildning inom mjukvaruutveckling Minst 1 års erfarenhet av C#-programmering En skön personlighet Vilja att arbeta Agilt Lagom goda kunskaper i svenska och engelska, i både tal och skrift Ambition att utveckla dig själv Ett genuint teknikintresse och är en teamplayer Vana av att arbeta i en agil miljö.  Ambition att utveckla dig själv.   Vad är nästa steg?  Om du är intresserad av att veta mer om oss kan vi koppla ihop dig med en av våra anställda som kan ge dig en bild av hur det är att arbeta här.  Annars börjar din nystart här! https://pnty-apply.ponty-system.se/cilbuper?id=20  Vi ses snart 😊 Vännerna på Cilbuper Group! Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Är du en **frontend**-/fullstack-utvecklare som let... | x | x | 8 | 8 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 2 | Är du en frontend**-/fullstac**k-utvecklare som letar efter e... |  | x |  | 10 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 3 | Är du en frontend-/**fullstack-utvecklare** som letar efter en ny utmanin... | x | x | 20 | 20 | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
| 4 | ...fter en ny utmaning? Är du en **front**-/fullstack-utvecklare som let... | x |  |  | 5 | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| 5 | ... ny utmaning? Är du en front-/**fullstack-utvecklare** som letar efter en ny utmanin... | x | x | 20 | 20 | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
| 6 | ... av fyra bolag, där Cilbuper I**T,** Pagesplit, Agoshi och Republi... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 7 | ...g framför sig.  Vårt härliga k**ontor **är beläget vid Järntorget i Gö... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 8 | ...r är beläget vid Järntorget i **Göteborg** och vi arbetar systematiskt m... |  | x |  | 8 | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| 9 | ...under och våra konsulter inom **I**T-branschen. Vi tillgodoser vå... |  | x |  | 1 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 10 | ...nder och våra konsulter inom I**T-**branschen. Vi tillgodoser våra... | x | x | 2 | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 11 | ...er och våra konsulter inom IT-**branschen**. Vi tillgodoser våra anställd... |  | x |  | 9 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 12 | ...mfortabla nödvändigheter som f**riskvård **och pensionstillägg samt även ... | x |  |  | 9 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 13 | ...ch ett brinnande intresse för **p**rogrammering så passar man per... |  | x |  | 1 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 14 | ...h ett brinnande intresse för p**rogrammering** så passar man perfekt i vårt ... | x | x | 12 | 12 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 15 | ...nde intresse för programmering** **så passar man perfekt i vårt t... | x |  |  | 1 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 16 | ...kollega?  Vi tror att du har: **E**ftergymnasial utbildning inom ... |  | x |  | 1 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 17 | ...ollega?  Vi tror att du har: E**ftergymnasial utbildning** inom mjukvaruutveckling Minst... | x | x | 24 | 24 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 18 | ...har: Eftergymnasial utbildning** **inom mjukvaruutveckling Minst ... | x |  |  | 1 | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| 19 | ...ftergymnasial utbildning inom **m**jukvaruutveckling Minst 1 års ... |  | x |  | 1 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 20 | ...tergymnasial utbildning inom m**jukvaruutveckling** Minst 1 års erfarenhet av C#-... | x | x | 17 | 17 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 21 | ...ldning inom mjukvaruutveckling** **Minst 1 års erfarenhet av C#-p... | x |  |  | 1 | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| 22 | ...ning inom mjukvaruutveckling M**inst 1 års erfarenhet **av C#-programmering En skön pe... | x |  |  | 22 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| 23 | ...ng Minst 1 års erfarenhet av C**#-programmering **En skön personlighet Vilja att... | x |  |  | 16 | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| 24 | ... skön personlighet Vilja att a**rbeta Agilt **Lagom goda kunskaper i svenska... | x |  |  | 12 | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| 25 | ... Agilt Lagom goda kunskaper i **s**venska och engelska, i både ta... |  | x |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 26 | ...Agilt Lagom goda kunskaper i s**venska** och engelska, i både tal och ... | x | x | 6 | 6 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 27 | ...Lagom goda kunskaper i svenska** **och engelska, i både tal och s... | x |  |  | 1 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 28 | ... goda kunskaper i svenska och **e**ngelska, i både tal och skrift... |  | x |  | 1 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 29 | ...goda kunskaper i svenska och e**ngelska**, i både tal och skrift Ambiti... | x | x | 7 | 7 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 30 | ...nskaper i svenska och engelska**,** i både tal och skrift Ambitio... | x |  |  | 1 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| | **Overall** | | | **116** | **226** | 116/226 = **51%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Fullstack-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/71Ji_irM_rSJ) |
| x | x | x | [Mjukvaruutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/A5HJ_aMw_BQt) |
| x | x | x | [Frontend-utvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/GDHs_eoz_uKx) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x |  |  | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
|  | x |  | [Göteborg, **municipality**](http://data.jobtechdev.se/taxonomy/concept/PVZL_BQT_XtL) |
| x |  |  | [Agila arbetsmetoder, **skill**](http://data.jobtechdev.se/taxonomy/concept/TMVb_DnH_etF) |
| x | x | x | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x | x | x | [Eftergymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/bg6r_KVf_KNN) |
| x | x | x | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x |  |  | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| x |  |  | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **8** | 8/14 = **57%** |