# Results for '7ea04a2468ea094bf463c1e490fe735e44cb02c1'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [7ea04a2468ea094bf463c1e490fe735e44cb02c1](README.md) | 1 | 4203 | 26 | 24 | 121/394 = **31%** | 11/23 = **48%** |

## Source text

Säljledare Täby Vi är ett svenskt bolag i en stor, internationell familj. Med mer än 5 000 medarbetare över hela landet, försvinner du aldrig i mängden. Istället är du med och gör vår fantastiska resa möjlig. Övertygade om att god och hållbar mat, av hög kvalitet, inte ska behöva kosta skjortan, utmanar vi den svenska livsmedelsbranschen. Och vi gör det framgångsrikt, vi växer snabbare än någon annan livsmedelskedja.  Sammanfattning av tjänsten  Vill du arbeta för dagligvaruhandelns snabbast växande kedja som också är certifierad som Top Employer 2022? Vill du utveckla människor och ge våra kunder en upplevelse som överträffar deras förväntningar? Välkommen till rollen som säljledare på Lidl!  Din roll  Som säljledare på Lidl är du butikschefens högra hand och har en given plats i butiksledningen. Du motiverar och inspirerar butiksteamet till att uppnå sin fulla potential varje dag och säkerställer att uppgifter utförs effektivt, korrekt och i tid. Du deltar självklart i det dagliga butiksarbetet och arbetar tillsammans med teamet för att ge våra kunder den bästa butiksupplevelsen.  Rollen innefattar uppgifter som:   *  Säkerställa och leverera utmärkt kundservice i butiken    *  Stötta butikschefen i det dagliga arbetet i butiken och se till att ni når resultat genom att leda, motivera och utveckla butikspersonalen samt delta i det dagliga arbetet på ett föredömligt sätt   *  Säkerställa att butiken lever upp till våra grundpelare inom kundvänlighet, varutillgänglighet, färskhet, renlighet och lönsamhet    *  Arbeta målinriktat med butikens nyckeltal   *  Samordna och organisera introduktion och vidareutbildning av alla butiksmedarbetare   *  Du har även totalansvaret för butiksdriften när butikschefen inte är på plats   Din profil    *  Du har erfarenhet av att leda, engagera, inspirera och utveckla team i en snabbrörlig och resultatorienterad miljö    *  Du har god problemlösningsförmåga och är duktig på att planera och delegera arbetsuppgifter    *  Du har starkt kundfokus och mycket god kommunikationsförmåga där du kan ge feedback som utvecklar teamet och butiken   *  Du har viljestyrkan att slutföra saker i tid och motiveras av att överträffa satta mål och maximera produktiviteten   *  Du har möjligtvis tidigare erfarenhet av att arbeta som ställföreträdande butikschef, butikschef eller som avdelningsansvarig inom dagligvaruhandeln   *  Du har en avslutad gymnasieutbildning eller motsvarande, gärna med inriktning mot handel   *  Om du har erfarenhet av att arbeta inom dagligvaruhandeln eller liknande bransch är det meriterande   *  Du behärskar svenska flytande i både tal och skrift   Vi erbjuder  Vi erbjuder dig en utmanande och utvecklande arbetsmiljö där du får mycket ansvar och möjlighet att påverka Lidls framgång i Sverige. Vårt dagliga arbete är, förutom att leda, inspirera och leverera utifrån tydligt satta mål även karaktäriserade av våra värdeord: engagemang, ansvar, smart, tillsammans och ha kul. Och eftersom vi växer, kan du fortsätta utvecklas. Snabbt. Inom din roll, eller mot en annan. På samma plats, eller någon annanstans. Inom butik, lager, kontor. Lokalt, nationellt eller globalt. Vi stöttar dig på din resa mot mer ansvar och nya utmaningar. Du blir en del av ett stort team där alla kavlar upp ärmarna och hjälps åt för att fortsatt kunna utmana branschen. Tjänsten är en tillsvidareanställning med inledande provanställning. Vi har förmåner så som friskvårdsbidrag, medarbetarrabatt i våra butiker samt tillgång till mängder av erbjudanden via vår förmånsportal. Självklart omfattas du också av kollektivavtal. Då butiken är öppen alla veckans dagar kommer både dags-, kvälls- och helgarbete att förekomma. Vill du vara med på vår Lidl resa? Om ditt svar är ja, då söker du jobbet såhär: o    Klicka på “Sök nu” o    Fyll i formuläret o    Bifoga CV samt personligt brev Denna rekrytering sker med löpande urval, varför vi gärna ser att du skickar din ansökan snarast. Vår rekryteringsprocess består av tester, telefonintervju, personlig intervju och digital referenstagning. Har du frågor om tjänsten eller ansökningsprocessen, vänligen kontakta Rekryteringsteamet på mail: jobb@lidl.se. Vi ser fram emot din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Säljledare** Täby Vi är ett svenskt bolag ... | x | x | 10 | 10 | [Säljledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QLwS_aMP_Jcu) |
| 1 | **Säljledare** Täby Vi är ett svenskt bolag ... |  | x |  | 10 | [Säljledare, resebyrå, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3Qo_rk6_3fg) |
| 1 | **Säljledare** Täby Vi är ett svenskt bolag ... |  | x |  | 10 | [Assisterande säljledare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/nmPG_gSS_rBm) |
| 2 | Säljledare **Täby** Vi är ett svenskt bolag i en ... | x | x | 4 | 4 | [Täby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/onpA_B5a_zfv) |
| 3 | ...osta skjortan, utmanar vi den **svenska** livsmedelsbranschen. Och vi g... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 4 | ...ar? Välkommen till rollen som **säljledare** på Lidl!  Din roll  Som säljl... | x | x | 10 | 10 | [Säljledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QLwS_aMP_Jcu) |
| 4 | ...ar? Välkommen till rollen som **säljledare** på Lidl!  Din roll  Som säljl... |  | x |  | 10 | [Säljledare, resebyrå, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3Qo_rk6_3fg) |
| 4 | ...ar? Välkommen till rollen som **säljledare** på Lidl!  Din roll  Som säljl... |  | x |  | 10 | [Assisterande säljledare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/nmPG_gSS_rBm) |
| 5 | ...till rollen som säljledare på **Lidl**!  Din roll  Som säljledare på... | x |  |  | 4 | [Lidl, **keyword**](http://data.jobtechdev.se/taxonomy/concept/TvrT_vwf_HwW) |
| 6 | ...edare på Lidl!  Din roll  Som **säljledare** på Lidl är du butikschefens h... | x | x | 10 | 10 | [Säljledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QLwS_aMP_Jcu) |
| 6 | ...edare på Lidl!  Din roll  Som **säljledare** på Lidl är du butikschefens h... |  | x |  | 10 | [Säljledare, resebyrå, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3Qo_rk6_3fg) |
| 6 | ...edare på Lidl!  Din roll  Som **säljledare** på Lidl är du butikschefens h... |  | x |  | 10 | [Assisterande säljledare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/nmPG_gSS_rBm) |
| 7 | ...  Din roll  Som säljledare på **Lidl** är du butikschefens högra han... | x |  |  | 4 | [Lidl, **keyword**](http://data.jobtechdev.se/taxonomy/concept/TvrT_vwf_HwW) |
| 8 | ... Som säljledare på Lidl är du **butikschefens** högra hand och har en given p... | x |  |  | 13 | [Butikschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WWVG_4sM_faC) |
| 9 | ...erställa och leverera utmärkt **kundservice** i butiken    *  Stötta butiks... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 10 | ...ervice i butiken    *  Stötta **butikschefen** i det dagliga arbetet i butik... | x |  |  | 12 | [Butikschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WWVG_4sM_faC) |
| 11 | ... och vidareutbildning av alla **butiksmedarbetare**   *  Du har även totalansvare... | x | x | 17 | 17 | [Butiksmedarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ifET_wHx_6w5) |
| 12 | ...nsvaret för butiksdriften när **butikschefen** inte är på plats   Din profil... | x |  |  | 12 | [Butikschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WWVG_4sM_faC) |
| 13 | ...arkt kundfokus och mycket god **kommunikationsförmåga** där du kan ge feedback som ut... | x |  |  | 21 | [Kommunikationsförmåga - Butiksledare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/HJoH_Nqx_gAH) |
| 14 | ... arbeta som ställföreträdande **butikschef**, butikschef eller som avdelni... | x | x | 10 | 10 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 15 | ...ställföreträdande butikschef, **butikschef** eller som avdelningsansvarig ... | x |  |  | 10 | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| 16 | ...ndeln   *  Du har en avslutad **gymnasieutbildning** eller motsvarande, gärna med ... | x |  |  | 18 | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
| 16 | ...ndeln   *  Du har en avslutad **gymnasieutbildning** eller motsvarande, gärna med ... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 17 | ...nde, gärna med inriktning mot **handel**   *  Om du har erfarenhet av ... | x |  |  | 6 | [Handel och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/5ema_o59_LJL) |
| 17 | ...nde, gärna med inriktning mot **handel**   *  Om du har erfarenhet av ... |  | x |  | 6 | [Handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/qPyg_Zo8_EEd) |
| 18 | ...meriterande   *  Du behärskar **svenska** flytande i både tal och skrif... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 19 | ... en utmanande och utvecklande **arbetsmiljö** där du får mycket ansvar och ... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 20 | ...var och möjlighet att påverka **Lidls** framgång i Sverige. Vårt dagl... | x |  |  | 5 | [Lidl, **keyword**](http://data.jobtechdev.se/taxonomy/concept/TvrT_vwf_HwW) |
| 21 | ... att påverka Lidls framgång i **Sverige**. Vårt dagliga arbete är, föru... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 22 | ... eller någon annanstans. Inom **butik**, lager, kontor. Lokalt, natio... | x | x | 5 | 5 | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| 23 | ...någon annanstans. Inom butik, **lager**, kontor. Lokalt, nationellt e... | x | x | 5 | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 24 | ...nnanstans. Inom butik, lager, **kontor**. Lokalt, nationellt eller glo... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 25 | ...ana branschen. Tjänsten är en **tillsvidareanställning med inledande provanställning**. Vi har förmåner så som frisk... | x |  |  | 52 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 26 | ...älvklart omfattas du också av **kollektivavtal**. Då butiken är öppen alla vec... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 27 | ...omma. Vill du vara med på vår **Lidl** resa? Om ditt svar är ja, då ... | x |  |  | 4 | [Lidl, **keyword**](http://data.jobtechdev.se/taxonomy/concept/TvrT_vwf_HwW) |
| 28 | ...ingsprocess består av tester, **telefonintervju**, personlig intervju och digit... |  | x |  | 15 | [Telefonintervjuare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sDjW_zkk_bXW) |
| | **Overall** | | | **121** | **394** | 121/394 = **31%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Handel och administration, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/5ema_o59_LJL) |
| x |  |  | [Kommunikationsförmåga - Butiksledare, **swedish-retail-and-wholesale-council-skill**](http://data.jobtechdev.se/taxonomy/concept/HJoH_Nqx_gAH) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| x | x | x | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x | x | x | [Säljledare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QLwS_aMP_Jcu) |
| x |  |  | [Lidl, **keyword**](http://data.jobtechdev.se/taxonomy/concept/TvrT_vwf_HwW) |
| x |  |  | [Butikschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/WWVG_4sM_faC) |
| x | x | x | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
|  | x |  | [Säljledare, resebyrå, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/Y3Qo_rk6_3fg) |
| x | x | x | [butikschef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/bRZF_sdj_cdk) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Butiksmedarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/ifET_wHx_6w5) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [Assisterande säljledare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/nmPG_gSS_rBm) |
| x | x | x | [Täby, **municipality**](http://data.jobtechdev.se/taxonomy/concept/onpA_B5a_zfv) |
|  | x |  | [Handel, **keyword**](http://data.jobtechdev.se/taxonomy/concept/qPyg_Zo8_EEd) |
|  | x |  | [Telefonintervjuare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/sDjW_zkk_bXW) |
| x |  |  | [Gymnasial utbildning, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/t8bn_FTn_FiG) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x | x | x | [Butik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xLsd_zjY_5wr) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **11** | 11/23 = **48%** |