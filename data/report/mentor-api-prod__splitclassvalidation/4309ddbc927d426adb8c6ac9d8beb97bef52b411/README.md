# Results for '4309ddbc927d426adb8c6ac9d8beb97bef52b411'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4309ddbc927d426adb8c6ac9d8beb97bef52b411](README.md) | 1 | 617 | 6 | 2 | 15/53 = **28%** | 2/6 = **33%** |

## Source text

kok-kalkänka  Vi letar efter dig som kan ge den extra kick i köket! Vi letar efter en person i köket som har jobbat till små restauranger eller matcaffeer och kan vet om matlagning även lättare matlagning som utförs mest via rational ugn men inte bara. kan du göra egna bakelser och har erfarenhet med sallader och andra kala rätter då det är ett bonus! med några få ord vi letar dig som kan och ska ge den extra med sina kunskaper så att på ditt sätt lyfter upp matupplevelsen hos gästerna. vi kräver inte gourmet kunskaper men att du är säkert för dig själv att du kan din grej o att du förstå vad vi är i behov av.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | kok-**kalkänka**  Vi letar efter dig som kan g... | x |  |  | 8 | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| 2 | ...köket som har jobbat till små **restauranger** eller matcaffeer och kan vet ... | x |  |  | 12 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 3 | ...t till små restauranger eller **matcaffeer** och kan vet om matlagning äve... | x |  |  | 10 | [Café, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sv3s_HEf_rbC) |
| 4 | ...n inte bara. kan du göra egna **bakelser** och har erfarenhet med sallad... | x | x | 8 | 8 | [Tårtor/Bakelser, **skill**](http://data.jobtechdev.se/taxonomy/concept/UVj4_qZp_iqS) |
| 5 | ...kelser och har erfarenhet med **sallader** och andra kala rätter då det ... | x |  |  | 8 | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| 6 | ... hos gästerna. vi kräver inte **gourmet** kunskaper men att du är säker... | x | x | 7 | 7 | [Gourmet, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/t8t5_hsb_cpv) |
| | **Overall** | | | **15** | **53** | 15/53 = **28%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| x |  |  | [Café, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Sv3s_HEf_rbC) |
| x | x | x | [Tårtor/Bakelser, **skill**](http://data.jobtechdev.se/taxonomy/concept/UVj4_qZp_iqS) |
| x |  |  | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x |  |  | [Salladsberedning, **skill**](http://data.jobtechdev.se/taxonomy/concept/pT39_hq7_jAW) |
| x | x | x | [Gourmet, redovisningssystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/t8t5_hsb_cpv) |
| | | **2** | 2/6 = **33%** |