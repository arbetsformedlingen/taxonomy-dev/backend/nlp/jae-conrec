# Results for '848348a23f7de451b0a9f8d81b529d6b1b03e7d9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [848348a23f7de451b0a9f8d81b529d6b1b03e7d9](README.md) | 1 | 1642 | 8 | 9 | 46/115 = **40%** | 5/9 = **56%** |

## Source text

KLASSISK SVENSK ÖLHALL & MATSAL SÖKER SERVIS! Zynca samarbetar med en av Södermalms anrikaste omtyckta restauranger. Nu söker vi erfaren servispersonal!  Om Zynca: Vi är Sveriges digitala Jobbförmedling! Zyncas digitala plattform kopplar samman arbetsgivare med arbetssökande. Allt på en och samma plats!​ Om restaurangen: Det är en välbesökt restaurang, ölhall och matsal precis vid Slussen i en vacker kulturmärkt lokal med höga tak, vackra valv och en välkomnande atmosfär.  Menyn består av mat från det svenska traditionella köket med fokus på kött och fisk, men även ett stort utbud av öl med bland annat sin egen lager och hemmagjorda nubbar.  Här serveras bland annat klassisk husmanskost till lunch eller tre rätters  a´la carte till middag. Restaurangen är ofta fullsatt och målet är först klassisk service i alla lägen.  Vi söker: Dig som är en positiv, glad och serviceminded person. Du behärskar givetvis grunderna i serveringsyrket. Det du inte kan har Du en vilja att lära dig. Du är inte rädd för att bjuda på dig själv för att skapa en oförglömlig gästupplevelse. Du talar utöver en mycket bra svenska även engelska. Vi erbjuder: En arbetsplats med bra villkor, härlig och bra sammanhållning i ett team av kollegor där alla ställer upp för varandra. Hos oss vill vi att personalen ska trivas och bli en del av teamet! Vi söker både heltid och deltid. Du söker: Genom att skicka ett mail med din CV, en bild och ett brev där du berättar om dig och din erfarenhet till martina@zynca.se  *Tjänsten är en direktrekrytering där du blir anställd direkt av restaurangen efter att Zynca genomfört rekryteringsprocessen.    #jobbjustnu

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ... SVENSK ÖLHALL & MATSAL SÖKER **SERVIS**! Zynca samarbetar med en av S... | x | x | 6 | 6 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 2 | ...auranger. Nu söker vi erfaren **servispersonal**!  Om Zynca: Vi är Sveriges di... | x | x | 14 | 14 | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| 3 | ...lt på en och samma plats!​ Om **restaurang**en: Det är en välbesökt restau... |  | x |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 4 | ...aurangen: Det är en välbesökt **restaurang**, ölhall och matsal precis vid... | x |  |  | 10 | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| 5 | ... Menyn består av mat från det **svenska** traditionella köket med fokus... |  | x |  | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 6 | ...v öl med bland annat sin egen **lager** och hemmagjorda nubbar.  Här ... |  | x |  | 5 | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| 7 | ...serveras bland annat klassisk **husmanskost** till lunch eller tre rätters ... | x | x | 11 | 11 | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
| 8 | ...till lunch eller tre rätters  **a´la carte** till middag. Restaurangen är ... | x |  |  | 10 | [Kock, à la carte, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JbJw_as4_S93) |
| 9 | ...l lunch eller tre rätters  a´l**a carte ti**ll middag. Restaurangen är oft... |  | x |  | 10 | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| 10 | ...Du talar utöver en mycket bra **svenska** även engelska. Vi erbjuder: E... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 11 | ...er en mycket bra svenska även **engelska**. Vi erbjuder: En arbetsplats ... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 12 | ... del av teamet! Vi söker både **heltid och deltid**. Du söker: Genom att skicka e... | x |  |  | 17 | (not found in taxonomy) |
| | **Overall** | | | **46** | **115** | 46/115 = **40%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [A la carte, **skill**](http://data.jobtechdev.se/taxonomy/concept/Bb4Q_Vou_pbq) |
| x | x | x | [Husmanskost, **skill**](http://data.jobtechdev.se/taxonomy/concept/HfAs_ZWA_Jvv) |
|  | x |  | [Lager, **keyword**](http://data.jobtechdev.se/taxonomy/concept/J5gJ_6vV_egQ) |
| x |  |  | [Kock, à la carte, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JbJw_as4_S93) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | (not found in taxonomy) |
| x | x | x | [Restaurang, **keyword**](http://data.jobtechdev.se/taxonomy/concept/o2Cg_3Ap_qvK) |
| x | x | x | [Servitör/Servitris, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rGGf_KLs_To7) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/9 = **56%** |