# Results for 'ac84deed682488768004e722531d540951ba4d07'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [ac84deed682488768004e722531d540951ba4d07](README.md) | 1 | 344 | 6 | 10 | 38/131 = **29%** | 3/10 = **30%** |

## Source text

Träarbetare Är du träarbetare med minst fem års erfarenhet, självgående, kommunicerar på svenska och kan göra allt som rör träarbete(form, stommar, stomkomplettering, gipsa, sätta reglar, tak, golv osv) Då är du välkommen att skicka in din ansökan redan idag mejla ditt CV till: jobb@expanderamera.se Vid frågor ring 0853034378 eller 0707707232

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Träarbetare** Är du träarbetare med minst f... | x | x | 11 | 11 | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| 1 | **Träarbetare** Är du träarbetare med minst f... |  | x |  | 11 | [träarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FmH5_Pqk_6k3) |
| 2 | Träarbetare Är du **träarbetare** med minst fem års erfarenhet,... | x | x | 11 | 11 | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| 2 | Träarbetare Är du **träarbetare** med minst fem års erfarenhet,... |  | x |  | 11 | [träarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FmH5_Pqk_6k3) |
| 3 | ...rbetare Är du träarbetare med **minst fem års erfarenhet**, självgående, kommunicerar på... | x |  |  | 24 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 4 | ...med minst fem års erfarenhet, **självgående**, kommunicerar på svenska och ... | x |  |  | 11 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 5 | ... självgående, kommunicerar på **svenska** och kan göra allt som rör trä... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 6 | ...ska och kan göra allt som rör **träarbete**(form, stommar, stomkompletter... |  | x |  | 9 | [arbetsledare, träarbeten, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/D7QN_W7F_5kh) |
| 6 | ...ska och kan göra allt som rör **träarbete**(form, stommar, stomkompletter... | x | x | 9 | 9 | [Träarbete, husbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/EwT4_Jtg_AQf) |
| 6 | ...ska och kan göra allt som rör **träarbete**(form, stommar, stomkompletter... |  | x |  | 9 | [Service och underhåll, träarbete bygg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Tv4S_faX_pzi) |
| 6 | ...ska och kan göra allt som rör **träarbete**(form, stommar, stomkompletter... |  | x |  | 9 | [Yrkesbevis, lekredskap, träarbeten, **skill**](http://data.jobtechdev.se/taxonomy/concept/YNqd_JMH_kh7) |
| 6 | ...ska och kan göra allt som rör **träarbete**(form, stommar, stomkompletter... |  | x |  | 9 | [Träarbete, anläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/tQr7_pSR_XWn) |
| | **Overall** | | | **38** | **131** | 38/131 = **29%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Träarbetare/Snickare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4Z7F_oBG_vMG) |
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
|  | x |  | [arbetsledare, träarbeten, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/D7QN_W7F_5kh) |
| x | x | x | [Träarbete, husbyggnad, **skill**](http://data.jobtechdev.se/taxonomy/concept/EwT4_Jtg_AQf) |
|  | x |  | [träarbetare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/FmH5_Pqk_6k3) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Service och underhåll, träarbete bygg, **skill**](http://data.jobtechdev.se/taxonomy/concept/Tv4S_faX_pzi) |
|  | x |  | [Yrkesbevis, lekredskap, träarbeten, **skill**](http://data.jobtechdev.se/taxonomy/concept/YNqd_JMH_kh7) |
|  | x |  | [Träarbete, anläggning, **skill**](http://data.jobtechdev.se/taxonomy/concept/tQr7_pSR_XWn) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **3** | 3/10 = **30%** |