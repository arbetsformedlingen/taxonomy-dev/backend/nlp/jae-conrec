# Results for 'd92378b6c6aac59dcea5f04fad86ff64f57cf7d2'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d92378b6c6aac59dcea5f04fad86ff64f57cf7d2](README.md) | 1 | 2850 | 13 | 11 | 74/139 = **53%** | 7/13 = **54%** |

## Source text

Kallskänka / köksbiträde Om jobbet   Stresstålig, snabbfotad och har intresse för matlagning ?    Då kan du vara Cafe Fika and Wine´s nya team-medlem i köket. Vi är ett trevligt café med alkoholrättigheter som serverar Gamla Stans mysigaste fika, enkel och vällagad mat samt erbjuder både gäster och medarbetare en historisk miljö att vistas i sommar som vinter.   Till oss kommer stammisen som bor runt hörnet likväl som internationella besökare från världens många hörn, middagssällskap och inbokade grupper i vårt chambre separee. Ingen dag är den andra lik eftersom vi har så många olika typer av gäster med olika behov och önskningar. Nu när sommaren är här så vi ser flödet av stammisar, nya lokala gäster likväl som turister ökar i vår verksamhet. Därför vill vi nu bygga upp ett väl fungerande team i köket som vill vara med och utveckla menyn . Därför letar vi efter dig som vill bidra till att allt som går ut från vårt kök är av hög kvalitet och är tilltalande för ögat som bidrar till att vi är stolta över vår verksamhet.    Vi har normalt öppet för service från lunch till och med middag. För att få vår verksamhet att fungera optimalt behöver vi förstärka vårt köksteam med dig som gillar mat och matlagning, har energi och snabbhet i kroppen och en tydlig känsla för kvalité.   Vi letar främst efter dig som har en ansvarfull och vuxen inställning till ditt arbete, har ett naturligt fokus på lösningar, blir glad av att jobba i team samtidigt som du har förmågan att självständigt leverera resultat när det krävs. Du behöver ha erfarenhet av att arbeta i varierat och ibland mycket högt tempo och vara bra på ordning och reda. Vi ser helst att du har kunskap om matlagning mer än i ditt eget kök men inte nödvändigtvis lång erfarenhet av fik/restaurant. För rätt person lägger vi gärna tid på upplärning ut ifrån verksamhetens behov.    • Gillar du att laga god mat?    • Har du förmågan att skapa struktur under press?   • Har energi i benen och ett snabbt huvud?    Då tror vi att du är den vi letar efter!   VI ERBJUDER • Engagemang och glädje. • Gamla stans trevligaste kunder och väldigt trivsam och mysig miljö. Vi har lika höga ambitioner för vår arbetsmiljö och trivsel som vi har för det vi serverar våra gäster, så hos oss kommer du vara viktig!   KRAV    • Arbetslivserfarenhet av att leverera kvalitet i högt tempo, minst ett år.  • Lösningsorienterad och strukturerad. • Positiv inställning till utmaningar. • God svenska i tal.       MERITERANDE • Tidigare erfarenhet av matlagning i professionell miljö. • Erfarenhet av service • Utbildning som kock eller kallskänka.  • Kunskap om regler kring hygien i kök.  • Positiv attityd till tillvaron.  • Körkort.       Vi är måna om att snabbt hitta rätt person och jobbet kommer tillsättas så fort som möjligt. Välkommen med din ansökan per mail till rekrytering@cafefika.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kallskänka** / köksbiträde Om jobbet   Str... | x | x | 10 | 10 | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| 2 | Kallskänka / **köksbiträde** Om jobbet   Stresstålig, snab... | x | x | 11 | 11 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 3 | ...nka / köksbiträde Om jobbet   **Stresstålig**, snabbfotad och har intresse ... | x | x | 11 | 11 | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| 4 | ...atlagning ?    Då kan du vara **Cafe** Fika and Wine´s nya team-medl... |  | x |  | 4 | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| 5 | ...m i köket. Vi är ett trevligt **café** med alkoholrättigheter som se... | x | x | 4 | 4 | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| 6 | ...r främst efter dig som har en **ansvarfull** och vuxen inställning till di... | x | x | 10 | 10 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 7 | ...idigt som du har förmågan att **självständigt** leverera resultat när det krä... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 8 | ... lika höga ambitioner för vår **arbetsmiljö** och trivsel som vi har för de... | x | x | 11 | 11 | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| 9 | ...r du vara viktig!   KRAV    • **Arbetslivserfarenhet** av att leverera kvalitet i hö... | x |  |  | 20 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| 10 | ... kvalitet i högt tempo, minst **ett år**.  • Lösningsorienterad och st... | x |  |  | 6 | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| 11 | ...llning till utmaningar. • God **svenska** i tal.       MERITERANDE • Ti... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 12 | ...t av service • Utbildning som **kock** eller kallskänka.  • Kunskap ... | x |  |  | 4 | [Kockutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/62ja_BdV_Xdo) |
| 12 | ...t av service • Utbildning som **kock** eller kallskänka.  • Kunskap ... |  | x |  | 4 | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| 13 | ...e • Utbildning som kock eller **kallskänka**.  • Kunskap om regler kring h... | x | x | 10 | 10 | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| 14 | ...iv attityd till tillvaron.  • **Körkort**.       Vi är måna om att snab... | x |  |  | 7 | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
| 14 | ...iv attityd till tillvaron.  • **Körkort**.       Vi är måna om att snab... |  | x |  | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| | **Overall** | | | **74** | **139** | 74/139 = **53%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Kockutbildning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/62ja_BdV_Xdo) |
| x | x | x | [Kallskänka, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/JECy_3w6_gah) |
| x |  |  | [1-2 års erfarenhet, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/LLnd_5GJ_4ju) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [kock, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/Ti2H_QoQ_EMX) |
| x |  |  | [körkort, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/UvDr_eNo_HSQ) |
|  | x |  | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| x | x | x | [Café, **skill**](http://data.jobtechdev.se/taxonomy/concept/f2Nt_4bK_HNX) |
| x | x | x | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| x | x | x | [hantera stress, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/z5Zo_DVb_VEK) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| x | x | x | [arbetsmiljö, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zdQY_QJh_nUH) |
| | | **7** | 7/13 = **54%** |