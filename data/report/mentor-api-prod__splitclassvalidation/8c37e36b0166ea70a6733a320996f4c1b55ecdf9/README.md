# Results for '8c37e36b0166ea70a6733a320996f4c1b55ecdf9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [8c37e36b0166ea70a6733a320996f4c1b55ecdf9](README.md) | 1 | 1852 | 8 | 9 | 89/125 = **71%** | 5/8 = **62%** |

## Source text

Kantpressare till kund i Nossebro! Om tjänsten Vi söker duktiga kantpressare till kund i Nossebro!  Arbetsmoment som ska utföras:  Du kommer att arbeta vid en kantbock, svart material i tjocklek 2-20mm och utifrån ritningsläsning.  För denna tjänst krävs inga programmeringskunskaper.   Arbetstider: Dagtid 7-16 Önskad starttid: Snarast eller enligt överenskommelse   Vid rätt person kommer tjänsten att övergå i anställning hos kund, du bör därför bo i närområdet.  Din profil Vi söker dig som har:    Tidigare arbetserfarenhet inom området  Kunskaper i ritningsläsning Viljan att lära och är intresserad Körkort B   Som person vill vi att du är ansvarstagande, målinriktad, flexibel, positiv, ordningsam och har en god samarbetsförmåga eftersom arbetet sker ute hos våra kunder. Vi arbetar löpande med inkomna ansökningar och kontaktar endast de som motsvarar ansökningskraven.  Välkommen med din ansökan idag!  Om Ikett Personalpartner AB Ikett Personalpartner AB är ett auktoriserat bemanningsföretag som arbetar med uthyrning och rekrytering av kvalificerad kollektivpersonal och tjänstemän. Främst inom yrkesområdena industri, el, bygg och administration.   Som konsult hos Ikett har du möjligheten att söka lokala arbeten och researbeten i Sverige samt Norge. Vi arbetar kontinuerligt för att du och kunden ska trivas så bra som möjligt med varandra och för att era önskemål ska tillgodoses.   Ikett är medlem i Almega Kompetensföretagen och har kollektivavtal med LO, Unionen och Akademikerförbunden. Ikett är certifierat enligt ISO 9001.  Målet med vårt arbete är nöjda kunder och skickliga medarbetare som trivs. Detta vill vi uppnå genom kvalitet, kompetens och effektivitet.  För mer information och nyheter, följ oss gärna på:  Facebook: www.facebook.com/ikettpersonalpartner  LinkedIn: www.linkedin.com/company/ikett-personalpartner-ab-as

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Kantpressare** till kund i Nossebro! Om tjän... | x | x | 12 | 12 | [Kantpressare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ubjR_VPU_wnv) |
| 2 | ... Om tjänsten Vi söker duktiga **kantpressare** till kund i Nossebro!  Arbets... | x | x | 12 | 12 | [Kantpressare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ubjR_VPU_wnv) |
| 3 | ...i tjocklek 2-20mm och utifrån **ritningsläsning**.  För denna tjänst krävs inga... | x | x | 15 | 15 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 4 | ...  För denna tjänst krävs inga **programmeringskunskaper**.   Arbetstider: Dagtid 7-16 Ö... |  | x |  | 23 | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| 5 | ...het inom området  Kunskaper i **ritningsläsning** Viljan att lära och är intres... | x | x | 15 | 15 | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| 6 | ...n att lära och är intresserad **Körkort** B   Som person vill vi att du... | x | x | 7 | 7 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 7 | ...ära och är intresserad Körkort** B**   Som person vill vi att du ä... | x |  |  | 2 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 8 | ... Som person vill vi att du är **ansvarstagande**, målinriktad, flexibel, posit... | x | x | 14 | 14 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 9 | ...m yrkesområdena industri, el, **bygg** och administration.   Som kon... | x |  |  | 4 | [Bygg, **occupation-collection**](http://data.jobtechdev.se/taxonomy/concept/ja7J_P8X_YC9) |
| 10 | ...ala arbeten och researbeten i **Sverige** samt Norge. Vi arbetar kontin... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 11 | ...ga Kompetensföretagen och har **kollektivavtal** med LO, Unionen och Akademike... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | **Overall** | | | **89** | **125** | 89/125 = **71%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x | x | x | [Ritningsläsning, **skill**](http://data.jobtechdev.se/taxonomy/concept/aHCV_VsV_LiD) |
| x | x | x | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
|  | x |  | [Programmering, **skill**](http://data.jobtechdev.se/taxonomy/concept/iUew_moc_BRk) |
| x |  |  | [Bygg, **occupation-collection**](http://data.jobtechdev.se/taxonomy/concept/ja7J_P8X_YC9) |
| x | x | x | [Kantpressare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/ubjR_VPU_wnv) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **5** | 5/8 = **62%** |