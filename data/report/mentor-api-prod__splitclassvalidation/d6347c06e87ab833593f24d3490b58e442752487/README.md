# Results for 'd6347c06e87ab833593f24d3490b58e442752487'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [d6347c06e87ab833593f24d3490b58e442752487](README.md) | 1 | 6329 | 46 | 45 | 122/1036 = **12%** | 15/58 = **26%** |

## Source text

Lagsamordnare till Markverkstaden Skövde Vi står inför en stor tillväxt och söker dig som har erfarenhet av arbetsledande arbete inom fordonsbranschen. Har du dessutom ett allmänt teknikintresse och tycker om att jobba varierat?   Då kan detta vara ett passande jobb för dig!  Vad kan vi erbjuda dig  Hos oss får du möjlighet att arbeta med spännande arbetsuppgifter för att bidra till att göra Sverige och världen lite säkrare. Vi värdesätter ömsesidigt förtroende och hos oss får du både ett eget ansvar och goda möjligheter att kompetensutvecklas. Våra medarbetare är viktiga för oss och du har möjlighet att ha en bra balans mellan arbetsliv och privatliv. Vi ger även möjlighet att träna på arbetstid, generösa arbetstidsavtal, föräldralön samt upp till sju veckor semester per år.  Markverkstad Skövde är en avdelning inom FMTS Markverkstad med cirka 170 medarbetare.  Vi utför underhåll på Försvarsmaktens markmateriel och levererar tekniskt systemstöd. Detta innebär underhåll i form av tillsyn, reparationer, tillverkning, demontering/skrotning, modifieringar, driftstöd, teknisk anpassning och materielundersökning. Du kommer att tillhöra Sektionen Hjulfordon och arbeta tillsammans med en lagsamordnare och 25 andra fordonsmekaniker.    Arbetsuppgifter  Som lagsamordnare samordnar, leder och fördelar du det dagliga arbetet inom arbetslaget utifrån de direktiv som anges av närmaste chef , TKM samt utifrån MvE verksamhetsledningssystem. Du ansvarar för att genomföra beredning , reservdels beställning på det du har berett och stödja med planering av produktion och återrapportering av genomförd underhållsproduktion.  Du arbetsleder medarbetarna i ditt lag i det dagliga arbetet som främst består av reparation och underhåll av Försvarsmarsmaktens hjulfordon. Du kan också stödja mekaniker i felsökning och beslut om reparationsmetoder samt till viss del utföra reparationer/besiktning/statusbedömning och ev. provkörning av olika fordon.  Kvalifikationer   • Du har lägst två- eller treårig gymnasieutbildning inom fordonsteknik, inriktning fordon eller annan utbildning/kompetens förvärvad genom yrkeserfarenhet som arbetsgivaren bedömer som likvärdig • Aktuell erfarenhet av liknande arbete • Goda kunskaper om fordonsreparationer och felsökning • Datorvana med kunskaper i MS Officepaketet, främst Word och Excel • B-körkort  Meriterande   • Erfarenhet av arbete med Försvarsmaktens materiel och fordon • Erfarenhet av arbetat i en arbetsledande roll • C-körkort, CE körkort • Kunskaper i affärssystemet SAP  Personliga egenskaper  Som person är du trygg, stabil, har självinsikt och du arbetar bra självständigt likväl som i grupp. Du är strukturerad, beslutsam, planerar ditt arbete och gör tydliga prioriteringar. Du har samtidigt ett arbetssätt som motiverar och målstyr teamet. I rollen som lagsamordnare är det viktigt att du är kommunikativ och kan förmedla ett budskap tydligt och konkret. Vidare tar du ansvar för resultatet, tar egna initiativ och ansvar.  Vi söker dig som har en positiv attityd till arbetet och verksamheten. Vi arbetar för en jämnare könsfördelning och ser gärna kvinnliga sökanden.  Stor vikt kommer att läggas vid personlig lämplighet.   För att myndighetens uppdrag ska vara framgångsrikt förutsätts att alla medarbetare uppträder enligt den värdegrund som finns. Försvarsmaktens värdegrund slår vakt om alla människors lika värde, rättvisa och jämlikhet och främjar demokrati och mänskliga rättigheter (läs mer på www.forsvarsmakten.se).  Övrigt  Anställningsform: tillsvidareanställning som inleds med sex månaders provanställning Sysselsättningsgrad: heltid, dagtid   Arbetsort: Skövde  Tillträdesdatum: snarast efter överenskommelse  Arbete medför även krav på resor och tjänstgöring, såväl inrikes som utrikes.  Tjänsten är en civil befattning.  Upplysningar om befattningen  Sektionschef Jan Åberg 070-918 37 81 (Semester vecka 29-33)  Information om rekryteringsprocessen  HR Johanna Jansson 073-334 24 00 (Semester vecka 27-32)  Fackliga företrädare  SACO: Bengt Blom  OFR-S: Tony Svensson  SEKO: Gerard Woll  OFR-O: Anders Norén  Samtliga kontaktas via tfn 035-266 20 00  Sista ansökningsdag  Välkommen med din ansökan senast 2022-08-21. Din ansökan bör innehålla CV samt ansökningsbrev där du motiverar varför du är lämpad för denna befattning.       Ansökningar till denna befattning kommer endast tas emot via Försvarsmaktens webbplats.  -------------------------------------------------------- Information om Försvarsmakten och det rekryterande förbandet:  Försvarsmaktens tekniska skola med huvudort Halmstad, gör tekniken tillgänglig i Försvarsmakten genom teknisk utbildning, funktionsutveckling, verkstadsproduktion och insatsverksamhet. Här finns spännande utvecklingsmöjligheter för den som är tekniskt intresserad, både på grundläggande och mycket avancerad nivå. FMTS utbildar officerare, specialistofficerare, civilanställda i teknisk tjänst inom samtliga försvarsgrenar. Utöver det så utbildas soldater inom logistik. FMTS har verksamhet på 23 orter i Sverige, från Boden i norr till Revingehed i söder.  Varje dag och varje timme gör våra medarbetare insatser som påverkar andra människors liv, både i stort och smått, och både på hemmaplan och runt om i världen.  I Försvarsmakten finns en stark värdegrund som bygger på öppenhet, resultat och ansvar. Professionell utveckling och personlig hälsa värdesätts och uppmuntras. Det finns goda förutsättningar för intern karriärrörlighet, friskvård samt bra balans mellan arbete och privatliv.  Försvarsmakten tillvaratar de kvaliteter som mångfald och jämn könsfördelning tillför verksamheten. Vi välkomnar därför sökanden med olika bakgrund och erfarenheter i våra rekryteringar.  En anställning hos oss innebär placering i säkerhetsklass. Vanligtvis krävs svenskt medborgarskap. Säkerhetsprövning med registerkontroll kommer att genomföras före anställning enligt 3 kap i säkerhetsskyddslagen. Med anställning följer en skyldighet att krigsplaceras. I anställningen ingår även en skyldighet att tjänstgöra utomlands. Innebörden av detta varierar beroende på typ av befattning.  Till ansökan om anställning ska CV och personligt brev bifogas. Om du går vidare i anställningsprocessen ska alltid vidimerade kopior av betyg och intyg uppvisas.  Samtal från externa rekryteringsföretag och säljare undanbedes.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Lagsamordnare** till Markverkstaden Skövde Vi... |  | x |  | 13 | [Lagsamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/B2Gy_2EF_8Sp) |
| 2 | ...amordnare till Markverkstaden **Skövde** Vi står inför en stor tillväx... | x | x | 6 | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 3 | ...r för att bidra till att göra **Sverige** och världen lite säkrare. Vi ... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 4 | ...t att ha en bra balans mellan **arbetsliv** och privatliv. Vi ger även mö... |  | x |  | 9 | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| 5 | ...emester per år.  Markverkstad **Skövde** är en avdelning inom FMTS Mar... | x | x | 6 | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 6 | ...a innebär underhåll i form av **tillsyn**, reparationer, tillverkning, ... | x |  |  | 7 | [Fordonsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/cq2h_mQV_qhi) |
| 7 | ... underhåll i form av tillsyn, **reparationer**, tillverkning, demontering/sk... | x |  |  | 12 | [utföra reparation av fordon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cnN4_QUm_5Kj) |
| 8 | ...orm av tillsyn, reparationer, **tillverkning**, demontering/skrotning, modif... |  | x |  | 12 | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
| 8 | ...orm av tillsyn, reparationer, **tillverkning**, demontering/skrotning, modif... | x |  |  | 12 | [fordonstillverkningsprocess, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zvXC_RJ3_XtP) |
| 9 | ..., reparationer, tillverkning, **demontering**/skrotning, modifieringar, dri... | x | x | 11 | 11 | [Demontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/f8Sz_Xaf_TEU) |
| 10 | ...ner, tillverkning, demontering**/skrotning**, modifieringar, driftstöd, te... | x |  |  | 10 | [Demontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/f8Sz_Xaf_TEU) |
| 11 | ...er, tillverkning, demontering/**skrotning**, modifieringar, driftstöd, te... |  | x |  | 9 | [Skrotning, maskinell, bergarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/r4Te_Mzt_yrC) |
| 12 | ...kning, demontering/skrotning, **modifieringar**, driftstöd, teknisk anpassnin... | x |  |  | 13 | [Fordonsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/cCMP_ufr_9Ke) |
| 13 | ...ing/skrotning, modifieringar, **driftstöd**, teknisk anpassning och mater... | x |  |  | 9 | [Fordonsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/cCMP_ufr_9Ke) |
| 14 | ...ng, modifieringar, driftstöd, **teknisk anpassning** och materielundersökning. Du ... | x |  |  | 18 | [Fordonsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/cCMP_ufr_9Ke) |
| 15 | ...tstöd, teknisk anpassning och **materielundersökning**. Du kommer att tillhöra Sekti... | x |  |  | 20 | [Materialteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/YHb7_uwj_7NG) |
| 16 | ...och arbeta tillsammans med en **lagsamordnare** och 25 andra fordonsmekaniker... |  | x |  | 13 | [Lagsamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/B2Gy_2EF_8Sp) |
| 17 | ...en lagsamordnare och 25 andra **fordonsmekaniker**.    Arbetsuppgifter  Som lags... | x | x | 16 | 16 | [Fordonsmekaniker och reparatörer m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/wdJT_znW_3Te) |
| 18 | ...iker.    Arbetsuppgifter  Som **lagsamordnare** samordnar, leder och fördelar... |  | x |  | 13 | [Lagsamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/B2Gy_2EF_8Sp) |
| 19 | ...suppgifter  Som lagsamordnare **samordnar, leder och fördelar du det dagliga arbetet inom arbetslaget** utifrån de direktiv som anges... | x |  |  | 69 | [leda anställda, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/isL4_Ze4_wsE) |
| 20 | ...Du ansvarar för att genomföra **beredning** , reservdels beställning på d... | x |  |  | 9 | [Beredning, verkstadsmaskiner, **skill**](http://data.jobtechdev.se/taxonomy/concept/J8Sg_Pqv_Dtd) |
| 21 | ...för att genomföra beredning , **reservdels beställning** på det du har berett och stöd... | x |  |  | 22 | [se till att reservdelar finns tillgängliga, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sSKY_rdM_hF9) |
| 22 | ... du har berett och stödja med **planering av produktion** och återrapportering av genom... | x |  |  | 23 | [delta i produktionsplanläggning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LL7E_2zE_9sK) |
| 23 | ...d planering av produktion och **återrapportering** av genomförd underhållsproduk... | x |  |  | 16 | [rapportera om verksamheten, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jSmC_RhK_Abi) |
| 24 | ...örd underhållsproduktion.  Du **arbetsleder medarbetarna** i ditt lag i det dagliga arbe... | x |  |  | 24 | [leda anställda, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/isL4_Ze4_wsE) |
| 25 | ... arbetet som främst består av **reparation och underhåll** av Försvarsmarsmaktens hjulfo... | x |  |  | 24 | [underhåll och reparation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i19U_G6R_t19) |
| 26 | ... kan också stödja mekaniker i **felsökning** och beslut om reparationsmeto... |  | x |  | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 26 | ... kan också stödja mekaniker i **felsökning** och beslut om reparationsmeto... | x |  |  | 10 | [Felsökning, bilmekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/qQms_DVy_WhS) |
| 27 | ...tning/statusbedömning och ev. **provkörning** av olika fordon.  Kvalifikati... | x | x | 11 | 11 | [hantera provkörningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cwZC_VRz_yeS) |
| 28 | ....  Kvalifikationer   • Du har **lägst två- eller treårig gymnasieutbildning** inom fordonsteknik, inriktnin... | x |  |  | 43 | [Gymnasial utbildning, 2 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/GNkn_NLL_SFQ) |
| 29 | ... har lägst två- eller treårig **gymnasieutbildning** inom fordonsteknik, inriktnin... |  | x |  | 18 | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| 30 | ...eårig gymnasieutbildning inom **fordonsteknik**, inriktning fordon eller anna... |  | x |  | 13 | [Fordons- och farkostteknik, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/4VCJ_KsY_p5G) |
| 30 | ...eårig gymnasieutbildning inom **fordonsteknik**, inriktning fordon eller anna... |  | x |  | 13 | [Annan utbildning i fordons- och farkostteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/9fxM_ZJm_vdc) |
| 30 | ...eårig gymnasieutbildning inom **fordonsteknik**, inriktning fordon eller anna... |  | x |  | 13 | [Civilingenjörsutbildning, fordons- och farkostteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/GXfx_auf_8Hk) |
| 30 | ...eårig gymnasieutbildning inom **fordonsteknik**, inriktning fordon eller anna... | x | x | 13 | 13 | [Fordonsteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/MpMo_yet_PdK) |
| 30 | ...eårig gymnasieutbildning inom **fordonsteknik**, inriktning fordon eller anna... |  | x |  | 13 | [Fordonsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/cCMP_ufr_9Ke) |
| 30 | ...eårig gymnasieutbildning inom **fordonsteknik**, inriktning fordon eller anna... |  | x |  | 13 | [Yrkesteknisk högskoleutbildning, fordonsteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/haU4_MnM_WcE) |
| 30 | ...eårig gymnasieutbildning inom **fordonsteknik**, inriktning fordon eller anna... |  | x |  | 13 | [fordonsteknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qgtu_bij_woC) |
| 30 | ...eårig gymnasieutbildning inom **fordonsteknik**, inriktning fordon eller anna... |  | x |  | 13 | [Ingenjörsutbildning, fordons- och farkostteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/wHMQ_pPy_pQQ) |
| 31 | ...de arbete • Goda kunskaper om **fordonsreparationer** och felsökning • Datorvana me... |  | x |  | 19 | [övervaka fordonsreparationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7rcy_jag_S4E) |
| 31 | ...de arbete • Goda kunskaper om **fordonsreparationer** och felsökning • Datorvana me... | x |  |  | 19 | [utföra reparation av fordon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cnN4_QUm_5Kj) |
| 31 | ...de arbete • Goda kunskaper om **fordonsreparationer** och felsökning • Datorvana me... |  | x |  | 19 | [utföra improviserade fordonsreparationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/p4TB_6bM_6yR) |
| 31 | ...de arbete • Goda kunskaper om **fordonsreparationer** och felsökning • Datorvana me... |  | x |  | 19 | [utföra mindre fordonsreparationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uPTg_RWE_yhr) |
| 32 | ...er om fordonsreparationer och **felsökning** • Datorvana med kunskaper i M... |  | x |  | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 32 | ...er om fordonsreparationer och **felsökning** • Datorvana med kunskaper i M... | x |  |  | 10 | [Felsökning, bilmekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/qQms_DVy_WhS) |
| 33 | ...reparationer och felsökning • **Datorvana** med kunskaper i MS Officepake... | x |  |  | 9 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 34 | ...g • Datorvana med kunskaper i **MS **Officepaketet, främst Word och... | x |  |  | 3 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 35 | ... Datorvana med kunskaper i MS **Officepaketet**, främst Word och Excel • B-kö... | x | x | 13 | 13 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 36 | ...er i MS Officepaketet, främst **Word** och Excel • B-körkort  Merite... | x | x | 4 | 4 | [MS Word, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Ei3_fZV_YKM) |
| 37 | ...fficepaketet, främst Word och **Excel** • B-körkort  Meriterande   • ... | x | x | 5 | 5 | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| 38 | ...etet, främst Word och Excel • **B-körkort**  Meriterande   • Erfarenhet a... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 39 | ... • Erfarenhet av arbetat i en **arbetsledande roll** • C-körkort, CE körkort • Kun... | x |  |  | 18 | [teknisk ledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/fvDc_4K3_f7f) |
| 40 | ...tat i en arbetsledande roll • **C-körkort**, CE körkort • Kunskaper i aff... | x | x | 9 | 9 | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
| 41 | ...betsledande roll • C-körkort, **CE körkort** • Kunskaper i affärssystemet ... | x | x | 10 | 10 | [CE, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/zZu8_iZ9_wMH) |
| 42 | ...ort, CE körkort • Kunskaper i **affärssystemet **SAP  Personliga egenskaper  So... |  | x |  | 15 | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
| 43 | ... • Kunskaper i affärssystemet **SAP**  Personliga egenskaper  Som p... | x | x | 3 | 3 | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
| 44 | ...tabil, har självinsikt och du **arbetar bra självständigt** likväl som i grupp. Du är str... | x |  |  | 25 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 45 | ... målstyr teamet. I rollen som **lagsamordnare** är det viktigt att du är komm... |  | x |  | 13 | [Lagsamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/B2Gy_2EF_8Sp) |
| 46 | ...nare är det viktigt att du är **kommunikativ** och kan förmedla ett budskap ... | x |  |  | 12 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 47 | ...tatet, tar egna initiativ och **ansvar**.  Vi söker dig som har en pos... | x |  |  | 6 | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
| 48 | ...het och främjar demokrati och **mänskliga rättigheter** (läs mer på www.forsvarsmakte... |  | x |  | 21 | [Mänskliga rättigheter, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hJAv_oR2_rLa) |
| 49 | ...).  Övrigt  Anställningsform: **tillsvidareanställning** som inleds med sex månaders p... | x |  |  | 22 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 50 | ...tällning Sysselsättningsgrad: **heltid**, dagtid   Arbetsort: Skövde  ... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 51 | ...: heltid, dagtid   Arbetsort: **Skövde**  Tillträdesdatum: snarast eft... | x | x | 6 | 6 | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| 52 | ...--------------------------- In**formation om Försva**rsmakten och det rekryterande ... |  | x |  | 19 | [tillämpa funktionsprogrammering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nAcT_8Ci_Z9S) |
| 53 | ...--- Information om Försvarsmak**ten och det rekryt**erande förbandet:  Försvarsmak... |  | x |  | 18 | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| 54 | ...formation om Försvarsmakten oc**h det re**kryterande förbandet:  Försvar... |  | x |  | 8 | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| 55 | ...s tekniska skola med huvudort **Halmstad**, gör tekniken tillgänglig i F... | x |  |  | 8 | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| 56 | ...h insatsverksamhet. Här finns **spännande utveckling**smöjligheter för den som är te... |  | x |  | 20 | [Specialistofficer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EsHf_niD_7PB) |
| 57 | ...och mycket avancerad nivå. FMT**S utbild**ar officerare, specialistoffic... |  | x |  | 8 | [Soldater m.fl., **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/MUoR_1Bx_A2P) |
| 58 | ...TS utbildar officerare, specia**listoffi**cerare, civilanställda i tekni... |  | x |  | 8 | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| 59 | ...ga försvarsgrenar. Utöver det **så utbi**ldas soldater inom logistik. F... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 60 | ...rsvarsgrenar. Utöver det så ut**bilda**s soldater inom logistik. FMTS... |  | x |  | 5 | [Boden, **municipality**](http://data.jobtechdev.se/taxonomy/concept/y4NQ_tnB_eVd) |
| 61 | ...t på 23 orter i Sverige, från **Boden** i norr till Revingehed i söde... | x |  |  | 5 | [Boden, **municipality**](http://data.jobtechdev.se/taxonomy/concept/y4NQ_tnB_eVd) |
| 62 | ...värdegrund som bygger på öppen**het, **resultat och ansvar. Professio... |  | x |  | 5 | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
| 63 | ...erhetsklass. Vanligtvis krävs **svenskt medborgarskap**. Säkerhetsprövning med regist... | x |  |  | 21 | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| | **Overall** | | | **122** | **1036** | 122/1036 = **12%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Fordons- och farkostteknik, **sun-education-field-3**](http://data.jobtechdev.se/taxonomy/concept/4VCJ_KsY_p5G) |
| x | x | x | [MS Excel, **skill**](http://data.jobtechdev.se/taxonomy/concept/5wab_aFK_gdL) |
| x | x | x | [MS Word, **skill**](http://data.jobtechdev.se/taxonomy/concept/6Ei3_fZV_YKM) |
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
|  | x |  | [övervaka fordonsreparationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/7rcy_jag_S4E) |
|  | x |  | [Annan utbildning i fordons- och farkostteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/9fxM_ZJm_vdc) |
|  | x |  | [Hälsa, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Abys_5p1_Cnq) |
|  | x |  | [Lagsamordnare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/B2Gy_2EF_8Sp) |
| x | x | x | [C, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/BKCx_hST_Pcm) |
| x |  |  | [visa professionellt ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/DRjM_LSF_w8x) |
|  | x |  | [Specialistofficer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EsHf_niD_7PB) |
| x |  |  | [Gymnasial utbildning, 2 år, **sun-education-level-2**](http://data.jobtechdev.se/taxonomy/concept/GNkn_NLL_SFQ) |
|  | x |  | [Civilingenjörsutbildning, fordons- och farkostteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/GXfx_auf_8Hk) |
| x |  |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
|  | x |  | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| x |  |  | [Beredning, verkstadsmaskiner, **skill**](http://data.jobtechdev.se/taxonomy/concept/J8Sg_Pqv_Dtd) |
| x |  |  | [delta i produktionsplanläggning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/LL7E_2zE_9sK) |
|  | x |  | [Soldater m.fl., **ssyk-level-2**](http://data.jobtechdev.se/taxonomy/concept/MUoR_1Bx_A2P) |
| x | x | x | [Fordonsteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/MpMo_yet_PdK) |
|  | x |  | [Annan utbildning inom teknik och teknisk industri, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/NKNc_sE8_2ok) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Arbetsliv, **keyword**](http://data.jobtechdev.se/taxonomy/concept/UceZ_JCp_23T) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [Materialteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/YHb7_uwj_7NG) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x | x | x | [Fordonsteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/cCMP_ufr_9Ke) |
| x |  |  | [Svenskt medborgarskap, **skill**](http://data.jobtechdev.se/taxonomy/concept/chKz_HpD_XL5) |
| x |  |  | [utföra reparation av fordon, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cnN4_QUm_5Kj) |
| x |  |  | [Fordonsvård, **skill**](http://data.jobtechdev.se/taxonomy/concept/cq2h_mQV_qhi) |
| x | x | x | [hantera provkörningar, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/cwZC_VRz_yeS) |
| x | x | x | [Demontering, **skill**](http://data.jobtechdev.se/taxonomy/concept/f8Sz_Xaf_TEU) |
| x | x | x | [Skövde, **municipality**](http://data.jobtechdev.se/taxonomy/concept/fqAy_4ji_Lz2) |
| x |  |  | [teknisk ledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/fvDc_4K3_f7f) |
|  | x |  | [Mänskliga rättigheter, **keyword**](http://data.jobtechdev.se/taxonomy/concept/hJAv_oR2_rLa) |
|  | x |  | [Yrkesteknisk högskoleutbildning, fordonsteknik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/haU4_MnM_WcE) |
| x |  |  | [underhåll och reparation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/i19U_G6R_t19) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [SAP, applikationsplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/iGBs_5FQ_Kk9) |
| x |  |  | [leda anställda, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/isL4_Ze4_wsE) |
| x |  |  | [rapportera om verksamheten, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/jSmC_RhK_Abi) |
| x | x | x | [Halmstad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/kUQB_KdK_kAh) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
|  | x |  | [tillämpa funktionsprogrammering, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/nAcT_8Ci_Z9S) |
|  | x |  | [utföra improviserade fordonsreparationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/p4TB_6bM_6yR) |
| x |  |  | [Felsökning, bilmekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/qQms_DVy_WhS) |
|  | x |  | [fordonsteknik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/qgtu_bij_woC) |
|  | x |  | [Skrotning, maskinell, bergarbete, **skill**](http://data.jobtechdev.se/taxonomy/concept/r4Te_Mzt_yrC) |
| x |  |  | [se till att reservdelar finns tillgängliga, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/sSKY_rdM_hF9) |
|  | x |  | [Tillverkning, **sni-level-1**](http://data.jobtechdev.se/taxonomy/concept/trPt_iHy_oFP) |
|  | x |  | [utföra mindre fordonsreparationer, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/uPTg_RWE_yhr) |
| x | x | x | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
|  | x |  | [Ingenjörsutbildning, fordons- och farkostteknik, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/wHMQ_pPy_pQQ) |
| x | x | x | [Fordonsmekaniker och reparatörer m.fl., **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/wdJT_znW_3Te) |
|  | x |  | [Gymnasial utbildning, **sun-education-level-1**](http://data.jobtechdev.se/taxonomy/concept/wyLL_K3a_HRC) |
| x | x | x | [Boden, **municipality**](http://data.jobtechdev.se/taxonomy/concept/y4NQ_tnB_eVd) |
|  | x |  | [Logistik, **skill**](http://data.jobtechdev.se/taxonomy/concept/yXmf_iKi_7ci) |
| x | x | x | [CE, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/zZu8_iZ9_wMH) |
| x |  |  | [fordonstillverkningsprocess, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zvXC_RJ3_XtP) |
| | | **15** | 15/58 = **26%** |