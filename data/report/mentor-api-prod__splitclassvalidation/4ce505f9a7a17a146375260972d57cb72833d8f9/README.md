# Results for '4ce505f9a7a17a146375260972d57cb72833d8f9'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4ce505f9a7a17a146375260972d57cb72833d8f9](README.md) | 1 | 2018 | 17 | 9 | 52/205 = **25%** | 5/16 = **31%** |

## Source text

Junior projektledare Vi söker dig som är en lagspelare, samarbetsvillig och prestigelös. I detta projekt efterfrågas förmågor som lösningsorienterade och samtidigt ett bra detaljfokus. Som junior projektledare rapporterar du till senior projektledare och fördelar din tid mellan två arbetsflöden, operativ förändring samt kommunikation och support. Du kommer ge stöd till butikskommunikation i form av att att säkerställa en plan gällande informationsspridning, utbildning och budskap till butiken, inom affärsutveckling kommer du att vara en samordnande part mellan reklam, IT och Profil.   Vi söker dig som har erfarenhet av att stötta, koordinera och skapa framsteg i flera parallella projekt under ledning av huvudprojektledaren. Du har även erfarenhet av att leda och stötta medarbetare som har möjlighet att bidra med en del av sin tid till detta projekt. Arbetsspråket är svenska och engelska.  Start omgående, heltid, tillsvidare.  Din ansökan  Låter rollen intressant och passande? Ansök i så fall omgående för vi intervjuar löpande och rollen kan tillsättas innan sista ansökningsdatum.   Vi är flera rekryterare som hanterar rekryteringen av denna tjänst, vid frågor gällande den här rekryteringen eller tjänsten når du oss enklast på e-post info@quest.se   Vi kan enbart ta emot och bearbeta din ansökan genom att du registrerar ditt CV i vår portal. Med avseende på GDPR kan vi ej ta emot ansökningar via e-post.   Varmt välkommen med din ansökan!   Uppdraget är en del av Quest Consulting personaluthyrning. Du kommer att vara anställd av Quest Consulting och arbeta som konsult hos vår kund.  Om Quest Consulting  Quest Consulting är ett auktoriserat konsultbolag med kollektivavtal, försäkringar, friskvård och tjänstepension. Vi är specialiserade inom IT, Teknik, HR, Administration och Ekonomi. Vår målsättning är att vara din personliga samarbetspartner och just därför är det så viktigt för oss att arbeta efter våra kärnvärden där våra ledord är att vara Personliga, Nyskapande och Professionella.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Junior **projektledare** Vi söker dig som är en lagspe... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 1 | Junior **projektledare** Vi söker dig som är en lagspe... | x |  |  | 13 | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
| 2 | ...t bra detaljfokus. Som junior **projektledare** rapporterar du till senior pr... |  | x |  | 13 | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| 2 | ...t bra detaljfokus. Som junior **projektledare** rapporterar du till senior pr... | x |  |  | 13 | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
| 3 | ...re rapporterar du till senior **projektledare** och fördelar din tid mellan t... | x |  |  | 13 | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
| 4 | ...pport. Du kommer ge stöd till **butikskommunikation** i form av att att säkerställa... |  | x |  | 19 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 4 | ...pport. Du kommer ge stöd till **butikskommunikation** i form av att att säkerställa... | x |  |  | 19 | [Butikskommunikation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/nYan_zib_hXG) |
| 5 | ...ch budskap till butiken, inom **affärsutveckling** kommer du att vara en samordn... | x | x | 16 | 16 | [Affärsutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TzHP_38y_mhm) |
| 6 | ...mordnande part mellan reklam, **IT** och Profil.   Vi söker dig so... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 7 | ...tta projekt. Arbetsspråket är **svenska** och engelska.  Start omgående... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 8 | ... Arbetsspråket är svenska och **engelska**.  Start omgående, heltid, til... | x | x | 8 | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 9 | ...ch engelska.  Start omgående, **heltid**, tillsvidare.  Din ansökan  L... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| 10 | ...ska.  Start omgående, heltid, **tillsvidare**.  Din ansökan  Låter rollen i... | x |  |  | 11 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 11 | ...nsökningsdatum.   Vi är flera **rekryterare** som hanterar rekryteringen av... | x |  |  | 11 | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| 12 | ...est Consulting och arbeta som **konsult** hos vår kund.  Om Quest Consu... | x |  |  | 7 | [Konsulttjänster till företag, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/APkH_QHR_9X2) |
| 13 | ...auktoriserat konsultbolag med **kollektivavtal**, försäkringar, friskvård och ... | x | x | 14 | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 14 | ...kollektivavtal, försäkringar, **friskvård** och tjänstepension. Vi är spe... | x |  |  | 9 | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| 15 | ...on. Vi är specialiserade inom **IT**, Teknik, HR, Administration o... | x |  |  | 2 | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| 16 | ...ecialiserade inom IT, Teknik, **HR**, Administration och Ekonomi. ... | x |  |  | 2 | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
| 17 | ...eknik, HR, Administration och **Ekonomi**. Vår målsättning är att vara ... | x | x | 7 | 7 | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| | **Overall** | | | **52** | **205** | 52/205 = **25%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Konsulttjänster till företag, **sni-level-3**](http://data.jobtechdev.se/taxonomy/concept/APkH_QHR_9X2) |
| x | x | x | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x | x | x | [Ekonomi, **keyword**](http://data.jobtechdev.se/taxonomy/concept/PW4f_bmT_new) |
| x |  |  | [Rekryterare/Rekryteringskonsult, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/QaQC_ozP_Bme) |
| x | x | x | [Affärsutveckling, **skill**](http://data.jobtechdev.se/taxonomy/concept/TzHP_38y_mhm) |
| x |  |  | [HR, **keyword**](http://data.jobtechdev.se/taxonomy/concept/YxV3_BQC_eMP) |
|  | x |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x |  |  | [Data/IT, **occupation-field**](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [Butikskommunikation, **keyword**](http://data.jobtechdev.se/taxonomy/concept/nYan_zib_hXG) |
|  | x |  | [projektledare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/nzMp_xN5_r6G) |
| x |  |  | [Friskvård, **keyword**](http://data.jobtechdev.se/taxonomy/concept/yoHe_xjK_qjH) |
| x |  |  | [Projektledare, IT, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/z5AM_ayf_WcL) |
| x | x | x | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **5** | 5/16 = **31%** |