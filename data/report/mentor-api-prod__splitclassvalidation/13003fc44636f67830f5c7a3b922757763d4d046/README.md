# Results for '13003fc44636f67830f5c7a3b922757763d4d046'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [13003fc44636f67830f5c7a3b922757763d4d046](README.md) | 1 | 2649 | 23 | 22 | 47/621 = **8%** | 5/32 = **16%** |

## Source text

VILL DU HA ETT SPÄNNANDE OCH VÄLBETALT ARBETE SOM GÖR DIG HET PÅ ARBETSMARKNADEN? ÄR DU DUKTIG PÅ PROBLEMLÖSNING OCH ÖNSKAR ETT KREATIVT OCH OMVÄXLANDE ARBETE? DÅ SKA DU UTBILDNA DIG TILL UNDERHÅLLSTEKNIKER!    Underhållstekniker inom automatiserad tillverkningsindustri är en Yrkeshögskoleutbildning på 1,5 år som ger dig breda kunskaper i bl a elteknik, mekanik, automation och underhållsplanering. De flesta utbildningar inom underhållsteknik är antingen mot el- eller mekanikområdet men denna utbildning ger dig kompetens inom båda områdena. Efter avslutad utbildning kommer du att vara en multikompetent underhållstekniker.    Som underhållstekniker är du en viktig lagspelare med flera olika arbetsuppgifter. Du kommer framförallt att sköta underhållsplaneringen och det förebyggande underhållet av maskinparken samt hjälpa maskinoperatörerna i deras arbete att underhålla maskinerna för att undvika att fel uppstår. Underhållsarbetet dokumenteras i en underhållsplan och administreras med ett underhållssystem. Ibland uppstår fel på maskinerna trots att man underhåller dem och då ska underhållsteknikern snabbt hitta felet, genom olika metoder av felsökning och se till att maskinen fungerar igen. En underhållstekniker arbetar både självständigt och i team och samarbetar med både produktionsledningen, driftspersonal och kunder. Utbildningen innehåller därför även kurser inom arbetsledning, organisation och kommunikation.    ARBETSGIVARNA BEHÖVER DIG    Efterfrågan på underhållstekniker är mycket stor och arbetsplatserna är många inom både energi-, process- och tillverkningsindustrin. Du kan även arbeta på företag som utför underhåll på uppdrag åt olika kunder, vilket kan innebära resor både inom och utanför Sverige. Utbildningen samarbetar med ett flertal företag t ex Trioplats.    UTBILDNINGSUPPLÄGG - NÄRA SAMARBETE MED NÄRINGSLIVET    Utbildningen är 1,5 år och har tre skolförlagda dagar i veckan. Övriga två dagar är självstudiedagar. Du kan välja om du vill läsa utbildningen i Gislaved eller Vetlanda. Utbildningen genomsyras av ett nära samarbete med näringslivet, via föreläsningar, studiebesök och fältövningar. Cirka 25% av utbildningen är arbetsplatsförlagd (LIA) till ett företag inom branschen. Under LIA-perioderna får du arbetslivserfarenhet och kontakter som ofta leder till jobb direkt efter examen.    KURSER I UTBILDNINGEN    Introduktionskurs    Ellära för teoretiks B-behörighet    Pneumatik och hydraulik    Mekanik, material och tribologi    LIA 1 (Lärande i arbete)    Automation och styrteknik    Underhållsplanering    Arbetsledning och kommunikation    LIA 2 (Lärande i arbete)    Examensarbete       

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | ...G TILL UNDERHÅLLSTEKNIKER!    **Underhållstekniker** inom automatiserad tillverkni... | x |  |  | 18 | [Utbildning till underhållstekniker (YH), **keyword**](http://data.jobtechdev.se/taxonomy/concept/LcZM_TLH_Mi6) |
| 2 | ...lstekniker inom automatiserad **tillverkningsindustri** är en Yrkeshögskoleutbildning... |  | x |  | 21 | [Konstruktör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/44U4_7Fz_GCy) |
| 2 | ...lstekniker inom automatiserad **tillverkningsindustri** är en Yrkeshögskoleutbildning... |  | x |  | 21 | [Processtekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4eYC_a1M_xtL) |
| 2 | ...lstekniker inom automatiserad **tillverkningsindustri** är en Yrkeshögskoleutbildning... |  | x |  | 21 | [Civilingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/84z6_pWk_XJQ) |
| 2 | ...lstekniker inom automatiserad **tillverkningsindustri** är en Yrkeshögskoleutbildning... |  | x |  | 21 | [Utvecklingsingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8r8B_8bP_usp) |
| 2 | ...lstekniker inom automatiserad **tillverkningsindustri** är en Yrkeshögskoleutbildning... |  | x |  | 21 | [Automationstekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CC4H_YCh_77T) |
| 2 | ...lstekniker inom automatiserad **tillverkningsindustri** är en Yrkeshögskoleutbildning... |  | x |  | 21 | [Produktionsledare, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NaMW_ie6_VVV) |
| 2 | ...lstekniker inom automatiserad **tillverkningsindustri** är en Yrkeshögskoleutbildning... |  | x |  | 21 | [Kalibreringsingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/RU3Y_Gqt_h3c) |
| 2 | ...lstekniker inom automatiserad **tillverkningsindustri** är en Yrkeshögskoleutbildning... |  | x |  | 21 | [Kundserviceingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cPyP_GEv_NJ5) |
| 2 | ...lstekniker inom automatiserad **tillverkningsindustri** är en Yrkeshögskoleutbildning... |  | x |  | 21 | [Produktionsplanerare, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zmTy_qPb_e9e) |
| 3 | ...d tillverkningsindustri är en **Yrkeshögskoleutbildning på 1,5 år** som ger dig breda kunskaper i... | x |  |  | 33 | [Högskoleutbildning, yrkesinriktad, kortare än 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/oroB_8GN_Fe5) |
| 4 | ...er dig breda kunskaper i bl a **elteknik**, mekanik, automation och unde... |  | x |  | 8 | [Projektledare, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1jm3_XFQ_TdJ) |
| 4 | ...er dig breda kunskaper i bl a **elteknik**, mekanik, automation och unde... | x | x | 8 | 8 | [Elteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/TGZ1_Xb5_77h) |
| 5 | ...da kunskaper i bl a elteknik, **mekanik**, automation och underhållspla... | x | x | 7 | 7 | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| 6 | ...per i bl a elteknik, mekanik, **automation** och underhållsplanering. De f... | x |  |  | 10 | [Automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/tDxT_7oT_NyU) |
| 7 | ...nderhållsplanering. De flesta **utbildningar inom underhållsteknik** är antingen mot el- eller mek... | x |  |  | 34 | [Utbildning till underhållstekniker (YH), **keyword**](http://data.jobtechdev.se/taxonomy/concept/LcZM_TLH_Mi6) |
| 8 | .... De flesta utbildningar inom **underhållsteknik** är antingen mot el- eller mek... |  | x |  | 16 | [Underhållsarbete, VA-nät/Underhållsarbete, gatunät, **skill**](http://data.jobtechdev.se/taxonomy/concept/2SCx_2dX_M79) |
| 9 | ...erhållsteknik är antingen mot **el**- eller mekanikområdet men den... | x |  |  | 2 | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| 10 | ...nik är antingen mot el- eller **mekanikområdet** men denna utbildning ger dig ... | x |  |  | 14 | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| 11 | ...t av maskinparken samt hjälpa **maskinoperatörerna** i deras arbete att underhålla... | x |  |  | 18 | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
| 12 | ...felet, genom olika metoder av **felsökning** och se till att maskinen fung... |  | x |  | 10 | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| 13 | ...r igen. En underhållstekniker **arbetar** både självständigt och i team... | x |  |  | 7 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 14 | ...derhållstekniker arbetar både **självständigt** och i team och samarbetar med... | x |  |  | 13 | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
| 15 | ... självständigt och i team och **samarbetar med både produktionsledningen, driftspersonal** och kunder. Utbildningen inne... | x |  |  | 56 | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| 16 | ...betsledning, organisation och **kommunikation**.    ARBETSGIVARNA BEHÖVER DIG... | x |  |  | 13 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| 17 | ...a kunder, vilket kan innebära **resor både inom och utanför Sverige**. Utbildningen samarbetar med ... | x |  |  | 35 | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| 18 | ...a resor både inom och utanför **Sverige**. Utbildningen samarbetar med ... |  | x |  | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 19 | ...SAMARBETE MED NÄRINGSLIVET    **Utbildningen är 1,5 år** och har tre skolförlagda daga... | x |  |  | 22 | [Högskoleutbildning, yrkesinriktad, kortare än 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/oroB_8GN_Fe5) |
| 20 | ...m du vill läsa utbildningen i **Gislaved** eller Vetlanda. Utbildningen ... | x | x | 8 | 8 | [Gislaved, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cNQx_Yqi_83Q) |
| 21 | ...utbildningen i Gislaved eller **Vetlanda**. Utbildningen genomsyras av e... | x | x | 8 | 8 | [Vetlanda, **municipality**](http://data.jobtechdev.se/taxonomy/concept/xJqx_SLC_415) |
| 22 | ... leder till jobb direkt efter **examen**.    KURSER I UTBILDNINGEN    ... | x |  |  | 6 | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
| 23 | ...skurs    Ellära för teoretiks **B-behörighet**    Pneumatik och hydraulik   ... |  | x |  | 12 | [B-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/HeXq_amc_ABd) |
| 24 | ...för teoretiks B-behörighet    **Pneumatik** och hydraulik    Mekanik, mat... | x | x | 9 | 9 | [Pneumatik, **skill**](http://data.jobtechdev.se/taxonomy/concept/pXy8_4zz_azx) |
| 25 | ...B-behörighet    Pneumatik och **hydraulik**    Mekanik, material och trib... | x |  |  | 9 | [Hydraulik, **skill**](http://data.jobtechdev.se/taxonomy/concept/73Pw_nip_4EW) |
| 25 | ...B-behörighet    Pneumatik och **hydraulik**    Mekanik, material och trib... |  | x |  | 9 | [hydraulik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zwYL_deZ_JXX) |
| 26 | ...   Pneumatik och hydraulik    **Mekanik**, material och tribologi    LI... | x | x | 7 | 7 | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
| 27 | ...  LIA 1 (Lärande i arbete)    **Automation** och styrteknik    Underhållsp... | x |  |  | 10 | [Automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/tDxT_7oT_NyU) |
| 28 | ...e i arbete)    Automation och **styrteknik**    Underhållsplanering    Arb... |  | x |  | 10 | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
| 29 | ...lanering    Arbetsledning och **kommunikation**    LIA 2 (Lärande i arbete)  ... | x |  |  | 13 | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| | **Overall** | | | **47** | **621** | 47/621 = **8%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Projektledare, el, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/1jm3_XFQ_TdJ) |
|  | x |  | [Underhållsarbete, VA-nät/Underhållsarbete, gatunät, **skill**](http://data.jobtechdev.se/taxonomy/concept/2SCx_2dX_M79) |
|  | x |  | [Konstruktör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/44U4_7Fz_GCy) |
|  | x |  | [Processtekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/4eYC_a1M_xtL) |
| x |  |  | [Hydraulik, **skill**](http://data.jobtechdev.se/taxonomy/concept/73Pw_nip_4EW) |
|  | x |  | [Civilingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/84z6_pWk_XJQ) |
|  | x |  | [Utvecklingsingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8r8B_8bP_usp) |
|  | x |  | [Automationstekniker, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/CC4H_YCh_77T) |
|  | x |  | [B-truckar, **skill**](http://data.jobtechdev.se/taxonomy/concept/HeXq_amc_ABd) |
| x |  |  | [Yrkesbevis/examen, **skill-headline**](http://data.jobtechdev.se/taxonomy/concept/HjPN_99i_cFD) |
|  | x |  | [göra felsökning, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HqC8_fVY_ACM) |
| x |  |  | [Utbildning till underhållstekniker (YH), **keyword**](http://data.jobtechdev.se/taxonomy/concept/LcZM_TLH_Mi6) |
|  | x |  | [Produktionsledare, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/NaMW_ie6_VVV) |
| x |  |  | [arbeta självständigt, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/NyFu_HHd_zhz) |
|  | x |  | [Kalibreringsingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/RU3Y_Gqt_h3c) |
| x |  |  | [elektronik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/S9Th_73n_Fpu) |
| x | x | x | [Elteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/TGZ1_Xb5_77h) |
|  | x |  | [Styr- och reglerteknik, **skill**](http://data.jobtechdev.se/taxonomy/concept/Udbb_Eay_MuR) |
| x |  |  | [samarbeta med kolleger, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/X15R_797_q62) |
| x |  |  | [kommunikation, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/ahPU_HgE_Jeb) |
| x | x | x | [Gislaved, **municipality**](http://data.jobtechdev.se/taxonomy/concept/cNQx_Yqi_83Q) |
|  | x |  | [Kundserviceingenjör, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/cPyP_GEv_NJ5) |
| x |  |  | [Övriga process- och maskinoperatörer, **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/hxm6_xkr_dHm) |
|  | x |  | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x |  |  | [Resor i arbetet förekommer, **work-place-environment**](http://data.jobtechdev.se/taxonomy/concept/oaM2_JTK_muh) |
| x |  |  | [Högskoleutbildning, yrkesinriktad, kortare än 2 år, **sun-education-level-3**](http://data.jobtechdev.se/taxonomy/concept/oroB_8GN_Fe5) |
| x | x | x | [Pneumatik, **skill**](http://data.jobtechdev.se/taxonomy/concept/pXy8_4zz_azx) |
| x |  |  | [Automation, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/tDxT_7oT_NyU) |
| x | x | x | [Vetlanda, **municipality**](http://data.jobtechdev.se/taxonomy/concept/xJqx_SLC_415) |
| x | x | x | [Mekanik, **skill**](http://data.jobtechdev.se/taxonomy/concept/ygEQ_Pwp_ozn) |
|  | x |  | [Produktionsplanerare, tillverkningsindustri, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/zmTy_qPb_e9e) |
|  | x |  | [hydraulik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/zwYL_deZ_JXX) |
| | | **5** | 5/32 = **16%** |