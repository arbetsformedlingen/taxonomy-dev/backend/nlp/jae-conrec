# Results for '00bf1cac01cdbbb0273b2a89c9d9c76b0dd32968'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [00bf1cac01cdbbb0273b2a89c9d9c76b0dd32968](README.md) | 1 | 1422 | 9 | 6 | 39/89 = **44%** | 4/7 = **57%** |

## Source text

Extrajobb i Norrköpings krogliv Extrajobb i Norrköpings krogliv   Vi söker dig som vill vara en del av utelivet i stan och arbeta i en rolig och positiv miljö. Du är utåtriktad och tycker om att arbeta direkt mot gästerna för att skapa en trevlig stämning. Vill du ha ett socialt utvecklande extrajobb där ingen kväll är den andra lik? Då har vi jobbet för dig. Cherry Spelglädje söker blivande dealers i Norrköping. Vi erbjuder casino på barer, nattklubbar och restauranger. Att arbeta som dealer är ett väldigt spännande jobb som lämpar sig bra för dig som studerar, behöver ett arbete till eller helt enkelt vill ha väldigt roligt på jobbet. Ett arbete hos oss är flexibelt då Cherry Spelglädje bedriver casino runt om i hela Sverige från Kiruna till Ystad. Så du kan med fördel ta med dig jobbet om du flyttar till annan ort. Vi erbjuder minst fyra helgkvällar i månaden vilket motsvarar ca 20 timmar. Möjlighet att arbeta mer vardagar och helger utöver detta finns. Ingen tidigare erfarenhet krävs, då vi lägger större vikt vid dina personliga egenskaper. Vi erbjuder lön enligt kollektivavtal samt möjlighet till provision.   Känner du att detta är ett arbete som passar dig lämna du enkelt din ansökan på www.cherry.se/jobba/  Om du har frågor kan du vända dig till Leona Brandt som är ansvarig i Norrköping. Du når Leona på 0739-80 16 36 eller leona.brandt@cherry.se. Kolla gärna in vår Instagram @cherryspelgladje

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Extrajobb i **Norrköpings** krogliv Extrajobb i Norrköpin... | x |  |  | 11 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 2 | ...rrköpings krogliv Extrajobb i **Norrköpings** krogliv   Vi söker dig som vi... | x |  |  | 11 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 3 | ...ädje söker blivande dealers i **Norrköping**. Vi erbjuder casino på barer,... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| 4 | ... restauranger. Att arbeta som **dealer** är ett väldigt spännande jobb... | x | x | 6 | 6 | [Dealer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/r8ph_Hv3_eWg) |
| 5 | ...edriver casino runt om i hela **Sverige** från Kiruna till Ystad. Så du... | x | x | 7 | 7 | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| 6 | ...o runt om i hela Sverige från **Kiruna** till Ystad. Så du kan med för... | x | x | 6 | 6 | [Kiruna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/biN6_UiL_Qob) |
| 7 | ...hela Sverige från Kiruna till **Ystad**. Så du kan med fördel ta med ... | x |  |  | 5 | [Ystad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hdYk_hnP_uju) |
| 8 | ...i månaden vilket motsvarar ca **20 timmar**. Möjlighet att arbeta mer var... | x |  |  | 9 | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| 9 | ...kaper. Vi erbjuder lön enligt **kollektivavtal** samt möjlighet till provision... |  | x |  | 14 | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| 10 | ...eona Brandt som är ansvarig i **Norrköping**. Du når Leona på 0739-80 16 3... | x | x | 10 | 10 | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| | **Overall** | | | **39** | **89** | 39/89 = **44%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Deltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2) |
| x | x | x | [Norrköping, **municipality**](http://data.jobtechdev.se/taxonomy/concept/SYty_Yho_JAF) |
| x | x | x | [Kiruna, **municipality**](http://data.jobtechdev.se/taxonomy/concept/biN6_UiL_Qob) |
| x |  |  | [Ystad, **municipality**](http://data.jobtechdev.se/taxonomy/concept/hdYk_hnP_uju) |
| x | x | x | [Sverige, **country**](http://data.jobtechdev.se/taxonomy/concept/i46j_HmG_v64) |
| x | x | x | [Dealer, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/r8ph_Hv3_eWg) |
|  | x |  | [Kollektivavtal, **skill**](http://data.jobtechdev.se/taxonomy/concept/z7wE_9xb_3jX) |
| | | **4** | 4/7 = **57%** |