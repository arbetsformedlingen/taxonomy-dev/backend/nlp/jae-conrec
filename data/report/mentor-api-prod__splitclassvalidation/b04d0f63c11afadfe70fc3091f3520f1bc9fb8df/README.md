# Results for 'b04d0f63c11afadfe70fc3091f3520f1bc9fb8df'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [b04d0f63c11afadfe70fc3091f3520f1bc9fb8df](README.md) | 1 | 4126 | 21 | 18 | 85/225 = **38%** | 7/18 = **39%** |

## Source text

Allente söker säljande kundservicemedarbetare! Just nu söker vi nya säljinriktade servicestjärnor till vårt gäng på Allentes Kundservice i Kalmar!  Ett jobb där du kommer att tillbringa dina dagar på en energifylld och härlig arbetsplats omgiven av engagerade och kunniga medarbetare Där dina huvudsakliga arbetsuppgifter kommer bestå utav att ge kundservice i världsklass till Allentes kunder, detta genom att svara på inkommande kundsamtal där ditt huvudfokus ligger på att bemöta kunder med lyhördhet och engagemang samt att kunna anpassa ditt bemötande utefter kundens behov och önskemål.  Arbetsrollen är bred och du hanterar inkommande samtal inom kundservice, fakturafrågor och teknisk support. För oss är det viktigt att du ska lyckas! Därför arbetar vi mot tydliga mål som kontinuerligt följs upp för att främja din utveckling.  Försäljning är en naturlig del av varje samtal, det skapar förutsättningar att skräddarsy anpassade lösningar utifrån varje kunds behov. Fördelen med att jobba på Webhelp är att du får möjlighet att utvecklas inom försäljning och har chansen att påverka din egen lön. Det finns inget tak på provisionen, det innebär att du kan tjäna riktigt bra - utöver din grundlön!  Vi ger dig möjligheten att utvecklas i Kalmar, lära dig försäljning och mera...  Då Webhelp växer i en snabb takt finns det, för dig med rätt drivkraft, goda chanser till utveckling med interna karriärmöjligheter. Vi är idag den tredje största kundtjänstleverantören i världen där vi strävar efter att nå toppen - det gör vi gärna tillsammans med dig! Stämningen på våra kontor är energisk, öppen och positiv. Medarbetarna delar med sig av erfarenheter och tips som kan underlätta arbetet och bidrar till en god stämning på kontoret. För att stärka sammanhållningen i våra team anordnas det med jämna mellanrum aktiviteter och gemensamma tävlingar.  Blir du vår kollega kommer du börja tjänsten med en utbildning som sträcker sig över 4 veckor där vi varierar utbildning med arbete. Under utbildningen fokuserar vi på att förbereda dig som nyanställd på bästa sätt och ger dig kunskaper inom följande områden:  - Kundsystem - Fakturahantering - Teknik - Försäljning, retorik och samtalshantering   Sammantaget ska du på bästa tänkbara sätt lösa kundens ärende och leverera en kundupplevelse i världsklass.  Vi söker dig som har grundläggande datorkunskap och... Du tycker om att möta nya människor och lägger stor vikt vid att vara hjälpsam, såväl mot kunder som kollegor. Du tycker försäljning låter intressant och ser fram emot att utvecklas i din arbetsroll. Tidigare erfarenhet inom service och försäljning är meriterande men inget krav.  Vi vill att du uppfyller följande:  - Godkända betyg i svenska A eller 1 och engelska A eller 5 - Talar flytande Svenska och känner dig bekväm med samtal på engelska. - Fyllt 18 år - Finns ej med i belastningsregister     STARTALTERNATIV: 2022-08-01  PLACERINGSORT: Kalmar  ANSTÄLLNINGSFORM: Tillsvidareanställning på 100% (börjar med 6 månaders provanställning). Timanställning, flexibel anställningsform på 75%-100% efter överenskommelse.  ARBETSTIDER: Varierande under öppettiderna, Mån-Fre 08.00-19.00, helg 10.00-19.00  SISTA ANSÖKNINGSDATUM: 2022-07-25  Sök jobbet redan idag! Vi gör löpande rekryteringar och urval och tjänsten kan komma att tillsättas innan sista ansökningsdatum.    Webhelp is a multinational Business Process Outsourcing company with its head office in Paris, France, that is making business more human for the world’s most exciting brands.  By choosing Webhelp, you will be part of a community of over 100'000 game-changers from more than 190 locations, speaking more than 80 languages in 50+ countries.   Join us at Webhelp you will have the opportunity to be part of a multicultural team, a fast-paced environment, and a diverse pool of engaging projects for some of the most progressive brands worldwide.  More information can be found at  www.webhelp.com (http://www.webhelp.com/)  Non-discrimination: Webhelp sees diversity as a resource and encourages all regardless of gender, age, religion, ethnic origin to seek employment with us  Swedish

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Allente söker säljande **kundservicemedarbetare**! Just nu söker vi nya säljinr... |  | x |  | 22 | [Kundtjänstmedarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8Uhp_XYo_z5f) |
| 1 | Allente söker säljande **kundservicemedarbetare**! Just nu söker vi nya säljinr... | x |  |  | 22 | [Kundservicemedarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/9XrQ_sWD_twF) |
| 2 | ...or till vårt gäng på Allentes **Kundservice** i Kalmar!  Ett jobb där du ko... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 3 | ...äng på Allentes Kundservice i **Kalmar**!  Ett jobb där du kommer att ... | x | x | 6 | 6 | [Kalmar, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Pnmg_SgP_uHQ) |
| 4 | ...fter kommer bestå utav att ge **kundservice** i världsklass till Allentes k... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 5 | ...nterar inkommande samtal inom **kundservice**, fakturafrågor och teknisk su... | x | x | 11 | 11 | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| 6 | ...n lön. Det finns inget tak på **provisionen**, det innebär att du kan tjäna... | x |  |  | 11 | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
| 7 | ...g möjligheten att utvecklas i **Kalmar**, lära dig försäljning och mer... | x | x | 6 | 6 | [Kalmar, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Pnmg_SgP_uHQ) |
| 8 | ...s med dig! Stämningen på våra **kontor** är energisk, öppen och positi... | x |  |  | 6 | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
| 9 | ...ering - Teknik - Försäljning, **retorik** och samtalshantering   Samman... | x | x | 7 | 7 | [retorik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rNfF_a6W_8Nq) |
| 10 | ...ker dig som har grundläggande **datorkunskap** och... Du tycker om att möta ... |  | x |  | 12 | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| 11 | ...följande:  - Godkända betyg i **svenska** A eller 1 och engelska A elle... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 12 | ...betyg i svenska A eller 1 och **engelska** A eller 5 - Talar flytande Sv... |  | x |  | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 12 | ...betyg i svenska A eller 1 och **engelska** A eller 5 - Talar flytande Sv... | x |  |  | 8 | [Engelska, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xiD7_snJ_D8x) |
| 13 | ...ka A eller 5 - Talar flytande **Svenska** och känner dig bekväm med sam... | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| 14 | ...nner dig bekväm med samtal på **engelska**. - Fyllt 18 år - Finns ej med... |  | x |  | 8 | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| 14 | ...nner dig bekväm med samtal på **engelska**. - Fyllt 18 år - Finns ej med... | x |  |  | 8 | [Engelska, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xiD7_snJ_D8x) |
| 15 | ...V: 2022-08-01  PLACERINGSORT: **Kalmar**  ANSTÄLLNINGSFORM: Tillsvidar... |  | x |  | 6 | [Kalmar, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Pnmg_SgP_uHQ) |
| 16 | ... 6 månaders provanställning). **Timanställning**, flexibel anställningsform på... | x |  |  | 14 | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| 17 | ...cess Outsourcing company with **its** head office in Paris, France,... | x | x | 3 | 3 | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| 18 | ...ourcing company with its head **office** in Paris, France, that is mak... |  | x |  | 6 | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| 19 | ...ss more human for the world’s **most** exciting brands.  By choosing... | x | x | 4 | 4 | [MOST, kommunikationsprotokoll, **skill**](http://data.jobtechdev.se/taxonomy/concept/ob2k_Bcp_XHY) |
| 20 | ...ging projects for some of the **most** progressive brands worldwide.... | x | x | 4 | 4 | [MOST, kommunikationsprotokoll, **skill**](http://data.jobtechdev.se/taxonomy/concept/ob2k_Bcp_XHY) |
| 21 | ... worldwide.  More information **can** be found at  www.webhelp.com ... | x |  |  | 3 | [CAN, kommunikationsprotokoll, **skill**](http://data.jobtechdev.se/taxonomy/concept/nTdr_FJz_ioy) |
| 22 | ... can be found at  www.webhelp.**com** (http://www.webhelp.com/)  No... | x |  |  | 3 | [Object Component Model/COM, **skill**](http://data.jobtechdev.se/taxonomy/concept/Np6W_aQg_Rkn) |
| 23 | ...bhelp.com (http://www.webhelp.**com**/)  Non-discrimination: Webhel... | x |  |  | 3 | [Object Component Model/COM, **skill**](http://data.jobtechdev.se/taxonomy/concept/Np6W_aQg_Rkn) |
| 24 | ...ll regardless of gender, age, **religion**, ethnic origin to seek employ... | x | x | 8 | 8 | [Religion, **keyword**](http://data.jobtechdev.se/taxonomy/concept/pLEo_8vZ_jG5) |
| | **Overall** | | | **85** | **225** | 85/225 = **38%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [Kundtjänstmedarbetare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/8Uhp_XYo_z5f) |
| x |  |  | [Kundservicemedarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/9XrQ_sWD_twF) |
|  | x |  | [använda dator, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/HEbR_xrb_MpT) |
| x |  |  | [Kontor, **keyword**](http://data.jobtechdev.se/taxonomy/concept/Hyz1_7HF_AkP) |
|  | x |  | [Engelska, **language**](http://data.jobtechdev.se/taxonomy/concept/NVxJ_hLg_TYS) |
| x |  |  | [Object Component Model/COM, **skill**](http://data.jobtechdev.se/taxonomy/concept/Np6W_aQg_Rkn) |
| x | x | x | [Kalmar, **municipality**](http://data.jobtechdev.se/taxonomy/concept/Pnmg_SgP_uHQ) |
| x | x | x | [kundservice, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/WykS_79d_Fz7) |
| x | x | x | [ITS (internationell spedition), **skill**](http://data.jobtechdev.se/taxonomy/concept/ePzH_pgt_BsZ) |
| x |  |  | [Timanställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/kfXw_Vmt_Zzk) |
| x |  |  | [CAN, kommunikationsprotokoll, **skill**](http://data.jobtechdev.se/taxonomy/concept/nTdr_FJz_ioy) |
| x | x | x | [MOST, kommunikationsprotokoll, **skill**](http://data.jobtechdev.se/taxonomy/concept/ob2k_Bcp_XHY) |
| x | x | x | [Religion, **keyword**](http://data.jobtechdev.se/taxonomy/concept/pLEo_8vZ_jG5) |
| x | x | x | [retorik, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/rNfF_a6W_8Nq) |
| x |  |  | [Rörlig ackords- eller provisionslön, **wage-type**](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu) |
|  | x |  | [MS Office, **skill**](http://data.jobtechdev.se/taxonomy/concept/w8bU_wHe_omF) |
| x |  |  | [Engelska, **keyword**](http://data.jobtechdev.se/taxonomy/concept/xiD7_snJ_D8x) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **7** | 7/18 = **39%** |