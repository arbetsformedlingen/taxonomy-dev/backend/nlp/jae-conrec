# Results for 'a76dc3af57bfbf4f8419081817c7060a8d140538'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [a76dc3af57bfbf4f8419081817c7060a8d140538](README.md) | 1 | 1142 | 2 | 2 | 14/42 = **33%** | 1/3 = **33%** |

## Source text

Biträdande Restaurangchef Just nu söker vi vår nya stjärna!!!! Vi är nu på jakt efter en person att leda vårt fina team! Din personlighet är din viktigaste egenskap. Vi letar efter någon som är en naturlig ledare och som med positivitet och engagemang driver restaurangen framåt.  Du har jobbat i branschen tidigare och förstår vikten av såväl nöjda gäster som medarbetare.  Vi kan erbjuda dig ett ambitiöst och härligt team att jobba tillsammans med stora möjligheter att utvecklas ett innovativt koncept en rolig arbetsplats  möjligheter att påverka och inspirera     Vi vill gärna att du trivs med personalansvar - rekrytering, anställning, utbildning, uppföljning och schemaläggning älskar att arbeta med människor och kan få såväl gäster som kollegor att trivas på Pinchos är duktig på daglig administration för att följa och nå våra uppsatta mål gillar att leda servisen och vara en del av den dagliga driften drivs av försäljning    Låter det som något för dig? Skynda dig att söka! Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Biträdande **Restaurangchef** Just nu söker vi vår nya stjä... | x |  |  | 14 | [restaurangchef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8pov_8nw_N1D) |
| 1 | Biträdande **Restaurangchef** Just nu söker vi vår nya stjä... |  | x |  | 14 | [Restaurangchef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XSBp_5wD_VaU) |
| 2 | ...i vill gärna att du trivs med **personalansvar** - rekrytering, anställning, u... | x | x | 14 | 14 | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| | **Overall** | | | **14** | **42** | 14/42 = **33%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [restaurangchef, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/8pov_8nw_N1D) |
|  | x |  | [Restaurangchef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/XSBp_5wD_VaU) |
| x | x | x | [Personalansvar, **skill**](http://data.jobtechdev.se/taxonomy/concept/ZzN5_m7B_5CY) |
| | | **1** | 1/3 = **33%** |