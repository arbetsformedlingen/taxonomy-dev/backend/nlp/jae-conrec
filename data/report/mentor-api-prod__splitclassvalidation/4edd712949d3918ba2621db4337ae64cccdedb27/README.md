# Results for '4edd712949d3918ba2621db4337ae64cccdedb27'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [4edd712949d3918ba2621db4337ae64cccdedb27](README.md) | 1 | 2218 | 25 | 19 | 202/268 = **75%** | 9/16 = **56%** |

## Source text

SVT Nyheter Stockholm söker reporter Vad vi erbjuder dig På SVT Nyheter Stockholm vill vi göra skillnad. Genom vass och relevant journalistik ska vi beröra och intressera två miljoner människor i Mälardalen. Vi gör det genom att göra agendasättande journalistik och rapportera om händelser som sker här och nu. Vi vill berätta har ett tydligt onlinefokus mot berättande i rörlig bild.  Nu söker vi en nyhetsreporter som kan ta sig an alla ämnen, har tusen egna uppslag och kan slutföra och hålla deadline.  och har en god förståelse för berättande i rörlig bild online. Vi är 21 medarbetare på SVT Nyheter Stockholm som tillsammans med vår systerredaktion i Södertälje gör nyheter för Stockholms län. Vi har redaktionslokaler i Norrtälje, Rinkeby, Flemingsberg och i TV-huset på Gärdet. Denna tjänst är placerad i TV-huset.  Vem vi tror att du är Du är en nyfiken person som är duktig på att nyhetsvärdera och du drivs av att få göra skillnad med din journalistik.   Du är självgående med ett stort eget driv och förmåga att sätta nyheter i ett sammanhang.  Du växlar gärna mellan att jobba med planerad journalistik och snabba nyhetshändelser. Du håller en hög kvalitet på dina jobb och är publicistiskt medveten.  Tjänsten kräver:   •         Journalistutbildning, eller motsvarande.   •         Tidigare erfarenhet av journalistiskt arbete. •         B-Körkort  •         Erfarenhet av rörligt bildberättande och redigering är meriterande.   Vad den här tjänsten innebär Att bevaka och granska det som händer i Stockholms län, i första hand för publicering online men också för TV. Som reporter producerar du självständigt lokal nyhetsvideo och texter, från idé till färdigt innehåll. I rollen ingår att fota, redigera, skriva och publicera på alla plattformar.   Hur du ansöker Anställningen är en tillsvidaretjänst. Placeringen är på redaktionen i TV-huset på Gärdet och i anställningen ingår morgon-, kvälls- och helgpass. Vi ser fram emot din ansökan senast den 15/8.  Har du frågor om anställningen är du välkommen att kontakta redaktionschef Geronimo Åkerlund, geronimo.akerlund@svt.se.  Fackliga frågor besvaras av Margareta Benson, SJF som nås på växelnummer 08-784 00 00.   Varmt välkommen med din ansökan!

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | SVT Nyheter **Stockholm** söker reporter Vad vi erbjude... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 2 | SVT Nyheter Stockholm söker **reporter** Vad vi erbjuder dig På SVT Ny... | x | x | 8 | 8 | [Journalist/Reporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BduY_UUY_pvK) |
| 3 | ...i erbjuder dig På SVT Nyheter **Stockholm** vill vi göra skillnad. Genom ... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 4 | ...lnad. Genom vass och relevant **journalistik** ska vi beröra och intressera ... | x | x | 12 | 12 | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| 5 | ...genom att göra agendasättande **journalistik** och rapportera om händelser s... | x | x | 12 | 12 | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| 6 | ... onlinefokus mot berättande i **rörlig bild**.  Nu söker vi en nyhetsreport... | x | x | 11 | 11 | [Rörlig bild, **skill**](http://data.jobtechdev.se/taxonomy/concept/sjcy_d9R_ZGE) |
| 7 | ... rörlig bild.  Nu söker vi en **nyhets**reporter som kan ta sig an all... |  | x |  | 6 | [Journalist/Reporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BduY_UUY_pvK) |
| 8 | ...g bild.  Nu söker vi en nyhets**reporter** som kan ta sig an alla ämnen,... | x | x | 8 | 8 | [Journalist/Reporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BduY_UUY_pvK) |
| 9 | ...d förståelse för berättande i **rörlig bild** online. Vi är 21 medarbetare ... | x | x | 11 | 11 | [Rörlig bild, **skill**](http://data.jobtechdev.se/taxonomy/concept/sjcy_d9R_ZGE) |
| 10 | ...21 medarbetare på SVT Nyheter **Stockholm** som tillsammans med vår syste... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 11 | ...ans med vår systerredaktion i **Södertälje** gör nyheter för Stockholms lä... | x | x | 10 | 10 | [Södertälje, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g6hK_M1o_hiU) |
| 12 | ... i Södertälje gör nyheter för **Stockholms län**. Vi har redaktionslokaler i N... | x | x | 14 | 14 | [Stockholms län, **region**](http://data.jobtechdev.se/taxonomy/concept/CifL_Rzy_Mku) |
| 13 | ...n. Vi har redaktionslokaler i **Norrtälje**, Rinkeby, Flemingsberg och i ... |  | x |  | 9 | [Norrtälje, **municipality**](http://data.jobtechdev.se/taxonomy/concept/btgf_fS7_sKG) |
| 14 | ...em vi tror att du är Du är en **nyfiken** person som är duktig på att n... | x |  |  | 7 | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
| 15 | ... att få göra skillnad med din **journalistik**.   Du är självgående med ett ... | x | x | 12 | 12 | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| 16 | ...mellan att jobba med planerad **journalistik** och snabba nyhetshändelser. D... | x | x | 12 | 12 | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| 17 | ... Tjänsten kräver:   •         **Journalistutbildning**, eller motsvarande.   •      ... | x | x | 20 | 20 | [Journalistik och information, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/MwbE_Nvv_VN4) |
| 18 | ...rnalistiskt arbete. •         **B-Körkort**  •         Erfarenhet av rörl... | x | x | 9 | 9 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 19 | ...kt arbete. •         B-Körkort** ** •         Erfarenhet av rörli... | x |  |  | 1 | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| 20 | ...av rörligt bildberättande och **redigering** är meriterande.   Vad den här... | x |  |  | 10 | [Redigering, rörlig bild, **skill**](http://data.jobtechdev.se/taxonomy/concept/Eizh_YsE_amu) |
| 21 | ... och granska det som händer i **Stockholms län**, i första hand för publicerin... | x | x | 14 | 14 | [Stockholms län, **region**](http://data.jobtechdev.se/taxonomy/concept/CifL_Rzy_Mku) |
| 22 | ...licering online men också för **TV**. Som reporter producerar du s... | x |  |  | 2 | [TV, **skill**](http://data.jobtechdev.se/taxonomy/concept/YBuN_Wgv_19X) |
| 23 | ... online men också för TV. Som **reporter** producerar du självständigt l... | x | x | 8 | 8 | [Journalist/Reporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BduY_UUY_pvK) |
| 24 | ... innehåll. I rollen ingår att **fota**, redigera, skriva och publice... | x |  |  | 4 | [Fotografering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zpaf_mDu_foK) |
| 25 | ...åll. I rollen ingår att fota, **redigera**, skriva och publicera på alla... | x |  |  | 8 | [Redigerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EFw9_WtY_7jQ) |
| 26 | ...u ansöker Anställningen är en **tillsvidaretjänst**. Placeringen är på redaktione... | x |  |  | 17 | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| 27 | ...aceringen är på redaktionen i **TV**-huset på Gärdet och i anställ... | x |  |  | 2 | [TV, **skill**](http://data.jobtechdev.se/taxonomy/concept/YBuN_Wgv_19X) |
| 28 | ... är du välkommen att kontakta **redaktionschef** Geronimo Åkerlund, geronimo.a... | x | x | 14 | 14 | [Redaktionschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/RdVK_4zC_kFV) |
| | **Overall** | | | **202** | **268** | 202/268 = **75%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| x | x | x | [Journalist/Reporter, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/BduY_UUY_pvK) |
| x | x | x | [Stockholms län, **region**](http://data.jobtechdev.se/taxonomy/concept/CifL_Rzy_Mku) |
| x |  |  | [Redigerare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/EFw9_WtY_7jQ) |
| x |  |  | [Redigering, rörlig bild, **skill**](http://data.jobtechdev.se/taxonomy/concept/Eizh_YsE_amu) |
| x | x | x | [Journalistik och information, allmän utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/MwbE_Nvv_VN4) |
| x | x | x | [Redaktionschef, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/RdVK_4zC_kFV) |
| x | x | x | [B, **driving-licence**](http://data.jobtechdev.se/taxonomy/concept/VTK8_WRx_GcM) |
| x |  |  | [TV, **skill**](http://data.jobtechdev.se/taxonomy/concept/YBuN_Wgv_19X) |
|  | x |  | [Norrtälje, **municipality**](http://data.jobtechdev.se/taxonomy/concept/btgf_fS7_sKG) |
| x | x | x | [Journalistik, **keyword**](http://data.jobtechdev.se/taxonomy/concept/g4uT_VRd_5WC) |
| x | x | x | [Södertälje, **municipality**](http://data.jobtechdev.se/taxonomy/concept/g6hK_M1o_hiU) |
| x |  |  | [Tillsvidareanställning (inkl. eventuell provanställning), **employment-type**](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU) |
| x |  |  | [visa nyfikenhet, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/mbHa_NFf_vyf) |
| x | x | x | [Rörlig bild, **skill**](http://data.jobtechdev.se/taxonomy/concept/sjcy_d9R_ZGE) |
| x |  |  | [Fotografering, **skill**](http://data.jobtechdev.se/taxonomy/concept/zpaf_mDu_foK) |
| | | **9** | 9/16 = **56%** |