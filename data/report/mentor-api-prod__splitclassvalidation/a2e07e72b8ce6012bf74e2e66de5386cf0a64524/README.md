# Results for 'a2e07e72b8ce6012bf74e2e66de5386cf0a64524'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [a2e07e72b8ce6012bf74e2e66de5386cf0a64524](README.md) | 1 | 1660 | 18 | 11 | 75/152 = **49%** | 7/13 = **54%** |

## Source text

Wemotion AB söker mjukvaruutvecklare Wemotion AB utvecklar och säljer produkter som ska göra träning mer intressant och underhållande. Vi jobbar med innovativa produkter som påminner mycket om datorspel, men som dessutom ger fysisk träning.  Ett exempel är Webike där användarna kan träna och tävla i en virtuell värld genom att cykla på motionscyklar.   Utöver att vi jobbar med våra egna produkter tar vi externa uppdrag, helst inom teknikområden som tangerar spelutveckling, men det är även vanligt att vi tar helt andra uppdrag. Du ska ha ett brett tekniskt intresse och trivas med att jobba ute hos kunder.   Under arbetet med våra egna produkter är några typiska tekniker som vi jobbar med Unity, Windows Azure, C++, C#, .Net, och utveckling av appar för iOS och Android.   I konsultrollen är det meriterande med erfarenhet av t.ex. C, C++, C#, .Net, Java och Internetteknik.   Du behöver dock inte vara expert på alla dessa områden för att söka. Det som däremot är viktigt är att du är en duktig och driven mjukvaruutvecklare. Lämplig utbildningsbakgrund är civilingenjör eller motsvarande. På Wemotion jobbar vi både som konsulter och med våra egna produkter.  Wemotion utvecklar och säljer produkter som ska göra träning mer intressant och underhållande. Vi jobbar med innovativa produkter som påminner mycket om datorspel, men som dessutom ger fysisk träning.. Ett exempel är Webike där användarna kan träna och tävla i en virtuell värld genom att cykla på motionscyklar.  Utöver att vi jobbar med våra egna produkter tar vi externa uppdrag, helst inom teknikområden som tangerar spelutveckling, men det är även vanligt att vi tar helt andra uppdrag.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Wemotion AB söker **mjukvaruutvecklare** Wemotion AB utvecklar och säl... | x | x | 18 | 18 | [Mjukvaruutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rQds_YGd_quU) |
| 2 | ...säljer produkter som ska göra **träning** mer intressant och underhålla... | x |  |  | 7 | [Träning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/n19Z_hzx_Apu) |
| 3 | ..., men som dessutom ger fysisk **träning**.  Ett exempel är Webike där a... | x |  |  | 7 | [Träning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/n19Z_hzx_Apu) |
| 4 | ...ka tekniker som vi jobbar med **Unity**, Windows Azure, C++, C#, .Net... | x | x | 5 | 5 | [Unity, spelmotor, **skill**](http://data.jobtechdev.se/taxonomy/concept/bN1e_YZo_pHh) |
| 5 | ...iker som vi jobbar med Unity, **Windows Azure**, C++, C#, .Net, och utvecklin... | x | x | 13 | 13 | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| 6 | ...bar med Unity, Windows Azure, **C++**, C#, .Net, och utveckling av ... | x | x | 3 | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 7 | ...ed Unity, Windows Azure, C++, **C#**, .Net, och utveckling av appa... | x | x | 2 | 2 | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| 8 | ..., och utveckling av appar för **iOS** och Android.   I konsultrolle... | x |  |  | 3 | [IOS, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/t78J_nRk_xGn) |
| 9 | ...veckling av appar för iOS och **Android**.   I konsultrollen är det mer... | x | x | 7 | 7 | [Android, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/WQdS_Jtu_qPC) |
| 10 | ...rande med erfarenhet av t.ex. **C**, C++, C#, .Net, Java och Inte... | x |  |  | 1 | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| 11 | ...de med erfarenhet av t.ex. C, **C++**, C#, .Net, Java och Internett... | x | x | 3 | 3 | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| 12 | ...d erfarenhet av t.ex. C, C++, **C#**, .Net, Java och Internettekni... | x | x | 2 | 2 | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| 13 | ...et av t.ex. C, C++, C#, .Net, **Java** och Internetteknik.   Du behö... | x | x | 4 | 4 | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| 14 | ...tt du är en duktig och driven **mjukvaruutvecklare**. Lämplig utbildningsbakgrund ... | x | x | 18 | 18 | [Mjukvaruutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rQds_YGd_quU) |
| 15 | ...n mjukvaruutvecklare. Lämplig **utbildningsbakgrund** är civilingenjör eller motsva... | x |  |  | 19 | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
| 16 | ...ämplig utbildningsbakgrund är **civilingenjör** eller motsvarande. På Wemotio... |  | x |  | 13 | [Civilingenjörsyrken, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/4jZs_twz_epW) |
| 16 | ...ämplig utbildningsbakgrund är **civilingenjör** eller motsvarande. På Wemotio... | x |  |  | 13 | [Civilingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/idCW_nKR_Xnm) |
| 17 | ...säljer produkter som ska göra **träning** mer intressant och underhålla... | x |  |  | 7 | [Träning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/n19Z_hzx_Apu) |
| 18 | ..., men som dessutom ger fysisk **träning**.. Ett exempel är Webike där a... | x |  |  | 7 | [Träning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/n19Z_hzx_Apu) |
| | **Overall** | | | **75** | **152** | 75/152 = **49%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Universitets- eller högskoleutbildning, **sni-level-5**](http://data.jobtechdev.se/taxonomy/concept/4Hs1_pdH_C7k) |
|  | x |  | [Civilingenjörsyrken, **ssyk-level-3**](http://data.jobtechdev.se/taxonomy/concept/4jZs_twz_epW) |
| x | x | x | [Windows Azure, molnplattform, **skill**](http://data.jobtechdev.se/taxonomy/concept/HKgZ_SWa_vn8) |
| x | x | x | [Android, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/WQdS_Jtu_qPC) |
| x | x | x | [Unity, spelmotor, **skill**](http://data.jobtechdev.se/taxonomy/concept/bN1e_YZo_pHh) |
| x |  |  | [Civilingenjörsutbildning, övrig och ospecificerad utbildning, **sun-education-field-4**](http://data.jobtechdev.se/taxonomy/concept/idCW_nKR_Xnm) |
| x | x | x | [C#, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jBKc_5Yx_Y6T) |
| x | x | x | [Java, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/jCsR_WQX_CKD) |
| x |  |  | [Träning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/n19Z_hzx_Apu) |
| x |  |  | [C, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/o8wR_57f_jv9) |
| x | x | x | [Mjukvaruutvecklare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/rQds_YGd_quU) |
| x |  |  | [IOS, operativsystem, **skill**](http://data.jobtechdev.se/taxonomy/concept/t78J_nRk_xGn) |
| x | x | x | [C++, programmeringsspråk, **skill**](http://data.jobtechdev.se/taxonomy/concept/yNm8_krX_usR) |
| | | **7** | 7/13 = **54%** |